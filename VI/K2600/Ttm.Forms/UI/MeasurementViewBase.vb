Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.Drawing
Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions
Imports System.Windows.Forms.DataVisualization.Charting

''' <summary> Measurement panel base. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-04-12 </para><para>
''' David, 2014-03-15 </para>
''' </remarks>
Public Class MeasurementViewBase
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me._TtmMeasureControlsToolStrip.Enabled = False
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.ReleaseResources()
                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " ON CONTROL EVENTS "

    ''' <summary> Releases the resources. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overridable Sub ReleaseResources()
        Me._TriggerSequencer = Nothing
        Me._MeasureSequencer = Nothing
        Me._LastTimeSeries = Nothing
    End Sub

    ''' <summary> Gets or sets the is device open. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The is device open. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Overridable ReadOnly Property IsDeviceOpen As Boolean

    ''' <summary> True if trace is available, false if not. </summary>
    Private _IsTraceAvailable As Boolean

    ''' <summary> Updates the availability of the controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Sub OnStateChanged()

        Dim measurementSequenceState As MeasurementSequenceState = MeasurementSequenceState.None
        If Me.MeasureSequencer IsNot Nothing Then
            measurementSequenceState = Me.MeasureSequencer.MeasurementSequenceState
        End If

        Dim triggerSequenceState As TriggerSequenceState = TriggerSequenceState.None
        If Me.TriggerSequencer IsNot Nothing Then
            triggerSequenceState = Me.TriggerSequencer.TriggerSequenceState
        End If

        Me._TtmMeasureControlsToolStrip.Enabled = Me.IsDeviceOpen

        Me._TriggerToolStripDropDownButton.Enabled = Me.IsDeviceOpen AndAlso MeasurementSequenceState.Idle = measurementSequenceState
        If Me._TriggerToolStripDropDownButton.Enabled Then
            Me._WaitForTriggerToolStripMenuItem.Enabled = Me.IsDeviceOpen AndAlso
                                                         (MeasurementSequenceState.Idle = measurementSequenceState OrElse
                                                          TriggerSequenceState.WaitingForTrigger = triggerSequenceState)
            Me._AssertTriggerToolStripMenuItem.Enabled = TriggerSequenceState.WaitingForTrigger = triggerSequenceState
        End If

        Me._MeasureToolStripDropDownButton.Enabled = Me.IsDeviceOpen AndAlso TriggerSequenceState.Idle = triggerSequenceState
        If Me._TtmMeasureControlsToolStrip.Enabled Then
            Me._InitialResistanceToolStripMenuItem.Enabled = MeasurementSequenceState.Idle = measurementSequenceState
            Me._ThermalTransientToolStripMenuItem.Enabled = MeasurementSequenceState.Idle = measurementSequenceState
            Me._FinalResistanceToolStripMenuItem.Enabled = MeasurementSequenceState.Idle = measurementSequenceState
            Me._MeasureAllToolStripMenuItem.Enabled = MeasurementSequenceState.Idle = measurementSequenceState
            Me._AbortSequenceToolStripMenuItem.Enabled = MeasurementSequenceState.Idle <> measurementSequenceState
        End If

        Me._TraceToolStripDropDownButton.Enabled = Me.IsDeviceOpen AndAlso
                                             TriggerSequenceState.Idle = triggerSequenceState AndAlso
                                             MeasurementSequenceState.Idle = measurementSequenceState
        If Me._TraceToolStripDropDownButton.Enabled Then
            Me._ReadTraceToolStripMenuItem.Enabled = Me._IsTraceAvailable
            Dim enabled As Boolean = Me._LastTimeSeries IsNot Nothing AndAlso Me._LastTimeSeries.Any
            Me._SaveTraceToolStripMenuItem.Enabled = enabled
            Me._ClearTraceToolStripMenuItem.Enabled = enabled
            Me._ModelTraceToolStripMenuItem.Enabled = enabled
        End If
    End Sub

#End Region

#Region " TRACE TOOL STRIP "

#Region " TRACE LIST VIEW "

    ''' <summary> Displays a part described by grid. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="grid">   The grid. </param>
    ''' <param name="values"> The values. </param>
    Private Shared Sub DisplayTrace(ByVal grid As DataGridView, ByVal values As IList(Of System.Windows.Point))

        If grid Is Nothing Then Throw New ArgumentNullException(NameOf(grid))

        grid.Enabled = False
        grid.Columns.Clear()
        grid.DataSource = New List(Of System.Drawing.PointF)
        grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSteelBlue
        grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
        grid.RowHeadersVisible = False
        grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
        If values IsNot Nothing Then
            grid.DataSource = values
        End If
        grid.Refresh()
        If grid.Columns IsNot Nothing AndAlso grid.Columns.Count > 0 Then
            Dim column As DataGridViewColumn
            For Each column In grid.Columns
                column.Visible = False
            Next
            Dim displayIndex As Integer = 0
            column = grid.Columns("X")
            column.Visible = True
            column.HeaderText = " Time, ms "
            column.DisplayIndex = displayIndex
            displayIndex += 1

            column = grid.Columns("Y")
            column.Visible = True
            column.HeaderText = " Voltage, mV "
            column.DisplayIndex = displayIndex

            grid.Enabled = True
        End If
    End Sub

#End Region

    ''' <summary> Gets or sets the last time series. </summary>
    ''' <value> The last time series. </value>
    Private ReadOnly Property LastTimeSeries As IList(Of System.Windows.Point)

    ''' <summary> Displays the thermal transient trace. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="chart"> The chart. </param>
    ''' <returns> The trace values. </returns>
    Protected Overridable Function DisplayThermalTransientTrace(ByVal chart As DataVisualization.Charting.Chart) As IList(Of System.Windows.Point)
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        Return New List(Of System.Windows.Point)(New System.Windows.Point() {})
#Enable Warning CA1825 ' Avoid zero-length array allocations.
    End Function

    ''' <summary> Event handler. Called by _ReadTraceToolStripMenuItem for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadTraceToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles _ReadTraceToolStripMenuItem.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            activity = $"reading and displaying the trace"
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
            Me.InfoProvider.SetIconPadding(Me._TtmMeasureControlsToolStrip, -15)
            ' MeterThermalTransient.EmulateThermalTransientTrace(Me._Chart)
            Me._LastTimeSeries = Me.DisplayThermalTransientTrace(Me._Chart)
            ' list the trace values
            MeasurementViewBase.DisplayTrace(Me._TraceDataGridView, Me._LastTimeSeries)
            Dim enabled As Boolean = Me._LastTimeSeries IsNot Nothing AndAlso Me._LastTimeSeries.Any
            Me._SaveTraceToolStripMenuItem.Enabled = enabled
            Me._ClearTraceToolStripMenuItem.Enabled = enabled
            Me._ModelTraceToolStripMenuItem.Enabled = enabled

        Catch ex As Exception
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Failed reading and displaying the thermal transient trace.")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the trace list. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearTraceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearTraceToolStripMenuItem.Click
        Me.PublishVerbose("User action @{0};. ",
                                   System.Reflection.MethodInfo.GetCurrentMethod.Name)
        MeasurementViewBase.DisplayTrace(Me._TraceDataGridView, Nothing)
    End Sub

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks>
    ''' Uses a temporary random file name to test if the file can be created. The file is deleted
    ''' thereafter.
    ''' </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = String.Empty
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Sets the default file path in case the data folder does not exist. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="filePath"> The file path. </param>
    ''' <returns> System.String. </returns>
    Private Shared Function UpdateDefaultFilePath(ByVal filePath As String) As String

        ' check validity of data folder.
        Dim dataFolder As String = System.IO.Path.GetDirectoryName(filePath)

        If Not System.IO.Directory.Exists(dataFolder) Then

            dataFolder = My.Application.Info.DirectoryPath

            If MeasurementViewBase.IsFolderWritable(dataFolder) Then

                dataFolder = System.IO.Path.Combine(dataFolder, "..\Measurements")
                dataFolder = If(System.IO.Directory.Exists(dataFolder),
                    System.IO.Path.GetDirectoryName(dataFolder),
                    System.IO.Directory.CreateDirectory(dataFolder).FullName)

            Else

                ' in run time use the documents folder.
                dataFolder = My.Computer.FileSystem.SpecialDirectories.MyDocuments

                dataFolder = System.IO.Path.Combine(dataFolder, "TTM")
                If Not System.IO.Directory.Exists(dataFolder) Then
                    System.IO.Directory.CreateDirectory(dataFolder)
                End If

                dataFolder = System.IO.Path.Combine(dataFolder, "Measurements")
                If Not System.IO.Directory.Exists(dataFolder) Then
                    System.IO.Directory.CreateDirectory(dataFolder)
                End If

            End If

        End If

        Return System.IO.Path.Combine(dataFolder, System.IO.Path.GetFileName(filePath))

    End Function

    ''' <summary> Gets a new file for storing the data. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="filePath"> The file path. </param>
    ''' <returns> A file name or empty if error. </returns>
    Private Shared Function BrowseForFile(ByVal filePath As String) As String

        ' make sure the default data file name is valid.
        filePath = UpdateDefaultFilePath(filePath)

        Using dialog As New System.Windows.Forms.SaveFileDialog()
            dialog.CheckFileExists = False
            dialog.CheckPathExists = True
            dialog.DefaultExt = ".xls"
            dialog.FileName = filePath
            dialog.Filter = "Comma Separated Values (*.csv)|*.csv|All Files (*.*)|*.*"
            dialog.FilterIndex = 0
            dialog.InitialDirectory = System.IO.Path.GetDirectoryName(filePath)
            dialog.RestoreDirectory = True
            dialog.Title = "Select a Comma-Separated File"

            ' Open the Open dialog
            If dialog.ShowDialog = Windows.Forms.DialogResult.OK Then

                ' if file selected,
                Return dialog.FileName

            Else

                ' if some error, just ignore
                Return String.Empty

            End If
        End Using

    End Function

    ''' <summary> Saves the trace to file. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SaveTraceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveTraceToolStripMenuItem.Click

        Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
        Me.PublishVerbose("User action @{0};. ", System.Reflection.MethodInfo.GetCurrentMethod.Name)

        If Me._LastTimeSeries IsNot Nothing AndAlso Me._LastTimeSeries.Any Then

            Me.PublishVerbose("Browsing for file;. ")
            Dim filePath As String = MeasurementViewBase.BrowseForFile(isr.VI.Ttm.My.MySettings.Default.TraceFilePath)
            If String.IsNullOrWhiteSpace(filePath) Then
                Return
            End If
            isr.VI.Ttm.My.MySettings.Default.TraceFilePath = filePath

            Dim activity As String = String.Empty
            Try
                activity = $"Saving trace;. to '{filePath}' "
                Me.PublishVerbose(activity)

                Using writer As New System.IO.StreamWriter(filePath, False)
                    Dim record As String = String.Empty
                    For Each point As System.Windows.Point In Me._LastTimeSeries
                        If String.IsNullOrWhiteSpace(record) Then
                            record = "Trace time Series"
                            writer.WriteLine(record)
                            record = "Time,Voltage"
                            writer.WriteLine(record)
                            record = "ms,mV"
                            writer.WriteLine(record)
                        End If
                        record = String.Format(Globalization.CultureInfo.CurrentCulture, "{0},{1}", point.X, point.Y)
                        writer.WriteLine(record)
                    Next
                    writer.Flush()
                End Using
                Me.PublishVerbose("Done saving;. ")

            Catch ex As Exception
                Me.PublishException(activity, ex)
                Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Exception occurred saving trace")
            Finally
            End Try
        End If


    End Sub

    ''' <summary> Creates a model of the thermal transient trace. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="chart"> The chart. </param>
    Protected Overridable Sub ModelThermalTransientTrace(ByVal chart As DataVisualization.Charting.Chart)
    End Sub

    ''' <summary> Event handler. Called by _ModelTraceToolStripMenuItem for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ModelTraceToolStripMenuItem_Click(sender As Object, e As System.EventArgs) Handles _ModelTraceToolStripMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = $"modeling the trace"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
            Me.InfoProvider.SetIconPadding(Me._TtmMeasureControlsToolStrip, -15)
            ' MeterThermalTransient.EmulateThermalTransientTrace(Me._Chart)
            Me.ModelThermalTransientTrace(Me._Chart)
        Catch ex As Exception
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Failed modeling the thermal transient trace.")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TTM MEASURE TOOL STRIP "

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overridable Sub ClearPartMeasurements()
    End Sub

    ''' <summary> Event handler. Called by _ClearToolStripMenuItem for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles _ClearToolStripMenuItem.Click
        Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
        Me.ClearPartMeasurements()
    End Sub

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The MeasurementOutcomes. </returns>
    Protected Overridable Function MeasureInitialResistance() As MeasurementOutcomes
        Return MeasurementOutcomes.None
    End Function

    ''' <summary> Measures initial resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitialResistanceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InitialResistanceToolStripMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = $"Measuring Initial Resistance"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
            Me.InfoProvider.SetIconPadding(Me._TtmMeasureControlsToolStrip, -15)
            Me.MeasureInitialResistance()
            Dim outcome As MeasurementOutcomes = Me.MeasureInitialResistance
            If MeasurementOutcomes.None = outcome OrElse outcome = MeasurementOutcomes.PartPassed Then
                Me.PublishVerbose("Initial Resistance measured;. ")
            Else
                If (outcome And MeasurementOutcomes.FailedContactCheck) <> 0 Then
                    Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Failed contact check.")
                    Me.PublishWarning("Contact check failed;. ")
                Else
                    Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Failed initial resistance.")
                    Me.PublishWarning("Initial Resistance failed with outcome = {0};. ", CInt(outcome))
                End If
            End If
        Catch ex As Exception
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Failed Measuring Initial Resistance")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The MeasurementOutcomes. </returns>
    Protected Overridable Function MeasureThermalTransient() As MeasurementOutcomes
        Return MeasurementOutcomes.None
    End Function

    ''' <summary> Measures the thermal transient voltage. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ThermalTransientToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ThermalTransientToolStripMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = $"Measuring thermal Resistance"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
            Me.InfoProvider.SetIconPadding(Me._TtmMeasureControlsToolStrip, -15)
            Dim outcome As MeasurementOutcomes = Me.MeasureThermalTransient
            If MeasurementOutcomes.None = outcome OrElse outcome = MeasurementOutcomes.PartPassed Then
                Me._IsTraceAvailable = True
                Me.PublishVerbose("Thermal Transient measured;. ")
            Else
                Me._IsTraceAvailable = False
                Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Thermal transient failed.")
                Me.PublishWarning("Thermal transient failed with outcome = {0};. ", CInt(outcome))
            End If
            Me.OnStateChanged()
        Catch ex As Exception
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Failed Measuring Thermal Transient")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The MeasurementOutcomes. </returns>
    Protected Overridable Function MeasureFinalResistance() As MeasurementOutcomes
        Return MeasurementOutcomes.None
    End Function

    ''' <summary> Measures the final resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FinalResistanceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FinalResistanceToolStripMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = $"Measuring final Resistance"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
            Me.InfoProvider.SetIconPadding(Me._TtmMeasureControlsToolStrip, -15)
            Me.PublishVerbose("Measuring Final Resistance...;. ")
            Dim outcome As MeasurementOutcomes = Me.MeasureFinalResistance
            If MeasurementOutcomes.None = outcome OrElse outcome = MeasurementOutcomes.PartPassed Then
                Me.PublishVerbose("Final Resistance measured;. ")
            Else
                Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Failed final resistance.")
                Me.PublishWarning("Final Resistance failed with outcome = {0};. ", CInt(outcome))
            End If
        Catch ex As Exception
            Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "Failed Measuring Final Resistance")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SEQUENCED MEASUREMENTS "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _MeasureSequencer As MeasureSequencer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the sequencer. </summary>
    ''' <value> The sequencer. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MeasureSequencer As MeasureSequencer
        Get
            Return Me._MeasureSequencer
        End Get
        Set(value As MeasureSequencer)
            Me._MeasureSequencer = value
        End Set
    End Property

    ''' <summary> Handles the measure sequencer property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As MeasureSequencer, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.MeasureSequencer.MeasurementSequenceState)
                Me.OnMeasurementSequenceStateChanged(sender.MeasurementSequenceState)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _MeasureSequencer for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSequencer_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs) Handles _MeasureSequencer.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"handler measure sequencer {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.MeasureSequencer_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, MeasureSequencer), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Updates the progress bar. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="state"> The state. </param>
    Private Sub UpdateProgressbar(ByVal state As MeasurementSequenceState)

        ' unhide the progress bar.
        Me._TtmToolStripProgressBar.Visible = True

        If state = MeasurementSequenceState.Failed OrElse
            state = MeasurementSequenceState.Starting OrElse
            state = MeasurementSequenceState.Idle Then
            Me._TtmToolStripProgressBar.Value = Me._TtmToolStripProgressBar.Minimum
            Me._TtmToolStripProgressBar.ToolTipText = "Failed"
        ElseIf state = MeasurementSequenceState.Completed Then
            Me._TtmToolStripProgressBar.Value = Me._TtmToolStripProgressBar.Maximum
            Me._TtmToolStripProgressBar.ToolTipText = "Completed"
        Else
            Me._TtmToolStripProgressBar.Value = Me.MeasureSequencer.PercentProgress
        End If

    End Sub

    ''' <summary> Handles the change in measurement state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="state"> The state. </param>
    Private Sub OnMeasurementSequenceStateChanged(ByVal state As MeasurementSequenceState)
        Me.PublishInfo("Processing the {0} state;. ", state.Description)
        Me.UpdateProgressbar(state)
        Select Case state
            Case MeasurementSequenceState.Aborted
            Case MeasurementSequenceState.Completed
            Case MeasurementSequenceState.Failed
            Case MeasurementSequenceState.MeasureInitialResistance
            Case MeasurementSequenceState.MeasureThermalTransient
            Case MeasurementSequenceState.Idle
                Me.OnStateChanged()
            Case MeasurementSequenceState.None
            Case MeasurementSequenceState.PostTransientPause
                Me._IsTraceAvailable = True
            Case MeasurementSequenceState.MeasureFinalResistance
            Case MeasurementSequenceState.Starting
                Me._IsTraceAvailable = False
                Me.OnStateChanged()
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "Unhandled state: " & state.ToString)
        End Select
    End Sub

    ''' <summary> Aborts the measurement sequence. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AbortSequenceToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AbortSequenceToolStripMenuItem.Click
        Me.PublishVerbose("User action @{0};. ", System.Reflection.MethodInfo.GetCurrentMethod.Name)
        Me.PublishVerbose("Abort requested;. ")
        Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Abort)
    End Sub

    ''' <summary> Starts measurement sequence. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MeasureAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MeasureAllToolStripMenuItem.Click
        Me.PublishVerbose("User action @{0};. ", System.Reflection.MethodInfo.GetCurrentMethod.Name)
        Me.PublishVerbose("Start requested;. ")
        Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
        Me.InfoProvider.SetIconPadding(Me._TtmMeasureControlsToolStrip, -15)
        Me.MeasureSequencer.StartMeasurementSequence()
    End Sub

#End Region

#Region " TRIGGERED MEASUREMENTS "

    ''' <summary> Abort triggered measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overridable Sub AbortTriggerSequenceIf()
    End Sub

    ''' <summary> Event handler. Called by _AbortToolStripMenuItem for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AbortToolStripMenuItem_Click(sender As Object, e As System.EventArgs) Handles _AbortToolStripMenuItem.Click
        Me.InfoProvider.SetError(Me._TtmMeasureControlsToolStrip, "")
        Me.AbortTriggerSequenceIf()
    End Sub

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _TriggerSequencer As TriggerSequencer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the trigger sequencer. </summary>
    ''' <value> The sequencer. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property TriggerSequencer As TriggerSequencer
        Get
            Return Me._TriggerSequencer
        End Get
        Set(value As TriggerSequencer)
            Me._TriggerSequencer = value
        End Set
    End Property

    ''' <summary> Handles the trigger sequencer property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As TriggerSequencer, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.TriggerSequencer.TriggerSequenceState)
                Me.OnTriggerSequenceStateChanged(sender.TriggerSequenceState)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _TriggerSequencer for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSequencer_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _TriggerSequencer.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"handler trigger sequencer {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TriggerSequencer_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, TriggerSequencer), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> The last status bar. </summary>
    Private ReadOnly _LastStatusBar As String

    ''' <summary> Handles the change in measurement state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="state"> The state. </param>
    Private Sub OnTriggerSequenceStateChanged(ByVal state As TriggerSequenceState)

        Me._TriggerActionToolStripLabel.Text = Me.TriggerSequencer.ProgressMessage(Me._LastStatusBar)
        Select Case state
            Case TriggerSequenceState.Aborted
                Me.OnStateChanged()
            Case TriggerSequenceState.Stopped
            Case TriggerSequenceState.Failed
            Case TriggerSequenceState.WaitingForTrigger
            Case TriggerSequenceState.MeasurementCompleted
                Me._IsTraceAvailable = True
            Case TriggerSequenceState.ReadingValues
            Case TriggerSequenceState.Idle
                Me.OnStateChanged()
                Me._WaitForTriggerToolStripMenuItem.Image = Global.isr.VI.Ttm.Forms.My.Resources.Resources.ViewRefresh7
            Case TriggerSequenceState.None
            Case TriggerSequenceState.Starting
                Me._IsTraceAvailable = False
                Me.OnStateChanged()
                Me._AssertTriggerToolStripMenuItem.Enabled = True
                Me._WaitForTriggerToolStripMenuItem.Image = Global.isr.VI.Ttm.Forms.My.Resources.Resources.MediaPlaybackStop2
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "Unhandled state: " & state.ToString)
        End Select
    End Sub

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AssertTrigger()
        Me.PublishVerbose("Assert requested;. ")
        Me.TriggerSequencer.AssertTrigger()
    End Sub

    ''' <summary> Asserts a trigger to emulate triggering for timing measurements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AssertTriggerToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AssertTriggerToolStripMenuItem.Click
        Me.AssertTrigger()
    End Sub

    ''' <summary>
    ''' Event handler. Called by _WaitForTriggerToolStripMenuItem for click events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub WaitForTriggerToolStripMenuItem_CheckChanged(sender As Object, e As System.EventArgs) Handles _WaitForTriggerToolStripMenuItem.CheckedChanged
        If Me._WaitForTriggerToolStripMenuItem.Enabled Then
            If Me._TriggerSequencer.TriggerSequenceState = TriggerSequenceState.Idle Then
                Me.PublishVerbose("Starting triggered measurements;. ")
                Me._TriggerSequencer.StartMeasurementSequence()
            ElseIf Me._TriggerSequencer.TriggerSequenceState = TriggerSequenceState.WaitingForTrigger Then
                Me.PublishVerbose("Stopping triggered measurements;. ")
                Me._TriggerSequencer.Enqueue(TriggerSequenceSignal.Stop)
            End If
        End If
    End Sub

#End Region

#Region " TRACE LEVEL "

    ''' <summary> The master device. </summary>
    Private _MasterDevice As VI.Tsp.K2600.K2600Device

    ''' <summary> Gets or sets the master device. </summary>
    ''' <value> The master device. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MasterDevice As VI.Tsp.K2600.K2600Device
        Get
            Return Me._MasterDevice
        End Get
        Set(value As VI.Tsp.K2600.K2600Device)
            Me._MasterDevice = value
            If value Is Nothing Then
                Me.DisableTraceLevelControls()
            Else
                Me.EnableTraceLevelControls()
            End If
        End Set
    End Property

    ''' <summary> Disables the trace level controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub DisableTraceLevelControls()
        RemoveHandler Me._LogTraceLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.LogTraceLevelComboBox_SelectedIndexChanged
        RemoveHandler Me._DisplayTraceLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.DisplayTraceLevelComboBox_SelectedIndexChanged
    End Sub

    ''' <summary> Enables the trace level controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub EnableTraceLevelControls()

        ModelViewTalkerBase.ListTraceEventLevels(Me._LogTraceLevelComboBox.ComboBox)
        AddHandler Me._LogTraceLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.LogTraceLevelComboBox_SelectedIndexChanged

        ModelViewTalkerBase.ListTraceEventLevels(Me._DisplayTraceLevelComboBox.ComboBox)
        AddHandler Me._DisplayTraceLevelComboBox.ComboBox.SelectedValueChanged, AddressOf Me.DisplayTraceLevelComboBox_SelectedIndexChanged

        ModelViewTalkerBase.SelectItem(Me._LogTraceLevelComboBox, isr.VI.Ttm.My.MySettings.Default.TraceLogLevel)
        ModelViewTalkerBase.SelectItem(Me._DisplayTraceLevelComboBox, isr.VI.Ttm.My.MySettings.Default.TraceShowLevel)

    End Sub

    ''' <summary>
    ''' Event handler. Called by _LogTraceLevelComboBox for selected value changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LogTraceLevelComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _LogTraceLevelComboBox.SelectedIndexChanged
        Dim activity As String = String.Empty
        Try
            activity = $"selecting log trace level on this instrument only"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.MasterDevice.ApplyTalkerTraceLevel(Core.ListenerType.Logger,
                                            ModelViewTalkerBase.SelectedValue(Me._LogTraceLevelComboBox, isr.VI.Ttm.My.MySettings.Default.TraceLogLevel))
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by _DisplayTraceLevelComboBox for selected value changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DisplayTraceLevelComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _DisplayTraceLevelComboBox.SelectedIndexChanged
        Dim activity As String = String.Empty
        Try
            activity = $"selecting display trace level on this instrument only"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.MasterDevice.ApplyTalkerTraceLevel(ListenerType.Display,
                                            ModelViewTalkerBase.SelectedValue(Me._DisplayTraceLevelComboBox, isr.VI.Ttm.My.MySettings.Default.TraceShowLevel))
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
