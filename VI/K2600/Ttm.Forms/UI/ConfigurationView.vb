﻿Imports System.ComponentModel

''' <summary> Panel for editing the TTM configuration. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-02-25 </para>
''' </remarks>
Public Class ConfigurationView
    Inherits ConfigurationViewBase

#Region " CONFIGURE "

    ''' <summary> Releases the resources. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ReleaseResources()
        MyBase.ReleaseResources()
        Me._Meter = Nothing
    End Sub

        Private _Meter As Meter

    ''' <summary> Gets or sets reference to the thermal transient meter device. </summary>
    ''' <value> The meter. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Meter() As Meter
        Get
            Return Me._Meter
        End Get
        Set(ByVal value As Meter)
            Me._Meter = value
            Me.DeviceUnderTest = If(value Is Nothing, Nothing, Me.Meter.ConfigInfo)
        End Set
    End Property

    ''' <summary> Configure changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="part"> The part. </param>
    Protected Overrides Sub ConfigureChanged(part As DeviceUnderTest)
        If Me.Meter IsNot Nothing Then
            Me.Meter.ConfigureChanged(part)
        End If
    End Sub

    ''' <summary> Configures the given part. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="part"> The part. </param>
    Protected Overrides Sub Configure(part As DeviceUnderTest)
        If Me.Meter IsNot Nothing Then
            Me.Meter.Configure(Me.Part)
        End If
    End Sub

#End Region

End Class
