Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions

''' <summary> Measurements header. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-02-25 </para>
''' </remarks>
Public Class MeasurementsHeader
    Inherits ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._OutcomeTextBox.Text = String.Empty

    End Sub

    ''' <summary> Releases the resources. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ReleaseResources()
        Me._InitialResistance = Nothing
        Me._FinalResistance = Nothing
        Me._ThermalTransient = Nothing
        Me._DeviceUnderTest = Nothing
    End Sub

#End Region

#Region " DUT "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DeviceUnderTest As DeviceUnderTest
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the device under test. </summary>
    ''' <value> The device under test. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property DeviceUnderTest As DeviceUnderTest
        Get
            Return Me._DeviceUnderTest
        End Get
        Set(value As DeviceUnderTest)
            Me._DeviceUnderTest = value
            If value Is Nothing Then
                Me._InitialResistance = Nothing
                Me._FinalResistance = Nothing
                Me._ThermalTransient = Nothing
            Else
                Me._InitialResistance = value.InitialResistance
                Me._FinalResistance = value.FinalResistance
                Me._ThermalTransient = value.ThermalTransient
            End If
        End Set
    End Property

    ''' <summary> Handles the device under test property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As DeviceUnderTest, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.DeviceUnderTest.Outcome)
                Me.OutcomeSetter(sender.Outcome)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _DeviceUnderTest for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DeviceUnderTest_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _DeviceUnderTest.PropertyChanged
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.DeviceUnderTest_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, DeviceUnderTest), e?.PropertyName)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)

        End Try
    End Sub

    ''' <summary> Message describing the measurement. </summary>
    Private _MeasurementMessage As String

    ''' <summary> Gets or sets a message describing the measurement. </summary>
    ''' <value> A message describing the measurement. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MeasurementMessage() As String
        Get
            Return Me._MeasurementMessage
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then
                value = String.Empty
            ElseIf value <> Me.MeasurementMessage Then
                Me._OutcomeTextBox.Text = value
            End If
            Me._MeasurementMessage = value
        End Set
    End Property

    ''' <summary> Gets or sets the test <see cref="MeasurementOutcomes">outcome</see>. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub OutcomeSetter(ByVal value As MeasurementOutcomes)

        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of MeasurementOutcomes)(AddressOf Me.OutcomeSetter), New Object() {value})
            Return
        End If
        If value = MeasurementOutcomes.None Then

            Me.MeasurementMessage = String.Empty
            Me._OutcomePictureBox.Visible = False

        ElseIf (value And MeasurementOutcomes.MeasurementFailed) = 0 Then

            Me.MeasurementMessage = "OKAY"

        Else

            If (value And MeasurementOutcomes.FailedContactCheck) <> 0 Then
                Me.MeasurementMessage = "CONTACTS"
            ElseIf (value And MeasurementOutcomes.HitCompliance) <> 0 Then
                Me.MeasurementMessage = "COMPLIANCE"
            ElseIf (value And MeasurementOutcomes.UnexpectedReadingFormat) <> 0 Then
                Me.MeasurementMessage = "READING ?"
            ElseIf (value And MeasurementOutcomes.UnexpectedOutcomeFormat) <> 0 Then
                Me.MeasurementMessage = "OUTCOME ?"
            ElseIf (value And MeasurementOutcomes.UnspecifiedStatusException) <> 0 Then
                Me.MeasurementMessage = "DEVICE"
            ElseIf (value And MeasurementOutcomes.UnspecifiedProgramFailure) <> 0 Then
                Me.MeasurementMessage = "PROGRAM"
            ElseIf (value And MeasurementOutcomes.MeasurementNotMade) <> 0 Then
                Me.MeasurementMessage = "PARTIAL"
            Else
                Me.MeasurementMessage = "FAILED"
                If (value And MeasurementOutcomes.UnknownOutcome) <> 0 Then
                    Debug.Assert(Not Debugger.IsAttached, "Unknown outcome")
                End If
            End If

        End If

        If (value And MeasurementOutcomes.PartPassed) <> 0 Then

            Me._OutcomePictureBox.Visible = True
            Me._OutcomePictureBox.Image = Global.isr.VI.Ttm.Forms.My.Resources.Resources.Good

        ElseIf (value And MeasurementOutcomes.PartFailed) <> 0 Then

            Me._OutcomePictureBox.Visible = True
            Me._OutcomePictureBox.Image = Global.isr.VI.Ttm.Forms.My.Resources.Resources.Bad

        End If

    End Sub

#End Region

#Region " DISPLAY ALERT "

    ''' <summary> Shows the alerts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="show">   true to show, false to hide. </param>
    ''' <param name="isGood"> true if this object is good. </param>
    Public Sub ShowAlerts(ByVal show As Boolean, ByVal isGood As Boolean)
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Boolean, Boolean)(AddressOf Me.ShowAlerts), New Object() {show, isGood})
        Else
            Me._AlertsPictureBox.Image = If(show, If(isGood, My.Resources.Good, My.Resources.Bad), Nothing)
        End If
    End Sub

    ''' <summary> Shows the outcome. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="show">   true to show, false to hide. </param>
    ''' <param name="isGood"> true if this object is good. </param>
    Public Sub ShowOutcome(ByVal show As Boolean, ByVal isGood As Boolean)
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Boolean, Boolean)(AddressOf Me.ShowOutcome), New Object() {show, isGood})
        Else
            Me._OutcomePictureBox.Image = If(show, If(isGood, My.Resources.Good, My.Resources.Bad), Nothing)
        End If
    End Sub

#End Region

#Region " DISPLAY VALUE "

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub Clear()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.Clear))
        Else
            Me.InfoProvider.Clear()
            Me.ShowOutcome(False, False)
            Me.ShowAlerts(False, False)
            Me._InitialResistanceTextBox.Text = String.Empty
            Me._FinalResistanceTextBox.Text = String.Empty
            Me._ThermalTransientVoltageTextBox.Text = String.Empty
        End If
    End Sub

    ''' <summary> Displays the thermal transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="textBox">    The text box control. </param>
    ''' <param name="resistance"> The resistance. </param>
    Private Sub SetErrorProvider(ByVal textBox As TextBox, ByVal resistance As ResistanceMeasureBase)
        If (resistance.Outcome And MeasurementOutcomes.PartFailed) <> 0 Then
            Me.InfoProvider.SetIconPadding(textBox, -Me.InfoProvider.Icon.Width)
            Me.InfoProvider.SetError(textBox, "Value out of range")
        ElseIf (resistance.Outcome And MeasurementOutcomes.MeasurementFailed) <> 0 Then
            Me.InfoProvider.SetIconPadding(textBox, -Me.InfoProvider.Icon.Width)
            Me.InfoProvider.SetError(textBox, "Measurement failed")
        ElseIf (resistance.Outcome And MeasurementOutcomes.MeasurementNotMade) <> 0 Then
            Me.InfoProvider.SetIconPadding(textBox, -Me.InfoProvider.Icon.Width)
            Me.InfoProvider.SetError(textBox, "Measurement not made")
        Else
            Me.InfoProvider.SetError(textBox, "")
        End If
    End Sub

    ''' <summary> Displays the resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="textBox"> The text box control. </param>
    Private Sub ClearResistance(ByVal textBox As TextBox)
        If textBox IsNot Nothing Then
            If textBox.InvokeRequired Then
                textBox.Invoke(New Action(Of TextBox)(AddressOf Me.ClearResistance), textBox)
            Else
                textBox.Text = String.Empty
                Me.InfoProvider.SetError(textBox, "")
            End If
        End If
    End Sub

    ''' <summary> Displays the resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="textBox">    The text box control. </param>
    ''' <param name="resistance"> The resistance. </param>
    Private Sub ShowResistance(ByVal textBox As TextBox, ByVal resistance As ResistanceMeasureBase)
        If textBox IsNot Nothing Then
            If textBox.InvokeRequired Then
                textBox.Invoke(New Action(Of TextBox, ResistanceMeasureBase)(AddressOf Me.ShowResistance), New Object() {textBox, resistance})
            Else
                textBox.Text = resistance.ResistanceCaption
                Me.SetErrorProvider(textBox, resistance)
            End If
        End If
    End Sub

    ''' <summary> Displays the thermal transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="textBox">    The text box control. </param>
    ''' <param name="resistance"> The resistance. </param>
    Private Sub ShowThermalTransient(ByVal textBox As TextBox, ByVal resistance As ResistanceMeasureBase)
        If textBox IsNot Nothing Then
            If textBox.InvokeRequired Then
                textBox.Invoke(New Action(Of TextBox, ResistanceMeasureBase)(AddressOf Me.ShowThermalTransient), New Object() {textBox, resistance})
            Else
                textBox.Text = resistance.VoltageCaption
                Me.SetErrorProvider(textBox, resistance)
            End If
        End If
    End Sub

#End Region

#Region " PART: INITIAL RESISTANCE "

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The Part Initial Resistance. </summary>
    Private WithEvents _InitialResistance As ColdResistance
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Executes the initial resistance property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnInitialResistancePropertyChanged(ByVal sender As ColdResistance, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.ColdResistance.MeasurementAvailable)
                If sender.MeasurementAvailable Then
                    Me.ShowResistance(Me._InitialResistanceTextBox, sender)
                End If
            Case NameOf(Ttm.ColdResistance.LastReading)
                If String.IsNullOrWhiteSpace(sender.LastReading) Then
                    Me.ClearResistance(Me._InitialResistanceTextBox)
                End If
        End Select
    End Sub

    ''' <summary> Event handler. Called by _InitialResistance for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitialResistance_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs) Handles _InitialResistance.PropertyChanged
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.InitialResistance_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnInitialResistancePropertyChanged(TryCast(sender, ColdResistance), e?.PropertyName)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " PART: FINAL RESISTANCE "

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The Part Final Resistance. </summary>
    Private WithEvents _FinalResistance As ColdResistance
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Executes the Final resistance property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnFinalResistancePropertyChanged(ByVal sender As ColdResistance, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.ColdResistance.MeasurementAvailable)
                If sender.MeasurementAvailable Then
                    Me.ShowResistance(Me._FinalResistanceTextBox, sender)
                End If
            Case NameOf(Ttm.ColdResistance.LastReading)
                If String.IsNullOrWhiteSpace(sender.LastReading) Then
                    Me.ClearResistance(Me._FinalResistanceTextBox)
                End If
        End Select
    End Sub

    ''' <summary> Event handler. Called by _FinalResistance for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FinalResistance_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs) Handles _FinalResistance.PropertyChanged
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.FinalResistance_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnFinalResistancePropertyChanged(TryCast(sender, ColdResistance), e?.PropertyName)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub


#End Region

#Region " PART: THERMAL TRANSIENT "

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The Part Thermal Transient. </summary>
    Private WithEvents _ThermalTransient As ThermalTransient
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Executes the initial resistance property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As ResistanceMeasureBase, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.ResistanceMeasureBase.MeasurementAvailable)
                If sender.MeasurementAvailable Then
                    Me.ShowThermalTransient(Me._ThermalTransientVoltageTextBox, sender)
                End If
            Case NameOf(Ttm.ResistanceMeasureBase.LastReading)
                If String.IsNullOrWhiteSpace(sender.LastReading) Then
                    Me.ClearResistance(Me._ThermalTransientVoltageTextBox)
                End If
        End Select
    End Sub

    ''' <summary> Event handler. Called by _ThermalTransient for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ThermalTransient_PropertyChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _ThermalTransient.PropertyChanged
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ThermalTransient_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, ResistanceMeasureBase), e?.PropertyName)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

End Class
