Imports System.ComponentModel

Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions

''' <summary> Thermal Transient Model header. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-02-25 </para>
''' </remarks>
Public Class ThermalTransientHeader
    Inherits ModelViewBase

#Region " DUT "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DeviceUnderTest As DeviceUnderTest
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the device under test. </summary>
    ''' <value> The device under test. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property DeviceUnderTest As DeviceUnderTest
        Get
            Return Me._DeviceUnderTest
        End Get
        Set(value As DeviceUnderTest)
            Me._DeviceUnderTest = value
            Me._ThermalTransient = value?.ThermalTransient
        End Set
    End Property

    ''' <summary> Releases the resources. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ReleaseResources()
        Me._ThermalTransient = Nothing
        Me._DeviceUnderTest = Nothing
    End Sub

    ''' <summary> Executes the device under test property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As DeviceUnderTest, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.DeviceUnderTest.Outcome)
                If sender.Outcome = MeasurementOutcomes.None Then
                    Me.Clear()
                End If
        End Select
    End Sub

    ''' <summary> Event handler. Called by _DeviceUnderTest for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DeviceUnderTest_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _DeviceUnderTest.PropertyChanged
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.DeviceUnderTest_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, DeviceUnderTest), e?.PropertyName)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)

        End Try
    End Sub

#End Region

#Region " DISPLAY VALUE "

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub Clear()
        Me.InfoProvider.Clear()
        Me._AsymptoteTextBox.Text = String.Empty
        Me._EstimatedVoltageTextBox.Text = String.Empty
        Me._IterationsCountTextBox.Text = String.Empty
        Me._CorrelationCoefficientTextBox.Text = String.Empty
        Me._StandardErrorTextBox.Text = String.Empty
        Me._TimeConstantTextBox.Text = String.Empty
        Me._OutcomeTextBox.Text = String.Empty
    End Sub

#End Region

#Region " PART: THERMAL TRANSIENT "

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The Part Thermal Transient. </summary>
    Private WithEvents _ThermalTransient As ThermalTransient
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Executes the device under test property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As ThermalTransient, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.ThermalTransient.TimeConstant)
                Me._TimeConstantTextBox.Text = sender.TimeConstantCaption
            Case NameOf(Ttm.ThermalTransient.Asymptote)
                Me._AsymptoteTextBox.Text = sender.AsymptoteCaption
            Case NameOf(Ttm.ThermalTransient.EstimatedVoltage)
                Me._EstimatedVoltageTextBox.Text = sender.EstimatedVoltageCaption
            Case NameOf(Ttm.ThermalTransient.CorrelationCoefficient)
                Me._CorrelationCoefficientTextBox.Text = sender.CorrelationCoefficientCaption
            Case NameOf(Ttm.ThermalTransient.StandardError)
                Me._StandardErrorTextBox.Text = sender.StandardErrorCaption
            Case NameOf(Ttm.ThermalTransient.Iterations)
                Me._IterationsCountTextBox.Text = sender.IterationsCaption
            Case NameOf(Ttm.ThermalTransient.OptimizationOutcome)
                Me._OutcomeTextBox.Text = sender.OptimizationOutcomeCaption
                Me.ToolTip.SetToolTip(Me._OutcomeTextBox, sender.OptimizationOutcomeDescription)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _ThermalTransient for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ThermalTransient_PropertyChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _ThermalTransient.PropertyChanged
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ThermalTransient_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, ThermalTransient), e?.PropertyName)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

End Class
