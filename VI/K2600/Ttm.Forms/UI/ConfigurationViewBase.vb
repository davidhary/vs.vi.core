Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Core.Forma
Imports isr.Core.NumericExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Panel for editing the configuration. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-02-25 </para>
''' </remarks>
Public Class ConfigurationViewBase
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me.ApplyConfigurationButtonCaption = Me._ApplyConfigurationButton.Text
        Me.ApplyNewConfigurationButtonCaption = Me._ApplyNewConfigurationButton.Text
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.ReleaseResources()
                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " ON CONTROL EVENTS "

    ''' <summary> Releases the resources. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overridable Sub ReleaseResources()
        Me._DeviceUnderTest = Nothing
        Me._Part = Nothing
    End Sub

#End Region

#Region " PART "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Part As DeviceUnderTest
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the part. </summary>
    ''' <value> The part. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Part As DeviceUnderTest
        Get
            Return Me._Part
        End Get
        Set(ByVal value As DeviceUnderTest)
            Me._Part = value
            If value IsNot Nothing Then
                Me.BindIntialResitanceControls()
                Me.BindThermalTransientControls()
            End If
        End Set
    End Property

    ''' <summary> Executes the property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As DeviceUnderTest, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.DeviceUnderTest.InitialResistance)
                Me.OnConfigurationChanged()
            Case NameOf(Ttm.DeviceUnderTest.FinalResistance)
                Me.OnConfigurationChanged()
            Case NameOf(Ttm.DeviceUnderTest.ShuntResistance)
            Case NameOf(Ttm.DeviceUnderTest.ThermalTransient)
                Me.OnConfigurationChanged()
        End Select
    End Sub

    ''' <summary> Event handler. Called by _InitialResistance for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Part_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs) Handles _Part.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"handling part {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.Part_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, DeviceUnderTest), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Bind controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub BindIntialResitanceControls()

        ' set the GUI based on the current defaults.
        Dim instrumentSettings As isr.VI.Ttm.My.MySettings = isr.VI.Ttm.My.MySettings.Default

        Me._CheckContactsCheckBox.DataBindings.Clear()
        Me._CheckContactsCheckBox.DataBindings.Add(New Binding("Checked", Me.Part, "ContactCheckEnabled", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._CheckContactsCheckBox.Checked = isr.VI.Ttm.My.MySettings.Default.ContactCheckEnabled
        Me.Part.ContactCheckEnabled = Me._CheckContactsCheckBox.Checked
        isr.Core.ApplianceBase.DoEvents()

        Me._ColdVoltageNumeric.Minimum = instrumentSettings.ColdResistanceVoltageMinimum
        Me._ColdVoltageNumeric.Maximum = instrumentSettings.ColdResistanceVoltageMaximum
        Me._ColdVoltageNumeric.DataBindings.Clear()
        Me._ColdVoltageNumeric.DataBindings.Add(New Binding("Value", Me.Part.InitialResistance, "VoltageLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ColdVoltageNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ColdResistanceVoltageLimit.Clip(Me._ColdVoltageNumeric.Minimum, Me._ColdVoltageNumeric.Maximum)
        Me.Part.InitialResistance.VoltageLimit = Me._ColdVoltageNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

        Me._ColdCurrentNumeric.Minimum = instrumentSettings.ColdResistanceCurrentMinimum
        Me._ColdCurrentNumeric.Maximum = instrumentSettings.ColdResistanceCurrentMaximum
        Me._ColdCurrentNumeric.DataBindings.Clear()
        Me._ColdCurrentNumeric.DataBindings.Add(New Binding("Value", Me.Part.InitialResistance, "CurrentLevel", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ColdCurrentNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ColdResistanceCurrentLevel.Clip(Me._ColdCurrentNumeric.Minimum, Me._ColdCurrentNumeric.Maximum)
        Me.Part.InitialResistance.CurrentLevel = Me._ColdCurrentNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

        Me._ColdResistanceHighLimitNumeric.Minimum = instrumentSettings.ColdResistanceMinimum
        Me._ColdResistanceHighLimitNumeric.Maximum = instrumentSettings.ColdResistanceMaximum
        Me._ColdResistanceHighLimitNumeric.DataBindings.Clear()
        Me._ColdResistanceHighLimitNumeric.DataBindings.Add(New Binding("Value", Me.Part.InitialResistance, "HighLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ColdResistanceHighLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ColdResistanceHighLimit.Clip(Me._ColdResistanceHighLimitNumeric.Minimum, Me._ColdResistanceHighLimitNumeric.Maximum)
        Me.Part.InitialResistance.HighLimit = Me._ColdResistanceHighLimitNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

        Me._ColdResistanceLowLimitNumeric.Minimum = instrumentSettings.ColdResistanceMinimum
        Me._ColdResistanceLowLimitNumeric.Maximum = instrumentSettings.ColdResistanceMaximum
        Me._ColdResistanceLowLimitNumeric.DataBindings.Clear()
        Me._ColdResistanceLowLimitNumeric.DataBindings.Add(New Binding("Value", Me.Part.InitialResistance, "LowLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ColdResistanceLowLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ColdResistanceLowLimit.Clip(Me._ColdResistanceLowLimitNumeric.Minimum, Me._ColdResistanceLowLimitNumeric.Maximum)
        Me.Part.InitialResistance.LowLimit = Me._ColdResistanceLowLimitNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

    End Sub

    ''' <summary> Bind controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub BindThermalTransientControls()

        ' set the GUI based on the current defaults.
        Dim instrumentSettings As isr.VI.Ttm.My.MySettings = isr.VI.Ttm.My.MySettings.Default

        Me._PostTransientDelayNumeric.Minimum = instrumentSettings.PostTransientDelayMinimum
        Me._PostTransientDelayNumeric.Maximum = instrumentSettings.PostTransientDelayMaximum
        Me._PostTransientDelayNumeric.DataBindings.Clear()
        Me._PostTransientDelayNumeric.DataBindings.Add(New Binding("Value", Me.Part.ThermalTransient, "PostTransientDelay", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._PostTransientDelayNumeric.Value = isr.VI.Ttm.My.MySettings.Default.PostTransientDelay.Clip(Me._PostTransientDelayNumeric.Minimum, Me._PostTransientDelayNumeric.Maximum)
        Me.Part.ThermalTransient.PostTransientDelay = Me._PostTransientDelayNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

        Me._ThermalVoltageNumeric.Minimum = instrumentSettings.ThermalTransientVoltageMinimum
        Me._ThermalVoltageNumeric.Maximum = instrumentSettings.ThermalTransientVoltageMaximum
        Me._ThermalVoltageNumeric.DataBindings.Clear()
        Me._ThermalVoltageNumeric.DataBindings.Add(New Binding("Value", Me.Part.ThermalTransient, "AllowedVoltageChange", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ThermalVoltageNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ThermalTransientVoltageChange.Clip(Me._ThermalVoltageNumeric.Minimum, Me._ThermalVoltageNumeric.Maximum)
        Me.Part.ThermalTransient.AllowedVoltageChange = Me._ThermalVoltageNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

        Me._ThermalCurrentNumeric.Minimum = instrumentSettings.ThermalTransientCurrentMinimum
        Me._ThermalCurrentNumeric.Maximum = instrumentSettings.ThermalTransientCurrentMaximum
        Me._ThermalCurrentNumeric.DataBindings.Clear()
        Me._ThermalCurrentNumeric.DataBindings.Add(New Binding("Value", Me.Part.ThermalTransient, "CurrentLevel", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ThermalCurrentNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ThermalTransientCurrentLevel.Clip(Me._ThermalCurrentNumeric.Minimum, Me._ThermalCurrentNumeric.Maximum)
        Me.Part.ThermalTransient.CurrentLevel = Me._ThermalCurrentNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

        Me._ThermalVoltageLimitNumeric.Minimum = instrumentSettings.ThermalTransientVoltageMinimum
        Me._ThermalVoltageLimitNumeric.Maximum = instrumentSettings.ThermalTransientVoltageMaximum
        Me._ThermalVoltageLimitNumeric.DataBindings.Clear()
        Me._ThermalVoltageLimitNumeric.DataBindings.Add(New Binding("Value", Me.Part.ThermalTransient, "VoltageLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ThermalVoltageLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ThermalTransientVoltageLimit.Clip(Me._ThermalVoltageLimitNumeric.Minimum, Me._ThermalVoltageLimitNumeric.Maximum)
        Me.Part.ThermalTransient.VoltageLimit = Me._ThermalVoltageLimitNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

        Me._ThermalVoltageHighLimitNumeric.Minimum = instrumentSettings.ThermalTransientVoltageChangeMinimum
        Me._ThermalVoltageHighLimitNumeric.Maximum = instrumentSettings.ThermalTransientVoltageChangeMaximum
        Me._ThermalVoltageHighLimitNumeric.DataBindings.Clear()
        Me._ThermalVoltageHighLimitNumeric.DataBindings.Add(New Binding("Value", Me.Part.ThermalTransient, "HighLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ThermalVoltageHighLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ThermalTransientHighLimit.Clip(Me._ThermalVoltageHighLimitNumeric.Minimum, Me._ThermalVoltageHighLimitNumeric.Maximum)
        Me.Part.ThermalTransient.HighLimit = Me._ThermalVoltageHighLimitNumeric.Value
        isr.Core.ApplianceBase.DoEvents()

        Me._ThermalVoltageLowLimitNumeric.Minimum = instrumentSettings.ThermalTransientVoltageChangeMinimum
        Me._ThermalVoltageLowLimitNumeric.Maximum = instrumentSettings.ThermalTransientVoltageChangeMaximum
        Me._ThermalVoltageLowLimitNumeric.DataBindings.Clear()
        Me._ThermalVoltageLowLimitNumeric.DataBindings.Add(New Binding("Value", Me.Part.ThermalTransient, "LowLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ThermalVoltageLowLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ThermalTransientLowLimit.Clip(Me._ThermalVoltageLowLimitNumeric.Minimum, Me._ThermalVoltageLowLimitNumeric.Maximum)
        Me.Part.ThermalTransient.LowLimit = Me._ThermalVoltageLowLimitNumeric.Value

    End Sub

#End Region

#Region " DEVICE UNDER TEST "

    ''' <summary> Gets or sets the device under test. </summary>
    ''' <value> The device under test. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property DeviceUnderTest As DeviceUnderTest

#End Region

#Region " CONFIGURE "

    ''' <summary> Restore defaults. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Shared Sub RestoreDefaults()
        isr.VI.Ttm.My.MySettings.Default.ContactCheckEnabled = isr.VI.Ttm.My.MySettings.Default.ContactCheckEnabledDefault
        isr.VI.Ttm.My.MySettings.Default.ContactCheckThreshold = CInt(isr.VI.Ttm.My.MySettings.Default.ContactCheckThresholdDefault)

        isr.VI.Ttm.My.MySettings.Default.ColdResistanceVoltageLimit = isr.VI.Ttm.My.MySettings.Default.ColdResistanceVoltageLimitDefault
        isr.VI.Ttm.My.MySettings.Default.ColdResistanceCurrentLevel = isr.VI.Ttm.My.MySettings.Default.ColdResistanceCurrentLevelDefault
        isr.VI.Ttm.My.MySettings.Default.ColdResistanceHighLimit = isr.VI.Ttm.My.MySettings.Default.ColdResistanceHighLimitDefault
        isr.VI.Ttm.My.MySettings.Default.ColdResistanceLowLimit = isr.VI.Ttm.My.MySettings.Default.ColdResistanceLowLimitDefault

        isr.VI.Ttm.My.MySettings.Default.PostTransientDelay = isr.VI.Ttm.My.MySettings.Default.PostTransientDelayDefault
        isr.VI.Ttm.My.MySettings.Default.ThermalTransientVoltageChange = isr.VI.Ttm.My.MySettings.Default.ThermalTransientVoltageChangeDefault
        isr.VI.Ttm.My.MySettings.Default.ThermalTransientCurrentLevel = isr.VI.Ttm.My.MySettings.Default.ThermalTransientCurrentLevelDefault
        isr.VI.Ttm.My.MySettings.Default.ThermalTransientHighLimit = isr.VI.Ttm.My.MySettings.Default.ThermalTransientHighLimitDefault
        isr.VI.Ttm.My.MySettings.Default.ThermalTransientLowLimit = isr.VI.Ttm.My.MySettings.Default.ThermalTransientLowLimitDefault
        isr.VI.Ttm.My.MySettings.Default.ThermalTransientVoltageLimit = isr.VI.Ttm.My.MySettings.Default.ThermalTransientVoltageLimitDefault
    End Sub

    ''' <summary> Copy part values to the settings store. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub CopySettings()
        If Me.Part IsNot Nothing Then
            isr.VI.Ttm.My.MySettings.Default.SourceMeasureUnit = Me.Part.InitialResistance.SourceMeasureUnit

            isr.VI.Ttm.My.MySettings.Default.ContactCheckEnabled = Me.Part.ContactCheckEnabled
            isr.VI.Ttm.My.MySettings.Default.ContactCheckThreshold = Me.Part.ContactCheckThreshold

            isr.VI.Ttm.My.MySettings.Default.ColdResistanceVoltageLimit = CDec(Me.Part.InitialResistance.VoltageLimit)
            isr.VI.Ttm.My.MySettings.Default.ColdResistanceCurrentLevel = CDec(Me.Part.InitialResistance.CurrentLevel)
            isr.VI.Ttm.My.MySettings.Default.ColdResistanceHighLimit = CDec(Me.Part.InitialResistance.HighLimit)
            isr.VI.Ttm.My.MySettings.Default.ColdResistanceLowLimit = CDec(Me.Part.InitialResistance.LowLimit)

            isr.VI.Ttm.My.MySettings.Default.PostTransientDelay = CDec(Me.Part.ThermalTransient.PostTransientDelay)
            isr.VI.Ttm.My.MySettings.Default.ThermalTransientVoltageChange = CDec(Me.Part.ThermalTransient.AllowedVoltageChange)
            isr.VI.Ttm.My.MySettings.Default.ThermalTransientCurrentLevel = CDec(Me.Part.ThermalTransient.CurrentLevel)
            isr.VI.Ttm.My.MySettings.Default.ThermalTransientHighLimit = CDec(Me.Part.ThermalTransient.HighLimit)
            isr.VI.Ttm.My.MySettings.Default.ThermalTransientLowLimit = CDec(Me.Part.ThermalTransient.LowLimit)
            isr.VI.Ttm.My.MySettings.Default.ThermalTransientVoltageLimit = CDec(Me.Part.ThermalTransient.VoltageLimit)
        End If
    End Sub

    ''' <summary> Gets or sets the apply new configuration button caption. </summary>
    ''' <value> The apply new configuration button caption. </value>
    Private Property ApplyNewConfigurationButtonCaption As String

    ''' <summary> Gets or sets the configuration button caption. </summary>
    ''' <value> The apply configuration button caption. </value>
    Private Property ApplyConfigurationButtonCaption As String

    ''' <summary> Checks if new configuration settings need to be applied. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns>
    ''' <c>True</c> if settings where updated so that meter settings needs to be updated.
    ''' </returns>
    Private Function IsNewConfigurationSettings() As Boolean
        Return Not Me.Part.ThermalTransientConfigurationEquals(Me.DeviceUnderTest)
    End Function

    ''' <summary> True if new configuration setting is available, false if not. </summary>
    Private _IsNewConfigurationSettingAvailable As Boolean

    ''' <summary> Gets or sets the is new configuration setting available. </summary>
    ''' <value> The is new configuration setting available. </value>
    Public Property IsNewConfigurationSettingAvailable As Boolean
        Get
            Return Me._IsNewConfigurationSettingAvailable
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.IsNewConfigurationSettingAvailable) Then
                Me._IsNewConfigurationSettingAvailable = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the configuration status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub OnConfigurationChanged()
        Dim caption As String = Me.ApplyConfigurationButtonCaption
        Dim changedCaption As String = Me.ApplyNewConfigurationButtonCaption
        Me.IsNewConfigurationSettingAvailable = Me.IsNewConfigurationSettings
        If Me.IsNewConfigurationSettingAvailable Then
            caption &= " !"
            changedCaption &= " !"
        End If
        If Not caption.Equals(Me._ApplyConfigurationButton.Text) Then
            Me._ApplyConfigurationButton.Text = caption
        End If
        If Not changedCaption.Equals(Me._ApplyNewConfigurationButton.Text) Then
            Me._ApplyNewConfigurationButton.Text = changedCaption
        End If
    End Sub

    ''' <summary> Configures changed values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="part"> The Part. </param>
    Protected Overridable Sub ConfigureChanged(ByVal part As DeviceUnderTest)
    End Sub

    ''' <summary>
    ''' Event handler. Called by _ApplyNewThermalTransientConfigurationButton for click events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyNewConfigurationButton_Click(sender As Object, e As System.EventArgs) Handles _ApplyNewConfigurationButton.Click

        If Me.Part Is Nothing OrElse Me.DeviceUnderTest Is Nothing Then
            Me.InfoProvider.SetError(Me._ApplyNewConfigurationButton, "Meter not connected")
            Me.PublishWarning("Meter not connected;. ")
            Return
        End If
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"Applying configuration" : Me.PublishVerbose($"{activity};. ")
            Me.Part.FinalResistance.CopyConfiguration(Me.Part.InitialResistance)
            If Not Me.DeviceUnderTest.InfoConfigurationEquals(Me.Part) Then
                ' clear execution state is not required - done before the measurement.
                ' Me.Meter.ClearExecutionState()
                activity = $"Configuring Thermal Transient" : Me.PublishVerbose($"{activity};. ")
                Me.ConfigureChanged(Me.Part)
            End If
        Catch ex As isr.Core.OperationFailedException
            Me.InfoProvider.SetError(Me._ApplyNewConfigurationButton, "failed configuring--most one configuration element did not succeed in setting the correct value. See messages for details.")
            Me.PublishException(activity, ex)
        Catch ex As ArgumentException
            Me.InfoProvider.SetError(Me._ApplyNewConfigurationButton, "failed configuring--most likely due to a validation issue. See messages for details.")
            Me.PublishException(activity, ex)
        Catch ex As Exception
            Me.InfoProvider.SetError(Me._ApplyNewConfigurationButton, "failed configuring thermal transient")
            Me.PublishException(activity, ex)
        Finally
            Me.OnConfigurationChanged()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Configures all values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="part"> The Part. </param>
    Protected Overridable Sub Configure(ByVal part As DeviceUnderTest)
    End Sub

    ''' <summary> Saves the configuration settings and sends them to the meter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyConfigurationButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ApplyConfigurationButton.Click

        If Me.Part Is Nothing OrElse Me.DeviceUnderTest Is Nothing Then
            Me.InfoProvider.SetError(Me._ApplyConfigurationButton, "Meter not connected")
            Me.PublishWarning("Meter not connected;. ")
            Return
        End If
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"Applying configuration" : Me.PublishVerbose($"{activity};. ")
            Me.Part.FinalResistance.CopyConfiguration(Me.Part.InitialResistance)
            ' clear execution state is not required - done before the measurement.
            ' Me.Meter.ClearExecutionState()
            Me.PublishVerbose("Configuring Thermal Transient;. ")
            Me.Configure(Me.Part)
        Catch ex As isr.Core.OperationFailedException
            Me.InfoProvider.SetError(Me._ApplyNewConfigurationButton, "failed configuring--most one configuration element did not succeed in setting the correct value. See messages for details.")
            Me.PublishException(activity, ex)
        Catch ex As ArgumentException
            Me.InfoProvider.SetError(Me._ApplyNewConfigurationButton, "failed configuring--most likely due to a validation issue. See messages for details.")
            Me.PublishException(activity, ex)
        Catch ex As Exception
            Me.InfoProvider.SetError(Me._ApplyNewConfigurationButton, "failed configuring thermal transient")
            Me.PublishException(activity, ex)
        Finally
            Me.OnConfigurationChanged()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Event handler. Called by _RestoreDefaultsButton for click events. Restores the defaults
    ''' settings.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RestoreDefaultsButton_Click(sender As System.Object, e As System.EventArgs) Handles _RestoreDefaultsButton.Click

        ' read the instrument default settings.
        ConfigurationViewBase.RestoreDefaults()

        ' reset part to know state based on the current defaults
        Me.Part.ResetKnownState()
        isr.Core.ApplianceBase.DoEvents()

        ' bind.
        Me.BindIntialResitanceControls()
        Me.BindThermalTransientControls()

    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

