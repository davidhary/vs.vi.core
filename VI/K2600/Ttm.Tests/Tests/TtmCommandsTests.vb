Imports isr.Core

''' <summary>
''' This is a test class for Measure and is intended to contain all Measure Unit Tests.
''' </summary>
''' <remarks> David, 2020-10-12. </remarks>
<TestClass(), TestCategory("k2600")>
Public Class TtmCommandsTests

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

#Region "Additional test attributes"

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        OnInitialize()
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub MyClassCleanup()
        _Logger = Nothing
    End Sub
#End Region

    ''' <summary> Gets or sets the logger. </summary>
    ''' <value> The logger. </value>
    Public Shared ReadOnly Property Logger As Logger = isr.Core.Logger.NewInstance(My.Application.Info.ProductName)

    ''' <summary> Executes the 'initialize' action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Shared Sub OnInitialize()

        _Logger = isr.Core.Logger.NewInstance(My.Application.Info.ProductName)
        Dim listener As Microsoft.VisualBasic.Logging.FileLogTraceListener
        listener = _Logger.ReplaceDefaultTraceListener(True)

        ' set the log for the application
        My.Application.Log.TraceSource.Listeners.Remove(CustomFileLogTraceListener.DefaultFileLogWriterName)
        My.Application.Log.TraceSource.Listeners.Add(listener)
        My.Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose
        _Logger.ApplyTraceLevel(TraceEventType.Verbose)
    End Sub

    ''' <summary> A test for Open session. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    <TestMethod()>
    Public Sub OpenSessionTest()
        Dim resourceName As String = "TCPIP0::192.168.1.134::inst0::INSTR"
        Dim target As VI.Pith.SessionBase = isr.VI.SessionFactory.Get.Factory.Session()
        target.OpenSession(resourceName)
        Assert.AreEqual(resourceName, target.OpenResourceName)
        Assert.AreEqual(True, target.IsDeviceOpen)
    End Sub

    ''' <summary> Opens a session. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> A VI.Pith.SessionBase. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Function OpenSession(ByVal resourceName As String) As VI.Pith.SessionBase
        Dim target As VI.Pith.SessionBase = isr.VI.SessionFactory.Get.Factory.Session()
        target.OpenSession(resourceName)
        Assert.AreEqual(resourceName, target.OpenResourceName)
        Assert.AreEqual(True, target.IsDeviceOpen)
        Return target
    End Function

    ''' <summary> A test for measure current. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="session"> The session. </param>
    ''' <returns> A String. </returns>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Function MeasureCurrent(ByVal session As VI.Pith.SessionBase) As String
        If session Is Nothing Then Throw New ArgumentNullException(NameOf(session))
        session.WriteLine("_G.status.reset()")
        session.ReadStatusByte()
        session.WriteLine("print(smua.measure.i())")
        Dim value As String = session.ReadLineTrimEnd
        Return value
    End Function

    ''' <summary> A test for measure current. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MeasureCurrentTest()
        Dim resourceName As String = "TCPIP0::192.168.1.134::inst0::INSTR"
        Using target As VI.Pith.SessionBase = Me.OpenSession(resourceName)
            Dim value As String = Me.MeasureCurrent(target)
            Assert.AreEqual(False, String.IsNullOrWhiteSpace(value))
            My.Application.Log.WriteEntry(value)
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests measure current many. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
	Public Sub MeasureCurrentManyTest()
		Dim resourceName As String = "TCPIP0::192.168.1.134::inst0::INSTR"
		Using target As VI.Pith.SessionBase = Me.OpenSession(resourceName)
			For i As Integer = 1 To 100
				Dim value As String = Me.MeasureCurrent(target)
				My.Application.Log.WriteEntry($"{i} {value}")
			Next
		End Using
	End Sub

    ''' <summary> Logs an iterator. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub LogIT(ByVal value As String)
        My.Application.Log.WriteEntry(value)
    End Sub

    ''' <summary> (Unit Test Method) tests start commands. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub StartCommandsTest()
        Dim resourceName As String = "TCPIP0::192.168.1.134::inst0::INSTR"
        Using session As VI.Pith.SessionBase = Me.OpenSession(resourceName)
            Dim command As String = String.Empty
            Dim value As String = String.Empty
            Dim i As Integer = 0

            command = "_G.status.request_enable=0" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.reset()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(localnode.linefreq)" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(_G.ttm.coldResistance.Defaults.smuI)" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%7.4f',_G.ttm.coldResistance.Defaults.aperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.level))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minCurrent))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxCurrent))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.limit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minVoltage))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxVoltage))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.lowLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.highLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minResistance))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxResistance))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.ir:currentSourceChannelSetter(_G.ttm.coldResistance.Defaults.smuI)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.ir:apertureSetter(_G.ttm.coldResistance.Defaults.aperture)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.ir:levelSetter(_G.ttm.coldResistance.Defaults.level)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.ir:limitSetter(_G.ttm.coldResistance.Defaults.limit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.ir:lowLimitSetter(_G.ttm.coldResistance.Defaults.lowLimit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.ir:highLimitSetter(_G.ttm.coldResistance.Defaults.highLimit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(_G.ttm.ir.smuI==_G.smua)" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%7.4f',_G.ttm.ir.aperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.ir.level))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.ir.highLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.ir.lowLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.ir.limit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(_G.ttm.coldResistance.Defaults.smuI)" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%7.4f',_G.ttm.coldResistance.Defaults.aperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.level))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minCurrent))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxCurrent))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.limit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minVoltage))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxVoltage))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.lowLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.highLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minResistance))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxResistance))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.fr:currentSourceChannelSetter(_G.ttm.coldResistance.Defaults.smuI)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.fr:apertureSetter(_G.ttm.coldResistance.Defaults.aperture)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.fr:levelSetter(_G.ttm.coldResistance.Defaults.level)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.fr:limitSetter(_G.ttm.coldResistance.Defaults.limit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.fr:lowLimitSetter(_G.ttm.coldResistance.Defaults.lowLimit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.fr:highLimitSetter(_G.ttm.coldResistance.Defaults.highLimit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(_G.ttm.fr.smuI==_G.smua)" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%7.4f',_G.ttm.fr.aperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.fr.level))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.fr.highLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.fr.lowLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.fr.limit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.estimator.Defaults.thermalCoefficient))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.est:thermalCoefficientSetter(_G.ttm.estimator.Defaults.thermalCoefficient)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.estimator.Defaults.thermalCoefficient))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.est:thermalCoefficientSetter(_G.ttm.estimator.Defaults.thermalCoefficient)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")

            command = "_G.print(string.format('%9.6f',_G.ttm.est.thermalCoefficient))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.est.thermalCoefficient))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(_G.ttm.trace.Defaults.smuI)" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%7.4f',_G.ttm.trace.Defaults.aperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.minAperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxAperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.level))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.minCurrent))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxCurrent))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.limit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.minVoltage))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxVoltage))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.highLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.lowLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxVoltageChange))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%d',_G.ttm.trace.Defaults.medianFilterSize))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.period))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.minPeriod))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxPeriod))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%d',_G.ttm.trace.Defaults.points))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%d',_G.ttm.trace.Defaults.minPoints))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%1f',_G.ttm.trace.Defaults.maxPoints))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:currentSourceChannelSetter(_G.ttm.trace.Defaults.smuI)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")


            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:apertureSetter(_G.ttm.trace.Defaults.aperture)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:levelSetter(_G.ttm.trace.Defaults.level)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:limitSetter(_G.ttm.trace.Defaults.limit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:lowLimitSetter(_G.ttm.trace.Defaults.lowLimit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:highLimitSetter(_G.ttm.trace.Defaults.highLimit)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:maxRateSetter()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:maxVoltageChangeSetter(_G.ttm.trace.Defaults.level)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:latencySetter(_G.ttm.trace.Defaults.latency)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:medianFilterSizeSetter(_G.ttm.trace.Defaults.medianFilterSize)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.ttm.tr:pointsSetter(_G.ttm.trace.Defaults.points)" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")

            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(_G.ttm.tr.smuI==_G.smua)" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%7.4f',_G.ttm.tr.aperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.level))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.highLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.lowLimit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.limit))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%d',_G.ttm.tr.medianFilterSize))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.3f',_G.ttm.postTransientDelayGetter()))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.period))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%d',_G.ttm.tr.points))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.maxVoltageChange))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.status.reset()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.errorqueue.clear() waitcomplete()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "_G.status.reset() _G.status.standard.enable=253 _G.status.request_enable=32 _G.opc()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*IDN? *WAI" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.errorqueue.clear() waitcomplete()"

            i = session.ReadStatusByte : Me.LogIT($"STB: {i}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "*IDN? *WAI" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.reset()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(_G.ttm.coldResistance.Defaults.smuI)" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.print(string.format('%7.4f',_G.ttm.coldResistance.Defaults.aperture))" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")

            i = session.ReadStatusByte : Me.LogIT($"STB: {i}")
            command = "_G.errorqueue.clear() waitcomplete()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            i = session.ReadStatusByte : Me.LogIT($"STB: {i}")
            command = "_G.status.reset()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*IDN? *WAI" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            session.CommunicationTimeout = TimeSpan.FromMilliseconds(30000)
            command = "_G.errorqueue.clear() waitcomplete()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            session.CommunicationTimeout = TimeSpan.FromMilliseconds(2000)
            i = session.ReadStatusByte : Me.LogIT($"STB: {i}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            command = "_G.status.reset()" : session.WriteLine(command) : value = String.Empty : Me.LogIT($"{command} {value}")
            command = "*OPC?" : session.WriteLine(command) : value = session.ReadLineTrimEnd : Me.LogIT($"{command} {value}")
            i = session.ReadStatusByte : Me.LogIT($"STB: {i}")
            command = "_G.print(display)" : session.WriteLine(command) : session.ReadLineTrimEnd() : Me.LogIT($"{command} {value}")
        End Using
    End Sub

End Class

