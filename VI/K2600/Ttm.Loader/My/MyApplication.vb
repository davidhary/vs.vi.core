Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = 2 ' TraceEventIds.IsrVITtmLoader

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "TTM Loader"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "TTM Loader"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "TTM.Loader"

    End Class

End Namespace


