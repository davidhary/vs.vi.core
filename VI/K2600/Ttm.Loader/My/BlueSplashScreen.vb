﻿''' <summary>
''' Inherits from the <see cref="isr.Core.Forma.BlueSplash"></see> to provide a splash screen for
''' the assembly.
''' </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2015-05-19, x.x.5617.x. </para>
''' </remarks>
Public Class MySplashScreen
    Inherits isr.Core.Forma.BlueSplash

#Region " SPLASH "

    ''' <summary> Gets the sentinel indicating if the splash is created. </summary>
    ''' <value> <c>True</c> if not nothing or disposed. </value>
    Public Shared ReadOnly Property IsCreated As Boolean
        Get
            Return MySplashScreen.Instance IsNot Nothing AndAlso Not MySplashScreen.Instance.IsDisposed
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the splash is created and visible. </summary>
    ''' <value> <c>True</c> if not nothing or disposed and visible. </value>
    Public Shared ReadOnly Property IsVisible As Boolean
        Get
            Return MySplashScreen.IsCreated AndAlso MySplashScreen.Instance.Visible
        End Get
    End Property

    ''' <summary> The shared instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property Instance() As MySplashScreen

    ''' <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary> Creates the instance based on the assembly splash form. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Shared Sub CreateInstance(ByVal value As Windows.Forms.Form)
        If value IsNot Nothing AndAlso Not value.IsDisposed Then
            SyncLock MySplashScreen.syncLocker
                MySplashScreen.Instance = CType(value, MySplashScreen)
                MySplashScreen.Instance.TopmostSetter(Not Debugger.IsAttached)
                ' MySplashScreen.instance.LicenseeName = "Integrated Scientific Resources, Inc."
            End SyncLock
        End If
    End Sub

    ''' <summary> Displays a message on the splash screen. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The message. </param>
    Public Shared Sub SplashMessage(ByVal value As String)
        If MySplashScreen.IsVisible Then MySplashScreen.Instance.DisplayMessage(value)
    End Sub

#End Region

End Class
