Imports isr.VI.ExceptionExtensions
Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Destroys objects for this project. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        Friend Sub Destroy()
            MySplashScreen.Close()
            MySplashScreen.Dispose()
            Me.SplashScreen = Nothing
        End Sub

        ''' <summary> Gets or sets the name of the computer. </summary>
        ''' <value> The name of the computer. </value>
        Public Shared Property ComputerName As String

        ''' <summary> Instantiates the application to its known state. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Function TryinitializeKnownState() As Boolean

            Try

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting

                ' show status
                If My.MyApplication.InDesignMode Then
                    Me.SplashTraceEvent(TraceEventType.Verbose, "Application is initializing. Design Mode.")
                Else
                    Me.SplashTraceEvent(TraceEventType.Verbose, "Application is initializing. Runtime Mode.")
                End If

                ' Apply command line results.
                If CommandLineInfo.DevicesEnabled.HasValue Then
                    Me.SplashTraceEvent(TraceEventType.Information, "{0} use of devices from command line",
                                        IIf(CommandLineInfo.DevicesEnabled.Value, "Enabled", "Disabling"))
                    My.MySettings.Default.DevicesEnabled = CommandLineInfo.DevicesEnabled.Value
                End If

                If String.IsNullOrWhiteSpace(My.MyApplication.ComputerName) Then
                    MyApplication.ComputerName = My.Computer.Name
                End If

                Return True

            Catch ex As Exception

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Me.SplashTraceEvent(TraceEventType.Error, "Exception occurred initializing application known state;. {0}", ex.ToFullBlownString)
                Try
                    Me.Destroy()
                Finally
                End Try
                Return False
            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Function

        ''' <summary>
        ''' Processes the startup. Sets the event arguments
        ''' <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs.Cancel">cancel</see>
        ''' value if failed.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        '''                  instance containing the event data. </param>
        Private Sub ProcessStartup(ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs)
            If e IsNot Nothing AndAlso Not e.Cancel Then
                MySplashScreen.CreateInstance(My.Application.SplashScreen)
                Me.SplashTraceEvent(TraceEventType.Verbose, "Using splash panel.")
                Me.SplashTraceEvent(TraceEventType.Verbose, "Parsing command line")
                e.Cancel = Not CommandLineInfo.TryParseCommandLine(e.CommandLine)
            End If
        End Sub

        ''' <summary> Processes the shut down. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        Private Sub ProcessShutDown()
            My.Application.SaveMySettingsOnExit = True
            If My.Application.SaveMySettingsOnExit Then
                isr.VI.Tsp.Rig.My.MySettings.Default.Save()
            End If
        End Sub

    End Class

End Namespace

