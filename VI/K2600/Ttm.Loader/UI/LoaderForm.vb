Imports isr.VI.ExceptionExtensions

''' <summary> Form for viewing the loader. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Class LoaderForm
    Inherits isr.Core.Forma.ConsoleForm

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Gets or sets the model view base. </summary>
    ''' <value> The model view base. </value>
    Private Property ModelViewBase As isr.Core.Forma.ModelViewTalkerBase = Nothing

    ''' <summary> Gets or sets the visa session. </summary>
    ''' <value> The visa session. </value>
    Private Property VisaSession As VI.VisaSessionBase = Nothing

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.VisaSession IsNot Nothing Then Me.VisaSession.Dispose() : Me.VisaSession = Nothing
        If Me.ModelViewBase IsNot Nothing Then Me.ModelViewBase.Dispose() : Me.ModelViewBase = Nothing
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    '''                  event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnClosing(e As ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            If Me.VisaSession IsNot Nothing Then
                If Me.VisaSession.CandidateResourceNameValidated Then
                    My.MySettings.Default.ResourceName = Me.VisaSession.ValidatedResourceName
                End If
                If Not String.IsNullOrWhiteSpace(Me.VisaSession.OpenResourceTitle) Then
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Name} adding Meter View"
            Me._LoaderView = New isr.VI.Tsp.Rig.Forms.LoaderView
            Me.ModelViewBase = Me.LoaderView
            Me.AssignDeviceThis(New isr.VI.Tsp.Rig.TspDevice)
            Me.AddTalkerControl("TTM", Me.ModelViewBase, False, False)
            ' any form messages will be logged.
            activity = $"{Me.Name}; adding log listener"
            Me.AddListener(My.Application.Logger)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Gets or sets the await the resource name validation task enabled. </summary>
    ''' <value> The await the resource name validation task enabled. </value>
    Protected Property AwaitResourceNameValidationTaskEnabled As Boolean

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnShown(e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.ModelViewBase.Cursor = Windows.Forms.Cursors.WaitCursor
            activity = $"{Me.Name} showing dialog"
            MyBase.OnShown(e)

            Me.LoaderView.SelectNavigatorTreeViewNode(Me.LoaderView.ConnectorNodeName)

            If Me.VisaSession IsNot Nothing AndAlso Me.VisaSession.IsValidatingResourceName Then
                If Me.AwaitResourceNameValidationTaskEnabled Then
                    activity = $"{Me.Name}; awaiting {Me.VisaSession.CandidateResourceName} validation"
                    Me.VisaSession.AwaitResourceNameValidation(TimeSpan.FromSeconds(3))
                Else
                    activity = $"{Me.Name}; validating {Me.VisaSession.CandidateResourceName}"
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
            If Me.ModelViewBase IsNot Nothing Then Me.ModelViewBase.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#Region " DEVICE EVENTS "

    ''' <summary> Gets or sets reference to the TSP device accessing the TTM Driver. </summary>
    ''' <value> The tsp device. </value>
    Public ReadOnly Property TspDevice() As isr.VI.Tsp.Rig.TspDevice

    ''' <summary> Gets or sets the loader view. </summary>
    ''' <value> The loader view. </value>
    Private ReadOnly Property LoaderView As isr.VI.Tsp.Rig.Forms.LoaderView

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As isr.VI.Tsp.Rig.TspDevice)
        If Me.TspDevice IsNot Nothing Then
            RemoveHandler Me.TspDevice.Opened, AddressOf Me.DeviceOpened
            Me.AssignTalker(Nothing)
            Me._TspDevice = Nothing
            Me.LoaderView?.AssignDevice(Me.TspDevice)
        End If
        Me._TspDevice = value
        If value IsNot Nothing Then
            ' the talker control publishes the device messages which thus get published to the form message box.
            Me.LoaderView.AssignDevice(Me.TspDevice)
            Me.LoaderView.ScriptManager.AuthorTitle = My.Resources.AuthorTitle
            Me.LoaderView.ScriptManager.BootFirmwareVersion = My.Resources.BootFirmwareVersion
            Me.LoaderView.ScriptManager.FirmwareReleasedVersion = My.Resources.MeterFirmwareVersion
            Me.LoaderView.ScriptManager.FrameworkName = My.Resources.FrameworkName
            Me.LoaderView.ScriptManager.FrameworkNamespace = My.Resources.FrameworkNamespace
            Me.LoaderView.ScriptManager.FrameworkTitle = My.Resources.FrameworkTitle
            Me.LoaderView.ScriptManager.MeterFirmwareVersion = My.Resources.MeterFirmwareVersion
            Me.LoaderView.ScriptManager.SupportFirmwareVersion = My.Resources.SupportFirmwareVersion
            Me.LoaderView.ScriptManager.LegacyScriptNames = My.Resources.LegacyScriptNames
            Me.LoaderView.ScriptManager.ScriptFilesFolderName = My.MySettings.Default.ScriptFilesFolderName
            Me.LoaderView.ScriptManager.FtpAddress = My.MySettings.Default.FtpAddress

            Me.LoaderView.ResourceName = My.MySettings.Default.ResourceName
            Me.VisaSession = Me.LoaderView.VisaSessionBase
            Me.VisaSession.CandidateResourceName = My.MySettings.Default.ResourceName
            Me.VisaSession.CandidateResourceTitle = "TTM"
            If Not String.IsNullOrWhiteSpace(Me.VisaSession.CandidateResourceName) Then
                Me.PublishInfo($"{Me.Name}; starting {Me.VisaSession.CandidateResourceName} selection task")
                Me.VisaSession.AsyncValidateResourceName(Me.VisaSession.CandidateResourceName)
            End If
            Me.AssignTalker(Me.TspDevice.Talker)
            AddHandler Me.TspDevice.Opened, AddressOf Me.DeviceOpened
        End If
    End Sub

    ''' <summary> Device opened. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = "setting access system properties"
            Me.TspDevice.AccessSubsystem.ScriptsFolderName = My.MySettings.Default.ScriptFilesFolderName
            Me.TspDevice.AccessSubsystem.ReleasedInstrumentsFileName = My.MySettings.Default.ReleasedInstrumentsFileName
            Me.TspDevice.AccessSubsystem.CertifiedInstrumentsFileName = My.MySettings.Default.CertifiedInstrumentsFileName
            Me.TspDevice.AccessSubsystem.SaltFileName = My.MySettings.Default.SaltFileName
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.Application.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyApplication.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
