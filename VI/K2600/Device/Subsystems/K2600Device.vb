Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Implements a Keithley 2600 source meter. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-12 </para>
''' </remarks>
Public Class K2600Device
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="K2600Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(StatusSubsystem.Create())
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.Settings_PropertyChanged
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As K2600Device
        Dim device As K2600Device = Nothing
        Try
            device = New K2600Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

    ''' <summary> Validated the given device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A Device. </returns>
    Public Shared Function Validated(ByVal device As K2600Device) As K2600Device
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

#Region " I Disposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"Exception disposing {GetType(K2600Device)}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindMeasureResistanceSubsystem(Nothing)
            Me.BindMeasureVoltageSubsystem(Nothing)
            Me.BindSourceSubsystem(Nothing)
            Me.BindSenseSubsystem(Nothing)
            Me.BindCurrentSourceSubsystem(Nothing)
            Me.BindDisplaySubsystem(Nothing)
            Me.BindContactSubsystem(Nothing)
            Me.BindLocalNodeSubsystem(Nothing)
            Me.BindSourceSubsystem(Nothing)
            Me.BindSystemSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
            Me.BindSourceMeasureUnit(New SourceMeasureUnit(Me.StatusSubsystem))
            Me.BindLocalNodeSubsystem(New LocalNodeSubsystem(Me.StatusSubsystem))
            Me.BindDisplaySubsystem(New DisplaySubsystem(Me.StatusSubsystem))
            Me.BindContactSubsystem(New ContactSubsystem(Me.StatusSubsystem))
            Me.BindCurrentSourceSubsystem(New CurrentSourceSubsystem(Me.StatusSubsystem))
            Me.BindSenseSubsystem(New SenseSubsystem(Me.StatusSubsystem))
            Me.BindSourceSubsystem(New SourceSubsystem(Me.StatusSubsystem))
            Me.BindMeasureVoltageSubsystem(New MeasureVoltageSubsystem(Me.StatusSubsystem))
            Me.BindMeasureResistanceSubsystem(New MeasureResistanceSubsystem(Me.StatusSubsystem))
        End If
    End Sub

    ''' <summary>
    ''' Allows the derived device to take actions after opening. Adds subsystems and event handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnOpened(ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceTitleCaption} handling on opened event" : Me.PublishVerbose($"{activity};. ")
            MyBase.OnOpened(e)
            Me.Session.ApplyServiceRequestEnableBitmask(VI.Pith.ServiceRequests.None)
        Catch ex As Exception
            Me.PublishException($"{activity}; closing...", ex)
            Me.CloseSession()
        End Try
    End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " COLLECTION "

    ''' <summary> Adds a subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub AddSubsystem(ByVal subsystem As VI.SubsystemBase)
        Me.Subsystems.Add(subsystem)
    End Sub

    ''' <summary> Removes the subsystem described by subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Sub RemoveSubsystem(ByVal subsystem As VI.SubsystemBase)
        Me.Subsystems.Remove(subsystem)
    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Bind the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SystemSubsystem)
        End If
    End Sub

#End Region

#Region " CURRENT SOURCE "

    ''' <summary> Gets or sets the Current Source Subsystem. </summary>
    ''' <value> The Current Source Subsystem. </value>
    Public ReadOnly Property CurrentSourceSubsystem As CurrentSourceSubsystem

    ''' <summary> Binds the Current Source subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindCurrentSourceSubsystem(ByVal subsystem As CurrentSourceSubsystem)
        If Me.CurrentSourceSubsystem IsNot Nothing Then
            Me.SourceMeasureUnit.Remove(Me.CurrentSourceSubsystem)
            Me.Subsystems.Remove(Me.CurrentSourceSubsystem)
            Me._CurrentSourceSubsystem = Nothing
        End If
        Me._CurrentSourceSubsystem = subsystem
        If Me.CurrentSourceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.CurrentSourceSubsystem)
            Me.SourceMeasureUnit.Add(Me.CurrentSourceSubsystem)
        End If
    End Sub

#End Region

#Region " CONTACT "

    ''' <summary> Gets or sets the contact Subsystem. </summary>
    ''' <value> The contact Subsystem. </value>
    Public ReadOnly Property ContactSubsystem As ContactSubsystem

    ''' <summary> Binds the contact subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindContactSubsystem(ByVal subsystem As ContactSubsystem)
        If Me.ContactSubsystem IsNot Nothing Then
            Me.SourceMeasureUnit.Remove(Me.ContactSubsystem)
            Me.Subsystems.Remove(Me.ContactSubsystem)
            Me._ContactSubsystem = Nothing
        End If
        Me._ContactSubsystem = subsystem
        If Me.ContactSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.ContactSubsystem)
            Me.SourceMeasureUnit.Add(Me.ContactSubsystem)
        End If
    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary> Gets or sets the Display Subsystem. </summary>
    ''' <value> The Display Subsystem. </value>
    Public ReadOnly Property DisplaySubsystem As DisplaySubsystem

    ''' <summary> Binds the Display subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindDisplaySubsystem(ByVal subsystem As DisplaySubsystem)
        If Me.DisplaySubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.DisplaySubsystem)
            Me._DisplaySubsystem = Nothing
        End If
        Me._DisplaySubsystem = subsystem
        If Me.DisplaySubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.DisplaySubsystem)
        End If
    End Sub

#End Region

#Region " SOURCE MEASURE UNIT "

    ''' <summary> Gets or sets the source measure unit Subsystem. </summary>
    ''' <value> The source measure unit Subsystem. </value>
    Public ReadOnly Property SourceMeasureUnit As SourceMeasureUnit

    ''' <summary> Binds the source measure unit subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSourceMeasureUnit(ByVal subsystem As SourceMeasureUnit)
        If Me.SourceMeasureUnit IsNot Nothing Then
            Me.SourceMeasureUnit.Remove(Me.SourceMeasureUnit)
            Me.Subsystems.Remove(Me.SourceMeasureUnit)
            Me._SourceMeasureUnit = Nothing
        End If
        Me._SourceMeasureUnit = subsystem
        If Me.SourceMeasureUnit IsNot Nothing Then
            Me.Subsystems.Add(Me.SourceMeasureUnit)
            Me.SourceMeasureUnit.Add(Me.SourceMeasureUnit)
        End If
    End Sub

#End Region

#Region " LOCAL NODE "

    ''' <summary> Gets or sets the Local Node Subsystem. </summary>
    ''' <value> The Local Node Subsystem. </value>
    Public ReadOnly Property LocalNodeSubsystem As LocalNodeSubsystem

    ''' <summary> Binds the Local Node subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindLocalNodeSubsystem(ByVal subsystem As LocalNodeSubsystem)
        If Me.LocalNodeSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.LocalNodeSubsystem)
            Me._LocalNodeSubsystem = Nothing
        End If
        Me._LocalNodeSubsystem = subsystem
        If Me.LocalNodeSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.LocalNodeSubsystem)
        End If
    End Sub

#End Region

#Region " SENSE "

    ''' <summary> Gets or sets the Sense Subsystem. </summary>
    ''' <value> The Sense Subsystem. </value>
    Public ReadOnly Property SenseSubsystem As SenseSubsystem

    ''' <summary> Binds the Sense subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseSubsystem(ByVal subsystem As SenseSubsystem)
        If Me.SenseSubsystem IsNot Nothing Then
            Me.SourceMeasureUnit.Remove(Me.SenseSubsystem)
            Me.Subsystems.Remove(Me.SenseSubsystem)
            Me._SenseSubsystem = Nothing
        End If
        Me._SenseSubsystem = subsystem
        If Me.SenseSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseSubsystem)
            Me.SourceMeasureUnit.Add(Me.SenseSubsystem)
        End If
    End Sub

#End Region

#Region " SOURCE "

    ''' <summary> Gets or sets the Source Subsystem. </summary>
    ''' <value> The Source Subsystem. </value>
    Public ReadOnly Property SourceSubsystem As SourceSubsystem

    ''' <summary> Binds the Source subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSourceSubsystem(ByVal subsystem As SourceSubsystem)
        If Me.SourceSubsystem IsNot Nothing Then
            Me.SourceMeasureUnit.Remove(Me.SourceSubsystem)
            Me.Subsystems.Remove(Me.SourceSubsystem)
            Me._SourceSubsystem = Nothing
        End If
        Me._SourceSubsystem = subsystem
        If Me.SourceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SourceSubsystem)
            Me.SourceMeasureUnit.Add(Me.SourceSubsystem)
        End If
    End Sub

#End Region

#Region " MEASURE RESISTANCE "

    ''' <summary> Gets or sets the Measure Resistance Subsystem. </summary>
    ''' <value> The Measure Resistance Subsystem. </value>
    Public ReadOnly Property MeasureResistanceSubsystem As MeasureResistanceSubsystem

    ''' <summary> Binds the Measure Resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindMeasureResistanceSubsystem(ByVal subsystem As MeasureResistanceSubsystem)
        If Me.MeasureResistanceSubsystem IsNot Nothing Then
            Me.SourceMeasureUnit.Remove(Me.MeasureResistanceSubsystem)
            Me.Subsystems.Remove(Me.MeasureResistanceSubsystem)
            Me._MeasureResistanceSubsystem = Nothing
        End If
        Me._MeasureResistanceSubsystem = subsystem
        If Me.MeasureResistanceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.MeasureResistanceSubsystem)
            Me.SourceMeasureUnit.Add(Me.MeasureResistanceSubsystem)
        End If
    End Sub

#End Region

#Region " MEASURE VOLTAGE "

    ''' <summary> Gets or sets the Measure Voltage Subsystem. </summary>
    ''' <value> The Measure Voltage Subsystem. </value>
    Public ReadOnly Property MeasureVoltageSubsystem As MeasureVoltageSubsystem

    ''' <summary> Binds the Measure Voltage subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindMeasureVoltageSubsystem(ByVal subsystem As MeasureVoltageSubsystem)
        If Me.MeasureVoltageSubsystem IsNot Nothing Then
            Me.SourceMeasureUnit.Remove(Me.MeasureVoltageSubsystem)
            Me.Subsystems.Remove(Me.MeasureVoltageSubsystem)
            Me._MeasureVoltageSubsystem = Nothing
        End If
        Me._MeasureVoltageSubsystem = subsystem
        If Me.MeasureVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.MeasureVoltageSubsystem)
            Me.SourceMeasureUnit.Add(Me.MeasureVoltageSubsystem)
        End If
    End Sub

#End Region

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("K2600 Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InterfaceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadDelay))

    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceShowLevel}")
            Case NameOf(My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ClearRefractoryPeriod}")
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InitRefractoryPeriod}")
            Case NameOf(My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} changed to {sender.InitializeTimeout}")
            Case NameOf(My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ResetRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
            Case NameOf(My.MySettings.ReadDelay)
                Me.Session.ReadDelay = TimeSpan.FromMilliseconds(sender.ReadDelay)
            Case NameOf(My.MySettings.StatusReadDelay)
                Me.Session.StatusReadDelay = TimeSpan.FromMilliseconds(sender.StatusReadDelay)
            Case NameOf(My.MySettings.StatusReadTurnaroundTime)
                Me.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime
            Case NameOf(My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Settings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

