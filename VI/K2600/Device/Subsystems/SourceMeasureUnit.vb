﻿''' <summary> Source Measure Unit subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class SourceMeasureUnit
    Inherits isr.VI.Tsp.SourceMeasureUnitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SourceMeasureUnit" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="P:isr.VI.SubsystemPlusStatusBase.StatusSubsystem">TSP
    '''                                status Subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="SourceMeasureUnit" /> class. </summary>
    ''' <remarks>
    ''' Note that the local node status clear command only clears the SMU status.  So, issue a CLS
    ''' and RST as necessary when adding an SMU.
    ''' </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    '''                                Subsystem</see>. </param>
    ''' <param name="nodeNumber">      Specifies the node number. </param>
    ''' <param name="smuNumber">       Specifies the SMU (either 'a' or 'b'. </param>
    Public Sub New(ByVal statusSubsystem As StatusSubsystemBase, ByVal nodeNumber As Integer, ByVal smuNumber As String)
        MyBase.New(statusSubsystem, nodeNumber, smuNumber)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#End Region

End Class
