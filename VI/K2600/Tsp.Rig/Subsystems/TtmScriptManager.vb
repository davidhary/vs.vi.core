Imports isr.VI.Tsp.Script

''' <summary> TTM Script Manager. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class TtmScriptManager
    Inherits ScriptManagerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ScriptManager" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As TspDevice)
        MyBase.New(device.StatusSubsystem)
        Me.DisplaySubsystem = device.DisplaySubsystem
        Me.LinkSubsystem = device.LinkSubsystem
        Me.InteractiveSubsystem = device.InteractiveSubsystem
    End Sub

#End Region

#Region " RESOURCES "

    ''' <summary> The FTP address. </summary>
    Private _FtpAddress As String

    ''' <summary> Gets or sets the Author Title. </summary>
    ''' <remarks> Get from My.Settings.FtpAddress. </remarks>
    ''' <value> The Author Title. </value>
    Public Property FtpAddress As String
        Get
            Return Me._FtpAddress
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FtpAddress) Then
                Me._FtpAddress = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The author title. </summary>
    Private _AuthorTitle As String

    ''' <summary> Gets or sets the Author Title. </summary>
    ''' <remarks> Get from My.Resources.AuthorTitle. </remarks>
    ''' <value> The Author Title. </value>
    Public Property AuthorTitle As String
        Get
            Return Me._AuthorTitle
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.AuthorTitle) Then
                Me._AuthorTitle = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The firmware released version. </summary>
    Private _FirmwareReleasedVersion As String

    ''' <summary> Gets or sets the firmware released version. </summary>
    ''' <remarks> Get from My.Resources.MeterFirmwareVersion. </remarks>
    ''' <value> The firmware released version. </value>
    Public Property FirmwareReleasedVersion As String
        Get
            Return Me._FirmwareReleasedVersion
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FirmwareReleasedVersion) Then
                Me._FirmwareReleasedVersion = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The framework namespace. </summary>
    Private _FrameworkNamespace As String

    ''' <summary> Gets or sets the framework namespace. </summary>
    ''' <remarks> Get from My.Resources.FrameworkNamespace. </remarks>
    ''' <value> The framework namespace. </value>
    Public Property FrameworkNamespace As String
        Get
            Return Me._FrameworkNamespace
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FrameworkNamespace) Then
                Me._FrameworkNamespace = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Name of the framework. </summary>
    Private _FrameworkName As String

    ''' <summary> Gets or sets the framework Name. </summary>
    ''' <remarks> Get from My.Resources.FrameworkName. </remarks>
    ''' <value> The framework Name. </value>
    Public Property FrameworkName As String
        Get
            Return Me._FrameworkName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FrameworkName) Then
                Me._FrameworkName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The framework title. </summary>
    Private _FrameworkTitle As String

    ''' <summary> Gets or sets the framework Title. </summary>
    ''' <remarks> Get from My.Resources.FrameworkTitle. </remarks>
    ''' <value> The framework Title. </value>
    Public Property FrameworkTitle As String
        Get
            Return Me._FrameworkTitle
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FrameworkTitle) Then
                Me._FrameworkTitle = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The boot firmware version. </summary>
    Private _BootFirmwareVersion As String

    ''' <summary> Gets or sets the Boot firmware version. </summary>
    ''' <remarks> Get from My.Resources.BootFirmwareVersion. </remarks>
    ''' <value> The Boot firmware version. </value>
    Public Property BootFirmwareVersion As String
        Get
            Return Me._BootFirmwareVersion
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.BootFirmwareVersion) Then
                Me._BootFirmwareVersion = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The meter firmware version. </summary>
    Private _MeterFirmwareVersion As String

    ''' <summary> Gets or sets the meter firmware version. </summary>
    ''' <remarks> Get from My.Resources.MeterFirmwareVersion. </remarks>
    ''' <value> The meter firmware version. </value>
    Public Property MeterFirmwareVersion As String
        Get
            Return Me._MeterFirmwareVersion
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.MeterFirmwareVersion) Then
                Me._MeterFirmwareVersion = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The support firmware version. </summary>
    Private _SupportFirmwareVersion As String

    ''' <summary> Gets or sets the Support firmware version. </summary>
    ''' <remarks> Get from My.Resources.SupportFirmwareVersion. </remarks>
    ''' <value> The Support firmware version. </value>
    Public Property SupportFirmwareVersion As String
        Get
            Return Me._SupportFirmwareVersion
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.SupportFirmwareVersion) Then
                Me._SupportFirmwareVersion = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> List of names of the legacy scripts. </summary>
    Private _LegacyScriptNames As String

    ''' <summary> Gets or sets a list of names of the legacy scripts. </summary>
    ''' <remarks> Get from My.Resources.LegacyScripts. </remarks>
    ''' <value> A list of names of the legacy scripts. </value>
    Public Property LegacyScriptNames As String
        Get
            Return Me._LegacyScriptNames
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.LegacyScriptNames) Then
                Me._LegacyScriptNames = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Pathname of the script files folder. </summary>
    Private _ScriptFilesFolderName As String = "C:\My\Private\ttm"

    ''' <summary> Gets or sets the pathname of the script files folder. </summary>
    ''' <remarks> C:\My\Private\ttm. </remarks>
    ''' <value> The pathname of the script files folder. </value>
    Public Property ScriptFilesFolderName As String
        Get
            Return Me._ScriptFilesFolderName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ScriptFilesFolderName) Then
                Me._ScriptFilesFolderName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " FRAMEWORK SCRIPTS "

    ''' <summary> Define scripts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineScripts()

        Dim script As VI.Tsp.Script.ScriptEntityBase
        Me.NewLegacyScripts()

        If Debugger.IsAttached Then
            ' enumerate the legacy scripts.
            If Not String.IsNullOrWhiteSpace(Me.LegacyScriptNames) Then
                Dim lagacyScrpitNames As String() = Me.LegacyScriptNames.Split(","c)
                For Each scriptName As String In lagacyScrpitNames
                    script = Me.AddLegacyScript(scriptName)
                    script.RequiresDeletion = True
                Next
            End If
        End If

        Me.NewScripts()

        Me.AddSupportScript("isr_support", VI.InstrumentModelFamily.K2600)
        Me.AddSupportScript("isr_support", VI.InstrumentModelFamily.K2600A)
        Me.AddMeterScript("isr_ttm", VI.InstrumentModelFamily.K2600)
        Me.AddMeterScript("isr_ttm", VI.InstrumentModelFamily.K2600A)

        ' the boot script must be last.
        Me.AddBootScript()

    End Sub

    ''' <summary> Adds a meter script to 'instrumentModelFamily'. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="scriptName">            Name of the script. </param>
    ''' <param name="instrumentModelFamily"> The instrument model family. </param>
    Private Sub AddMeterScript(ByVal scriptName As String, ByVal instrumentModelFamily As VI.InstrumentModelFamily)
        Dim script As VI.Tsp.Script.ScriptEntityBase
        script = Me.AddScript(scriptName, VI.NodeEntity.ModelFamilyMask(instrumentModelFamily))
        script.FileName = "isr_ttm.tsp"
        script.ResourceFileName = $"{script.FileName}{VI.NodeEntity.ModelFamilyResourceFileSuffix(instrumentModelFamily)}"
        script.NamespaceList = "isr,isr.meters,isr.meters.thermalTransient"
        script.FirmwareVersionGetter = "_G.isr.meters.thermalTransient.version()"
        script.ReleasedFirmwareVersion = Me.MeterFirmwareVersion
        script.IsPrimaryScript = True
        script.FileFormat = VI.Tsp.Script.ScriptFileFormats.Binary Or VI.Tsp.Script.ScriptFileFormats.Compressed
        ' script.Source = EmbeddedResourceManager.ReadEmbeddedTextResource(script.ResourceFileName)
        script.Source = My.Computer.FileSystem.ReadAllText(System.IO.Path.Combine(Me.ScriptFilesFolderName, script.ResourceFileName))
    End Sub

    ''' <summary> Adds a support script to 'instrumentModelFamily'. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="scriptName">            Name of the script. </param>
    ''' <param name="instrumentModelFamily"> The instrument model family. </param>
    Private Sub AddSupportScript(ByVal scriptName As String, ByVal instrumentModelFamily As VI.InstrumentModelFamily)
        Dim script As VI.Tsp.Script.ScriptEntityBase
        script = Me.AddScript(scriptName, VI.NodeEntity.ModelFamilyMask(instrumentModelFamily))
        script.FileName = "isr_support.tsp"
        script.ResourceFileName = $"{script.FileName}{VI.NodeEntity.ModelFamilyResourceFileSuffix(instrumentModelFamily)}"
        script.NamespaceList = "isr"
        script.FirmwareVersionGetter = "_G.isr.version()"
        script.ReleasedFirmwareVersion = Me.SupportFirmwareVersion
        script.IsSupportScript = True
        script.FileFormat = VI.Tsp.Script.ScriptFileFormats.Binary Or VI.Tsp.Script.ScriptFileFormats.Compressed
        ' script.Source = EmbeddedResourceManager.ReadEmbeddedTextResource(script.ResourceFileName)
        script.Source = My.Computer.FileSystem.ReadAllText(System.IO.Path.Combine(Me.ScriptFilesFolderName, script.ResourceFileName))
    End Sub

    ''' <summary> Adds boot script. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AddBootScript()

        Dim script As VI.Tsp.Script.ScriptEntityBase
        script = Me.AddScript("isr_ttm_boot", "")
        script.FileName = String.Empty ' not available from file
        script.NamespaceList = "isr,isr.ttm"
        script.FirmwareVersionGetter = "_G.isr.ttm.version()"
        script.ReleasedFirmwareVersion = Me.BootFirmwareVersion
        script.IsBootScript = True

        ' create auto script custom commands.
        Dim customCommands As New System.Text.StringBuilder(2048)
        Dim bootNamespace As String = String.Empty
        For Each bootNamespace In script.Namespaces
            customCommands.AppendFormat("{0} = {0} or {{}}", bootNamespace)
            customCommands.AppendLine()
        Next
        If String.IsNullOrWhiteSpace(bootNamespace) Then
            bootNamespace = Me.FrameworkNamespace
        End If
        customCommands.AppendFormat("function {0} return '{1}' end", script.FirmwareVersionGetter, script.ReleasedFirmwareVersion)
        customCommands.AppendLine()
        customCommands.AppendFormat("{0}.displayTitle = function()", bootNamespace)
        customCommands.AppendLine()
        customCommands.AppendLine("if _G.display ~= nil then")
        customCommands.AppendLine("_G.display.clear()")
        customCommands.AppendLine("_G.display.setcursor(1, 1)")
        customCommands.AppendFormat("_G.display.settext('{0}')", Me.FrameworkTitle)
        customCommands.AppendLine()
        customCommands.AppendLine("_G.display.setcursor(2, 1)")
        customCommands.AppendFormat("_G.display.settext('{0}')", Me.AuthorTitle)
        customCommands.AppendLine()
        customCommands.AppendLine("end")
        customCommands.AppendLine("end")
        customCommands.AppendFormat("{0}.displayTitle()", bootNamespace)
        customCommands.AppendLine()
        script.Source = VI.Tsp.Script.ScriptManagerBase.BuildAutoRunScript(customCommands, Me.Scripts)
    End Sub

    ''' <summary> Deletes the user scripts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="statusSubsystem">  A reference to a
    '''                                 <see cref="VI.Tsp.StatusSubsystemBase">status
    '''                                 subsystem</see>. </param>
    ''' <param name="displaySubsystem"> A reference to a
    '''                                 <see cref="VI.Tsp.DisplaySubsystemBase">display
    '''                                 subsystem</see>. </param>
    Public Overrides Sub DeleteUserScripts(ByVal statusSubsystem As VI.Tsp.StatusSubsystemBase, ByVal displaySubsystem As VI.Tsp.DisplaySubsystemBase)

        If statusSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(statusSubsystem))
        If displaySubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(displaySubsystem))

        Dim hasLegacyScripts As Boolean = Me.LegacyScripts.FindAnyScript(Me.LinkSubsystem.ControllerNode, Me.Session)
        Dim hasScripts As Boolean = Me.Scripts.FindAnyScript(Me.LinkSubsystem.ControllerNode, Me.Session)

        If hasLegacyScripts OrElse hasScripts Then

            ' reset all status values so they are forced to be read.
            Me.DefineKnownResetState()

            If hasLegacyScripts Then
                displaySubsystem.DisplayLine(1, "Deleting {0}", Me.FrameworkName)
                displaySubsystem.DisplayLine(2, "Deleting legacy scripts")
                Me.DeleteUserScripts(Me.LegacyScripts, Me.LinkSubsystem.NodeEntities, True, True)
            End If

            If hasScripts Then
                If Not Me.IsDeleteUserScriptRequired(Me.Scripts, Me.LinkSubsystem.NodeEntities, True) Then
                    If isr.Core.MyMessageBox.ShowDialogCancelOkay("Scripts are up to date -- deletion is not required. Select OK to proceed and remove the scripts.",
                                                                  "Scripts up to date, Are you sure?") = Core.MyDialogResult.Ok Then

                        displaySubsystem.DisplayLine(1, "Deleting {0}", Me.FrameworkName)
                        displaySubsystem.DisplayLine(2, "Deleting scripts")
                        Me.DeleteUserScripts(Me.Scripts, Me.LinkSubsystem.NodeEntities, True, False)
                    End If
                End If
            End If

            ' clear state
            Me.DefineKnownResetState()

        End If

    End Sub

    ''' <summary> Saves the user scripts applying the release value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeout">          The timeout. </param>
    ''' <param name="statusSubsystem">  A reference to a
    '''                                 <see cref="VI.Tsp.StatusSubsystemBase">status
    '''                                 subsystem</see>. </param>
    ''' <param name="displaySubsystem"> A reference to a
    '''                                 <see cref="VI.Tsp.DisplaySubsystemBase">display
    '''                                 subsystem</see>. </param>
    ''' <param name="accessSubsystem">  The access subsystem. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function SaveUserScripts(ByVal timeout As TimeSpan,
                                                        ByVal statusSubsystem As VI.Tsp.StatusSubsystemBase,
                                                        ByVal displaySubsystem As VI.Tsp.DisplaySubsystemBase,
                                                        ByVal accessSubsystem As VI.AccessSubsystemBase) As Boolean

        If statusSubsystem Is Nothing Then
            Throw New ArgumentNullException(NameOf(statusSubsystem))
        End If
        If displaySubsystem Is Nothing Then
            Throw New ArgumentNullException(NameOf(displaySubsystem))
        End If
        If accessSubsystem Is Nothing Then
            Throw New ArgumentNullException(NameOf(accessSubsystem))
        End If

        Dim value As String = accessSubsystem.ReleaseValue(statusSubsystem.SerialNumberReading, accessSubsystem.Salt)

        ' ensure we can finalize TTM.
        Me.PublishVerbose($"'{Me.Session.ResourceNameCaption}' confirming {Me.FirmwareNameGetter()} loaded;. ")
        If Not Me.FindFirmware() Then
            Me.PublishWarning($"'{Me.Session.ResourceNameCaption}' failed saving custom firmware {Me.FirmwareNameGetter()};. because the firmware was not loaded.")
            Return False
        End If

        If accessSubsystem.CertifiedInstruments.IndexOf(isr.Core.HashExtensions.HashExtensionMethods.ToBase64Hash(statusSubsystem.SerialNumberReading), StringComparison.Ordinal) < 0 Then
            If accessSubsystem.Loaded() Then
                Me.PublishWarning(Me.NewProgramRequired)
            Else
                Me.PublishWarning($"Saving custom firmware failed;. Instrument '{statusSubsystem.Identity}' may require upgrading the custom scripts loaded by this program to the instrument or registration with this program vendor")
            End If
            Return False
        End If

        If Not accessSubsystem.Certify(value) Then
            Me.PublishWarning($"Saving custom firmware failed;. Instrument '{statusSubsystem.Identity}' console program requires update of its custom scripts or registration with the program vendor")
            Return False
        End If

        displaySubsystem.DisplayLine(1, $"Saving {Me.FrameworkName}")
        displaySubsystem.DisplayLine(2, "Saving scripts")
        If Me.SaveUserScripts(Me.Scripts, Me.LinkSubsystem.NodeEntities) Then

            ' write script files of new scripts were saved.
            displaySubsystem.DisplayLine(2, $"Current Version {Me.FirmwareReleasedVersionGetter()}")
            If Debugger.IsAttached Then
                displaySubsystem.DisplayLine(1, $"Writing {Me.FrameworkName}")
                If Not Me.WriteScriptFiles(Me.FilePath, Me.Scripts, Me.LinkSubsystem.NodeEntities) Then
                    Me.PublishInfo($"Instrument '{Me.Session.ResourceNameCaption}' failed writing one or more user scripts;. Error ignored")
                End If
            End If

            ' do a garbage collection
            displaySubsystem.DisplayLine(1, $"Cleaning up {Me.FrameworkName}")
            displaySubsystem.DisplayLine(2, "Collecting garbage")
            Me.PublishVerbose($"'{Me.Session.ResourceNameCaption}' collecting garbage")
            If Not statusSubsystem.CollectGarbageWaitComplete(timeout, "collecting garbage;. ") Then
                Me.PublishInfo($"'{Me.Session.ResourceNameCaption}' failed collecting garbage;. Problem ignored.")
            End If

            displaySubsystem.DisplayLine(1, $"Resetting {Me.FrameworkName}")
            displaySubsystem.DisplayLine(2, "Resetting node")
            Me.PublishVerbose("Instrument '{0}' resetting node;. ", Me.Session.ResourceNameCaption)
            Me.LinkSubsystem.ResetNode()
            displaySubsystem.DisplayTitle(Me.FrameworkTitle, Me.AuthorTitle)
        Else
            Me.PublishInfo($"'{Me.Session.ResourceNameCaption}' failed saving one or more user scripts;. ")
            Return False
        End If
        Return True
    End Function

    ''' <summary> Reads and parses user scripts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="statusSubsystem">  A reference to a
    '''                                 <see cref="VI.Tsp.StatusSubsystemBase">status
    '''                                 subsystem</see>. </param>
    ''' <param name="displaySubsystem"> A reference to a
    '''                                 <see cref="VI.Tsp.DisplaySubsystemBase">display
    '''                                 subsystem</see>. </param>
    Public Sub ReadParseUserScripts(ByVal statusSubsystem As VI.Tsp.StatusSubsystemBase, ByVal displaySubsystem As VI.Tsp.DisplaySubsystemBase)
        If statusSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(statusSubsystem))
        If displaySubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(displaySubsystem))
        displaySubsystem.DisplayLine(1, $"Parsing {Me.FrameworkName}")
        VI.Tsp.Script.ScriptManagerBase.ReadParseWriteScripts(Me.LinkSubsystem.ControllerNode.ModelNumber, Me.FilePath, Me.Scripts, True)
        Me.PublishInfo($"'{Me.Session.ResourceNameCaption}' saved parsed scripts to disk;. Application cannot proceed until scripts are added to the program resource.")
    End Sub

    ''' <summary> Try read parse user scripts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem">  A reference to a
    '''                                 <see cref="VI.Tsp.StatusSubsystemBase">status
    '''                                 subsystem</see>. </param>
    ''' <param name="displaySubsystem"> A reference to a
    '''                                 <see cref="VI.Tsp.DisplaySubsystemBase">display
    '''                                 subsystem</see>. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Function TryReadParseUserScripts(ByVal statusSubsystem As VI.Tsp.StatusSubsystemBase, ByVal displaySubsystem As VI.Tsp.DisplaySubsystemBase) As Boolean
        If statusSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(statusSubsystem))
        If displaySubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(displaySubsystem))
        Dim activity As String = String.Empty
        Dim result As Boolean = False
        Try
            activity = "reading and parsing user scripts"
            Me.ReadParseUserScripts(statusSubsystem, displaySubsystem)
            result = True
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
        Return result
    End Function

    ''' <summary> Uploads the user scripts to the controller instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem">  A reference to a
    '''                                 <see cref="T:isr.VI.Tsp.StatusSubsystemBase">status
    '''                                 subsystem</see>. </param>
    ''' <param name="displaySubsystem"> A reference to a
    '''                                 <see cref="T:isr.VI.Tsp.DisplaySubsystemBase">display
    '''                                 subsystem</see>. </param>
    ''' <param name="accessSubsystem">  The access subsystem. </param>
    ''' <returns> <c>True</c> if uploaded; otherwise, <c>False</c>. </returns>
    Public Overrides Function UploadUserScripts(ByVal statusSubsystem As VI.Tsp.StatusSubsystemBase,
                                                ByVal displaySubsystem As VI.Tsp.DisplaySubsystemBase,
                                                ByVal accessSubsystem As VI.AccessSubsystemBase) As Boolean
        If statusSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(statusSubsystem))
        If displaySubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(displaySubsystem))
        If accessSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(accessSubsystem))

        ' reset all status values so as to force a read.
        Me.DefineKnownResetState()

        ' read the serial number.
        statusSubsystem.QuerySerialNumber()

        ' read the instrument identity.
        statusSubsystem.QueryIdentity()

        ' check if new script source is available for storage. 
        If Me.Scripts.RequiresReadParseWrite() Then
            Me.ReadParseUserScripts(statusSubsystem, displaySubsystem)
            Return False
        End If

        ' Check if instrument serial number is listed in the instrument resource.
        If accessSubsystem.CertifiedInstruments.IndexOf(isr.Core.HashExtensions.HashExtensionMethods.ToBase64Hash(statusSubsystem.SerialNumberReading),
                                                        StringComparison.Ordinal) < 0 Then
            Me.PublishWarning(Me.NewProgramRequired)
            Return False
        End If

        ' delete legacy scripts if any and ignore any errors that might have occurred.
        ' do not ignore version on upload.
        Me.DeleteUserScripts(statusSubsystem, displaySubsystem)

        displaySubsystem.DisplayLine(1, $"Uploading {Me.FrameworkName}")
        displaySubsystem.DisplayLine(2, "Selecting scripts for upload")
        Dim failed As Boolean = Not Me.UpdateUserScripts(Me.Scripts, Me.LinkSubsystem.NodeEntities)

        ' display the title if not failed.
        If Not failed Then displaySubsystem.DisplayTitle(Me.FrameworkTitle, Me._FrameworkName)
        Return Not failed

    End Function

#End Region

End Class
