﻿Imports isr.VI.Tsp.Script

''' <summary> Script Manager. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class ScriptManager
    Inherits ScriptManagerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ScriptManager" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As TspDevice)
        MyBase.New(device.StatusSubsystem)
        Me.DisplaySubsystem = device.DisplaySubsystem
        Me.LinkSubsystem = device.LinkSubsystem
        Me.InteractiveSubsystem = device.InteractiveSubsystem
    End Sub

#End Region


End Class
