''' <summary> Display subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class DisplaySubsystem
    Inherits isr.VI.Tsp.DisplaySubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="DisplaySubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.Tsp.StatusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " ENABLED "

    ''' <summary> Gets the display enable command format. </summary>
    ''' <value> The display enable command format. </value>
    Protected Overrides Property DisplayEnableCommandFormat As String
        Get
            ' ":DISP:ENAB {0:'ON';'ON';'OFF'}"
            Return String.Empty
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets the display enabled query command. </summary>
    ''' <value> The display enabled query command. </value>
    Protected Overrides Property DisplayEnabledQueryCommand As String
        Get
            ' ":DISP:ENAB?"
            Return String.Empty
        End Get
        Set(value As String)
        End Set
    End Property

#End Region

#End Region


End Class
