''' <summary> Status subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class StatusSubsystem
    Inherits isr.VI.Tsp.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> The session. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(session)
        Me.VersionInfoBase = New VersionInfo
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create() As StatusSubsystem
        Dim subsystem As StatusSubsystem = Nothing
        Try
            subsystem = New StatusSubsystem(isr.VI.SessionFactory.Get.Factory.Session())
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function


#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        ' establish the current node as the controller node. 
        ' moved to tsp subsystem base with setting the conditional to true when creating the subsystem: 
        ' Me.StatusSubsystem.InitiateControllerNode()
    End Sub

#End Region

#Region " TSP ACCESS  "

    ''' <summary> The new program required. </summary>
    Private _NewProgramRequired As String

    ''' <summary>
    ''' Gets the message indicating that a new program is required for the instrument because this
    ''' instrument is not included in the instrument list.
    ''' </summary>
    ''' <value> The new program required. </value>
    Public ReadOnly Property NewProgramRequired() As String
        Get
            If String.IsNullOrWhiteSpace(Me._NewProgramRequired) Then
                Me._NewProgramRequired = $"A new version of the program is required;. for instrument {Me.Identity}"
            End If
            Return Me._NewProgramRequired
        End Get
    End Property

#End Region

End Class
