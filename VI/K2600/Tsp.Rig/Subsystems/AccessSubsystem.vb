''' <summary> Defines a System Subsystem for a TSP System. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-12-12, 3.0.5093. </para>
''' </remarks>
Public Class AccessSubsystem
    Inherits VI.AccessSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="AccessSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="Vi.Tsp.StatusSubsystemBase">TSP
    '''                                status Subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.Tsp.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Dim filename As String = System.IO.Path.Combine(Me.ScriptsFolderName, Me.ReleasedInstrumentsFileName)
        If My.Computer.FileSystem.FileExists(filename) Then
            Me.ReleasedInstruments = My.Computer.FileSystem.ReadAllText(filename)
        End If
        filename = System.IO.Path.Combine(Me.ScriptsFolderName, Me.ReleasedInstrumentsFileName)
        If My.Computer.FileSystem.FileExists(filename) Then
            Me.ReleasedInstruments = My.Computer.FileSystem.ReadAllText(filename)
        End If
        filename = System.IO.Path.Combine(Me.ScriptsFolderName, Me.SaltFileName)
        If My.Computer.FileSystem.FileExists(filename) Then
            Me.Salt = My.Computer.FileSystem.ReadAllText(filename).Trim
        End If
    End Sub

#End Region

#Region " SESSION / STATUS SUBSYSTEM"

    ''' <summary> Gets the session. </summary>
    ''' <value> The tsp session. </value>
    Private ReadOnly Property TspSession As isr.VI.Pith.SessionBase
        Get
            Return Me.StatusSubsystem.Session
        End Get
    End Property

#End Region

#Region " CERTIFY "

    ''' <summary> Certifies. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns>
    ''' <c>null</c> if not known; <c>True</c> if certified; otherwise, <c>False</c>.
    ''' </returns>
    Public Overrides Function Certify(ByVal value As String) As Boolean?
        If Me.StatusSubsystem?.IsDeviceOpen Then
            Me.Session.StoreCommunicationTimeout(Me.CertifyTimeout)
            Try
                Me.Certified = If(String.IsNullOrWhiteSpace(value), New Boolean?, Me.TspSession.IsStatementTrue("_G.isr.access.allow('{0}')", value.Trim))
            Catch
                Throw
            Finally
                Me.Session.RestoreCommunicationTimeout()
            End Try
        End If
        Return Me.Certified
    End Function

#End Region

#Region " ACCESS MANAGEMENT "

    ''' <summary> Pathname of the scripts folder. </summary>
    Private _ScriptsFolderName As String

    ''' <summary> Gets or sets the pathname of the scripts folder. </summary>
    ''' <value> The pathname of the scripts folder. </value>
    Public Property ScriptsFolderName As String
        Get
            Return Me._ScriptsFolderName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ScriptsFolderName) Then
                Me._ScriptsFolderName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Filename of the certified instruments file. </summary>
    Private _CertifiedInstrumentsFileName As String

    ''' <summary> Gets or sets the filename of the certified instruments file. </summary>
    ''' <value> The filename of the certified instruments file. </value>
    Public Property CertifiedInstrumentsFileName As String
        Get
            Return Me._CertifiedInstrumentsFileName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.CertifiedInstrumentsFileName) Then
                Me._CertifiedInstrumentsFileName = value
                Dim filename As String = System.IO.Path.Combine(Me.ScriptsFolderName, Me.CertifiedInstrumentsFileName)
                If My.Computer.FileSystem.FileExists(filename) Then
                    Me.CertifiedInstruments = My.Computer.FileSystem.ReadAllText(filename)
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Filename of the salt file. </summary>
    Private _SaltFileName As String

    ''' <summary> Gets or sets the filename of the released instruments file. </summary>
    ''' <value> The filename of the released instruments file. </value>
    Public Property SaltFileName As String
        Get
            Return Me._SaltFileName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.SaltFileName) Then
                Me._SaltFileName = value
                Dim filename As String = System.IO.Path.Combine(Me.ScriptsFolderName, Me.SaltFileName)
                If My.Computer.FileSystem.FileExists(filename) Then
                    Me.Salt = My.Computer.FileSystem.ReadAllText(filename).Trim
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Filename of the released instruments file. </summary>
    Private _ReleasedInstrumentsFileName As String

    ''' <summary> Gets or sets the filename of the released instruments file. </summary>
    ''' <value> The filename of the released instruments file. </value>
    Public Property ReleasedInstrumentsFileName As String
        Get
            Return Me._ReleasedInstrumentsFileName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ReleasedInstrumentsFileName) Then
                Me._ReleasedInstrumentsFileName = value
                Dim filename As String = System.IO.Path.Combine(Me.ScriptsFolderName, Me.ReleasedInstrumentsFileName)
                If My.Computer.FileSystem.FileExists(filename) Then
                    Me.ReleasedInstruments = My.Computer.FileSystem.ReadAllText(filename)
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Checks if the custom scripts loaded successfully. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> <c>True</c> if loaded; otherwise, <c>False</c>. </returns>
    Public Overrides Function Loaded() As Boolean
        Return Me.TspSession.IsStatementTrue("_G.isr.access.loaded()")
    End Function

    ''' <summary> Gets the release code for the controller instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="serialNumber"> The serial number. </param>
    ''' <param name="salt">         The released instruments. </param>
    ''' <returns> The release value of the controller instrument. </returns>
    Public Overrides Function ReleaseValue(ByVal serialNumber As String, ByVal salt As String) As String
        Dim result As String = String.Empty
        Dim certifiedInstruments() As String = Me.CertifiedInstruments.Split(Environment.NewLine.ToCharArray, StringSplitOptions.RemoveEmptyEntries)
        Dim releasedInstruments() As String = Me.ReleasedInstruments.Split(Environment.NewLine.ToCharArray, StringSplitOptions.RemoveEmptyEntries)
        For i As Integer = 0 To certifiedInstruments.Length - 1
            If String.Equals(certifiedInstruments(i).Trim, isr.Core.HashExtensions.HashExtensionMethods.ToBase64Hash($"{serialNumber}{salt}")) Then
                If i < releasedInstruments.Length Then
                    result = releasedInstruments(i)
                End If
                Exit For
            End If
        Next
        Return result
    End Function

#End Region

End Class
