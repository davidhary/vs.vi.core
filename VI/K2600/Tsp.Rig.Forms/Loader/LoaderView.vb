Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

Imports isr.Core.EnumExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A loader view. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-04-24 </para>
''' </remarks>
Public Class LoaderView
    Inherits isr.VI.Facade.SplitVisaView

#Region " CONSTRUCTION AND CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the K2600 View and optionally releases the managed
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                Me.AssignDeviceThis(Nothing)
                If Me.ConnectorView IsNot Nothing Then Me.ConnectorView.Dispose() : Me._ConnectorView = Nothing
                If Me.TopHeader IsNot Nothing Then Me.TopHeader.Dispose() : Me._TopHeader = Nothing
                If Me.FirmwareLoaderView IsNot Nothing Then Me.FirmwareLoaderView.Dispose() : Me._FirmwareLoaderView = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " TSP RIG DEVICE "

    ''' <summary> Gets or sets reference to the TSP device accessing the TTM Driver. </summary>
    ''' <value> The tsp device. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property TspDevice() As isr.VI.Tsp.Rig.TspDevice

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As isr.VI.Tsp.Rig.TspDevice)
        If Me.TspDevice IsNot Nothing Then
            RemoveHandler Me.TspDevice.PropertyChanged, AddressOf Me.TspDevice_PropertyChanged
            Me.AssignTalker(Nothing)
            Me._TspDevice = Nothing
            Me.AssignTopHeader(Nothing)
            Me.AddFirmwareLoaderNode(Nothing)
            Me.AddConnectorNode(Nothing)
        End If
        Me._TspDevice = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.TspDevice.Talker)
            AddHandler Me.TspDevice.PropertyChanged, AddressOf Me.TspDevice_PropertyChanged
            Me.AssignTopHeader(New LoaderTopHeader)
            Me.AddProgramInfoNode()
            Me.AddFirmwareLoaderNode(New FirmwareLoaderView)
            Me.AddConnectorNode(New isr.VI.Facade.ConnectorView)
            Me.TopHeader.BindVisaSessionBase(TryCast(value, VI.VisaSessionBase))
            Me.ConnectorView.BindVisaSessionBase(TryCast(value, VI.VisaSessionBase))
            Me.FirmwareLoaderView.AssignDevice(value)
            Me.AssignScriptManager(Me.FirmwareLoaderView.ScriptManager)
            Me.HandlePropertyChanged(value, NameOf(isr.VI.Tsp.Rig.TspDevice.Enabled))
        End If
        Me.BindVisaSessionBase(TryCast(value, VI.VisaSessionBase))
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Overloads Sub AssignDevice(ByVal value As isr.VI.Tsp.Rig.TspDevice)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Raises the system. component model. property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Specifies the object where the call originated. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overloads Sub HandlePropertyChanged(ByVal sender As VI.Tsp.Rig.TspDevice, ByVal propertyName As String)
        If sender IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            Select Case propertyName
                Case NameOf(VI.Tsp.Rig.TspDevice.Enabled)
                    Me.StatusLabel.Text = If(sender.Enabled, $"Ready to Connect to Instrument at {sender.CandidateResourceName}", "Emulation Mode: Ready to 'Measure'")
                    ' the smu unit name seems unnecessary for loading the script.
                    Me.TopHeader.SourceMeasureUnitName = "smua"
                    ' not clear if this was set in the legacy code.
                    Me.TopHeader.LastLoadDate = "2019-04-01"
            End Select
        End If
    End Sub

    ''' <summary> Event handler. Called by _MasterDevice for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TspDevice_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling {GetType(VI.Tsp.Rig.TspDevice)}.{e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TspDevice_PropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.Tsp.Rig.TspDevice), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DEVICE EVENTS "

    ''' <summary>
    ''' Event handler. Called upon device opening so as to instantiated all subsystems.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overrides Sub DeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.DeviceOpening(sender, e)
        Me.PublishVerbose($"Opening access to {Me.ResourceName};. ")
    End Sub

    ''' <summary>
    ''' Event handler. Called after the device opened and all subsystems were defined.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="T:System.Object" /> instance of this
    '''                       <see cref="T:System.Windows.Forms.Control" /> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.DeviceOpened(sender, e)
        Dim activity As String = String.Empty
        Try
            activity = "displaying top header resource name" : Me.PublishVerbose($"{activity};. ")
            ' display the resource name
            Me.TopHeader.ResourceName = Me.ResourceName
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " INITALIZING / INITIALIZED  "

    ''' <summary> Device initializing. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Protected Overrides Sub DeviceInitializing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.DeviceInitializing(sender, e)
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overrides Sub DeviceInitialized(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.DeviceInitialized(sender, e)
    End Sub

#End Region

#Region " CLOSING / CLOSED "

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="T:System.Object" /> instance of this
    '''                                             <see cref="T:System.Windows.Forms.Control" /> 
    ''' </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub DeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.DeviceClosing(sender, e)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="T:System.Object" /> instance of this
    '''                                          <see cref="T:System.Windows.Forms.Control" /> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.DeviceClosed(sender, e)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        End If
    End Sub

#End Region

#Region " SCRIPT MANAGER "

    ''' <summary> Gets or sets reference to the TSP Script Manager. </summary>
    ''' <value> The tsp Script Manager. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property ScriptManager() As isr.VI.Tsp.Rig.TtmScriptManager

    ''' <summary> Assigns the Script Manager and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignScriptManagerThis(ByVal value As isr.VI.Tsp.Rig.TtmScriptManager)
        If Me.ScriptManager IsNot Nothing Then
            RemoveHandler Me.ScriptManager.PropertyChanged, AddressOf Me.ScriptManager_PropertyChanged
            Me.AssignTalker(Nothing)
            Me._ScriptManager = Nothing
        End If
        Me._ScriptManager = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.ScriptManager.Talker)
            AddHandler Me.ScriptManager.PropertyChanged, AddressOf Me.ScriptManager_PropertyChanged
        End If
    End Sub

    ''' <summary> Assigns a ScriptManager. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignScriptManager(ByVal value As isr.VI.Tsp.Rig.TtmScriptManager)
        Me.AssignScriptManagerThis(value)
    End Sub

    ''' <summary> Raises the system. component model. property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As VI.Tsp.Rig.ScriptManager, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If sender IsNot Nothing AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
            Select Case e.PropertyName
            End Select
        End If
    End Sub

    ''' <summary> Event handler. Called by _ScriptManager for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ScriptManager_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling {GetType(VI.Tsp.Rig.ScriptManager)}.{e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ScriptManager_PropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.Tsp.Rig.ScriptManager), e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TOP HEADER "

    ''' <summary> Gets or sets the top header. </summary>
    ''' <value> The top header. </value>
    Private ReadOnly Property TopHeader As LoaderTopHeader

    ''' <summary> Assign top header. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="topHeader"> The top header. </param>
    Private Sub AssignTopHeader(ByVal topHeader As LoaderTopHeader)
        If Me.TopHeader IsNot Nothing Then
            Me.RemoveBinding(Me.TopHeader, NameOf(LoaderTopHeader.IsCheckAlerts), Me.TraceMessagesBox, NameOf(isr.Core.Forma.TraceMessagesBox.AlertsAnnounced))
            Me.RemoveBinding(Me.TopHeader, NameOf(LoaderTopHeader.ResourceName), Me, NameOf(LoaderView.ResourceName))
        End If
        Me._TopHeader = topHeader
        If topHeader IsNot Nothing Then
            Dim binding As Binding = Me.AddBinding(Me.TopHeader, NameOf(LoaderTopHeader.IsCheckAlerts), Me.TraceMessagesBox, NameOf(isr.Core.Forma.TraceMessagesBox.AlertsAnnounced))
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
            Me.AddBinding(Me.TopHeader, NameOf(LoaderTopHeader.ResourceName), Me, NameOf(LoaderView.ResourceName))
            MyBase.AddHeader(topHeader)
        End If
    End Sub

#End Region

#Region " CONNECTOR "

    ''' <summary> Gets or sets the connector view. </summary>
    ''' <value> The connector view. </value>
    Private ReadOnly Property ConnectorView As isr.VI.Facade.ConnectorView

    ''' <summary> Name of the connector node. </summary>
    Private _ConnectorNodeName As String = "Connect"

    ''' <summary> Gets or sets the name of the connector node. </summary>
    ''' <value> The name of the connector node. </value>
    Public Property ConnectorNodeName As String
        Get
            Return Me._ConnectorNodeName
        End Get
        Set(value As String)
            If Not String.Equals(Me.ConnectorNodeName, value) Then
                Me._ConnectorNodeName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Adds a connector node. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="connector"> The connector. </param>
    Private Sub AddConnectorNode(ByVal connector As isr.VI.Facade.ConnectorView)
        If Me.ConnectorView IsNot Nothing Then
            Me.RemoveNode(Me.ConnectorNodeName)
        End If
        Me._ConnectorView = connector
        If Me.ConnectorView IsNot Nothing Then
            Me.AddNode(Me.ConnectorNodeName, Me.ConnectorNodeName, Me.ConnectorView)
            Me.SelectNavigatorTreeViewNode(Me.ConnectorNodeName)
        End If
    End Sub

#End Region

#Region " FIRMWARE LOADER "

    ''' <summary> Gets or sets the firmware loader view. </summary>
    ''' <value> The firmware loader view. </value>
    Private ReadOnly Property FirmwareLoaderView As FirmwareLoaderView

    ''' <summary> Name of the firmware loader node. </summary>
    Private _FirmwareLoaderNodeName As String = "Firmware"

    ''' <summary> Gets or sets the name of the FirmwareLoader node. </summary>
    ''' <value> The name of the FirmwareLoader node. </value>
    Public Property FirmwareLoaderNodeName As String
        Get
            Return Me._FirmwareLoaderNodeName
        End Get
        Set(value As String)
            If Not String.Equals(Me.FirmwareLoaderNodeName, value) Then
                Me._FirmwareLoaderNodeName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Adds a firmware loader node. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="loaderView"> The loader view. </param>
    Private Sub AddFirmwareLoaderNode(ByVal loaderView As FirmwareLoaderView)
        If Me.FirmwareLoaderView IsNot Nothing Then
            RemoveHandler Me.FirmwareLoaderView.PropertyChanged, AddressOf Me.FirmwareLoaderViewPropertyChanged
            Me.RemoveNode(Me.FirmwareLoaderNodeName)
        End If
        Me._FirmwareLoaderView = loaderView
        If Me.FirmwareLoaderView IsNot Nothing Then
            AddHandler Me.FirmwareLoaderView.PropertyChanged, AddressOf Me.FirmwareLoaderViewPropertyChanged
            Me.AddNode(Me.FirmwareLoaderNodeName, Me.FirmwareLoaderNodeName, Me.FirmwareLoaderView)
            Me.SelectNavigatorTreeViewNode(Me.FirmwareLoaderNodeName)
            Me.HandlePropertyChanged(loaderView, NameOf(FirmwareLoaderView.UpdateRequired))
            Me.HandlePropertyChanged(loaderView, NameOf(FirmwareLoaderView.VersionCompareStatus))
            Me.HandlePropertyChanged(loaderView, NameOf(FirmwareLoaderView.FirmwareInstalledVersion))
            Me.HandlePropertyChanged(loaderView, NameOf(FirmwareLoaderView.FirmwareReleasedVersion))
        End If
    End Sub

    ''' <summary> Raises the system. component model. property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Specifies the object where the call originated. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As isr.VI.Tsp.Rig.Forms.FirmwareLoaderView, ByVal propertyName As String)
        If sender IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            Select Case propertyName
                Case NameOf(isr.VI.Tsp.Rig.Forms.FirmwareLoaderView.UpdateRequired)
                    If sender.UpdateRequired Then Me.PublishInfo("Updated required;. ")
                Case NameOf(isr.VI.Tsp.Rig.Forms.FirmwareLoaderView.VersionCompareStatus)
                    Me.StatusLabel.Text = sender.VersionCompareStatus.Description
                Case NameOf(isr.VI.Tsp.Rig.Forms.FirmwareLoaderView.FirmwareInstalledVersion)
                    Me.TopHeader.InstalledFirmwareVersion = sender.FirmwareInstalledVersion
                Case NameOf(isr.VI.Tsp.Rig.Forms.FirmwareLoaderView.FirmwareReleasedVersion)
                    Me.TopHeader.ReleasedFirmwareVersion = sender.FirmwareReleasedVersion
            End Select
        End If
    End Sub

    ''' <summary> Event handler. Called by _FirmwareLoader for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FirmwareLoaderViewPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"Handling {NameOf(isr.VI.Tsp.Rig.Forms.FirmwareLoaderView)}.{e?.PropertyName} change"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.FirmwareLoaderViewPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.Tsp.Rig.Forms.FirmwareLoaderView), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " PROGRAM INFO "

    ''' <summary> Adds program information node. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AddProgramInfoNode()
        Me._AboutProgramRichTextBox = New RichTextBox
        Me.AddNode(Me.ProgramInfoNodeName, Me.ProgramInfoNodeName, Me.AboutProgramRichTextBox)
        Me.DisplayProgramInfo(Me.Font)
        Me.SelectNavigatorTreeViewNode(Me.ProgramInfoNodeName)
    End Sub

    ''' <summary> Name of the program information node. </summary>
    Private _ProgramInfoNodeName As String = "About"

    ''' <summary> Gets or sets the name of the program information node. </summary>
    ''' <value> The name of the program information node. </value>
    Public Property ProgramInfoNodeName As String
        Get
            Return Me._ProgramInfoNodeName
        End Get
        Set(value As String)
            If Not String.Equals(Me.ProgramInfoNodeName, value) Then
                Me._ProgramInfoNodeName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the about program rich text box. </summary>
    ''' <value> The about program rich text box. </value>
    Private ReadOnly Property AboutProgramRichTextBox As RichTextBox

        Private WithEvents ProgramInfo As New isr.Core.ProgramInfo

    ''' <summary> Displays a program information described by contentsFont. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="contentsFont"> The contents font. </param>
    Private Sub DisplayProgramInfo(ByVal contentsFont As Font)
        Dim headerFont As New Font(contentsFont, FontStyle.Bold)
        Me.ProgramInfo.BuildContents(headerFont, contentsFont)
        Me.ProgramInfo.NotifyRefreshRequested()
    End Sub

    ''' <summary> Refresh program information display. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="box"> The box control. </param>
    Private Sub RefreshProgramInfoDisplay(ByVal box As RichTextBox)
        box.Rtf = String.Empty
        For Each line As isr.Core.ProgramInfoLine In Me.ProgramInfo.Lines
            box.SelectionStart = box.TextLength
            box.SelectionFont = line.Font
            box.AppendText(line.Text)
            box.AppendText(Environment.NewLine)
        Next
    End Sub

    ''' <summary> Appends a program information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="contentsFont"> The contents font. </param>
    Private Sub AppendProgramInfo(ByVal contentsFont As Font)
        Dim headerFont As New Font(contentsFont, FontStyle.Bold)

        Me.ProgramInfo.AppendLine("Product Application-Data Folder:", headerFont)
        Dim fi As New System.IO.FileInfo(New isr.Core.MyAssemblyInfo(My.Application.Info).BuildApplicationConfigFilePath(False, 2))
        Me.ProgramInfo.AppendLine(fi.DirectoryName, contentsFont)
        Me.ProgramInfo.AppendLine("", contentsFont)

        Dim logFile As String = My.MyLibrary.Logger.DefaultFileLogWriter.FullLogFileName
        Me.ProgramInfo.AppendLine("Application log file:", headerFont)
        Me.ProgramInfo.AppendLine(logFile, contentsFont)
        Dim traceSourceFile As String = My.MyLibrary.Logger.DefaultFileLogWriter.FullLogFileName
        If Not String.Equals(logFile, traceSourceFile) Then
            Me.ProgramInfo.AppendLine("Application log trace source file:", headerFont)
            Me.ProgramInfo.AppendLine(traceSourceFile, contentsFont)
        End If
        Me.ProgramInfo.AppendLine(String.Empty, contentsFont)
    End Sub

    ''' <summary> Program information program information requested. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ProgramInfo_ProgramInfoRequested(sender As Object, e As System.EventArgs) Handles ProgramInfo.ProgramInfoRequested
        Dim activity As String = String.Empty
        Try
            activity = $"Handling {GetType(isr.Core.ProgramInfo)}.{NameOf(Me.ProgramInfo.ProgramInfoRequested)} event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.EventArgs)(AddressOf Me.ProgramInfo_ProgramInfoRequested), New Object() {sender, e})
            Else
                Me.AppendProgramInfo(Me.Font)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Program information refresh requested. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ProgramInfo_RefreshRequested(sender As Object, e As System.EventArgs) Handles ProgramInfo.RefreshRequested
        Dim activity As String = String.Empty
        Try
            activity = $"Handling {GetType(isr.Core.ProgramInfo)}.{NameOf(Me.ProgramInfo.RefreshRequested)} event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.EventArgs)(AddressOf Me.ProgramInfo_RefreshRequested), New Object() {sender, e})
            Else
                Me.RefreshProgramInfoDisplay(Me.AboutProgramRichTextBox)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " NAVIGATION "

    ''' <summary> After node selected. </summary>
    ''' <remarks> David, 2020-09-03. </remarks>
    ''' <param name="e"> Tree view event information. </param>
    Protected Overrides Sub AfterNodeSelected(ByVal e As System.Windows.Forms.TreeViewEventArgs)
        MyBase.AfterNodeSelected(e)
        Select Case e.Node.Name
            Case Me.ProgramInfoNodeName
                Me.ProgramInfo.NotifyRefreshRequested()
        End Select
    End Sub

#End Region

#Region " SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("TSP Rig UI Settings Editor", My.MySettings.Default)
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
