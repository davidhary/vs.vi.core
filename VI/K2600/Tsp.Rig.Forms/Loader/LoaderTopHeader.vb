Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions

''' <summary> Displays current test results. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2006-06-13, 1.15.2355.x. </para>
''' </remarks>
Public Class LoaderTopHeader
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._PassedPicture.Visible = False
        Me._PassedPicture.Invalidate()

        ' Add any initialization after the InitializeComponent() call
        ' onInstantiate()
        Me.ClearDisplayThis()
        Me.InitializingComponents = False
    End Sub

#End Region

#Region " I RESETTABLE IMPLEMENTATION "

    ''' <summary> Clears the display. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ClearDisplay()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.ClearDisplayThis))
        Else
            Me.ClearDisplayThis()
        End If
    End Sub

    ''' <summary> Set internal value to match the display state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ClearDisplayThis()
        Me.InstalledFirmwareVersion = String.Empty
        Me.IsCheckAlerts = False
        Me.ResourceName = String.Empty
        Me.ReleasedFirmwareVersion = String.Empty
        Me.SourceMeasureUnitName = String.Empty
        Me.LastLoadDate = String.Empty
    End Sub

#End Region

#Region " visa session base (device base) "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VI.VisaSessionBase

    ''' <summary> Bind visa session base. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub BindVisaSessionBaseThis(ByVal value As VI.VisaSessionBase)
        If Me._VisaSessionBase IsNot Nothing Then
            RemoveHandler Me.VisaSessionBase.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged
            Me.AssignTalker(Nothing)
            Me._VisaSessionBase = Nothing
        End If
        Me._VisaSessionBase = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.VisaSessionBase.Talker)
            AddHandler Me.VisaSessionBase.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged
            ' Me.VisaSessionBase.SessionFactory.CandidateResourceName = isr.VI.Ttm.My.Settings.ResourceName
            Me.HandlePropertyChanged(Me.VisaSessionBase.SessionFactory, NameOf(Me.VisaSessionBase.SessionFactory.IsOpen))
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Overridable Sub BindVisaSessionBase(ByVal value As VI.VisaSessionBase)
        Me.BindVisaSessionBaseThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.VisaSessionBase.ResourceNameCaption} reading service request"
        Try
            Me.VisaSessionBase.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#Region " SESSION FACTORY "

    ''' <summary> Handles the property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Specifies the object where the call originated. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Sub HandlePropertyChanged(ByVal sender As SessionFactory, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(SessionFactory.ValidatedResourceName)
                Me.ResourceName = If(sender.IsOpen, sender.ValidatedResourceName, String.Empty)
            Case NameOf(SessionFactory.IsOpen)
                Me.ResourceName = If(sender.IsOpen, sender.ValidatedResourceName, String.Empty)
        End Select
    End Sub

    ''' <summary> Session factory property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionFactoryPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling session factory {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.SessionFactoryPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SessionFactory), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#End Region

#Region " DISPLAY PROPERTIES "

    ''' <summary> Displays the resource name. </summary>
    ''' <value> The Last Load Date. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ResourceName() As String
        Get
            Return Me._ResourceTextBox.Text
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.ResourceName, value) Then
                If Me.InvokeRequired Then
                    Me.BeginInvoke(CType(Sub() Me._ResourceTextBox.Text = value, MethodInvoker))
                Else
                    Me._ResourceTextBox.Text = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Displays the Last Load Date. </summary>
    ''' <value> The Last Load Date. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LastLoadDate() As String
        Get
            Return Me._LastLoadDateTextBox.Text
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.LastLoadDate, value) Then
                If Me.InvokeRequired Then
                    Me.BeginInvoke(CType(Sub() Me._LastLoadDateTextBox.Text = value, MethodInvoker))
                Else
                    Me._LastLoadDateTextBox.Text = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Displays the Installed Firmware Version. </summary>
    ''' <value> The initial resistance caption. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property InstalledFirmwareVersion() As String
        Get
            Return Me._InstalledFirmwareVersionTextBox.Text
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.InstalledFirmwareVersion, value) Then
                If Me.InvokeRequired Then
                    Me.BeginInvoke(CType(Sub() Me._InstalledFirmwareVersionTextBox.Text = value, MethodInvoker))
                Else
                    Me._InstalledFirmwareVersionTextBox.Text = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Turns on the alerts message. </summary>
    ''' <value> The is check alerts. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property IsCheckAlerts() As Boolean
        Get
            Return Me._FailedPicture.Visible
        End Get
        Set(ByVal value As Boolean)
            If Me.IsCheckAlerts <> value Then
                If Me.InvokeRequired Then
                    Me.BeginInvoke(CType(Sub() Me._FailedPicture.Visible = value, MethodInvoker))
                Else
                    Me._FailedPicture.Visible = value
                    Me._FailedPicture.Invalidate()
                End If
                If value AndAlso My.MySettings.Default.PlayAlertSound Then
                    My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Exclamation)
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The released firmware version. </summary>
    Private _ReleasedFirmwareVersion As String

    ''' <summary> Gets or sets the Released Firmware Version. </summary>
    ''' <value> The Released Firmware Version. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ReleasedFirmwareVersion() As String
        Get
            Return Me._ReleasedFirmwareVersion
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.ReleasedFirmwareVersion, value) Then
                Me._ReleasedFirmwareVersion = value
                If Me.InvokeRequired Then
                    Me.BeginInvoke(CType(Sub() Me._ReleasedFirmwareVersionTextBox.Text = value, MethodInvoker))
                Else
                    Me._ReleasedFirmwareVersionTextBox.Text = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Name of the source measure unit. </summary>
    Private _SourceMeasureUnitName As String = String.Empty

    ''' <summary> Gets or sets the Source Measure Unit Name. </summary>
    ''' <value> The Source Measure Unit Name. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SourceMeasureUnitName() As String
        Get
            Return Me._SourceMeasureUnitName
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.SourceMeasureUnitName, value) Then
                Me._SourceMeasureUnitName = value
                If Me.InvokeRequired Then
                    Me.BeginInvoke(CType(Sub() Me._SourceMeasureUnitTextBox.Text = value, MethodInvoker))
                Else
                    Me._SourceMeasureUnitTextBox.Text = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the header title. </summary>
    ''' <value> The main title. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MainTitle() As String
        Get
            Return Me._TitleLabel.Text
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.MainTitle) Then
                If Me.InvokeRequired Then
                    Me.BeginInvoke(CType(Sub() Me._TitleLabel.Text = value, MethodInvoker))
                Else
                    Me._TitleLabel.Text = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Event handler. Called by failedPicture for double click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FailedPicture_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FailedPicture.DoubleClick
        Me._FailedPicture.Visible = False
    End Sub

    ''' <summary> Event handler. Called by passedPicture for double click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PassedPicture_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PassedPicture.DoubleClick
        Me._PassedPicture.Visible = False
    End Sub

    ''' <summary> Displays the version information panel. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenAboutBoxButton_Click(sender As Object, e As EventArgs) Handles _OpenAboutBoxButton.Click
        If Me.InitializingComponents Then Return
        Using aboutScreen As New isr.Core.About
            aboutScreen.TopMost = True
            Dim parentForm As Windows.Forms.Form = TryCast(Me.Parent, Windows.Forms.Form)
            If parentForm Is Nothing AndAlso Me.Parent.Parent IsNot Nothing Then
                parentForm = TryCast(Me.Parent.Parent, Windows.Forms.Form)
            End If
            If parentForm IsNot Nothing Then
                aboutScreen.Icon = parentForm.Icon
            End If
            aboutScreen.ShowDialog(System.Reflection.Assembly.GetExecutingAssembly,
               "Integrated Scientific Resources, Inc.", "", "TTM", "")
        End Using
    End Sub

    ''' <summary> Opens device settings button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenDeviceSettingsButton_Click(sender As Object, e As EventArgs) Handles _OpenDeviceSettingsButton.Click
        If Me.InitializingComponents Then Return
        isr.VI.Tsp.Rig.TspDevice.OpenSettingsEditor()
    End Sub

    ''' <summary> Opens user interface settings button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenUserInterfaceSettingsButton_Click(sender As Object, e As EventArgs) Handles _OpenUserInterfaceSettingsButton.Click
        If Me.InitializingComponents Then Return
        LoaderView.OpenSettingsEditor()
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
