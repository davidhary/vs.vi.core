Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> Firmware loader control. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-04-12 </para>
''' </remarks>
Public Class FirmwareLoaderView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        ' indicates the versions are current.
        Me.VersionCompareStatus = VersionCompare.Current
        Me.InitializingComponents = False
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me._TspDevice = Nothing
                If disposing AndAlso Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " TSP RIG DEVICE "

    ''' <summary> Gets or sets reference to the TSP device accessing the TTM Driver. </summary>
    ''' <value> The tsp device. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property TspDevice() As isr.VI.Tsp.Rig.TspDevice

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As isr.VI.Tsp.Rig.TspDevice)
        If Me._VisaSessionBase IsNot Nothing Then
            RemoveHandler Me.TspDevice.PropertyChanged, AddressOf Me.TspDevice_PropertyChanged
            Me.AssignScriptManager(Nothing)
            Me.AssignTalker(Nothing)
            Me._VisaSessionBase = Nothing
        End If
        Me._TspDevice = value
        If value IsNot Nothing Then
            Me.AssignScriptManager(New VI.Tsp.Rig.TtmScriptManager(value))
            Me.AssignTalker(Me.TspDevice.Talker)
            AddHandler Me.TspDevice.PropertyChanged, AddressOf Me.TspDevice_PropertyChanged
        End If
        Me.BindVisaSessionBase(TryCast(value, VI.VisaSessionBase))
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As isr.VI.Tsp.Rig.TspDevice)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Executes the session factory property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub HandlePropertyChanged(ByVal sender As VI.Tsp.Rig.TspDevice, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If sender IsNot Nothing AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
            Select Case e.PropertyName
            End Select
        End If
    End Sub

    ''' <summary> Event handler. Called by _MasterDevice for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TspDevice_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling {GetType(VI.Tsp.Rig.TspDevice)}.{e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TspDevice_PropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.Tsp.Rig.TspDevice), e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " visa session base (device base) "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VI.VisaSessionBase

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub BinVisaSessionBaseThis(ByVal value As VI.VisaSessionBase)
        If Me._VisaSessionBase IsNot Nothing Then
            RemoveHandler Me.VisaSessionBase.Opening, AddressOf Me.DeviceOpening
            RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.DeviceOpened
            RemoveHandler Me.VisaSessionBase.Closing, AddressOf Me.DeviceClosing
            RemoveHandler Me.VisaSessionBase.Closed, AddressOf Me.DeviceClosed
            RemoveHandler Me.VisaSessionBase.Initialized, AddressOf Me.DeviceInitialized
            RemoveHandler Me.VisaSessionBase.Initializing, AddressOf Me.DeviceInitializing
            RemoveHandler Me.VisaSessionBase.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged
            Me.AssignTalker(Nothing)
            Me._VisaSessionBase = Nothing
        End If
        Me._VisaSessionBase = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.VisaSessionBase.Talker)
            AddHandler Me.VisaSessionBase.Opening, AddressOf Me.DeviceOpening
            AddHandler Me.VisaSessionBase.Opened, AddressOf Me.DeviceOpened
            AddHandler Me.VisaSessionBase.Closing, AddressOf Me.DeviceClosing
            AddHandler Me.VisaSessionBase.Closed, AddressOf Me.DeviceClosed
            AddHandler Me.VisaSessionBase.Initialized, AddressOf Me.DeviceInitialized
            AddHandler Me.VisaSessionBase.Initializing, AddressOf Me.DeviceInitializing
            AddHandler Me.VisaSessionBase.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged
            ' Me.VisaSessionBase.SessionFactory.CandidateResourceName = isr.VI.Ttm.My.Settings.ResourceName
            If Me.VisaSessionBase.IsDeviceOpen Then
                Me.DeviceOpened(Me.VisaSessionBase, System.EventArgs.Empty)
            Else
                Me.DeviceClosed(Me.VisaSessionBase, System.EventArgs.Empty)
            End If
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub BindVisaSessionBase(ByVal value As VI.VisaSessionBase)
        Me.BinVisaSessionBaseThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.VisaSessionBase.ResourceNameCaption} reading service request"
        Try
            Me.VisaSessionBase.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#Region " SESSION FACTORY "

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    Public Property ResourceName As String
        Get
            Return Me.VisaSessionBase.SessionFactory.CandidateResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourceName) Then
                Me.VisaSessionBase.SessionFactory.CandidateResourceName = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Search Pattern of the resource. </summary>
    ''' <value> The Search Pattern of the resource. </value>
    Public Property ResourceFilter As String
        Get
            Return Me.VisaSessionBase.SessionFactory.ResourcesFilter
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourceFilter) Then
                Me.VisaSessionBase.SessionFactory.ResourcesFilter = value
            End If
        End Set
    End Property

    ''' <summary> Executes the session factory property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Specifies the object where the call originated. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal sender As VI.SessionFactory, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.SessionFactory.ValidatedResourceName)
                If sender.IsOpen Then
                    Me.PublishInfo($"Resource connected;. {sender.ValidatedResourceName}")
                Else
                    Me.PublishInfo($"Resource locate;. {sender.ValidatedResourceName}")
                End If
            Case NameOf(VI.SessionFactory.CandidateResourceName)
                If Not sender.IsOpen Then
                    Me.PublishInfo($"Candidate resource;. {sender.ValidatedResourceName}")
                End If
        End Select
    End Sub

    ''' <summary> Session factory property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionFactoryPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling {GetType(VI.SessionFactory)}.{e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.SessionFactoryPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SessionFactory), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DEVICE EVENTS "

    ''' <summary>
    ''' Event handler. Called upon device opening so as to instantiated all subsystems.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Sub DeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary>
    ''' Event handler. Called after the device opened and all subsystems were defined.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this device. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim outcome As TraceEventType = TraceEventType.Information
        If Me.VisaSessionBase.Session.Enabled And Not Me.VisaSessionBase.Session.IsSessionOpen Then outcome = TraceEventType.Warning
        Me.Publish(outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ",
                   Me.VisaSessionBase.ResourceTitleCaption, Me.VisaSessionBase.Session.Enabled.GetHashCode,
                   Me.VisaSessionBase.Session.IsDeviceOpen.GetHashCode, Me.VisaSessionBase.Session.IsSessionOpen.GetHashCode)
        Dim activity As String = String.Empty
        Try
            activity = "finding firmware"

            ' check if firmware is embedded in the Driver.
            If Me.ScriptManager.FindFirmware() Then
                ' display the title.
                activity = "instrument displaying title"
                Me.TspDevice.DisplaySubsystem.DisplayTitle(Me.ScriptManager.FrameworkTitle, Me.ScriptManager.AuthorTitle)
            Else
                activity = "instrument restoring display"
                Me.TspDevice.DisplaySubsystem.RestoreDisplay()
            End If

            activity = "validating script catalog"
            Me.ValidateSavedScriptsCatalog(True, True)

        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " OPENING / OPEN "

    ''' <summary>
    ''' Attempts to open a session to the device using the specified resource name.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The title. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryOpenDeviceSession(ByVal resourceName As String, ByVal resourceTitle As String) As (Success As Boolean, Details As String)
        Return Me.VisaSessionBase.TryOpenSession(resourceName, resourceTitle)
    End Function

#End Region

#Region " INITALIZING / INITIALIZED  "

    ''' <summary> Device initializing. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Cancel event information. </param>
    Protected Overridable Sub DeviceInitializing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceInitialized(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

#End Region

#Region " CLOSING / CLOSED "

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of the device. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
                activity = $"Disabling meter timer" : Me.PublishVerbose($"{activity};. ")
                Me.DisableFirmwareControls()
                activity = $"Disconnecting from {Me.ResourceName}" : Me.PublishInfo($"{activity};. ")
                If Me.VisaSessionBase.Session IsNot Nothing Then Me.VisaSessionBase.Session.DisableServiceRequestEventHandler()
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of the device. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.VisaSessionBase IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
                If Me.VisaSessionBase.Session.IsSessionOpen Then
                    Me.PublishWarning($"{Me.VisaSessionBase.Session.ResourceNameCaption} closed but session still open;. ")
                ElseIf Me.VisaSessionBase.Session.IsDeviceOpen Then
                    Me.PublishWarning($"{Me.VisaSessionBase.Session.ResourceNameCaption} closed but emulated session still open;. ")
                Else
                    Me.PublishVerbose("Disconnected; Device access closed.")
                End If
                activity = $"disables controls" : Me.PublishVerbose($"{activity};. ")
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        Else
            Me.PublishInfo("Disconnected; Device disposed.")
        End If
    End Sub

#End Region

#End Region

#Region " SCRIPT MANAGER "

    ''' <summary> Gets or sets reference to the TSP Script Manager. </summary>
    ''' <value> The tsp Script Manager. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property ScriptManager() As isr.VI.Tsp.Rig.TtmScriptManager

    ''' <summary> Assigns the Script Manager and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignScriptManagerThis(ByVal value As isr.VI.Tsp.Rig.TtmScriptManager)
        If Me.ScriptManager IsNot Nothing Then
            RemoveHandler Me.ScriptManager.PropertyChanged, AddressOf Me.ScriptManager_PropertyChanged
            Me.AssignTalker(Nothing)
            Me._ScriptManager = Nothing
        End If
        Me._ScriptManager = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.ScriptManager.Talker)
            AddHandler Me.ScriptManager.PropertyChanged, AddressOf Me.ScriptManager_PropertyChanged
        End If
    End Sub

    ''' <summary> Assigns a ScriptManager. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignScriptManager(ByVal value As isr.VI.Tsp.Rig.TtmScriptManager)
        Me.AssignScriptManagerThis(value)
    End Sub

    ''' <summary> Executes the session factory property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub HandlePropertyChanged(ByVal sender As VI.Tsp.Rig.ScriptManager, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If sender IsNot Nothing AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
            Select Case e.PropertyName
            End Select
        End If
    End Sub

    ''' <summary> Event handler. Called by _ScriptManager for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ScriptManager_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling {GetType(VI.Tsp.Rig.ScriptManager)}.{e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ScriptManager_PropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, isr.VI.Tsp.Rig.ScriptManager), e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CHECK FIRMWARE "

    ''' <summary> Gets or sets the installed firmware version. </summary>
    ''' <value> The installed firmware version. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FirmwareInstalledVersion As String
        Get
            Return Me._InstalledFirmwareVersionTextBox.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FirmwareInstalledVersion) Then
                Me._InstalledFirmwareVersionTextBox.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Released firmware version. </summary>
    ''' <value> The Released firmware version. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FirmwareReleasedVersion As String
        Get
            Return Me._ReleasedFirmwareVersionTextBox.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FirmwareReleasedVersion) Then
                Me._ReleasedFirmwareVersionTextBox.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the firmware installed status. </summary>
    ''' <value> The firmware installed status. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FirmwareInstalledStatus As String
        Get
            Return Me._FirmwareStatusTextBox.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.FirmwareInstalledStatus) Then
                Me._FirmwareStatusTextBox.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Validates the catalog of saved scripts. Turns on <see cref="UpdateRequired">update
    ''' sentinel</see> if the catalog needs to be updated.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="refreshScriptCatalog"> Specifies the condition for updating the catalog of saved
    '''                                     scripts before checking the status of these scripts. 
    ''' </param>
    ''' <param name="updateDisplay">        true to update the instrument display. </param>
    Public Sub ValidateSavedScriptsCatalog(ByVal refreshScriptCatalog As Boolean, ByVal updateDisplay As Boolean)

        Dim firmwareStatusBuilder As New System.Text.StringBuilder
        Dim installedVersion As String = "<not found>"
        Dim releasedVersion As String = "<unknown>"

        If Me.TspDevice.IsDeviceOpen Then

            Dim panelMessage As String = String.Empty
            If Debugger.IsAttached() AndAlso Me.ScriptManager.Scripts.RequiresReadParseWrite() Then

                Me.LoadEnabled = True
                Me._LoadFirmwareButton.Text = "PARSE SCRIPTS"
                Me.UpdateRequired = False

                firmwareStatusBuilder.AppendLine("Scripts are available for parsing. Click PARSE SCRIPTS.")

                ' set version status to indicate that scripts need to be parsed
                Me.VersionCompareStatus = VersionCompare.ParseScripts

            ElseIf Me.ScriptManager.AnyLegacyScriptExists() Then

                Me.DeleteEnabled = Me.TspDevice.IsDeviceOpen
                Me.LoadEnabled = False
                Me.SaveEnabled = False
                Me.UpdateRequired = False

                ' set version status to indicate that legacy scripts must be deleted.
                Me.VersionCompareStatus = VersionCompare.DeleteLegacyScripts

                firmwareStatusBuilder.AppendLine("Instrument has legacy firmware loaded. These must be deleted first.")
                panelMessage = "Delete Legacy F/W"

            ElseIf String.IsNullOrWhiteSpace(Me.TspDevice.StatusSubsystem.SerialNumberReading) Then

                firmwareStatusBuilder.AppendLine("Instrument serial number is empty. Contact developer--Error at Firmware Loader Control Base line 196.")
                panelMessage = "Instrument serial number is empty"

                ' Check if instrument serial number is listed in the instrument resource.
            ElseIf Me.TspDevice.AccessSubsystem.CertifiedInstruments.IndexOf(
                      isr.Core.HashExtensions.HashExtensionMethods.ToBase64Hash(Me.TspDevice.StatusSubsystem.SerialNumberReading),
                      StringComparison.Ordinal) < 0 OrElse
                  String.IsNullOrWhiteSpace(Me.TspDevice.AccessSubsystem.ReleaseValue(Me.TspDevice.StatusSubsystem.SerialNumberReading,
                                                                                      Me.TspDevice.AccessSubsystem.Salt)) Then

                Me.PublishWarning(Me.TspDevice.StatusSubsystem.NewProgramRequired)
                firmwareStatusBuilder.AppendLine(Me.TspDevice.StatusSubsystem.NewProgramRequired)

                panelMessage = "GET NEW TTM PROGRAM"

                Me.UpdateRequired = True

            Else

                ' allow loading if any firmware does not exist.
                Me.LoadEnabled = Not Me.ScriptManager.AllScriptsExist()
                Me.DeleteEnabled = False
                Me.SaveEnabled = False

                If Me.ScriptManager.AnyScriptExists() Then

                    ' allow deleting if any firmware exists.
                    Me.DeleteEnabled = True

                    ' allow saving if script loaded but not saved.
                    Me.SaveEnabled = Not Me.ScriptManager.AllScriptsSaved(refreshScriptCatalog)

                End If

                Me.UpdateRequired = False

                ' check if firmware is embedded in the instrument
                If Me.ScriptManager.FindSupportFirmware() Then

                    If Me.ScriptManager.FindFirmware() Then

                        If updateDisplay Then
                            ' display the firmware title.
                            Me.TspDevice.DisplaySubsystem.DisplayTitle(Me.ScriptManager.FrameworkTitle, Me.ScriptManager.AuthorTitle)
                        End If

                        ' read the firmware versions
                        Me.ScriptManager.ReadFirmwareVersions()

                        ' update the released version
                        releasedVersion = Me.ScriptManager.FirmwareReleasedVersionGetter()

                        ' set loaded firmware
                        installedVersion = Me.ScriptManager.FirmwareVersionGetter()

                        ' check if we have the most current firmware
                        Dim compareStatus As Integer = String.Compare(Me.ScriptManager.FirmwareVersionGetter(),
                                                                      New System.Version(Me.ScriptManager.FirmwareReleasedVersionGetter()).ToString(3),
                                                                      Globalization.CultureInfo.CurrentCulture, Globalization.CompareOptions.OrdinalIgnoreCase)
                        If compareStatus = 0 Then
                            Me.VersionCompareStatus = VersionCompare.Current
                            firmwareStatusBuilder.AppendLine("Thermal Transient Meter firmware is up to date. No actions required.")
                        ElseIf compareStatus < 0 Then
                            Me.VersionCompareStatus = VersionCompare.UpdateRequired
                            firmwareStatusBuilder.AppendLine("Thermal Transient Meter firmware needs updating. Load new TTM Firmware.")
                        ElseIf compareStatus > 0 Then
                            Me.VersionCompareStatus = VersionCompare.NewVersionAvailable
                            firmwareStatusBuilder.AppendFormat($"Thermal Transient Meter firmware is old; A new release is available at {Me.ScriptManager.FtpAddress}")
                        End If

                    Else

                        ' set version status to indicate that the program needs to be updated.
                        Me.VersionCompareStatus = VersionCompare.UpdateRequired

                        firmwareStatusBuilder.AppendLine("Firmware not found in the Thermal Transient Meter. Use this application to load new Thermal Transient Meter firmware.")

                        panelMessage = "Load TTM Firmware"
                    End If

                Else

                    ' set version status to indicate that the program needs to be loaded
                    Me.VersionCompareStatus = VersionCompare.LoadFirmware
                    firmwareStatusBuilder.AppendLine("Firmware not found in the Thermal Transient Meter. Use this program to load Thermal Transient Meter firmware.")
                    panelMessage = "Load TTM Firmware"

                End If

                If Not String.IsNullOrWhiteSpace(Me.ScriptManager.LastFetchedSavedScripts) Then
                    firmwareStatusBuilder.Append("Thermal Transient Meter Firmware saved in non-volatile memory: ")
                    For Each s As String In Me.ScriptManager.LastFetchedAuthorScripts
                        firmwareStatusBuilder.AppendLine(s)
                    Next
                End If

            End If
            If updateDisplay AndAlso Not String.IsNullOrWhiteSpace(panelMessage) Then
                Me.TspDevice.DisplaySubsystem.ClearDisplay()
                Me.TspDevice.DisplaySubsystem.DisplayLine(1, panelMessage)
            End If
        Else
            Me.UpdateRequired = False
            Me.DisableFirmwareControls()
            releasedVersion = Me.ScriptManager.MeterFirmwareVersion
            installedVersion = "<unknown>"
            firmwareStatusBuilder.AppendLine("<not connected>")
        End If

        Me.FirmwareInstalledStatus = firmwareStatusBuilder.ToString
        Me.FirmwareInstalledVersion = installedVersion
        Me.FirmwareReleasedVersion = releasedVersion

    End Sub

#End Region

#Region " FIRMWARE MANAGEMENT "

    ''' <summary> True if update required. </summary>
    Private _UpdateRequired As Boolean

    ''' <summary> Gets or sets the update required. </summary>
    ''' <value> The update required. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property UpdateRequired As Boolean
        Get
            Return Me._UpdateRequired
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.UpdateRequired) Then
                Me._UpdateRequired = value
                Me.NotifyPropertyChanged()
                If value Then
                    Me.DisableFirmwareControls()
                End If
            End If
        End Set
    End Property

    ''' <summary> The version compare status. </summary>
    Private _VersionCompareStatus As VersionCompare

    ''' <summary> Gets or sets the version comparison status. </summary>
    ''' <value> The version compare status. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property VersionCompareStatus As VersionCompare
        Get
            Return Me._VersionCompareStatus
        End Get
        Set(value As VersionCompare)
            If Not value.Equals(Me.VersionCompareStatus) Then
                Me._VersionCompareStatus = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " CONTROLS "

    ''' <summary> Disables the firmware controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub DisableFirmwareControls()

        Me.DeleteEnabled = False
        Me.LoadEnabled = False
        Me.SaveEnabled = False

    End Sub

    ''' <summary> Enable or disable Delete. </summary>
    ''' <value> The Delete enabled sentinel. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property DeleteEnabled As Boolean
        Get
            Return Me._DeleteFirmwareButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._DeleteFirmwareButton.Enabled = value
        End Set
    End Property

    ''' <summary> Enable or disable load. </summary>
    ''' <value> The load enabled sentinel. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LoadEnabled As Boolean
        Get
            Return Me._LoadFirmwareButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._LoadFirmwareButton.Enabled = value
        End Set
    End Property

    ''' <summary> Enable or disable Save. </summary>
    ''' <value> The Save enabled sentinel. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SaveEnabled As Boolean
        Get
            Return Me._SaveFirmwareButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._SaveFirmwareButton.Enabled = value
        End Set
    End Property

    ''' <summary>
    ''' Event handler. Called by _deleteFirmwareButton for click events. Deletes existing firmware.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DeleteFirmwareButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _DeleteFirmwareButton.Click
        Dim activity As String = $"User action @{System.Reflection.MethodInfo.GetCurrentMethod.Name}"
        Me.PublishVerbose($"{activity};. ")

        Me.InfoProvider.Clear()
        activity = "Unloading TTM Firmware" : Me.PublishInfo($"{activity};. ")

        ' delete existing scripts if any
        Try

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.UpdateRequired = True
            Me.DisableFirmwareControls()

            ' delete script ignoring versions.
            Me.ScriptManager.DeleteUserScripts(Me.TspDevice.StatusSubsystem, Me.TspDevice.DisplaySubsystem)

        Catch ex As Exception

            Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._DeleteFirmwareButton, Core.Forma.InfoProviderLevel.Error, ex.Message)
            Try
                ' flush the buffer so a time out error that might leave stuff will clear the buffers.
                If Me.VisaSessionBase.IsSessionOpen Then Me.VisaSessionBase.Session.DiscardUnreadData()
            Catch
            End Try

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

            Me.ValidateSavedScriptsCatalog(False, True)

        End Try

    End Sub

    ''' <summary> Loads new firmware or updates existing firmware. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LoadFirmwareButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _LoadFirmwareButton.Click
        Dim activity As String = $"User action @{System.Reflection.MethodInfo.GetCurrentMethod.Name}"
        Me.PublishVerbose($"{activity};. ")
        Me.InfoProvider.Clear()

        activity = "Loading TTM Firmware" : Me.PublishInfo($"{activity};. ")
        Dim firmwareStatusMessage As String = String.Empty
        Try

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.UpdateRequired = True
            Me.DisableFirmwareControls()

            If Me.ScriptManager.Scripts.RequiresReadParseWrite() Then

                Me.LoadEnabled = False

                If Me.ScriptManager.TryReadParseUserScripts(Me.TspDevice.StatusSubsystem, Me.TspDevice.DisplaySubsystem) Then

                    activity = "Adding parsed scripts to resources" : Me.PublishInfo($"{activity};. ")
                    firmwareStatusMessage = "TTM Firmware parsed and saved to disk. Add parsed firmware to the application resource."

                Else

                    activity = "Failed reading or parsing TTM Firmware" : Me.PublishWarning($"{activity};. ")
                    Me.InfoProvider.Annunciate(Me._LoadFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Failed reading or parsing TTM Firmware")

                End If

            ElseIf Me.ScriptManager.UploadUserScripts(Me.TspDevice.StatusSubsystem, Me.TspDevice.DisplaySubsystem,
                                                                   Me.TspDevice.AccessSubsystem) Then

                ' check if code loaded.
                If Me.TspDevice.AccessSubsystem.Loaded() Then
                    activity = "Ready for measurements" : Me.PublishInfo($"{activity};. ")
                ElseIf Me.TspDevice.AccessSubsystem.Certify(Me.TspDevice.AccessSubsystem.ReleaseValue(Me.TspDevice.StatusSubsystem.SerialNumberReading,
                                                                                                      Me.TspDevice.AccessSubsystem.Salt)) Then
                    activity = "Ready for measurements" : Me.PublishInfo($"{activity};. ")
                Else
                    activity = $"Loading TTM Firmware failed;. Most likely, instrument '{Me.TspDevice.StatusSubsystem.Identity}' requires a firmware update."
                    Me.PublishWarning($"{activity};. ")
                    Me.InfoProvider.Annunciate(Me._LoadFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Loading TTM Firmware failed")
                End If

            Else
                activity = $"Failed uploading TTM Firmware" : Me.PublishWarning($"{activity};. ")
                Me.InfoProvider.Annunciate(Me._LoadFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Failed uploading TTM Firmware")

            End If

        Catch ex As Exception

            Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._LoadFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Exception occurred loading TTM Firmware")
            Try
                ' flush the buffer so a time out error that might leave stuff will clear the buffers.
                If Me.VisaSessionBase.IsSessionOpen Then Me.VisaSessionBase.Session.DiscardUnreadData()
            Catch
            End Try

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

            Me.ValidateSavedScriptsCatalog(True, True)

            If Not String.IsNullOrWhiteSpace(firmwareStatusMessage) Then
                Me._FirmwareStatusTextBox.Text = firmwareStatusMessage
            End If

        End Try

    End Sub

    ''' <summary> Saves any loaded firmware. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SaveFirmwareButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SaveFirmwareButton.Click
        Dim activity As String = $"User action @{System.Reflection.MethodInfo.GetCurrentMethod.Name}"
        Me.PublishVerbose($"{activity};. ")
        Me.InfoProvider.Clear()
        activity = $"Saving TTM Firmware" : Me.PublishInfo($"{activity};. ")

        Try

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' toggles measurements off.
            Me.UpdateRequired = True
            Me.DisableFirmwareControls()

            If Me.ScriptManager.SaveUserScripts(Me.VisaSessionBase.StatusSubsystemBase.InitializeTimeout, Me.TspDevice.StatusSubsystem,
                                                Me.TspDevice.DisplaySubsystem, Me.TspDevice.AccessSubsystem) Then

                ' check if code loaded.
                If Me.TspDevice.AccessSubsystem.Loaded() Then
                    ' save the autoexec script
                    If Me.TspDevice.AccessSubsystem.Loaded() Then
                        activity = $"Ready for measurements" : Me.PublishInfo($"{activity};. ")
                    ElseIf Me.TspDevice.AccessSubsystem.Certify(Me.TspDevice.AccessSubsystem.ReleaseValue(Me.TspDevice.StatusSubsystem.SerialNumberReading,
                                                                                                          Me.TspDevice.AccessSubsystem.Salt)) Then
                        activity = $"Ready for measurements" : Me.PublishInfo($"{activity};. ")
                    Else
                        activity = $"Failed saving startup firmware" : Me.PublishWarning($"{activity};. ")
                        Me.InfoProvider.Annunciate(Me._SaveFirmwareButton, Core.Forma.InfoProviderLevel.Alert, "Failed saving startup firmware")
                    End If

                Else
                    activity = $"Saving TTM Firmware failed;. Most likely, instrument '{Me.TspDevice.StatusSubsystem.Identity}' requires a firmware update."
                    Me.PublishWarning($"{activity};. ")
                    Me.InfoProvider.Annunciate(Me._SaveFirmwareButton, Core.Forma.InfoProviderLevel.Alert, "Saving TTM Firmware failed")
                End If

            Else

                activity = $"Failed saving TTM Firmware" : Me.PublishWarning($"{activity};. ")
                Me.InfoProvider.Annunciate(Me._SaveFirmwareButton, Core.Forma.InfoProviderLevel.Alert, "Failed saving TTM Firmware")

            End If

        Catch ex As Exception

            Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me._SaveFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Exception occurred saving TTM Firmware")
            Try
                ' flush the buffer so a time out error that might leave stuff will clear the buffers.
                If Me.VisaSessionBase.IsSessionOpen Then Me.VisaSessionBase.Session.DiscardUnreadData()
            Catch
            End Try

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.ValidateSavedScriptsCatalog(True, True)

        End Try

    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        isr.VI.Tsp.Rig.My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

''' <summary> Values that represent VersionCompare. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum VersionCompare

    ''' <summary> An enum constant representing the current option. </summary>
    <ComponentModel.Description("Thermal Transient Meter firmware is up to date")>
    Current = 0

    ''' <summary> An enum constant representing the parse scripts option. </summary>
    <ComponentModel.Description("Scripts are available for parsing")>
    ParseScripts = -3

    ''' <summary> An enum constant representing the delete legacy scripts option. </summary>
    <ComponentModel.Description("Legacy scripts must be deleted")>
    DeleteLegacyScripts = -2

    ''' <summary> An enum constant representing the update required option. </summary>
    <ComponentModel.Description("Thermal Transient Meter firmware needs updating")>
    UpdateRequired = -1

    ''' <summary> An enum constant representing the new version available option. </summary>
    <ComponentModel.Description("Thermal Transient Meter firmware is available to download")>
    NewVersionAvailable = 1

    ''' <summary> An enum constant representing the load firmware option. </summary>
    <ComponentModel.Description("Thermal Transient Meter firmware loading required")>
    LoadFirmware = 2
End Enum

