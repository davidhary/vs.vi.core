Imports System.Drawing
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated(),
ToolboxBitmap(GetType(LoaderTopHeader), "TopHeader.ToolboxBitmap")>
Partial Class LoaderTopHeader

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                ' onDisposeManagedResources()
                If Not components Is Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    ''' <summary> The with events control. </summary>
    Private WithEvents _SourceMeasureUnitTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _FailedPicture As System.Windows.Forms.PictureBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _PassedPicture As System.Windows.Forms.PictureBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _LastLoadDateTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _InstalledFirmwareVersionTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _ReleasedFirmwareVersionTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _SourceMeasureUnitTextBoxLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _TitleLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _InstalledFirmwareVersionTextBoxLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _LastLoadDateTextBoxLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _ReleasedFirmwareVersionTextBoxLabel As System.Windows.Forms.Label

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoaderTopHeader))
        Me._SourceMeasureUnitTextBox = New System.Windows.Forms.TextBox()
        Me._LastLoadDateTextBox = New System.Windows.Forms.TextBox()
        Me._InstalledFirmwareVersionTextBox = New System.Windows.Forms.TextBox()
        Me._ReleasedFirmwareVersionTextBox = New System.Windows.Forms.TextBox()
        Me._SourceMeasureUnitTextBoxLabel = New System.Windows.Forms.Label()
        Me._TitleLabel = New System.Windows.Forms.Label()
        Me._InstalledFirmwareVersionTextBoxLabel = New System.Windows.Forms.Label()
        Me._LastLoadDateTextBoxLabel = New System.Windows.Forms.Label()
        Me._ReleasedFirmwareVersionTextBoxLabel = New System.Windows.Forms.Label()
        Me._DataLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._TopLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ResourceTextBoxLabel = New System.Windows.Forms.Label()
        Me._ResourceTextBox = New System.Windows.Forms.TextBox()
        Me._LeftPanel = New System.Windows.Forms.Panel()
        Me._SettingsToolStrip = New System.Windows.Forms.ToolStrip()
        Me._FailedPicture = New System.Windows.Forms.PictureBox()
        Me._PassedPicture = New System.Windows.Forms.PictureBox()
        Me._OpenAboutBoxButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenDeviceSettingsButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenUserInterfaceSettingsButton = New System.Windows.Forms.ToolStripButton()
        Me._DataLayout.SuspendLayout()
        Me._TopLayout.SuspendLayout()
        Me._LeftPanel.SuspendLayout()
        Me._SettingsToolStrip.SuspendLayout()
        CType(Me._FailedPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._PassedPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_SourceMeasureUnitTextBox
        '
        Me._SourceMeasureUnitTextBox.AcceptsReturn = True
        Me._SourceMeasureUnitTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(173, Byte), Integer), CType(CType(239, Byte), Integer))
        Me._SourceMeasureUnitTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me._SourceMeasureUnitTextBox.CausesValidation = False
        Me._SourceMeasureUnitTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._SourceMeasureUnitTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._SourceMeasureUnitTextBox.Font = New System.Drawing.Font("Segoe UI", 18.0!)
        Me._SourceMeasureUnitTextBox.ForeColor = System.Drawing.Color.White
        Me._SourceMeasureUnitTextBox.Location = New System.Drawing.Point(0, 20)
        Me._SourceMeasureUnitTextBox.Margin = New System.Windows.Forms.Padding(0)
        Me._SourceMeasureUnitTextBox.MaxLength = 0
        Me._SourceMeasureUnitTextBox.Name = "_SourceMeasureUnitTextBox"
        Me._SourceMeasureUnitTextBox.ReadOnly = True
        Me._SourceMeasureUnitTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SourceMeasureUnitTextBox.Size = New System.Drawing.Size(169, 32)
        Me._SourceMeasureUnitTextBox.TabIndex = 5
        Me._SourceMeasureUnitTextBox.TabStop = False
        Me._SourceMeasureUnitTextBox.Text = "1"
        Me._SourceMeasureUnitTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me._SourceMeasureUnitTextBox, "Source measure unit")
        '
        '_LastLoadDateTextBox
        '
        Me._LastLoadDateTextBox.AcceptsReturn = True
        Me._LastLoadDateTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(173, Byte), Integer), CType(CType(239, Byte), Integer))
        Me._LastLoadDateTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me._LastLoadDateTextBox.CausesValidation = False
        Me._LastLoadDateTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._LastLoadDateTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._LastLoadDateTextBox.Font = New System.Drawing.Font("Segoe UI", 18.0!)
        Me._LastLoadDateTextBox.ForeColor = System.Drawing.Color.White
        Me._LastLoadDateTextBox.Location = New System.Drawing.Point(507, 20)
        Me._LastLoadDateTextBox.Margin = New System.Windows.Forms.Padding(0)
        Me._LastLoadDateTextBox.MaxLength = 0
        Me._LastLoadDateTextBox.Name = "_LastLoadDateTextBox"
        Me._LastLoadDateTextBox.ReadOnly = True
        Me._LastLoadDateTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LastLoadDateTextBox.Size = New System.Drawing.Size(170, 32)
        Me._LastLoadDateTextBox.TabIndex = 11
        Me._LastLoadDateTextBox.TabStop = False
        Me._LastLoadDateTextBox.Text = "0000-00-00"
        Me._LastLoadDateTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me._LastLoadDateTextBox, "Last load date")
        '
        '_InstalledFirmwareVersionTextBox
        '
        Me._InstalledFirmwareVersionTextBox.AcceptsReturn = True
        Me._InstalledFirmwareVersionTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(173, Byte), Integer), CType(CType(239, Byte), Integer))
        Me._InstalledFirmwareVersionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me._InstalledFirmwareVersionTextBox.CausesValidation = False
        Me._InstalledFirmwareVersionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._InstalledFirmwareVersionTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._InstalledFirmwareVersionTextBox.Font = New System.Drawing.Font("Segoe UI", 18.0!)
        Me._InstalledFirmwareVersionTextBox.ForeColor = System.Drawing.Color.White
        Me._InstalledFirmwareVersionTextBox.Location = New System.Drawing.Point(338, 20)
        Me._InstalledFirmwareVersionTextBox.Margin = New System.Windows.Forms.Padding(0)
        Me._InstalledFirmwareVersionTextBox.MaxLength = 0
        Me._InstalledFirmwareVersionTextBox.Name = "_InstalledFirmwareVersionTextBox"
        Me._InstalledFirmwareVersionTextBox.ReadOnly = True
        Me._InstalledFirmwareVersionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InstalledFirmwareVersionTextBox.Size = New System.Drawing.Size(169, 32)
        Me._InstalledFirmwareVersionTextBox.TabIndex = 9
        Me._InstalledFirmwareVersionTextBox.TabStop = False
        Me._InstalledFirmwareVersionTextBox.Text = "0.0.0000"
        Me._InstalledFirmwareVersionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me._InstalledFirmwareVersionTextBox, "Installed version")
        '
        '_ReleasedFirmwareVersionTextBox
        '
        Me._ReleasedFirmwareVersionTextBox.AcceptsReturn = True
        Me._ReleasedFirmwareVersionTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(173, Byte), Integer), CType(CType(239, Byte), Integer))
        Me._ReleasedFirmwareVersionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me._ReleasedFirmwareVersionTextBox.CausesValidation = False
        Me._ReleasedFirmwareVersionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._ReleasedFirmwareVersionTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._ReleasedFirmwareVersionTextBox.Font = New System.Drawing.Font("Segoe UI", 18.0!)
        Me._ReleasedFirmwareVersionTextBox.ForeColor = System.Drawing.Color.White
        Me._ReleasedFirmwareVersionTextBox.Location = New System.Drawing.Point(169, 20)
        Me._ReleasedFirmwareVersionTextBox.Margin = New System.Windows.Forms.Padding(0)
        Me._ReleasedFirmwareVersionTextBox.MaxLength = 0
        Me._ReleasedFirmwareVersionTextBox.Name = "_ReleasedFirmwareVersionTextBox"
        Me._ReleasedFirmwareVersionTextBox.ReadOnly = True
        Me._ReleasedFirmwareVersionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ReleasedFirmwareVersionTextBox.Size = New System.Drawing.Size(169, 32)
        Me._ReleasedFirmwareVersionTextBox.TabIndex = 7
        Me._ReleasedFirmwareVersionTextBox.TabStop = False
        Me._ReleasedFirmwareVersionTextBox.Text = "0.0.4000"
        Me._ReleasedFirmwareVersionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me._ReleasedFirmwareVersionTextBox, "Released Firmware Version")
        '
        '_SourceMeasureUnitTextBoxLabel
        '
        Me._SourceMeasureUnitTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._SourceMeasureUnitTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SourceMeasureUnitTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._SourceMeasureUnitTextBoxLabel.ForeColor = System.Drawing.Color.White
        Me._SourceMeasureUnitTextBoxLabel.Location = New System.Drawing.Point(0, 5)
        Me._SourceMeasureUnitTextBoxLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._SourceMeasureUnitTextBoxLabel.Name = "_SourceMeasureUnitTextBoxLabel"
        Me._SourceMeasureUnitTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SourceMeasureUnitTextBoxLabel.Size = New System.Drawing.Size(169, 15)
        Me._SourceMeasureUnitTextBoxLabel.TabIndex = 4
        Me._SourceMeasureUnitTextBoxLabel.Text = "SMU"
        Me._SourceMeasureUnitTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        '_TitleLabel
        '
        Me._TitleLabel.AutoSize = True
        Me._TitleLabel.BackColor = System.Drawing.Color.Transparent
        Me._TitleLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._TitleLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TitleLabel.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TitleLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(172, Byte), Integer), CType(CType(60, Byte), Integer))
        Me._TitleLabel.Location = New System.Drawing.Point(0, 0)
        Me._TitleLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._TitleLabel.Name = "_TitleLabel"
        Me._TitleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TitleLabel.Size = New System.Drawing.Size(611, 35)
        Me._TitleLabel.TabIndex = 1
        Me._TitleLabel.Text = "Thermal Transient Meter - Firmware Loader"
        Me._TitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_InstalledFirmwareVersionTextBoxLabel
        '
        Me._InstalledFirmwareVersionTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._InstalledFirmwareVersionTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._InstalledFirmwareVersionTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._InstalledFirmwareVersionTextBoxLabel.ForeColor = System.Drawing.Color.White
        Me._InstalledFirmwareVersionTextBoxLabel.Location = New System.Drawing.Point(338, 5)
        Me._InstalledFirmwareVersionTextBoxLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._InstalledFirmwareVersionTextBoxLabel.Name = "_InstalledFirmwareVersionTextBoxLabel"
        Me._InstalledFirmwareVersionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InstalledFirmwareVersionTextBoxLabel.Size = New System.Drawing.Size(169, 15)
        Me._InstalledFirmwareVersionTextBoxLabel.TabIndex = 8
        Me._InstalledFirmwareVersionTextBoxLabel.Text = "FIRMWARE"
        Me._InstalledFirmwareVersionTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        '_LastLoadDateTextBoxLabel
        '
        Me._LastLoadDateTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._LastLoadDateTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._LastLoadDateTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._LastLoadDateTextBoxLabel.ForeColor = System.Drawing.Color.White
        Me._LastLoadDateTextBoxLabel.Location = New System.Drawing.Point(507, 5)
        Me._LastLoadDateTextBoxLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._LastLoadDateTextBoxLabel.Name = "_LastLoadDateTextBoxLabel"
        Me._LastLoadDateTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LastLoadDateTextBoxLabel.Size = New System.Drawing.Size(170, 15)
        Me._LastLoadDateTextBoxLabel.TabIndex = 10
        Me._LastLoadDateTextBoxLabel.Text = "LAST LOADED"
        Me._LastLoadDateTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        '_ReleasedFirmwareVersionTextBoxLabel
        '
        Me._ReleasedFirmwareVersionTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._ReleasedFirmwareVersionTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ReleasedFirmwareVersionTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ReleasedFirmwareVersionTextBoxLabel.ForeColor = System.Drawing.Color.White
        Me._ReleasedFirmwareVersionTextBoxLabel.Location = New System.Drawing.Point(169, 5)
        Me._ReleasedFirmwareVersionTextBoxLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._ReleasedFirmwareVersionTextBoxLabel.Name = "_ReleasedFirmwareVersionTextBoxLabel"
        Me._ReleasedFirmwareVersionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ReleasedFirmwareVersionTextBoxLabel.Size = New System.Drawing.Size(169, 15)
        Me._ReleasedFirmwareVersionTextBoxLabel.TabIndex = 6
        Me._ReleasedFirmwareVersionTextBoxLabel.Text = "S/W Version"
        Me._ReleasedFirmwareVersionTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        '_DataLayout
        '
        Me._DataLayout.BackColor = System.Drawing.Color.Transparent
        Me._DataLayout.ColumnCount = 4
        Me._DataLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._DataLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._DataLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._DataLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._DataLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._DataLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._DataLayout.Controls.Add(Me._SourceMeasureUnitTextBox, 0, 1)
        Me._DataLayout.Controls.Add(Me._SourceMeasureUnitTextBoxLabel, 0, 0)
        Me._DataLayout.Controls.Add(Me._ReleasedFirmwareVersionTextBoxLabel, 1, 0)
        Me._DataLayout.Controls.Add(Me._ReleasedFirmwareVersionTextBox, 1, 1)
        Me._DataLayout.Controls.Add(Me._InstalledFirmwareVersionTextBoxLabel, 2, 0)
        Me._DataLayout.Controls.Add(Me._InstalledFirmwareVersionTextBox, 2, 1)
        Me._DataLayout.Controls.Add(Me._LastLoadDateTextBox, 3, 1)
        Me._DataLayout.Controls.Add(Me._LastLoadDateTextBoxLabel, 3, 0)
        Me._DataLayout.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._DataLayout.Location = New System.Drawing.Point(31, 62)
        Me._DataLayout.Name = "_DataLayout"
        Me._DataLayout.RowCount = 2
        Me._DataLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._DataLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._DataLayout.Size = New System.Drawing.Size(677, 52)
        Me._DataLayout.TabIndex = 18
        '
        '_TopLayout
        '
        Me._TopLayout.ColumnCount = 5
        Me._TopLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._TopLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._TopLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._TopLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._TopLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._TopLayout.Controls.Add(Me._TitleLabel, 0, 0)
        Me._TopLayout.Controls.Add(Me._FailedPicture, 4, 0)
        Me._TopLayout.Controls.Add(Me._PassedPicture, 3, 0)
        Me._TopLayout.Dock = System.Windows.Forms.DockStyle.Top
        Me._TopLayout.Location = New System.Drawing.Point(31, 0)
        Me._TopLayout.Name = "_TopLayout"
        Me._TopLayout.RowCount = 1
        Me._TopLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._TopLayout.Size = New System.Drawing.Size(677, 35)
        Me._TopLayout.TabIndex = 19
        '
        '_ResourceTextBoxLabel
        '
        Me._ResourceTextBoxLabel.AutoSize = True
        Me._ResourceTextBoxLabel.ForeColor = System.Drawing.Color.White
        Me._ResourceTextBoxLabel.Location = New System.Drawing.Point(97, 40)
        Me._ResourceTextBoxLabel.Name = "_ResourceTextBoxLabel"
        Me._ResourceTextBoxLabel.Size = New System.Drawing.Size(108, 17)
        Me._ResourceTextBoxLabel.TabIndex = 20
        Me._ResourceTextBoxLabel.Text = "Resource Name: "
        Me._ResourceTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        '_ResourceTextBox
        '
        Me._ResourceTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ResourceTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(173, Byte), Integer), CType(CType(239, Byte), Integer))
        Me._ResourceTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me._ResourceTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResourceTextBox.ForeColor = System.Drawing.Color.White
        Me._ResourceTextBox.Location = New System.Drawing.Point(202, 39)
        Me._ResourceTextBox.Name = "_ResourceTextBox"
        Me._ResourceTextBox.Size = New System.Drawing.Size(503, 18)
        Me._ResourceTextBox.TabIndex = 21
        '
        '_LeftPanel
        '
        Me._LeftPanel.Controls.Add(Me._SettingsToolStrip)
        Me._LeftPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me._LeftPanel.Location = New System.Drawing.Point(0, 0)
        Me._LeftPanel.Name = "_LeftPanel"
        Me._LeftPanel.Size = New System.Drawing.Size(31, 114)
        Me._LeftPanel.TabIndex = 22
        '
        '_SettingsToolStrip
        '
        Me._SettingsToolStrip.BackColor = System.Drawing.Color.Transparent
        Me._SettingsToolStrip.Dock = System.Windows.Forms.DockStyle.Left
        Me._SettingsToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._SettingsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._SettingsToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._OpenAboutBoxButton, Me._OpenDeviceSettingsButton, Me._OpenUserInterfaceSettingsButton})
        Me._SettingsToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow
        Me._SettingsToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._SettingsToolStrip.Name = "_SettingsToolStrip"
        Me._SettingsToolStrip.Size = New System.Drawing.Size(32, 114)
        Me._SettingsToolStrip.TabIndex = 2
        Me._SettingsToolStrip.Text = "ToolStrip1"
        '
        '_FailedPicture
        '
        Me._FailedPicture.BackColor = System.Drawing.Color.Transparent
        Me._FailedPicture.Cursor = System.Windows.Forms.Cursors.Default
        Me._FailedPicture.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FailedPicture.Image = CType(resources.GetObject("_FailedPicture.Image"), System.Drawing.Image)
        Me._FailedPicture.Location = New System.Drawing.Point(647, 5)
        Me._FailedPicture.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me._FailedPicture.Name = "_FailedPicture"
        Me._FailedPicture.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FailedPicture.Size = New System.Drawing.Size(27, 26)
        Me._FailedPicture.TabIndex = 3
        Me._FailedPicture.TabStop = False
        Me.ToolTip.SetToolTip(Me._FailedPicture, "FAILED")
        '
        '_PassedPicture
        '
        Me._PassedPicture.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(173, Byte), Integer), CType(CType(239, Byte), Integer))
        Me._PassedPicture.Cursor = System.Windows.Forms.Cursors.Default
        Me._PassedPicture.ForeColor = System.Drawing.SystemColors.WindowText
        Me._PassedPicture.Image = CType(resources.GetObject("_PassedPicture.Image"), System.Drawing.Image)
        Me._PassedPicture.Location = New System.Drawing.Point(614, 5)
        Me._PassedPicture.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me._PassedPicture.Name = "_PassedPicture"
        Me._PassedPicture.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PassedPicture.Size = New System.Drawing.Size(27, 25)
        Me._PassedPicture.TabIndex = 2
        Me._PassedPicture.TabStop = False
        Me.ToolTip.SetToolTip(Me._PassedPicture, "PASSED")
        '
        '_OpenAboutBoxButton
        '
        Me._OpenAboutBoxButton.AutoSize = False
        Me._OpenAboutBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._OpenAboutBoxButton.Image = Global.isr.VI.Tsp.Rig.Forms.My.Resources.Resources.logo48
        Me._OpenAboutBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenAboutBoxButton.Name = "_OpenAboutBoxButton"
        Me._OpenAboutBoxButton.Size = New System.Drawing.Size(29, 29)
        Me._OpenAboutBoxButton.ToolTipText = "Open program about dialog"
        '
        '_OpenDeviceSettingsButton
        '
        Me._OpenDeviceSettingsButton.AutoSize = False
        Me._OpenDeviceSettingsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._OpenDeviceSettingsButton.Image = CType(resources.GetObject("_OpenDeviceSettingsButton.Image"), System.Drawing.Image)
        Me._OpenDeviceSettingsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenDeviceSettingsButton.Name = "_OpenDeviceSettingsButton"
        Me._OpenDeviceSettingsButton.Size = New System.Drawing.Size(29, 29)
        Me._OpenDeviceSettingsButton.ToolTipText = "Open device settings"
        '
        '_OpenUserInterfaceSettingsButton
        '
        Me._OpenUserInterfaceSettingsButton.AutoSize = False
        Me._OpenUserInterfaceSettingsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._OpenUserInterfaceSettingsButton.Image = CType(resources.GetObject("_OpenUserInterfaceSettingsButton.Image"), System.Drawing.Image)
        Me._OpenUserInterfaceSettingsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenUserInterfaceSettingsButton.Name = "_OpenUserInterfaceSettingsButton"
        Me._OpenUserInterfaceSettingsButton.Size = New System.Drawing.Size(29, 29)
        Me._OpenUserInterfaceSettingsButton.ToolTipText = "Open user interface settings"
        '
        'LoaderTopHeader
        '
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(173, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.Controls.Add(Me._ResourceTextBox)
        Me.Controls.Add(Me._ResourceTextBoxLabel)
        Me.Controls.Add(Me._TopLayout)
        Me.Controls.Add(Me._DataLayout)
        Me.Controls.Add(Me._LeftPanel)
        Me.Name = "LoaderTopHeader"
        Me.Size = New System.Drawing.Size(708, 114)
        Me._DataLayout.ResumeLayout(False)
        Me._DataLayout.PerformLayout()
        Me._TopLayout.ResumeLayout(False)
        Me._TopLayout.PerformLayout()
        Me._LeftPanel.ResumeLayout(False)
        Me._LeftPanel.PerformLayout()
        Me._SettingsToolStrip.ResumeLayout(False)
        Me._SettingsToolStrip.PerformLayout()
        CType(Me._FailedPicture, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._PassedPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

        Private WithEvents _DataLayout As System.Windows.Forms.TableLayoutPanel

        Private WithEvents _TopLayout As System.Windows.Forms.TableLayoutPanel

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResourceTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResourceTextBoxLabel As System.Windows.Forms.Label

        Private WithEvents _LeftPanel As Windows.Forms.Panel

        Private WithEvents _SettingsToolStrip As Windows.Forms.ToolStrip

        Private WithEvents _OpenAboutBoxButton As Windows.Forms.ToolStripButton

        Private WithEvents _OpenDeviceSettingsButton As Windows.Forms.ToolStripButton

        Private WithEvents _OpenUserInterfaceSettingsButton As Windows.Forms.ToolStripButton
End Class
