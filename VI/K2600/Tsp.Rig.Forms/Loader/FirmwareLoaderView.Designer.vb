﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FirmwareLoaderView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._SaveFirmwareButton = New System.Windows.Forms.Button()
        Me._DeleteFirmwareButton = New System.Windows.Forms.Button()
        Me._LoadFirmwareButton = New System.Windows.Forms.Button()
        Me._FirmwareStatusTextBox = New System.Windows.Forms.TextBox()
        Me._FirmwareStatusTextBoxLabel = New System.Windows.Forms.Label()
        Me._ReleasedFirmwareVersionTextBox = New System.Windows.Forms.TextBox()
        Me._InstalledFirmwareVersionTextBox = New System.Windows.Forms.TextBox()
        Me._ReleasedFirmwareVersionTextBoxLabel = New System.Windows.Forms.Label()
        Me._InstalledFirmwareVersionTextBoxLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        '_SaveFirmwareButton
        '
        Me._SaveFirmwareButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SaveFirmwareButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._SaveFirmwareButton.Location = New System.Drawing.Point(325, 251)
        Me._SaveFirmwareButton.Name = "_SaveFirmwareButton"
        Me._SaveFirmwareButton.Size = New System.Drawing.Size(147, 28)
        Me._SaveFirmwareButton.TabIndex = 8
        Me._SaveFirmwareButton.Text = "SAVE FIRMWARE"
        Me.ToolTip.SetToolTip(Me._SaveFirmwareButton, "Saves firmware to non-volatile memory")
        Me._SaveFirmwareButton.UseVisualStyleBackColor = True
        '
        '_DeleteFirmwareButton
        '
        Me._DeleteFirmwareButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._DeleteFirmwareButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._DeleteFirmwareButton.Location = New System.Drawing.Point(6, 250)
        Me._DeleteFirmwareButton.Name = "_DeleteFirmwareButton"
        Me._DeleteFirmwareButton.Size = New System.Drawing.Size(147, 28)
        Me._DeleteFirmwareButton.TabIndex = 6
        Me._DeleteFirmwareButton.Text = "UNLOAD FIRMWARE"
        Me.ToolTip.SetToolTip(Me._DeleteFirmwareButton, "Deletes the current TTM version")
        Me._DeleteFirmwareButton.UseVisualStyleBackColor = True
        '
        '_LoadFirmwareButton
        '
        Me._LoadFirmwareButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me._LoadFirmwareButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._LoadFirmwareButton.Location = New System.Drawing.Point(167, 252)
        Me._LoadFirmwareButton.Name = "_LoadFirmwareButton"
        Me._LoadFirmwareButton.Size = New System.Drawing.Size(147, 28)
        Me._LoadFirmwareButton.TabIndex = 7
        Me._LoadFirmwareButton.Text = "L&OAD FIRMWARE"
        Me.ToolTip.SetToolTip(Me._LoadFirmwareButton, "Loads the current firmware to the Driver.")
        Me._LoadFirmwareButton.UseVisualStyleBackColor = True
        '
        '_FirmwareStatusTextBox
        '
        Me._FirmwareStatusTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._FirmwareStatusTextBox.BackColor = System.Drawing.SystemColors.Info
        Me._FirmwareStatusTextBox.Location = New System.Drawing.Point(2, 50)
        Me._FirmwareStatusTextBox.Multiline = True
        Me._FirmwareStatusTextBox.Name = "_FirmwareStatusTextBox"
        Me._FirmwareStatusTextBox.ReadOnly = True
        Me._FirmwareStatusTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._FirmwareStatusTextBox.Size = New System.Drawing.Size(474, 194)
        Me._FirmwareStatusTextBox.TabIndex = 5
        '
        '_FirmwareStatusTextBoxLabel
        '
        Me._FirmwareStatusTextBoxLabel.AutoSize = True
        Me._FirmwareStatusTextBoxLabel.Location = New System.Drawing.Point(-2, 32)
        Me._FirmwareStatusTextBoxLabel.Name = "_FirmwareStatusTextBoxLabel"
        Me._FirmwareStatusTextBoxLabel.Size = New System.Drawing.Size(144, 17)
        Me._FirmwareStatusTextBoxLabel.TabIndex = 4
        Me._FirmwareStatusTextBoxLabel.Text = "STATUS AND ACTIONS:"
        '
        '_ReleasedFirmwareVersionTextBox
        '
        Me._ReleasedFirmwareVersionTextBox.BackColor = System.Drawing.SystemColors.Info
        Me._ReleasedFirmwareVersionTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ReleasedFirmwareVersionTextBox.Location = New System.Drawing.Point(377, 1)
        Me._ReleasedFirmwareVersionTextBox.Name = "_ReleasedFirmwareVersionTextBox"
        Me._ReleasedFirmwareVersionTextBox.ReadOnly = True
        Me._ReleasedFirmwareVersionTextBox.Size = New System.Drawing.Size(80, 25)
        Me._ReleasedFirmwareVersionTextBox.TabIndex = 3
        Me._ReleasedFirmwareVersionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me._ReleasedFirmwareVersionTextBox, "Last released firmware revision. The installed version must be the same as the re" &
        "leased version.")
        '
        '_InstalledFirmwareVersionTextBox
        '
        Me._InstalledFirmwareVersionTextBox.BackColor = System.Drawing.SystemColors.Info
        Me._InstalledFirmwareVersionTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InstalledFirmwareVersionTextBox.Location = New System.Drawing.Point(159, 1)
        Me._InstalledFirmwareVersionTextBox.Name = "_InstalledFirmwareVersionTextBox"
        Me._InstalledFirmwareVersionTextBox.ReadOnly = True
        Me._InstalledFirmwareVersionTextBox.Size = New System.Drawing.Size(80, 25)
        Me._InstalledFirmwareVersionTextBox.TabIndex = 1
        Me._InstalledFirmwareVersionTextBox.Text = "2.1.3320"
        Me._InstalledFirmwareVersionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me._InstalledFirmwareVersionTextBox, "Firmware revision now installed in the instrument (if any)")
        '
        '_ReleasedFirmwareVersionTextBoxLabel
        '
        Me._ReleasedFirmwareVersionTextBoxLabel.AutoSize = True
        Me._ReleasedFirmwareVersionTextBoxLabel.Location = New System.Drawing.Point(305, 5)
        Me._ReleasedFirmwareVersionTextBoxLabel.Name = "_ReleasedFirmwareVersionTextBoxLabel"
        Me._ReleasedFirmwareVersionTextBoxLabel.Size = New System.Drawing.Size(70, 17)
        Me._ReleasedFirmwareVersionTextBoxLabel.TabIndex = 2
        Me._ReleasedFirmwareVersionTextBoxLabel.Text = "RELEASED:"
        '
        '_InstalledFirmwareVersionTextBoxLabel
        '
        Me._InstalledFirmwareVersionTextBoxLabel.AutoSize = True
        Me._InstalledFirmwareVersionTextBoxLabel.Location = New System.Drawing.Point(8, 5)
        Me._InstalledFirmwareVersionTextBoxLabel.Name = "_InstalledFirmwareVersionTextBoxLabel"
        Me._InstalledFirmwareVersionTextBoxLabel.Size = New System.Drawing.Size(149, 17)
        Me._InstalledFirmwareVersionTextBoxLabel.TabIndex = 0
        Me._InstalledFirmwareVersionTextBoxLabel.Text = "VERSIONS.   INSTALLED:"
        '
        'FirmwareLoaderControlBase
        '
        Me.Controls.Add(Me._SaveFirmwareButton)
        Me.Controls.Add(Me._DeleteFirmwareButton)
        Me.Controls.Add(Me._LoadFirmwareButton)
        Me.Controls.Add(Me._InstalledFirmwareVersionTextBoxLabel)
        Me.Controls.Add(Me._FirmwareStatusTextBox)
        Me.Controls.Add(Me._ReleasedFirmwareVersionTextBoxLabel)
        Me.Controls.Add(Me._FirmwareStatusTextBoxLabel)
        Me.Controls.Add(Me._InstalledFirmwareVersionTextBox)
        Me.Controls.Add(Me._ReleasedFirmwareVersionTextBox)
        Me.Name = "FirmwareLoaderControlBase"
        Me.Size = New System.Drawing.Size(479, 281)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SaveFirmwareButton As System.Windows.Forms.Button
    Private WithEvents _DeleteFirmwareButton As System.Windows.Forms.Button
    Private WithEvents _LoadFirmwareButton As System.Windows.Forms.Button
    Private WithEvents _FirmwareStatusTextBox As System.Windows.Forms.TextBox
    Private WithEvents _FirmwareStatusTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ReleasedFirmwareVersionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _InstalledFirmwareVersionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ReleasedFirmwareVersionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _InstalledFirmwareVersionTextBoxLabel As System.Windows.Forms.Label
End Class
