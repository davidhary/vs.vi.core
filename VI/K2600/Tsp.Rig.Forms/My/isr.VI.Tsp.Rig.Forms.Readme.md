## ISR VI Tsp Rig Forms<sub>&trade;</sub>: Test Script Processor Windows Forms Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*7.1.7220 2019-10-08*  
Visa 2019. Updates all views to using base-class info
provider and tool tip. Updates all views to notify of form closing and
use for updating resource names and titles.

*7.0.7054 2019-04-25*  
Upgrades to 2019 core libraries and open sourced.

*6.1.7019 2019-03-21*  
Uses the Facade Views framework.

*6.1.6602 2018-01-28*  
Created from the instrument Class Library

*6.1.6944 2019-01-05*  
New project evolved from the legacy 2600 project. Uses
Facade classes in place of the legacy Instrument project classes.

\(C\) 2013 Integrated Scientific Resources, Inc. All rights reserved.\
### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)  
[Lua Global Support Libraries](https://bitbucket.org/davidhary/tsp.core)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](http://www.keithley.com)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
