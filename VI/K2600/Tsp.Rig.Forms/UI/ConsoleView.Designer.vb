﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConsoleView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._ExecutionTimeTextBox = New System.Windows.Forms.TextBox()
        Me._OutputTextBox = New System.Windows.Forms.TextBox()
        Me._ConsoleTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._InputPanel = New System.Windows.Forms.Panel()
        Me._InputTextBox = New System.Windows.Forms.TextBox()
        Me._TimingTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._QueryCommandTextBoxLabel = New System.Windows.Forms.Label()
        Me._ExecutionTimeTextBoxLabel = New System.Windows.Forms.Label()
        Me._OutputPanel = New System.Windows.Forms.Panel()
        Me._ReplyTextBoxLabel = New System.Windows.Forms.Label()
        Me._ConsoleTableLayoutPanel.SuspendLayout()
        Me._InputPanel.SuspendLayout()
        Me._TimingTableLayoutPanel.SuspendLayout()
        Me._OutputPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ExecutionTimeTextBox
        '
        Me._ExecutionTimeTextBox.AcceptsReturn = True
        Me._ExecutionTimeTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._ExecutionTimeTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._ExecutionTimeTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ExecutionTimeTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ExecutionTimeTextBox.Location = New System.Drawing.Point(307, 3)
        Me._ExecutionTimeTextBox.MaxLength = 0
        Me._ExecutionTimeTextBox.Name = "_ExecutionTimeTextBox"
        Me._ExecutionTimeTextBox.ReadOnly = True
        Me._ExecutionTimeTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ExecutionTimeTextBox.Size = New System.Drawing.Size(74, 25)
        Me._ExecutionTimeTextBox.TabIndex = 37
        Me._ExecutionTimeTextBox.Text = "0.000"
        '
        '_OutputTextBox
        '
        Me._OutputTextBox.AcceptsReturn = True
        Me._OutputTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me._OutputTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._OutputTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._OutputTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._OutputTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._OutputTextBox.Location = New System.Drawing.Point(0, 32)
        Me._OutputTextBox.MaxLength = 0
        Me._OutputTextBox.Multiline = True
        Me._OutputTextBox.Name = "_OutputTextBox"
        Me._OutputTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._OutputTextBox.Size = New System.Drawing.Size(384, 428)
        Me._OutputTextBox.TabIndex = 1
        Me._OutputTextBox.WordWrap = False
        '
        '_ConsoleTableLayoutPanel
        '
        Me._ConsoleTableLayoutPanel.ColumnCount = 2
        Me._ConsoleTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConsoleTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConsoleTableLayoutPanel.Controls.Add(Me._InputPanel, 0, 0)
        Me._ConsoleTableLayoutPanel.Controls.Add(Me._OutputPanel, 1, 0)
        Me._ConsoleTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ConsoleTableLayoutPanel.Location = New System.Drawing.Point(1, 1)
        Me._ConsoleTableLayoutPanel.Name = "_ConsoleTableLayoutPanel"
        Me._ConsoleTableLayoutPanel.RowCount = 1
        Me._ConsoleTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._ConsoleTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 470.0!))
        Me._ConsoleTableLayoutPanel.Size = New System.Drawing.Size(780, 466)
        Me._ConsoleTableLayoutPanel.TabIndex = 9
        '
        '_InputPanel
        '
        Me._InputPanel.Controls.Add(Me._InputTextBox)
        Me._InputPanel.Controls.Add(Me._TimingTableLayoutPanel)
        Me._InputPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InputPanel.Location = New System.Drawing.Point(3, 3)
        Me._InputPanel.Name = "_InputPanel"
        Me._InputPanel.Size = New System.Drawing.Size(384, 460)
        Me._InputPanel.TabIndex = 41
        '
        '_InputTextBox
        '
        Me._InputTextBox.AcceptsReturn = True
        Me._InputTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._InputTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._InputTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._InputTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._InputTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputTextBox.Location = New System.Drawing.Point(0, 33)
        Me._InputTextBox.MaxLength = 0
        Me._InputTextBox.Multiline = True
        Me._InputTextBox.Name = "_InputTextBox"
        Me._InputTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._InputTextBox.Size = New System.Drawing.Size(384, 427)
        Me._InputTextBox.TabIndex = 0
        Me._InputTextBox.Text = "print(_VERSION)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me._InputTextBox.WordWrap = False
        '
        '_TimingTableLayoutPanel
        '
        Me._TimingTableLayoutPanel.ColumnCount = 3
        Me._TimingTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._TimingTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._TimingTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._TimingTableLayoutPanel.Controls.Add(Me._ExecutionTimeTextBox, 2, 0)
        Me._TimingTableLayoutPanel.Controls.Add(Me._QueryCommandTextBoxLabel, 0, 0)
        Me._TimingTableLayoutPanel.Controls.Add(Me._ExecutionTimeTextBoxLabel, 1, 0)
        Me._TimingTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._TimingTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._TimingTableLayoutPanel.Name = "_TimingTableLayoutPanel"
        Me._TimingTableLayoutPanel.RowCount = 1
        Me._TimingTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._TimingTableLayoutPanel.Size = New System.Drawing.Size(384, 33)
        Me._TimingTableLayoutPanel.TabIndex = 42
        '
        '_QueryCommandTextBoxLabel
        '
        Me._QueryCommandTextBoxLabel.AutoSize = True
        Me._QueryCommandTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._QueryCommandTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._QueryCommandTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._QueryCommandTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._QueryCommandTextBoxLabel.Location = New System.Drawing.Point(3, 0)
        Me._QueryCommandTextBoxLabel.Name = "_QueryCommandTextBoxLabel"
        Me._QueryCommandTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._QueryCommandTextBoxLabel.Size = New System.Drawing.Size(146, 33)
        Me._QueryCommandTextBoxLabel.TabIndex = 0
        Me._QueryCommandTextBoxLabel.Text = "Instrument Input: "
        Me._QueryCommandTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_ExecutionTimeTextBoxLabel
        '
        Me._ExecutionTimeTextBoxLabel.AutoSize = True
        Me._ExecutionTimeTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._ExecutionTimeTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ExecutionTimeTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ExecutionTimeTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ExecutionTimeTextBoxLabel.Location = New System.Drawing.Point(155, 0)
        Me._ExecutionTimeTextBoxLabel.Name = "_ExecutionTimeTextBoxLabel"
        Me._ExecutionTimeTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ExecutionTimeTextBoxLabel.Size = New System.Drawing.Size(146, 33)
        Me._ExecutionTimeTextBoxLabel.TabIndex = 38
        Me._ExecutionTimeTextBoxLabel.Text = "Execution Time [ms]:"
        Me._ExecutionTimeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_OutputPanel
        '
        Me._OutputPanel.Controls.Add(Me._OutputTextBox)
        Me._OutputPanel.Controls.Add(Me._ReplyTextBoxLabel)
        Me._OutputPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._OutputPanel.Location = New System.Drawing.Point(393, 3)
        Me._OutputPanel.Name = "_OutputPanel"
        Me._OutputPanel.Size = New System.Drawing.Size(384, 460)
        Me._OutputPanel.TabIndex = 40
        '
        '_ReplyTextBoxLabel
        '
        Me._ReplyTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._ReplyTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ReplyTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._ReplyTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ReplyTextBoxLabel.Location = New System.Drawing.Point(0, 0)
        Me._ReplyTextBoxLabel.Name = "_ReplyTextBoxLabel"
        Me._ReplyTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ReplyTextBoxLabel.Size = New System.Drawing.Size(384, 32)
        Me._ReplyTextBoxLabel.TabIndex = 0
        Me._ReplyTextBoxLabel.Text = "Instrument Output: "
        Me._ReplyTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ConsoleView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._ConsoleTableLayoutPanel)
        Me.Name = "ConsoleView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(782, 468)
        Me._ConsoleTableLayoutPanel.ResumeLayout(False)
        Me._InputPanel.ResumeLayout(False)
        Me._InputPanel.PerformLayout()
        Me._TimingTableLayoutPanel.ResumeLayout(False)
        Me._TimingTableLayoutPanel.PerformLayout()
        Me._OutputPanel.ResumeLayout(False)
        Me._OutputPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ConsoleTableLayoutPanel As Windows.Forms.TableLayoutPanel
    Private WithEvents _InputPanel As Windows.Forms.Panel
    Private WithEvents _InputTextBox As Windows.Forms.TextBox
    Private WithEvents _TimingTableLayoutPanel As Windows.Forms.TableLayoutPanel
    Private WithEvents _ExecutionTimeTextBox As Windows.Forms.TextBox
    Private WithEvents _QueryCommandTextBoxLabel As Windows.Forms.Label
    Private WithEvents _ExecutionTimeTextBoxLabel As Windows.Forms.Label
    Private WithEvents _OutputPanel As Windows.Forms.Panel
    Private WithEvents _OutputTextBox As Windows.Forms.TextBox
    Private WithEvents _ReplyTextBoxLabel As Windows.Forms.Label
End Class
