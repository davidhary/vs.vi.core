﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InstrumentView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._GroupTriggerButton = New System.Windows.Forms.Button()
        Me._DeviceClearButton = New System.Windows.Forms.Button()
        Me._ShowPromptsCheckBox = New System.Windows.Forms.CheckBox()
        Me._ShowErrorsCheckBox = New System.Windows.Forms.CheckBox()
        Me._AbortButton = New System.Windows.Forms.Button()
        Me._ResetLocalNodeButton = New System.Windows.Forms.Button()
        Me._InstrumentTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._InstrumentPanel3 = New System.Windows.Forms.Panel()
        Me._InstrumentPanel2 = New System.Windows.Forms.Panel()
        Me._InstrumentPanel1 = New System.Windows.Forms.Panel()
        Me._InstrumentTableLayoutPanel.SuspendLayout()
        Me._InstrumentPanel3.SuspendLayout()
        Me._InstrumentPanel2.SuspendLayout()
        Me._InstrumentPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_GroupTriggerButton
        '
        Me._GroupTriggerButton.BackColor = System.Drawing.SystemColors.Control
        Me._GroupTriggerButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._GroupTriggerButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GroupTriggerButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._GroupTriggerButton.Location = New System.Drawing.Point(9, 64)
        Me._GroupTriggerButton.Name = "_GroupTriggerButton"
        Me._GroupTriggerButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._GroupTriggerButton.Size = New System.Drawing.Size(145, 33)
        Me._GroupTriggerButton.TabIndex = 1
        Me._GroupTriggerButton.Text = "ASSERT &TRIGGER"
        Me._GroupTriggerButton.UseVisualStyleBackColor = True
        '
        '_DeviceClearButton
        '
        Me._DeviceClearButton.BackColor = System.Drawing.SystemColors.Control
        Me._DeviceClearButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._DeviceClearButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DeviceClearButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._DeviceClearButton.Location = New System.Drawing.Point(9, 16)
        Me._DeviceClearButton.Name = "_DeviceClearButton"
        Me._DeviceClearButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DeviceClearButton.Size = New System.Drawing.Size(145, 33)
        Me._DeviceClearButton.TabIndex = 0
        Me._DeviceClearButton.Text = "CLEAR &DEVICE"
        Me._DeviceClearButton.UseVisualStyleBackColor = True
        '
        '_ShowPromptsCheckBox
        '
        Me._ShowPromptsCheckBox.BackColor = System.Drawing.Color.Transparent
        Me._ShowPromptsCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ShowPromptsCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ShowPromptsCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ShowPromptsCheckBox.Location = New System.Drawing.Point(7, 22)
        Me._ShowPromptsCheckBox.Name = "_ShowPromptsCheckBox"
        Me._ShowPromptsCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ShowPromptsCheckBox.Size = New System.Drawing.Size(185, 21)
        Me._ShowPromptsCheckBox.TabIndex = 0
        Me._ShowPromptsCheckBox.Text = "ENABLE &PROMPTS"
        Me._ShowPromptsCheckBox.ThreeState = True
        Me._ShowPromptsCheckBox.UseVisualStyleBackColor = False
        '
        '_ShowErrorsCheckBox
        '
        Me._ShowErrorsCheckBox.BackColor = System.Drawing.Color.Transparent
        Me._ShowErrorsCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ShowErrorsCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ShowErrorsCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ShowErrorsCheckBox.Location = New System.Drawing.Point(7, 70)
        Me._ShowErrorsCheckBox.Name = "_ShowErrorsCheckBox"
        Me._ShowErrorsCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ShowErrorsCheckBox.Size = New System.Drawing.Size(185, 21)
        Me._ShowErrorsCheckBox.TabIndex = 1
        Me._ShowErrorsCheckBox.Text = "ENABLE &ERROR OUTPUT"
        Me._ShowErrorsCheckBox.ThreeState = True
        Me._ShowErrorsCheckBox.UseVisualStyleBackColor = True
        '
        '_AbortButton
        '
        Me._AbortButton.BackColor = System.Drawing.SystemColors.Control
        Me._AbortButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._AbortButton.Enabled = False
        Me._AbortButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AbortButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AbortButton.Location = New System.Drawing.Point(8, 16)
        Me._AbortButton.Name = "_AbortButton"
        Me._AbortButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AbortButton.Size = New System.Drawing.Size(173, 33)
        Me._AbortButton.TabIndex = 0
        Me._AbortButton.Text = "&ABORT ACTIVE SCRIPT"
        Me._AbortButton.UseVisualStyleBackColor = True
        '
        '_ResetLocalNodeButton
        '
        Me._ResetLocalNodeButton.BackColor = System.Drawing.SystemColors.Control
        Me._ResetLocalNodeButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._ResetLocalNodeButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResetLocalNodeButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ResetLocalNodeButton.Location = New System.Drawing.Point(8, 64)
        Me._ResetLocalNodeButton.Name = "_ResetLocalNodeButton"
        Me._ResetLocalNodeButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ResetLocalNodeButton.Size = New System.Drawing.Size(173, 33)
        Me._ResetLocalNodeButton.TabIndex = 1
        Me._ResetLocalNodeButton.Text = "&RESET LOCAL NODE"
        Me._ResetLocalNodeButton.UseVisualStyleBackColor = True
        '
        '_InstrumentTableLayoutPanel
        '
        Me._InstrumentTableLayoutPanel.ColumnCount = 7
        Me._InstrumentTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._InstrumentTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._InstrumentTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._InstrumentTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._InstrumentTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._InstrumentTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._InstrumentTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me._InstrumentTableLayoutPanel.Controls.Add(Me._InstrumentPanel3, 1, 0)
        Me._InstrumentTableLayoutPanel.Controls.Add(Me._InstrumentPanel2, 5, 0)
        Me._InstrumentTableLayoutPanel.Controls.Add(Me._InstrumentPanel1, 3, 0)
        Me._InstrumentTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._InstrumentTableLayoutPanel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._InstrumentTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._InstrumentTableLayoutPanel.Name = "_InstrumentTableLayoutPanel"
        Me._InstrumentTableLayoutPanel.RowCount = 1
        Me._InstrumentTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._InstrumentTableLayoutPanel.Size = New System.Drawing.Size(696, 123)
        Me._InstrumentTableLayoutPanel.TabIndex = 54
        '
        '_InstrumentPanel3
        '
        Me._InstrumentPanel3.Controls.Add(Me._GroupTriggerButton)
        Me._InstrumentPanel3.Controls.Add(Me._DeviceClearButton)
        Me._InstrumentPanel3.Location = New System.Drawing.Point(37, 3)
        Me._InstrumentPanel3.Name = "_InstrumentPanel3"
        Me._InstrumentPanel3.Size = New System.Drawing.Size(160, 113)
        Me._InstrumentPanel3.TabIndex = 54
        '
        '_InstrumentPanel2
        '
        Me._InstrumentPanel2.Controls.Add(Me._ShowPromptsCheckBox)
        Me._InstrumentPanel2.Controls.Add(Me._ShowErrorsCheckBox)
        Me._InstrumentPanel2.Location = New System.Drawing.Point(465, 3)
        Me._InstrumentPanel2.Name = "_InstrumentPanel2"
        Me._InstrumentPanel2.Size = New System.Drawing.Size(194, 113)
        Me._InstrumentPanel2.TabIndex = 52
        '
        '_InstrumentPanel1
        '
        Me._InstrumentPanel1.Controls.Add(Me._AbortButton)
        Me._InstrumentPanel1.Controls.Add(Me._ResetLocalNodeButton)
        Me._InstrumentPanel1.Location = New System.Drawing.Point(237, 3)
        Me._InstrumentPanel1.Name = "_InstrumentPanel1"
        Me._InstrumentPanel1.Size = New System.Drawing.Size(188, 113)
        Me._InstrumentPanel1.TabIndex = 51
        '
        'InstrumentView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._InstrumentTableLayoutPanel)
        Me.Name = "InstrumentView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(696, 455)
        Me._InstrumentTableLayoutPanel.ResumeLayout(False)
        Me._InstrumentPanel3.ResumeLayout(False)
        Me._InstrumentPanel2.ResumeLayout(False)
        Me._InstrumentPanel1.ResumeLayout(False)

        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _InstrumentTableLayoutPanel As Windows.Forms.TableLayoutPanel
    Private WithEvents _InstrumentPanel3 As Windows.Forms.Panel
    Private WithEvents _GroupTriggerButton As Windows.Forms.Button
    Private WithEvents _DeviceClearButton As Windows.Forms.Button
    Private WithEvents _InstrumentPanel2 As Windows.Forms.Panel
    Private WithEvents _ShowPromptsCheckBox As Windows.Forms.CheckBox
    Private WithEvents _ShowErrorsCheckBox As Windows.Forms.CheckBox
    Private WithEvents _InstrumentPanel1 As Windows.Forms.Panel
    Private WithEvents _AbortButton As Windows.Forms.Button
    Private WithEvents _ResetLocalNodeButton As Windows.Forms.Button
End Class
