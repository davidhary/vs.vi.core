Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> A function view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class FunctionsView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="FunctionsView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="FunctionsView"/>. </returns>
    Public Shared Function Create() As FunctionsView
        Dim view As FunctionsView = Nothing
        Try
            view = New FunctionsView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As TspDevice

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As TspDevice
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As TspDevice)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As TspDevice)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by _callFunctionButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CallFunctionButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _CallFunctionButton.Click
        Dim activity As String = String.Empty
        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            activity = "calling function"
            Dim functionName As String = Me._FunctionNameTextBox.Text

            Dim functionArgs As String = Me._FunctionArgsTextBox.Text
            activity = $"calling function '{functionName}({functionArgs})'"

            If Not String.IsNullOrWhiteSpace(functionName) Then
                Tsp.Syntax.Lua.CallFunction(Me.Device.Session, functionName, functionArgs)
                Me.Device.StatusSubsystem.TraceVisaOperation($"{activity};. ")
            End If
        Catch ex As VI.Pith.NativeException
            Me.Device.StatusSubsystem.TraceVisaOperation($"{activity};. ")
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            ' Turn off the form hourglass
            isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(300))
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Event handler. Called by _loadFunctionButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LoadFunctionButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _LoadFunctionButton.Click

        Dim activity As String = String.Empty

        Try
            activity = "loading function code"

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Dim functionCode As String
            functionCode = Me._FunctionCodeTextBox.Text.Replace(vbCrLf, Space(1))
            If Me.Device.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(functionCode) Then
                Me.PublishInfo($"{activity };. ")
                Me.Device.Session.WriteLine(functionCode)
                Me.Device.StatusSubsystem.TraceVisaOperation($"{activity };. ")
            End If

        Catch ex As VI.Pith.NativeException
            Me.Device.StatusSubsystem.TraceVisaOperation(ex, $"{activity };. ")
        Catch ex As Exception

            Me.PublishException(activity, ex)

        Finally

            ' Turn off the form hourglass
            isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(300))
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
