﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FunctionsView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._CallFunctionButton = New System.Windows.Forms.Button()
        Me._LoadFunctionButton = New System.Windows.Forms.Button()
        Me._FunctionsTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me._FunctionCodeTextBox = New System.Windows.Forms.TextBox()
        Me._FunctionsPanel1 = New System.Windows.Forms.Panel()
        Me._FunctionArgsTextBox = New System.Windows.Forms.TextBox()
        Me._FunctionNameTextBox = New System.Windows.Forms.TextBox()
        Me._FunctionNameTextBoxLabel = New System.Windows.Forms.Label()
        Me._FunctionArgsTextBoxLabel = New System.Windows.Forms.Label()
        Me._FunctionsTableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me._FunctionCodeTextBoxLabel = New System.Windows.Forms.Label()
        Me._CallInstructionsLabel = New System.Windows.Forms.Label()
        Me._FunctionsTableLayoutPanel.SuspendLayout()
        Me._FunctionsPanel1.SuspendLayout()
        Me._FunctionsTableLayoutPanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        '_CallFunctionButton
        '
        Me._CallFunctionButton.BackColor = System.Drawing.SystemColors.Control
        Me._CallFunctionButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._CallFunctionButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._CallFunctionButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CallFunctionButton.Location = New System.Drawing.Point(415, 17)
        Me._CallFunctionButton.Name = "_CallFunctionButton"
        Me._CallFunctionButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CallFunctionButton.Size = New System.Drawing.Size(93, 33)
        Me._CallFunctionButton.TabIndex = 4
        Me._CallFunctionButton.Text = "&CALL"
        Me._CallFunctionButton.UseVisualStyleBackColor = True
        '
        '_LoadFunctionButton
        '
        Me._LoadFunctionButton.BackColor = System.Drawing.SystemColors.Control
        Me._LoadFunctionButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._LoadFunctionButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._LoadFunctionButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._LoadFunctionButton.Location = New System.Drawing.Point(630, 3)
        Me._LoadFunctionButton.Name = "_LoadFunctionButton"
        Me._LoadFunctionButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LoadFunctionButton.Size = New System.Drawing.Size(129, 27)
        Me._LoadFunctionButton.TabIndex = 1
        Me._LoadFunctionButton.Text = "&LOAD FUNCTION"
        Me._LoadFunctionButton.UseVisualStyleBackColor = True
        '
        '_FunctionsTableLayoutPanel
        '
        Me._FunctionsTableLayoutPanel.ColumnCount = 3
        Me._FunctionsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 7.0!))
        Me._FunctionsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._FunctionsTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5.0!))
        Me._FunctionsTableLayoutPanel.Controls.Add(Me._FunctionCodeTextBox, 1, 5)
        Me._FunctionsTableLayoutPanel.Controls.Add(Me._FunctionsPanel1, 1, 1)
        Me._FunctionsTableLayoutPanel.Controls.Add(Me._FunctionsTableLayoutPanel5, 1, 3)
        Me._FunctionsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FunctionsTableLayoutPanel.Location = New System.Drawing.Point(1, 1)
        Me._FunctionsTableLayoutPanel.Name = "_FunctionsTableLayoutPanel"
        Me._FunctionsTableLayoutPanel.RowCount = 7
        Me._FunctionsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._FunctionsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._FunctionsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._FunctionsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._FunctionsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._FunctionsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._FunctionsTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._FunctionsTableLayoutPanel.Size = New System.Drawing.Size(780, 466)
        Me._FunctionsTableLayoutPanel.TabIndex = 1
        '
        '_FunctionCodeTextBox
        '
        Me._FunctionCodeTextBox.AcceptsReturn = True
        Me._FunctionCodeTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._FunctionCodeTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._FunctionCodeTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FunctionCodeTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._FunctionCodeTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FunctionCodeTextBox.Location = New System.Drawing.Point(10, 117)
        Me._FunctionCodeTextBox.MaxLength = 0
        Me._FunctionCodeTextBox.Multiline = True
        Me._FunctionCodeTextBox.Name = "_FunctionCodeTextBox"
        Me._FunctionCodeTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FunctionCodeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._FunctionCodeTextBox.Size = New System.Drawing.Size(762, 337)
        Me._FunctionCodeTextBox.TabIndex = 1
        Me._FunctionCodeTextBox.Text = "function sum ( ... )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  local result = 0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  for index, value in ipairs( arg ) do " &
    "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "    result = result + value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  end" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  print ( table.concat( arg , "" + "" ) .. """ &
    " = String.Empty .. result )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "end" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        '_FunctionsPanel1
        '
        Me._FunctionsPanel1.Controls.Add(Me._FunctionArgsTextBox)
        Me._FunctionsPanel1.Controls.Add(Me._FunctionNameTextBox)
        Me._FunctionsPanel1.Controls.Add(Me._CallFunctionButton)
        Me._FunctionsPanel1.Controls.Add(Me._FunctionNameTextBoxLabel)
        Me._FunctionsPanel1.Controls.Add(Me._FunctionArgsTextBoxLabel)
        Me._FunctionsPanel1.Location = New System.Drawing.Point(10, 13)
        Me._FunctionsPanel1.Name = "_FunctionsPanel1"
        Me._FunctionsPanel1.Size = New System.Drawing.Size(519, 69)
        Me._FunctionsPanel1.TabIndex = 0
        '
        '_FunctionArgsTextBox
        '
        Me._FunctionArgsTextBox.AcceptsReturn = True
        Me._FunctionArgsTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._FunctionArgsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._FunctionArgsTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._FunctionArgsTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FunctionArgsTextBox.Location = New System.Drawing.Point(100, 36)
        Me._FunctionArgsTextBox.MaxLength = 0
        Me._FunctionArgsTextBox.Name = "_FunctionArgsTextBox"
        Me._FunctionArgsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FunctionArgsTextBox.Size = New System.Drawing.Size(305, 25)
        Me._FunctionArgsTextBox.TabIndex = 3
        Me._FunctionArgsTextBox.Text = "1,2"
        '
        '_FunctionNameTextBox
        '
        Me._FunctionNameTextBox.AcceptsReturn = True
        Me._FunctionNameTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._FunctionNameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._FunctionNameTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._FunctionNameTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FunctionNameTextBox.Location = New System.Drawing.Point(100, 8)
        Me._FunctionNameTextBox.MaxLength = 0
        Me._FunctionNameTextBox.Name = "_FunctionNameTextBox"
        Me._FunctionNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FunctionNameTextBox.Size = New System.Drawing.Size(305, 25)
        Me._FunctionNameTextBox.TabIndex = 1
        Me._FunctionNameTextBox.Text = "sum"
        '
        '_FunctionNameTextBoxLabel
        '
        Me._FunctionNameTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._FunctionNameTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._FunctionNameTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._FunctionNameTextBoxLabel.Location = New System.Drawing.Point(3, 8)
        Me._FunctionNameTextBoxLabel.Name = "_FunctionNameTextBoxLabel"
        Me._FunctionNameTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FunctionNameTextBoxLabel.Size = New System.Drawing.Size(105, 22)
        Me._FunctionNameTextBoxLabel.TabIndex = 0
        Me._FunctionNameTextBoxLabel.Text = "Function Name: "
        Me._FunctionNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_FunctionArgsTextBoxLabel
        '
        Me._FunctionArgsTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._FunctionArgsTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._FunctionArgsTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._FunctionArgsTextBoxLabel.Location = New System.Drawing.Point(3, 36)
        Me._FunctionArgsTextBoxLabel.Name = "_FunctionArgsTextBoxLabel"
        Me._FunctionArgsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FunctionArgsTextBoxLabel.Size = New System.Drawing.Size(105, 22)
        Me._FunctionArgsTextBoxLabel.TabIndex = 2
        Me._FunctionArgsTextBoxLabel.Text = "Arguments: "
        Me._FunctionArgsTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_FunctionsTableLayoutPanel5
        '
        Me._FunctionsTableLayoutPanel5.ColumnCount = 3
        Me._FunctionsTableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._FunctionsTableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._FunctionsTableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._FunctionsTableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._FunctionsTableLayoutPanel5.Controls.Add(Me._LoadFunctionButton, 2, 0)
        Me._FunctionsTableLayoutPanel5.Controls.Add(Me._FunctionCodeTextBoxLabel, 0, 0)
        Me._FunctionsTableLayoutPanel5.Controls.Add(Me._CallInstructionsLabel, 1, 0)
        Me._FunctionsTableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FunctionsTableLayoutPanel5.Location = New System.Drawing.Point(10, 83)
        Me._FunctionsTableLayoutPanel5.Name = "_FunctionsTableLayoutPanel5"
        Me._FunctionsTableLayoutPanel5.RowCount = 1
        Me._FunctionsTableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._FunctionsTableLayoutPanel5.Size = New System.Drawing.Size(762, 33)
        Me._FunctionsTableLayoutPanel5.TabIndex = 25
        '
        '_FunctionCodeTextBoxLabel
        '
        Me._FunctionCodeTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._FunctionCodeTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._FunctionCodeTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._FunctionCodeTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._FunctionCodeTextBoxLabel.Location = New System.Drawing.Point(3, 0)
        Me._FunctionCodeTextBoxLabel.Name = "_FunctionCodeTextBoxLabel"
        Me._FunctionCodeTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FunctionCodeTextBoxLabel.Size = New System.Drawing.Size(100, 33)
        Me._FunctionCodeTextBoxLabel.TabIndex = 0
        Me._FunctionCodeTextBoxLabel.Text = "Function Code: "
        Me._FunctionCodeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_CallInstructionsLabel
        '
        Me._CallInstructionsLabel.BackColor = System.Drawing.Color.Transparent
        Me._CallInstructionsLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CallInstructionsLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._CallInstructionsLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CallInstructionsLabel.Location = New System.Drawing.Point(109, 0)
        Me._CallInstructionsLabel.Name = "_CallInstructionsLabel"
        Me._CallInstructionsLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CallInstructionsLabel.Size = New System.Drawing.Size(515, 33)
        Me._CallInstructionsLabel.TabIndex = 30
        Me._CallInstructionsLabel.Text = "Values returned by the instrument show under the Console tab. "
        Me._CallInstructionsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FunctionsView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._FunctionsTableLayoutPanel)
        Me.Name = "FunctionsView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(782, 468)
        Me._FunctionsTableLayoutPanel.ResumeLayout(False)
        Me._FunctionsTableLayoutPanel.PerformLayout()
        Me._FunctionsPanel1.ResumeLayout(False)
        Me._FunctionsPanel1.PerformLayout()
        Me._FunctionsTableLayoutPanel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _FunctionsTableLayoutPanel As Windows.Forms.TableLayoutPanel
    Private WithEvents _FunctionCodeTextBox As Windows.Forms.TextBox
    Private WithEvents _FunctionsPanel1 As Windows.Forms.Panel
    Private WithEvents _FunctionArgsTextBox As Windows.Forms.TextBox
    Private WithEvents _FunctionNameTextBox As Windows.Forms.TextBox
    Private WithEvents _CallFunctionButton As Windows.Forms.Button
    Private WithEvents _FunctionNameTextBoxLabel As Windows.Forms.Label
    Private WithEvents _FunctionArgsTextBoxLabel As Windows.Forms.Label
    Private WithEvents _FunctionsTableLayoutPanel5 As Windows.Forms.TableLayoutPanel
    Private WithEvents _LoadFunctionButton As Windows.Forms.Button
    Private WithEvents _FunctionCodeTextBoxLabel As Windows.Forms.Label
    Private WithEvents _CallInstructionsLabel As Windows.Forms.Label
End Class
