Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> A console view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ConsoleView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ConsoleView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ConsoleView"/>. </returns>
    Public Shared Function Create() As ConsoleView
        Dim view As ConsoleView = Nothing
        Try
            view = New ConsoleView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                Me.DisposeRefreshTimer()
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As TspDevice

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As TspDevice
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As TspDevice)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me.StopRefreshTimer()
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
            Me.StartRefreshTimer(TimeSpan.FromMilliseconds(500))
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As TspDevice)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " IO "

    ''' <summary> The receive lock. </summary>
    Private ReadOnly _ReceiveLock As New Object

    ''' <summary> Receive a message from the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="updateConsole"> True to update the instrument output text box. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Receive(ByVal updateConsole As Boolean)

        SyncLock (Me._ReceiveLock)
            Dim activity As String = String.Empty
            Try
                activity = "receiving"

                ' read a message if we have messages.
                Dim receiveBuffer As String = String.Empty
                If updateConsole AndAlso Me.Device.Session.QueryMessageAvailableStatus(TimeSpan.FromMilliseconds(50), 3) Then

                    ' Turn on the form hourglass
                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                    activity = "reading" : Me.PublishInfo($"{activity};. ")

                    If Me.Device?.IsDeviceOpen Then
                        Me.Device.Session.StartElapsedStopwatch()
                        receiveBuffer = Me.Device.Session.ReadLine()
                        Me.Device.Session.ReadElapsedTime(True)
                    End If

                    activity = "done reading" : Me.PublishInfo($"{activity};. ")

                    Me._OutputTextBox.SelectionStart = Me._OutputTextBox.Text.Length
                    Me._OutputTextBox.SelectionLength = 0
                    Me._OutputTextBox.SelectedText = receiveBuffer & vbCrLf
                    Me._OutputTextBox.SelectionStart = Me._OutputTextBox.Text.Length

                End If

            Catch ex As Exception
                Me.PublishException(activity, ex)
            Finally

                ' Turn off the form hourglass
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End SyncLock

    End Sub

    ''' <summary> Sends a message to the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sendBuffer">         Specifies the message to send. </param>
    ''' <param name="updateInputConsole"> True to update the instrument input text box. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Send(ByVal sendBuffer As String, ByVal updateInputConsole As Boolean)

        ' Turn on the form hourglass
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim activity As String = String.Empty
        Try

            If Not String.IsNullOrWhiteSpace(sendBuffer) Then
                Me.RefreshTimer.Enabled = False

                activity = "Sending query/command" : Me.PublishInfo($"{activity};. ")

                Me.Device.Session.StartElapsedStopwatch()
                Me.Device.Session.WriteLine(sendBuffer)
                Me.Device.StatusSubsystem.TraceVisaOperation("sending query/command;. '{0}'", sendBuffer)
                Me.Device.Session.ReadElapsedTime(True)

                If updateInputConsole Then
                    Me._InputTextBox.SelectionStart = Me._InputTextBox.Text.Length
                    Me._InputTextBox.SelectionLength = 0
                    Me._InputTextBox.SelectedText = sendBuffer & vbCrLf
                    Me._InputTextBox.SelectionStart = Me._InputTextBox.Text.Length
                End If

            End If

        Catch ex As VI.Pith.NativeException
            Me.Device.StatusSubsystem.TraceVisaOperation(ex, "sending query/command;. '{0}'", sendBuffer)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally

            Me.RefreshTimer.Enabled = True

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " REFRESH TIMER "

    ''' <summary> Dispose refresh timer. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub DisposeRefreshTimer()
        ' dispose of the timer.
        If Me.RefreshTimer IsNot Nothing Then
            Me.RefreshTimer.Close()
            Me.RefreshTimer.Dispose()
            Me.RefreshTimer = Nothing
        End If
    End Sub

    ''' <summary> Starts refresh timer. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="interval"> The interval. </param>
    Private Sub StartRefreshTimer(ByVal interval As TimeSpan)
        Me.RefreshTimer = New isr.Core.Controls.StateAwareTimer With {.SynchronizingObject = Me}
        Me.RestartRefreshTimer(interval)
    End Sub

    ''' <summary> Restarts refresh timer. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="interval"> The interval. </param>
    Private Sub RestartRefreshTimer(ByVal interval As TimeSpan)
        If Me.RefreshTimer IsNot Nothing Then
            Me.RefreshTimer.Interval = interval.TotalMilliseconds
            Me.RefreshTimer.AutoReset = True
            Me.RefreshTimer.Start()
        End If
    End Sub

    ''' <summary> Stops the refresh timer. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub StopRefreshTimer()
        If Me.RefreshTimer IsNot Nothing Then
            Me.RefreshTimer.Stop()
            Me.RefreshTimer.AutoReset = False
            Me.PublishInfo("Timer stopped;. ")
        End If
    End Sub

        Private WithEvents RefreshTimer As isr.Core.Controls.StateAwareTimer

    ''' <summary> Serial polls and receives. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RefreshTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RefreshTimer.Tick
        Dim activity As String = String.Empty
        Try
            activity = "refreshing"
            If Me.Device.IsSessionOpen Then
                Me.Receive(True)
                Me.Device.Session.ReadStatusRegister()
                Me.Device.InteractiveSubsystem.ReadExecutionState()
            End If
        Catch ex As Exception
            ' stop the time on error
            Me.StopRefreshTimer()
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub


#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Gets or sets the 'query' command. </summary>
    ''' <value> The 'query' command. </value>
    Private Property QueryCommand As String

    ''' <summary>
    ''' Event handler. Called by _inputTextBox for key press events. Handles key at the command
    ''' console.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Key press event information. </param>
    Private Sub InputTextBox_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles _InputTextBox.KeyPress

        Dim keyAscii As Integer = Asc(eventArgs.KeyChar)

        Select Case keyAscii

            Case System.Windows.Forms.Keys.Return ' enter key

                ' User pressed enter key.  Send the string to the instrument.

                ' Disable the command console while waiting for command to process.
                Me._InputTextBox.Enabled = False


                ' dh 6.0.02 use command line at cursor
                If Me._InputTextBox.Lines IsNot Nothing AndAlso Me._InputTextBox.Lines.Length > 0 Then
                    Dim line As Integer = Me._InputTextBox.GetLineFromCharIndex(Me._InputTextBox.SelectionStart)
                    Me.QueryCommand = Me._InputTextBox.Lines(line)
                Else
                    Me.QueryCommand = String.Empty
                End If
                'queryCommand = getLine(Me._inputTextBox)

                ' Send the string to the instrument. Receive is invoked by the timer.
                Me.Send(Me.QueryCommand, False)

                ' TO_DO: this is a perfect place for setting a service request before read and
                ' letting the service request invoke the next read.

                ' clear the command
                Me.QueryCommand = String.Empty

                ' Turn command console back on and display response.
                Me._InputTextBox.Enabled = True
                Me._InputTextBox.Focus()

            Case System.Windows.Forms.Keys.Back ' backspace key
                Me.QueryCommand = If(Me.QueryCommand.Length < 2, String.Empty, Me.QueryCommand.Substring(0, Me.QueryCommand.Length - 1))

            Case Else ' normal key
                ' Stash key in buffer.
                Me.QueryCommand &= Chr(keyAscii)
        End Select

        eventArgs.KeyChar = Chr(keyAscii)
        If keyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
