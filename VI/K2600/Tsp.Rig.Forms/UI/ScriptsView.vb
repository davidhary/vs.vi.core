Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions

''' <summary> A script view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ScriptsView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ScriptsView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ScriptsView"/>. </returns>
    Public Shared Function Create() As ScriptsView
        Dim view As ScriptsView = Nothing
        Try
            view = New ScriptsView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As TspDevice

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As TspDevice
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As TspDevice)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.AssignScriptManager(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As TspDevice)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SCRIPT MANAGER "

    ''' <summary> Manager for script. </summary>
    Private _ScriptManager As ScriptManager

    ''' <summary> Gets the ScriptManager. </summary>
    ''' <value> The ScriptManager. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ScriptManager As ScriptManager
        Get
            Return Me._ScriptManager
        End Get
    End Property

    ''' <summary> Assigns the ScriptManager and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AssignScriptManager(ByVal value As TspDevice)
        If Me._ScriptManager IsNot Nothing Then
            Me._ScriptManager = Nothing
        End If
        If value IsNot Nothing Then
            Me._ScriptManager = New ScriptManager(Me.Device)
            ' set the default file path for scripts.
            Me.ScriptManager.FilePath = System.IO.Path.GetFullPath(System.IO.Path.Combine(My.Application.Info.DirectoryPath, "Scripts"))
            Me.ListUserScripts()
        End If
    End Sub

#End Region

#Region " METHODS "

    ''' <summary> List the user scripts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ListUserScripts()

        Dim activity As String = String.Empty
        Try
            activity = "listing scripts"
            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.PublishInfo("Flushing the read buffer...;. ")
            Me.Device.Session.DiscardUnreadData()
            Me.PublishInfo("Fetching script names...;. ")
            Dim scriptCount As Integer = Me.ScriptManager.FetchUserScriptNames()
            If scriptCount > 0 Then
                Me.PublishInfo("Listing script names...;. ")
                Me._UserScriptsList.DataSource = Nothing
                Me._UserScriptsList.Items.Clear()
                Me._UserScriptsList.DataSource = Me.ScriptManager.UserScriptNames.ToList
                If Me._UserScriptsList.Items.Count > 0 Then
                    Me._UserScriptsList.SelectedIndex = 0
                End If
            Else
                Me.PublishInfo("No Scripts;. ")

                Me._UserScriptsList.DataSource = Nothing
                Me._UserScriptsList.Items.Clear()
            End If

        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Toggle the font on the control to highlight selection of this control. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub Button_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _RemoveScriptButton.Enter, _RefreshUserScriptsListButton.Enter
        Dim thisButton As Control = CType(eventSender, Control)
        thisButton.Font = New Drawing.Font(thisButton.Font, Drawing.FontStyle.Bold)
    End Sub

    ''' <summary> Toggle the font on the control to highlight leaving this control. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub Button_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _RemoveScriptButton.Leave, _RefreshUserScriptsListButton.Leave
        Dim thisButton As Control = CType(eventSender, Control)
        thisButton.Font = New Drawing.Font(thisButton.Font, Drawing.FontStyle.Regular)
    End Sub

    ''' <summary> Loads the script. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="scriptName"> Name of the script. </param>
    ''' <param name="filePath">   The file path. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function LoadScript(ByVal scriptName As String, ByVal filePath As String) As Boolean
        Dim activity As String = String.Empty
        Dim result As Boolean = False
        Try
            activity = "loading or running script"
            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If Me.Device.IsDeviceOpen Then
                activity = $"loading script {scriptName} from {filePath}" : Me.PublishInfo($"{activity};. ")
                Me.ScriptManager.ScriptNameSetter(scriptName)
                Me.ScriptManager.FilePath = filePath
                Me.ScriptManager.LoadScriptFile(True, True, Me._RetainCodeOutlineToggle.Checked)
                activity = $"done loading script {scriptName} from {filePath}" : Me.PublishInfo($"{activity};. ")
                activity = $"listing user scripts" : Me.PublishInfo($"{activity};. ")
                Me.ListUserScripts()
                result = True
            End If
        Catch ex As Exception
            result = False
            Me.PublishException(activity, ex)
        Finally
            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
        Return result
    End Function

    ''' <summary> Executes the script operation. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="scriptName"> Name of the script. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function RunScript(ByVal scriptName As String) As Boolean
        Dim activity As String = String.Empty
        Dim result As Boolean = False
        Try
            If Me.Device.IsDeviceOpen Then
                activity = $"running script {scriptName}" : Me.PublishInfo($"{activity};. ")
                Me.ScriptManager.RunScript(TimeSpan.FromMilliseconds(3000))
                activity = $"script {scriptName} run okay" : Me.PublishInfo($"{activity};. ")
                result = True
            End If
        Catch ex As Exception
            result = False
            Me.PublishException(activity, ex)
        Finally
            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
        Return result
    End Function

    ''' <summary> Removes the script described by scriptName. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="scriptName"> Name of the script. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Function RemoveScript(ByVal scriptName As String) As Boolean
        Dim activity As String = String.Empty
        Dim result As Boolean = False
        Try
            If Me.Device.IsDeviceOpen Then
                activity = $"removing script {scriptName}" : Me.PublishInfo($"{activity};. ")
                Me.ScriptManager.RemoveScript(scriptName)
                activity = $"script {scriptName} removed" : Me.PublishInfo($"{activity};. ")
                activity = $"listing user scripts" : Me.PublishInfo($"{activity};. ")
                Me.ListUserScripts()
                result = True
            End If
        Catch ex As Exception
            result = False
            Me.PublishException(activity, ex)
        Finally
            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
        Return result
    End Function

    ''' <summary> Loads the and run script. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="scriptName"> Name of the script. </param>
    ''' <param name="filePath">   The file path. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function LoadAndRunScript(ByVal scriptName As String, ByVal filePath As String) As Boolean
        Return Me.LoadScript(scriptName, filePath) AndAlso Me.RunScript(scriptName)
    End Function

    ''' <summary> Event handler. Called by _loadAndRunButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub LoadAndRunButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _LoadAndRunButton.Click
        If Not Tsp.Script.ScriptEntityBase.IsValidScriptFileName(Me._ScriptNameTextBox.Text) Then Return
        Me.LoadAndRunScript(Me._ScriptNameTextBox.Text, Me._TspScriptSelector.FilePath)
    End Sub

    ''' <summary> Event handler. Called by _loadScriptButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub LoadScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _LoadScriptButton.Click
        If Not Tsp.Script.ScriptEntityBase.IsValidScriptFileName(Me._ScriptNameTextBox.Text) Then Return
        Me.LoadScript(Me._ScriptNameTextBox.Text, Me._TspScriptSelector.FilePath)
    End Sub

    ''' <summary>
    ''' Event handler. Called by _refreshUserScriptsListButton for click events. Updates the list of
    ''' user scripts.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RefreshUserScriptsListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RefreshUserScriptsListButton.Click
        Me.ListUserScripts()
    End Sub

    ''' <summary> Event handler. Called by _removeScriptButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub RemoveScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _RemoveScriptButton.Click
        If Not Tsp.Script.ScriptEntityBase.IsValidScriptFileName(Me._ScriptNameTextBox.Text) Then Return
        Me.RemoveScript(Me._ScriptNameTextBox.Text)
    End Sub

    ''' <summary> Event handler. Called by _runScriptButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub RunScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _RunScriptButton.Click
        If Not Tsp.Script.ScriptEntityBase.IsValidScriptFileName(Me._ScriptNameTextBox.Text) Then Return
        Me.RunScript(Me._ScriptNameTextBox.Text)
    End Sub

    ''' <summary> Event handler. Called by _tspScriptSelector for selected changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TspScriptSelector_SelectedChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles _TspScriptSelector.SelectedChanged
        If Not String.IsNullOrWhiteSpace(Me._TspScriptSelector.FileTitle) Then
            Me._ScriptNameTextBox.Text = Me._TspScriptSelector.FileTitle.Replace(".", "_")
            Me.PublishInfo("Selected Script;. '{0}'.", Me._ScriptNameTextBox.Text)
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
