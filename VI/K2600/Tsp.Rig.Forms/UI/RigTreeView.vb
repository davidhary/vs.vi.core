Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> Keithley Tsp Device User Interface. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-30 </para></remarks>
<System.ComponentModel.DisplayName("Tsp User Interface"),
      System.ComponentModel.Description("Keithley Tsp Device User Interface"),
      System.Drawing.ToolboxBitmap(GetType(RigTreeView))>
Public Class RigTreeView
    Inherits isr.VI.Facade.VisaTreeView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me._ConsoleView = ConsoleView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Console", "Console", Me._ConsoleView)
        Me._ScriptsView = ScriptsView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Scripts", "Scripts", Me._ScriptsView)
        Me._FunctionsView = FunctionsView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Functions", "Functions", Me._FunctionsView)
        Me._InstrumentView = InstrumentView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Instrument", "Instrument", Me._InstrumentView)
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Initializes the component. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'RigView
        '
        Me.Name = "RigView"
        Me.Size = New System.Drawing.Size(899, 550)
        Me.ResumeLayout(False)

    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As TspDevice)
        Me.New()
        Me.AssignDeviceThis(device)
    End Sub

    ''' <summary> Gets the console view. </summary>
    ''' <value> The console view. </value>
    Private ReadOnly Property ConsoleView As ConsoleView

    ''' <summary> Gets the scripts view. </summary>
    ''' <value> The scripts view. </value>
    Private ReadOnly Property ScriptsView As ScriptsView

    ''' <summary> Gets the functions view. </summary>
    ''' <value> The functions view. </value>
    Private ReadOnly Property FunctionsView As FunctionsView

    ''' <summary> Gets the instrument view. </summary>
    ''' <value> The instrument view. </value>
    Private ReadOnly Property InstrumentView As InstrumentView

    ''' <summary>
    ''' Releases the unmanaged resources used by the K2700 View and optionally releases the managed
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' If Me.SourceView IsNot Nothing Then Me._SourceView.Dispose() : Me._SourceView = Nothing
                Me.AssignDeviceThis(Nothing)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As TspDevice

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As TspDevice
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assign device. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Private Sub AssignDeviceThis(ByVal value As TspDevice)
        If Me._Device IsNot Nothing OrElse MyBase.VisaSessionBase IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = Nothing
            MyBase.StatusView.UserInterfaceSettings = Nothing
            Me._Device = Nothing
        End If
        Me._Device = value
        MyBase.BindVisaSessionBase(value)
        If value IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = isr.VI.Tsp.Rig.My.MySettings.Default
            MyBase.StatusView.UserInterfaceSettings = Nothing
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Public Overloads Sub AssignDevice(ByVal value As TspDevice)
        Me.AssignDeviceThis(value)
    End Sub

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Executes the device closing action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.OnDeviceClosing(e)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            ' release the device before subsystems are disposed
            Me.ConsoleView.AssignDevice(Nothing)
            Me.ScriptsView.AssignDevice(Nothing)
            Me.FunctionsView.AssignDevice(Nothing)
            Me.InstrumentView.AssignDevice(Nothing)
        End If
    End Sub

    ''' <summary> Executes the device closed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub OnDeviceClosed()
        MyBase.OnDeviceClosed()
        ' remove binding after subsystems are disposed
        ' because the device closed the subsystems are null and binding will be removed.
        ' MyBase.DisplayView.BindMeasureToolstrip(Me.Device.MeasureSubsystem)
        ' MyBase.DisplayView.BindSubsystemToolstrip(Me.Device.SourceSubsystem)
    End Sub

    ''' <summary> Executes the device opened action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub OnDeviceOpened()
        MyBase.OnDeviceOpened()
        ' assigning device and subsystems after the subsystems are created
        Me.ConsoleView.AssignDevice(Me.Device)
        Me.ScriptsView.AssignDevice(Me.Device)
        Me.FunctionsView.AssignDevice(Me.Device)
        Me.InstrumentView.AssignDevice(Me.Device)
        ' TO_DO: Assign execution state caption to the display.
        ' to_do: Assign prompt and errors display toggle to the display.
        ' MyBase.DisplayView.BindMeasureToolstrip(Me.Device.MeasureSubsystem)
        ' MyBase.DisplayView.BindSubsystemToolstrip(Me.Device.SourceSubsystem)
    End Sub

#End Region

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
