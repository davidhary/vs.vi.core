Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A instrument view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class InstrumentView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="InstrumentView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="InstrumentView"/>. </returns>
    Public Shared Function Create() As InstrumentView
        Dim view As InstrumentView = Nothing
        Try
            view = New InstrumentView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As TspDevice

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As TspDevice
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As TspDevice)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindLocalNodeSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As TspDevice)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " LOCAL NODE "

    ''' <summary> Gets or sets the local node subsystem. </summary>
    ''' <value> The local node subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property LocalNodeSubsystem As Tsp.LocalNodeSubsystemBase

    ''' <summary> Bind local node subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindLocalNodeSubsystem(ByVal device As TspDevice)
        If Me.LocalNodeSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.LocalNodeSubsystem)
            Me._LocalNodeSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._LocalNodeSubsystem = device.InteractiveSubsystem
            Me.BindSubsystem(True, Me.LocalNodeSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As Tsp.LocalNodeSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.LocalNodeSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(Tsp.LocalNodeSubsystemBase.ExecutionStateCaption))
            ' must Not read setting when biding because the instrument may be locked Or in a trigger mode
            ' The bound values should be sent when binding Or when applying propert change.
            ' TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            ' If Not Me.LocalNodeSubsystem.ProcessExecutionStateEnabled Then
            '   read execution state explicitly, because session events are disabled.
            '   Me.LocalNodeSubsystem.ReadExecutionState()
            ' End If
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.LocalNodeSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the Tsp.LocalNode subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As Tsp.LocalNodeSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Tsp.LocalNodeSubsystemBase.ExecutionStateCaption)
                ' Me._TspStatusLabel.Text = subsystem.ExecutionStateCaption
            Case NameOf(Tsp.LocalNodeSubsystemBase.ExecutionState)
            Case NameOf(Tsp.LocalNodeSubsystemBase.ShowErrors)
                Me._ShowErrorsCheckBox.CheckState = subsystem.ShowErrors.ToCheckState
            Case NameOf(Tsp.LocalNodeSubsystemBase.ShowPrompts)
                Me._ShowPromptsCheckBox.CheckState = subsystem.ShowPrompts.ToCheckState
        End Select
    End Sub

    ''' <summary> Tsp.LocalNode subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LocalNodeSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            If Me.InvokeRequired Then
                activity = $"invoking {NameOf(Tsp.LocalNodeSubsystemBase)}.{e.PropertyName} change"
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.LocalNodeSubsystemPropertyChanged), New Object() {sender, e})
            Else
                activity = $"handling {NameOf(Tsp.LocalNodeSubsystemBase)}.{e.PropertyName} change"
                Me.HandlePropertyChanged(TryCast(sender, Tsp.LocalNodeSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Terminates script execution. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AbortButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _AbortButton.Click

        Dim activity As String = String.Empty
        Try

            activity = "aborting"

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Terminates script execution when called from a script that is being
            ' executed. This command will not wait for overlapped commands to complete
            ' before terminating script execution. If overlapped commands are required
            ' to finish, use the wait complete function prior to calling exit.
            ' to_do: add a send method that will update the output display.  Send("exit() ", True)
            Me.Device.Session.WriteLine("exit() ")

            If Not Me.Device.InteractiveSubsystem.ProcessExecutionStateEnabled Then
                ' read execution state explicitly, because session events are disabled.
                ' update the instrument status.
                Me.Device.InteractiveSubsystem.ReadExecutionState()
            End If


            Me.PublishInfo("Aborted;. ")

        Catch ex As Exception

            Me.PublishException(activity, ex)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Toggle the font on the control to highlight selection of this control. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub Button_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _AbortButton.Enter, _DeviceClearButton.Enter, _GroupTriggerButton.Enter,
       _ResetLocalNodeButton.Enter, _ResetLocalNodeButton.Enter
        Dim thisButton As Control = CType(eventSender, Control)
        thisButton.Font = New Drawing.Font(thisButton.Font, Drawing.FontStyle.Bold)
    End Sub

    ''' <summary> Toggle the font on the control to highlight leaving this control. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub Button_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _AbortButton.Leave, _DeviceClearButton.Leave, _GroupTriggerButton.Leave,
      _ResetLocalNodeButton.Leave, _ResetLocalNodeButton.Leave
        Dim thisButton As Control = CType(eventSender, Control)
        thisButton.Font = New Drawing.Font(thisButton.Font, Drawing.FontStyle.Regular)
    End Sub

    ''' <summary> Event handler. Called by _deviceClearButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DeviceClearButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _DeviceClearButton.Click

        Dim activity As String = String.Empty
        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            activity = "clearing device"

            If Me.Device.IsDeviceOpen Then
                Me.PublishInfo("Clearing device..;. ")
                Me.Device.Session.ClearActiveState()
                Me.PublishInfo("Device cleared.;. ")
            End If

        Catch ex As Exception

            Me.PublishException(activity, ex)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _groupTriggerButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GroupTriggerButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _GroupTriggerButton.Click,
                                                                                                            _GroupTriggerButton.Click
        Dim activity As String = String.Empty

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            activity = "asserting trigger"

            If Me.Device.IsDeviceOpen Then
                Me.PublishInfo("Asserting trigger..;. ")
                Me.Device.Session.AssertTrigger()
                Me.PublishInfo("Trigger asserted.;. ")
            End If

        Catch ex As Exception

            Me.PublishException(activity, ex)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try
    End Sub

    ''' <summary> Event handler. Called by _resetLocalNodeButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ResetLocalNodeButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _ResetLocalNodeButton.Click

        Dim activity As String = String.Empty

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            activity = "resetting local node"

            If Me.Device.IsDeviceOpen Then

                Me.PublishInfo("Resetting local node..;. ")
                Me.Device.Session.WriteLine("localnode.reset() ")
                Me.PublishInfo("Local node was reset;. ")

                If Not Me.Device.InteractiveSubsystem.ProcessExecutionStateEnabled Then
                    ' read execution state explicitly, because session events are disabled.
                    Me.PublishInfo("Updating instrument state..;. ")
                    Me.Device.InteractiveSubsystem.ReadExecutionState()

                End If

                Me.PublishInfo("Clearing error queue..;. ")
                Me.Device.Session.WriteLine("localnode.errorqueue.clear() ")
                Me.PublishInfo("Error queue cleared;. ")

                ' update the instrument state.
                If Not Me.Device.InteractiveSubsystem.ProcessExecutionStateEnabled Then
                    ' read execution state explicitly, because session events are disabled.
                    Me.PublishInfo("Updating instrument state..;. ")
                    Me.Device.InteractiveSubsystem.ReadExecutionState()
                    Me.PublishInfo("Instrument state updated;. ")
                End If

            End If

        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by _showErrorsCheckBox for check state changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The event sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ShowErrorsCheckBox_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ShowErrorsCheckBox.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            activity = "showing errors"
            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If Me._ShowErrorsCheckBox.Enabled AndAlso Me.Device.IsDeviceOpen Then
                If Me.Device.InteractiveSubsystem.QueryShowErrors() <> Me._ShowErrorsCheckBox.Checked Then
                    Me.PublishInfo("Toggling showing errors..;. ")
                    Me.Device.InteractiveSubsystem.WriteShowErrors(Me._ShowErrorsCheckBox.Checked)
                    If Not Me.Device.InteractiveSubsystem.ShowErrors.HasValue Then
                        Me.PublishWarning("Failed toggling showing errors--value not set;. ")
                    ElseIf Me.Device.InteractiveSubsystem.ShowErrors.Value <> Me._ShowErrorsCheckBox.Checked Then
                        Me.PublishWarning("Failed toggling showing errors--incorrect value;. ")
                    End If
                    Me.PublishInfo("Showing errors: {0};. ", IIf(Me.Device.InteractiveSubsystem.ShowErrors.Value, "ON", "OFF"))
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by _showPromptsCheckBox for check state changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The event sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ShowPromptsCheckBox_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ShowPromptsCheckBox.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            activity = "showing prompts"
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If Me._ShowPromptsCheckBox.Enabled AndAlso Me.Device.IsDeviceOpen Then
                Me.PublishInfo("Toggling showing prompts..;. ")

                If Me.Device.InteractiveSubsystem.QueryShowPrompts() <> Me._ShowPromptsCheckBox.Checked Then
                    Me.Device.InteractiveSubsystem.WriteShowPrompts(Me._ShowPromptsCheckBox.Checked)

                    If Not Me.Device.InteractiveSubsystem.ShowPrompts.HasValue Then
                        Me.PublishWarning("Failed toggling showing Prompts--value not set;. ")
                    ElseIf Me.Device.InteractiveSubsystem.ShowPrompts.Value <> Me._ShowPromptsCheckBox.Checked Then
                        Me.PublishWarning("Failed toggling showing Prompts--incorrect value;. ")
                    End If
                    Me.PublishInfo("Showing Prompts: {0};. ", IIf(Me.Device.InteractiveSubsystem.ShowPrompts.Value, "ON", "OFF"))
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
