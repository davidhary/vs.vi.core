#Region " TYPES "

''' <summary> Values that represent Trigger Mode. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum TriggerMode
    ''' <summary> An enum constant representing the t 0 option. </summary>

    <ComponentModel.Description("Fast Continuous (T0)")>
    T0 = 0
    ''' <summary> An enum constant representing the t 1 option. </summary>

    <ComponentModel.Description("Fast One Shot (T1)")>
    T1 = 1
    ''' <summary> An enum constant representing the t 2 option. </summary>

    <ComponentModel.Description("Delay Continuous (T2)")>
    T2 = 2
    ''' <summary> An enum constant representing the t 3 option. </summary>

    <ComponentModel.Description("Delay One Shot (T3)")>
    T3 = 3
End Enum

''' <summary> Values that represent Error Codes. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<Flags()>
Public Enum ErrorCodes
    ''' <summary> An enum constant representing the none option. </summary>

    <ComponentModel.Description("No Errors")>
    None = 0
    ''' <summary> An enum constant representing the safe test failed option. </summary>

    <ComponentModel.Description("self test fail")>
    SafeTestFailed = 8
    ''' <summary> An enum constant representing the illegal command option. </summary>

    <ComponentModel.Description("illegal command")>
    IllegalCommand = 16
    ''' <summary> An enum constant representing the conflict option. </summary>

    <ComponentModel.Description("conflict")>
    Conflict = 32
    ''' <summary> An enum constant representing the illegal command option option. </summary>

    <ComponentModel.Description("illegal command option")>
    IllegalCommandOption = 64
End Enum

#End Region


