Imports isr.Core.EnumExtensions

''' <summary> Defines a Measure Subsystem for a Tegam 1750 Resistance Measuring System. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-07 </para>
''' </remarks>
Public Class MeasureSubsystem
    Inherits VI.MeasureResistanceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ohm)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm
        Me._SupportedCommands = New String() {"D111x", "M63x", "R1x", "T0x", "U0x", "U1x", "U2x", "Y3x"}
        Me.RandomResistance = New Random(CInt(DateTimeOffset.Now.Ticks Mod Integer.MaxValue))
        Me.MaximumDifference = 0.01
        Me.InitialDelay = TimeSpan.FromMilliseconds(150)
        Me.MeasurementDelay = TimeSpan.FromMilliseconds(150)
        Me.MaximumTrialsCount = 5

        MyBase.ResistanceRangeCurrents.Clear()
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0, 0, "Auto Range"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0.002D, 1, $"2 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0.02D, 1, $"20 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0.02D, 0.1D, $"20 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0.2D, 1, $"200 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0.2D, 0.1D, $"200 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2, 0.1D, $"2 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2, 0.01D, $"2 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20, 0.01D, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20, 0.001D, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200, 0.01D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200, 0.001D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200, 0.0001D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000, 0.001D, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000, 0.0001D, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20000, 0.0001D, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20000, 0.00001D, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200000, 0.00001D, $"200 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000000, 0.000001D, $"2 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20000000, 0.0000001D, $"20 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 nA"))

        Me.ResistanceRangeCurrentDictionary = New Dictionary(Of Integer, ResistanceRangeCurrent) From {
            {ResistanceRangeMode.R0, New ResistanceRangeCurrent(0, 0, "Auto Range")},
            {ResistanceRangeMode.R1, New ResistanceRangeCurrent(0.002D, 1, $"2 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A")},
            {ResistanceRangeMode.R2, New ResistanceRangeCurrent(0.02D, 1, $"20 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A")},
            {ResistanceRangeMode.R3, New ResistanceRangeCurrent(0.02D, 0.1D, $"20 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA")},
            {ResistanceRangeMode.R4, New ResistanceRangeCurrent(0.2D, 1, $"200 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A")},
            {ResistanceRangeMode.R5, New ResistanceRangeCurrent(0.2D, 0.1D, $"200 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA")},
            {ResistanceRangeMode.R6, New ResistanceRangeCurrent(2, 0.1D, $"2 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA")},
            {ResistanceRangeMode.R7, New ResistanceRangeCurrent(2, 0.01D, $"2 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA")},
            {ResistanceRangeMode.R8, New ResistanceRangeCurrent(20, 0.01D, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA")},
            {ResistanceRangeMode.R9, New ResistanceRangeCurrent(20, 0.001D, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA")},
            {ResistanceRangeMode.R10, New ResistanceRangeCurrent(200, 0.01D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA")},
            {ResistanceRangeMode.R11, New ResistanceRangeCurrent(200, 0.001D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA")},
            {ResistanceRangeMode.R12, New ResistanceRangeCurrent(200, 0.0001D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A")},
            {ResistanceRangeMode.R13, New ResistanceRangeCurrent(2000, 0.001D, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA")},
            {ResistanceRangeMode.R14, New ResistanceRangeCurrent(2000, 0.0001D, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A")},
            {ResistanceRangeMode.R15, New ResistanceRangeCurrent(20000, 0.0001D, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A")},
            {ResistanceRangeMode.R16, New ResistanceRangeCurrent(20000, 0.00001D, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A")},
            {ResistanceRangeMode.R17, New ResistanceRangeCurrent(200000, 0.00001D, $"200 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A")},
            {ResistanceRangeMode.R18, New ResistanceRangeCurrent(2000000, 0.000001D, $"2 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 {Arebis.StandardUnits.UnitSymbols.MU}A")},
            {ResistanceRangeMode.R19, New ResistanceRangeCurrent(20000000, 0.0000001D, $"20 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 nA")}
        }

        Me.ResistanceRangeReadWrites = New Pith.EnumReadWriteCollection From {
            {ResistanceRangeMode.R0, "R00", "R0", ResistanceRangeMode.R0.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R1, "R01", "R1", ResistanceRangeMode.R1.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R2, "R02", "R2", ResistanceRangeMode.R2.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R3, "R03", "R3", ResistanceRangeMode.R3.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R4, "R04", "R4", ResistanceRangeMode.R4.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R5, "R05", "R5", ResistanceRangeMode.R5.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R6, "R06", "R6", ResistanceRangeMode.R6.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R7, "R07", "R7", ResistanceRangeMode.R7.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R8, "R08", "R8", ResistanceRangeMode.R8.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R9, "R09", "R9", ResistanceRangeMode.R9.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R11, "R11", "R11", ResistanceRangeMode.R11.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R12, "R12", "R12", ResistanceRangeMode.R12.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R13, "R13", "R13", ResistanceRangeMode.R13.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)},
            {ResistanceRangeMode.R14, "R14", "R14", ResistanceRangeMode.R14.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega).Replace("u", Arebis.StandardUnits.UnitSymbols.MU)},
            {ResistanceRangeMode.R15, "R15", "R15", ResistanceRangeMode.R15.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega).Replace("u", Arebis.StandardUnits.UnitSymbols.MU)},
            {ResistanceRangeMode.R16, "R16", "R16", ResistanceRangeMode.R16.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega).Replace("u", Arebis.StandardUnits.UnitSymbols.MU)},
            {ResistanceRangeMode.R17, "R17", "R17", ResistanceRangeMode.R17.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega).Replace("u", Arebis.StandardUnits.UnitSymbols.MU)},
            {ResistanceRangeMode.R18, "R18", "R18", ResistanceRangeMode.R18.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega).Replace("u", Arebis.StandardUnits.UnitSymbols.MU)},
            {ResistanceRangeMode.R19, "R19", "R19", ResistanceRangeMode.R19.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega).Replace("u", Arebis.StandardUnits.UnitSymbols.MU)},
            {ResistanceRangeMode.R10, "R10", "R10", ResistanceRangeMode.R10.DescriptionUntil.Replace(" ohm", Arebis.StandardUnits.UnitSymbols.Omega)}
        }
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineClearExecutionState()
        MyBase.DefineClearExecutionState()
        Me.LastReading = String.Empty
        Me.OverRangeOpenWire = New Boolean?
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.LastReading = String.Empty
        Me.OverRangeOpenWire = New Boolean?
    End Sub

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        MyBase.DefineKnownResetState()
        Me.RangeMode = T1700.ResistanceRangeMode.R0
        Me.TriggerMode = T1700.TriggerMode.T2
        Me.MaximumDifference = 0.01
        Me.InitialDelay = TimeSpan.FromMilliseconds(150)
        Me.MeasurementDelay = TimeSpan.FromMilliseconds(150)
        Me.MaximumTrialsCount = 5
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region "  INIT, READ, FETCH "

    ''' <summary> Gets the fetch command. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The fetch command. </value>
    <Obsolete("Not supported")>
    Protected Overrides Property FetchCommand As String = String.Empty

    ''' <summary> Gets the read command. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The read command. </value>
    <Obsolete("Not supported")>
    Protected Overrides Property ReadCommand As String = String.Empty

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    <Obsolete("Not supported")>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    <Obsolete("Not supported")>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#End Region

#Region " WRITE "

    ''' <summary> The supported commands. </summary>
    Private ReadOnly _SupportedCommands As String()

    ''' <summary> Gets the supported commands. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A String. </returns>
    Public Function SupportedCommands() As String()
        Return Me._SupportedCommands
    End Function

#End Region

#Region " FETCH "

    ''' <summary> Fetches the data. </summary>
    ''' <remarks> the Tegam 1700 meter does not require issuing a read command. </remarks>
    ''' <returns> A Double? </returns>
    Public Overrides Function Fetch() As Double?
        Return Me.Read()
    End Function

    ''' <summary> Fetches the data. </summary>
    ''' <remarks> the Tegam 1700 meter does not require issuing a read command. </remarks>
    ''' <returns> A Double? </returns>
    Public Overrides Function Read() As Double?
        Dim value As String = Me.Session.ReadLineTrimEnd
        If Not String.IsNullOrWhiteSpace(Me.LastReading) Then
            ' the emulator will set the last reading. 
            Me.PrimaryReading.Value = Me.ParsePrimaryReading(value)
        End If
        Return Me.PrimaryReadingValue
    End Function

#End Region

#Region " RANDOM READING "

    ''' <summary> Gets or sets the random resistance. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The random reading. </value>
    Private Property RandomResistance As Random

    ''' <summary> Creates a new reading. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="meanValue"> The mean value. </param>
    ''' <param name="range">     The range. </param>
    ''' <returns> null if it fails, else a list of. </returns>
    Public Function NewReading(ByVal meanValue As Double, ByVal range As Double) As String
        Dim value As Double = meanValue * (1 + range * (Me.RandomResistance.NextDouble() - 0.5))
        Return String.Format(Globalization.CultureInfo.InvariantCulture, "{0} Ohm", value)
    End Function

#End Region

#Region " PARSE READING "

    ''' <summary> Parse scale. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function ParseScale(ByVal value As String) As Double
        If String.IsNullOrWhiteSpace(value) Then Throw New ArgumentNullException(NameOf(value))
        value = value.Trim
        Select Case True
            Case value.StartsWith("mOhm", StringComparison.Ordinal)
                Return 0.001
            Case value.StartsWith("Ohm", StringComparison.Ordinal)
                Return 1
            Case value.StartsWith("KOhm", StringComparison.Ordinal)
                Return 1000
            Case value.StartsWith("MOhm", StringComparison.Ordinal)
                Return 1000000
            Case Else
                Return 0
        End Select
    End Function

    ''' <summary> Parses a new set of reading elements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="reading"> Specifies the measurement text to parse into the new reading. </param>
    ''' <returns> A Tuple(Of Boolean?, Double?) </returns>
    Private Shared Function ParseReadings(ByVal reading As String) As Tuple(Of Boolean?, Double?)
        Dim overRanges As String() = New String() {"2.9999", "29.999", "299.99", "2999.9", "29999"}
        Dim res As Double?
        Dim overRange As Boolean?
        If String.IsNullOrWhiteSpace(reading) Then
            overRange = New Boolean?
            res = New Double?
        Else
            reading = reading.Trim
            Dim readings As String() = reading.Split(" "c)
            If readings.Count >= 2 Then
                If overRanges.Contains(readings(0)) Then
                    overRange = True
                    res = New Double?
                Else
                    Dim r As Double = Double.Parse(readings(0))
                    Dim units As String = readings(readings.Count - 1).Trim
                    Dim s As Double = MeasureSubsystem.ParseScale(units)
                    If s > 0 Then
                        overRange = False
                        res = r * s
                    Else
                        overRange = New Boolean?
                        res = New Double?
                    End If
                End If
            Else
                overRange = New Boolean?
                res = New Double?
            End If
        End If
        Return New Tuple(Of Boolean?, Double?)(overRange, res)
    End Function

    ''' <summary> Parses the reading into the data elements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="reading"> Specifies the measurement text to parse into the new reading. </param>
    ''' <returns> A Double? </returns>
    Public Overrides Function ParsePrimaryReading(ByVal reading As String) As Double?
        Dim result As Tuple(Of Boolean?, Double?) = MeasureSubsystem.ParseReadings(reading)
        Me.OverRangeOpenWire = result.Item1
        isr.Core.ApplianceBase.DoEvents()
        Return result.Item2
    End Function

#End Region

#Region " OVER RANGE OPEN WIRE "

    ''' <summary> The over range open wire timeout. </summary>
    Private _OverRangeOpenWireTimeout As TimeSpan

    ''' <summary>
    ''' Gets or sets the over range open wire timeout. Measurements is allowed to repeat until the
    ''' timeout expires.
    ''' </summary>
    ''' <value> The over range open wire timeout. </value>
    Public Property OverRangeOpenWireTimeout As TimeSpan
        Get
            Return Me._OverRangeOpenWireTimeout
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.OverRangeOpenWireTimeout) Then
                Me._OverRangeOpenWireTimeout = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> The over range open wire stopwatch. </summary>
    Private _OverRangeOpenWireStopwatch As Stopwatch

    ''' <summary> Resets the over range open wire timeout. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ResetOverRangeOpenWireTimeout()
        Me._OverRangeOpenWireStopwatch = Stopwatch.StartNew
    End Sub

    ''' <summary>
    ''' Checks if time elapsed beyond the
    ''' <see cref="OverRangeOpenWireTimeout">over range open wire timeout</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns>
    ''' <c>True</c> if time elapsed beyond the
    ''' <see cref="OverRangeOpenWireTimeout">over range open wire timeout</see>; Otherwise,
    ''' <c>False</c>.
    ''' </returns>
    Public Function IsOverRangeOpenWireTimeoutExpired() As Boolean
        Return Me._OverRangeOpenWireStopwatch.Elapsed > Me.OverRangeOpenWireTimeout
    End Function

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the over range open wire. </summary>
    ''' <value> The over range open wire. </value>
    Private Property _OverRangeOpenWire As Boolean?
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the over range open wire. </summary>
    ''' <value> The over range open wire. </value>
    Public Property OverRangeOpenWire As Boolean?
        Get
            Return Me._OverRangeOpenWire
        End Get
        Set(ByVal value As Boolean?)
            Me._OverRangeOpenWire = value
            Me.NotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

#End Region

#Region " RANGE MODE PARSERS "

    ''' <summary> Try convert. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="current">   The current. </param>
    ''' <param name="range">     The range. </param>
    ''' <param name="rangeMode"> [in,out] The range mode. </param>
    ''' <returns> <c>True</c> if converted; Otherwise, <c>False</c>. </returns>
    Public Shared Function TryConvert(ByVal current As String, ByVal range As String, ByRef rangeMode As ResistanceRangeMode) As Boolean
        rangeMode = T1700.ResistanceRangeMode.R0
        For Each e As ResistanceRangeMode In [Enum].GetValues(GetType(ResistanceRangeMode))
            Dim c As String = String.Empty
            Dim r As String = String.Empty
            If MeasureSubsystem.TryParse(e, c, r) Then
                If String.Equals(c, current, StringComparison.OrdinalIgnoreCase) AndAlso
                        String.Equals(r, range, StringComparison.OrdinalIgnoreCase) Then
                    rangeMode = e
                    Exit For
                End If
            Else
            End If
        Next
        Return T1700.ResistanceRangeMode.R0 <> rangeMode
    End Function

    ''' <summary>
    ''' Find range match. The matched range is the first range where both current and range are
    ''' greater or equal to the specified values. If a current match is not found, a range is
    ''' selected based on the range value along.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="current">   The current. </param>
    ''' <param name="range">     The range. </param>
    ''' <param name="rangeMode"> [in,out] The range mode. </param>
    ''' <returns> <c>True</c> if converted; Otherwise, <c>False</c>. </returns>
    Public Shared Function TryMatch(ByVal current As Double, ByVal range As Double, ByRef rangeMode As ResistanceRangeMode) As Boolean
        rangeMode = ResistanceRangeMode.R0
        For Each e As ResistanceRangeMode In [Enum].GetValues(GetType(ResistanceRangeMode))
            Dim c As Double
            Dim r As Double
            If MeasureSubsystem.TryParse(e, c, r) Then
                If current >= c AndAlso range <= r Then
                    rangeMode = e
                    Exit For
                ElseIf range <= r Then
                    ' if has the last matched range so we use the smallest current; set it in case current does not match.
                    rangeMode = e
                End If
            Else
            End If
        Next
        Return ResistanceRangeMode.R0 <> rangeMode
    End Function

    ''' <summary> Try convert. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="current">   The current. </param>
    ''' <param name="range">     The range. </param>
    ''' <param name="rangeMode"> [in,out] The range mode. </param>
    ''' <returns> <c>True</c> if converted; Otherwise, <c>False</c>. </returns>
    Public Shared Function TryConvert(ByVal current As Double, ByVal range As Double, ByRef rangeMode As ResistanceRangeMode) As Boolean
        rangeMode = ResistanceRangeMode.R0
        For Each e As ResistanceRangeMode In [Enum].GetValues(GetType(ResistanceRangeMode))
            Dim c As Double
            Dim r As Double
            If MeasureSubsystem.TryParse(e, c, r) Then
                If Math.Abs(c - current) < 0.0001 * current AndAlso Math.Abs(r - range) < 0.0001 * range Then
                    rangeMode = e
                    Exit For
                End If
            Else
            End If
        Next
        Return ResistanceRangeMode.R0 <> rangeMode
    End Function

    ''' <summary> Tries parsing the range mode from the range description. </summary>
    ''' <remarks>
    ''' Parses the description, such as <para>
    ''' <c>2m ohm range @ 1 Amp Test Current (R01)</c></para><para>
    ''' To current (e.g,, 1 Amp) and resistance (2m ohm) values.</para>
    ''' </remarks>
    ''' <param name="rangeMode"> The range mode. </param>
    ''' <param name="current">   [in,out] The current. </param>
    ''' <param name="range">     [in,out] The range. </param>
    ''' <returns> <c>True</c> if parsed; Otherwise, <c>False</c>. </returns>
    Public Shared Function TryParse(ByVal rangeMode As ResistanceRangeMode, ByRef current As String, ByRef range As String) As Boolean
        Dim elements As String() = rangeMode.Description.Split(" "c)
        If elements.Count = 9 AndAlso elements.Contains("range") AndAlso elements.Contains("Test") Then
            Dim builder As New System.Text.StringBuilder(elements(0))
            builder.Append(" ")
            builder.Append(elements(1))
            range = builder.ToString
            builder = New System.Text.StringBuilder(elements(4))
            builder.Append(" ")
            builder.Append(elements(5))
            current = builder.ToString
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary> Tries parsing the range mode from the range description. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="rangeMode"> The range mode. </param>
    ''' <param name="current">   [in,out] The current. </param>
    ''' <param name="range">     [in,out] The range. </param>
    ''' <returns> <c>True</c> if parsed; Otherwise, <c>False</c>. </returns>
    Public Shared Function TryParse(ByVal rangeMode As ResistanceRangeMode, ByRef current As Double, ByRef range As Double) As Boolean
        Dim c As String = String.Empty
        Dim r As String = String.Empty
        If MeasureSubsystem.TryParse(rangeMode, c, r) Then
            Dim cc As String() = c.Split(" "c)
            If cc.Count = 2 Then
                If Double.TryParse(cc(0), current) Then
                    Select Case cc(1)
                        Case "A"
                            current *= 1
                        Case "mA"
                            current *= 0.001
                        Case "nA"
                            current *= 0.000000001
                        Case "uA"
                            current *= 0.000001
                        Case Else
                            Return False
                    End Select
                Else
                    Return False
                End If
            Else
                Return False
            End If
            Dim rr As String() = r.Split(" "c)
            If rr.Count = 2 Then
                Dim value As String = rr(0)
                Dim units As String = value.Substring(value.Length - 1, 1)
                Dim scale As Double = 0
                Select Case units
                    Case "2"
                        scale = 1
                    Case "0"
                        scale = 1
                    Case "m"
                        scale = 0.001
                        value = value.Substring(0, value.Length - 1)
                    Case "K", "k"
                        scale = 1000
                        value = value.Substring(0, value.Length - 1)
                    Case "M"
                        scale = 1000000
                        value = value.Substring(0, value.Length - 1)
                End Select
                If Double.TryParse(value, range) Then
                    range *= scale
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region " RANGE MODE "

    ''' <summary> Gets or sets a dictionary of resistance range currents. </summary>
    ''' <value> A dictionary of resistance range currents. </value>
    Public ReadOnly Property ResistanceRangeCurrentDictionary As IDictionary(Of Integer, ResistanceRangeCurrent)

    ''' <summary> Gets or sets the resistance range read writes. </summary>
    ''' <value> The resistance range read writes. </value>
    Public ReadOnly Property ResistanceRangeReadWrites As isr.VI.Pith.EnumReadWriteCollection

    ''' <summary> The range settling time in milli-seconds. </summary>
    Public Const RangeSettlingTimeMilliseconds As Integer = 250

    ''' <summary> The range mode. </summary>
    Private _RangeMode As ResistanceRangeMode?

    ''' <summary> Gets or sets the cached source RangeMode. </summary>
    ''' <value> The <see cref="RangeMode">Range Mode</see> or none if not set or unknown. </value>
    Public Overloads Property RangeMode As ResistanceRangeMode?
        Get
            Return Me._RangeMode
        End Get
        Protected Set(ByVal value As ResistanceRangeMode?)
            If Not Nullable.Equals(Me.RangeMode, value) Then
                Me._RangeMode = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Range Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The  Range Mode. </param>
    ''' <returns> The <see cref="RangeMode">Range Mode</see> or none if unknown. </returns>
    Public Function ApplyRangeMode(ByVal value As ResistanceRangeMode) As ResistanceRangeMode?
        Me.WriteRangeMode(value)
        Return Me.QueryRangeMode()
    End Function

    ''' <summary> Queries the Range Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The <see cref="RangeMode">Range Mode</see> or none if unknown. </returns>
    Public Function QueryRangeMode() As ResistanceRangeMode?
        Dim mode As String = Me.RangeMode.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        Me.RangeMode = Me.Session.Query(Of ResistanceRangeMode)(Me.RangeMode.GetValueOrDefault(0), Me.ResistanceRangeReadWrites, "U0x")
        Return Me.RangeMode
    End Function

    ''' <summary> Writes the Range Mode without reading back the value from the device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The range mode. </param>
    ''' <returns> The <see cref="RangeMode">Range Mode</see> or none if unknown. </returns>
    Public Function WriteRangeMode(ByVal value As ResistanceRangeMode) As ResistanceRangeMode?
        Me.Session.WriteLine(value.ExtractBetween() & "x")
        Me.RangeMode = value
        Return Me.RangeMode
    End Function

#End Region

#Region " TRIGGER DELAY "

    ''' <summary> The TriggerDelay. </summary>
    Private _TriggerDelay As TimeSpan?

    ''' <summary> Gets or sets the cached Trigger Delay. </summary>
    ''' <remarks>
    ''' The TriggerDelay is used to TriggerDelay operation in the trigger layer. After the programmed
    ''' trigger event occurs, the instrument waits until the TriggerDelay period expires before
    ''' performing the Device Action.
    ''' </remarks>
    ''' <value> The Trigger Delay or none if not set or unknown. </value>
    Public Overloads Property TriggerDelay As TimeSpan?
        Get
            Return Me._TriggerDelay
        End Get
        Protected Set(ByVal value As TimeSpan?)
            If Not Nullable.Equals(Me.TriggerDelay, value) Then
                Me._TriggerDelay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trigger Delay. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The current TriggerDelay. </param>
    ''' <returns> The Trigger Delay or none if unknown. </returns>
    Public Function ApplyTriggerDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.WriteTriggerDelay(value)
        Return Me.QueryTriggerDelay()
    End Function

    ''' <summary> Queries the TriggerDelay. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The TriggerDelay or none if unknown. </returns>
    Public Function QueryTriggerDelay() As TimeSpan?
        Dim reading As String = CInt(Me.TriggerDelay.GetValueOrDefault(TimeSpan.FromMilliseconds(111)).TotalMilliseconds).ToString
        Me.Session.MakeEmulatedReplyIfEmpty(reading)
        reading = Me.Session.QueryTrimEnd("U0x")
        If Not String.IsNullOrWhiteSpace(reading) Then
            ' 012345678901234567890123
            ' C0D111F0M63P0R05S0T0B0Y0
            reading = reading.Substring(2, 3)
        End If
        Dim value As Integer
        Me.TriggerDelay = If(Integer.TryParse(reading, value), TimeSpan.FromMilliseconds(value), New TimeSpan?)
        Return Me.TriggerDelay
    End Function

    ''' <summary> Writes the Trigger Delay without reading back the value from the device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The current TriggerDelay. </param>
    ''' <returns> The Trigger Delay or none if unknown. </returns>
    Public Function WriteTriggerDelay(ByVal value As TimeSpan) As TimeSpan?
        Me.Session.WriteLine("D{0:000}x", value.TotalMilliseconds)
        Me.TriggerDelay = value
        Return Me.TriggerDelay
    End Function

#End Region

#Region " TRIGGER MODE "

    ''' <summary> The trigger mode. </summary>
    Private _TriggerMode As TriggerMode?

    ''' <summary> Gets or sets the cached source TriggerMode. </summary>
    ''' <value> The <see cref="TriggerMode">Trigger Mode</see> or none if not set or unknown. </value>
    Public Overloads Property TriggerMode As TriggerMode?
        Get
            Return Me._TriggerMode
        End Get
        Protected Set(ByVal value As TriggerMode?)
            If Not Nullable.Equals(Me.TriggerMode, value) Then
                Me._TriggerMode = value
                Me.NotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trigger Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The  Trigger Mode. </param>
    ''' <returns> The <see cref="TriggerMode">Trigger Mode</see> or none if unknown. </returns>
    Public Function ApplyTriggerMode(ByVal value As TriggerMode) As TriggerMode?
        Me.WriteTriggerMode(value)
        Return Me.QueryTriggerMode()
    End Function

    ''' <summary> Queries the Trigger Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The <see cref="TriggerMode">Trigger Mode</see> or none if unknown. </returns>
    Public Function QueryTriggerMode() As TriggerMode?
        Dim mode As String = Me.TriggerMode.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        mode = Me.Session.QueryTrimEnd("U0x")
        If String.IsNullOrWhiteSpace(mode) Then
            Dim message As String = "Failed fetching Trigger Mode"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.TriggerMode = T1700.TriggerMode.T2
        Else
            ' 0         1         2
            ' 012345678901234567890123
            ' C0D111F0M63P0R05S0T0B0Y0
            mode = mode.Substring(18, 2)
            Me.TriggerMode = CType([Enum].Parse(GetType(TriggerMode), mode), TriggerMode)
        End If
        Return Me.TriggerMode
    End Function

    ''' <summary> Writes the Trigger Mode without reading back the value from the device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The trigger mode. </param>
    ''' <returns> The <see cref="TriggerMode">Trigger Mode</see> or none if unknown. </returns>
    Public Function WriteTriggerMode(ByVal value As TriggerMode) As TriggerMode?
        Me.Session.WriteLine(value.ExtractBetween() & "x")
        Me.TriggerMode = value
        Return Me.TriggerMode
    End Function

#End Region

#Region " MEASURE SEQUENCE "

    ''' <summary> The initial delay. </summary>
    Private _InitialDelay As TimeSpan

    ''' <summary> Gets or sets the initial delay. </summary>
    ''' <value> The initial delay. </value>
    Public Property InitialDelay As TimeSpan
        Get
            Return Me._InitialDelay
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.InitialDelay) Then
                Me._InitialDelay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The measurement delay. </summary>
    Private _MeasurementDelay As TimeSpan

    ''' <summary> Gets or sets the delay between successive measurements. </summary>
    ''' <value> The Measurement delay. </value>
    Public Property MeasurementDelay As TimeSpan
        Get
            Return Me._MeasurementDelay
        End Get
        Set(value As TimeSpan)
            If Not value.Equals(Me.MeasurementDelay) Then
                Me._MeasurementDelay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Number of maximum trials. </summary>
    Private _MaximumTrialsCount As Integer

    ''' <summary> Gets or sets the maximum number trials. </summary>
    ''' <value> The number of Maximum measurements. </value>
    Public Property MaximumTrialsCount As Integer
        Get
            Return Me._MaximumTrialsCount
        End Get
        Set(value As Integer)
            If Not value.Equals(Me.MaximumTrialsCount) Then
                Me._MaximumTrialsCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The maximum difference. </summary>
    Private _MaximumDifference As Double

    ''' <summary>
    ''' Gets or sets the maximum allowed difference between successive measurements.
    ''' </summary>
    ''' <value> The maximum difference. </value>
    Public Property MaximumDifference As Double
        Get
            Return Me._MaximumDifference
        End Get
        Set(value As Double)
            If Not value.Equals(Me._MaximumDifference) Then
                Me._MaximumDifference = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Are measurements done. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="values">     The values. </param>
    ''' <param name="finalValue"> [in,out] The final value. </param>
    ''' <returns> <c>True</c> if measurement accepted or maximum count is done. </returns>
    Private Function AreMeasurementsDone(ByVal values As IList(Of Double?), ByRef finalValue As Double?) As Boolean
        If values Is Nothing Then
            Return False
        ElseIf values.Count >= 2 AndAlso values(values.Count - 1).HasValue AndAlso values(values.Count - 2).HasValue Then
            If Math.Abs(values(values.Count - 1).Value - values(values.Count - 2).Value) <= Me.MaximumDifference Then
                finalValue = values(values.Count - 1)
                Return True
            ElseIf values.Count >= Me.MaximumTrialsCount Then
                finalValue = New Double?
                Return True
            Else
                Return False
            End If
        ElseIf values.Count >= Me.MaximumTrialsCount Then
            finalValue = New Double?
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary> Gets or sets the emulated resistance average. </summary>
    ''' <value> The emulated resistance average. </value>
    Public Property EmulatedResistanceAverage As Double = 1

    ''' <summary> Gets or sets the emulated resistance range. </summary>
    ''' <value> The emulated resistance range. </value>
    Public Property EmulatedResistanceRange As Double = 0.1

    ''' <summary> Measures a resistance using the measurement sequence. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overloads Sub Measure()
        Dim measurements As New List(Of Double?)
        Dim finalValue As Double? = New Double?
        Dim delayTime As TimeSpan = Me.InitialDelay
        Dim resistance As Double?
        Do
            Me.Session.EmulatedStatusByte = VI.Pith.ServiceRequests.MeasurementEvent
            Me.Session.ReadStatusRegister() ' was Me.StatusSubsystem.ReadRegisters(), which seems unnecessary
            isr.Core.ApplianceBase.DoEvents()
            If Not Me.StatusSubsystem.ErrorAvailable Then
                isr.Core.ApplianceBase.DoEventsWaitElapsed(TimeSpan.FromMilliseconds(10), delayTime)
                delayTime = Me.MeasurementDelay
                Me.Session.MakeEmulatedReply(Me.NewReading(Me.EmulatedResistanceAverage, Me.EmulatedResistanceRange))
                Me.LastReading = Me.Session.ReadLineTrimEnd
                isr.Core.ApplianceBase.DoEvents()
                If String.IsNullOrWhiteSpace(Me.LastReading) Then
                    Me.OverRangeOpenWire = False
                    resistance = New Double?
                    isr.Core.ApplianceBase.DoEvents()
                Else
                    ' the emulator will set the last reading. 
                    resistance = Me.ParsePrimaryReading(Me.LastReading)
                    isr.Core.ApplianceBase.DoEvents()
                End If
                measurements.Add(resistance)
            End If
        Loop Until Me.StatusSubsystem.ErrorAvailable OrElse Me.AreMeasurementsDone(measurements, finalValue)
        Me.PrimaryReading.Value = finalValue
        isr.Core.ApplianceBase.DoEvents()
    End Sub

#End Region

End Class

''' <summary> Values that represent the resistance ranges with current specifications. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum ResistanceRangeMode

    ''' <summary> An enum constant representing the auto range option. </summary>
    <ComponentModel.Description("Auto Range (R0)")>
    R0 = 0

    ''' <summary> An enum constant representing the r 1 option. </summary>
    <ComponentModel.Description("2 m ohm range @ 1 A Test Current (R1)")>
    R1

    ''' <summary> An enum constant representing the r 2 option. </summary>
    <ComponentModel.Description("20 m ohm range @ 1 A Test Current (R2)")>
    R2

    ''' <summary> An enum constant representing the r 3 option. </summary>
    <ComponentModel.Description("20 m ohm range @ 100 mA Test Current (R3)")>
    R3

    ''' <summary> An enum constant representing the r 4 option. </summary>
    <ComponentModel.Description("200 m ohm range @ 1 A Test Current (R4)")>
    R4

    ''' <summary> An enum constant representing the r 5 option. </summary>
    <ComponentModel.Description("200 m ohm range @ 100 mA Test Current (R5)")>
    R5

    ''' <summary> An enum constant representing the r 6 option. </summary>
    <ComponentModel.Description("2  ohm range @ 100 mA Test Current (R6)")>
    R6

    ''' <summary> An enum constant representing the r 7 option. </summary>
    <ComponentModel.Description("2  ohm range @ 10 mA Test Current (R7)")>
    R7

    ''' <summary> An enum constant representing the r 8 option. </summary>
    <ComponentModel.Description("20  ohm range @ 10 mA Test Current (R8)")>
    R8

    ''' <summary> An enum constant representing the r 9 option. </summary>
    <ComponentModel.Description("20  ohm range @ 1 mA Test Current (R9)")>
    R9

    ''' <summary> An enum constant representing the 10 option. </summary>
    <ComponentModel.Description("200  ohm range @ 10 mA Test Current (R10)")>
    R10

    ''' <summary> An enum constant representing the 11 option. </summary>
    <ComponentModel.Description("200  ohm range @ 1 mA Test Current (R11)")>
    R11

    ''' <summary> An enum constant representing the 12 option. </summary>
    <ComponentModel.Description("200  ohm range @ 100 uA Test Current (R12)")>
    R12

    ''' <summary> An enum constant representing the 13 option. </summary>
    <ComponentModel.Description("2 K ohm range @ 1 mA Test Current (R13)")>
    R13

    ''' <summary> An enum constant representing the 14 option. </summary>
    <ComponentModel.Description("2 K ohm range @ 100 uA Test Current (R14)")>
    R14

    ''' <summary> An enum constant representing the 15 option. </summary>
    <ComponentModel.Description("20 K ohm range @ 100 uA Test Current (R15)")>
    R15

    ''' <summary> An enum constant representing the 16 option. </summary>
    <ComponentModel.Description("20 K ohm range @ 10 uA Test Current (R16)")>
    R16

    ''' <summary> An enum constant representing the 17 option. </summary>
    <ComponentModel.Description("200 K ohm range @ 10 uA Test Current (R17)")>
    R17

    ''' <summary> An enum constant representing the 18 option. </summary>
    <ComponentModel.Description("2 M ohm range @ 1 uA Test Current (R18)")>
    R18

    ''' <summary> An enum constant representing the 19 option. </summary>
    <ComponentModel.Description("20 M ohm range @ 100 nA Test Current (R19)")>
    R19
End Enum

