﻿Imports isr.Core.EnumExtensions

''' <summary> Device error. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-10-07 </para>
''' </remarks>
Public Class DeviceError
    Inherits isr.VI.DeviceError

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New(VI.Pith.Scpi.Syntax.NoErrorCompoundMessage)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="compoundError"> The compound error. </param>
    Public Overrides Sub Parse(ByVal compoundError As String)
        If String.IsNullOrWhiteSpace(compoundError) Then
        ElseIf compoundError = "000" Then
            Me.ErrorNumber = 0
            Me.ErrorMessage = CType(Me.ErrorNumber, ErrorCodes).Description
            Me.CompoundErrorMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0},{1}", Me.ErrorNumber, Me.ErrorMessage)
        ElseIf Integer.TryParse(compoundError.Trim("0"c), Me.ErrorNumber) Then
            If Me.ErrorNumber = 0 Then
                Me.ErrorMessage = CType(Me.ErrorNumber, ErrorCodes).Description
                Me.CompoundErrorMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0},{1}", Me.ErrorNumber, Me.ErrorMessage)
            Else
                If [Enum].IsDefined(GetType(ErrorCodes), Me.ErrorNumber) Then
                    Me.ErrorMessage = CType(Me.ErrorNumber, ErrorCodes).Description
                    Me.CompoundErrorMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0},{1}", Me.ErrorNumber, Me.ErrorMessage)
                End If
            End If
        End If
    End Sub
End Class
