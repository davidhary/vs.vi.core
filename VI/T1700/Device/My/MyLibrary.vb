﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = VI.Pith.My.ProjectTraceEventId.T1700

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "VI Tegam T1700 Meter Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Tegam T1700 Meter Virtual Instrument Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "VI.T1700.Meter"

    End Class


End Namespace

