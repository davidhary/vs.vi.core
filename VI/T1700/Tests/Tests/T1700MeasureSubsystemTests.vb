''' <summary>
''' This is a test class for T1700.MeasureSubsystemTest and is intended to contain all
''' MeasureSubsystemTest Unit Tests.
''' </summary>
''' <remarks> David, 2020-10-12. </remarks>
<TestClass(), TestCategory("t1700")>
Public Class T1700MeasureSubsystemTests

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="rangeMode">       The range mode. </param>
    ''' <param name="expectedCurrent"> The expected current. </param>
    ''' <param name="expectedRange">   The expected range. </param>
    Public Shared Sub AssertParseShouldPass(ByVal rangeMode As T1700.ResistanceRangeMode, ByVal expectedCurrent As String, ByVal expectedRange As String)
        Dim current As String = String.Empty
        Dim range As String = String.Empty
        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = T1700.MeasureSubsystem.TryParse(rangeMode, current, range)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(expectedCurrent, current)
        Assert.AreEqual(expectedRange, range)
    End Sub

    ''' <summary> (Unit Test Method) parse should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ParseShouldPass()
        T1700MeasureSubsystemTests.AssertParseShouldPass(T1700.ResistanceRangeMode.R10, "10 mA", "200 ohm")
    End Sub

    ''' <summary> Assert parse from numeric should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="rangeMode">       The range mode. </param>
    ''' <param name="expectedCurrent"> The expected current. </param>
    ''' <param name="expectedRange">   The expected range. </param>
    Public Shared Sub AssertParseFromNumericShouldPass(ByVal rangeMode As T1700.ResistanceRangeMode, ByVal expectedCurrent As Double, ByVal expectedRange As Double)
        Dim current As Double
        Dim range As Double
        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = T1700.MeasureSubsystem.TryParse(rangeMode, current, range)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(expectedCurrent, current, 0.00001 * current)
        Assert.AreEqual(expectedRange, range, 0.00001 * range)
    End Sub

    ''' <summary> (Unit Test Method) parse from numeric should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ParseFromNumericShouldPass()
        T1700MeasureSubsystemTests.AssertParseFromNumericShouldPass(T1700.ResistanceRangeMode.R10, 0.01, 200)
    End Sub

    ''' <summary> (Unit Test Method) parse from numeric r 1 should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ParseFromNumericR1ShouldPass()
        T1700MeasureSubsystemTests.AssertParseFromNumericShouldPass(T1700.ResistanceRangeMode.R1, 1, 0.002)
    End Sub

    ''' <summary> (Unit Test Method) parse from numeric r 5 should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ParseFromNumericR5ShouldPass()
        T1700MeasureSubsystemTests.AssertParseFromNumericShouldPass(T1700.ResistanceRangeMode.R5, 0.1, 0.2)
    End Sub

    ''' <summary> (Unit Test Method) parse from numeric r 14 should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ParseFromNumericR14ShouldPass()
        T1700MeasureSubsystemTests.AssertParseFromNumericShouldPass(T1700.ResistanceRangeMode.R14, 0.0001, 2000)
    End Sub

    ''' <summary> (Unit Test Method) parse from numeric r 19 should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ParseFromNumericR19ShouldPass()
        T1700MeasureSubsystemTests.AssertParseFromNumericShouldPass(T1700.ResistanceRangeMode.R19, 0.0000001, 20000000.0)
    End Sub

    ''' <summary> Assert convert should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="current">           The current. </param>
    ''' <param name="range">             The range. </param>
    ''' <param name="expectedRangeMode"> The expected range mode. </param>
    Public Shared Sub AssertConvertShouldPass(ByVal current As Double, ByVal range As Double, ByVal expectedRangeMode As T1700.ResistanceRangeMode)
        Dim rangeMode As T1700.ResistanceRangeMode = T1700.ResistanceRangeMode.R0
        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = T1700.MeasureSubsystem.TryConvert(current, range, rangeMode)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(expectedRangeMode, rangeMode)
    End Sub

    ''' <summary> (Unit Test Method) convert numeric r 10 should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ConvertNumericR10ShouldPass()
        T1700MeasureSubsystemTests.AssertConvertShouldPass(0.01, 200, T1700.ResistanceRangeMode.R10)
    End Sub

End Class

