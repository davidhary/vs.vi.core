Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions

''' <summary> A Reading view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ReadingView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ReadingView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ReadingView"/>. </returns>
    Public Shared Function Create() As ReadingView
        Dim view As ReadingView = Nothing
        Try
            view = New ReadingView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As T1700Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As T1700Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As T1700Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindMeasureSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As T1700Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> Gets or sets the resistance Measure subsystem . </summary>
    ''' <value> The Resistance Measure subsystem . </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MeasureSubsystem As MeasureSubsystem

    ''' <summary> Bind Measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindMeasureSubsystem(ByVal device As T1700Device)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._MeasureSubsystem = device.MeasureSubsystem
            Me.BindSubsystem(True, Me.MeasureSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MeasureSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.InitialDelay))
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.MaximumTrialsCount))
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.MaximumDifference))
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.MeasurementDelay))
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.OverRangeOpenWire))
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.PrimaryReadingValue))
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.SupportedCommands))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the measurement available action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub OnMeasurementAvailable(ByVal subsystem As MeasureSubsystem)
        If subsystem Is Nothing Then
        ElseIf Me.Device.MeasureSubsystem.PrimaryReadingValue.HasValue AndAlso Me._ReadContinuouslyCheckBox.Checked Then
            isr.Core.ApplianceBase.DoEvents()
            Me.Device.Session.ReadStatusRegister() ' was Me.Device.ReadRegisters(), which seems unnecessary
            isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(Me._PostReadingDelayNumeric.Value))
            If Not Me.Device.StatusSubsystem.ErrorAvailable Then
                Me.Device.MeasureSubsystem.Read()
            End If
        ElseIf Me.Device.MeasureSubsystem.OverRangeOpenWire.GetValueOrDefault(False) Then
            Me.PublishInfo("Measurement over range or open wire detected;. ")
            isr.Core.ApplianceBase.DoEvents()
        End If
    End Sub

    ''' <summary> Handles the measurement available action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <Obsolete("replaced with measurementAvailable(MeasureSubsystem)")>
    Private Sub OnMeasurementAvailable()
        If Me.Device.MeasureSubsystem.PrimaryReadingValue.HasValue AndAlso Me._ReadContinuouslyCheckBox.Checked Then
            isr.Core.ApplianceBase.DoEvents()
            Me.Device.Session.ReadStatusRegister() ' was Me.Device.ReadRegisters(), which seems unnecessary
            isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(Me._PostReadingDelayNumeric.Value))
            If Not Me.Device.StatusSubsystem.ErrorAvailable Then
                Me.Device.MeasureSubsystem.Read()
            End If
        End If
    End Sub

    ''' <summary> Executes the over range open wire action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <Obsolete("replaced with measurementAvailable(MeasureSubsystem)")>
    Private Sub OnOverRangeOpenWire()
        If Me.Device.MeasureSubsystem.OverRangeOpenWire.GetValueOrDefault(False) Then
            Me.PublishInfo("Measurement over range or open wire detected;. ")
            isr.Core.ApplianceBase.DoEvents()
        End If
    End Sub

    ''' <summary> Handles the supported commands changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub OnSupportedCommandsChanged(ByVal subsystem As MeasureSubsystem)
        If subsystem IsNot Nothing Then
            Me._CommandComboBox.DataSource = Nothing
            Me._CommandComboBox.Items.Clear()
            Me._CommandComboBox.DataSource = subsystem.SupportedCommands.ToList
            Me._CommandComboBox.SelectedIndex = 0
        End If
    End Sub

    ''' <summary> Updates the display of measurement settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub OnMeasureSettingsChanged(ByVal subsystem As MeasureSubsystem)
        If subsystem IsNot Nothing Then
            Me._MeasureSettingsLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Trials: {0}; Initial Delay: {1} ms; Measurement Delay: {2} ms; Delta: {3:0.0%}",
                                                          subsystem.MaximumTrialsCount, subsystem.InitialDelay.TotalMilliseconds,
                                                          subsystem.MeasurementDelay.TotalMilliseconds, subsystem.MaximumDifference)
        End If
    End Sub

    ''' <summary> Handles the Measure subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MeasureSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.T1700.MeasureSubsystem.MaximumTrialsCount), NameOf(VI.T1700.MeasureSubsystem.InitialDelay),
                 NameOf(VI.T1700.MeasureSubsystem.MeasurementDelay), NameOf(VI.T1700.MeasureSubsystem.MaximumDifference)
                Me.OnMeasureSettingsChanged(subsystem)
            Case NameOf(VI.T1700.MeasureSubsystem.OverRangeOpenWire)
                ' Me.onOverRangeOpenWire()
                Me.OnMeasurementAvailable(subsystem)
            Case NameOf(VI.T1700.MeasureSubsystem.PrimaryReadingValue)
                If subsystem.PrimaryReadingValue.HasValue AndAlso Not subsystem?.OverRangeOpenWire.GetValueOrDefault(False) Then
                    Me.PublishInfo("Parsed resistance value;. Resistance = {0}", subsystem.PrimaryReadingValue.Value)
                End If
                Me.OnMeasurementAvailable(subsystem)
            Case NameOf(VI.T1700.MeasureSubsystem.SupportedCommands)
                Me.OnSupportedCommandsChanged(subsystem)
        End Select
    End Sub

    ''' <summary> measure subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            If Me.InvokeRequired Then
                activity = $"invoking {NameOf(T1700.MeasureSubsystem)}.{e.PropertyName} change"
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.MeasureSubsystemPropertyChanged), New Object() {sender, e})
            Else
                activity = $"handling {NameOf(T1700.MeasureSubsystem)}.{e.PropertyName} change"
                Me.HandlePropertyChanged(TryCast(sender, T1700.MeasureSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: READ "

    ''' <summary> Initiates a reading for retrieval by way of the service request event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadSRQButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ReadSRQButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            activity = $"{Me.Device.ResourceNameCaption} reading status register"
            Me.Device.Session.ReadStatusRegister() ' was Me.Device.ReadRegisters(), which seems unnecessary
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Query the Device for a reading. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            ' update display modalities if changed.
            Me.Device.MeasureSubsystem.Read()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Event handler. Called by _WriteButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub WriteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _WriteButton.Click
        If Not String.IsNullOrWhiteSpace(Me._CommandComboBox.Text) Then
            Dim dataToWrite As String = Me._CommandComboBox.Text.Trim
            Dim activity As String = String.Empty
            Try
                Me.InfoProvider.Clear()
                Me.Cursor = Cursors.WaitCursor
                activity = $"{Me.Device.ResourceNameCaption} writing {dataToWrite}"
                If dataToWrite.StartsWith("U", StringComparison.OrdinalIgnoreCase) Then
                    Me.Device.MeasureSubsystem.LastReading = Me.Device.Session.QueryTrimEnd(dataToWrite)
                Else
                    Me.Device.Session.WriteLine(dataToWrite)
                End If
            Catch ex As Exception
                Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
                Me.PublishException(activity, ex)
            Finally
                Me.Device.Session.ReadStatusRegister() ' was Me.Device.ReadRegisters(), which seems unnecessary
                Me.Cursor = Cursors.Default
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _MeasureButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureButton_Click(sender As System.Object, e As System.EventArgs) Handles _MeasureButton.Click
        If Not String.IsNullOrWhiteSpace(Me._CommandComboBox.Text) Then
            Dim activity As String = String.Empty
            Try
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.Clear()
                activity = $"{Me.Device.ResourceNameCaption} measuring" : Me.PublishInfo($"{activity};. ")
                Me.Device.MeasureSubsystem.Measure()
            Catch ex As Exception
                Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
                Me.PublishException(activity, ex)
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End If

    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
