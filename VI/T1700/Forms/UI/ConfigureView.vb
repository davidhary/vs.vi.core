Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Core.EnumExtensions
Imports isr.Core.WinForms.ComboBoxExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Configure view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ConfigureView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ConfigureView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ConfigureView"/>. </returns>
    Public Shared Function Create() As ConfigureView
        Dim view As ConfigureView = Nothing
        Try
            view = New ConfigureView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As T1700Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As T1700Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As T1700Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindMeasureSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As T1700Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> Gets or sets the resistance Measure subsystem . </summary>
    ''' <value> The Resistance Measure subsystem . </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MeasureSubsystem As MeasureSubsystem

    ''' <summary> Bind Measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindMeasureSubsystem(ByVal device As T1700Device)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._MeasureSubsystem = device.MeasureSubsystem
            Me.BindSubsystem(True, Me.MeasureSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MeasureSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.RangeMode))
            Me.HandlePropertyChanged(subsystem, NameOf(T1700.MeasureSubsystem.TriggerMode))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the Measure subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MeasureSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.T1700.MeasureSubsystem.RangeMode)
                If subsystem.RangeMode.HasValue Then
                    Me._RangeComboBox.SilentSelectItem(subsystem.RangeMode.Value.Description)
                    Me._RangeComboBox.Refresh()
                End If
            Case NameOf(VI.T1700.MeasureSubsystem.TriggerMode)
                If subsystem.TriggerMode.HasValue Then
                    Me._TriggerCombo.SilentSelectItem(subsystem.TriggerMode.Value.Description)
                    Me._TriggerCombo.Refresh()
                End If
        End Select
    End Sub

    ''' <summary> measure subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            If Me.InvokeRequired Then
                activity = $"invoking {NameOf(T1700.MeasureSubsystem)}.{e.PropertyName} change"
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.MeasureSubsystemPropertyChanged), New Object() {sender, e})
            Else
                activity = $"handling {NameOf(T1700.MeasureSubsystem)}.{e.PropertyName} change"
                Me.HandlePropertyChanged(TryCast(sender, T1700.MeasureSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: CONFIGURE "

    ''' <summary> Event handler. Called by _ConfigureButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConfigureButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ConfigureButton.Click
        Dim range As ResistanceRangeMode = ResistanceRangeMode.R0
        Dim trigger As TriggerMode = TriggerMode.T0
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} configuring Range {range} Trigger {trigger}"
            range = CType([Enum].Parse(GetType(ResistanceRangeMode), Me._RangeComboBox.SelectedValue.ToString), ResistanceRangeMode)
            Me.Device.MeasureSubsystem.ApplyRangeMode(range)
            trigger = CType([Enum].Parse(GetType(TriggerMode), Me._TriggerCombo.SelectedValue.ToString), TriggerMode)
            Me.Device.MeasureSubsystem.ApplyTriggerMode(trigger)
        Catch ex As Exception
            Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
        Finally
            Me.Device.Session.ReadStatusRegister() ' was Me.Device.ReadRegisters(), which seems unnecessary
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
