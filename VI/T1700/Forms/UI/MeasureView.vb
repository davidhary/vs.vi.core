Imports System.ComponentModel
Imports isr.Core.EnumExtensions
Imports isr.Core.WinForms.ComboBoxExtensions
Imports isr.VI.Facade.ComboBoxExtensions
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.VI.ExceptionExtensions
Imports isr.Core.TimeSpanExtensions

''' <summary> A measure view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class MeasureView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        ' Add any initialization after the InitializeComponent() call.
        ' populate the range mode selector
        Me.ListNonAutoRangeModes()
        ' populate the emulated reply combo.
        Me.ListOneShotTriggerModes()
        Me.SelectMeterTrigger(TriggerMode.T3)

    End Sub

    ''' <summary> Creates a new <see cref="MeasureView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="MeasureView"/>. </returns>
    Public Shared Function Create() As MeasureView
        Dim view As MeasureView = Nothing
        Try
            view = New MeasureView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As T1700Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As T1700Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As T1700Device)
        If Me._Device IsNot Nothing Then
            RemoveHandler Me.Device.Initialized, AddressOf Me.DeviceInitialized
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
            AddHandler Me.Device.Initialized, AddressOf Me.DeviceInitialized
        End If
        Me.BindMeasureSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As T1700Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Initializes the device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub InitializeDevice()
        Me.DeviceInitialized(Me.Device, System.EventArgs.Empty)
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    Protected Sub DeviceInitialized(ByVal sender As T1700Device, ByVal e As System.EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.ApplyMeterTriggerMode()
        Me.ApplyMeterRangeMode()
        Me.ApplyTriggerDelay()
        If Me.Device.IsDeviceOpen Then
            Me.PublishVerbose($"Setting initial delay to {Me.InitialDelay.TotalMilliseconds}ms;. ")
            Me.Device.MeasureSubsystem.InitialDelay = Me.InitialDelay
            Me.PublishVerbose($"Setting measurement delay to {Me.MeasurementDelay.TotalMilliseconds}ms;. ")
            Me.Device.MeasureSubsystem.MeasurementDelay = Me.MeasurementDelay
            Me.PublishVerbose($"Setting maximum trial count to {Me.MaximumTrialsCount};. ")
            Me.Device.MeasureSubsystem.MaximumTrialsCount = Me.MaximumTrialsCount
            Me.PublishVerbose($"Setting maximum difference to {100 * Me.MaximumDifference}%;. ")
            Me.Device.MeasureSubsystem.MaximumDifference = Me.MaximumDifference
        End If
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DeviceInitialized(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} handling device initialized event"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, EventArgs)(AddressOf Me.DeviceInitialized), New Object() {sender, e})
            Else
                Me.DeviceInitialized(TryCast(sender, T1700Device), e)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " MEASURE SUBSYSTEM "

    ''' <summary> Gets the measure subsystem. </summary>
    ''' <value> The measure subsystem. </value>
    Private ReadOnly Property MeasureSubsystem As MeasureSubsystem

    ''' <summary> Bind measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindMeasureSubsystem(ByVal device As T1700Device)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._MeasureSubsystem = device.MeasureSubsystem
            Me.BindSubsystem(True, Me.MeasureSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MeasureSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
            Dim int As Boolean = Me.InitializingComponents
            Me.InitializingComponents = True
            Me._MeterRangeComboBox.ListResistanceRangeCurrents(subsystem.ResistanceRangeCurrents, New Integer() {0})
            Me.InitializingComponents = int
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Measure subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MeasureSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.T1700.MeasureSubsystem.RangeMode)
                If subsystem.RangeMode.HasValue Then
                    Me.SelectMeterRange(subsystem.RangeMode.Value.Description)
                End If
            Case NameOf(VI.T1700.MeasureSubsystem.TriggerMode)
                If subsystem.TriggerMode.HasValue Then
                    Me.SelectMeterTrigger(subsystem.TriggerMode.Value.Description)
                End If
            Case NameOf(VI.T1700.MeasureSubsystem.TriggerDelay)
                If subsystem.TriggerDelay.HasValue Then
                    Me.TriggerDelay = subsystem.TriggerDelay.Value
                End If
            Case NameOf(VI.T1700.MeasureSubsystem.InitialDelay)
                Me.InitialDelay = subsystem.InitialDelay
            Case NameOf(VI.T1700.MeasureSubsystem.MeasurementDelay)
                Me.MeasurementDelay = subsystem.MeasurementDelay
            Case NameOf(VI.T1700.MeasureSubsystem.MaximumTrialsCount)
                Me.MaximumTrialsCount = subsystem.MaximumTrialsCount
            Case NameOf(VI.T1700.MeasureSubsystem.MaximumDifference)
                Me.MaximumDifference = CDec(subsystem.MaximumDifference)
        End Select
    End Sub

    ''' <summary> Measure subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(MeasureSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.MeasureSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, MeasureSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary> Applies the meter trigger mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ApplyMeterTriggerMode()
        If Me.Device?.IsDeviceOpen Then
            If Me.Device.MeasureSubsystem IsNot Nothing AndAlso Not Nullable.Equals(Me.Device.MeasureSubsystem.TriggerMode, Me.SelectedMeterTrigger) Then
                Me.PublishVerbose("Applying meter trigger {0};. ", Me.SelectedMeterTrigger)
                Me.Device.MeasureSubsystem.ApplyTriggerMode(Me.SelectedMeterTrigger)
                Me.Device.Session.ReadStatusRegister()
                ' a delay is required between the two settings
                Me.PublishVerbose($"Waiting {MeasureSubsystem.RangeSettlingTimeMilliseconds}ms for meter trigger to settle;. ")
                TimeSpan.FromMilliseconds(MeasureSubsystem.RangeSettlingTimeMilliseconds).SpinWait()
            End If
        End If
    End Sub

    ''' <summary> Applies the meter range mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ApplyMeterRangeMode()
        If Me.Device?.IsDeviceOpen Then
            If Me.Device.MeasureSubsystem IsNot Nothing AndAlso Not Nullable.Equals(Me.Device.MeasureSubsystem.RangeMode, Me.SelectedMeterRange) Then
                Me.PublishVerbose("Applying meter range settings;. range={0}; current={1}, mode='{2}'",
                                  Me._MeterCurrentNumeric.Value, Me._MeterRangeNumeric.Value, Me.SelectedMeterRange)
                Me.Device.MeasureSubsystem.ApplyRangeMode(Me.SelectedMeterRange)
                Me.Device.Session.ReadStatusRegister()
                ' a delay is required between the two settings
                Me.PublishVerbose($"Waiting {MeasureSubsystem.RangeSettlingTimeMilliseconds}ms for meter trigger to settle;. ")
                ' at this point, 2014-01-31, the first reading comes in too quickly. Trying to detect operation completion using bit 16 of the
                ' service register does not work. So we are resorting to a brute force delay.
                TimeSpan.FromMilliseconds(MeasureSubsystem.RangeSettlingTimeMilliseconds).SpinWait()
            End If
        End If
    End Sub

    ''' <summary> Applies the meter trigger delay. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ApplyTriggerDelay()
        If Me.Device?.IsDeviceOpen Then
            If Me.Device.MeasureSubsystem IsNot Nothing AndAlso Not Nullable.Equals(Me.Device.MeasureSubsystem.TriggerDelay, Me.TriggerDelay) Then
                Me.PublishVerbose("Applying trigger delay {0} ms;. ", Me.TriggerDelay.TotalMilliseconds)
                Me.Device.MeasureSubsystem.ApplyTriggerDelay(Me.TriggerDelay)
                Me.Device.Session.ReadStatusRegister()
                ' a delay is required between the two settings
                Me.PublishVerbose($"Waiting {MeasureSubsystem.RangeSettlingTimeMilliseconds}ms for meter trigger to settle;. ")
                ' at this point, 2014-01-31, the first reading comes in too quickly. Trying to detect operation completion using bit 16 of the
                ' service register does not work. So we are resorting to a brute force delay.
                TimeSpan.FromMilliseconds(MeasureSubsystem.RangeSettlingTimeMilliseconds).SpinWait()
            End If
        End If
    End Sub

#End Region

#Region " RANGE "

    ''' <summary> List range modes. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ListRangeModes()
        ' populate the range mode selector
        Me._MeterRangeComboBox.Enabled = False
        Me._MeterRangeComboBox.DataSource = Nothing
        Me._MeterRangeComboBox.Items.Clear()
        Me._MeterRangeComboBox.DataSource = GetType(ResistanceRangeMode).ValueDescriptionPairs.ToList
        Me._MeterRangeComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._MeterRangeComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._MeterRangeComboBox.Enabled = True
    End Sub

    ''' <summary> List range modes. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub ListRangeModes(ByVal values As ResistanceRangeMode())
        Dim keyValuePairs As New ArrayList()
        If values IsNot Nothing AndAlso values.Count > 0 Then
            For Each value As ResistanceRangeMode In values
                keyValuePairs.Add(value.ValueDescriptionPair())
            Next
        End If
        Me._MeterRangeComboBox.Enabled = False
        Me._MeterRangeComboBox.DataSource = Nothing
        Me._MeterRangeComboBox.Items.Clear()
        Me._MeterRangeComboBox.DataSource = keyValuePairs
        Me._MeterRangeComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._MeterRangeComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._MeterRangeComboBox.Enabled = True
    End Sub

    ''' <summary> List all range modes other than auto range. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ListNonAutoRangeModes()
        Dim keyValuePairs As New ArrayList()
        For Each value As ResistanceRangeMode In [Enum].GetValues(GetType(ResistanceRangeMode))
            If value <> ResistanceRangeMode.R0 Then
                keyValuePairs.Add(value.ValueDescriptionPair())
            End If
        Next
        Me._MeterRangeComboBox.Enabled = False
        Me._MeterRangeComboBox.DataSource = Nothing
        Me._MeterRangeComboBox.Items.Clear()
        Me._MeterRangeComboBox.DataSource = keyValuePairs
        Me._MeterRangeComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._MeterRangeComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._MeterRangeComboBox.Enabled = True
    End Sub

    ''' <summary> Gets the selected meter range. </summary>
    ''' <value> The selected meter range. </value>
    Public ReadOnly Property SelectedMeterRange As ResistanceRangeMode
        Get
            Return CType(CType(Me._MeterRangeComboBox.SelectedItem, KeyValuePair(Of System.Enum, String)).Key, ResistanceRangeMode)
        End Get
    End Property

    ''' <summary> Selects the meter range based on the range mode description. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="description"> The description. </param>
    Public Sub SelectMeterRange(ByVal description As String)
        Me._MeterRangeComboBox.SilentSelectItem(description)
        Me._MeterRangeComboBox.Refresh()
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Selects the meter range based on the range mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub SelectMeterRange(ByVal value As ResistanceRangeMode)
        Me._MeterRangeComboBox.SilentSelectValue(value)
        Me._MeterRangeComboBox.Refresh()
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Selects the meter range based on the current and range settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="current"> The current. </param>
    ''' <param name="range">   The range. </param>
    Public Sub SelectMeterRange(ByVal current As Double, ByVal range As Double)
        Dim rangeMode As ResistanceRangeMode = ResistanceRangeMode.R0
        If MeasureSubsystem.TryMatch(current, range, rangeMode) Then
            Me._MeterRangeComboBox.SilentSelectValue(rangeMode)
            Me._MeterRangeComboBox.Refresh()
            isr.Core.ApplianceBase.DoEvents()
        End If
    End Sub

    ''' <summary> Gets or sets the meter current. </summary>
    ''' <value> The meter current. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MeterCurrent As Decimal
        Get
            Return Me._MeterCurrentNumeric.Value
        End Get
        Set(value As Decimal)
            If Not Decimal.Equals(value, Me.MeterCurrent) Then
                Me._MeterCurrentNumeric.SilentValueSetter(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the meter range. </summary>
    ''' <value> The meter range. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MeterRange As Decimal
        Get
            Return Me._MeterRangeNumeric.Value
        End Get
        Set(value As Decimal)
            If Not Decimal.Equals(value, Me.MeterRange) Then
                Me._MeterRangeNumeric.SilentValueSetter(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the range selection as read only. </summary>
    ''' <value> The sentinel indicating if the meter range is read only. </value>
    Public Property RangeReadOnly As Boolean
        Get
            Return Me._MeterRangeComboBox.ReadOnly
        End Get
        Set(value As Boolean)
            If Me.RangeReadOnly <> value Then
                Me._MeterRangeComboBox.ReadOnly = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Event handler. Called by _MeterRangeComboBox for selected value changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MeterRangeComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MeterRangeComboBox.SelectedValueChanged
        If Me._MeterRangeComboBox.Enabled Then
            Dim range As ResistanceRangeMode = CType([Enum].Parse(GetType(ResistanceRangeMode), Me._MeterRangeComboBox.SelectedValue.ToString), ResistanceRangeMode)
            Dim c, r As Double
            If MeasureSubsystem.TryParse(range, c, r) Then
                Me.MeterCurrent = CDec(c)
                ' if both range and current change, binding cause the range to restore to its previous value!
                ' the code below allows the first binding event to complete before issuing the change on the second
                ' binding event.
                For i As Integer = 1 To 10 : isr.Core.ApplianceBase.DoEvents() : Next
                Me.MeterRange = CDec(r)
            End If
        End If
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> List trigger modes. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ListTriggerModes()
        ' populate the emulated reply combo. 
        Me._TriggerCombo.DataSource = Nothing
        Me._TriggerCombo.Items.Clear()
        Me._TriggerCombo.DataSource = GetType(TriggerMode).ValueDescriptionPairs.ToList
        Me._TriggerCombo.SelectedIndex = 0
        Me._TriggerCombo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._TriggerCombo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
    End Sub

    ''' <summary> List one shot trigger modes. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ListOneShotTriggerModes()
        Me.ListTriggerModes(New TriggerMode() {TriggerMode.T1, TriggerMode.T3})
    End Sub

    ''' <summary> List trigger modes. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="values"> The values. </param>
    Public Sub ListTriggerModes(ByVal values As TriggerMode())

        Dim keyValuePairs As New ArrayList()
        If values IsNot Nothing AndAlso values.Count > 0 Then
            For Each value As TriggerMode In values
                keyValuePairs.Add(value.ValueDescriptionPair())
            Next
        End If

        ' populate the emulated reply combo.
        Me._TriggerCombo.DataSource = Nothing
        Me._TriggerCombo.Items.Clear()
        Me._TriggerCombo.DataSource = keyValuePairs
        Me._TriggerCombo.SelectedIndex = 0
        Me._TriggerCombo.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._TriggerCombo.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)

    End Sub

    ''' <summary> Gets the selected meter Trigger Mode. </summary>
    ''' <value> The selected meter Trigger. </value>
    Public ReadOnly Property SelectedMeterTrigger As TriggerMode
        Get
            Return CType(CType(Me._TriggerCombo.SelectedItem, KeyValuePair(Of System.Enum, String)).Key, TriggerMode)
        End Get
    End Property

    ''' <summary> Selects the meter Trigger mode based on the Trigger mode description. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="description"> The description. </param>
    Public Sub SelectMeterTrigger(ByVal description As String)
        Me._TriggerCombo.SilentSelectItem(description)
        Me._TriggerCombo.Refresh()
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Selects the meter Trigger mode based on the Trigger mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub SelectMeterTrigger(ByVal value As TriggerMode)
        Me._TriggerCombo.SilentSelectValue(value)
        Me._TriggerCombo.Refresh()
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> The trigger delay. </summary>
    Private _TriggerDelay As TimeSpan

    ''' <summary> Gets or sets the Trigger Delay. </summary>
    ''' <value> The Trigger Delay. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property TriggerDelay As TimeSpan
        Get
            Return Me._TriggerDelay
        End Get
        Set(value As TimeSpan)
            If Not Decimal.Equals(value, Me.TriggerDelay) Then
                Me._TriggerDelay = value
                Me._TriggerDelayNumeric.SilentValueSetter(value.TotalMilliseconds)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Event handler. Called by _TriggerDelayNumeric for value changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TriggerDelayNumeric_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TriggerDelayNumeric.ValueChanged
        If Me._TriggerDelayNumeric.Enabled Then
            Me.TriggerDelay = TimeSpan.FromMilliseconds(Me._TriggerDelayNumeric.Value)
        End If
    End Sub

#End Region

#Region " VALUES "

    ''' <summary> The initial delay. </summary>
    Private _InitialDelay As TimeSpan

    ''' <summary> Gets or sets the initial delay. </summary>
    ''' <value> The initial delay. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property InitialDelay As TimeSpan
        Get
            Return Me._InitialDelay
        End Get
        Set(value As TimeSpan)
            If Not TimeSpan.Equals(value, Me.InitialDelay) Then
                Me._InitialDelay = value
                Me._InitialDelayNumeric.Value = CDec(Me._InitialDelayNumeric.SilentValueSetter(value.TotalMilliseconds))
                If Me.Device?.IsDeviceOpen Then
                    Me.Device.MeasureSubsystem.InitialDelay = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Event handler. Called by _InitialDelayNumeric for value changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub InitialDelayNumeric_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InitialDelayNumeric.ValueChanged
        If Me._InitialDelayNumeric.Enabled Then
            Me.InitialDelay = TimeSpan.FromMilliseconds(Me._InitialDelayNumeric.Value)
        End If
    End Sub

    ''' <summary> The measurement delay. </summary>
    Private _MeasurementDelay As TimeSpan

    ''' <summary> Gets or sets the Measurement delay. </summary>
    ''' <value> The Measurement delay. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MeasurementDelay As TimeSpan
        Get
            Return Me._MeasurementDelay
        End Get
        Set(value As TimeSpan)
            If Not TimeSpan.Equals(value, Me.MeasurementDelay) Then
                Me._MeasurementDelay = value
                Me._MeasurementDelayNumeric.Value = CDec(Me._MeasurementDelayNumeric.SilentValueSetter(value.TotalMilliseconds))
                If Me.Device?.IsDeviceOpen Then
                    Me.Device.MeasureSubsystem.MeasurementDelay = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Event handler. Called by _MeasurementDelayNumeric for value changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MeasurementDelayNumeric_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MeasurementDelayNumeric.ValueChanged
        If Me._MeasurementDelayNumeric.Enabled Then
            Me.MeasurementDelay = TimeSpan.FromMilliseconds(Me._MeasurementDelayNumeric.Value)
        End If
    End Sub

    ''' <summary> Number of maximum trials. </summary>
    Private _MaximumTrialsCount As Integer

    ''' <summary> Gets or sets the maximum trial count. </summary>
    ''' <value> The maximum number of trials before giving up. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MaximumTrialsCount As Integer
        Get
            Return Me._MaximumTrialsCount
        End Get
        Set(value As Integer)
            If Not Integer.Equals(value, Me.MaximumTrialsCount) Then
                Me._MaximumTrialsCount = value
                Me._MaximumTrialsCountNumeric.Value = CDec(Me._MaximumTrialsCountNumeric.SilentValueSetter(value))
                If Me.Device?.IsDeviceOpen Then
                    Me.Device.MeasureSubsystem.MaximumTrialsCount = value
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Event handler. Called by _MaximumTrialsCountNumeric for value changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MaximumTrialsCountNumeric_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MaximumTrialsCountNumeric.ValueChanged
        If Me._MaximumTrialsCountNumeric.Enabled Then
            Me.MaximumTrialsCount = CInt(Me._MaximumTrialsCountNumeric.Value)
        End If
    End Sub

    ''' <summary> The maximum difference. </summary>
    Private _MaximumDifference As Decimal

    ''' <summary> Gets or sets the maximum difference between consecutive measurements. </summary>
    ''' <value> The maximum difference between consecutive measurements. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MaximumDifference As Decimal
        Get
            Return Me._MaximumDifference
        End Get
        Set(value As Decimal)
            If Not Decimal.Equals(value, Me.MaximumDifference) Then
                Me._MaximumDifference = value
                Me._MaximumDifferenceNumeric.SilentValueSetter(100 * value)
                If Me.Device?.IsDeviceOpen Then
                    Me.Device.MeasureSubsystem.MaximumDifference = Me.MaximumDifference
                End If
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Event handler. Called by _MaximumDifferenceNumeric for value changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MaximumDifferenceNumeric_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MaximumDifferenceNumeric.ValueChanged
        If Me._MaximumDifferenceNumeric.Enabled Then
            Me.MaximumDifference = CDec(0.01 * Me._MaximumDifferenceNumeric.Value)
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
