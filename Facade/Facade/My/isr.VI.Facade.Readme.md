## ISR VI Facade<sub>&trade;</sub>: Base Facade classes for VISA instrument Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*7.3.7644 2020-12-05*  
Converted to C#.

*7.3.7622 2020-11-13*  
Adds digital output view.

*7.3.7464 2020-06-08*  
Adds resource names editor. 
Adds a resource name editor menu item to the Application menu of the status view.

*7.2.7411 2020-04-16*  
Supports streaming Keithley 2002 scan card readings.

*7.2.7375 2020-03-11*  
Stream binning: Sets pass digital output to active low.

*7.2.7367 2020-03-03*  
Visa 19.5. Stream binning: Sets pass digital output to active high.

*7.1.7220 2019-10-08*  
Visa 19.0. Updates all views to using base-class info
provider and tool tip. Updates all views to notify of form closing and
use for updating resource names and titles.

*6.1.6944 2019-01-05*  
New project evolved from the legacy Instrument
project.

\(C\) 2013 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
