using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A scanner view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ScannerView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public ScannerView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ExplainExternalTriggerPlanMenuItem.Name = "_ExplainExternalTriggerPlanMenuItem";
            this.__ConfigureExternalTriggerPlanMenuItem.Name = "_ConfigureExternalTriggerPlanMenuItem";
            this.__ExplainBusTriggerPlanMenuItem.Name = "_ExplainBusTriggerPlanMenuItem";
            this.__ConfigureBusTriggerPlanMenuItem.Name = "_ConfigureBusTriggerPlanMenuItem";
            this.__InitiateButton.Name = "_InitiateButton";
            this.__SendBusTriggerButton.Name = "_SendBusTriggerButton";
            this.__AbortButton.Name = "_AbortButton";
        }

        /// <summary> Creates a new <see cref="ScannerView"/> </summary>
        /// <returns> A <see cref="ScannerView"/>. </returns>
        public static ScannerView Create()
        {
            ScannerView view = null;
            try
            {
                view = new ScannerView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Aborts the trigger plan. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void Abort()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} aborting trigger plan";
                _ = this.PublishInfo( $"{activity};. " );
                this.TriggerSubsystem.StartElapsedStopwatch();
                this.TriggerSubsystem.Abort();
                this.TriggerSubsystem.StopElapsedStopwatch();
                _ = this.Device.Session.QueryOperationCompleted();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Initiates the trigger plan. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void Initiate()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing execution state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                _ = this.Device.Session.QueryOperationCompleted();
                activity = $"{this.Device.ResourceNameCaption} initiating trigger plan";
                _ = this.PublishInfo( $"{activity};. " );
                this.TriggerSubsystem.StartElapsedStopwatch();
                this.TriggerSubsystem.Initiate();
                this.TriggerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
                try
                {
                    activity = $"{this.Device.ResourceNameCaption} aborting trigger plan";
                    this.TriggerSubsystem.Abort();
                }
                catch
                {
                    this.Device.Session.StatusPrompt = $"failed {activity}";
                    activity = this.PublishException( activity, ex );
                    _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
                }
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Sends the bus trigger. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void SendBusTrigger()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} sending bus trigger";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.Session.AssertTrigger();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Builds bus trigger description. </summary>
        /// <returns> A String. </returns>
        public string BuildBusTriggerDescription()
        {
            if ( this.InitializingComponents )
                return string.Empty;
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"Scan plan with bus triggering:" );
            _ = builder.AppendLine( $"Scan list: {this._ScanListView.ScanList}" );
            _ = builder.AppendLine( $"Arm layer 1: Count {1} {ArmSources.Immediate} delay {0}" );
            _ = builder.AppendLine( $"Arm layer 2: Count {1} {ArmSources.Immediate} delay {0}" );
            _ = builder.AppendLine( $"Trigger layer: Source {TriggerSources.Bus} count {9999} delay {0} bypass {TriggerLayerBypassModes.Acceptor}" );
            return builder.ToString();
        }

        /// <summary> Configure bus trigger plan. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ConfigureBusTriggerPlan()
        {
            if ( this.InitializingComponents )
                return;
            string title = this.Device.OpenResourceTitle;
            string propertyName = string.Empty;
            string activity = $"{title} Configuring bus scan trigger plan";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );
                activity = $"{title} aborting trigger plan";
                _ = this.PublishInfo( $"{activity};. " );
                this.TriggerSubsystem.Abort();
                _ = this.Device.Session.QueryOperationCompleted();
                activity = $"{title} clearing execution state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                _ = this.Device.Session.QueryOperationCompleted();
                propertyName = $"{nameof( TriggerSubsystemBase ).SplitWords()}.{nameof( TriggerSubsystemBase.ContinuousEnabled ).SplitWords()}";
                activity = $"{title} setting [{propertyName}]=False";
                _ = this.PublishInfo( $"{activity};. " );
                _ = this.TriggerSubsystem.ApplyContinuousEnabled( false );
                this.TriggerSubsystem.Abort();
                this.Device.Session.EnableServiceRequestWaitComplete();
                _ = this.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
                _ = this.RouteSubsystem.QueryClosedChannels();
                _ = this.RouteSubsystem.ApplyScanList( this._ScanListView.ScanList );
                _ = this.ArmLayer1Subsystem.ApplyArmSource( ArmSources.Immediate );
                _ = this.ArmLayer1Subsystem.ApplyArmCount( 1 );
                _ = this.ArmLayer1Subsystem.ApplyArmLayerBypassMode( TriggerLayerBypassModes.Acceptor );
                _ = this.ArmLayer2Subsystem.ApplyArmSource( ArmSources.Immediate );
                _ = this.ArmLayer2Subsystem.ApplyArmCount( 1 );
                _ = this.ArmLayer2Subsystem.ApplyDelay( TimeSpan.Zero );
                _ = this.TriggerSubsystem.ApplyTriggerSource( TriggerSources.Bus );
                _ = this.TriggerSubsystem.ApplyTriggerCount( 9999 ); // in place of infinite
                _ = this.TriggerSubsystem.ApplyDelay( TimeSpan.Zero );
                _ = this.TriggerSubsystem.ApplyTriggerLayerBypassMode( TriggerLayerBypassModes.Acceptor );
                this.Device.Session.StatusPrompt = "Ready: Initiate (1) Meter; (2) 7001r";
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Builds external trigger description. </summary>
        /// <returns> A String. </returns>
        public string BuildExternalTriggerDescription()
        {
            if ( this.InitializingComponents )
                return string.Empty;
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( "Scan plan with external triggering handshake with a meter:" );
            _ = builder.AppendLine( $"Scan list: {this._ScanListView.ScanList}" );
            _ = builder.AppendLine( $"Arm layer 1: Count {1} {ArmSources.Immediate} delay {0}" );
            _ = builder.AppendLine( $"Arm layer 2: Count {1} {ArmSources.Immediate} delay {0}" );
            _ = builder.AppendLine( $"Trigger layer: Source {TriggerSources.External} count {9999} delay {0} bypass {TriggerLayerBypassModes.Source}" );
            return builder.ToString();
        }

        /// <summary> Configure external trigger plan. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ConfigureExternalTriggerPlan()
        {
            if ( this.InitializingComponents )
                return;
            string activity = $"{this.Device.ResourceNameCaption} Configuring external scan trigger plan";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.Device.ClearExecutionState();
                // enable service requests
                // Me.EnableServiceRequestEventHandler()
                // Me.Device.StatusSubsystem.EnableServiceRequest(Me.Device.Session.DefaultServiceRequestEnableBitmask)

                _ = this.PublishInfo( $"{activity};. " );
                _ = this.TriggerSubsystem.ApplyContinuousEnabled( false );
                this.TriggerSubsystem.Abort();
                this.Device.Session.EnableServiceRequestWaitComplete();
                _ = this.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
                _ = this.RouteSubsystem.QueryClosedChannels();
                _ = this.RouteSubsystem.ApplyScanList( this._ScanListView.ScanList ); // "(@1!1:1!10)"
                _ = this.ArmLayer1Subsystem.ApplyArmSource( ArmSources.Immediate );
                _ = this.ArmLayer1Subsystem.ApplyArmCount( 1 );
                _ = this.ArmLayer2Subsystem.ApplyArmSource( ArmSources.Immediate );
                _ = this.ArmLayer2Subsystem.ApplyArmCount( 1 );
                _ = this.ArmLayer2Subsystem.ApplyDelay( TimeSpan.Zero );
                _ = this.TriggerSubsystem.ApplyTriggerSource( TriggerSources.External );
                _ = this.TriggerSubsystem.ApplyTriggerCount( 9999 ); // in place of infinite
                _ = this.TriggerSubsystem.ApplyDelay( TimeSpan.Zero );
                _ = this.TriggerSubsystem.ApplyTriggerLayerBypassMode( TriggerLayerBypassModes.Source );
                this.Device.Session.StatusPrompt = "Ready: Initiate (1) 7001; (2) Meter";
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device.PropertyChanged -= this.VisaSessionBasePropertyChanged;
                this.Device = null;
            }

            this.Device = value;
            this._SlotView.AssignDevice( value );
            this._ServiceRequestView.AssignDevice( value );
            this._ScanListView.AssignDevice( value );
            if ( value is object )
            {
                this.Device.PropertyChanged += this.VisaSessionBasePropertyChanged;
                this.HandlePropertyChanged( this.Device, nameof( VisaSessionBase.OpenResourceTitle ) );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handle the visa session base property changed event. </summary>
        /// <param name="device">       The visa session base. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( VisaSessionBase device, string propertyName )
        {
            if ( device is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( VisaSessionBase.OpenResourceTitle ):
                    {
                        this._SubsystemSplitButton.Text = device.OpenResourceTitle;
                        break;
                    }
            }
        }

        /// <summary> visa session base  property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void VisaSessionBasePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( VisaSessionBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.VisaSessionBasePropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.VisaSessionBasePropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as VisaSessionBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " RELAY VIEW "

        /// <summary> Handle the relay view property changed event. </summary>
        /// <param name="view">         The view. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( RelayView view, string propertyName )
        {
            if ( view is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( RelayView.ChannelList ):
                    {
                        this._InfoTextBox.Text = $"Channel List: {view.ChannelList}";
                        break;
                    }
            }
        }

        /// <summary> Relay view property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RelayViewPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.RouteSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.RelayViewPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.RelayViewPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as RelayView, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SCAN LIST VIEW "

        /// <summary> Handle the ScanList view property changed event. </summary>
        /// <param name="view">         The view. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ScanListView view, string propertyName )
        {
            if ( view is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ScanListView.ScanList ):
                    {
                        this._InfoTextBox.Text = $"Scan List: {view.ScanList}";
                        break;
                    }
            }
        }

        /// <summary> ScanList view property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ScanListViewPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( ScanListView )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ScanListViewPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.ScanListViewPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ScanListView, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " ROUTE SUBSYSTEM "

        /// <summary> Gets or sets the route subsystem. </summary>
        /// <value> The route  subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public RouteSubsystemBase RouteSubsystem { get; private set; }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected virtual void BindSubsystem( RouteSubsystemBase subsystem )
        {
            this._SlotView.BindSubsystem( subsystem );
            this._ScanListView.BindSubsystem( subsystem );
            if ( subsystem is null )
            {
                this.RouteSubsystem = null;
                this._RelayView.PropertyChanged -= this.RelayViewPropertyChanged;
                this._ScanListView.PropertyChanged -= this.ScanListViewPropertyChanged;
            }
            else
            {
                this.RouteSubsystem = subsystem;
                this._RelayView.PropertyChanged += this.RelayViewPropertyChanged;
                this._ScanListView.PropertyChanged += this.ScanListViewPropertyChanged;
                this.HandlePropertyChanged( this._RelayView, nameof( RelayView.ChannelList ) );
            }
        }

        #endregion

        #region " ARM LAYER SUBSYSTEMs "

        /// <summary> Gets or sets the arm layer 1 subsystem. </summary>
        /// <value> The arm layer 1 subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmLayerSubsystemBase ArmLayer1Subsystem { get; private set; }

        /// <summary> Gets or sets the arm layer 2 subsystem. </summary>
        /// <value> The arm layer 2 subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmLayerSubsystemBase ArmLayer2Subsystem { get; private set; }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem">   The subsystem. </param>
        /// <param name="layerNumber"> The layer number. </param>
        protected void BindSubsystem( ArmLayerSubsystemBase subsystem, int layerNumber )
        {
            if ( layerNumber == 1 )
            {
                this.BindSubsystem1( subsystem );
            }
            else
            {
                this.BindSubsystem2( subsystem );
            }
        }

        /// <summary> Bind subsystem 1. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected void BindSubsystem1( ArmLayerSubsystemBase subsystem )
        {
            if ( this.ArmLayer1Subsystem is object )
            {
                this.ArmLayer1Subsystem = null;
            }

            this.ArmLayer1Subsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }

        /// <summary> Bind subsystem 2. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected void BindSubsystem2( ArmLayerSubsystemBase subsystem )
        {
            if ( this.ArmLayer2Subsystem is object )
            {
                this.ArmLayer2Subsystem = null;
            }

            this.ArmLayer2Subsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }

        #endregion

        #region " TRIGGER SUBSYSTEM "

        /// <summary> Gets or sets the trigger subsystem. </summary>
        /// <value> The trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystemBase TriggerSubsystem { get; private set; }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected void BindSubsystem( TriggerSubsystemBase subsystem )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.TriggerSubsystem = null;
            }

            this.TriggerSubsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Initiate click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void InitiateButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.Initiate();
        }

        /// <summary> Abort button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AbortButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.Abort();
        }

        /// <summary> Sends the bus trigger button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SendBusTriggerButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.SendBusTrigger();
        }

        /// <summary> Explain external trigger plan menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExplainExternalTriggerPlanMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} building external scan trigger plan description";
            string description = string.Empty;
            try
            {
                description = this.BuildExternalTriggerDescription();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this._InfoTextBox.Text = description;
            }
        }

        /// <summary> Configure external trigger plan menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureExternalTriggerPlanMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ConfigureExternalTriggerPlan();
        }

        /// <summary> Explain bus trigger plan menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExplainBusTriggerPlanMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} building bus trigger plan description";
            string description = string.Empty;
            try
            {
                description = this.BuildBusTriggerDescription();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this._InfoTextBox.Text = description;
            }
        }

        /// <summary> Configure bus trigger plan menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureBusTriggerPlanMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ConfigureBusTriggerPlan();
        }

        #endregion

        #region " TALKER "

        /// <summary> Assigns talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( Core.ITraceMessageTalker talker )
        {
            this._SlotView.AssignTalker( talker );
            this._ServiceRequestView.AssignTalker( talker );
            this._RelayView.AssignTalker( talker );
            this._ScanListView.AssignTalker( talker );
            // assigned last as this identifies all talkers.
            base.AssignTalker( talker );
        }

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
