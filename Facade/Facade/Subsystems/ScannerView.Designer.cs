﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class ScannerView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ScannerView));
            _Layout = new TableLayoutPanel();
            _RelayView = new RelayView();
            _ScanListView = new ScanListView();
            _SlotView = new SlotView();
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            _ExternalTriggerPlanToolMenuItem = new ToolStripMenuItem();
            __ExplainExternalTriggerPlanMenuItem = new ToolStripMenuItem();
            __ExplainExternalTriggerPlanMenuItem.Click += new EventHandler(ExplainExternalTriggerPlanMenuItem_Click);
            __ConfigureExternalTriggerPlanMenuItem = new ToolStripMenuItem();
            __ConfigureExternalTriggerPlanMenuItem.Click += new EventHandler(ConfigureExternalTriggerPlanMenuItem_Click);
            _BusTriggerPlanMenuItem = new ToolStripMenuItem();
            __ExplainBusTriggerPlanMenuItem = new ToolStripMenuItem();
            __ExplainBusTriggerPlanMenuItem.Click += new EventHandler(ExplainBusTriggerPlanMenuItem_Click);
            __ConfigureBusTriggerPlanMenuItem = new ToolStripMenuItem();
            __ConfigureBusTriggerPlanMenuItem.Click += new EventHandler(ConfigureBusTriggerPlanMenuItem_Click);
            __InitiateButton = new ToolStripButton();
            __InitiateButton.Click += new EventHandler(InitiateButton_Click);
            __SendBusTriggerButton = new ToolStripButton();
            __SendBusTriggerButton.Click += new EventHandler(SendBusTriggerButton_Click);
            __AbortButton = new ToolStripButton();
            __AbortButton.Click += new EventHandler(AbortButton_Click);
            _ServiceRequestView = new ServiceRequestView();
            _InfoTextBox = new TextBox();
            _Layout.SuspendLayout();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 2;
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 2.0f));
            _Layout.Controls.Add(_RelayView, 0, 4);
            _Layout.Controls.Add(_ScanListView, 0, 3);
            _Layout.Controls.Add(_SlotView, 0, 1);
            _Layout.Controls.Add(_SubsystemToolStrip, 0, 0);
            _Layout.Controls.Add(_ServiceRequestView, 0, 2);
            _Layout.Controls.Add(_InfoTextBox, 0, 5);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 6;
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.Size = new System.Drawing.Size(421, 324);
            _Layout.TabIndex = 0;
            // 
            // _RelayView
            // 
            _RelayView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _RelayView.Dock = DockStyle.Top;
            _RelayView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _RelayView.Location = new System.Drawing.Point(3, 132);
            _RelayView.Name = "_RelayView";
            _RelayView.Padding = new Padding(1);
            _RelayView.Size = new System.Drawing.Size(413, 28);
            _RelayView.TabIndex = 17;
            // 
            // _ScanListView
            // 
            _ScanListView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _ScanListView.Dock = DockStyle.Top;
            _ScanListView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ScanListView.Location = new System.Drawing.Point(3, 100);
            _ScanListView.Name = "_ScanListView";
            _ScanListView.Padding = new Padding(1);
            _ScanListView.Size = new System.Drawing.Size(413, 26);
            _ScanListView.TabIndex = 16;
            // 
            // _SlotView
            // 
            _SlotView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _SlotView.Dock = DockStyle.Top;
            _SlotView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SlotView.Location = new System.Drawing.Point(3, 28);
            _SlotView.Name = "_SlotView";
            _SlotView.Padding = new Padding(1);
            _SlotView.Size = new System.Drawing.Size(413, 31);
            _SlotView.TabIndex = 1;
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, __InitiateButton, __SendBusTriggerButton, __AbortButton });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(419, 25);
            _SubsystemToolStrip.TabIndex = 2;
            _SubsystemToolStrip.Text = "Subsystem Tool strip";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _ExternalTriggerPlanToolMenuItem, _BusTriggerPlanMenuItem });
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(65, 22);
            _SubsystemSplitButton.Text = "Scanner";
            _SubsystemSplitButton.ToolTipText = "Device options and actions";
            // 
            // _ExternalTriggerPlanToolMenuItem
            // 
            _ExternalTriggerPlanToolMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __ExplainExternalTriggerPlanMenuItem, __ConfigureExternalTriggerPlanMenuItem });
            _ExternalTriggerPlanToolMenuItem.Name = "_ExternalTriggerPlanToolMenuItem";
            _ExternalTriggerPlanToolMenuItem.Size = new System.Drawing.Size(190, 22);
            _ExternalTriggerPlanToolMenuItem.Text = "External Trigger Plan...";
            _ExternalTriggerPlanToolMenuItem.ToolTipText = "Configures an external trigger plan";
            // 
            // _ExplainExternalTriggerPlanMenuItem
            // 
            __ExplainExternalTriggerPlanMenuItem.Name = "__ExplainExternalTriggerPlanMenuItem";
            __ExplainExternalTriggerPlanMenuItem.Size = new System.Drawing.Size(127, 22);
            __ExplainExternalTriggerPlanMenuItem.Text = "Info";
            __ExplainExternalTriggerPlanMenuItem.ToolTipText = "Displays information about the external trigger plan";
            // 
            // _ConfigureExternalTriggerPlanMenuItem
            // 
            __ConfigureExternalTriggerPlanMenuItem.Name = "__ConfigureExternalTriggerPlanMenuItem";
            __ConfigureExternalTriggerPlanMenuItem.Size = new System.Drawing.Size(127, 22);
            __ConfigureExternalTriggerPlanMenuItem.Text = "Configure";
            __ConfigureExternalTriggerPlanMenuItem.ToolTipText = "Configures an external trigger plan";
            // 
            // _BusTriggerPlanMenuItem
            // 
            _BusTriggerPlanMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __ExplainBusTriggerPlanMenuItem, __ConfigureBusTriggerPlanMenuItem });
            _BusTriggerPlanMenuItem.Name = "_BusTriggerPlanMenuItem";
            _BusTriggerPlanMenuItem.Size = new System.Drawing.Size(190, 22);
            _BusTriggerPlanMenuItem.Text = "Bus Trigger Plan...";
            _BusTriggerPlanMenuItem.ToolTipText = "Configures a bus trigger plan";
            // 
            // _ExplainBusTriggerPlanMenuItem
            // 
            __ExplainBusTriggerPlanMenuItem.Name = "__ExplainBusTriggerPlanMenuItem";
            __ExplainBusTriggerPlanMenuItem.Size = new System.Drawing.Size(127, 22);
            __ExplainBusTriggerPlanMenuItem.Text = "Info";
            __ExplainBusTriggerPlanMenuItem.ToolTipText = "Displays information about the simple trigger plan";
            // 
            // _ConfigureBusTriggerPlanMenuItem
            // 
            __ConfigureBusTriggerPlanMenuItem.Name = "__ConfigureBusTriggerPlanMenuItem";
            __ConfigureBusTriggerPlanMenuItem.Size = new System.Drawing.Size(127, 22);
            __ConfigureBusTriggerPlanMenuItem.Text = "Configure";
            __ConfigureBusTriggerPlanMenuItem.ToolTipText = "Configures a bus trigger plan";
            // 
            // _InitiateButton
            // 
            __InitiateButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __InitiateButton.Image = (System.Drawing.Image)resources.GetObject("_InitiateButton.Image");
            __InitiateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __InitiateButton.Name = "__InitiateButton";
            __InitiateButton.Size = new System.Drawing.Size(47, 22);
            __InitiateButton.Text = "Initiate";
            __InitiateButton.ToolTipText = "Starts the trigger plan";
            // 
            // _SendBusTriggerButton
            // 
            __SendBusTriggerButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __SendBusTriggerButton.Image = (System.Drawing.Image)resources.GetObject("_SendBusTriggerButton.Image");
            __SendBusTriggerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __SendBusTriggerButton.Name = "__SendBusTriggerButton";
            __SendBusTriggerButton.Size = new System.Drawing.Size(43, 22);
            __SendBusTriggerButton.Text = "Assert";
            __SendBusTriggerButton.ToolTipText = "Sends a bus trigger to the instrument";
            // 
            // _AbortButton
            // 
            __AbortButton.Alignment = ToolStripItemAlignment.Right;
            __AbortButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AbortButton.Image = (System.Drawing.Image)resources.GetObject("_AbortButton.Image");
            __AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AbortButton.Margin = new Padding(10, 1, 0, 2);
            __AbortButton.Name = "__AbortButton";
            __AbortButton.Size = new System.Drawing.Size(41, 22);
            __AbortButton.Text = "Abort";
            __AbortButton.ToolTipText = "Aborts a trigger plan";
            // 
            // _ServiceRequestView
            // 
            _ServiceRequestView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _ServiceRequestView.Dock = DockStyle.Top;
            _ServiceRequestView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ServiceRequestView.Location = new System.Drawing.Point(3, 65);
            _ServiceRequestView.Name = "_ServiceRequestView";
            _ServiceRequestView.Padding = new Padding(1);
            _ServiceRequestView.Size = new System.Drawing.Size(413, 29);
            _ServiceRequestView.TabIndex = 3;
            // 
            // _InfoTextBox
            // 
            _InfoTextBox.Dock = DockStyle.Fill;
            _InfoTextBox.Location = new System.Drawing.Point(3, 166);
            _InfoTextBox.Multiline = true;
            _InfoTextBox.Name = "_InfoTextBox";
            _InfoTextBox.ReadOnly = true;
            _InfoTextBox.ScrollBars = ScrollBars.Both;
            _InfoTextBox.Size = new System.Drawing.Size(413, 155);
            _InfoTextBox.TabIndex = 4;
            _InfoTextBox.Text = "<info>";
            // 
            // ScannerView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "ScannerView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(423, 326);
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
        }

        private TableLayoutPanel _Layout;
        private ToolStrip _SubsystemToolStrip;
        private ToolStripSplitButton _SubsystemSplitButton;
        private SlotView _SlotView;
        private TextBox _InfoTextBox;
        private ToolStripMenuItem _ExternalTriggerPlanToolMenuItem;
        private ToolStripMenuItem __ExplainExternalTriggerPlanMenuItem;

        private ToolStripMenuItem _ExplainExternalTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExplainExternalTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExplainExternalTriggerPlanMenuItem != null)
                {
                    __ExplainExternalTriggerPlanMenuItem.Click -= ExplainExternalTriggerPlanMenuItem_Click;
                }

                __ExplainExternalTriggerPlanMenuItem = value;
                if (__ExplainExternalTriggerPlanMenuItem != null)
                {
                    __ExplainExternalTriggerPlanMenuItem.Click += ExplainExternalTriggerPlanMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ConfigureExternalTriggerPlanMenuItem;

        private ToolStripMenuItem _ConfigureExternalTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConfigureExternalTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConfigureExternalTriggerPlanMenuItem != null)
                {
                    __ConfigureExternalTriggerPlanMenuItem.Click -= ConfigureExternalTriggerPlanMenuItem_Click;
                }

                __ConfigureExternalTriggerPlanMenuItem = value;
                if (__ConfigureExternalTriggerPlanMenuItem != null)
                {
                    __ConfigureExternalTriggerPlanMenuItem.Click += ConfigureExternalTriggerPlanMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem _BusTriggerPlanMenuItem;
        private ToolStripMenuItem __ExplainBusTriggerPlanMenuItem;

        private ToolStripMenuItem _ExplainBusTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExplainBusTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExplainBusTriggerPlanMenuItem != null)
                {
                    __ExplainBusTriggerPlanMenuItem.Click -= ExplainBusTriggerPlanMenuItem_Click;
                }

                __ExplainBusTriggerPlanMenuItem = value;
                if (__ExplainBusTriggerPlanMenuItem != null)
                {
                    __ExplainBusTriggerPlanMenuItem.Click += ExplainBusTriggerPlanMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ConfigureBusTriggerPlanMenuItem;

        private ToolStripMenuItem _ConfigureBusTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConfigureBusTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConfigureBusTriggerPlanMenuItem != null)
                {
                    __ConfigureBusTriggerPlanMenuItem.Click -= ConfigureBusTriggerPlanMenuItem_Click;
                }

                __ConfigureBusTriggerPlanMenuItem = value;
                if (__ConfigureBusTriggerPlanMenuItem != null)
                {
                    __ConfigureBusTriggerPlanMenuItem.Click += ConfigureBusTriggerPlanMenuItem_Click;
                }
            }
        }

        private ToolStripButton __InitiateButton;

        private ToolStripButton _InitiateButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitiateButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitiateButton != null)
                {
                    __InitiateButton.Click -= InitiateButton_Click;
                }

                __InitiateButton = value;
                if (__InitiateButton != null)
                {
                    __InitiateButton.Click += InitiateButton_Click;
                }
            }
        }

        private ToolStripButton __SendBusTriggerButton;

        private ToolStripButton _SendBusTriggerButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SendBusTriggerButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SendBusTriggerButton != null)
                {
                    __SendBusTriggerButton.Click -= SendBusTriggerButton_Click;
                }

                __SendBusTriggerButton = value;
                if (__SendBusTriggerButton != null)
                {
                    __SendBusTriggerButton.Click += SendBusTriggerButton_Click;
                }
            }
        }

        private ToolStripButton __AbortButton;

        private ToolStripButton _AbortButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortButton != null)
                {
                    __AbortButton.Click -= AbortButton_Click;
                }

                __AbortButton = value;
                if (__AbortButton != null)
                {
                    __AbortButton.Click += AbortButton_Click;
                }
            }
        }

        private ServiceRequestView _ServiceRequestView;
        private ScanListView _ScanListView;
        private RelayView _RelayView;
    }
}