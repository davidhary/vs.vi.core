using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A Calculate Subsystem view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class BinningView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public BinningView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._PassBitPatternNumeric.NumericUpDownControl.Minimum = 1m;
            this._PassBitPatternNumeric.NumericUpDownControl.Maximum = 63m;
            this._PassBitPatternNumeric.NumericUpDownControl.Value = 32m;
            this._PassBitPatternNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._BinningStrobeDurationNumeric.NumericUpDownControl.Minimum = 0m;
            this._BinningStrobeDurationNumeric.NumericUpDownControl.Maximum = 999m;
            this._BinningStrobeDurationNumeric.NumericUpDownControl.Value = 0m;
            this._BinningStrobeDurationNumeric.NumericUpDownControl.DecimalPlaces = 2;
            this.__ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
            this.__PreformLimitTestMenuItem.Name = "_PreformLimitTestMenuItem";
            this.__ReadLimitTestStateMenuItem.Name = "_ReadLimitTestStateMenuItem";
            this.__PassBitPatternNumericButton.Name = "_PassBitPatternNumericButton";
            this.__BinningStobeToggleButton.Name = "_BinningStobeToggleButton";
            this.__LimitFailedButton.Name = "_LimitFailedButton";
        }

        /// <summary> Creates a new Calculate View. </summary>
        /// <returns> A Calculate View. </returns>
        public static BinningView Create()
        {
            BinningView view = null;
            try
            {
                view = new BinningView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Applies the settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying Binning subsystem state";
                _ = this.PublishInfo( $"{activity};. " );
                this.BinningSubsystem.StartElapsedStopwatch();
                _ = this.BinningSubsystem.ApplyPassSource( ( int ) this._PassBitPatternNumeric.Value );
                _ = this.BinningSubsystem.ApplyBinningStrobeEnabled( this._BinningStobeToggleButton.CheckState == CheckState.Checked );
                this.BinningSubsystem.StopElapsedStopwatch();
                this.ApplySettings( this.DigitalOutputSubsystem );
                this._Limit1View.ApplySettings();
                this._Limit2View.ApplySettings();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads the settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} reading Binning subsystem state";
                _ = this.PublishInfo( $"{activity};. " );
                ReadSettings( this.BinningSubsystem );
                this.ApplyPropertyChanged( this.BinningSubsystem );
                ReadSettings( this.DigitalOutputSubsystem );
                this.ApplyPropertyChanged( this.DigitalOutputSubsystem );
                this._Limit1View.ReadSettings();
                this._Limit2View.ReadSettings();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Preform a limit test and read the fail state. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void PerformLimitTest()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} perform Binning limit test";
                _ = this.PublishInfo( $"{activity};. " );
                PerformLimitTest( this.BinningSubsystem );
                this.ReadLimitTestState();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> read limit test result. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadLimitTestState()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.BinningSubsystem.StartElapsedStopwatch();
                _ = this.BinningSubsystem.QueryLimitsFailed();
                _ = this.BinningSubsystem.QueryLimit1Failed();
                _ = this.BinningSubsystem.QueryLimit2Failed();
                _ = this.Device.StatusSubsystemBase.QueryMeasurementEventStatus();
                this.BinningSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( BinningView ).SplitWords()}" );
            }

            this._Limit1View.AssignDevice( value );
            this._Limit2View.AssignDevice( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " BINNING "

        /// <summary> Gets or sets the <see cref="BinningSubsystemBase"/>. </summary>
        /// <value> The <see cref="BinningSubsystemBase"/>. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public BinningSubsystemBase BinningSubsystem { get; private set; }

        /// <summary> Binds the <see cref="BinningSubsystemBase"/> subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( BinningSubsystemBase subsystem )
        {
            if ( this.BinningSubsystem is object )
            {
                this.BindSubsystem( false, this.BinningSubsystem );
                this.BinningSubsystem = null;
            }

            this.BinningSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, subsystem );
            }

            this._Limit1View.BindSubsystem( subsystem, 1 );
            this._Limit2View.BindSubsystem( subsystem, 2 );
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, BinningSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.BinningSubsystemBasePropertyChanged;
                // must Not read setting when biding because the instrument may be locked Or in a trigger mode
                // The bound values should be sent when binding Or when applying propert change.
                // ReadSettings( subsystem );
                // ReadLimitTestState( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.BinningSubsystemBasePropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( BinningSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.BinningStrobeEnabled ) );
            this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.LimitsFailed ) );
            this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.PassSource ) );
        }

        /// <summary> Tests perform limit. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void PerformLimitTest( BinningSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            subsystem.Immediate();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> read limit test result. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadLimitTestState( BinningSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryLimitsFailed();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Reads the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( BinningSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryPassSource();
            _ = subsystem.QueryBinningStrobeEnabled();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Handle the Binning subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( BinningSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( BinningSubsystemBase.BinningStrobeEnabled ):
                    {
                        this._BinningStobeToggleButton.CheckState = subsystem.BinningStrobeEnabled.ToCheckState();
                        break;
                    }

                case nameof( BinningSubsystemBase.PassSource ):
                    {
                        if ( subsystem.PassSource.HasValue )
                            this._PassBitPatternNumeric.Value = subsystem.PassSource.Value;
                        break;
                    }

                case nameof( BinningSubsystemBase.LimitsFailed ):
                    {
                        this._LimitFailedButton.CheckState = subsystem.LimitsFailed.ToCheckState();
                        break;
                    }

                case nameof( BinningSubsystemBase.BinningStrobeDuration ):
                    {
                        this._BinningStrobeDurationNumeric.Value = ( decimal ) (subsystem.BinningStrobeDuration.Ticks / ( double ) TimeSpan.TicksPerMillisecond);
                        break;
                    }
            }
        }

        /// <summary> Binning subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void BinningSubsystemBasePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( BinningSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.BinningSubsystemBasePropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.BinningSubsystemBasePropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as BinningSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DIGITAL OUTPUT SUBSYSTEM "

        /// <summary> Gets or sets the digital output subsystem. </summary>
        /// <value> The digital output subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DigitalOutputSubsystemBase DigitalOutputSubsystem { get; private set; }

        /// <summary> Bind ArmLayer2 subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( DigitalOutputSubsystemBase subsystem )
        {
            if ( this.DigitalOutputSubsystem is object )
            {
                this.DigitalOutputSubsystem = null;
            }

            this.DigitalOutputSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, subsystem );
            }
        }

        /// <summary> Reads the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public static void ReadSettings( DigitalOutputSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryDigitalActiveLevels( new int[] { 1, 2, 3, 4 } );
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Applies the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void ApplySettings( DigitalOutputSubsystemBase subsystem )
        {
            ApplySettings( subsystem, this._DigitalOutputActiveHighMenuItem.Checked ? DigitalActiveLevels.High : DigitalActiveLevels.Low );
        }

        /// <summary> Applies the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="polarity">  The polarity. </param>
        public static void ApplySettings( DigitalOutputSubsystemBase subsystem, DigitalActiveLevels polarity )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.ApplyDigitalActiveLevel( new int[] { 1, 2, 3, 4 }, polarity );
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, DigitalOutputSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.DigitalOutputSubsystemBasePropertyChanged;
                // must Not read setting when biding because the instrument may be locked Or in a trigger mode
                // The bound values should be sent when binding Or when applying propert change.
                // ReadSettings( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.DigitalOutputSubsystemBasePropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( DigitalOutputSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( this.DigitalOutputSubsystem.CurrentDigitalActiveLevel ) );
        }

        /// <summary> Handle the Calculate subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( DigitalOutputSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( DigitalOutputSubsystemBase.CommonDigitalActiveLevel ):
                    {
                        this._DigitalOutputActiveHighMenuItem.CheckState = (subsystem.CommonDigitalActiveLevel.HasValue
                                                                            ? subsystem.CommonDigitalActiveLevel.Value == DigitalActiveLevels.High
                                                                            : new bool?()).ToCheckState();
                        break;
                    }
            }
        }

        /// <summary> Digital Output subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DigitalOutputSubsystemBasePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( DigitalOutputSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.DigitalOutputSubsystemBasePropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.DigitalOutputSubsystemBasePropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as DigitalOutputSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }


        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Pass bit pattern numeric button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PassBitPatternNumericButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = $"Pass {(button.Checked ? "0x" : "0d")}";
                this._PassBitPatternNumeric.NumericUpDownControl.Hexadecimal = button.Checked;
            }
        }

        /// <summary> Binning stobe toggle button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void BinningStobeToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = $"Strobe: {button.CheckState.ToCheckStateCaption( "On", "Off", "?" )}";
            }
        }

        /// <summary> Limit failed button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LimitFailedButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = button.CheckState.ToCheckStateCaption( "Fail", "Pass", "P/F ?" );
            }
        }

        /// <summary> Applies the settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ReadSettings();
        }

        /// <summary> Reads state menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadStateMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ReadLimitTestState();
        }

        /// <summary> Preform limit test menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PreformLimitTestMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.PerformLimitTest();
        }

        #endregion

        #region " TALKER "

        /// <summary> Assigns talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( Core.ITraceMessageTalker talker )
        {
            this._Limit1View.AssignTalker( talker );
            this._Limit2View.AssignTalker( talker );
            base.AssignTalker( talker );
        }

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
