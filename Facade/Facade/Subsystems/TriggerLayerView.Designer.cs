﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class TriggerLayerView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(TriggerLayerView));
            _ToolStripPanel = new ToolStripPanel();
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            __SendBusTriggerMenuItem = new ToolStripMenuItem();
            __SendBusTriggerMenuItem.Click += new EventHandler(SendBusTriggerMenuItem_Click);
            _CountNumericLabel = new ToolStripLabel();
            _CountNumeric = new Core.Controls.ToolStripNumericUpDown();
            __InfiniteCountButton = new ToolStripButton();
            __InfiniteCountButton.CheckStateChanged += new EventHandler(InfiniteCountButton_CheckStateChanged);
            _SourceComboLabel = new ToolStripLabel();
            _SourceComboBox = new ToolStripComboBox();
            _TriggerConfigurationToolStrip = new ToolStrip();
            _TriggerCondigureLabel = new ToolStripLabel();
            _InputLineNumericLabel = new ToolStripLabel();
            _InputLineNumeric = new Core.Controls.ToolStripNumericUpDown();
            _OutputLineNumericLabel = new ToolStripLabel();
            _OutputLineNumeric = new Core.Controls.ToolStripNumericUpDown();
            __BypassToggleButton = new ToolStripButton();
            __BypassToggleButton.CheckStateChanged += new EventHandler(BypassToggleButton_CheckStateChanged);
            _TimingToolStrip = new ToolStrip();
            _LayerTimingLabel = new ToolStripLabel();
            _DelayNumericLabel = new ToolStripLabel();
            _DelayNumeric = new Core.Controls.ToolStripNumericUpDown();
            _TimerIntervalNumericLabel = new ToolStripLabel();
            _TimerIntervalNumeric = new Core.Controls.ToolStripNumericUpDown();
            _ToolStripPanel.SuspendLayout();
            _SubsystemToolStrip.SuspendLayout();
            _TriggerConfigurationToolStrip.SuspendLayout();
            _TimingToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ToolStripPanel
            // 
            _ToolStripPanel.BackColor = System.Drawing.Color.Transparent;
            _ToolStripPanel.Controls.Add(_SubsystemToolStrip);
            _ToolStripPanel.Controls.Add(_TriggerConfigurationToolStrip);
            _ToolStripPanel.Controls.Add(_TimingToolStrip);
            _ToolStripPanel.Dock = DockStyle.Top;
            _ToolStripPanel.Location = new System.Drawing.Point(1, 1);
            _ToolStripPanel.Name = "_ToolStripPanel";
            _ToolStripPanel.Orientation = Orientation.Horizontal;
            _ToolStripPanel.RowMargin = new Padding(0);
            _ToolStripPanel.Size = new System.Drawing.Size(395, 84);
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.Dock = DockStyle.None;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _CountNumericLabel, _CountNumeric, __InfiniteCountButton, _SourceComboLabel, _SourceComboBox });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(395, 28);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 3;
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DoubleClickEnabled = true;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem, __SendBusTriggerMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(53, 25);
            _SubsystemSplitButton.Text = "Trig1";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(162, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            __ApplySettingsMenuItem.ToolTipText = "Applies settings onto the instrument";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(162, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            __ReadSettingsMenuItem.ToolTipText = "Reads settings from the instrument";
            // 
            // _SendBusTriggerMenuItem
            // 
            __SendBusTriggerMenuItem.Name = "__SendBusTriggerMenuItem";
            __SendBusTriggerMenuItem.Size = new System.Drawing.Size(162, 22);
            __SendBusTriggerMenuItem.Text = "Send Bus Trigger";
            __SendBusTriggerMenuItem.ToolTipText = "Sends a buis trigger to the instrument";
            // 
            // _CountNumericLabel
            // 
            _CountNumericLabel.Name = "_CountNumericLabel";
            _CountNumericLabel.Size = new System.Drawing.Size(43, 25);
            _CountNumericLabel.Text = "Count:";
            // 
            // _CountNumeric
            // 
            _CountNumeric.AutoSize = false;
            _CountNumeric.Name = "_CountNumeric";
            _CountNumeric.Size = new System.Drawing.Size(60, 25);
            _CountNumeric.Text = "0";
            _CountNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _InfiniteCountButton
            // 
            __InfiniteCountButton.Checked = true;
            __InfiniteCountButton.CheckOnClick = true;
            __InfiniteCountButton.CheckState = CheckState.Indeterminate;
            __InfiniteCountButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __InfiniteCountButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __InfiniteCountButton.Name = "__InfiniteCountButton";
            __InfiniteCountButton.Size = new System.Drawing.Size(37, 25);
            __InfiniteCountButton.Text = "INF ?";
            __InfiniteCountButton.ToolTipText = "Toggle to enable infinite count";
            // 
            // _SourceComboLabel
            // 
            _SourceComboLabel.Name = "_SourceComboLabel";
            _SourceComboLabel.Size = new System.Drawing.Size(46, 25);
            _SourceComboLabel.Text = "Source:";
            // 
            // _SourceComboBox
            // 
            _SourceComboBox.Name = "_SourceComboBox";
            _SourceComboBox.Size = new System.Drawing.Size(80, 28);
            _SourceComboBox.Text = "Immediate";
            _SourceComboBox.ToolTipText = "Selects the source";
            // 
            // _TriggerConfigurationToolStrip
            // 
            _TriggerConfigurationToolStrip.BackColor = System.Drawing.Color.Transparent;
            _TriggerConfigurationToolStrip.Dock = DockStyle.None;
            _TriggerConfigurationToolStrip.GripMargin = new Padding(0);
            _TriggerConfigurationToolStrip.Items.AddRange(new ToolStripItem[] { _TriggerCondigureLabel, _InputLineNumericLabel, _InputLineNumeric, _OutputLineNumericLabel, _OutputLineNumeric, __BypassToggleButton });
            _TriggerConfigurationToolStrip.Location = new System.Drawing.Point(0, 28);
            _TriggerConfigurationToolStrip.Name = "_TriggerConfigurationToolStrip";
            _TriggerConfigurationToolStrip.Size = new System.Drawing.Size(395, 28);
            _TriggerConfigurationToolStrip.Stretch = true;
            _TriggerConfigurationToolStrip.TabIndex = 2;
            // 
            // _TriggerCondigureLabel
            // 
            _TriggerCondigureLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerCondigureLabel.Margin = new Padding(6, 1, 0, 2);
            _TriggerCondigureLabel.Name = "_TriggerCondigureLabel";
            _TriggerCondigureLabel.Size = new System.Drawing.Size(44, 25);
            _TriggerCondigureLabel.Text = "Trigger";
            // 
            // _InputLineNumericLabel
            // 
            _InputLineNumericLabel.Name = "_InputLineNumericLabel";
            _InputLineNumericLabel.Size = new System.Drawing.Size(63, 25);
            _InputLineNumericLabel.Text = "Input Line:";
            // 
            // _InputLineNumeric
            // 
            _InputLineNumeric.Name = "_InputLineNumeric";
            _InputLineNumeric.Size = new System.Drawing.Size(41, 25);
            _InputLineNumeric.Text = "3";
            _InputLineNumeric.ToolTipText = "Asynchronous trigger link input line";
            _InputLineNumeric.Value = new decimal(new int[] { 3, 0, 0, 0 });
            // 
            // _OutputLineNumericLabel
            // 
            _OutputLineNumericLabel.Margin = new Padding(3, 1, 0, 2);
            _OutputLineNumericLabel.Name = "_OutputLineNumericLabel";
            _OutputLineNumericLabel.Size = new System.Drawing.Size(73, 25);
            _OutputLineNumericLabel.Text = "Output Line:";
            // 
            // _OutputLineNumeric
            // 
            _OutputLineNumeric.Name = "_OutputLineNumeric";
            _OutputLineNumeric.Size = new System.Drawing.Size(41, 25);
            _OutputLineNumeric.Text = "0";
            _OutputLineNumeric.ToolTipText = "Trigger Link output line";
            _OutputLineNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _BypassToggleButton
            // 
            __BypassToggleButton.Checked = true;
            __BypassToggleButton.CheckOnClick = true;
            __BypassToggleButton.CheckState = CheckState.Indeterminate;
            __BypassToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __BypassToggleButton.Image = (System.Drawing.Image)resources.GetObject("_BypassToggleButton.Image");
            __BypassToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __BypassToggleButton.Name = "__BypassToggleButton";
            __BypassToggleButton.Size = new System.Drawing.Size(55, 25);
            __BypassToggleButton.Text = "Bypass ?";
            __BypassToggleButton.ToolTipText = "Toggle to bypass or enable triggers";
            // 
            // _TimingToolStrip
            // 
            _TimingToolStrip.BackColor = System.Drawing.Color.Transparent;
            _TimingToolStrip.Dock = DockStyle.None;
            _TimingToolStrip.GripMargin = new Padding(0);
            _TimingToolStrip.Items.AddRange(new ToolStripItem[] { _LayerTimingLabel, _DelayNumericLabel, _DelayNumeric, _TimerIntervalNumericLabel, _TimerIntervalNumeric });
            _TimingToolStrip.Location = new System.Drawing.Point(0, 56);
            _TimingToolStrip.Name = "_TimingToolStrip";
            _TimingToolStrip.Size = new System.Drawing.Size(395, 28);
            _TimingToolStrip.Stretch = true;
            _TimingToolStrip.TabIndex = 1;
            // 
            // _LayerTimingLabel
            // 
            _LayerTimingLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _LayerTimingLabel.Margin = new Padding(6, 1, 0, 2);
            _LayerTimingLabel.Name = "_LayerTimingLabel";
            _LayerTimingLabel.Size = new System.Drawing.Size(43, 25);
            _LayerTimingLabel.Text = "Timing";
            // 
            // _DelayNumericLabel
            // 
            _DelayNumericLabel.Name = "_DelayNumericLabel";
            _DelayNumericLabel.Size = new System.Drawing.Size(50, 25);
            _DelayNumericLabel.Text = "Delay, s:";
            // 
            // _DelayNumeric
            // 
            _DelayNumeric.AutoSize = false;
            _DelayNumeric.Name = "_DelayNumeric";
            _DelayNumeric.Size = new System.Drawing.Size(61, 25);
            _DelayNumeric.Text = "3";
            _DelayNumeric.ToolTipText = "Delay in seconds";
            _DelayNumeric.Value = new decimal(new int[] { 3, 0, 0, 0 });
            // 
            // _TimerIntervalNumericLabel
            // 
            _TimerIntervalNumericLabel.Margin = new Padding(3, 1, 0, 2);
            _TimerIntervalNumericLabel.Name = "_TimerIntervalNumericLabel";
            _TimerIntervalNumericLabel.Size = new System.Drawing.Size(51, 25);
            _TimerIntervalNumericLabel.Text = "Timer, s:";
            // 
            // _TimerIntervalNumeric
            // 
            _TimerIntervalNumeric.AutoSize = false;
            _TimerIntervalNumeric.Name = "_TimerIntervalNumeric";
            _TimerIntervalNumeric.Size = new System.Drawing.Size(61, 25);
            _TimerIntervalNumeric.Text = "10";
            _TimerIntervalNumeric.ToolTipText = "Timer interval in seconds";
            _TimerIntervalNumeric.Value = new decimal(new int[] { 9999, 0, 0, 196608 });
            // 
            // TriggerLayerView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_ToolStripPanel);
            Name = "TriggerLayerView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(397, 85);
            _ToolStripPanel.ResumeLayout(false);
            _ToolStripPanel.PerformLayout();
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            _TriggerConfigurationToolStrip.ResumeLayout(false);
            _TriggerConfigurationToolStrip.PerformLayout();
            _TimingToolStrip.ResumeLayout(false);
            _TimingToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStripPanel _ToolStripPanel;
        private ToolStrip _TimingToolStrip;
        private ToolStripLabel _LayerTimingLabel;
        private ToolStripLabel _TimerIntervalNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _TimerIntervalNumeric;
        private ToolStripLabel _DelayNumericLabel;
        private ToolStrip _TriggerConfigurationToolStrip;
        private ToolStripLabel _TriggerCondigureLabel;
        private Core.Controls.ToolStripNumericUpDown _InputLineNumeric;
        private ToolStripLabel _OutputLineNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _OutputLineNumeric;
        private ToolStripButton __BypassToggleButton;

        private ToolStripButton _BypassToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BypassToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BypassToggleButton != null)
                {
                    __BypassToggleButton.CheckStateChanged -= BypassToggleButton_CheckStateChanged;
                }

                __BypassToggleButton = value;
                if (__BypassToggleButton != null)
                {
                    __BypassToggleButton.CheckStateChanged += BypassToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStrip _SubsystemToolStrip;
        private ToolStripLabel _InputLineNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _DelayNumeric;
        private ToolStripLabel _CountNumericLabel;
        private ToolStripButton __InfiniteCountButton;

        private ToolStripButton _InfiniteCountButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InfiniteCountButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InfiniteCountButton != null)
                {
                    __InfiniteCountButton.CheckStateChanged -= InfiniteCountButton_CheckStateChanged;
                }

                __InfiniteCountButton = value;
                if (__InfiniteCountButton != null)
                {
                    __InfiniteCountButton.CheckStateChanged += InfiniteCountButton_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _CountNumeric;
        private ToolStripLabel _SourceComboLabel;
        private ToolStripComboBox _SourceComboBox;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __SendBusTriggerMenuItem;

        private ToolStripMenuItem _SendBusTriggerMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SendBusTriggerMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SendBusTriggerMenuItem != null)
                {
                    __SendBusTriggerMenuItem.Click -= SendBusTriggerMenuItem_Click;
                }

                __SendBusTriggerMenuItem = value;
                if (__SendBusTriggerMenuItem != null)
                {
                    __SendBusTriggerMenuItem.Click += SendBusTriggerMenuItem_Click;
                }
            }
        }
    }
}