using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A Slot view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SlotView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public SlotView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._SettlingTimeNumeric.NumericUpDownControl.Minimum = 0m;
            this._SettlingTimeNumeric.NumericUpDownControl.Maximum = 1000m;
            this._SettlingTimeNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._SettlingTimeNumeric.NumericUpDownControl.Increment = 10m;
            this._SlotNumberNumeric.NumericUpDownControl.Minimum = 1m;
            this._SlotNumberNumeric.NumericUpDownControl.Maximum = 10m;
            this._SlotNumberNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._SlotNumberNumeric.NumericUpDownControl.Increment = 1m;
            this.__ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
        }

        /// <summary> Creates a new <see cref="SlotView"/> </summary>
        /// <returns> A <see cref="SlotView"/>. </returns>
        public static SlotView Create()
        {
            SlotView view = null;
            try
            {
                view = new SlotView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Gets or sets the slot card number. </summary>
        /// <value> The slot card number. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int SlotNumber
        {
            get => ( int ) this._SlotNumberNumeric.Value;

            set {
                if ( value != this.SlotNumber )
                {
                    this._SlotNumberNumeric.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the settling time. </summary>
        /// <value> The settling time. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan SettlingTime
        {
            get => TimeSpan.FromMilliseconds( ( double ) this._SettlingTimeNumeric.Value );

            set {
                if ( value != this.SettlingTime )
                {
                    this._SettlingTimeNumeric.Value = ( decimal ) value.TotalMilliseconds;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the type of the card. </summary>
        /// <value> The type of the card. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string CardType
        {
            get => this._CardTypeTextBox.Text;

            set {
                if ( !string.Equals( value, this.CardType ) )
                {
                    this._CardTypeTextBox.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Reads the settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadSettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading slot configuration";
                ReadSettings( this.RouteSubsystem, this.SlotNumber );
                this.ApplyPropertyChanged( this.RouteSubsystem );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} updating slot configuration";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.SlotNumber > 0 )
                {
                    _ = this.RouteSubsystem.ApplySlotCardType( this.SlotNumber, this._CardTypeTextBox.Text );
                    _ = this.RouteSubsystem.ApplySlotCardSettlingTime( this.SlotNumber, TimeSpan.FromMilliseconds( ( double ) this._SettlingTimeNumeric.Value ) );
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( SlotView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " ROUTE SUBSYSTEM "

        /// <summary> Gets or sets the Route subsystem. </summary>
        /// <value> The Route subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public RouteSubsystemBase RouteSubsystem { get; private set; }

        /// <summary> Bind Route subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( RouteSubsystemBase subsystem )
        {
            if ( this.RouteSubsystem is object )
            {
                this.BindSubsystem( false, this.RouteSubsystem );
                this.RouteSubsystem = null;
            }

            this.RouteSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.RouteSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, RouteSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.RouteSubsystemPropertyChanged;
                // must not read setting when biding because the instrument may be locked or in a trigger mode
                // The bound values should be sent when binding or when applying propert change.
                // ReadSettings( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.RouteSubsystemPropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( RouteSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( RouteSubsystemBase.SlotCardType ) );
            this.HandlePropertyChanged( subsystem, nameof( RouteSubsystemBase.SlotCardSettlingTime ) );
        }

        /// <summary> Reads the settings. </summary>
        /// <param name="subsystem">  The subsystem. </param>
        /// <param name="slotNumber"> The slot card number. </param>
        private static void ReadSettings( RouteSubsystemBase subsystem, int slotNumber )
        {
            if ( slotNumber > 0 )
            {
                subsystem.StartElapsedStopwatch();
                _ = subsystem.QuerySlotCardType( slotNumber );
                _ = subsystem.QuerySlotCardSettlingTime( slotNumber );
                subsystem.StopElapsedStopwatch();
            }
        }

        /// <summary> Handle the Route subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( RouteSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( RouteSubsystemBase.SlotCardType ):
                    {
                        this._CardTypeTextBox.Text = subsystem.SlotCardType( this.SlotNumber );
                        break;
                    }

                case nameof( RouteSubsystemBase.SlotCardSettlingTime ):
                    {
                        this.SettlingTime = subsystem.SlotCardSettlingTime( this.SlotNumber );
                        break;
                    }
            }
        }

        /// <summary> Route subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RouteSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( RouteSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.RouteSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.RouteSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as RouteSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Applies the settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ReadSettings();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
