﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class SlotView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(SlotView));
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            _SlotNumberComboBoxLabel = new ToolStripLabel();
            _SlotNumberNumeric = new Core.Controls.ToolStripNumericUpDown();
            _CardTypeTextBoxLabel = new ToolStripLabel();
            _CardTypeTextBox = new ToolStripTextBox();
            _SettlingTimeNumericLabel = new ToolStripLabel();
            _SettlingTimeNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SettlingTimeUnitLabel = new ToolStripLabel();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _SlotNumberComboBoxLabel, _SlotNumberNumeric, _CardTypeTextBoxLabel, _CardTypeTextBox, _SettlingTimeNumericLabel, _SettlingTimeNumeric, _SettlingTimeUnitLabel });
            _SubsystemToolStrip.Location = new System.Drawing.Point(1, 1);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(447, 27);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 1;
            _SubsystemToolStrip.Text = "Slot Tool Strip";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(42, 24);
            _SubsystemSplitButton.Text = "Slot";
            _SubsystemSplitButton.ToolTipText = "Select action";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(180, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            __ApplySettingsMenuItem.ToolTipText = "Applies the settings onto the instrument";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(180, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            // 
            // _SlotNumberComboBoxLabel
            // 
            _SlotNumberComboBoxLabel.Margin = new Padding(3, 1, 0, 2);
            _SlotNumberComboBoxLabel.Name = "_SlotNumberComboBoxLabel";
            _SlotNumberComboBoxLabel.Size = new System.Drawing.Size(14, 24);
            _SlotNumberComboBoxLabel.Text = "#";
            // 
            // _SlotNumberNumeric
            // 
            _SlotNumberNumeric.Name = "_SlotNumberNumeric";
            _SlotNumberNumeric.Size = new System.Drawing.Size(41, 24);
            _SlotNumberNumeric.Text = "0";
            _SlotNumberNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _CardTypeTextBoxLabel
            // 
            _CardTypeTextBoxLabel.Name = "_CardTypeTextBoxLabel";
            _CardTypeTextBoxLabel.Size = new System.Drawing.Size(34, 24);
            _CardTypeTextBoxLabel.Text = "Type:";
            // 
            // _CardTypeTextBox
            // 
            _CardTypeTextBox.Font = new System.Drawing.Font("Segoe UI", 9.0f);
            _CardTypeTextBox.Name = "_CardTypeTextBox";
            _CardTypeTextBox.Size = new System.Drawing.Size(100, 27);
            _CardTypeTextBox.ToolTipText = "Card type";
            // 
            // _SettlingTimeNumericLabel
            // 
            _SettlingTimeNumericLabel.Margin = new Padding(2, 1, 0, 2);
            _SettlingTimeNumericLabel.Name = "_SettlingTimeNumericLabel";
            _SettlingTimeNumericLabel.Size = new System.Drawing.Size(50, 24);
            _SettlingTimeNumericLabel.Text = "Settling:";
            // 
            // _SettlingTimeNumeric
            // 
            _SettlingTimeNumeric.AutoSize = false;
            _SettlingTimeNumeric.Margin = new Padding(0, 1, 0, 1);
            _SettlingTimeNumeric.Name = "_SettlingTimeNumeric";
            _SettlingTimeNumeric.Size = new System.Drawing.Size(45, 25);
            _SettlingTimeNumeric.Text = "0";
            _SettlingTimeNumeric.ToolTipText = "Relay settling time";
            _SettlingTimeNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _SettlingTimeUnitLabel
            // 
            _SettlingTimeUnitLabel.Name = "_SettlingTimeUnitLabel";
            _SettlingTimeUnitLabel.Size = new System.Drawing.Size(23, 24);
            _SettlingTimeUnitLabel.Text = "ms";
            // 
            // SlotView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_SubsystemToolStrip);
            Name = "SlotView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(449, 29);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _SubsystemToolStrip;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripLabel _SlotNumberComboBoxLabel;
        private ToolStripLabel _SettlingTimeNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _SettlingTimeNumeric;
        private ToolStripLabel _CardTypeTextBoxLabel;
        private ToolStripTextBox _CardTypeTextBox;
        private ToolStripLabel _SettlingTimeUnitLabel;
        private Core.Controls.ToolStripNumericUpDown _SlotNumberNumeric;
    }
}