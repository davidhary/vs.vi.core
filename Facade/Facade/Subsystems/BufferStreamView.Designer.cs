﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class BufferStreamView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(BufferStreamView));
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __DisplayBufferMenuItem = new ToolStripMenuItem();
            __DisplayBufferMenuItem.Click += new EventHandler(DisplayBufferMenuItem_Click);
            __ClearBufferDisplayMenuItem = new ToolStripMenuItem();
            __ClearBufferDisplayMenuItem.Click += new EventHandler(ClearBufferDisplayMenuItem_Click);
            _BufferStreamingMenuItem = new ToolStripMenuItem();
            __ExplainBufferStreamingMenuItem = new ToolStripMenuItem();
            __ExplainBufferStreamingMenuItem.Click += new EventHandler(ExplainBufferStreamingMenuItem_Click);
            _UsingScanCardMenuItem = new ToolStripMenuItem();
            __ConfigureStreamingMenuItem = new ToolStripMenuItem();
            __ConfigureStreamingMenuItem.CheckStateChanged += new EventHandler(ConfigureStreamingMenuItem_CheckStateChanged);
            __StartBufferStreamMenuItem = new ToolStripMenuItem();
            __StartBufferStreamMenuItem.CheckStateChanged += new EventHandler(StartBufferStreamMenuItem_CheckStateChanged);
            _AbortButton = new ToolStripButton();
            _BufferCountLabel = new Core.Controls.ToolStripLabel();
            _LastPointNumberLabel = new Core.Controls.ToolStripLabel();
            _FirstPointNumberLabel = new Core.Controls.ToolStripLabel();
            _BufferSizeNumericLabel = new ToolStripLabel();
            __BufferSizeNumeric = new Core.Controls.ToolStripNumericUpDown();
            __BufferSizeNumeric.Validating += new System.ComponentModel.CancelEventHandler(BufferSizeNumeric_Validating);
            _TriggerStateLabel = new Core.Controls.ToolStripLabel();
            __AssertBusTriggerButton = new ToolStripButton();
            __AssertBusTriggerButton.Click += new EventHandler(AssertBusTriggerButton_Click);
            UseTriggerPlanMenuItem = new ToolStripMenuItem();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _AbortButton, _BufferCountLabel, _LastPointNumberLabel, _FirstPointNumberLabel, _BufferSizeNumericLabel, __BufferSizeNumeric, _TriggerStateLabel, __AssertBusTriggerButton });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Padding = new Padding(0);
            _SubsystemToolStrip.Size = new System.Drawing.Size(349, 27);
            _SubsystemToolStrip.TabIndex = 11;
            _SubsystemToolStrip.Text = "Buffer";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __DisplayBufferMenuItem, __ClearBufferDisplayMenuItem, _BufferStreamingMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(55, 24);
            _SubsystemSplitButton.Text = "Buffer";
            // 
            // _DisplayBufferMenuItem
            // 
            __DisplayBufferMenuItem.Name = "__DisplayBufferMenuItem";
            __DisplayBufferMenuItem.Size = new System.Drawing.Size(180, 22);
            __DisplayBufferMenuItem.Text = "Display Buffer";
            __DisplayBufferMenuItem.ToolTipText = "Displays the buffer";
            // 
            // _ClearBufferDisplayMenuItem
            // 
            __ClearBufferDisplayMenuItem.Name = "__ClearBufferDisplayMenuItem";
            __ClearBufferDisplayMenuItem.Size = new System.Drawing.Size(180, 22);
            __ClearBufferDisplayMenuItem.Text = "Clear Buffer Display";
            __ClearBufferDisplayMenuItem.ToolTipText = "Clears the display";
            // 
            // _BufferStreamingMenuItem
            // 
            _BufferStreamingMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __ExplainBufferStreamingMenuItem, _UsingScanCardMenuItem, UseTriggerPlanMenuItem, __ConfigureStreamingMenuItem, __StartBufferStreamMenuItem });
            _BufferStreamingMenuItem.Name = "_BufferStreamingMenuItem";
            _BufferStreamingMenuItem.Size = new System.Drawing.Size(180, 22);
            _BufferStreamingMenuItem.Text = "Buffer Streaming";
            _BufferStreamingMenuItem.ToolTipText = "Select action";
            // 
            // _ExplainBufferStreamingMenuItem
            // 
            __ExplainBufferStreamingMenuItem.Name = "__ExplainBufferStreamingMenuItem";
            __ExplainBufferStreamingMenuItem.Size = new System.Drawing.Size(180, 22);
            __ExplainBufferStreamingMenuItem.Text = "Info";
            __ExplainBufferStreamingMenuItem.ToolTipText = "Display buffer streaming settings";
            // 
            // _UsingScanCardMenuItem
            // 
            _UsingScanCardMenuItem.CheckOnClick = true;
            _UsingScanCardMenuItem.Name = "_UsingScanCardMenuItem";
            _UsingScanCardMenuItem.Size = new System.Drawing.Size(180, 22);
            _UsingScanCardMenuItem.Text = "Use Scan Card";
            _UsingScanCardMenuItem.ToolTipText = "Use scan card";
            // 
            // _ConfigureStreamingMenuItem
            // 
            __ConfigureStreamingMenuItem.CheckOnClick = true;
            __ConfigureStreamingMenuItem.Name = "__ConfigureStreamingMenuItem";
            __ConfigureStreamingMenuItem.Size = new System.Drawing.Size(180, 22);
            __ConfigureStreamingMenuItem.Text = "Configure";
            __ConfigureStreamingMenuItem.ToolTipText = "Configure buffer streaming";
            // 
            // _StartBufferStreamMenuItem
            // 
            __StartBufferStreamMenuItem.CheckOnClick = true;
            __StartBufferStreamMenuItem.Name = "__StartBufferStreamMenuItem";
            __StartBufferStreamMenuItem.Size = new System.Drawing.Size(180, 22);
            __StartBufferStreamMenuItem.Text = "Start";
            __StartBufferStreamMenuItem.ToolTipText = "Continuously reads new values while a trigger plan is active";
            // 
            // _AbortButton
            // 
            _AbortButton.Alignment = ToolStripItemAlignment.Right;
            _AbortButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _AbortButton.Image = (System.Drawing.Image)resources.GetObject("_AbortButton.Image");
            _AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _AbortButton.Name = "_AbortButton";
            _AbortButton.Size = new System.Drawing.Size(45, 24);
            _AbortButton.Text = "Abort";
            _AbortButton.ToolTipText = "Aborts triggering";
            // 
            // _BufferCountLabel
            // 
            _BufferCountLabel.Alignment = ToolStripItemAlignment.Right;
            _BufferCountLabel.Name = "_BufferCountLabel";
            _BufferCountLabel.Size = new System.Drawing.Size(15, 24);
            _BufferCountLabel.Text = "0";
            _BufferCountLabel.ToolTipText = "Buffer count";
            // 
            // _LastPointNumberLabel
            // 
            _LastPointNumberLabel.Alignment = ToolStripItemAlignment.Right;
            _LastPointNumberLabel.Name = "_LastPointNumberLabel";
            _LastPointNumberLabel.Size = new System.Drawing.Size(15, 24);
            _LastPointNumberLabel.Text = "2";
            _LastPointNumberLabel.ToolTipText = "Number of last buffer reading";
            // 
            // _FirstPointNumberLabel
            // 
            _FirstPointNumberLabel.Alignment = ToolStripItemAlignment.Right;
            _FirstPointNumberLabel.Name = "_FirstPointNumberLabel";
            _FirstPointNumberLabel.Size = new System.Drawing.Size(15, 24);
            _FirstPointNumberLabel.Text = "1";
            _FirstPointNumberLabel.ToolTipText = "Number of the first buffer reading";
            // 
            // _BufferSizeNumericLabel
            // 
            _BufferSizeNumericLabel.Name = "_BufferSizeNumericLabel";
            _BufferSizeNumericLabel.Size = new System.Drawing.Size(34, 24);
            _BufferSizeNumericLabel.Text = "Size:";
            // 
            // _BufferSizeNumeric
            // 
            __BufferSizeNumeric.AutoSize = false;
            __BufferSizeNumeric.Name = "__BufferSizeNumeric";
            __BufferSizeNumeric.Size = new System.Drawing.Size(60, 24);
            __BufferSizeNumeric.Text = "0";
            __BufferSizeNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _TriggerStateLabel
            // 
            _TriggerStateLabel.Name = "_TriggerStateLabel";
            _TriggerStateLabel.Size = new System.Drawing.Size(29, 24);
            _TriggerStateLabel.Text = "idle";
            _TriggerStateLabel.ToolTipText = "Trigger state";
            // 
            // _AssertBusTriggerButton
            // 
            __AssertBusTriggerButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AssertBusTriggerButton.Image = (System.Drawing.Image)resources.GetObject("_AssertBusTriggerButton.Image");
            __AssertBusTriggerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AssertBusTriggerButton.Name = "__AssertBusTriggerButton";
            __AssertBusTriggerButton.Size = new System.Drawing.Size(48, 24);
            __AssertBusTriggerButton.Text = "Assert";
            __AssertBusTriggerButton.ToolTipText = "Assert BUS trigger";
            // 
            // UseTriggerPlanMenuItem
            // 
            UseTriggerPlanMenuItem.CheckOnClick = true;
            UseTriggerPlanMenuItem.Name = "UseTriggerPlanMenuItem";
            UseTriggerPlanMenuItem.Size = new System.Drawing.Size(180, 22);
            UseTriggerPlanMenuItem.Text = "Use Trigger Plan";
            UseTriggerPlanMenuItem.ToolTipText = "Select trigger plan or application settings for trigger source and trigger count." + "";
            // 
            // BufferStreamView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_SubsystemToolStrip);
            Name = "BufferStreamView";
            Size = new System.Drawing.Size(349, 26);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _SubsystemToolStrip;
        private Core.Controls.ToolStripLabel _BufferCountLabel;
        private Core.Controls.ToolStripLabel _LastPointNumberLabel;
        private Core.Controls.ToolStripLabel _FirstPointNumberLabel;
        private Core.Controls.ToolStripNumericUpDown __BufferSizeNumeric;

        private Core.Controls.ToolStripNumericUpDown _BufferSizeNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BufferSizeNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BufferSizeNumeric != null)
                {
                    __BufferSizeNumeric.Validating -= BufferSizeNumeric_Validating;
                }

                __BufferSizeNumeric = value;
                if (__BufferSizeNumeric != null)
                {
                    __BufferSizeNumeric.Validating += BufferSizeNumeric_Validating;
                }
            }
        }

        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripLabel _BufferSizeNumericLabel;
        private ToolStripButton _AbortButton;
        private Core.Controls.ToolStripLabel _TriggerStateLabel;
        private ToolStripMenuItem __DisplayBufferMenuItem;

        private ToolStripMenuItem _DisplayBufferMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DisplayBufferMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DisplayBufferMenuItem != null)
                {
                    __DisplayBufferMenuItem.Click -= DisplayBufferMenuItem_Click;
                }

                __DisplayBufferMenuItem = value;
                if (__DisplayBufferMenuItem != null)
                {
                    __DisplayBufferMenuItem.Click += DisplayBufferMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ClearBufferDisplayMenuItem;

        private ToolStripMenuItem _ClearBufferDisplayMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearBufferDisplayMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearBufferDisplayMenuItem != null)
                {
                    __ClearBufferDisplayMenuItem.Click -= ClearBufferDisplayMenuItem_Click;
                }

                __ClearBufferDisplayMenuItem = value;
                if (__ClearBufferDisplayMenuItem != null)
                {
                    __ClearBufferDisplayMenuItem.Click += ClearBufferDisplayMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem _UsingScanCardMenuItem;
        private ToolStripMenuItem __ConfigureStreamingMenuItem;

        private ToolStripMenuItem _ConfigureStreamingMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConfigureStreamingMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConfigureStreamingMenuItem != null)
                {
                    __ConfigureStreamingMenuItem.CheckStateChanged -= ConfigureStreamingMenuItem_CheckStateChanged;
                }

                __ConfigureStreamingMenuItem = value;
                if (__ConfigureStreamingMenuItem != null)
                {
                    __ConfigureStreamingMenuItem.CheckStateChanged += ConfigureStreamingMenuItem_CheckStateChanged;
                }
            }
        }

        private ToolStripMenuItem __StartBufferStreamMenuItem;

        private ToolStripMenuItem _StartBufferStreamMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartBufferStreamMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartBufferStreamMenuItem != null)
                {
                    __StartBufferStreamMenuItem.CheckStateChanged -= StartBufferStreamMenuItem_CheckStateChanged;
                }

                __StartBufferStreamMenuItem = value;
                if (__StartBufferStreamMenuItem != null)
                {
                    __StartBufferStreamMenuItem.CheckStateChanged += StartBufferStreamMenuItem_CheckStateChanged;
                }
            }
        }

        private ToolStripMenuItem _BufferStreamingMenuItem;
        private ToolStripMenuItem __ExplainBufferStreamingMenuItem;

        private ToolStripMenuItem _ExplainBufferStreamingMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ExplainBufferStreamingMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ExplainBufferStreamingMenuItem != null)
                {
                    __ExplainBufferStreamingMenuItem.Click -= ExplainBufferStreamingMenuItem_Click;
                }

                __ExplainBufferStreamingMenuItem = value;
                if (__ExplainBufferStreamingMenuItem != null)
                {
                    __ExplainBufferStreamingMenuItem.Click += ExplainBufferStreamingMenuItem_Click;
                }
            }
        }

        private ToolStripButton __AssertBusTriggerButton;

        private ToolStripButton _AssertBusTriggerButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AssertBusTriggerButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AssertBusTriggerButton != null)
                {
                    __AssertBusTriggerButton.Click -= AssertBusTriggerButton_Click;
                }

                __AssertBusTriggerButton = value;
                if (__AssertBusTriggerButton != null)
                {
                    __AssertBusTriggerButton.Click += AssertBusTriggerButton_Click;
                }
            }
        }

        private ToolStripMenuItem UseTriggerPlanMenuItem;
    }
}