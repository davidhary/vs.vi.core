﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class DigitalOutputView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(DigitalOutputView));
            _Layout = new TableLayoutPanel();
            _InfoTextBox = new TextBox();
            _DigitalOutputLine1View = new DigitalOutputLineView();
            _DigitalOutputLine2View = new DigitalOutputLineView();
            _DigitalOutputLine3View = new DigitalOutputLineView();
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsToolStripMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsToolStripMenuItem_Click);
            __StrobeButton = new ToolStripButton();
            __StrobeButton.Click += new EventHandler(InitiateButton_Click);
            _Layout.SuspendLayout();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 2;
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 2.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _Layout.Controls.Add(_InfoTextBox, 0, 4);
            _Layout.Controls.Add(_DigitalOutputLine1View, 0, 1);
            _Layout.Controls.Add(_DigitalOutputLine2View, 0, 2);
            _Layout.Controls.Add(_DigitalOutputLine3View, 0, 3);
            _Layout.Controls.Add(_SubsystemToolStrip, 0, 0);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 5;
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _Layout.Size = new System.Drawing.Size(423, 352);
            _Layout.TabIndex = 0;
            // 
            // _InfoTextBox
            // 
            _InfoTextBox.Dock = DockStyle.Fill;
            _InfoTextBox.Location = new System.Drawing.Point(3, 134);
            _InfoTextBox.Margin = new Padding(3, 4, 3, 4);
            _InfoTextBox.Multiline = true;
            _InfoTextBox.Name = "_InfoTextBox";
            _InfoTextBox.ReadOnly = true;
            _InfoTextBox.ScrollBars = ScrollBars.Both;
            _InfoTextBox.Size = new System.Drawing.Size(415, 214);
            _InfoTextBox.TabIndex = 14;
            _InfoTextBox.Text = "<info>";
            // 
            // _DigitalOutputLine1View
            // 
            _DigitalOutputLine1View.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _DigitalOutputLine1View.Dock = DockStyle.Top;
            _DigitalOutputLine1View.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _DigitalOutputLine1View.LineLevel = 0;
            _DigitalOutputLine1View.LineName = "Line";
            _DigitalOutputLine1View.LineNumber = 0;
            _DigitalOutputLine1View.Location = new System.Drawing.Point(3, 28);
            _DigitalOutputLine1View.Name = "_DigitalOutputLine1View";
            _DigitalOutputLine1View.Padding = new Padding(1);
            _DigitalOutputLine1View.PulseWidth = TimeSpan.Parse("00:00:00");
            _DigitalOutputLine1View.Size = new System.Drawing.Size(415, 30);
            _DigitalOutputLine1View.TabIndex = 0;
            // 
            // _DigitalOutputLine2View
            // 
            _DigitalOutputLine2View.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _DigitalOutputLine2View.Dock = DockStyle.Top;
            _DigitalOutputLine2View.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _DigitalOutputLine2View.LineLevel = 0;
            _DigitalOutputLine2View.LineName = "Line";
            _DigitalOutputLine2View.LineNumber = 0;
            _DigitalOutputLine2View.Location = new System.Drawing.Point(3, 64);
            _DigitalOutputLine2View.Name = "_DigitalOutputLine2View";
            _DigitalOutputLine2View.Padding = new Padding(1);
            _DigitalOutputLine2View.PulseWidth = TimeSpan.Parse("00:00:00");
            _DigitalOutputLine2View.Size = new System.Drawing.Size(415, 28);
            _DigitalOutputLine2View.TabIndex = 1;
            // 
            // _DigitalOutputLine3View
            // 
            _DigitalOutputLine3View.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _DigitalOutputLine3View.Dock = DockStyle.Top;
            _DigitalOutputLine3View.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _DigitalOutputLine3View.LineLevel = 0;
            _DigitalOutputLine3View.LineName = "Line";
            _DigitalOutputLine3View.LineNumber = 0;
            _DigitalOutputLine3View.Location = new System.Drawing.Point(3, 98);
            _DigitalOutputLine3View.Name = "_DigitalOutputLine3View";
            _DigitalOutputLine3View.Padding = new Padding(1);
            _DigitalOutputLine3View.PulseWidth = TimeSpan.Parse("00:00:00");
            _DigitalOutputLine3View.Size = new System.Drawing.Size(415, 29);
            _DigitalOutputLine3View.TabIndex = 2;
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.AutoSize = false;
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, __StrobeButton });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(421, 25);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 9;
            _SubsystemToolStrip.Text = "Digital Out";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(82, 22);
            _SubsystemSplitButton.Text = "Digital Out";
            _SubsystemSplitButton.ToolTipText = "Subsystem actions split button";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(150, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(150, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            // 
            // _StrobeButton
            // 
            __StrobeButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __StrobeButton.Image = (System.Drawing.Image)resources.GetObject("_StrobeButton.Image");
            __StrobeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __StrobeButton.Name = "__StrobeButton";
            __StrobeButton.Size = new System.Drawing.Size(45, 22);
            __StrobeButton.Text = "Strobe";
            __StrobeButton.ToolTipText = "Outputs a binning strop signal";
            // 
            // DigitalOutputView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "DigitalOutputView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(425, 354);
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
        }

        private DigitalOutputLineView _DigitalOutputLine1View;
        private DigitalOutputLineView _DigitalOutputLine2View;
        private DigitalOutputLineView _DigitalOutputLine3View;
        private TableLayoutPanel _Layout;
        private ToolStrip _SubsystemToolStrip;
        private ToolStripButton __StrobeButton;

        private ToolStripButton _StrobeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StrobeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StrobeButton != null)
                {
                    __StrobeButton.Click -= InitiateButton_Click;
                }

                __StrobeButton = value;
                if (__StrobeButton != null)
                {
                    __StrobeButton.Click += InitiateButton_Click;
                }
            }
        }

        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsToolStripMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsToolStripMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsToolStripMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsToolStripMenuItem_Click;
                }
            }
        }

        private TextBox _InfoTextBox;
    }
}