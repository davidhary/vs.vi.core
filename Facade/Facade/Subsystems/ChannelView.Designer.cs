﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class ChannelView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ChannelView));
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __CloseChannelMenuItem = new ToolStripMenuItem();
            __CloseChannelMenuItem.Click += new EventHandler(CloseChannelMenuItem_Click);
            __OpenChannelMenuItem = new ToolStripMenuItem();
            __OpenChannelMenuItem.Click += new EventHandler(OpenChannelMenuItem_Click);
            __OpenAllMenuItem = new ToolStripMenuItem();
            __OpenAllMenuItem.Click += new EventHandler(OpenAllMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            _ChannelNumberNumericLabel = new ToolStripLabel();
            _ChannelNumberNumeric = new Core.Controls.ToolStripNumericUpDown();
            __CloseChannelButton = new ToolStripButton();
            __CloseChannelButton.Click += new EventHandler(CloseChannelButton_Click);
            __OpenChannelButton = new ToolStripButton();
            __OpenChannelButton.Click += new EventHandler(OpenChannelButton_Click);
            __OpenChannelsButton = new ToolStripButton();
            __OpenChannelsButton.Click += new EventHandler(OpenChannelsButton_Click);
            _ClosedChannelLabel = new ToolStripLabel();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _ChannelNumberNumericLabel, _ChannelNumberNumeric, __CloseChannelButton, __OpenChannelButton, __OpenChannelsButton, _ClosedChannelLabel });
            _SubsystemToolStrip.Location = new System.Drawing.Point(1, 1);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(382, 26);
            _SubsystemToolStrip.TabIndex = 0;
            _SubsystemToolStrip.Text = "ChannelToolStrip";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __CloseChannelMenuItem, __OpenChannelMenuItem, __OpenAllMenuItem, __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(68, 23);
            _SubsystemSplitButton.Text = "Channel";
            // 
            // _CloseChannelMenuItem
            // 
            __CloseChannelMenuItem.Name = "__CloseChannelMenuItem";
            __CloseChannelMenuItem.Size = new System.Drawing.Size(180, 22);
            __CloseChannelMenuItem.Text = "Close Channel";
            __CloseChannelMenuItem.ToolTipText = "Closes the specified channel";
            // 
            // _OpenChannelMenuItem
            // 
            __OpenChannelMenuItem.Name = "__OpenChannelMenuItem";
            __OpenChannelMenuItem.Size = new System.Drawing.Size(180, 22);
            __OpenChannelMenuItem.Text = "Open Channel";
            __OpenChannelMenuItem.ToolTipText = "Opens the selected channel";
            // 
            // _OpenAllMenuItem
            // 
            __OpenAllMenuItem.Name = "__OpenAllMenuItem";
            __OpenAllMenuItem.Size = new System.Drawing.Size(180, 22);
            __OpenAllMenuItem.Text = "Open All";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(180, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            __ReadSettingsMenuItem.ToolTipText = "Reads settings from the instrument";
            // 
            // _ChannelNumberNumericLabel
            // 
            _ChannelNumberNumericLabel.Name = "_ChannelNumberNumericLabel";
            _ChannelNumberNumericLabel.Size = new System.Drawing.Size(14, 23);
            _ChannelNumberNumericLabel.Text = "#";
            // 
            // _ChannelNumberNumeric
            // 
            _ChannelNumberNumeric.Name = "_ChannelNumberNumeric";
            _ChannelNumberNumeric.Size = new System.Drawing.Size(41, 23);
            _ChannelNumberNumeric.Text = "0";
            _ChannelNumberNumeric.ToolTipText = "Channel Number";
            _ChannelNumberNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _CloseChannelButton
            // 
            __CloseChannelButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __CloseChannelButton.Image = (System.Drawing.Image)resources.GetObject("_CloseChannelButton.Image");
            __CloseChannelButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __CloseChannelButton.Name = "__CloseChannelButton";
            __CloseChannelButton.Size = new System.Drawing.Size(40, 23);
            __CloseChannelButton.Text = "Close";
            __CloseChannelButton.ToolTipText = "Closes the selected channel";
            // 
            // _OpenChannelButton
            // 
            __OpenChannelButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __OpenChannelButton.Image = (System.Drawing.Image)resources.GetObject("_OpenChannelButton.Image");
            __OpenChannelButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenChannelButton.Name = "__OpenChannelButton";
            __OpenChannelButton.Size = new System.Drawing.Size(40, 23);
            __OpenChannelButton.Text = "Open";
            __OpenChannelButton.ToolTipText = "Opens the selected channel";
            // 
            // _OpenChannelsButton
            // 
            __OpenChannelsButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __OpenChannelsButton.Image = (System.Drawing.Image)resources.GetObject("_OpenChannelsButton.Image");
            __OpenChannelsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenChannelsButton.Name = "__OpenChannelsButton";
            __OpenChannelsButton.Size = new System.Drawing.Size(57, 23);
            __OpenChannelsButton.Text = "Open All";
            __OpenChannelsButton.ToolTipText = "Opens all channels";
            // 
            // _ClosedChannelLabel
            // 
            _ClosedChannelLabel.Name = "_ClosedChannelLabel";
            _ClosedChannelLabel.Size = new System.Drawing.Size(54, 23);
            _ClosedChannelLabel.Text = "Closed: ?";
            // 
            // ChannelView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_SubsystemToolStrip);
            Name = "ChannelView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(384, 26);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStripLabel _ChannelNumberNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _ChannelNumberNumeric;
        private ToolStripLabel _ClosedChannelLabel;
        private ToolStripButton __CloseChannelButton;

        private ToolStripButton _CloseChannelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseChannelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseChannelButton != null)
                {
                    __CloseChannelButton.Click -= CloseChannelButton_Click;
                }

                __CloseChannelButton = value;
                if (__CloseChannelButton != null)
                {
                    __CloseChannelButton.Click += CloseChannelButton_Click;
                }
            }
        }

        private ToolStripButton __OpenChannelButton;

        private ToolStripButton _OpenChannelButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenChannelButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenChannelButton != null)
                {
                    __OpenChannelButton.Click -= OpenChannelButton_Click;
                }

                __OpenChannelButton = value;
                if (__OpenChannelButton != null)
                {
                    __OpenChannelButton.Click += OpenChannelButton_Click;
                }
            }
        }

        private ToolStripButton __OpenChannelsButton;

        private ToolStripButton _OpenChannelsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenChannelsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenChannelsButton != null)
                {
                    __OpenChannelsButton.Click -= OpenChannelsButton_Click;
                }

                __OpenChannelsButton = value;
                if (__OpenChannelsButton != null)
                {
                    __OpenChannelsButton.Click += OpenChannelsButton_Click;
                }
            }
        }

        private ToolStrip _SubsystemToolStrip;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __CloseChannelMenuItem;

        private ToolStripMenuItem _CloseChannelMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseChannelMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseChannelMenuItem != null)
                {
                    __CloseChannelMenuItem.Click -= CloseChannelMenuItem_Click;
                }

                __CloseChannelMenuItem = value;
                if (__CloseChannelMenuItem != null)
                {
                    __CloseChannelMenuItem.Click += CloseChannelMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __OpenChannelMenuItem;

        private ToolStripMenuItem _OpenChannelMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenChannelMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenChannelMenuItem != null)
                {
                    __OpenChannelMenuItem.Click -= OpenChannelMenuItem_Click;
                }

                __OpenChannelMenuItem = value;
                if (__OpenChannelMenuItem != null)
                {
                    __OpenChannelMenuItem.Click += OpenChannelMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __OpenAllMenuItem;

        private ToolStripMenuItem _OpenAllMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenAllMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenAllMenuItem != null)
                {
                    __OpenAllMenuItem.Click -= OpenAllMenuItem_Click;
                }

                __OpenAllMenuItem = value;
                if (__OpenAllMenuItem != null)
                {
                    __OpenAllMenuItem.Click += OpenAllMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }
    }
}