﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class ScanView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanView));
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplyInternalScanListMenuItem = new ToolStripMenuItem();
            __ApplyInternalScanListMenuItem.Click += new EventHandler(ApplyInternalScanListMenuItem_Click);
            __ApplyInternalScanFunctionListMenuItem = new ToolStripMenuItem();
            __ApplyInternalScanFunctionListMenuItem.Click += new EventHandler(ApplyInternalScanFunctionListMenuItem_Click);
            __SelectInternalScanListMenuItem = new ToolStripMenuItem();
            __SelectInternalScanListMenuItem.Click += new EventHandler(SelectInternalScanListMenuItem_Click);
            __ReleaseInternalScanListMenuItem = new ToolStripMenuItem();
            __ReleaseInternalScanListMenuItem.Click += new EventHandler(ReleaseInternalScanListMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            _CandidateScanListComboBox = new ToolStripComboBox();
            _InternalScanListLabel = new ToolStripLabel();
            _ScanListFunctionLabel = new ToolStripLabel();
            _ClosedChannelsLabel = new ToolStripLabel();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _CandidateScanListComboBox, _InternalScanListLabel, _ScanListFunctionLabel, _ClosedChannelsLabel });
            _SubsystemToolStrip.Location = new System.Drawing.Point(1, 1);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(422, 25);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 0;
            _SubsystemToolStrip.Text = "ChannelToolStrip";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplyInternalScanListMenuItem, __ApplyInternalScanFunctionListMenuItem, __SelectInternalScanListMenuItem, __ReleaseInternalScanListMenuItem, __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(48, 22);
            _SubsystemSplitButton.Text = "Scan";
            _SubsystemSplitButton.ToolTipText = "Select action";
            // 
            // _ApplyInternalScanListMenuItem
            // 
            __ApplyInternalScanListMenuItem.Name = "__ApplyInternalScanListMenuItem";
            __ApplyInternalScanListMenuItem.Size = new System.Drawing.Size(246, 22);
            __ApplyInternalScanListMenuItem.Text = "Apply Internal Scan List";
            __ApplyInternalScanListMenuItem.ToolTipText = "Applies the list as an internal scan list";
            // 
            // _ApplyInternalScanFunctionListMenuItem
            // 
            __ApplyInternalScanFunctionListMenuItem.Name = "__ApplyInternalScanFunctionListMenuItem";
            __ApplyInternalScanFunctionListMenuItem.Size = new System.Drawing.Size(246, 22);
            __ApplyInternalScanFunctionListMenuItem.Text = "Apply Internal Scan Function List";
            __ApplyInternalScanFunctionListMenuItem.ToolTipText = "Applies the list as an internal function scan list";
            // 
            // _SelectInternalScanListMenuItem
            // 
            __SelectInternalScanListMenuItem.Name = "__SelectInternalScanListMenuItem";
            __SelectInternalScanListMenuItem.Size = new System.Drawing.Size(246, 22);
            __SelectInternalScanListMenuItem.Text = "Select Internal Scan List";
            __SelectInternalScanListMenuItem.ToolTipText = "Select the list as an internal scan list";
            // 
            // _ReleaseInternalScanListMenuItem
            // 
            __ReleaseInternalScanListMenuItem.Name = "__ReleaseInternalScanListMenuItem";
            __ReleaseInternalScanListMenuItem.Size = new System.Drawing.Size(246, 22);
            __ReleaseInternalScanListMenuItem.Text = "Release Internal Scan List";
            __ReleaseInternalScanListMenuItem.ToolTipText = "Releases the internal scan list";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(246, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            // 
            // _CandidateScanListComboBox
            // 
            _CandidateScanListComboBox.AutoSize = false;
            _CandidateScanListComboBox.Items.AddRange(new object[] { "(@1:3)" });
            _CandidateScanListComboBox.Name = "_CandidateScanListComboBox";
            _CandidateScanListComboBox.Size = new System.Drawing.Size(140, 23);
            _CandidateScanListComboBox.Text = "(@1:3)";
            _CandidateScanListComboBox.ToolTipText = "Candidate scan list in the format:  (@<card>:<relay>)";
            // 
            // _InternalScanListLabel
            // 
            _InternalScanListLabel.Margin = new Padding(3, 1, 0, 2);
            _InternalScanListLabel.Name = "_InternalScanListLabel";
            _InternalScanListLabel.Size = new System.Drawing.Size(55, 22);
            _InternalScanListLabel.Text = "Internal ?";
            _InternalScanListLabel.ToolTipText = "Internal scan list";
            // 
            // _ScanListFunctionLabel
            // 
            _ScanListFunctionLabel.Margin = new Padding(3, 1, 0, 2);
            _ScanListFunctionLabel.Name = "_ScanListFunctionLabel";
            _ScanListFunctionLabel.Size = new System.Drawing.Size(62, 22);
            _ScanListFunctionLabel.Text = "Function ?";
            _ScanListFunctionLabel.ToolTipText = "Function scan list";
            // 
            // _ClosedChannelsLabel
            // 
            _ClosedChannelsLabel.Name = "_ClosedChannelsLabel";
            _ClosedChannelsLabel.Size = new System.Drawing.Size(51, 22);
            _ClosedChannelsLabel.Text = "Closed ?";
            _ClosedChannelsLabel.ToolTipText = "Closed channel(s)";
            // 
            // ScanView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(_SubsystemToolStrip);
            Name = "ScanView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(424, 27);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStripLabel _InternalScanListLabel;
        private ToolStrip _SubsystemToolStrip;
        private ToolStripLabel _ScanListFunctionLabel;
        private ToolStripLabel _ClosedChannelsLabel;
        private ToolStripMenuItem __ReleaseInternalScanListMenuItem;

        private ToolStripMenuItem _ReleaseInternalScanListMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReleaseInternalScanListMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReleaseInternalScanListMenuItem != null)
                {
                    __ReleaseInternalScanListMenuItem.Click -= ReleaseInternalScanListMenuItem_Click;
                }

                __ReleaseInternalScanListMenuItem = value;
                if (__ReleaseInternalScanListMenuItem != null)
                {
                    __ReleaseInternalScanListMenuItem.Click += ReleaseInternalScanListMenuItem_Click;
                }
            }
        }

        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ApplyInternalScanListMenuItem;

        private ToolStripMenuItem _ApplyInternalScanListMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyInternalScanListMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyInternalScanListMenuItem != null)
                {
                    __ApplyInternalScanListMenuItem.Click -= ApplyInternalScanListMenuItem_Click;
                }

                __ApplyInternalScanListMenuItem = value;
                if (__ApplyInternalScanListMenuItem != null)
                {
                    __ApplyInternalScanListMenuItem.Click += ApplyInternalScanListMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ApplyInternalScanFunctionListMenuItem;

        private ToolStripMenuItem _ApplyInternalScanFunctionListMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyInternalScanFunctionListMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyInternalScanFunctionListMenuItem != null)
                {
                    __ApplyInternalScanFunctionListMenuItem.Click -= ApplyInternalScanFunctionListMenuItem_Click;
                }

                __ApplyInternalScanFunctionListMenuItem = value;
                if (__ApplyInternalScanFunctionListMenuItem != null)
                {
                    __ApplyInternalScanFunctionListMenuItem.Click += ApplyInternalScanFunctionListMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __SelectInternalScanListMenuItem;

        private ToolStripMenuItem _SelectInternalScanListMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SelectInternalScanListMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SelectInternalScanListMenuItem != null)
                {
                    __SelectInternalScanListMenuItem.Click -= SelectInternalScanListMenuItem_Click;
                }

                __SelectInternalScanListMenuItem = value;
                if (__SelectInternalScanListMenuItem != null)
                {
                    __SelectInternalScanListMenuItem.Click += SelectInternalScanListMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripComboBox _CandidateScanListComboBox;
    }
}