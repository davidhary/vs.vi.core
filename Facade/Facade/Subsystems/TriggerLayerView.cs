using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.SplitExtensions;
using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{

    /// <summary> An Trigger Layer view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class TriggerLayerView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public TriggerLayerView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._CountNumeric.NumericUpDownControl.Minimum = 1m;
            this._CountNumeric.NumericUpDownControl.Maximum = 99999m;
            this._CountNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._CountNumeric.NumericUpDownControl.Value = 1m;
            this._InputLineNumeric.NumericUpDownControl.Minimum = 1m;
            this._InputLineNumeric.NumericUpDownControl.Maximum = 6m;
            this._InputLineNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._InputLineNumeric.NumericUpDownControl.Value = 2m;
            this._OutputLineNumeric.NumericUpDownControl.Minimum = 1m;
            this._OutputLineNumeric.NumericUpDownControl.Maximum = 6m;
            this._OutputLineNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._OutputLineNumeric.NumericUpDownControl.Value = 1m;
            this._TimerIntervalNumeric.NumericUpDownControl.Minimum = 0m;
            this._TimerIntervalNumeric.NumericUpDownControl.Maximum = 99999m;
            this._TimerIntervalNumeric.NumericUpDownControl.DecimalPlaces = 3;
            this._TimerIntervalNumeric.NumericUpDownControl.Value = 1m;
            this._DelayNumeric.NumericUpDownControl.Minimum = 0m;
            this._DelayNumeric.NumericUpDownControl.Maximum = 99999m;
            this._DelayNumeric.NumericUpDownControl.DecimalPlaces = 3;
            this._DelayNumeric.NumericUpDownControl.Value = 0m;
            this.__ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
            this.__SendBusTriggerMenuItem.Name = "_SendBusTriggerMenuItem";
            this.__InfiniteCountButton.Name = "_InfiniteCountButton";
            this.__BypassToggleButton.Name = "_BypassToggleButton";
        }

        /// <summary> Creates a new TriggerLayerView. </summary>
        /// <returns> A TriggerLayerView. </returns>
        public static TriggerLayerView Create()
        {
            TriggerLayerView view = null;
            try
            {
                view = new TriggerLayerView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> The layer number. </summary>
        private int _LayerNumber;

        /// <summary> Gets or sets the layer number. </summary>
        /// <value> The layer number. </value>
        public int LayerNumber
        {
            get => this._LayerNumber;

            set {
                if ( value != this.LayerNumber )
                {
                    this._LayerNumber = value;
                    this._SubsystemSplitButton.Text = $"Trig{value}";
                }
            }
        }

        /// <summary> Gets or sets the number of triggers. </summary>
        /// <value> The number of triggers. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int Count
        {
            get => this._InfiniteCountButton.CheckState == CheckState.Checked ? int.MaxValue : ( int ) this._CountNumeric.Value;

            set {
                if ( value != this.Count )
                {
                    if ( value == int.MaxValue )
                    {
                        this._InfiniteCountButton.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        this._InfiniteCountButton.CheckState = CheckState.Unchecked;
                        this._CountNumeric.Value = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the trigger source. </summary>
        /// <value> The trigger source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSources Source
        {
            get => ( TriggerSources ) Conversions.ToInteger( this._SourceComboBox.ComboBox.SelectedValue );

            set {
                if ( value != this.Source )
                {
                    this._SourceComboBox.ComboBox.SelectedItem = value.ValueNamePair();
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Applies the settings onto the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying {this._SubsystemSplitButton.Text} settings";
                _ = this.PublishInfo( $"{activity};. " );
                this.TriggerSubsystem.StartElapsedStopwatch();
                _ = this._InfiniteCountButton.CheckState == CheckState.Checked
                    ? this.TriggerSubsystem.ApplyTriggerCount( int.MaxValue )
                    : this.TriggerSubsystem.ApplyTriggerCount( ( int ) this._CountNumeric.Value );

                _ = this.TriggerSubsystem.ApplyInputLineNumber( ( int ) this._InputLineNumeric.Value );
                _ = this.TriggerSubsystem.ApplyOutputLineNumber( ( int ) this._OutputLineNumeric.Value );
                _ = this.TriggerSubsystem.ApplyTriggerSource( ( TriggerSources ) Conversions.ToInteger( this._SourceComboBox.ComboBox.SelectedValue ) );
                _ = this.TriggerSubsystem.ApplyTriggerLayerBypassMode( this._BypassToggleButton.CheckState == CheckState.Checked ? TriggerLayerBypassModes.Source : TriggerLayerBypassModes.Acceptor );
                _ = this.TriggerSubsystem.ApplyDelay( TimeSpan.FromMilliseconds( ( double ) (1000m * this._DelayNumeric.Value) ) );
                _ = this.TriggerSubsystem.ApplyTimerTimeSpan( TimeSpan.FromMilliseconds( ( double ) (1000m * this._TimerIntervalNumeric.Value) ) );
                this.TriggerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads the settings from the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} reading  {this._SubsystemSplitButton.Text} settings";
                _ = this.PublishInfo( $"{activity};. " );
                ReadSettings( this.TriggerSubsystem );
                this.ApplyPropertyChanged( this.TriggerSubsystem );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( TriggerLayerView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER SUBSYSTEM "

        /// <summary> Gets or sets the trigger subsystem. </summary>
        /// <value> The trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystemBase TriggerSubsystem { get; private set; }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem">   The subsystem. </param>
        /// <param name="layerNumber"> The layer number. </param>
        public void BindSubsystem( TriggerSubsystemBase subsystem, int layerNumber )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            this.LayerNumber = layerNumber;
            this.TriggerSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.SupportedTriggerSources ) );
                // must not read setting when biding because the instrument may be locked or in a trigger mode
                // The bound values should be sent when binding or when applying propert change.
                // ReadSettings( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( TriggerSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.TriggerCount ) );
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.TriggerSource ) );
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.TriggerLayerBypassMode ) );
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.InputLineNumber ) );
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.IsTriggerLayerBypass ) );
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.IsTriggerCountInfinite ) );
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.OutputLineNumber ) );
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.Delay ) );
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.TimerInterval ) );
        }

        /// <summary> Reads the settings from the instrument. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( TriggerSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryTriggerCount();
            _ = subsystem.QueryInputLineNumber();
            _ = subsystem.QueryOutputLineNumber();
            _ = subsystem.QueryTriggerSource();
            _ = subsystem.QueryTriggerLayerBypassMode();
            _ = subsystem.QueryDelay();
            _ = subsystem.QueryTimerTimeSpan();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Handle the Calculate subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TriggerSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( TriggerSubsystemBase.TriggerCount ):
                    {
                        if ( subsystem.TriggerCount.HasValue )
                            this._CountNumeric.Value = subsystem.TriggerCount.Value;
                        break;
                    }

                case nameof( TriggerSubsystemBase.TriggerSource ):
                    {
                        if ( subsystem.TriggerSource.HasValue && this._SourceComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._SourceComboBox.ComboBox.SelectedItem = subsystem.TriggerSource.Value.ValueNamePair();
                        }

                        this._SendBusTriggerMenuItem.Enabled = Nullable.Equals( subsystem.TriggerSource, TriggerSources.Bus );
                        this.NotifyPropertyChanged( nameof( this.Source ) );
                        break;
                    }

                case nameof( TriggerSubsystemBase.SupportedTriggerSources ):
                    {
                        this.InitializingComponents = true;
                        this._SourceComboBox.ComboBox.ListSupportedTriggerSources( subsystem.SupportedTriggerSources );
                        this.InitializingComponents = false;
                        if ( subsystem.TriggerSource.HasValue && this._SourceComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._SourceComboBox.ComboBox.SelectedItem = subsystem.TriggerSource.Value.ValueNamePair();
                        }

                        break;
                    }

                case nameof( TriggerSubsystemBase.IsTriggerLayerBypass ):
                    {
                        this._BypassToggleButton.CheckState = subsystem.TriggerLayerBypassMode.HasValue ? subsystem.TriggerLayerBypassMode.Value == TriggerLayerBypassModes.Acceptor ? CheckState.Checked : CheckState.Unchecked : CheckState.Indeterminate;
                        break;
                    }

                case nameof( TriggerSubsystemBase.InputLineNumber ):
                    {
                        if ( subsystem.InputLineNumber.HasValue )
                            this._InputLineNumeric.Value = subsystem.InputLineNumber.Value;
                        break;
                    }

                case nameof( TriggerSubsystemBase.IsTriggerCountInfinite ):
                    {
                        this._InfiniteCountButton.CheckState = subsystem.IsTriggerCountInfinite.ToCheckState();
                        break;
                    }

                case var @case when @case == nameof( TriggerSubsystemBase.IsTriggerLayerBypass ):
                    {
                        this._BypassToggleButton.CheckState = subsystem.IsTriggerLayerBypass.ToCheckState();
                        break;
                    }

                case nameof( TriggerSubsystemBase.OutputLineNumber ):
                    {
                        if ( subsystem.OutputLineNumber.HasValue )
                            this._OutputLineNumeric.Value = subsystem.OutputLineNumber.Value;
                        break;
                    }

                case nameof( TriggerSubsystemBase.Delay ):
                    {
                        if ( subsystem.Delay.HasValue )
                            this._DelayNumeric.Value = ( decimal ) (0.001d * subsystem.Delay.Value.TotalMilliseconds);
                        break;
                    }

                case nameof( TriggerSubsystemBase.TimerInterval ):
                    {
                        if ( subsystem.TimerInterval.HasValue )
                            this._TimerIntervalNumeric.Value = ( decimal ) (0.001d * subsystem.TimerInterval.Value.TotalMilliseconds);
                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( TriggerSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Bypass toggle button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void BypassToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            this._BypassToggleButton.Text = this._BypassToggleButton.CheckState.ToCheckStateCaption( "Bypass", "~Bypass", "Bypass?" );
        }

        /// <summary> Infinite count button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void InfiniteCountButton_CheckStateChanged( object sender, EventArgs e )
        {
            this._InfiniteCountButton.Text = this._InfiniteCountButton.CheckState.ToCheckStateCaption( "Infinite", "Finite", "INF?" );
        }

        /// <summary> Applies the settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ReadSettings();
        }

        /// <summary> Sends the bus trigger menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SendBusTriggerMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.Device.ResourceNameCaption} sending bus trigger";
                    this.Device.Session.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.Device.Session.AssertTrigger();
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
