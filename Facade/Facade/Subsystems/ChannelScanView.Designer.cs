﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class ChannelScanView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ChannelScanView));
            _Layout = new TableLayoutPanel();
            _InfoTextBox = new TextBox();
            _ChannelView = new ChannelView();
            ScanView = new ScanView();
            _RouteToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            _Layout.SuspendLayout();
            _RouteToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 2;
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 2.0f));
            _Layout.Controls.Add(_InfoTextBox, 0, 3);
            _Layout.Controls.Add(_ChannelView, 0, 1);
            _Layout.Controls.Add(ScanView, 0, 2);
            _Layout.Controls.Add(_RouteToolStrip, 0, 0);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 4;
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.Size = new System.Drawing.Size(397, 274);
            _Layout.TabIndex = 0;
            // 
            // _InfoTextBox
            // 
            _InfoTextBox.Dock = DockStyle.Fill;
            _InfoTextBox.Location = new System.Drawing.Point(3, 95);
            _InfoTextBox.Margin = new Padding(3, 4, 3, 4);
            _InfoTextBox.Multiline = true;
            _InfoTextBox.Name = "_InfoTextBox";
            _InfoTextBox.ReadOnly = true;
            _InfoTextBox.ScrollBars = ScrollBars.Both;
            _InfoTextBox.Size = new System.Drawing.Size(389, 175);
            _InfoTextBox.TabIndex = 27;
            _InfoTextBox.Text = "<info>";
            // 
            // _ChannelView
            // 
            _ChannelView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _ChannelView.BorderStyle = BorderStyle.FixedSingle;
            _ChannelView.Dock = DockStyle.Top;
            _ChannelView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ChannelView.Location = new System.Drawing.Point(3, 28);
            _ChannelView.Name = "_ChannelView";
            _ChannelView.Padding = new Padding(1);
            _ChannelView.Size = new System.Drawing.Size(389, 27);
            _ChannelView.TabIndex = 10;
            // 
            // _ScanView
            // 
            ScanView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            ScanView.BorderStyle = BorderStyle.FixedSingle;
            ScanView.Dock = DockStyle.Top;
            ScanView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            ScanView.Location = new System.Drawing.Point(3, 61);
            ScanView.Name = "_ScanView";
            ScanView.Padding = new Padding(1);
            ScanView.Size = new System.Drawing.Size(389, 27);
            ScanView.TabIndex = 11;
            // 
            // _RouteToolStrip
            // 
            _RouteToolStrip.AutoSize = false;
            _RouteToolStrip.BackColor = System.Drawing.Color.Transparent;
            _RouteToolStrip.Dock = DockStyle.None;
            _RouteToolStrip.GripMargin = new Padding(0);
            _RouteToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton });
            _RouteToolStrip.Location = new System.Drawing.Point(0, 0);
            _RouteToolStrip.Name = "_RouteToolStrip";
            _RouteToolStrip.Size = new System.Drawing.Size(395, 25);
            _RouteToolStrip.Stretch = true;
            _RouteToolStrip.TabIndex = 9;
            _RouteToolStrip.Text = "Trigger";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(53, 22);
            _SubsystemSplitButton.Text = "Route";
            _SubsystemSplitButton.ToolTipText = "Subsystem actions split button";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(146, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            __ReadSettingsMenuItem.ToolTipText = "Reads settings";
            // 
            // ChannelScanView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "ChannelScanView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(399, 276);
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            _RouteToolStrip.ResumeLayout(false);
            _RouteToolStrip.PerformLayout();
            ResumeLayout(false);
        }

        private TableLayoutPanel _Layout;
        private ToolStrip _RouteToolStrip;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ChannelView _ChannelView;
        private TextBox _InfoTextBox;
        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }
    }
}