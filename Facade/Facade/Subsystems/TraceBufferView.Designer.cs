﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class TraceBufferView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(TraceBufferView));
            _LimitToolStripPanel = new ToolStripPanel();
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            _FeedSourceComboLabel = new ToolStripLabel();
            _FeedSourceComboBox = new ToolStripComboBox();
            __ElementGroupToggleButton = new ToolStripButton();
            __ElementGroupToggleButton.CheckStateChanged += new EventHandler(ElementGroupToggleButton_CheckStateChanged);
            _BufferControlComboBoxLabel = new ToolStripLabel();
            _FeedControlComboBox = new ToolStripComboBox();
            _BufferConfigurationToolStrip = new ToolStrip();
            _BufferCondigureLabel = new ToolStripLabel();
            _BufferNameLabel = new ToolStripLabel();
            _SizeNumericLabel = new ToolStripLabel();
            _SizeNumeric = new Core.Controls.ToolStripNumericUpDown();
            _PreTriggerCountNumericLabel = new ToolStripLabel();
            _PreTriggerCountNumeric = new Core.Controls.ToolStripNumericUpDown();
            __ClearBufferButton = new ToolStripButton();
            __ClearBufferButton.Click += new EventHandler(ClearBufferButton_Click);
            _FreeCountLabelLabel = new ToolStripLabel();
            _FreeCountLabel = new ToolStripLabel();
            _BufferFormatToolStrip = new ToolStrip();
            _FormatLabel = new ToolStripLabel();
            __TimestampFormatToggleButton = new ToolStripButton();
            __TimestampFormatToggleButton.CheckStateChanged += new EventHandler(TimestampFormatToggleButton_CheckStateChanged);
            _InfoTextBox = new TextBox();
            _LimitToolStripPanel.SuspendLayout();
            _SubsystemToolStrip.SuspendLayout();
            _BufferConfigurationToolStrip.SuspendLayout();
            _BufferFormatToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _LimitToolStripPanel
            // 
            _LimitToolStripPanel.BackColor = System.Drawing.Color.Transparent;
            _LimitToolStripPanel.Controls.Add(_BufferConfigurationToolStrip);
            _LimitToolStripPanel.Controls.Add(_SubsystemToolStrip);
            _LimitToolStripPanel.Controls.Add(_BufferFormatToolStrip);
            _LimitToolStripPanel.Dock = DockStyle.Top;
            _LimitToolStripPanel.Location = new System.Drawing.Point(1, 1);
            _LimitToolStripPanel.Name = "_LimitToolStripPanel";
            _LimitToolStripPanel.Orientation = Orientation.Horizontal;
            _LimitToolStripPanel.RowMargin = new Padding(0);
            _LimitToolStripPanel.Size = new System.Drawing.Size(440, 78);
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _FeedSourceComboLabel, _FeedSourceComboBox, __ElementGroupToggleButton, _BufferControlComboBoxLabel, _FeedControlComboBox });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(440, 25);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 0;
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DoubleClickEnabled = true;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(58, 22);
            _SubsystemSplitButton.Text = "Trace";
            _SubsystemSplitButton.ToolTipText = "Trace or buffer label";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(150, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            __ApplySettingsMenuItem.ToolTipText = "Applies settings onto the instrument";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(150, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            __ReadSettingsMenuItem.ToolTipText = "Reads settings fromt he instrument";
            // 
            // _FeedSourceComboLabel
            // 
            _FeedSourceComboLabel.Name = "_FeedSourceComboLabel";
            _FeedSourceComboLabel.Size = new System.Drawing.Size(34, 22);
            _FeedSourceComboLabel.Text = "Data:";
            // 
            // _FeedSourceComboBox
            // 
            _FeedSourceComboBox.Name = "_FeedSourceComboBox";
            _FeedSourceComboBox.Size = new System.Drawing.Size(80, 25);
            _FeedSourceComboBox.Text = "Sense";
            _FeedSourceComboBox.ToolTipText = "Selects the source of buffer data";
            // 
            // _ElementGroupToggleButton
            // 
            __ElementGroupToggleButton.Checked = true;
            __ElementGroupToggleButton.CheckOnClick = true;
            __ElementGroupToggleButton.CheckState = CheckState.Indeterminate;
            __ElementGroupToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __ElementGroupToggleButton.Image = (System.Drawing.Image)resources.GetObject("_ElementGroupToggleButton.Image");
            __ElementGroupToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ElementGroupToggleButton.Name = "__ElementGroupToggleButton";
            __ElementGroupToggleButton.Size = new System.Drawing.Size(67, 22);
            __ElementGroupToggleButton.Text = "Elements ?";
            __ElementGroupToggleButton.ToolTipText = "Toggle element group between Full and Compact";
            // 
            // _BufferControlComboBoxLabel
            // 
            _BufferControlComboBoxLabel.Name = "_BufferControlComboBoxLabel";
            _BufferControlComboBoxLabel.Size = new System.Drawing.Size(50, 22);
            _BufferControlComboBoxLabel.Text = "Control:";
            // 
            // _FeedControlComboBox
            // 
            _FeedControlComboBox.Name = "_FeedControlComboBox";
            _FeedControlComboBox.Size = new System.Drawing.Size(121, 25);
            _FeedControlComboBox.Text = "Never";
            _FeedControlComboBox.ToolTipText = "Selects the feed control";
            // 
            // _BufferConfigurationToolStrip
            // 
            _BufferConfigurationToolStrip.BackColor = System.Drawing.Color.Transparent;
            _BufferConfigurationToolStrip.GripMargin = new Padding(0);
            _BufferConfigurationToolStrip.Items.AddRange(new ToolStripItem[] { _BufferCondigureLabel, _BufferNameLabel, _SizeNumericLabel, _SizeNumeric, _PreTriggerCountNumericLabel, _PreTriggerCountNumeric, __ClearBufferButton, _FreeCountLabelLabel, _FreeCountLabel });
            _BufferConfigurationToolStrip.Location = new System.Drawing.Point(0, 25);
            _BufferConfigurationToolStrip.Name = "_BufferConfigurationToolStrip";
            _BufferConfigurationToolStrip.Size = new System.Drawing.Size(440, 28);
            _BufferConfigurationToolStrip.Stretch = true;
            _BufferConfigurationToolStrip.TabIndex = 1;
            // 
            // _BufferCondigureLabel
            // 
            _BufferCondigureLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _BufferCondigureLabel.Margin = new Padding(10, 1, 0, 2);
            _BufferCondigureLabel.Name = "_BufferCondigureLabel";
            _BufferCondigureLabel.Size = new System.Drawing.Size(39, 25);
            _BufferCondigureLabel.Text = "Buffer";
            // 
            // _BufferNameLabel
            // 
            _BufferNameLabel.Name = "_BufferNameLabel";
            _BufferNameLabel.Size = new System.Drawing.Size(34, 25);
            _BufferNameLabel.Text = "Trace";
            _BufferNameLabel.ToolTipText = "Buffer name";
            // 
            // _SizeNumericLabel
            // 
            _SizeNumericLabel.Name = "_SizeNumericLabel";
            _SizeNumericLabel.Size = new System.Drawing.Size(30, 25);
            _SizeNumericLabel.Text = "Size:";
            // 
            // _SizeNumeric
            // 
            _SizeNumeric.Name = "_SizeNumeric";
            _SizeNumeric.Size = new System.Drawing.Size(41, 25);
            _SizeNumeric.Text = "3";
            _SizeNumeric.ToolTipText = "Buffer size";
            _SizeNumeric.Value = new decimal(new int[] { 3, 0, 0, 0 });
            // 
            // _PreTriggerCountNumericLabel
            // 
            _PreTriggerCountNumericLabel.Margin = new Padding(3, 1, 0, 2);
            _PreTriggerCountNumericLabel.Name = "_PreTriggerCountNumericLabel";
            _PreTriggerCountNumericLabel.Size = new System.Drawing.Size(68, 25);
            _PreTriggerCountNumericLabel.Text = "Pre-Trigger:";
            // 
            // _PreTriggerCountNumeric
            // 
            _PreTriggerCountNumeric.Name = "_PreTriggerCountNumeric";
            _PreTriggerCountNumeric.Size = new System.Drawing.Size(41, 25);
            _PreTriggerCountNumeric.Text = "0";
            _PreTriggerCountNumeric.ToolTipText = "Number of Pre-Trigger  readings";
            _PreTriggerCountNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _ClearBufferButton
            // 
            __ClearBufferButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __ClearBufferButton.Image = (System.Drawing.Image)resources.GetObject("_ClearBufferButton.Image");
            __ClearBufferButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ClearBufferButton.Name = "__ClearBufferButton";
            __ClearBufferButton.Size = new System.Drawing.Size(38, 25);
            __ClearBufferButton.Text = "Clear";
            __ClearBufferButton.ToolTipText = "Clears the buffer";
            // 
            // _FreeCountLabelLabel
            // 
            _FreeCountLabelLabel.Name = "_FreeCountLabelLabel";
            _FreeCountLabelLabel.Size = new System.Drawing.Size(32, 25);
            _FreeCountLabelLabel.Text = "Free:";
            // 
            // _FreeCountLabel
            // 
            _FreeCountLabel.Name = "_FreeCountLabel";
            _FreeCountLabel.Size = new System.Drawing.Size(13, 25);
            _FreeCountLabel.Text = "0";
            // 
            // _BufferFormatToolStrip
            // 
            _BufferFormatToolStrip.BackColor = System.Drawing.Color.Transparent;
            _BufferFormatToolStrip.GripMargin = new Padding(0);
            _BufferFormatToolStrip.Items.AddRange(new ToolStripItem[] { _FormatLabel, __TimestampFormatToggleButton });
            _BufferFormatToolStrip.Location = new System.Drawing.Point(0, 53);
            _BufferFormatToolStrip.Name = "_BufferFormatToolStrip";
            _BufferFormatToolStrip.Size = new System.Drawing.Size(440, 25);
            _BufferFormatToolStrip.Stretch = true;
            _BufferFormatToolStrip.TabIndex = 2;
            // 
            // _FormatLabel
            // 
            _FormatLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _FormatLabel.Margin = new Padding(6, 1, 2, 2);
            _FormatLabel.Name = "_FormatLabel";
            _FormatLabel.Size = new System.Drawing.Size(44, 22);
            _FormatLabel.Text = "Format";
            // 
            // _TimestampFormatToggleButton
            // 
            __TimestampFormatToggleButton.Checked = true;
            __TimestampFormatToggleButton.CheckOnClick = true;
            __TimestampFormatToggleButton.CheckState = CheckState.Indeterminate;
            __TimestampFormatToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __TimestampFormatToggleButton.Image = (System.Drawing.Image)resources.GetObject("_TimestampFormatToggleButton.Image");
            __TimestampFormatToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __TimestampFormatToggleButton.Name = "__TimestampFormatToggleButton";
            __TimestampFormatToggleButton.Size = new System.Drawing.Size(131, 22);
            __TimestampFormatToggleButton.Text = "Timestamp: Absolute ?";
            __TimestampFormatToggleButton.ToolTipText = "Toggles timestamp format style";
            // 
            // _InfoTextBox
            // 
            _InfoTextBox.Dock = DockStyle.Fill;
            _InfoTextBox.Location = new System.Drawing.Point(1, 79);
            _InfoTextBox.Margin = new Padding(3, 4, 3, 4);
            _InfoTextBox.Multiline = true;
            _InfoTextBox.Name = "_InfoTextBox";
            _InfoTextBox.ReadOnly = true;
            _InfoTextBox.ScrollBars = ScrollBars.Both;
            _InfoTextBox.Size = new System.Drawing.Size(440, 167);
            _InfoTextBox.TabIndex = 0;
            _InfoTextBox.Text = "<info>";
            // 
            // TraceBufferView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_InfoTextBox);
            Controls.Add(_LimitToolStripPanel);
            Name = "TraceBufferView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(442, 247);
            _LimitToolStripPanel.ResumeLayout(false);
            _LimitToolStripPanel.PerformLayout();
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            _BufferConfigurationToolStrip.ResumeLayout(false);
            _BufferConfigurationToolStrip.PerformLayout();
            _BufferFormatToolStrip.ResumeLayout(false);
            _BufferFormatToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStripPanel _LimitToolStripPanel;
        private ToolStrip _BufferFormatToolStrip;
        private ToolStripLabel _FormatLabel;
        private ToolStrip _BufferConfigurationToolStrip;
        private ToolStripLabel _BufferCondigureLabel;
        private Core.Controls.ToolStripNumericUpDown _SizeNumeric;
        private ToolStripLabel _PreTriggerCountNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _PreTriggerCountNumeric;
        private ToolStrip _SubsystemToolStrip;
        private ToolStripLabel _SizeNumericLabel;
        private ToolStripLabel _FeedSourceComboLabel;
        private ToolStripComboBox _FeedSourceComboBox;
        private ToolStripLabel _BufferNameLabel;
        private ToolStripButton __ClearBufferButton;

        private ToolStripButton _ClearBufferButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearBufferButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearBufferButton != null)
                {
                    __ClearBufferButton.Click -= ClearBufferButton_Click;
                }

                __ClearBufferButton = value;
                if (__ClearBufferButton != null)
                {
                    __ClearBufferButton.Click += ClearBufferButton_Click;
                }
            }
        }

        private ToolStripLabel _FreeCountLabelLabel;
        private ToolStripLabel _FreeCountLabel;
        private ToolStripButton __ElementGroupToggleButton;

        private ToolStripButton _ElementGroupToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ElementGroupToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ElementGroupToggleButton != null)
                {
                    __ElementGroupToggleButton.CheckStateChanged -= ElementGroupToggleButton_CheckStateChanged;
                }

                __ElementGroupToggleButton = value;
                if (__ElementGroupToggleButton != null)
                {
                    __ElementGroupToggleButton.CheckStateChanged += ElementGroupToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStripLabel _BufferControlComboBoxLabel;
        private ToolStripComboBox _FeedControlComboBox;
        private ToolStripButton __TimestampFormatToggleButton;

        private ToolStripButton _TimestampFormatToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TimestampFormatToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TimestampFormatToggleButton != null)
                {
                    __TimestampFormatToggleButton.CheckStateChanged -= TimestampFormatToggleButton_CheckStateChanged;
                }

                __TimestampFormatToggleButton = value;
                if (__TimestampFormatToggleButton != null)
                {
                    __TimestampFormatToggleButton.CheckStateChanged += TimestampFormatToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }

        private TextBox _InfoTextBox;
    }
}