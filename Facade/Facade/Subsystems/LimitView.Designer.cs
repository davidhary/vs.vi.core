﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class LimitView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(LimitView));
            _LimitToolStripPanel = new ToolStripPanel();
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            __PerformLimitStripMenuItem = new ToolStripMenuItem();
            __PerformLimitStripMenuItem.Click += new EventHandler(PerformLimitStripMenuItem_Click);
            __ReadLimitTestMenuItem = new ToolStripMenuItem();
            __ReadLimitTestMenuItem.Click += new EventHandler(ReadLimitTestMenuItem_Click);
            __LimitEnabledToggleButton = new ToolStripButton();
            __LimitEnabledToggleButton.CheckStateChanged += new EventHandler(LimitEnabledToggleButton_CheckStateChanged);
            __AutoClearToggleButton = new ToolStripButton();
            __AutoClearToggleButton.CheckStateChanged += new EventHandler(AutoClearButton_CheckStateChanged);
            __LimitFailedButton = new ToolStripButton();
            __LimitFailedButton.CheckStateChanged += new EventHandler(LimitFailedToggleButton_CheckStateChanged);
            _UpperLimitToolStrip = new ToolStrip();
            _UpperLimitLabel = new ToolStripLabel();
            _UpperLimitDecimalsNumericLabel = new ToolStripLabel();
            __UpperLimitDecimalsNumeric = new Core.Controls.ToolStripNumericUpDown();
            __UpperLimitDecimalsNumeric.ValueChanged += new EventHandler<EventArgs>(UpperLimitDecimalsNumeric_ValueChanged);
            _UpperLimitNumericLabel = new ToolStripLabel();
            _UpperLimitNumeric = new Core.Controls.ToolStripNumericUpDown();
            __UpperLimitBitPatternNumericButton = new ToolStripButton();
            __UpperLimitBitPatternNumericButton.CheckStateChanged += new EventHandler(UpperLimitBitPatternNumericButton_CheckStateChanged);
            _UpperLimitBitPatternNumeric = new Core.Controls.ToolStripNumericUpDown();
            _LowerLimitToolStrip = new ToolStrip();
            _LowerLimitLabel = new ToolStripLabel();
            _LowerLimitDecimalsNumericLabel = new ToolStripLabel();
            __LowerLimitDecimalsNumeric = new Core.Controls.ToolStripNumericUpDown();
            __LowerLimitDecimalsNumeric.ValueChanged += new EventHandler<EventArgs>(LowerLimitDecimalsNumeric_ValueChanged);
            _LowerLimitNumericLabel = new ToolStripLabel();
            _LowerLimitNumeric = new Core.Controls.ToolStripNumericUpDown();
            __LowerLimitBitPatternNumericButton = new ToolStripButton();
            __LowerLimitBitPatternNumericButton.CheckStateChanged += new EventHandler(LowerBitPatternNumericButton_CheckStateChanged);
            _LowerLimitBitPatternNumeric = new Core.Controls.ToolStripNumericUpDown();
            _LimitToolStripPanel.SuspendLayout();
            _SubsystemToolStrip.SuspendLayout();
            _UpperLimitToolStrip.SuspendLayout();
            _LowerLimitToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _LimitToolStripPanel
            // 
            _LimitToolStripPanel.BackColor = System.Drawing.Color.Transparent;
            _LimitToolStripPanel.Controls.Add(_SubsystemToolStrip);
            _LimitToolStripPanel.Controls.Add(_UpperLimitToolStrip);
            _LimitToolStripPanel.Controls.Add(_LowerLimitToolStrip);
            _LimitToolStripPanel.Dock = DockStyle.Top;
            _LimitToolStripPanel.Location = new System.Drawing.Point(1, 1);
            _LimitToolStripPanel.Name = "_LimitToolStripPanel";
            _LimitToolStripPanel.Orientation = Orientation.Horizontal;
            _LimitToolStripPanel.RowMargin = new Padding(0);
            _LimitToolStripPanel.Size = new System.Drawing.Size(387, 81);
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.Dock = DockStyle.None;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, __LimitEnabledToggleButton, __AutoClearToggleButton, __LimitFailedButton });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(387, 25);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 3;
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DoubleClickEnabled = true;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem, __PerformLimitStripMenuItem, __ReadLimitTestMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(57, 22);
            _SubsystemSplitButton.Text = "Limit1";
            _SubsystemSplitButton.ToolTipText = "Double-click to read limit state";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(184, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            __ApplySettingsMenuItem.ToolTipText = "Applies settings onto the instrument";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(184, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            __ReadSettingsMenuItem.ToolTipText = "Reads settings from the instrument";
            // 
            // _PerformLimitStripMenuItem
            // 
            __PerformLimitStripMenuItem.Name = "__PerformLimitStripMenuItem";
            __PerformLimitStripMenuItem.Size = new System.Drawing.Size(184, 22);
            __PerformLimitStripMenuItem.Text = "Perform Limit Test";
            __PerformLimitStripMenuItem.ToolTipText = "Perform limit test";
            // 
            // _ReadLimitTestMenuItem
            // 
            __ReadLimitTestMenuItem.Name = "__ReadLimitTestMenuItem";
            __ReadLimitTestMenuItem.Size = new System.Drawing.Size(184, 22);
            __ReadLimitTestMenuItem.Text = "Read Limit Test State";
            __ReadLimitTestMenuItem.ToolTipText = "Read limit test result";
            // 
            // _LimitEnabledToggleButton
            // 
            __LimitEnabledToggleButton.Checked = true;
            __LimitEnabledToggleButton.CheckOnClick = true;
            __LimitEnabledToggleButton.CheckState = CheckState.Indeterminate;
            __LimitEnabledToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __LimitEnabledToggleButton.Image = (System.Drawing.Image)resources.GetObject("_LimitEnabledToggleButton.Image");
            __LimitEnabledToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __LimitEnabledToggleButton.Name = "__LimitEnabledToggleButton";
            __LimitEnabledToggleButton.Size = new System.Drawing.Size(56, 22);
            __LimitEnabledToggleButton.Text = "Disabled";
            __LimitEnabledToggleButton.ToolTipText = "toggle to enable or disable this limit";
            // 
            // _AutoClearToggleButton
            // 
            __AutoClearToggleButton.Checked = true;
            __AutoClearToggleButton.CheckOnClick = true;
            __AutoClearToggleButton.CheckState = CheckState.Indeterminate;
            __AutoClearToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AutoClearToggleButton.Image = (System.Drawing.Image)resources.GetObject("_AutoClearToggleButton.Image");
            __AutoClearToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AutoClearToggleButton.Name = "__AutoClearToggleButton";
            __AutoClearToggleButton.Size = new System.Drawing.Size(90, 22);
            __AutoClearToggleButton.Text = "Auto Clear: Off";
            __AutoClearToggleButton.ToolTipText = "Toggle to turn auto clear on or off";
            // 
            // _LimitFailedButton
            // 
            __LimitFailedButton.Checked = true;
            __LimitFailedButton.CheckState = CheckState.Indeterminate;
            __LimitFailedButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __LimitFailedButton.Image = (System.Drawing.Image)resources.GetObject("_LimitFailedButton.Image");
            __LimitFailedButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __LimitFailedButton.Name = "__LimitFailedButton";
            __LimitFailedButton.Size = new System.Drawing.Size(47, 22);
            __LimitFailedButton.Text = "Failed?";
            __LimitFailedButton.ToolTipText = "displays fail state";
            // 
            // _UpperLimitToolStrip
            // 
            _UpperLimitToolStrip.BackColor = System.Drawing.Color.Transparent;
            _UpperLimitToolStrip.Dock = DockStyle.None;
            _UpperLimitToolStrip.GripMargin = new Padding(0);
            _UpperLimitToolStrip.Items.AddRange(new ToolStripItem[] { _UpperLimitLabel, _UpperLimitDecimalsNumericLabel, __UpperLimitDecimalsNumeric, _UpperLimitNumericLabel, _UpperLimitNumeric, __UpperLimitBitPatternNumericButton, _UpperLimitBitPatternNumeric });
            _UpperLimitToolStrip.Location = new System.Drawing.Point(0, 25);
            _UpperLimitToolStrip.Name = "_UpperLimitToolStrip";
            _UpperLimitToolStrip.Size = new System.Drawing.Size(340, 28);
            _UpperLimitToolStrip.TabIndex = 2;
            // 
            // _UpperLimitLabel
            // 
            _UpperLimitLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _UpperLimitLabel.Margin = new Padding(6, 1, 0, 2);
            _UpperLimitLabel.Name = "_UpperLimitLabel";
            _UpperLimitLabel.Size = new System.Drawing.Size(39, 25);
            _UpperLimitLabel.Text = "Upper";
            // 
            // _UpperLimitDecimalsNumericLabel
            // 
            _UpperLimitDecimalsNumericLabel.Name = "_UpperLimitDecimalsNumericLabel";
            _UpperLimitDecimalsNumericLabel.Size = new System.Drawing.Size(58, 25);
            _UpperLimitDecimalsNumericLabel.Text = "Decimals:";
            // 
            // _UpperLimitDecimalsNumeric
            // 
            __UpperLimitDecimalsNumeric.Name = "__UpperLimitDecimalsNumeric";
            __UpperLimitDecimalsNumeric.Size = new System.Drawing.Size(41, 25);
            __UpperLimitDecimalsNumeric.Text = "3";
            __UpperLimitDecimalsNumeric.ToolTipText = "Number of decimal places for setting the limits";
            __UpperLimitDecimalsNumeric.Value = new decimal(new int[] { 3, 0, 0, 0 });
            // 
            // _UpperLimitNumericLabel
            // 
            _UpperLimitNumericLabel.Margin = new Padding(3, 1, 0, 2);
            _UpperLimitNumericLabel.Name = "_UpperLimitNumericLabel";
            _UpperLimitNumericLabel.Size = new System.Drawing.Size(38, 25);
            _UpperLimitNumericLabel.Text = "Value:";
            // 
            // _UpperLimitNumeric
            // 
            _UpperLimitNumeric.Name = "_UpperLimitNumeric";
            _UpperLimitNumeric.Size = new System.Drawing.Size(41, 25);
            _UpperLimitNumeric.Text = "0";
            _UpperLimitNumeric.ToolTipText = "Upper limit";
            _UpperLimitNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _UpperLimitBitPatternNumericButton
            // 
            __UpperLimitBitPatternNumericButton.CheckOnClick = true;
            __UpperLimitBitPatternNumericButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __UpperLimitBitPatternNumericButton.Image = (System.Drawing.Image)resources.GetObject("_UpperLimitBitPatternNumericButton.Image");
            __UpperLimitBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __UpperLimitBitPatternNumericButton.Name = "__UpperLimitBitPatternNumericButton";
            __UpperLimitBitPatternNumericButton.Size = new System.Drawing.Size(65, 25);
            __UpperLimitBitPatternNumericButton.Text = "Source: 0x";
            __UpperLimitBitPatternNumericButton.ToolTipText = "Toggle to switch between decimal and hex display";
            // 
            // _UpperLimitBitPatternNumeric
            // 
            _UpperLimitBitPatternNumeric.Name = "_UpperLimitBitPatternNumeric";
            _UpperLimitBitPatternNumeric.Size = new System.Drawing.Size(41, 25);
            _UpperLimitBitPatternNumeric.Text = "0";
            _UpperLimitBitPatternNumeric.ToolTipText = "Upper limit bit pattern";
            _UpperLimitBitPatternNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _LowerLimitToolStrip
            // 
            _LowerLimitToolStrip.BackColor = System.Drawing.Color.Transparent;
            _LowerLimitToolStrip.Dock = DockStyle.None;
            _LowerLimitToolStrip.GripMargin = new Padding(0);
            _LowerLimitToolStrip.Items.AddRange(new ToolStripItem[] { _LowerLimitLabel, _LowerLimitDecimalsNumericLabel, __LowerLimitDecimalsNumeric, _LowerLimitNumericLabel, _LowerLimitNumeric, __LowerLimitBitPatternNumericButton, _LowerLimitBitPatternNumeric });
            _LowerLimitToolStrip.Location = new System.Drawing.Point(0, 53);
            _LowerLimitToolStrip.Name = "_LowerLimitToolStrip";
            _LowerLimitToolStrip.Size = new System.Drawing.Size(341, 28);
            _LowerLimitToolStrip.TabIndex = 1;
            // 
            // _LowerLimitLabel
            // 
            _LowerLimitLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _LowerLimitLabel.Margin = new Padding(6, 1, 2, 2);
            _LowerLimitLabel.Name = "_LowerLimitLabel";
            _LowerLimitLabel.Size = new System.Drawing.Size(38, 25);
            _LowerLimitLabel.Text = "Lower";
            // 
            // _LowerLimitDecimalsNumericLabel
            // 
            _LowerLimitDecimalsNumericLabel.Name = "_LowerLimitDecimalsNumericLabel";
            _LowerLimitDecimalsNumericLabel.Size = new System.Drawing.Size(58, 25);
            _LowerLimitDecimalsNumericLabel.Text = "Decimals:";
            // 
            // _LowerLimitDecimalsNumeric
            // 
            __LowerLimitDecimalsNumeric.Name = "__LowerLimitDecimalsNumeric";
            __LowerLimitDecimalsNumeric.Size = new System.Drawing.Size(41, 25);
            __LowerLimitDecimalsNumeric.Text = "3";
            __LowerLimitDecimalsNumeric.ToolTipText = "Number of decimal places for setting the limits";
            __LowerLimitDecimalsNumeric.Value = new decimal(new int[] { 3, 0, 0, 0 });
            // 
            // _LowerLimitNumericLabel
            // 
            _LowerLimitNumericLabel.Margin = new Padding(3, 1, 0, 2);
            _LowerLimitNumericLabel.Name = "_LowerLimitNumericLabel";
            _LowerLimitNumericLabel.Size = new System.Drawing.Size(38, 25);
            _LowerLimitNumericLabel.Text = "Value:";
            // 
            // _LowerLimitNumeric
            // 
            _LowerLimitNumeric.Name = "_LowerLimitNumeric";
            _LowerLimitNumeric.Size = new System.Drawing.Size(41, 25);
            _LowerLimitNumeric.Text = "0";
            _LowerLimitNumeric.ToolTipText = "Start delay in seconds";
            _LowerLimitNumeric.Value = new decimal(new int[] { 20, 0, 0, 196608 });
            // 
            // _LowerLimitBitPatternNumericButton
            // 
            __LowerLimitBitPatternNumericButton.CheckOnClick = true;
            __LowerLimitBitPatternNumericButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __LowerLimitBitPatternNumericButton.Image = (System.Drawing.Image)resources.GetObject("_LowerLimitBitPatternNumericButton.Image");
            __LowerLimitBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __LowerLimitBitPatternNumericButton.Name = "__LowerLimitBitPatternNumericButton";
            __LowerLimitBitPatternNumericButton.Size = new System.Drawing.Size(65, 25);
            __LowerLimitBitPatternNumericButton.Text = "Source: 0x";
            __LowerLimitBitPatternNumericButton.ToolTipText = "Toggle to switch between decimal and hex display";
            // 
            // _LowerLimitBitPatternNumeric
            // 
            _LowerLimitBitPatternNumeric.Name = "_LowerLimitBitPatternNumeric";
            _LowerLimitBitPatternNumeric.Size = new System.Drawing.Size(41, 25);
            _LowerLimitBitPatternNumeric.Text = "1";
            _LowerLimitBitPatternNumeric.ToolTipText = "Lower limit bit pattern";
            _LowerLimitBitPatternNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // LimitView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_LimitToolStripPanel);
            Name = "LimitView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(389, 81);
            _LimitToolStripPanel.ResumeLayout(false);
            _LimitToolStripPanel.PerformLayout();
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            _UpperLimitToolStrip.ResumeLayout(false);
            _UpperLimitToolStrip.PerformLayout();
            _LowerLimitToolStrip.ResumeLayout(false);
            _LowerLimitToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStripPanel _LimitToolStripPanel;
        private ToolStrip _LowerLimitToolStrip;
        private ToolStripLabel _LowerLimitLabel;
        private ToolStripLabel _LowerLimitNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _LowerLimitNumeric;
        private ToolStripLabel _LowerLimitDecimalsNumericLabel;
        private ToolStrip _UpperLimitToolStrip;
        private ToolStripLabel _UpperLimitLabel;
        private Core.Controls.ToolStripNumericUpDown __UpperLimitDecimalsNumeric;

        private Core.Controls.ToolStripNumericUpDown _UpperLimitDecimalsNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __UpperLimitDecimalsNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__UpperLimitDecimalsNumeric != null)
                {
                    __UpperLimitDecimalsNumeric.ValueChanged -= UpperLimitDecimalsNumeric_ValueChanged;
                }

                __UpperLimitDecimalsNumeric = value;
                if (__UpperLimitDecimalsNumeric != null)
                {
                    __UpperLimitDecimalsNumeric.ValueChanged += UpperLimitDecimalsNumeric_ValueChanged;
                }
            }
        }

        private ToolStripLabel _UpperLimitNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _UpperLimitNumeric;
        private ToolStripButton __UpperLimitBitPatternNumericButton;

        private ToolStripButton _UpperLimitBitPatternNumericButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __UpperLimitBitPatternNumericButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__UpperLimitBitPatternNumericButton != null)
                {
                    __UpperLimitBitPatternNumericButton.CheckStateChanged -= UpperLimitBitPatternNumericButton_CheckStateChanged;
                }

                __UpperLimitBitPatternNumericButton = value;
                if (__UpperLimitBitPatternNumericButton != null)
                {
                    __UpperLimitBitPatternNumericButton.CheckStateChanged += UpperLimitBitPatternNumericButton_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _UpperLimitBitPatternNumeric;
        private ToolStripButton __LowerLimitBitPatternNumericButton;

        private ToolStripButton _LowerLimitBitPatternNumericButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LowerLimitBitPatternNumericButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LowerLimitBitPatternNumericButton != null)
                {
                    __LowerLimitBitPatternNumericButton.CheckStateChanged -= LowerBitPatternNumericButton_CheckStateChanged;
                }

                __LowerLimitBitPatternNumericButton = value;
                if (__LowerLimitBitPatternNumericButton != null)
                {
                    __LowerLimitBitPatternNumericButton.CheckStateChanged += LowerBitPatternNumericButton_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _LowerLimitBitPatternNumeric;
        private ToolStrip _SubsystemToolStrip;
        private ToolStripButton __LimitEnabledToggleButton;

        private ToolStripButton _LimitEnabledToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LimitEnabledToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LimitEnabledToggleButton != null)
                {
                    __LimitEnabledToggleButton.CheckStateChanged -= LimitEnabledToggleButton_CheckStateChanged;
                }

                __LimitEnabledToggleButton = value;
                if (__LimitEnabledToggleButton != null)
                {
                    __LimitEnabledToggleButton.CheckStateChanged += LimitEnabledToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStripButton __LimitFailedButton;

        private ToolStripButton _LimitFailedButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LimitFailedButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LimitFailedButton != null)
                {
                    __LimitFailedButton.CheckStateChanged -= LimitFailedToggleButton_CheckStateChanged;
                }

                __LimitFailedButton = value;
                if (__LimitFailedButton != null)
                {
                    __LimitFailedButton.CheckStateChanged += LimitFailedToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStripLabel _UpperLimitDecimalsNumericLabel;
        private Core.Controls.ToolStripNumericUpDown __LowerLimitDecimalsNumeric;

        private Core.Controls.ToolStripNumericUpDown _LowerLimitDecimalsNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LowerLimitDecimalsNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LowerLimitDecimalsNumeric != null)
                {
                    __LowerLimitDecimalsNumeric.ValueChanged -= LowerLimitDecimalsNumeric_ValueChanged;
                }

                __LowerLimitDecimalsNumeric = value;
                if (__LowerLimitDecimalsNumeric != null)
                {
                    __LowerLimitDecimalsNumeric.ValueChanged += LowerLimitDecimalsNumeric_ValueChanged;
                }
            }
        }

        private ToolStripButton __AutoClearToggleButton;

        private ToolStripButton _AutoClearToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AutoClearToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AutoClearToggleButton != null)
                {
                    __AutoClearToggleButton.CheckStateChanged -= AutoClearButton_CheckStateChanged;
                }

                __AutoClearToggleButton = value;
                if (__AutoClearToggleButton != null)
                {
                    __AutoClearToggleButton.CheckStateChanged += AutoClearButton_CheckStateChanged;
                }
            }
        }

        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __PerformLimitStripMenuItem;

        private ToolStripMenuItem _PerformLimitStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PerformLimitStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PerformLimitStripMenuItem != null)
                {
                    __PerformLimitStripMenuItem.Click -= PerformLimitStripMenuItem_Click;
                }

                __PerformLimitStripMenuItem = value;
                if (__PerformLimitStripMenuItem != null)
                {
                    __PerformLimitStripMenuItem.Click += PerformLimitStripMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadLimitTestMenuItem;

        private ToolStripMenuItem _ReadLimitTestMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadLimitTestMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadLimitTestMenuItem != null)
                {
                    __ReadLimitTestMenuItem.Click -= ReadLimitTestMenuItem_Click;
                }

                __ReadLimitTestMenuItem = value;
                if (__ReadLimitTestMenuItem != null)
                {
                    __ReadLimitTestMenuItem.Click += ReadLimitTestMenuItem_Click;
                }
            }
        }
    }
}