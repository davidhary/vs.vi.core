using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> An Route Subsystem channel view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ChannelView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public ChannelView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._SubsystemName = "Route";
            this._ChannelNumberNumeric.NumericUpDownControl.Minimum = 1m;
            this._ChannelNumberNumeric.NumericUpDownControl.Maximum = 10m;
            this._ChannelNumberNumeric.NumericUpDownControl.Value = 1m;
            this._ChannelNumberNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this.__CloseChannelMenuItem.Name = "_CloseChannelMenuItem";
            this.__OpenChannelMenuItem.Name = "_OpenChannelMenuItem";
            this.__OpenAllMenuItem.Name = "_OpenAllMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
            this.__CloseChannelButton.Name = "_CloseChannelButton";
            this.__OpenChannelButton.Name = "_OpenChannelButton";
            this.__OpenChannelsButton.Name = "_OpenChannelsButton";
        }

        /// <summary> Creates a new ChannelView. </summary>
        /// <returns> A ChannelView. </returns>
        public static ChannelView Create()
        {
            ChannelView view = null;
            try
            {
                view = new ChannelView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Name of the subsystem. </summary>
        private string _SubsystemName;

        /// <summary> Gets or sets the name of the subsystem. </summary>
        /// <value> The name of the subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string SubsystemName
        {
            get => this._SubsystemName;

            set {
                if ( !string.Equals( value, this.SubsystemName ) )
                {
                    this._SubsystemName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The closed channels. </summary>
        private string _ClosedChannels;

        /// <summary> Gets or sets the closed channels. </summary>
        /// <value> The closed channels. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string ClosedChannels
        {
            get => this._ClosedChannels;

            set {
                if ( !string.Equals( value, this.ClosedChannels ) )
                {
                    this._ClosedChannels = value;
                    this._ClosedChannelLabel.Text = $"Closed: {(string.IsNullOrEmpty( value ) ? "?" : value)}";
                }
            }
        }

        /// <summary> Closes the channel. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void CloseChannel()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying {this.SubsystemName} settings";
                _ = this.PublishInfo( $"{activity};. " );
                this.RouteSubsystem.StartElapsedStopwatch();
                _ = this.RouteSubsystem.ApplyClosedChannel( $"(@{this._ChannelNumberNumeric.Value})", TimeSpan.FromSeconds( 1d ) );
                this.RouteSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Opens the channel. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void OpenChannel()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying {this.SubsystemName} settings";
                _ = this.PublishInfo( $"{activity};. " );
                this.RouteSubsystem.StartElapsedStopwatch();
                _ = this.RouteSubsystem.ApplyOpenChannel( $"(@{this._ChannelNumberNumeric.Value})", TimeSpan.FromSeconds( 1d ) );
                this.RouteSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Opens the channels. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void OpenChannels()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying {this.SubsystemName} settings";
                _ = this.PublishInfo( $"{activity};. " );
                this.RouteSubsystem.StartElapsedStopwatch();
                _ = this.RouteSubsystem.WriteOpenAll( TimeSpan.FromSeconds( 1d ) );
                this.RouteSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads the settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} reading system subsystem settings";
                _ = this.PublishInfo( $"{activity};. " );
                ReadSettings( this.SystemSubsystem );
                this.ApplyPropertyChanged( this.SystemSubsystem );
                activity = $"{this.Device.ResourceNameCaption} reading {this.SubsystemName} settings";
                _ = this.PublishInfo( $"{activity};. " );
                ReadSettings( this.RouteSubsystem );
                this.ApplyPropertyChanged( this.RouteSubsystem );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( ChannelView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " ROUTE SUBSYSTEM "

        /// <summary> Gets or sets the Route subsystem. </summary>
        /// <value> The Route subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public RouteSubsystemBase RouteSubsystem { get; private set; }

        /// <summary> Bind Route subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( RouteSubsystemBase subsystem )
        {
            if ( this.RouteSubsystem is object )
            {
                this.BindSubsystem( false, this.RouteSubsystem );
                this.RouteSubsystem = null;
            }

            this.RouteSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.RouteSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, RouteSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.RouteSubsystemBasePropertyChanged;
                // must not read setting when biding because the instrument may be locked or in a trigger mode
                // The bound values should be sent when binding or when applying propert change.
                // ReadSettings( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.RouteSubsystemBasePropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( RouteSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( RouteSubsystemBase.ClosedChannel ) );
        }

        /// <summary> Reads the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( RouteSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryClosedChannel();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Handles the ROUTE subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( RouteSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( RouteSubsystemBase.ClosedChannel ):
                    {
                        this.ClosedChannels = subsystem.ClosedChannel;
                        break;
                    }
            }
        }

        /// <summary> ROUTE subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RouteSubsystemBasePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.RouteSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.RouteSubsystemBasePropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.RouteSubsystemBasePropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as RouteSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SYSTEM SUBSYSTEM "

        /// <summary> Gets or sets the System subsystem. </summary>
        /// <value> The System subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SystemSubsystemBase SystemSubsystem { get; private set; }

        /// <summary> Bind System subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( SystemSubsystemBase subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                this.BindSubsystem( false, this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.SystemSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SystemSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SystemSubsystemPropertyChanged;
                // must not read setting when biding because the instrument may be locked or in a trigger mode
                // The bound values should be sent when binding or when applying propert change.
                // ReadSettings( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.SystemSubsystemPropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( SystemSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( SystemSubsystemBase.InstalledScanCards ) );
        }

        /// <summary> Reads the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( SystemSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryInstalledScanCards();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Handles the System subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SystemSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SystemSubsystemBase.InstalledScanCards ):
                    {
                        // this control is disabled if the system supports scan cards and none was found
                        this.Enabled = subsystem.InstalledScanCards.Any() || !subsystem.SupportsScanCardOption;
                        break;
                    }
            }
        }

        /// <summary> System subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SystemSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SystemSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SystemSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.SystemSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SystemSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Opens channels button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenChannelsButton_Click( object sender, EventArgs e )
        {
            this.OpenChannels();
        }

        /// <summary> Opens channel button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenChannelButton_Click( object sender, EventArgs e )
        {
            this.OpenChannel();
        }

        /// <summary> Closes channel button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CloseChannelButton_Click( object sender, EventArgs e )
        {
            this.CloseChannel();
        }

        /// <summary> Closes channel menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CloseChannelMenuItem_Click( object sender, EventArgs e )
        {
            this.CloseChannel();
        }

        /// <summary> Opens channel menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenChannelMenuItem_Click( object sender, EventArgs e )
        {
            this.OpenChannel();
        }

        /// <summary> Opens all menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenAllMenuItem_Click( object sender, EventArgs e )
        {
            this.OpenChannels();
        }

        /// <summary> Reads settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsMenuItem_Click( object sender, EventArgs e )
        {
            this.ReadSettings();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
