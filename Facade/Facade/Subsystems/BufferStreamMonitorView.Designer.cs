﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class BufferStreamMonitorView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(BufferStreamMonitorView));
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            _DisplayMenuItem = new ToolStripMenuItem();
            _ClearBufferDisplayMenuItem = new ToolStripMenuItem();
            _DisplayBufferMenuItem = new ToolStripMenuItem();
            _TriggerMenuItem = new ToolStripMenuItem();
            __InitiateTriggerPlanMenuItem = new ToolStripMenuItem();
            __InitiateTriggerPlanMenuItem.Click += new EventHandler(InitiateTriggerPlanMenuItem_Click);
            __AbortStartTriggerPlanMenuItem = new ToolStripMenuItem();
            __AbortStartTriggerPlanMenuItem.Click += new EventHandler(AbortStartTriggerPlanMenuItem_Click);
            __MonitorActiveTriggerPlanMenuItem = new ToolStripMenuItem();
            __MonitorActiveTriggerPlanMenuItem.Click += new EventHandler(MonitorActiveTriggerPlanMenuItem_Click);
            __InitMonitorReadRepeatMenuItem = new ToolStripMenuItem();
            __InitMonitorReadRepeatMenuItem.Click += new EventHandler(InitMonitorReadRepeatMenuItem_Click);
            _RepeatMenuItem = new ToolStripMenuItem();
            _StreamMenuItem = new ToolStripMenuItem();
            __ToggleBufferStreamMenuItem = new ToolStripMenuItem();
            __ToggleBufferStreamMenuItem.CheckStateChanged += new EventHandler(ToggleBufferStreamMenuItem_CheckStateChanged);
            __ToggleBufferStreamMenuItem.Click += new EventHandler(ToggleBufferStreamMenuItem_Click);
            _AbortButton = new ToolStripButton();
            _BufferCountLabel = new Core.Controls.ToolStripLabel();
            _LastPointNumberLabel = new Core.Controls.ToolStripLabel();
            _FirstPointNumberLabel = new Core.Controls.ToolStripLabel();
            _BufferSizeNumericLabel = new ToolStripLabel();
            __BufferSizeNumeric = new Core.Controls.ToolStripNumericUpDown();
            __BufferSizeNumeric.Validating += new System.ComponentModel.CancelEventHandler(BufferSizeNumeric_Validating);
            _TriggerStateLabel = new Core.Controls.ToolStripLabel();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _AbortButton, _BufferCountLabel, _LastPointNumberLabel, _FirstPointNumberLabel, _BufferSizeNumericLabel, __BufferSizeNumeric, _TriggerStateLabel });
            _SubsystemToolStrip.Location = new System.Drawing.Point(1, 1);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(356, 28);
            _SubsystemToolStrip.TabIndex = 11;
            _SubsystemToolStrip.Text = "Buffer";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _DisplayMenuItem, _TriggerMenuItem, _StreamMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(55, 25);
            _SubsystemSplitButton.Text = "Buffer";
            // 
            // _DisplayMenuItem
            // 
            _DisplayMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _ClearBufferDisplayMenuItem, _DisplayBufferMenuItem });
            _DisplayMenuItem.Name = "_DisplayMenuItem";
            _DisplayMenuItem.Size = new System.Drawing.Size(180, 22);
            _DisplayMenuItem.Text = "Display";
            // 
            // _ClearBufferDisplayMenuItem
            // 
            _ClearBufferDisplayMenuItem.Name = "_ClearBufferDisplayMenuItem";
            _ClearBufferDisplayMenuItem.Size = new System.Drawing.Size(180, 22);
            _ClearBufferDisplayMenuItem.Text = "Clear Buffer Display";
            _ClearBufferDisplayMenuItem.ToolTipText = "Clears the display";
            // 
            // _DisplayBufferMenuItem
            // 
            _DisplayBufferMenuItem.Name = "_DisplayBufferMenuItem";
            _DisplayBufferMenuItem.Size = new System.Drawing.Size(180, 22);
            _DisplayBufferMenuItem.Text = "Display Buffer";
            _DisplayBufferMenuItem.ToolTipText = "Displays the buffer";
            // 
            // _TriggerMenuItem
            // 
            _TriggerMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __InitiateTriggerPlanMenuItem, __AbortStartTriggerPlanMenuItem, __MonitorActiveTriggerPlanMenuItem, __InitMonitorReadRepeatMenuItem, _RepeatMenuItem });
            _TriggerMenuItem.Name = "_TriggerMenuItem";
            _TriggerMenuItem.Size = new System.Drawing.Size(180, 22);
            _TriggerMenuItem.Text = "Trigger Monitor";
            // 
            // _InitiateTriggerPlanMenuItem
            // 
            __InitiateTriggerPlanMenuItem.Name = "__InitiateTriggerPlanMenuItem";
            __InitiateTriggerPlanMenuItem.Size = new System.Drawing.Size(219, 22);
            __InitiateTriggerPlanMenuItem.Text = "Initiate";
            __InitiateTriggerPlanMenuItem.ToolTipText = "Sends the Initiate command to the instrument";
            // 
            // _AbortStartTriggerPlanMenuItem
            // 
            __AbortStartTriggerPlanMenuItem.Name = "__AbortStartTriggerPlanMenuItem";
            __AbortStartTriggerPlanMenuItem.Size = new System.Drawing.Size(219, 22);
            __AbortStartTriggerPlanMenuItem.Text = "Abort, Clear, Initiate";
            __AbortStartTriggerPlanMenuItem.ToolTipText = "Aborts, clears buffer and starts the trigger plan";
            // 
            // _MonitorActiveTriggerPlanMenuItem
            // 
            __MonitorActiveTriggerPlanMenuItem.Name = "__MonitorActiveTriggerPlanMenuItem";
            __MonitorActiveTriggerPlanMenuItem.Size = new System.Drawing.Size(219, 22);
            __MonitorActiveTriggerPlanMenuItem.Text = "Monitor Active Trigger State";
            __MonitorActiveTriggerPlanMenuItem.ToolTipText = "Monitors active trigger state. Exits if trigger plan inactive";
            // 
            // _InitMonitorReadRepeatMenuItem
            // 
            __InitMonitorReadRepeatMenuItem.Name = "__InitMonitorReadRepeatMenuItem";
            __InitMonitorReadRepeatMenuItem.Size = new System.Drawing.Size(219, 22);
            __InitMonitorReadRepeatMenuItem.Text = "Init, Monitor, Read, Repeat";
            __InitMonitorReadRepeatMenuItem.ToolTipText = "Initiates a trigger plan, monitors it, reads and displays buffer and repeats if a" + "uto repeat is checked";
            // 
            // _RepeatMenuItem
            // 
            _RepeatMenuItem.CheckOnClick = true;
            _RepeatMenuItem.Name = "_RepeatMenuItem";
            _RepeatMenuItem.Size = new System.Drawing.Size(219, 22);
            _RepeatMenuItem.Text = "Repeat";
            _RepeatMenuItem.ToolTipText = "Repeat initiating the trigger plan";
            // 
            // _StreamMenuItem
            // 
            _StreamMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __ToggleBufferStreamMenuItem });
            _StreamMenuItem.Name = "_StreamMenuItem";
            _StreamMenuItem.Size = new System.Drawing.Size(180, 22);
            _StreamMenuItem.Text = "Stream";
            // 
            // _ToggleBufferStreamMenuItem
            // 
            __ToggleBufferStreamMenuItem.CheckOnClick = false;
            __ToggleBufferStreamMenuItem.Name = "__ToggleBufferStreamMenuItem";
            __ToggleBufferStreamMenuItem.Size = new System.Drawing.Size(174, 22);
            __ToggleBufferStreamMenuItem.Text = "Start Buffer Stream";
            __ToggleBufferStreamMenuItem.ToolTipText = "Toggles streaming values";
            // 
            // _AbortButton
            // 
            _AbortButton.Alignment = ToolStripItemAlignment.Right;
            _AbortButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _AbortButton.Image = (System.Drawing.Image)resources.GetObject("_AbortButton.Image");
            _AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _AbortButton.Name = "_AbortButton";
            _AbortButton.Size = new System.Drawing.Size(41, 25);
            _AbortButton.Text = "Abort";
            _AbortButton.ToolTipText = "Aborts triggering";
            // 
            // _BufferCountLabel
            // 
            _BufferCountLabel.Alignment = ToolStripItemAlignment.Right;
            _BufferCountLabel.Name = "_BufferCountLabel";
            _BufferCountLabel.Size = new System.Drawing.Size(13, 25);
            _BufferCountLabel.Text = "0";
            _BufferCountLabel.ToolTipText = "Buffer count";
            // 
            // _LastPointNumberLabel
            // 
            _LastPointNumberLabel.Alignment = ToolStripItemAlignment.Right;
            _LastPointNumberLabel.Name = "_LastPointNumberLabel";
            _LastPointNumberLabel.Size = new System.Drawing.Size(13, 25);
            _LastPointNumberLabel.Text = "2";
            _LastPointNumberLabel.ToolTipText = "Number of last buffer reading";
            // 
            // _FirstPointNumberLabel
            // 
            _FirstPointNumberLabel.Alignment = ToolStripItemAlignment.Right;
            _FirstPointNumberLabel.Name = "_FirstPointNumberLabel";
            _FirstPointNumberLabel.Size = new System.Drawing.Size(13, 25);
            _FirstPointNumberLabel.Text = "1";
            _FirstPointNumberLabel.ToolTipText = "Number of the first buffer reading";
            // 
            // _BufferSizeNumericLabel
            // 
            _BufferSizeNumericLabel.Name = "_BufferSizeNumericLabel";
            _BufferSizeNumericLabel.Size = new System.Drawing.Size(30, 25);
            _BufferSizeNumericLabel.Text = "Size:";
            // 
            // _BufferSizeNumeric
            // 
            __BufferSizeNumeric.AutoSize = false;
            __BufferSizeNumeric.Name = "__BufferSizeNumeric";
            __BufferSizeNumeric.Size = new System.Drawing.Size(71, 25);
            __BufferSizeNumeric.Text = "0";
            __BufferSizeNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _TriggerStateLabel
            // 
            _TriggerStateLabel.Name = "_TriggerStateLabel";
            _TriggerStateLabel.Size = new System.Drawing.Size(26, 25);
            _TriggerStateLabel.Text = "idle";
            _TriggerStateLabel.ToolTipText = "Trigger state";
            // 
            // BufferStreamMonitorView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_SubsystemToolStrip);
            Name = "BufferStreamMonitorView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(358, 27);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _SubsystemToolStrip;
        private Core.Controls.ToolStripLabel _BufferCountLabel;
        private Core.Controls.ToolStripLabel _LastPointNumberLabel;
        private Core.Controls.ToolStripLabel _FirstPointNumberLabel;
        private Core.Controls.ToolStripNumericUpDown __BufferSizeNumeric;

        private Core.Controls.ToolStripNumericUpDown _BufferSizeNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BufferSizeNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BufferSizeNumeric != null)
                {
                    __BufferSizeNumeric.Validating -= BufferSizeNumeric_Validating;
                }

                __BufferSizeNumeric = value;
                if (__BufferSizeNumeric != null)
                {
                    __BufferSizeNumeric.Validating += BufferSizeNumeric_Validating;
                }
            }
        }

        private ToolStripMenuItem __InitiateTriggerPlanMenuItem;

        private ToolStripMenuItem _InitiateTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitiateTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitiateTriggerPlanMenuItem != null)
                {
                    __InitiateTriggerPlanMenuItem.Click -= InitiateTriggerPlanMenuItem_Click;
                }

                __InitiateTriggerPlanMenuItem = value;
                if (__InitiateTriggerPlanMenuItem != null)
                {
                    __InitiateTriggerPlanMenuItem.Click += InitiateTriggerPlanMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __AbortStartTriggerPlanMenuItem;

        private ToolStripMenuItem _AbortStartTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortStartTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortStartTriggerPlanMenuItem != null)
                {
                    __AbortStartTriggerPlanMenuItem.Click -= AbortStartTriggerPlanMenuItem_Click;
                }

                __AbortStartTriggerPlanMenuItem = value;
                if (__AbortStartTriggerPlanMenuItem != null)
                {
                    __AbortStartTriggerPlanMenuItem.Click += AbortStartTriggerPlanMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __MonitorActiveTriggerPlanMenuItem;

        private ToolStripMenuItem _MonitorActiveTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MonitorActiveTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MonitorActiveTriggerPlanMenuItem != null)
                {
                    __MonitorActiveTriggerPlanMenuItem.Click -= MonitorActiveTriggerPlanMenuItem_Click;
                }

                __MonitorActiveTriggerPlanMenuItem = value;
                if (__MonitorActiveTriggerPlanMenuItem != null)
                {
                    __MonitorActiveTriggerPlanMenuItem.Click += MonitorActiveTriggerPlanMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __InitMonitorReadRepeatMenuItem;

        private ToolStripMenuItem _InitMonitorReadRepeatMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitMonitorReadRepeatMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitMonitorReadRepeatMenuItem != null)
                {
                    __InitMonitorReadRepeatMenuItem.Click -= InitMonitorReadRepeatMenuItem_Click;
                }

                __InitMonitorReadRepeatMenuItem = value;
                if (__InitMonitorReadRepeatMenuItem != null)
                {
                    __InitMonitorReadRepeatMenuItem.Click += InitMonitorReadRepeatMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem _RepeatMenuItem;
        private ToolStripMenuItem __ToggleBufferStreamMenuItem;

        private ToolStripMenuItem _ToggleBufferStreamMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToggleBufferStreamMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToggleBufferStreamMenuItem != null)
                {
                    __ToggleBufferStreamMenuItem.CheckStateChanged -= ToggleBufferStreamMenuItem_CheckStateChanged;
                    __ToggleBufferStreamMenuItem.Click -= ToggleBufferStreamMenuItem_Click;
                }

                __ToggleBufferStreamMenuItem = value;
                if (__ToggleBufferStreamMenuItem != null)
                {
                    __ToggleBufferStreamMenuItem.CheckStateChanged += ToggleBufferStreamMenuItem_CheckStateChanged;
                    __ToggleBufferStreamMenuItem.Click += ToggleBufferStreamMenuItem_Click;
                }
            }
        }

        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripLabel _BufferSizeNumericLabel;
        private ToolStripButton _AbortButton;
        private Core.Controls.ToolStripLabel _TriggerStateLabel;
        private ToolStripMenuItem _TriggerMenuItem;
        private ToolStripMenuItem _DisplayMenuItem;
        private ToolStripMenuItem _ClearBufferDisplayMenuItem;
        private ToolStripMenuItem _DisplayBufferMenuItem;
        private ToolStripMenuItem _StreamMenuItem;
    }
}