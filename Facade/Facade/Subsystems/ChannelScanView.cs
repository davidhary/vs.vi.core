using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A Channel Scan subsystem user interface. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ChannelScanView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public ChannelScanView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
        }

        /// <summary> Creates a new <see cref="ChannelScanView"/> </summary>
        /// <returns> A <see cref="ChannelScanView"/>. </returns>
        public static ChannelScanView Create()
        {
            ChannelScanView view = null;
            try
            {
                view = new ChannelScanView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Gets or sets the closed channels. </summary>
        /// <value> The closed channels. </value>
        public string ClosedChannels
        {
            get => this._ChannelView.ClosedChannels;

            set => this._ChannelView.ClosedChannels = value;
        }

        /// <summary> Gets or sets the scan list function. </summary>
        /// <value> The scan list function. </value>
        public string ScanListFunction
        {
            get => this.ScanView.ScanListFunction;

            set => this.ScanView.ScanListFunction = value;
        }

        /// <summary> Gets or sets the scan list of closed channels. </summary>
        /// <value> The closed channels. </value>
        public string InternalScanList
        {
            get => this.ScanView.InternalScanList;

            set => this.ScanView.InternalScanList = value;
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( ChannelScanView ).SplitWords()}" );
            }

            this._ChannelView.AssignDevice( value );
            this._ChannelView.SubsystemName = "Route";
            this.ScanView.AssignDevice( value );
            this.ScanView.SubsystemName = "Route";
        }

        /// <summary> Gets the system subsystem. </summary>
        /// <value> The system subsystem. </value>
        private SystemSubsystemBase SystemSubsystem { get; set; }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected virtual void BindSubsystem( SystemSubsystemBase subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            this._ChannelView.BindSubsystem( subsystem );
            this.ScanView.BindSubsystem( subsystem );
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected virtual void BindSubsystem( RouteSubsystemBase subsystem )
        {
            if ( subsystem is null )
            {
                this._ChannelView.BindSubsystem( subsystem );
                this.ScanView.BindSubsystem( subsystem );
            }
            else if ( this.SystemSubsystem.SupportsScanCardOption && this.SystemSubsystem.InstalledScanCards.Any() )
            {
                this._ChannelView.BindSubsystem( subsystem );
                this.ScanView.BindSubsystem( subsystem );
            }
            else if ( !this.SystemSubsystem.SupportsScanCardOption )
            {
                this._InfoTextBox.Text = "No scan card";
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected virtual void BindSubsystem( SenseSubsystemBase subsystem )
        {
            if ( subsystem is null )
            {
                this.ScanView.BindSubsystem( subsystem );
            }
            else if ( this.SystemSubsystem.SupportsScanCardOption && this.SystemSubsystem.InstalledScanCards.Any() )
            {
                this.ScanView.BindSubsystem( subsystem );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Reads settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsMenuItem_Click( object sender, EventArgs e )
        {
            this._ChannelView.ReadSettings();
            this.ScanView.ReadSettings();
        }

        #endregion

        #region " CONTROLS "

        /// <summary> Gets the scan view. </summary>
        /// <value> The scan view. </value>
        protected ScanView ScanView { get; private set; }


        #endregion

        #region " TALKER "

        /// <summary> Assigns talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( Core.ITraceMessageTalker talker )
        {
            this._ChannelView.AssignTalker( talker );
            this.ScanView.AssignTalker( talker );
            // assigned last as this identifies all talkers.
            base.AssignTalker( talker );
        }

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
