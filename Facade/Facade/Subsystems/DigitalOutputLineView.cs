using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{

    /// <summary> An Digital Output Line View. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class DigitalOutputLineView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public DigitalOutputLineView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
            this.__ReadButton.Name = "_ReadButton";
            this.__WriteButton.Name = "_WriteButton";
            this.__PulseButton.Name = "_PulseButton";
            this.__ToggleButton.Name = "_ToggleButton";
        }

        /// <summary> Creates a new DigitalOutputLineView. </summary>
        /// <returns> A DigitalOutputLineView. </returns>
        public static DigitalOutputLineView Create()
        {
            DigitalOutputLineView view = null;
            try
            {
                view = new DigitalOutputLineView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        private string _LineIdentity;

        /// <summary> Gets or sets the line identity. </summary>
        /// <value> The line identity. </value>
        public string LineIdentity
        {
            get => this._LineIdentity;

            set {
                if ( !string.Equals( value, this.LineIdentity ) )
                {
                    this._LineIdentity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the name of the line. </summary>
        /// <value> The name of the line. </value>
        public string LineName
        {
            get => this._SubsystemSplitButton.Text;

            set {
                if ( !string.Equals( value, this.LineName ) )
                {
                    this._SubsystemSplitButton.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the line number. </summary>
        /// <value> The line number. </value>
        public int LineNumber
        {
            get => this._LineNumberBox.ValueAsInt32;

            set {
                if ( value != this.LineNumber )
                {
                    this._LineNumberBox.ValueAsInt32 = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the line level. </summary>
        /// <value> The line level. </value>
        public int LineLevel
        {
            get => this._LineLevelNumberBox.ValueAsInt32;

            set {
                if ( value != this.LineLevel )
                {
                    this._LineLevelNumberBox.ValueAsInt32 = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the DigitalOutput source. </summary>
        /// <value> The DigitalOutput source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DigitalActiveLevels ActiveLevel
        {
            get => ( DigitalActiveLevels ) Conversions.ToInteger( this._ActiveLevelComboBox.ComboBox.SelectedValue );

            set {
                if ( value != this.ActiveLevel )
                {
                    this._ActiveLevelComboBox.ComboBox.SelectedItem = value.ValueNamePair();
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the width of the pulse. </summary>
        /// <value> The width of the pulse. </value>
        public TimeSpan PulseWidth
        {
            get => TimeSpan.FromMilliseconds( this._PulseWidthNumberBox.ValueAsDouble );

            set {
                if ( value != this.PulseWidth )
                {
                    this._PulseWidthNumberBox.ValueAsDouble = value.TotalMilliseconds;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Toggle line level. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        public void ToggleLineLevel()
        {
            this.ReadActiveLevel();
            this.WriteLineLevel( this.LineLevel == 0 ? 1 : 0 );
        }

        /// <summary> Reads line level. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void ReadLineLevel()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} reading {this._SubsystemSplitButton.Text}{this.LineNumber} line level";
                _ = this.PublishInfo( $"{activity};. " );
                this.DigitalOutputSubsystem.StartElapsedStopwatch();
                _ = this.DigitalOutputSubsystem.QueryLineLevel( this.LineNumber );
                this.DigitalOutputSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Writes the line level. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void WriteLineLevel( int value )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} write {this._SubsystemSplitButton.Text}{this.LineNumber}  line level";
                _ = this.PublishInfo( $"{activity};. " );
                this.DigitalOutputSubsystem.StartElapsedStopwatch();
                _ = this.DigitalOutputSubsystem.ApplyLineLevel( this.LineNumber, value );
                this.DigitalOutputSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Pulse line level. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="duration"> The duration. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void PulseLineLevel( int value, TimeSpan duration )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} output {this._SubsystemSplitButton.Text}{this.LineNumber} pulse";
                _ = this.PublishInfo( $"{activity};. " );
                this.DigitalOutputSubsystem.StartElapsedStopwatch();
                this.DigitalOutputSubsystem.Pulse( this.LineNumber, value, duration );
                this.DigitalOutputSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads active level. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void ReadActiveLevel()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} read {this._SubsystemSplitButton.Text}{this.LineNumber}  active level";
                _ = this.PublishInfo( $"{activity};. " );
                this.DigitalOutputSubsystem.StartElapsedStopwatch();
                _ = this.DigitalOutputSubsystem.QueryDigitalActiveLevel( this.LineNumber );
                this.DigitalOutputSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Writes the active level. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void WriteActiveLevel()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} write {this._SubsystemSplitButton.Text}{this.LineNumber}  active level";
                _ = this.PublishInfo( $"{activity};. " );
                this.DigitalOutputSubsystem.StartElapsedStopwatch();
                _ = this.DigitalOutputSubsystem.ApplyDigitalActiveLevel( this.LineNumber, this.ActiveLevel );
                this.DigitalOutputSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the settings onto the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying {this._SubsystemSplitButton.Text}{this.LineNumber}  settings";
                _ = this.PublishInfo( $"{activity};. " );
                this.DigitalOutputSubsystem.StartElapsedStopwatch();
                _ = this.DigitalOutputSubsystem.ApplyDigitalActiveLevel( this.LineNumber, this.ActiveLevel );
                _ = this.DigitalOutputSubsystem.ApplyLineLevel( this.LineNumber, this.LineLevel );
                this.DigitalOutputSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads the settings from the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} reading  {this._SubsystemSplitButton.Text}{this.LineNumber}  settings";
                _ = this.PublishInfo( $"{activity};. " );
                _ = this.DigitalOutputSubsystem.QueryDigitalActiveLevel( this.LineNumber );
                _ = this.DigitalOutputSubsystem.QueryLineLevel( this.LineNumber );
                this.ApplyPropertyChanged( this.DigitalOutputSubsystem );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( DigitalOutputLineView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DIGITAL OUTPUT SUBSYSTEM "

        /// <summary> Gets or sets the DigitalOutput subsystem. </summary>
        /// <value> The DigitalOutput subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DigitalOutputSubsystemBase DigitalOutputSubsystem { get; private set; }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-11-13. </remarks>
        /// <param name="subsystem">  The subsystem. </param>
        /// <param name="lineNumber"> The line number. </param>
        public void BindSubsystem( DigitalOutputSubsystemBase subsystem, int lineNumber )
        {
            if ( this.DigitalOutputSubsystem is object )
            {
                this.BindSubsystem( false, this.DigitalOutputSubsystem );
                this.DigitalOutputSubsystem = null;
            }

            this.LineNumber = lineNumber;
            this.DigitalOutputSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.DigitalOutputSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, DigitalOutputSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.DigitalOutputSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( DigitalOutputSubsystemBase.SupportedDigitalActiveLevels ) );
                // must not read setting when biding because the instrument may be locked or in a trigger mode
                // The bound values should be sent when binding or when applying propert change.
                // ReadSettings( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.DigitalOutputSubsystemPropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( DigitalOutputSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( DigitalOutputSubsystemBase.DigitalOutputLines ) );
        }

        /// <summary> Reads the settings from the instrument. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( DigitalOutputSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            subsystem.QueryDigitalOutputLines();
            subsystem.StopElapsedStopwatch();
        }

        private const int _NotSpecified = -2;
        private const int _NotSet = -1;

        /// <summary> Handle the Calculate subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( DigitalOutputSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( DigitalOutputSubsystemBase.DigitalOutputLines ):
                    {
                        if ( subsystem.DigitalOutputLines.Contains( this.LineNumber ) )
                        {
                            this.LineLevel = subsystem.DigitalOutputLines[this.LineNumber].LineLevel.GetValueOrDefault( _NotSet );
                            this.ActiveLevel = subsystem.DigitalOutputLines[this.LineNumber].ActiveLevel;
                        }
                        else
                        {
                            this.LineLevel = _NotSpecified;
                        }

                        break;
                    }

                case nameof( DigitalOutputSubsystemBase.SupportedDigitalActiveLevels ):
                    {
                        this.InitializingComponents = true;
                        this._ActiveLevelComboBox.ComboBox.ListDigitalActiveLevels( subsystem.SupportedDigitalActiveLevels );
                        this.InitializingComponents = false;
                        if ( subsystem.DigitalOutputLines.Contains( this.LineNumber ) )
                        {
                            this.ActiveLevel = subsystem.DigitalOutputLines[this.LineNumber].ActiveLevel;
                        }
                        else
                        {
                            this.LineLevel = _NotSpecified;
                            this.ActiveLevel = DigitalActiveLevels.None;
                        }

                        break;
                    }
                    // If subsystem.ac.HasValue AndAlso Me._SourceComboBox.ComboBox.Items.Count > 0 Then
                    // Me._SourceComboBox.ComboBox.SelectedItem = subsystem.TriggerSource.Value.ValueNamePair
                    // End If
            }
        }

        /// <summary> DigitalOutput subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DigitalOutputSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( DigitalOutputSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.DigitalOutputSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.DigitalOutputSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as DigitalOutputSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Read Button click. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadButton_Click( object sender, EventArgs e )
        {
            this.ReadLineLevel();
        }

        /// <summary> Write Button click. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void WriteButton_Click( object sender, EventArgs e )
        {
            this.WriteLineLevel( this.LineLevel );
        }

        /// <summary> Toggle button click. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ToggleButton_Click( object sender, EventArgs e )
        {
            this.ToggleLineLevel();
        }

        /// <summary> Pulse button click. </summary>
        /// <remarks> David, 2020-11-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PulseButton_Click( object sender, EventArgs e )
        {
            this.PulseLineLevel( this.LineLevel == 0 ? 1 : 0, this.PulseWidth );
        }

        /// <summary> Applies the settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ReadSettings();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
