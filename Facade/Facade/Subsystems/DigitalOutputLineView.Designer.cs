﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class DigitalOutputLineView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(DigitalOutputLineView));
            _ToolStripPanel = new ToolStripPanel();
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            _LineNumberNumericLabel = new ToolStripLabel();
            _LineNumberBox = new Core.Controls.ToolStripNumberBox();
            _ActiveLevelComboBoxLabel = new ToolStripLabel();
            _ActiveLevelComboBox = new ToolStripComboBox();
            __ReadButton = new ToolStripButton();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            _LineLevelNumberBox = new Core.Controls.ToolStripNumberBox();
            __WriteButton = new ToolStripButton();
            __WriteButton.Click += new EventHandler(WriteButton_Click);
            __PulseButton = new ToolStripButton();
            __PulseButton.Click += new EventHandler(PulseButton_Click);
            __ToggleButton = new ToolStripButton();
            __ToggleButton.Click += new EventHandler(ToggleButton_Click);
            _PulseWidthNumberBox = new Core.Controls.ToolStripNumberBox();
            _ToolStripPanel.SuspendLayout();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ToolStripPanel
            // 
            _ToolStripPanel.BackColor = System.Drawing.Color.Transparent;
            _ToolStripPanel.Controls.Add(_SubsystemToolStrip);
            _ToolStripPanel.Dock = DockStyle.Top;
            _ToolStripPanel.Location = new System.Drawing.Point(1, 1);
            _ToolStripPanel.Name = "_ToolStripPanel";
            _ToolStripPanel.Orientation = Orientation.Horizontal;
            _ToolStripPanel.RowMargin = new Padding(0);
            _ToolStripPanel.Size = new System.Drawing.Size(451, 28);
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.Dock = DockStyle.None;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _LineNumberNumericLabel, _LineNumberBox, _ActiveLevelComboBoxLabel, _ActiveLevelComboBox, __ReadButton, _LineLevelNumberBox, __WriteButton, __ToggleButton, __PulseButton, _PulseWidthNumberBox });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(451, 28);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 3;
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DoubleClickEnabled = true;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(45, 25);
            _SubsystemSplitButton.Text = "Line";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(150, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            __ApplySettingsMenuItem.ToolTipText = "Applies settings onto the instrument";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(150, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            __ReadSettingsMenuItem.ToolTipText = "Reads settings from the instrument";
            // 
            // _LineNumberNumericLabel
            // 
            _LineNumberNumericLabel.Name = "_LineNumberNumericLabel";
            _LineNumberNumericLabel.Size = new System.Drawing.Size(14, 25);
            _LineNumberNumericLabel.Text = "#";
            // 
            // _LineNumberBox
            // 
            _LineNumberBox.BackColor = System.Drawing.SystemColors.Window;
            _LineNumberBox.ForeColor = System.Drawing.SystemColors.ControlText;
            _LineNumberBox.MaxValue = 4.0d;
            _LineNumberBox.MinValue = 1.0d;
            _LineNumberBox.Name = "_LineNumberBox";
            _LineNumberBox.Prefix = null;
            _LineNumberBox.Size = new System.Drawing.Size(22, 25);
            _LineNumberBox.Snfs = "D";
            _LineNumberBox.Suffix = null;
            _LineNumberBox.Text = "-1";
            _LineNumberBox.ValueAsByte = Conversions.ToByte(0);
            _LineNumberBox.ValueAsDouble = 0d;
            _LineNumberBox.ValueAsFloat = 0f;
            _LineNumberBox.ValueAsInt16 = Conversions.ToShort(0);
            _LineNumberBox.ValueAsInt32 = 0;
            _LineNumberBox.ValueAsInt64 = Conversions.ToLong(0);
            _LineNumberBox.ValueAsSByte = 0;
            _LineNumberBox.ValueAsUInt16 = 0;
            _LineNumberBox.ValueAsUInt32 = 0U;
            _LineNumberBox.ValueAsUInt64 = 0UL;
            // 
            // _ActiveLevelComboBoxLabel
            // 
            _ActiveLevelComboBoxLabel.Name = "_ActiveLevelComboBoxLabel";
            _ActiveLevelComboBoxLabel.Size = new System.Drawing.Size(43, 25);
            _ActiveLevelComboBoxLabel.Text = "Active:";
            // 
            // _ActiveLevelComboBox
            // 
            _ActiveLevelComboBox.AutoSize = false;
            _ActiveLevelComboBox.Items.AddRange(new object[] { "Low", "High" });
            _ActiveLevelComboBox.Name = "_ActiveLevelComboBox";
            _ActiveLevelComboBox.Size = new System.Drawing.Size(50, 23);
            _ActiveLevelComboBox.Text = "Low";
            _ActiveLevelComboBox.ToolTipText = "Selects the active level";
            // 
            // _ReadButton
            // 
            __ReadButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __ReadButton.Image = (System.Drawing.Image)resources.GetObject("_ReadButton.Image");
            __ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(37, 25);
            __ReadButton.Text = "Read";
            __ReadButton.ToolTipText = "Read the digital level";
            // 
            // _LineLevelNumberBox
            // 
            _LineLevelNumberBox.BackColor = System.Drawing.SystemColors.Window;
            _LineLevelNumberBox.ForeColor = System.Drawing.SystemColors.ControlText;
            _LineLevelNumberBox.MaxValue = 1.0d;
            _LineLevelNumberBox.MinValue = -2.0d;
            _LineLevelNumberBox.Name = "_LineLevelNumberBox";
            _LineLevelNumberBox.Prefix = null;
            _LineLevelNumberBox.Size = new System.Drawing.Size(17, 25);
            _LineLevelNumberBox.Snfs = "D";
            _LineLevelNumberBox.Suffix = null;
            _LineLevelNumberBox.Text = "0";
            _LineLevelNumberBox.ToolTipText = "0 = inactive; 1 = active; -1 = not specified; -2 not set.";
            _LineLevelNumberBox.ValueAsByte = Conversions.ToByte(0);
            _LineLevelNumberBox.ValueAsDouble = 0d;
            _LineLevelNumberBox.ValueAsFloat = 0f;
            _LineLevelNumberBox.ValueAsInt16 = Conversions.ToShort(0);
            _LineLevelNumberBox.ValueAsInt32 = 0;
            _LineLevelNumberBox.ValueAsInt64 = Conversions.ToLong(0);
            _LineLevelNumberBox.ValueAsSByte = 0;
            _LineLevelNumberBox.ValueAsUInt16 = 0;
            _LineLevelNumberBox.ValueAsUInt32 = 0U;
            _LineLevelNumberBox.ValueAsUInt64 = 0UL;
            // 
            // _WriteButton
            // 
            __WriteButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __WriteButton.Image = (System.Drawing.Image)resources.GetObject("_WriteButton.Image");
            __WriteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __WriteButton.Name = "__WriteButton";
            __WriteButton.Size = new System.Drawing.Size(39, 25);
            __WriteButton.Text = "Write";
            __WriteButton.ToolTipText = "Output the value";
            // 
            // _PulseButton
            // 
            __PulseButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __PulseButton.Image = (System.Drawing.Image)resources.GetObject("_PulseButton.Image");
            __PulseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __PulseButton.Name = "__PulseButton";
            __PulseButton.Size = new System.Drawing.Size(61, 25);
            __PulseButton.Text = "Pulse";
            __PulseButton.ToolTipText = "Output a pulse for the specified duration in milliseconds";
            // 
            // _ToggleButton
            // 
            __ToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __ToggleButton.Image = (System.Drawing.Image)resources.GetObject("_ToggleButton.Image");
            __ToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ToggleButton.Name = "__ToggleButton";
            __ToggleButton.Size = new System.Drawing.Size(46, 25);
            __ToggleButton.Text = "Toggle";
            __ToggleButton.ToolTipText = "Toggle the line level";
            // 
            // _PulseWidthNumberBox
            // 
            _PulseWidthNumberBox.BackColor = System.Drawing.SystemColors.Window;
            _PulseWidthNumberBox.ForeColor = System.Drawing.SystemColors.ControlText;
            _PulseWidthNumberBox.MaxValue = 10000.0d;
            _PulseWidthNumberBox.MinValue = 1.0d;
            _PulseWidthNumberBox.Name = "_PulseWidthNumberBox";
            _PulseWidthNumberBox.Prefix = null;
            _PulseWidthNumberBox.Size = new System.Drawing.Size(17, 25);
            _PulseWidthNumberBox.Snfs = "N1";
            _PulseWidthNumberBox.Suffix = "ms";
            _PulseWidthNumberBox.Text = "1";
            _PulseWidthNumberBox.ToolTipText = "Pulse width in milliseconds";
            _PulseWidthNumberBox.ValueAsByte = Conversions.ToByte(0);
            _PulseWidthNumberBox.ValueAsDouble = 0d;
            _PulseWidthNumberBox.ValueAsFloat = 0f;
            _PulseWidthNumberBox.ValueAsInt16 = Conversions.ToShort(0);
            _PulseWidthNumberBox.ValueAsInt32 = 0;
            _PulseWidthNumberBox.ValueAsInt64 = Conversions.ToLong(0);
            _PulseWidthNumberBox.ValueAsSByte = 0;
            _PulseWidthNumberBox.ValueAsUInt16 = 0;
            _PulseWidthNumberBox.ValueAsUInt32 = 0U;
            _PulseWidthNumberBox.ValueAsUInt64 = 0UL;
            // 
            // DigitalOutputLineView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_ToolStripPanel);
            Name = "DigitalOutputLineView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(453, 31);
            _ToolStripPanel.ResumeLayout(false);
            _ToolStripPanel.PerformLayout();
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStripPanel _ToolStripPanel;
        private ToolStrip _SubsystemToolStrip;
        private ToolStripLabel _LineNumberNumericLabel;
        private ToolStripLabel _ActiveLevelComboBoxLabel;
        private ToolStripComboBox _ActiveLevelComboBox;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripButton __ReadButton;

        private ToolStripButton _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripNumberBox _LineLevelNumberBox;
        private ToolStripButton __WriteButton;

        private ToolStripButton _WriteButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WriteButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WriteButton != null)
                {
                    __WriteButton.Click -= WriteButton_Click;
                }

                __WriteButton = value;
                if (__WriteButton != null)
                {
                    __WriteButton.Click += WriteButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripNumberBox _LineNumberBox;
        private ToolStripButton __ToggleButton;

        private ToolStripButton _ToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToggleButton != null)
                {
                    __ToggleButton.Click -= ToggleButton_Click;
                }

                __ToggleButton = value;
                if (__ToggleButton != null)
                {
                    __ToggleButton.Click += ToggleButton_Click;
                }
            }
        }

        private ToolStripButton __PulseButton;

        private ToolStripButton _PulseButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PulseButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PulseButton != null)
                {
                    __PulseButton.Click -= PulseButton_Click;
                }

                __PulseButton = value;
                if (__PulseButton != null)
                {
                    __PulseButton.Click += PulseButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripNumberBox _PulseWidthNumberBox;
    }
}