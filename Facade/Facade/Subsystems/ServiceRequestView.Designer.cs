﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class ServiceRequestView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ServiceRequestView));
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            _ServiceRequestEnabledMenuItem = new ToolStripMenuItem();
            _UsingNegativeTransitionsMenuItem = new ToolStripMenuItem();
            __ToggleServiceRequestMenuItem = new ToolStripMenuItem();
            __ToggleServiceRequestMenuItem.Click += new EventHandler(ToggleServiceRequestMenuItem_Click);
            __HexadecimalToolStripMenuItem = new ToolStripMenuItem();
            __HexadecimalToolStripMenuItem.Click += new EventHandler(HexadecimalToolStripMenuItem_Click);
            _ServiceRequestFlagsComboBox = new ToolStripComboBox();
            _ServiceRequestMaskAddButton = new ToolStripButton();
            _ServiceRequestMaskRemoveButton = new ToolStripButton();
            _ServiceRequestMaskNumericLabel = new ToolStripLabel();
            _ServiceRequestMaskNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _ServiceRequestFlagsComboBox, _ServiceRequestMaskAddButton, _ServiceRequestMaskRemoveButton, _ServiceRequestMaskNumericLabel, _ServiceRequestMaskNumeric });
            _SubsystemToolStrip.Location = new System.Drawing.Point(1, 1);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(340, 26);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 1;
            _SubsystemToolStrip.Text = "Service Request Toolstrip";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _ServiceRequestEnabledMenuItem, _UsingNegativeTransitionsMenuItem, __ToggleServiceRequestMenuItem, __HexadecimalToolStripMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(45, 23);
            _SubsystemSplitButton.Text = "SRQ";
            _SubsystemSplitButton.ToolTipText = "SRQ Options and actions";
            // 
            // _ServiceRequestEnabledMenuItem
            // 
            _ServiceRequestEnabledMenuItem.CheckOnClick = true;
            _ServiceRequestEnabledMenuItem.Name = "_ServiceRequestEnabledMenuItem";
            _ServiceRequestEnabledMenuItem.Size = new System.Drawing.Size(213, 22);
            _ServiceRequestEnabledMenuItem.Text = "Service Request Enabled";
            _ServiceRequestEnabledMenuItem.ToolTipText = "Service request handling enabled when checked";
            // 
            // _UsingNegativeTransitionsMenuItem
            // 
            _UsingNegativeTransitionsMenuItem.CheckOnClick = true;
            _UsingNegativeTransitionsMenuItem.Name = "_UsingNegativeTransitionsMenuItem";
            _UsingNegativeTransitionsMenuItem.Size = new System.Drawing.Size(213, 22);
            _UsingNegativeTransitionsMenuItem.Text = "Using Negative transitions";
            _UsingNegativeTransitionsMenuItem.ToolTipText = "Uses negative transition when checked";
            // 
            // _ToggleServiceRequestMenuItem
            // 
            __ToggleServiceRequestMenuItem.CheckOnClick = true;
            __ToggleServiceRequestMenuItem.Name = "__ToggleServiceRequestMenuItem";
            __ToggleServiceRequestMenuItem.Size = new System.Drawing.Size(213, 22);
            __ToggleServiceRequestMenuItem.Text = "Enable Service Request";
            // 
            // _HexadecimalToolStripMenuItem
            // 
            __HexadecimalToolStripMenuItem.Checked = true;
            __HexadecimalToolStripMenuItem.CheckOnClick = true;
            __HexadecimalToolStripMenuItem.CheckState = CheckState.Checked;
            __HexadecimalToolStripMenuItem.Name = "__HexadecimalToolStripMenuItem";
            __HexadecimalToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            __HexadecimalToolStripMenuItem.Text = "Hexadecimal";
            __HexadecimalToolStripMenuItem.ToolTipText = "Toggles displays of service request max in hex or decimal";
            // 
            // _ServiceRequestFlagsComboBox
            // 
            _ServiceRequestFlagsComboBox.Name = "_ServiceRequestFlagsComboBox";
            _ServiceRequestFlagsComboBox.Size = new System.Drawing.Size(141, 26);
            // 
            // _ServiceRequestMaskAddButton
            // 
            _ServiceRequestMaskAddButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _ServiceRequestMaskAddButton.Image = (System.Drawing.Image)resources.GetObject("_ServiceRequestMaskAddButton.Image");
            _ServiceRequestMaskAddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ServiceRequestMaskAddButton.Name = "_ServiceRequestMaskAddButton";
            _ServiceRequestMaskAddButton.Size = new System.Drawing.Size(23, 23);
            _ServiceRequestMaskAddButton.Text = "+";
            _ServiceRequestMaskAddButton.ToolTipText = "Add to mask";
            // 
            // _ServiceRequestMaskRemoveButton
            // 
            _ServiceRequestMaskRemoveButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _ServiceRequestMaskRemoveButton.Image = (System.Drawing.Image)resources.GetObject("_ServiceRequestMaskRemoveButton.Image");
            _ServiceRequestMaskRemoveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ServiceRequestMaskRemoveButton.Name = "_ServiceRequestMaskRemoveButton";
            _ServiceRequestMaskRemoveButton.Size = new System.Drawing.Size(23, 23);
            _ServiceRequestMaskRemoveButton.Text = "-";
            // 
            // _ServiceRequestMaskNumericLabel
            // 
            _ServiceRequestMaskNumericLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _ServiceRequestMaskNumericLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            _ServiceRequestMaskNumericLabel.Name = "_ServiceRequestMaskNumericLabel";
            _ServiceRequestMaskNumericLabel.Size = new System.Drawing.Size(19, 23);
            _ServiceRequestMaskNumericLabel.Text = "0x";
            // 
            // _ServiceRequestMaskNumeric
            // 
            _ServiceRequestMaskNumeric.Name = "_ServiceRequestMaskNumeric";
            _ServiceRequestMaskNumeric.Size = new System.Drawing.Size(41, 23);
            _ServiceRequestMaskNumeric.Text = "0";
            _ServiceRequestMaskNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // ServiceRequestView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_SubsystemToolStrip);
            Name = "ServiceRequestView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(342, 28);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _SubsystemToolStrip;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem _ServiceRequestEnabledMenuItem;
        private ToolStripMenuItem _UsingNegativeTransitionsMenuItem;
        private ToolStripMenuItem __ToggleServiceRequestMenuItem;

        private ToolStripMenuItem _ToggleServiceRequestMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToggleServiceRequestMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToggleServiceRequestMenuItem != null)
                {
                    __ToggleServiceRequestMenuItem.Click -= ToggleServiceRequestMenuItem_Click;
                }

                __ToggleServiceRequestMenuItem = value;
                if (__ToggleServiceRequestMenuItem != null)
                {
                    __ToggleServiceRequestMenuItem.Click += ToggleServiceRequestMenuItem_Click;
                }
            }
        }

        private ToolStripComboBox _ServiceRequestFlagsComboBox;
        private ToolStripButton _ServiceRequestMaskAddButton;
        private ToolStripButton _ServiceRequestMaskRemoveButton;
        private ToolStripLabel _ServiceRequestMaskNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _ServiceRequestMaskNumeric;
        private ToolStripMenuItem __HexadecimalToolStripMenuItem;

        private ToolStripMenuItem _HexadecimalToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __HexadecimalToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__HexadecimalToolStripMenuItem != null)
                {
                    __HexadecimalToolStripMenuItem.Click -= HexadecimalToolStripMenuItem_Click;
                }

                __HexadecimalToolStripMenuItem = value;
                if (__HexadecimalToolStripMenuItem != null)
                {
                    __HexadecimalToolStripMenuItem.Click += HexadecimalToolStripMenuItem_Click;
                }
            }
        }
    }
}