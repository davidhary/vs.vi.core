using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.SplitExtensions;
using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{

    /// <summary> An trace buffer view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-01-13 </para>
    /// </remarks>
    public partial class TraceBufferView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public TraceBufferView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._SizeNumeric.NumericUpDownControl.Minimum = 0m;
            this._SizeNumeric.NumericUpDownControl.Maximum = int.MaxValue;
            this._SizeNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._SizeNumeric.NumericUpDownControl.Value = 2m;
            this._PreTriggerCountNumeric.NumericUpDownControl.Minimum = 0m;
            this._PreTriggerCountNumeric.NumericUpDownControl.Maximum = int.MaxValue;
            this._PreTriggerCountNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._PreTriggerCountNumeric.NumericUpDownControl.Value = 0m;

            // Hide to be done items
            this._ElementGroupToggleButton.Visible = false;
            this._PreTriggerCountNumeric.Visible = false;
            this._PreTriggerCountNumericLabel.Visible = false;
            this._TimestampFormatToggleButton.Visible = false;
            this.__ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
            this.__ElementGroupToggleButton.Name = "_ElementGroupToggleButton";
            this.__ClearBufferButton.Name = "_ClearBufferButton";
            this.__TimestampFormatToggleButton.Name = "_TimestampFormatToggleButton";
        }

        /// <summary> Creates a new TraceBufferView. </summary>
        /// <returns> A TraceBufferView. </returns>
        public static TraceBufferView Create()
        {
            TraceBufferView view = null;
            try
            {
                view = new TraceBufferView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Name of the buffer. </summary>
        private string _BufferName;

        /// <summary> Gets or sets the name of the buffer. </summary>
        /// <value> The name of the buffer. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string BufferName
        {
            get => this._BufferName;

            set {
                if ( !string.Equals( value, this.BufferName ) )
                {
                    this._BufferName = value;
                    this._BufferNameLabel.Text = value;
                }
            }
        }

        /// <summary> Gets or sets the size of the buffer. </summary>
        /// <value> The size of the buffer. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int BufferSize
        {
            get => ( int ) this._SizeNumeric.Value;

            set {
                if ( value != this.BufferSize )
                {
                    this._SizeNumeric.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the feed source. </summary>
        /// <value> The feed source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public FeedSources FeedSource
        {
            get => ( FeedSources ) Conversions.ToInteger( this._FeedSourceComboBox.ComboBox.SelectedValue );

            set {
                if ( value != this.FeedSource )
                {
                    this._FeedSourceComboBox.ComboBox.SelectedItem = value.ValueNamePair();
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the feed control. </summary>
        /// <value> The feed control. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public FeedControls FeedControl
        {
            get => ( FeedControls ) Conversions.ToInteger( this._FeedControlComboBox.ComboBox.SelectedValue );

            set {
                if ( ( int ) value != ( int ) this.FeedSource )
                {
                    this._FeedControlComboBox.ComboBox.SelectedItem = value.ValueNamePair();
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Applies the settings onto the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying {this._SubsystemSplitButton.Text} settings ";
                _ = this.PublishInfo( $"{activity};. " );
                this.TraceSubsystem.StartElapsedStopwatch();
                _ = this.TraceSubsystem.ApplyPointsCount( ( int ) this._SizeNumeric.Value );
                _ = this.TraceSubsystem.ApplyFeedSource( ( FeedSources ) Conversions.ToInteger( this._FeedSourceComboBox.ComboBox.SelectedValue ) );
                _ = this.TraceSubsystem.ApplyFeedControl( ( FeedControls ) Conversions.ToInteger( this._FeedControlComboBox.ComboBox.SelectedValue ) );
                this.TraceSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads the settings from the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} reading  {this._SubsystemSplitButton.Text} settings";
                _ = this.PublishInfo( $"{activity};. " );
                ReadSettings( this.TraceSubsystem );
                this.ApplyPropertyChanged( this.TraceSubsystem );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears the buffer to its blank/initial state. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void Clear()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} Clearing {this._SubsystemSplitButton.Text}";
                _ = this.PublishInfo( $"{activity};. " );
                this.TraceSubsystem.StartElapsedStopwatch();
                this.TraceSubsystem.ClearBuffer();
                this.TraceSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( TraceBufferView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRACE SUBSYSTEM "

        /// <summary> Gets or sets the trace subsystem. </summary>
        /// <value> The trace subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TraceSubsystemBase TraceSubsystem { get; private set; }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem">  The subsystem. </param>
        /// <param name="bufferName"> The name of the buffer. </param>
        public void BindSubsystem( TraceSubsystemBase subsystem, string bufferName )
        {
            if ( this.TraceSubsystem is object )
            {
                this.BindSubsystem( false, this.TraceSubsystem );
                this.TraceSubsystem = null;
            }

            this.BufferName = bufferName;
            this.TraceSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.TraceSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TraceSubsystemBase subsystem )
        {
            if ( add )
            {
                this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.SupportedFeedControls ) );
                this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.SupportedFeedSources ) );
                subsystem.PropertyChanged += this.TraceSubsystemPropertyChanged;
                // must not read setting when biding because the instrument may be locked or in a trigger mode
                // The bound values should be sent when binding or when applying propert change.
                // ReadSettings( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.TraceSubsystemPropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( TraceSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.AvailablePointCount ) );
            this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.PointsCount ) );
            this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.FeedControl ) );
            this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.FeedSource ) );
        }

        /// <summary> Reads the settings from the instrument. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( TraceSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryBufferFreePointCount();
            _ = subsystem.QueryPointsCount();
            _ = subsystem.QueryFeedSource();
            _ = subsystem.QueryFeedControl();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Handle the Calculate subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TraceSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( TraceSubsystemBase.AvailablePointCount ):
                    {
                        this._FreeCountLabel.Text = subsystem.AvailablePointCount is null ? "?" : subsystem.AvailablePointCount.Value.ToString();
                        break;
                    }

                case nameof( TraceSubsystemBase.FeedControl ):
                    {
                        if ( subsystem.FeedControl.HasValue && this._FeedControlComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._FeedControlComboBox.ComboBox.SelectedItem = subsystem.FeedControl.Value.ValueNamePair();
                        }

                        break;
                    }

                case nameof( TraceSubsystemBase.FeedSource ):
                    {
                        if ( subsystem.FeedSource.HasValue && this._FeedSourceComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._FeedSourceComboBox.ComboBox.SelectedItem = subsystem.FeedSource.Value.ValueNamePair();
                        }

                        break;
                    }

                case nameof( TraceSubsystemBase.PointsCount ):
                    {
                        if ( subsystem.PointsCount.HasValue )
                            this._SizeNumeric.Value = subsystem.PointsCount.Value;
                        break;
                    }

                case nameof( TraceSubsystemBase.SupportedFeedControls ):
                    {
                        this._FeedControlComboBox.ComboBox.ListSupportedFeedControls( subsystem.SupportedFeedControls );
                        if ( subsystem.FeedControl.HasValue && this._FeedControlComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._FeedControlComboBox.ComboBox.SelectedItem = subsystem.FeedControl.Value.ValueNamePair();
                        }

                        break;
                    }

                case nameof( TraceSubsystemBase.SupportedFeedSources ):
                    {
                        this._FeedSourceComboBox.ComboBox.ListSupportedFeedSources( subsystem.SupportedFeedSources );
                        if ( subsystem.FeedSource.HasValue && this._FeedSourceComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._FeedSourceComboBox.ComboBox.SelectedItem = subsystem.FeedSource.Value.ValueNamePair();
                        }

                        break;
                    }
            }
        }

        /// <summary> Trace subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TraceSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( TraceSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TraceSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.TraceSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TraceSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Clears the buffer button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ClearBufferButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.Clear();
        }

        /// <summary> Element group toggle button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ElementGroupToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = $"Elements: {button.CheckState.ToCheckStateCaption( "Full", "Compact", "?" )}";
            }
        }

        /// <summary> Timestamp format toggle button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TimestampFormatToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = $"Timestamp: {button.CheckState.ToCheckStateCaption( "Absolute", "Delta", "?" )}";
            }
        }

        /// <summary> Applies the settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ReadSettings();
        }


        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
