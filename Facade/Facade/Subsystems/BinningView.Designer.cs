﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class BinningView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(BinningView));
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            __PreformLimitTestMenuItem = new ToolStripMenuItem();
            __PreformLimitTestMenuItem.Click += new EventHandler(PreformLimitTestMenuItem_Click);
            __ReadLimitTestStateMenuItem = new ToolStripMenuItem();
            __ReadLimitTestStateMenuItem.Click += new EventHandler(ReadStateMenuItem_Click);
            __PassBitPatternNumericButton = new ToolStripButton();
            __PassBitPatternNumericButton.CheckStateChanged += new EventHandler(PassBitPatternNumericButton_CheckStateChanged);
            _PassBitPatternNumeric = new Core.Controls.ToolStripNumericUpDown();
            __BinningStobeToggleButton = new ToolStripButton();
            __BinningStobeToggleButton.CheckStateChanged += new EventHandler(BinningStobeToggleButton_CheckStateChanged);
            __LimitFailedButton = new ToolStripButton();
            __LimitFailedButton.CheckStateChanged += new EventHandler(LimitFailedButton_CheckStateChanged);
            _BinningStrobeDurationNumericLabel = new ToolStripLabel();
            _BinningStrobeDurationNumeric = new Core.Controls.ToolStripNumericUpDown();
            _BinningStrobeDurationNumericUnitLabel = new ToolStripLabel();
            _Limit2View = new LimitView();
            _Limit1View = new LimitView();
            _ToolStripPanel = new ToolStripPanel();
            _InfoTextBox = new TextBox();
            _DigitalOutputActiveHighMenuItem = new ToolStripMenuItem();
            _SubsystemToolStrip.SuspendLayout();
            _ToolStripPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.AutoSize = false;
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.Dock = DockStyle.None;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, __PassBitPatternNumericButton, _PassBitPatternNumeric, __BinningStobeToggleButton, __LimitFailedButton, _BinningStrobeDurationNumericLabel, _BinningStrobeDurationNumeric, _BinningStrobeDurationNumericUnitLabel });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(403, 28);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 0;
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem, __PreformLimitTestMenuItem, __ReadLimitTestStateMenuItem, _DigitalOutputActiveHighMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(42, 25);
            _SubsystemSplitButton.Text = "Bin";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(215, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            __ApplySettingsMenuItem.ToolTipText = "Applies the limit settings to the instrument";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(215, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            __ReadSettingsMenuItem.ToolTipText = "Read current settings from the instrument";
            // 
            // _PreformLimitTestMenuItem
            // 
            __PreformLimitTestMenuItem.Name = "__PreformLimitTestMenuItem";
            __PreformLimitTestMenuItem.Size = new System.Drawing.Size(215, 22);
            __PreformLimitTestMenuItem.Text = "Preform Limit Test";
            __PreformLimitTestMenuItem.ToolTipText = "Performs the limit test";
            // 
            // _ReadLimitTestStateMenuItem
            // 
            __ReadLimitTestStateMenuItem.Name = "__ReadLimitTestStateMenuItem";
            __ReadLimitTestStateMenuItem.Size = new System.Drawing.Size(215, 22);
            __ReadLimitTestStateMenuItem.Text = "Read Limit Test State";
            __ReadLimitTestStateMenuItem.ToolTipText = "Performs and reads limit test results";
            // 
            // _PassBitPatternNumericButton
            // 
            __PassBitPatternNumericButton.CheckOnClick = true;
            __PassBitPatternNumericButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __PassBitPatternNumericButton.Image = (System.Drawing.Image)resources.GetObject("_PassBitPatternNumericButton.Image");
            __PassBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __PassBitPatternNumericButton.Name = "__PassBitPatternNumericButton";
            __PassBitPatternNumericButton.Size = new System.Drawing.Size(52, 25);
            __PassBitPatternNumericButton.Text = "Pass: 0x";
            // 
            // _PassBitPatternNumeric
            // 
            _PassBitPatternNumeric.Name = "_PassBitPatternNumeric";
            _PassBitPatternNumeric.Size = new System.Drawing.Size(41, 25);
            _PassBitPatternNumeric.Text = "2";
            _PassBitPatternNumeric.ToolTipText = "Pass bit pattern";
            _PassBitPatternNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _BinningStobeToggleButton
            // 
            __BinningStobeToggleButton.Checked = true;
            __BinningStobeToggleButton.CheckOnClick = true;
            __BinningStobeToggleButton.CheckState = CheckState.Indeterminate;
            __BinningStobeToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __BinningStobeToggleButton.Image = (System.Drawing.Image)resources.GetObject("_BinningStobeToggleButton.Image");
            __BinningStobeToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __BinningStobeToggleButton.Name = "__BinningStobeToggleButton";
            __BinningStobeToggleButton.Size = new System.Drawing.Size(52, 25);
            __BinningStobeToggleButton.Text = "Stobe: ?";
            __BinningStobeToggleButton.ToolTipText = "Toggle to enable or disable binning strobe";
            // 
            // _LimitFailedButton
            // 
            __LimitFailedButton.Checked = true;
            __LimitFailedButton.CheckState = CheckState.Indeterminate;
            __LimitFailedButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __LimitFailedButton.Image = (System.Drawing.Image)resources.GetObject("_LimitFailedButton.Image");
            __LimitFailedButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __LimitFailedButton.Name = "__LimitFailedButton";
            __LimitFailedButton.Size = new System.Drawing.Size(47, 25);
            __LimitFailedButton.Text = "Failed?";
            __LimitFailedButton.ToolTipText = "Displays limit state";
            // 
            // _BinningStrobeDurationNumericLabel
            // 
            _BinningStrobeDurationNumericLabel.Margin = new Padding(3, 1, 0, 2);
            _BinningStrobeDurationNumericLabel.Name = "_BinningStrobeDurationNumericLabel";
            _BinningStrobeDurationNumericLabel.Size = new System.Drawing.Size(56, 25);
            _BinningStrobeDurationNumericLabel.Text = "Duration:";
            // 
            // _BinningStrobeDurationNumeric
            // 
            _BinningStrobeDurationNumeric.Name = "_BinningStrobeDurationNumeric";
            _BinningStrobeDurationNumeric.Size = new System.Drawing.Size(41, 25);
            _BinningStrobeDurationNumeric.Text = "0";
            _BinningStrobeDurationNumeric.ToolTipText = "Duration of binning strobe to use for timing the trigger plan";
            _BinningStrobeDurationNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _BinningStrobeDurationNumericUnitLabel
            // 
            _BinningStrobeDurationNumericUnitLabel.Name = "_BinningStrobeDurationNumericUnitLabel";
            _BinningStrobeDurationNumericUnitLabel.Size = new System.Drawing.Size(23, 25);
            _BinningStrobeDurationNumericUnitLabel.Text = "ms";
            // 
            // _Limit2View
            // 
            _Limit2View.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _Limit2View.Dock = DockStyle.Top;
            _Limit2View.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _Limit2View.Location = new System.Drawing.Point(1, 111);
            _Limit2View.Name = "_Limit2View";
            _Limit2View.Padding = new Padding(1);
            _Limit2View.Size = new System.Drawing.Size(403, 82);
            _Limit2View.TabIndex = 3;
            // 
            // _Limit1View
            // 
            _Limit1View.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _Limit1View.Dock = DockStyle.Top;
            _Limit1View.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _Limit1View.Location = new System.Drawing.Point(1, 29);
            _Limit1View.Name = "_Limit1View";
            _Limit1View.Padding = new Padding(1);
            _Limit1View.Size = new System.Drawing.Size(403, 82);
            _Limit1View.TabIndex = 1;
            // 
            // _ToolStripPanel
            // 
            _ToolStripPanel.BackColor = System.Drawing.Color.Transparent;
            _ToolStripPanel.Controls.Add(_SubsystemToolStrip);
            _ToolStripPanel.Dock = DockStyle.Top;
            _ToolStripPanel.Location = new System.Drawing.Point(1, 1);
            _ToolStripPanel.Name = "_ToolStripPanel";
            _ToolStripPanel.Orientation = Orientation.Horizontal;
            _ToolStripPanel.RowMargin = new Padding(3, 0, 0, 0);
            _ToolStripPanel.Size = new System.Drawing.Size(403, 28);
            // 
            // _InfoTextBox
            // 
            _InfoTextBox.Dock = DockStyle.Fill;
            _InfoTextBox.Location = new System.Drawing.Point(1, 193);
            _InfoTextBox.Margin = new Padding(3, 4, 3, 4);
            _InfoTextBox.Multiline = true;
            _InfoTextBox.Name = "_InfoTextBox";
            _InfoTextBox.ReadOnly = true;
            _InfoTextBox.ScrollBars = ScrollBars.Both;
            _InfoTextBox.Size = new System.Drawing.Size(403, 147);
            _InfoTextBox.TabIndex = 26;
            _InfoTextBox.Text = "<info>";
            // 
            // _DigitalOutputActiveHighMenuItem
            // 
            _DigitalOutputActiveHighMenuItem.CheckOnClick = true;
            _DigitalOutputActiveHighMenuItem.Name = "_DigitalOutputActiveHighMenuItem";
            _DigitalOutputActiveHighMenuItem.Size = new System.Drawing.Size(215, 22);
            _DigitalOutputActiveHighMenuItem.Text = "Digital Output Active High";
            _DigitalOutputActiveHighMenuItem.ToolTipText = "Toggles binning digital output polarity";
            // 
            // BinningView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_InfoTextBox);
            Controls.Add(_Limit2View);
            Controls.Add(_Limit1View);
            Controls.Add(_ToolStripPanel);
            Name = "BinningView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(405, 341);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            _ToolStripPanel.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _SubsystemToolStrip;
        private ToolStripButton __PassBitPatternNumericButton;

        private ToolStripButton _PassBitPatternNumericButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PassBitPatternNumericButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PassBitPatternNumericButton != null)
                {
                    __PassBitPatternNumericButton.CheckStateChanged -= PassBitPatternNumericButton_CheckStateChanged;
                }

                __PassBitPatternNumericButton = value;
                if (__PassBitPatternNumericButton != null)
                {
                    __PassBitPatternNumericButton.CheckStateChanged += PassBitPatternNumericButton_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _PassBitPatternNumeric;
        private LimitView _Limit2View;
        private ToolStripButton __BinningStobeToggleButton;

        private ToolStripButton _BinningStobeToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BinningStobeToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BinningStobeToggleButton != null)
                {
                    __BinningStobeToggleButton.CheckStateChanged -= BinningStobeToggleButton_CheckStateChanged;
                }

                __BinningStobeToggleButton = value;
                if (__BinningStobeToggleButton != null)
                {
                    __BinningStobeToggleButton.CheckStateChanged += BinningStobeToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStripButton __LimitFailedButton;

        private ToolStripButton _LimitFailedButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LimitFailedButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LimitFailedButton != null)
                {
                    __LimitFailedButton.CheckStateChanged -= LimitFailedButton_CheckStateChanged;
                }

                __LimitFailedButton = value;
                if (__LimitFailedButton != null)
                {
                    __LimitFailedButton.CheckStateChanged += LimitFailedButton_CheckStateChanged;
                }
            }
        }

        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadLimitTestStateMenuItem;

        private ToolStripMenuItem _ReadLimitTestStateMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadLimitTestStateMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadLimitTestStateMenuItem != null)
                {
                    __ReadLimitTestStateMenuItem.Click -= ReadStateMenuItem_Click;
                }

                __ReadLimitTestStateMenuItem = value;
                if (__ReadLimitTestStateMenuItem != null)
                {
                    __ReadLimitTestStateMenuItem.Click += ReadStateMenuItem_Click;
                }
            }
        }

        private LimitView _Limit1View;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripLabel _BinningStrobeDurationNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _BinningStrobeDurationNumeric;
        private ToolStripLabel _BinningStrobeDurationNumericUnitLabel;
        private ToolStripPanel _ToolStripPanel;
        private ToolStripMenuItem __PreformLimitTestMenuItem;

        private ToolStripMenuItem _PreformLimitTestMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PreformLimitTestMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PreformLimitTestMenuItem != null)
                {
                    __PreformLimitTestMenuItem.Click -= PreformLimitTestMenuItem_Click;
                }

                __PreformLimitTestMenuItem = value;
                if (__PreformLimitTestMenuItem != null)
                {
                    __PreformLimitTestMenuItem.Click += PreformLimitTestMenuItem_Click;
                }
            }
        }

        private TextBox _InfoTextBox;
        private ToolStripMenuItem _DigitalOutputActiveHighMenuItem;
    }
}