﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class RelayView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(RelayView));
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __AddRelayMenuItem = new ToolStripMenuItem();
            __AddRelayMenuItem.Click += new EventHandler(AddChannelToList_Click);
            __AddMemoryLocationToListMenuItem = new ToolStripMenuItem();
            __AddMemoryLocationToListMenuItem.Click += new EventHandler(AddMemoryLocationButton_Click);
            __ClearChannelLIstMenuItem = new ToolStripMenuItem();
            __ClearChannelLIstMenuItem.Click += new EventHandler(ClearChannelLIstMenuItem_Click);
            _SlotNumberComboBoxLabel = new ToolStripLabel();
            _SlotNumberNumeric = new Core.Controls.ToolStripNumericUpDown();
            _RelayNumberNumericLabel = new ToolStripLabel();
            _RelayNumberNumeric = new Core.Controls.ToolStripNumericUpDown();
            _MemoryLocationNumberLabel = new ToolStripLabel();
            _MemoryLocationNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, _SlotNumberComboBoxLabel, _SlotNumberNumeric, _RelayNumberNumericLabel, _RelayNumberNumeric, _MemoryLocationNumberLabel, _MemoryLocationNumeric });
            _SubsystemToolStrip.Location = new System.Drawing.Point(1, 1);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(475, 26);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 2;
            _SubsystemToolStrip.Text = "ChannelToolStrip";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __AddRelayMenuItem, __AddMemoryLocationToListMenuItem, __ClearChannelLIstMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(52, 23);
            _SubsystemSplitButton.Text = "Relay";
            _SubsystemSplitButton.ToolTipText = "Select action";
            // 
            // _AddRelayMenuItem
            // 
            __AddRelayMenuItem.Name = "__AddRelayMenuItem";
            __AddRelayMenuItem.Size = new System.Drawing.Size(222, 22);
            __AddRelayMenuItem.Text = "Add Relay to List";
            __AddRelayMenuItem.ToolTipText = "Adds a relay to the channel list";
            // 
            // _AddMemoryLocationToListMenuItem
            // 
            __AddMemoryLocationToListMenuItem.Name = "__AddMemoryLocationToListMenuItem";
            __AddMemoryLocationToListMenuItem.Size = new System.Drawing.Size(222, 22);
            __AddMemoryLocationToListMenuItem.Text = "Add Memory Location to List";
            __AddMemoryLocationToListMenuItem.ToolTipText = "Adds a memory location to the list";
            // 
            // _ClearChannelLIstMenuItem
            // 
            __ClearChannelLIstMenuItem.Name = "__ClearChannelLIstMenuItem";
            __ClearChannelLIstMenuItem.Size = new System.Drawing.Size(222, 22);
            __ClearChannelLIstMenuItem.Text = "Clear List";
            __ClearChannelLIstMenuItem.ToolTipText = "Clears the channel list builder";
            // 
            // _SlotNumberComboBoxLabel
            // 
            _SlotNumberComboBoxLabel.Margin = new Padding(3, 1, 0, 2);
            _SlotNumberComboBoxLabel.Name = "_SlotNumberComboBoxLabel";
            _SlotNumberComboBoxLabel.Size = new System.Drawing.Size(27, 23);
            _SlotNumberComboBoxLabel.Text = "Slot";
            // 
            // _SlotNumberNumeric
            // 
            _SlotNumberNumeric.Name = "_SlotNumberNumeric";
            _SlotNumberNumeric.Size = new System.Drawing.Size(41, 23);
            _SlotNumberNumeric.Text = "0";
            _SlotNumberNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _RelayNumberNumericLabel
            // 
            _RelayNumberNumericLabel.Name = "_RelayNumberNumericLabel";
            _RelayNumberNumericLabel.Size = new System.Drawing.Size(38, 23);
            _RelayNumberNumericLabel.Text = "Relay:";
            // 
            // _RelayNumberNumeric
            // 
            _RelayNumberNumeric.Margin = new Padding(0, 1, 0, 1);
            _RelayNumberNumeric.Name = "_RelayNumberNumeric";
            _RelayNumberNumeric.Size = new System.Drawing.Size(41, 24);
            _RelayNumberNumeric.Text = "0";
            _RelayNumberNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _MemoryLocationNumberLabel
            // 
            _MemoryLocationNumberLabel.Margin = new Padding(3, 1, 0, 2);
            _MemoryLocationNumberLabel.Name = "_MemoryLocationNumberLabel";
            _MemoryLocationNumberLabel.Size = new System.Drawing.Size(104, 23);
            _MemoryLocationNumberLabel.Text = "Memory Location:";
            // 
            // _MemoryLocationNumeric
            // 
            _MemoryLocationNumeric.Name = "_MemoryLocationNumeric";
            _MemoryLocationNumeric.Size = new System.Drawing.Size(41, 23);
            _MemoryLocationNumeric.Text = "0";
            _MemoryLocationNumeric.ToolTipText = "Memory location";
            _MemoryLocationNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // RelayView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_SubsystemToolStrip);
            Name = "RelayView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(477, 26);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _SubsystemToolStrip;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __AddRelayMenuItem;

        private ToolStripMenuItem _AddRelayMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AddRelayMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AddRelayMenuItem != null)
                {
                    __AddRelayMenuItem.Click -= AddChannelToList_Click;
                }

                __AddRelayMenuItem = value;
                if (__AddRelayMenuItem != null)
                {
                    __AddRelayMenuItem.Click += AddChannelToList_Click;
                }
            }
        }

        private ToolStripMenuItem __ClearChannelLIstMenuItem;

        private ToolStripMenuItem _ClearChannelLIstMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearChannelLIstMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearChannelLIstMenuItem != null)
                {
                    __ClearChannelLIstMenuItem.Click -= ClearChannelLIstMenuItem_Click;
                }

                __ClearChannelLIstMenuItem = value;
                if (__ClearChannelLIstMenuItem != null)
                {
                    __ClearChannelLIstMenuItem.Click += ClearChannelLIstMenuItem_Click;
                }
            }
        }

        private ToolStripLabel _SlotNumberComboBoxLabel;
        private Core.Controls.ToolStripNumericUpDown _SlotNumberNumeric;
        private ToolStripLabel _RelayNumberNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _RelayNumberNumeric;
        private ToolStripLabel _MemoryLocationNumberLabel;
        private Core.Controls.ToolStripNumericUpDown _MemoryLocationNumeric;
        private ToolStripMenuItem __AddMemoryLocationToListMenuItem;

        private ToolStripMenuItem _AddMemoryLocationToListMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AddMemoryLocationToListMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AddMemoryLocationToListMenuItem != null)
                {
                    __AddMemoryLocationToListMenuItem.Click -= AddMemoryLocationButton_Click;
                }

                __AddMemoryLocationToListMenuItem = value;
                if (__AddMemoryLocationToListMenuItem != null)
                {
                    __AddMemoryLocationToListMenuItem.Click += AddMemoryLocationButton_Click;
                }
            }
        }
    }
}