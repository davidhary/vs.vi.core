﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class ScanListView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _SubsystemToolStrip = new ToolStrip();
            _SubsytemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsMenuItem_Click);
            __CloseMenuItem = new ToolStripMenuItem();
            __CloseMenuItem.Click += new EventHandler(CloseMenuItem_Click);
            __OpenMenuItem = new ToolStripMenuItem();
            __OpenMenuItem.Click += new EventHandler(OpenMenuItem_Click);
            __OpenAllMenuItem = new ToolStripMenuItem();
            __OpenAllMenuItem.Click += new EventHandler(OpenAllMenuItem_Click);
            _SaveToMemoryLocationMenuItem = new ToolStripMenuItem();
            __MemoryLocationTextBox = new ToolStripTextBox();
            __MemoryLocationTextBox.Validating += new System.ComponentModel.CancelEventHandler(MemoryLocationTextBox_Validating);
            __SaveToMemotyLocationMenuItem = new ToolStripMenuItem();
            __SaveToMemotyLocationMenuItem.Click += new EventHandler(SaveToMemotyLocationMenuItem_Click);
            _CandidateScanListComboBox = new ToolStripComboBox();
            _ScanListLabel = new ToolStripLabel();
            _ClosedChannelsLabel = new ToolStripLabel();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsytemSplitButton, _CandidateScanListComboBox, _ScanListLabel, _ClosedChannelsLabel });
            _SubsystemToolStrip.Location = new System.Drawing.Point(1, 1);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(352, 25);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 15;
            _SubsystemToolStrip.Text = "Subsystem Tool Strip";
            // 
            // _SubsytemSplitButton
            // 
            _SubsytemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem, __CloseMenuItem, __OpenMenuItem, __OpenAllMenuItem, _SaveToMemoryLocationMenuItem });
            _SubsytemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsytemSplitButton.Name = "_SubsytemSplitButton";
            _SubsytemSplitButton.Size = new System.Drawing.Size(69, 22);
            _SubsytemSplitButton.Text = "Scan List";
            _SubsytemSplitButton.ToolTipText = "Scan List actions";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(163, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(163, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            // 
            // _CloseMenuItem
            // 
            __CloseMenuItem.Name = "__CloseMenuItem";
            __CloseMenuItem.Size = new System.Drawing.Size(163, 22);
            __CloseMenuItem.Text = "Close";
            // 
            // _OpenMenuItem
            // 
            __OpenMenuItem.Name = "__OpenMenuItem";
            __OpenMenuItem.Size = new System.Drawing.Size(163, 22);
            __OpenMenuItem.Text = "Open";
            // 
            // _OpenAllMenuItem
            // 
            __OpenAllMenuItem.Name = "__OpenAllMenuItem";
            __OpenAllMenuItem.Size = new System.Drawing.Size(163, 22);
            __OpenAllMenuItem.Text = "Open All";
            // 
            // _SaveToMemoryLocationMenuItem
            // 
            _SaveToMemoryLocationMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __MemoryLocationTextBox, __SaveToMemotyLocationMenuItem });
            _SaveToMemoryLocationMenuItem.Name = "_SaveToMemoryLocationMenuItem";
            _SaveToMemoryLocationMenuItem.Size = new System.Drawing.Size(163, 22);
            _SaveToMemoryLocationMenuItem.Text = "Memory Location";
            // 
            // _MemoryLocationTextBox
            // 
            __MemoryLocationTextBox.Font = new System.Drawing.Font("Segoe UI", 9.0f);
            __MemoryLocationTextBox.Name = "__MemoryLocationTextBox";
            __MemoryLocationTextBox.Size = new System.Drawing.Size(100, 23);
            __MemoryLocationTextBox.Text = "1";
            // 
            // _SaveToMemotyLocationMenuItem
            // 
            __SaveToMemotyLocationMenuItem.Name = "__SaveToMemotyLocationMenuItem";
            __SaveToMemotyLocationMenuItem.Size = new System.Drawing.Size(160, 22);
            __SaveToMemotyLocationMenuItem.Text = "Save";
            // 
            // _CandidateScanListComboBox
            // 
            _CandidateScanListComboBox.Items.AddRange(new object[] { "(@1!1:1!10)", "(@M1,M2)", "(@ 4!1,5!1)", "(@1!1)", "(@1!2:1!10, 2!3!6)", "(@1!30:1!40, 2!1:2!10)", "(@1!4!1:1!4!10, 2!1!1:2!4!10)" });
            _CandidateScanListComboBox.Name = "_CandidateScanListComboBox";
            _CandidateScanListComboBox.Size = new System.Drawing.Size(141, 25);
            _CandidateScanListComboBox.Text = "(@1!1:1!10)";
            _CandidateScanListComboBox.ToolTipText = "Edits the scan list";
            // 
            // _ScanListLabel
            // 
            _ScanListLabel.Name = "_ScanListLabel";
            _ScanListLabel.Size = new System.Drawing.Size(12, 22);
            _ScanListLabel.Text = "?";
            // 
            // _ClosedChannelsLabel
            // 
            _ClosedChannelsLabel.Name = "_ClosedChannelsLabel";
            _ClosedChannelsLabel.Size = new System.Drawing.Size(57, 22);
            _ClosedChannelsLabel.Text = "<closed>";
            _ClosedChannelsLabel.ToolTipText = "Scan list of closed channels";
            // 
            // ScanListView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_SubsystemToolStrip);
            Name = "ScanListView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(354, 28);
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStripComboBox _CandidateScanListComboBox;
        private ToolStrip _SubsystemToolStrip;
        private ToolStripSplitButton _SubsytemSplitButton;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripLabel _ScanListLabel;
        private ToolStripTextBox __MemoryLocationTextBox;

        private ToolStripTextBox _MemoryLocationTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MemoryLocationTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MemoryLocationTextBox != null)
                {
                    __MemoryLocationTextBox.Validating -= MemoryLocationTextBox_Validating;
                }

                __MemoryLocationTextBox = value;
                if (__MemoryLocationTextBox != null)
                {
                    __MemoryLocationTextBox.Validating += MemoryLocationTextBox_Validating;
                }
            }
        }

        private ToolStripMenuItem __SaveToMemotyLocationMenuItem;

        private ToolStripMenuItem _SaveToMemotyLocationMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SaveToMemotyLocationMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SaveToMemotyLocationMenuItem != null)
                {
                    __SaveToMemotyLocationMenuItem.Click -= SaveToMemotyLocationMenuItem_Click;
                }

                __SaveToMemotyLocationMenuItem = value;
                if (__SaveToMemotyLocationMenuItem != null)
                {
                    __SaveToMemotyLocationMenuItem.Click += SaveToMemotyLocationMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem _SaveToMemoryLocationMenuItem;
        private ToolStripMenuItem __OpenAllMenuItem;

        private ToolStripMenuItem _OpenAllMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenAllMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenAllMenuItem != null)
                {
                    __OpenAllMenuItem.Click -= OpenAllMenuItem_Click;
                }

                __OpenAllMenuItem = value;
                if (__OpenAllMenuItem != null)
                {
                    __OpenAllMenuItem.Click += OpenAllMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __CloseMenuItem;

        private ToolStripMenuItem _CloseMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseMenuItem != null)
                {
                    __CloseMenuItem.Click -= CloseMenuItem_Click;
                }

                __CloseMenuItem = value;
                if (__CloseMenuItem != null)
                {
                    __CloseMenuItem.Click += CloseMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __OpenMenuItem;

        private ToolStripMenuItem _OpenMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenMenuItem != null)
                {
                    __OpenMenuItem.Click -= OpenMenuItem_Click;
                }

                __OpenMenuItem = value;
                if (__OpenMenuItem != null)
                {
                    __OpenMenuItem.Click += OpenMenuItem_Click;
                }
            }
        }

        private ToolStripLabel _ClosedChannelsLabel;
    }
}