using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{

    /// <summary> A ServiceRequest view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ServiceRequestView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public ServiceRequestView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this._ServiceRequestFlagsComboBox.ComboBox.DataSource = null;
            this._ServiceRequestFlagsComboBox.ComboBox.Items.Clear();
            this._ServiceRequestFlagsComboBox.ComboBox.DataSource = Enum.GetNames( typeof( Pith.ServiceRequests ) ).ToList();
            this._ServiceRequestMaskNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._ServiceRequestMaskNumeric.NumericUpDownControl.Minimum = 0m;
            this._ServiceRequestMaskNumeric.NumericUpDownControl.Maximum = 255m;
            this._ServiceRequestMaskNumeric.NumericUpDownControl.Hexadecimal = true;
            this.InitializingComponents = false;
            this.__ToggleServiceRequestMenuItem.Name = "_ToggleServiceRequestMenuItem";
            this.__HexadecimalToolStripMenuItem.Name = "_HexadecimalToolStripMenuItem";
        }

        /// <summary> Creates a new <see cref="ServiceRequestView"/> </summary>
        /// <returns> A <see cref="ServiceRequestView"/>. </returns>
        public static ServiceRequestView Create()
        {
            ServiceRequestView view = null;
            try
            {
                view = new ServiceRequestView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Adds service request mask. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void AddServiceRequestMask()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} adding service request mask";
                Pith.ServiceRequests selectedFlag = ( Pith.ServiceRequests ) Conversions.ToInteger( this._ServiceRequestFlagsComboBox.SelectedItem );
                this._ServiceRequestMaskNumeric.Value = ( int ) this._ServiceRequestMaskNumeric.Value | ( int ) selectedFlag;
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Removes the service request mask. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RemoveServiceRequestMask()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} removing a service request mask";
                Pith.ServiceRequests selectedFlag = ( Pith.ServiceRequests ) Conversions.ToInteger( this._ServiceRequestFlagsComboBox.SelectedItem );
                this._ServiceRequestMaskNumeric.Value = ( decimal ) (( int ) this._ServiceRequestMaskNumeric.Value & ~( int ) selectedFlag);
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Toggle end of settling request enabled. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ToggleEndOfSettlingRequestEnabled()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} enabling end of settling time request";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.Device.StatusSubsystemBase.ToggleEndOfScanService( this._ServiceRequestEnabledMenuItem.Checked, this._UsingNegativeTransitionsMenuItem.Checked, ( Pith.ServiceRequests ) Conversions.ToInteger( ( int ) this._ServiceRequestMaskNumeric.Value ) );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( ServiceRequestView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: SRQ "

        /// <summary> Event handler. Called by ServiceRequestMaskAddButton for click events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ServiceRequestMaskAddButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.AddServiceRequestMask();
        }

        /// <summary>
        /// Event handler. Called by ServiceRequestMaskRemoveButton for click events.
        /// </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ServiceRequestMaskRemoveButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.RemoveServiceRequestMask();
        }

        /// <summary> Toggle service request menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ToggleServiceRequestMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ToggleEndOfSettlingRequestEnabled();
        }

        /// <summary> Hexadecimal tool strip menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void HexadecimalToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this._ServiceRequestMaskNumeric.NumericUpDownControl.Hexadecimal = this._HexadecimalToolStripMenuItem.Checked;
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
