using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A trigger subsystem user interface. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class TriggerView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public TriggerView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
            this.__InitiateButton.Name = "_InitiateButton";
            this.__AbortButton.Name = "_AbortButton";
            this.__SendBusTriggerButton.Name = "_SendBusTriggerButton";
        }

        /// <summary> Creates a new <see cref="TriggerView"/> </summary>
        /// <returns> A <see cref="TriggerView"/>. </returns>
        public static TriggerView Create()
        {
            TriggerView view = null;
            try
            {
                view = new TriggerView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Adds a menu item. </summary>
        /// <param name="item"> The item. </param>
        public void AddMenuItem( ToolStripMenuItem item )
        {
            _ = this._SubsystemSplitButton.DropDownItems.Add( item );
        }

        /// <summary> Applies the trigger plan settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} applying arm 1 subsystem instrument settings";
                this._ArmLayer1View.ApplySettings();
                activity = $"{this.Device.ResourceNameCaption} applying arm 2 subsystem instrument settings";
                this._ArmLayer2View.ApplySettings();
                activity = $"{this.Device.ResourceNameCaption} applying trigger subsystem instrument settings";
                this._TriggerLayer1View.ApplySettings();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads instrument settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading arm 1 subsystem instrument settings";
                this._ArmLayer1View.ReadSettings();
                activity = $"{this.Device.ResourceNameCaption} reading arm 2 subsystem instrument settings";
                this._ArmLayer2View.ReadSettings();
                activity = $"{this.Device.ResourceNameCaption} reading trigger subsystem instrument settings";
                this._TriggerLayer1View.ReadSettings();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Initiates the trigger plan. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void Initiate()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing execution state";
                this.Device.ClearExecutionState();
                activity = $"{this.Device.ResourceNameCaption} initiating trigger plan";
                this._TriggerLayer1View.TriggerSubsystem.StartElapsedStopwatch();
                this._TriggerLayer1View.TriggerSubsystem.Initiate();
                this._TriggerLayer1View.TriggerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
                try
                {
                    activity = $"{this.Device.ResourceNameCaption} aborting trigger plan";
                    this._TriggerLayer1View.TriggerSubsystem.Abort();
                }
                catch
                {
                    this.Device.Session.StatusPrompt = $"failed {activity}";
                    activity = this.PublishException( activity, ex );
                    _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
                }
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Aborts the trigger plan. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void Abort()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} aborting trigger plan";
                this._TriggerLayer1View.TriggerSubsystem.StartElapsedStopwatch();
                this._TriggerLayer1View.TriggerSubsystem.Abort();
                this._TriggerLayer1View.TriggerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Sends the bus trigger. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void SendBusTrigger()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} sending bus trigger";
                this.Device.Session.AssertTrigger();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CUSTOM ACTIONS "

        /// <summary> Initiate wait read. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void InitiateWaitRead()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing execution state";
                this.Device.ClearExecutionState();
                activity = $"{this.Device.ResourceNameCaption} initiating wait read";

                // Me.Device.ClearExecutionState()
                // set the service request
                // Me.Device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
                // Me.Device.StatusSubsystem.EnableServiceRequest(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)
                // Me.Device.Session.Write("*SRE 1") ' Set MSB bit of SRE register
                // Me.Device.Session.Write("stat:meas:ptr 32767; ntr 0; enab 512") ' Set all PTR bits and clear all NTR bits for measurement events Set Buffer Full bit of Measurement
                // Me.Device.Session.Write(":trac:feed calc") ' Select Calculate as reading source
                // Me.Device.Session.Write(":trac:poin 10")   ' Set buffer size to 10 points 
                // Me.Device.Session.Write(":trac:egr full")  ' Select Full element group

                // trigger the initiation of the measurement letting the triggering or service request do the rest.
                activity = $"{this.Device.ResourceNameCaption} Initiating meter";
                _ = this.PublishVerbose( $"{activity};. " );
                this._TriggerLayer1View.TriggerSubsystem.Initiate();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            this._ArmLayer1View.AssignDevice( value );
            this._ArmLayer2View.AssignDevice( value );
            this._TriggerLayer1View.AssignDevice( value );
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem">   The subsystem. </param>
        /// <param name="layerNumber"> The layer number. </param>
        protected virtual void BindSubsystem( ArmLayerSubsystemBase subsystem, int layerNumber )
        {
            switch ( layerNumber )
            {
                case 1:
                    {
                        this._ArmLayer1View.BindSubsystem( subsystem, layerNumber );
                        break;
                    }

                case 2:
                    {
                        this._ArmLayer2View.BindSubsystem( subsystem, layerNumber );
                        break;
                    }
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem">   The subsystem. </param>
        /// <param name="layerNumber"> The layer number. </param>
        protected virtual void BindSubsystem( TriggerSubsystemBase subsystem, int layerNumber )
        {
            this._TriggerLayer1View.BindSubsystem( subsystem, layerNumber );
            if ( subsystem is null )
            {
                this._TriggerLayer1View.PropertyChanged -= this.TriggerLayer1ViewPropertyChanged;
            }
            else
            {
                this._TriggerLayer1View.PropertyChanged += this.TriggerLayer1ViewPropertyChanged;
                this.HandlePropertyChanged( this._TriggerLayer1View, nameof( TriggerLayerView.Count ) );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER LAYER 1 VIEW "

        /// <summary> Gets the arm layer 1 source. </summary>
        /// <value> The arm layer 1 source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmSources ArmLayer1Source => this._ArmLayer1View.Source;

        /// <summary> Gets the arm layer 2 source. </summary>
        /// <value> The arm layer 2 source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmSources ArmLayer2Source => this._ArmLayer2View.Source;

        /// <summary> Gets the trigger layer source. </summary>
        /// <value> The trigger layer source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSources TriggerLayerSource => this._TriggerLayer1View.Source;

        /// <summary> Gets the number of triggers. </summary>
        /// <value> The number of triggers. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int TriggerCount => this._TriggerLayer1View.Count;

        /// <summary> Handle the TriggerLayer1 view property changed event. </summary>
        /// <param name="view">         The view. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( TriggerLayerView view, string propertyName )
        {
            if ( view is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( TriggerLayerView.Source ):
                    {
                        this._SendBusTriggerButton.Enabled = view.Source == TriggerSources.Bus;
                        break;
                    }
            }
        }

        /// <summary> TriggerLayer1 view property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerLayer1ViewPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( TriggerLayerView )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerLayer1ViewPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerLayer1ViewPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerLayerView, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Applies the settings tool strip menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySettingsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads settings tool strip menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ReadSettings();
        }

        /// <summary> Initiate click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void InitiateButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.Initiate();
        }

        /// <summary> Abort button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AbortButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.Abort();
        }

        /// <summary> Sends the bus trigger button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SendBusTriggerButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.SendBusTrigger();
        }

        #endregion

        #region " TALKER "

        /// <summary> Assigns talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( Core.ITraceMessageTalker talker )
        {
            this._ArmLayer1View.AssignTalker( talker );
            this._ArmLayer2View.AssignTalker( talker );
            this._TriggerLayer1View.AssignTalker( talker );
            // assigned last as this identifies all talkers.
            base.AssignTalker( talker );
        }

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
