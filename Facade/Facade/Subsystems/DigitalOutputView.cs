using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A Digital Output Subsystem user interface. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class DigitalOutputView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public DigitalOutputView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._DigitalOutputLine1View.LineIdentity = _StrobeLineIdentity;
            this._DigitalOutputLine1View.LineName = _StrobeLineIdentity;
            this._DigitalOutputLine1View.LineNumber = My.Settings.Default.StrobeLineNumber;
            this._DigitalOutputLine1View.PulseWidth = My.Settings.Default.StrobeDuration;
            this._DigitalOutputLine1View.ActiveLevel = DigitalActiveLevels.Low;
            this._DigitalOutputLine2View.LineIdentity = _BinLineIdentity;
            this._DigitalOutputLine2View.LineName = _BinLineIdentity;
            this._DigitalOutputLine2View.LineNumber = My.Settings.Default.BinLineNumber;
            this._DigitalOutputLine2View.PulseWidth = My.Settings.Default.BinDuration;
            this._DigitalOutputLine2View.ActiveLevel = DigitalActiveLevels.Low;
            this._DigitalOutputLine3View.LineIdentity = _ThirdLineIdentity;
            this._DigitalOutputLine3View.LineName = "Line";
            this.__ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
            this.__StrobeButton.Name = "_StrobeButton";
        }

        /// <summary> Creates a new <see cref="TriggerView"/> </summary>
        /// <returns> A <see cref="TriggerView"/>. </returns>
        public static TriggerView Create()
        {
            TriggerView view = null;
            try
            {
                view = new TriggerView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> Adds a menu item. </summary>
        /// <param name="item"> The item. </param>
        public void AddMenuItem( ToolStripMenuItem item )
        {
            _ = this._SubsystemSplitButton.DropDownItems.Add( item );
        }

        /// <summary> Applies the trigger plan settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} applying digital output 1 subsystem instrument settings";
                this._DigitalOutputLine1View.ApplySettings();
                activity = $"{this.Device.ResourceNameCaption} applying digital output 2 subsystem instrument settings";
                this._DigitalOutputLine2View.ApplySettings();
                activity = $"{this.Device.ResourceNameCaption} applying digital output 3 Subsystem instrument settings";
                this._DigitalOutputLine3View.ApplySettings();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads instrument settings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading digital output 1 subsystem instrument settings";
                this._DigitalOutputLine1View.ReadSettings();
                activity = $"{this.Device.ResourceNameCaption} reading digital output 2 subsystem instrument settings";
                this._DigitalOutputLine2View.ReadSettings();
                activity = $"{this.Device.ResourceNameCaption} reading digital output 3 Subsystem instrument settings";
                this._DigitalOutputLine3View.ReadSettings();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Gets or sets the strobe line number. </summary>
        /// <value> The strobe line number. </value>
        public int StrobeLineNumber
        {
            get => this._DigitalOutputLine1View.LineNumber;

            set => this._DigitalOutputLine1View.LineNumber = value;
        }

        /// <summary> Gets or sets the strobe pulse width. </summary>
        /// <value> The strobe pulse width. </value>
        public TimeSpan StrobeDuration
        {
            get => this._DigitalOutputLine1View.PulseWidth;

            set => this._DigitalOutputLine1View.PulseWidth = value;
        }

        /// <summary> Gets or sets the bin line number. </summary>
        /// <value> The bin line number. </value>
        public int BinLineNumber
        {
            get => this._DigitalOutputLine1View.LineNumber;

            set => this._DigitalOutputLine1View.LineNumber = value;
        }

        /// <summary> Gets or sets the bin pulse width. </summary>
        /// <value> The bin pulse width. </value>
        public TimeSpan BinDuration
        {
            get => this._DigitalOutputLine1View.PulseWidth;

            set => this._DigitalOutputLine1View.PulseWidth = value;
        }

        /// <summary> Outputs a strobe pulse. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual void Strobe()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing execution state";
                this.Device.ClearExecutionState();
                activity = $"{this.Device.ResourceNameCaption} initiating trigger plan";
                this._DigitalOutputLine3View.DigitalOutputSubsystem.StartElapsedStopwatch();
                this._DigitalOutputLine3View.DigitalOutputSubsystem.Strobe( this.StrobeLineNumber, this.StrobeDuration, this.BinLineNumber, this.BinDuration );
                this._DigitalOutputLine3View.DigitalOutputSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            this._DigitalOutputLine1View.AssignDevice( value );
            this._DigitalOutputLine2View.AssignDevice( value );
            this._DigitalOutputLine3View.AssignDevice( value );
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem">   The subsystem. </param>
        protected virtual void BindSubsystem( DigitalOutputSubsystemBase subsystem )
        {
            var lineNumber = default( int );
            var pulseWidth = default( TimeSpan );
            var activeLevel = default( DigitalActiveLevels );
            var views = new DigitalOutputLineView[] { this._DigitalOutputLine1View, this._DigitalOutputLine2View, this._DigitalOutputLine3View };
            foreach ( DigitalOutputLineView digitalLineView in views )
            {
                switch ( digitalLineView.LineIdentity ?? "" )
                {
                    case _StrobeLineIdentity:
                        {
                            lineNumber = My.Settings.Default.StrobeLineNumber;
                            pulseWidth = My.Settings.Default.StrobeDuration;
                            activeLevel = DigitalActiveLevels.Low;
                            break;
                        }

                    case _BinLineIdentity:
                        {
                            lineNumber = My.Settings.Default.BinLineNumber;
                            pulseWidth = My.Settings.Default.BinDuration;
                            activeLevel = DigitalActiveLevels.Low;
                            break;
                        }

                    case _ThirdLineIdentity:
                        {
                            lineNumber = 2;
                            pulseWidth = TimeSpan.FromMilliseconds( 10d );
                            activeLevel = DigitalActiveLevels.Low;
                            break;
                        }
                }

                digitalLineView.BindSubsystem( subsystem, lineNumber );
                if ( subsystem is null )
                {
                    digitalLineView.PropertyChanged -= this.DigitalOutputLineViewPropertyChanged;
                }
                else
                {
                    digitalLineView.PropertyChanged += this.DigitalOutputLineViewPropertyChanged;
                    digitalLineView.LineNumber = lineNumber;
                    digitalLineView.PulseWidth = pulseWidth;
                    digitalLineView.ActiveLevel = activeLevel;
                    this.HandlePropertyChanged( digitalLineView, nameof( DigitalOutputLineView.LineNumber ) );
                }
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DIGITAL OUTPUT LINE VIEW "

        /// <summary> The strobe line identity. </summary>
        private const string _StrobeLineIdentity = "Strobe";

        /// <summary> The bin line identity. </summary>
        private const string _BinLineIdentity = "Bin";
        private const string _ThirdLineIdentity = "3";

        /// <summary> Handle the Digital Output Line view property changed event. </summary>
        /// <remarks> David, 2020-11-13. </remarks>
        /// <param name="view">         The view. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( DigitalOutputLineView view, string propertyName )
        {
            if ( view is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( DigitalOutputLineView.LineNumber ):
                    {
                        switch ( view.LineIdentity ?? "" )
                        {
                            case _StrobeLineIdentity:
                                {
                                    My.Settings.Default.StrobeLineNumber = view.LineNumber;
                                    break;
                                }

                            case _BinLineIdentity:
                                {
                                    My.Settings.Default.BinLineNumber = view.LineNumber;
                                    break;
                                }

                            case _ThirdLineIdentity:
                                {
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( DigitalOutputLineView.PulseWidth ):
                    {
                        switch ( view.LineIdentity ?? "" )
                        {
                            case _StrobeLineIdentity:
                                {
                                    My.Settings.Default.StrobeDuration = view.PulseWidth;
                                    break;
                                }

                            case _BinLineIdentity:
                                {
                                    My.Settings.Default.BinDuration = view.PulseWidth;
                                    break;
                                }

                            case _ThirdLineIdentity:
                                {
                                    break;
                                }
                        }

                        break;
                    }
            }
        }

        /// <summary> Digital output line view property changed. </summary>
        /// <remarks> David, 2020-11-13. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DigitalOutputLineViewPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            DigitalOutputLineView digitalLineView = sender as DigitalOutputLineView;
            string activity = $"handling {nameof( DigitalOutputLineView )}.{e.PropertyName} change";
            try
            {
                if ( digitalLineView is object && e is object )
                {
                }
                else if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.DigitalOutputLineViewPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.DigitalOutputLineViewPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( digitalLineView, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Applies the settings tool strip menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySettingsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads settings tool strip menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ReadSettings();
        }

        /// <summary> Initiate click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void InitiateButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.Strobe();
        }

        #endregion

        #region " TALKER "

        /// <summary> Assigns talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( Core.ITraceMessageTalker talker )
        {
            this._DigitalOutputLine1View.AssignTalker( talker );
            this._DigitalOutputLine2View.AssignTalker( talker );
            this._DigitalOutputLine3View.AssignTalker( talker );
            // assigned last as this identifies all talkers.
            base.AssignTalker( talker );
        }

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
