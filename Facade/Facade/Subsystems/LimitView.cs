using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A Limit Subsystem view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class LimitView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public LimitView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._UpperLimitBitPatternNumeric.NumericUpDownControl.Minimum = 1m;
            this._UpperLimitBitPatternNumeric.NumericUpDownControl.Maximum = 63m;
            this._UpperLimitBitPatternNumeric.NumericUpDownControl.Value = 16m;
            this._LowerLimitBitPatternNumeric.NumericUpDownControl.Minimum = 1m;
            this._LowerLimitBitPatternNumeric.NumericUpDownControl.Maximum = 63m;
            this._LowerLimitBitPatternNumeric.NumericUpDownControl.Value = 48m;
            this._UpperLimitDecimalsNumeric.NumericUpDownControl.Minimum = 0m;
            this._UpperLimitDecimalsNumeric.NumericUpDownControl.Maximum = 10m;
            this._UpperLimitDecimalsNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._UpperLimitDecimalsNumeric.NumericUpDownControl.Value = 3m;
            this._UpperLimitNumeric.NumericUpDownControl.Minimum = 0m;
            this._UpperLimitNumeric.NumericUpDownControl.Maximum = 5000000m;
            this._UpperLimitNumeric.NumericUpDownControl.DecimalPlaces = 3;
            this._UpperLimitNumeric.NumericUpDownControl.Value = 9m;
            this._LowerLimitNumeric.NumericUpDownControl.Minimum = 0m;
            this._LowerLimitNumeric.NumericUpDownControl.Maximum = 5000000m;
            this._LowerLimitNumeric.NumericUpDownControl.DecimalPlaces = 3;
            this._LowerLimitNumeric.NumericUpDownControl.Value = 90m;
            this._LowerLimitDecimalsNumeric.NumericUpDownControl.Minimum = 0m;
            this._LowerLimitDecimalsNumeric.NumericUpDownControl.Maximum = 10m;
            this._LowerLimitDecimalsNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._LowerLimitDecimalsNumeric.NumericUpDownControl.Value = 3m;
            this._UpperLimitNumeric.NumericUpDownControl.DecimalPlaces = ( int ) this._UpperLimitDecimalsNumeric.Value;
            this._LowerLimitNumeric.NumericUpDownControl.DecimalPlaces = ( int ) this._LowerLimitDecimalsNumeric.Value;
            this.__ApplySettingsMenuItem.Name = "_ApplySettingsMenuItem";
            this.__ReadSettingsMenuItem.Name = "_ReadSettingsMenuItem";
            this.__PerformLimitStripMenuItem.Name = "_PerformLimitStripMenuItem";
            this.__ReadLimitTestMenuItem.Name = "_ReadLimitTestMenuItem";
            this.__LimitEnabledToggleButton.Name = "_LimitEnabledToggleButton";
            this.__AutoClearToggleButton.Name = "_AutoClearToggleButton";
            this.__LimitFailedButton.Name = "_LimitFailedButton";
            this.__UpperLimitDecimalsNumeric.Name = "_UpperLimitDecimalsNumeric";
            this.__UpperLimitBitPatternNumericButton.Name = "_UpperLimitBitPatternNumericButton";
            this.__LowerLimitDecimalsNumeric.Name = "_LowerLimitDecimalsNumeric";
            this.__LowerLimitBitPatternNumericButton.Name = "_LowerLimitBitPatternNumericButton";
        }

        /// <summary> Creates a new LimitView. </summary>
        /// <returns> A LimitView. </returns>
        public static LimitView Create()
        {
            LimitView view = null;
            try
            {
                view = new LimitView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> The limit number. </summary>
        private int _LimitNumber;

        /// <summary> Gets or sets the limit number. </summary>
        /// <value> The limit number. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int LimitNumber
        {
            get => this._LimitNumber;

            set {
                if ( value != this.LimitNumber )
                {
                    this._LimitNumber = value;
                    this._SubsystemSplitButton.Text = $"Limit{value}";
                }
            }
        }

        /// <summary> Applies the settings onto the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying Binning subsystem {this._SubsystemSplitButton.Text} state";
                _ = this.PublishInfo( $"{activity};. " );
                this.BinningSubsystem.StartElapsedStopwatch();
                if ( this.LimitNumber == 1 )
                {
                    _ = this.BinningSubsystem.ApplyLimit1AutoClear( this._AutoClearToggleButton.CheckState == CheckState.Checked );
                    _ = this.BinningSubsystem.ApplyLimit1Enabled( this._LimitEnabledToggleButton.CheckState == CheckState.Checked );
                    _ = this.BinningSubsystem.ApplyLimit1LowerLevel( ( double ) this._LowerLimitNumeric.Value );
                    _ = this.BinningSubsystem.ApplyLimit1LowerSource( ( int ) this._LowerLimitBitPatternNumeric.Value );
                    _ = this.BinningSubsystem.ApplyLimit1UpperLevel( ( double ) this._UpperLimitNumeric.Value );
                    _ = this.BinningSubsystem.ApplyLimit1UpperSource( ( int ) this._LowerLimitBitPatternNumeric.Value );
                }
                else
                {
                    _ = this.BinningSubsystem.ApplyLimit2AutoClear( this._AutoClearToggleButton.CheckState == CheckState.Checked );
                    _ = this.BinningSubsystem.ApplyLimit2Enabled( this._LimitEnabledToggleButton.CheckState == CheckState.Checked );
                    _ = this.BinningSubsystem.ApplyLimit2LowerLevel( ( double ) this._LowerLimitNumeric.Value );
                    _ = this.BinningSubsystem.ApplyLimit2LowerSource( ( int ) this._LowerLimitBitPatternNumeric.Value );
                    _ = this.BinningSubsystem.ApplyLimit2UpperLevel( ( double ) this._UpperLimitNumeric.Value );
                    _ = this.BinningSubsystem.ApplyLimit2UpperSource( ( int ) this._LowerLimitBitPatternNumeric.Value );
                }

                this.BinningSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads settings from the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} reading Binning subsystem {this._SubsystemSplitButton.Text} state";
                _ = this.PublishInfo( $"{activity};. " );
                ReadSettings( this.BinningSubsystem, this.LimitNumber );
                this.ApplyPropertyChanged( this.BinningSubsystem );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Preform a limit test and read the fail state. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void PerformLimitTest()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.BinningSubsystem.StartElapsedStopwatch();
                this.BinningSubsystem.Immediate();
                _ = this.BinningSubsystem.QueryLimitsFailed();
                _ = this.BinningSubsystem.QueryLimit1Failed();
                _ = this.BinningSubsystem.QueryLimit2Failed();
                this.BinningSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> read limit test result. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadLimitTestState()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.BinningSubsystem.StartElapsedStopwatch();
                _ = this.LimitNumber == 1 ? this.BinningSubsystem.QueryLimit1Failed() : this.BinningSubsystem.QueryLimit2Failed();

                _ = this.Device.StatusSubsystemBase.QueryMeasurementEventStatus();
                this.BinningSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }


        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( LimitView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " BINNING "

        /// <summary> Gets or sets the binning subsystem. </summary>
        /// <value> The binning subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public BinningSubsystemBase BinningSubsystem { get; private set; }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem">   The subsystem. </param>
        /// <param name="limitNumber"> The limit number. </param>
        public void BindSubsystem( BinningSubsystemBase subsystem, int limitNumber )
        {
            if ( this.BinningSubsystem is object )
            {
                this.BindSubsystem( false, this.BinningSubsystem );
                this.BinningSubsystem = null;
            }

            this.LimitNumber = limitNumber;
            this.BinningSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.BinningSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, BinningSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.BinningSubsystemPropertyChanged;
                // must not read setting when biding because the instrument may be locked or in a trigger mode
                // The bound values should be sent when binding or when applying propert change.
                // ReadSettings( this.BinningSubsystem, this.LimitNumber );
                // this.ReadLimitTestState();
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.BinningSubsystemPropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( BinningSubsystemBase subsystem )
        {
            switch ( this.LimitNumber )
            {
                case 1:
                    {
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit1AutoClear ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit1Enabled ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit1Failed ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit1LowerLevel ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit1LowerSource ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit1UpperLevel ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit1UpperSource ) );
                        break;
                    }

                case 2:
                    {
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit2AutoClear ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit2Enabled ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit2Failed ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit2LowerLevel ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit2LowerSource ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit2UpperLevel ) );
                        this.HandlePropertyChanged( subsystem, nameof( BinningSubsystemBase.Limit2UpperSource ) );
                        break;
                    }

                default:
                    {
                        throw new InvalidOperationException( $"Limit number {this.LimitNumber} must be either 1 or 2" );
                    }
            }
        }

        /// <summary> Reads settings from the instrument. </summary>
        /// <param name="subsystem">   The subsystem. </param>
        /// <param name="limitNumber"> The limit number. </param>
        private static void ReadSettings( BinningSubsystemBase subsystem, int limitNumber )
        {
            subsystem.StartElapsedStopwatch();
            if ( limitNumber == 1 )
            {
                _ = subsystem.QueryLimit1AutoClear();
                _ = subsystem.QueryLimit1Enabled();
                _ = subsystem.QueryLimit1LowerLevel();
                _ = subsystem.QueryLimit1LowerSource();
                _ = subsystem.QueryLimit1UpperLevel();
                _ = subsystem.QueryLimit1UpperSource();
            }
            else
            {
                _ = subsystem.QueryLimit2AutoClear();
                _ = subsystem.QueryLimit2Enabled();
                _ = subsystem.QueryLimit2LowerLevel();
                _ = subsystem.QueryLimit2LowerSource();
                _ = subsystem.QueryLimit2UpperLevel();
                _ = subsystem.QueryLimit2UpperSource();
            }

            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Handle the Calculate subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( BinningSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            if ( this.LimitNumber == 1 )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( BinningSubsystemBase.Limit1AutoClear ):
                        {
                            this._AutoClearToggleButton.CheckState = subsystem.Limit1AutoClear.ToCheckState();
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit1Enabled ):
                        {
                            this._LimitEnabledToggleButton.CheckState = subsystem.Limit1Enabled.ToCheckState();
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit1Failed ):
                        {
                            this._LimitFailedButton.CheckState = subsystem.Limit1Failed.ToCheckState();
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit1LowerLevel ):
                        {
                            if ( subsystem.Limit1LowerLevel.HasValue )
                                this._LowerLimitNumeric.Value = ( decimal ) subsystem.Limit1LowerLevel.Value;
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit1LowerSource ):
                        {
                            if ( subsystem.Limit1LowerSource.HasValue )
                                this._LowerLimitBitPatternNumeric.Value = subsystem.Limit1LowerSource.Value;
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit1UpperLevel ):
                        {
                            if ( subsystem.Limit1UpperLevel.HasValue )
                                this._UpperLimitNumeric.Value = ( decimal ) subsystem.Limit1UpperLevel.Value;
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit1UpperSource ):
                        {
                            if ( subsystem.Limit1UpperSource.HasValue )
                                this._UpperLimitBitPatternNumeric.Value = subsystem.Limit1UpperSource.Value;
                            break;
                        }
                }
            }
            else
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( BinningSubsystemBase.Limit2AutoClear ):
                        {
                            this._AutoClearToggleButton.CheckState = subsystem.Limit2AutoClear.ToCheckState();
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit2Enabled ):
                        {
                            this._LimitEnabledToggleButton.CheckState = subsystem.Limit2Enabled.ToCheckState();
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit2Failed ):
                        {
                            this._LimitFailedButton.CheckState = subsystem.Limit2Failed.ToCheckState();
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit2LowerLevel ):
                        {
                            if ( subsystem.Limit2LowerLevel.HasValue )
                                this._LowerLimitNumeric.Value = ( decimal ) subsystem.Limit2LowerLevel.Value;
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit2LowerSource ):
                        {
                            if ( subsystem.Limit2LowerSource.HasValue )
                                this._LowerLimitBitPatternNumeric.Value = subsystem.Limit2LowerSource.Value;
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit2UpperLevel ):
                        {
                            if ( subsystem.Limit2UpperLevel.HasValue )
                                this._UpperLimitNumeric.Value = ( decimal ) subsystem.Limit2UpperLevel.Value;
                            break;
                        }

                    case nameof( BinningSubsystemBase.Limit2UpperSource ):
                        {
                            if ( subsystem.Limit2UpperSource.HasValue )
                                this._UpperLimitBitPatternNumeric.Value = subsystem.Limit2UpperSource.Value;
                            break;
                        }
                }
            }
        }

        /// <summary> Binning subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void BinningSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( BinningSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.BinningSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.BinningSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as BinningSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: CALCULATE "

        /// <summary> Automatic clear button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AutoClearButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = $"Auto Clear: {button.CheckState.ToCheckStateCaption( "On", "Off", "?" )}";
            }
        }

        /// <summary> Limit enabled toggle button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LimitEnabledToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = button.CheckState.ToCheckStateCaption( "Enabled", "Disabled", "Enabled ?" );
            }
        }

        /// <summary> Limit failed toggle button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LimitFailedToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = button.CheckState.ToCheckStateCaption( "Fail", "Pass", "P/F ?" );
            }
        }

        /// <summary> Lower bit pattern numeric button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LowerBitPatternNumericButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                this._LowerLimitBitPatternNumeric.NumericUpDownControl.Hexadecimal = button.Checked;
                button.Text = $"Source {(button.Checked ? "0x" : "0d")}";
            }
        }

        /// <summary> Upper limit bit pattern numeric button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void UpperLimitBitPatternNumericButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripButton button )
            {
                button.Text = $"Source {(button.Checked ? "0x" : "0d")}";
                this._UpperLimitBitPatternNumeric.NumericUpDownControl.Hexadecimal = button.Checked;
            }
        }

        /// <summary> Upper limit decimals numeric value changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void UpperLimitDecimalsNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this._UpperLimitNumeric is object )
            {
                this._UpperLimitNumeric.NumericUpDownControl.DecimalPlaces = ( int ) this._UpperLimitDecimalsNumeric.Value;
            }
        }

        /// <summary> Lower limit decimals numeric value changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LowerLimitDecimalsNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this._LowerLimitNumeric is object )
            {
                this._LowerLimitNumeric.NumericUpDownControl.DecimalPlaces = ( int ) this._LowerLimitDecimalsNumeric.Value;
            }
        }

        /// <summary> Applies the settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplySettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads settings menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ReadSettings();
        }

        /// <summary> Performs the limit strip menu item click action. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PerformLimitStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.PerformLimitTest();
        }

        /// <summary> Reads limit test menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadLimitTestMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ReadLimitTestState();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
