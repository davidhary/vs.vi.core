﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class MeterView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MeterView));
            _ReadingToolStrip = new ToolStrip();
            _ReadSplitButton = new ToolStripLabel();
            _ReadingElementTypesComboBoxLabel = new ToolStripLabel();
            __ReadingElementTypesComboBox = new ToolStripComboBox();
            __ReadingElementTypesComboBox.SelectedIndexChanged += new EventHandler(ReadingElementTypesComboBox_SelectedIndexChanged);
            __MeasureValueButton = new ToolStripButton();
            __MeasureValueButton.Click += new EventHandler(MeasureValueButton_Click);
            _FunctionConfigurationToolStrip = new ToolStrip();
            _FunctionLabel = new ToolStripLabel();
            _ApertureNumericLabel = new ToolStripLabel();
            _ApertureNumeric = new Core.Controls.ToolStripNumericUpDown();
            _SenseRangeNumericLabel = new ToolStripLabel();
            _SenseRangeNumeric = new Core.Controls.ToolStripNumericUpDown();
            __AutoRangeToggleButton = new ToolStripButton();
            __AutoRangeToggleButton.CheckStateChanged += new EventHandler(AutoRangeToggleButton_CheckStateChanged);
            __AutoZeroToggleButton = new ToolStripButton();
            __AutoZeroToggleButton.CheckStateChanged += new EventHandler(AutoZeroToggleButton_CheckStateChanged);
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplyFunctionModeMenuItem = new ToolStripMenuItem();
            __ApplyFunctionModeMenuItem.Click += new EventHandler(ApplyFunctionModeButton_Click);
            __ReadFunctionModeMenuItem = new ToolStripMenuItem();
            __ReadFunctionModeMenuItem.Click += new EventHandler(ReadFunctionModeMenuItem_Click);
            _MeasureOptionsMenuItem = new ToolStripMenuItem();
            __MeasureImmediateMenuItem = new ToolStripMenuItem();
            __MeasureImmediateMenuItem.Click += new EventHandler(MeasureImmediateMenuItem_Click);
            _FetchOnMeasurementEventMenuItem = new ToolStripMenuItem();
            _AutoInitiateMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsToolStripMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsToolStripMenuItem_Click);
            __SenseFunctionComboBox = new ToolStripComboBox();
            __SenseFunctionComboBox.SelectedIndexChanged += new EventHandler(SenseFunctionComboBox_SelectedIndexChanged);
            __ApplyFunctionModeButton = new ToolStripButton();
            __ApplyFunctionModeButton.Click += new EventHandler(ApplyFunctionModeButton_Click);
            _FilterToolStrip = new ToolStrip();
            _FilterLabel = new ToolStripLabel();
            __FilterEnabledToggleButton = new ToolStripButton();
            __FilterEnabledToggleButton.CheckStateChanged += new EventHandler(FilterEnabledToggleButton_CheckStateChanged);
            _FilterCountNumericLabel = new ToolStripLabel();
            _FilterCountNumeric = new Core.Controls.ToolStripNumericUpDown();
            _FilterWindowNumericLabel = new ToolStripLabel();
            _FilterWindowNumeric = new Core.Controls.ToolStripNumericUpDown();
            __WindowTypeToggleButton = new ToolStripButton();
            __WindowTypeToggleButton.CheckStateChanged += new EventHandler(WindowTypeToggleButton_CheckStateChanged);
            _InfoToolStrip = new ToolStrip();
            _InfoLabel = new ToolStripLabel();
            __AutoDelayToggleButton = new ToolStripButton();
            __AutoDelayToggleButton.CheckStateChanged += new EventHandler(AutoDelayToggleButton_CheckStateChanged);
            __OpenDetectorToggleButton = new ToolStripButton();
            __OpenDetectorToggleButton.CheckStateChanged += new EventHandler(OpenDetectorToggleButton_CheckStateChanged);
            __TerminalStateReadButton = new ToolStripButton();
            __TerminalStateReadButton.CheckStateChanged += new EventHandler(TerminalStateReadButton_CheckStateChanged);
            __TerminalStateReadButton.Click += new EventHandler(TerminalStateReadButton_Click);
            _ResolutionDigitsNumericLabel = new ToolStripLabel();
            _ResolutionDigitsNumeric = new Core.Controls.ToolStripNumericUpDown();
            _ToolStripPanel = new ToolStripPanel();
            _ReadingToolStrip.SuspendLayout();
            _FunctionConfigurationToolStrip.SuspendLayout();
            _SubsystemToolStrip.SuspendLayout();
            _FilterToolStrip.SuspendLayout();
            _InfoToolStrip.SuspendLayout();
            _ToolStripPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _ReadingToolStrip
            // 
            _ReadingToolStrip.BackColor = System.Drawing.Color.Transparent;
            _ReadingToolStrip.Dock = DockStyle.None;
            _ReadingToolStrip.GripMargin = new Padding(0);
            _ReadingToolStrip.Items.AddRange(new ToolStripItem[] { _ReadSplitButton, _ReadingElementTypesComboBoxLabel, __ReadingElementTypesComboBox, __MeasureValueButton });
            _ReadingToolStrip.Location = new System.Drawing.Point(0, 25);
            _ReadingToolStrip.Name = "_ReadingToolStrip";
            _ReadingToolStrip.Size = new System.Drawing.Size(413, 25);
            _ReadingToolStrip.Stretch = true;
            _ReadingToolStrip.TabIndex = 1;
            _ReadingToolStrip.Text = "Meter Tool Strip";
            // 
            // _ReadSplitButton
            // 
            _ReadSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _ReadSplitButton.DoubleClickEnabled = true;
            _ReadSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ReadSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ReadSplitButton.Margin = new Padding(6, 1, 0, 2);
            _ReadSplitButton.Name = "_ReadSplitButton";
            _ReadSplitButton.Size = new System.Drawing.Size(34, 22);
            _ReadSplitButton.Text = "Read";
            // 
            // _ReadingElementTypesComboBoxLabel
            // 
            _ReadingElementTypesComboBoxLabel.Name = "_ReadingElementTypesComboBoxLabel";
            _ReadingElementTypesComboBoxLabel.Size = new System.Drawing.Size(34, 22);
            _ReadingElementTypesComboBoxLabel.Text = "Type:";
            // 
            // _ReadingElementTypesComboBox
            // 
            __ReadingElementTypesComboBox.Name = "__ReadingElementTypesComboBox";
            __ReadingElementTypesComboBox.Size = new System.Drawing.Size(195, 25);
            __ReadingElementTypesComboBox.ToolTipText = "Select reading type to display";
            // 
            // _MeasureValueButton
            // 
            __MeasureValueButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __MeasureValueButton.Image = (System.Drawing.Image)resources.GetObject("_MeasureValueButton.Image");
            __MeasureValueButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __MeasureValueButton.Name = "__MeasureValueButton";
            __MeasureValueButton.Size = new System.Drawing.Size(56, 22);
            __MeasureValueButton.Text = "Measure";
            __MeasureValueButton.ToolTipText = "Initiate a measurement and fetch value based on the fetch options";
            // 
            // _FunctionConfigurationToolStrip
            // 
            _FunctionConfigurationToolStrip.BackColor = System.Drawing.Color.Transparent;
            _FunctionConfigurationToolStrip.Dock = DockStyle.None;
            _FunctionConfigurationToolStrip.GripMargin = new Padding(0);
            _FunctionConfigurationToolStrip.Items.AddRange(new ToolStripItem[] { _FunctionLabel, _ApertureNumericLabel, _ApertureNumeric, _SenseRangeNumericLabel, _SenseRangeNumeric, __AutoRangeToggleButton, __AutoZeroToggleButton });
            _FunctionConfigurationToolStrip.Location = new System.Drawing.Point(0, 50);
            _FunctionConfigurationToolStrip.Name = "_FunctionConfigurationToolStrip";
            _FunctionConfigurationToolStrip.Size = new System.Drawing.Size(413, 28);
            _FunctionConfigurationToolStrip.Stretch = true;
            _FunctionConfigurationToolStrip.TabIndex = 2;
            // 
            // _FunctionLabel
            // 
            _FunctionLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _FunctionLabel.Margin = new Padding(6, 1, 0, 2);
            _FunctionLabel.Name = "_FunctionLabel";
            _FunctionLabel.Size = new System.Drawing.Size(28, 25);
            _FunctionLabel.Text = "Volt";
            _FunctionLabel.ToolTipText = "Selected function mode";
            // 
            // _ApertureNumericLabel
            // 
            _ApertureNumericLabel.Name = "_ApertureNumericLabel";
            _ApertureNumericLabel.Size = new System.Drawing.Size(56, 25);
            _ApertureNumericLabel.Text = "Aperture:";
            // 
            // _ApertureNumeric
            // 
            _ApertureNumeric.Name = "_ApertureNumeric";
            _ApertureNumeric.Size = new System.Drawing.Size(41, 25);
            _ApertureNumeric.Text = "3";
            _ApertureNumeric.ToolTipText = "Measurement aperture in number of power line cycles (NPLC)";
            _ApertureNumeric.Value = new decimal(new int[] { 3, 0, 0, 0 });
            // 
            // _SenseRangeNumericLabel
            // 
            _SenseRangeNumericLabel.Margin = new Padding(3, 1, 0, 2);
            _SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel";
            _SenseRangeNumericLabel.Size = new System.Drawing.Size(43, 25);
            _SenseRangeNumericLabel.Text = "Range:";
            // 
            // _SenseRangeNumeric
            // 
            _SenseRangeNumeric.Name = "_SenseRangeNumeric";
            _SenseRangeNumeric.Size = new System.Drawing.Size(41, 25);
            _SenseRangeNumeric.Text = "0";
            _SenseRangeNumeric.ToolTipText = "Sense range";
            _SenseRangeNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _AutoRangeToggleButton
            // 
            __AutoRangeToggleButton.Checked = true;
            __AutoRangeToggleButton.CheckOnClick = true;
            __AutoRangeToggleButton.CheckState = CheckState.Indeterminate;
            __AutoRangeToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AutoRangeToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AutoRangeToggleButton.Name = "__AutoRangeToggleButton";
            __AutoRangeToggleButton.Size = new System.Drawing.Size(45, 25);
            __AutoRangeToggleButton.Text = "Auto ?";
            __AutoRangeToggleButton.ToolTipText = "Toggle auto range";
            // 
            // _AutoZeroToggleButton
            // 
            __AutoZeroToggleButton.Checked = true;
            __AutoZeroToggleButton.CheckOnClick = true;
            __AutoZeroToggleButton.CheckState = CheckState.Indeterminate;
            __AutoZeroToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AutoZeroToggleButton.Image = (System.Drawing.Image)resources.GetObject("_AutoZeroToggleButton.Image");
            __AutoZeroToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AutoZeroToggleButton.Name = "__AutoZeroToggleButton";
            __AutoZeroToggleButton.Size = new System.Drawing.Size(43, 25);
            __AutoZeroToggleButton.Text = "Zero ?";
            __AutoZeroToggleButton.ToolTipText = "Toggle auto zero";
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.Dock = DockStyle.None;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, __SenseFunctionComboBox, __ApplyFunctionModeButton });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(413, 25);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 0;
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplyFunctionModeMenuItem, __ReadFunctionModeMenuItem, _MeasureOptionsMenuItem, __ApplySettingsMenuItem, __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(58, 22);
            _SubsystemSplitButton.Text = "Meter";
            _SubsystemSplitButton.ToolTipText = "Selects function actions";
            // 
            // _ApplyFunctionModeMenuItem
            // 
            __ApplyFunctionModeMenuItem.Name = "__ApplyFunctionModeMenuItem";
            __ApplyFunctionModeMenuItem.Size = new System.Drawing.Size(185, 22);
            __ApplyFunctionModeMenuItem.Text = "Apply Function Mode";
            __ApplyFunctionModeMenuItem.ToolTipText = "Applies the selected function mode";
            // 
            // _ReadFunctionModeMenuItem
            // 
            __ReadFunctionModeMenuItem.Name = "__ReadFunctionModeMenuItem";
            __ReadFunctionModeMenuItem.Size = new System.Drawing.Size(185, 22);
            __ReadFunctionModeMenuItem.Text = "Read Function Mode";
            __ReadFunctionModeMenuItem.ToolTipText = "reads the instrument function mode";
            // 
            // _MeasureOptionsMenuItem
            // 
            _MeasureOptionsMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __MeasureImmediateMenuItem, _FetchOnMeasurementEventMenuItem, _AutoInitiateMenuItem });
            _MeasureOptionsMenuItem.Enabled = false;
            _MeasureOptionsMenuItem.Name = "_MeasureOptionsMenuItem";
            _MeasureOptionsMenuItem.Size = new System.Drawing.Size(185, 22);
            _MeasureOptionsMenuItem.Text = "Measure Options";
            _MeasureOptionsMenuItem.ToolTipText = "Selects measurement options";
            // 
            // _MeasureImmediateMenuItem
            // 
            __MeasureImmediateMenuItem.Name = "__MeasureImmediateMenuItem";
            __MeasureImmediateMenuItem.Size = new System.Drawing.Size(225, 22);
            __MeasureImmediateMenuItem.Text = "Immediate";
            __MeasureImmediateMenuItem.ToolTipText = "Fetches a measurement irrespective of the 'Fetch of Measurement Event' setting";
            // 
            // _FetchOnMeasurementEventMenuItem
            // 
            _FetchOnMeasurementEventMenuItem.CheckOnClick = true;
            _FetchOnMeasurementEventMenuItem.Name = "_FetchOnMeasurementEventMenuItem";
            _FetchOnMeasurementEventMenuItem.Size = new System.Drawing.Size(225, 22);
            _FetchOnMeasurementEventMenuItem.Text = "Fetch on Measurement Event";
            _FetchOnMeasurementEventMenuItem.ToolTipText = "Fetch on measurement event; Pressing Measure, sets the measurement event handling" + " and initiates a measurement, which is fetched upon the event.";
            // 
            // _AutoInitiateMenuItem
            // 
            _AutoInitiateMenuItem.CheckOnClick = true;
            _AutoInitiateMenuItem.Name = "_AutoInitiateMenuItem";
            _AutoInitiateMenuItem.Size = new System.Drawing.Size(225, 22);
            _AutoInitiateMenuItem.Text = "Auto Initiate";
            _AutoInitiateMenuItem.ToolTipText = "When checked, trigger plan is restarted following each data fetch";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(185, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            __ApplySettingsMenuItem.ToolTipText = "Applies settings";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(185, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            __ReadSettingsMenuItem.ToolTipText = "Reads settings";
            // 
            // _SenseFunctionComboBox
            // 
            __SenseFunctionComboBox.Name = "__SenseFunctionComboBox";
            __SenseFunctionComboBox.Size = new System.Drawing.Size(200, 25);
            __SenseFunctionComboBox.Text = "Voltage";
            __SenseFunctionComboBox.ToolTipText = "Selects the function";
            // 
            // _ApplyFunctionModeButton
            // 
            __ApplyFunctionModeButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __ApplyFunctionModeButton.Image = (System.Drawing.Image)resources.GetObject("_ApplyFunctionModeButton.Image");
            __ApplyFunctionModeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ApplyFunctionModeButton.Name = "__ApplyFunctionModeButton";
            __ApplyFunctionModeButton.Size = new System.Drawing.Size(27, 22);
            __ApplyFunctionModeButton.Text = "Set";
            __ApplyFunctionModeButton.ToolTipText = "Set the selected function";
            // 
            // _FilterToolStrip
            // 
            _FilterToolStrip.BackColor = System.Drawing.Color.Transparent;
            _FilterToolStrip.Dock = DockStyle.None;
            _FilterToolStrip.GripMargin = new Padding(0);
            _FilterToolStrip.Items.AddRange(new ToolStripItem[] { _FilterLabel, __FilterEnabledToggleButton, _FilterCountNumericLabel, _FilterCountNumeric, _FilterWindowNumericLabel, _FilterWindowNumeric, __WindowTypeToggleButton });
            _FilterToolStrip.Location = new System.Drawing.Point(0, 78);
            _FilterToolStrip.Name = "_FilterToolStrip";
            _FilterToolStrip.Size = new System.Drawing.Size(413, 28);
            _FilterToolStrip.Stretch = true;
            _FilterToolStrip.TabIndex = 3;
            // 
            // _FilterLabel
            // 
            _FilterLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _FilterLabel.Margin = new Padding(6, 1, 0, 2);
            _FilterLabel.Name = "_FilterLabel";
            _FilterLabel.Size = new System.Drawing.Size(33, 25);
            _FilterLabel.Text = "Filter";
            _FilterLabel.ToolTipText = "Averaging Window";
            // 
            // _FilterEnabledToggleButton
            // 
            __FilterEnabledToggleButton.Checked = true;
            __FilterEnabledToggleButton.CheckOnClick = true;
            __FilterEnabledToggleButton.CheckState = CheckState.Indeterminate;
            __FilterEnabledToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __FilterEnabledToggleButton.Image = (System.Drawing.Image)resources.GetObject("_FilterEnabledToggleButton.Image");
            __FilterEnabledToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __FilterEnabledToggleButton.Name = "__FilterEnabledToggleButton";
            __FilterEnabledToggleButton.Size = new System.Drawing.Size(35, 25);
            __FilterEnabledToggleButton.Text = "On ?";
            __FilterEnabledToggleButton.ToolTipText = "Toggle filter enabled";
            // 
            // _FilterCountNumericLabel
            // 
            _FilterCountNumericLabel.Name = "_FilterCountNumericLabel";
            _FilterCountNumericLabel.Size = new System.Drawing.Size(43, 25);
            _FilterCountNumericLabel.Text = "Count:";
            // 
            // _FilterCountNumeric
            // 
            _FilterCountNumeric.AutoSize = false;
            _FilterCountNumeric.Name = "_FilterCountNumeric";
            _FilterCountNumeric.Size = new System.Drawing.Size(50, 25);
            _FilterCountNumeric.Text = "0";
            _FilterCountNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _FilterWindowNumericLabel
            // 
            _FilterWindowNumericLabel.Margin = new Padding(3, 1, 0, 2);
            _FilterWindowNumericLabel.Name = "_FilterWindowNumericLabel";
            _FilterWindowNumericLabel.Size = new System.Drawing.Size(75, 25);
            _FilterWindowNumericLabel.Text = "Window [%]:";
            // 
            // _FilterWindowNumeric
            // 
            _FilterWindowNumeric.AutoSize = false;
            _FilterWindowNumeric.Name = "_FilterWindowNumeric";
            _FilterWindowNumeric.Size = new System.Drawing.Size(50, 25);
            _FilterWindowNumeric.Text = "10";
            _FilterWindowNumeric.ToolTipText = "Noise tolerance window in % of range";
            _FilterWindowNumeric.Value = new decimal(new int[] { 9999, 0, 0, 196608 });
            // 
            // _WindowTypeToggleButton
            // 
            __WindowTypeToggleButton.Checked = true;
            __WindowTypeToggleButton.CheckOnClick = true;
            __WindowTypeToggleButton.CheckState = CheckState.Indeterminate;
            __WindowTypeToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __WindowTypeToggleButton.Image = (System.Drawing.Image)resources.GetObject("_WindowTypeToggleButton.Image");
            __WindowTypeToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __WindowTypeToggleButton.Name = "__WindowTypeToggleButton";
            __WindowTypeToggleButton.Size = new System.Drawing.Size(60, 25);
            __WindowTypeToggleButton.Text = "Moving ?";
            __WindowTypeToggleButton.ToolTipText = "Toggle window type";
            // 
            // _InfoToolStrip
            // 
            _InfoToolStrip.BackColor = System.Drawing.Color.Transparent;
            _InfoToolStrip.Dock = DockStyle.None;
            _InfoToolStrip.GripMargin = new Padding(0);
            _InfoToolStrip.Items.AddRange(new ToolStripItem[] { _InfoLabel, __AutoDelayToggleButton, __OpenDetectorToggleButton, __TerminalStateReadButton, _ResolutionDigitsNumericLabel, _ResolutionDigitsNumeric });
            _InfoToolStrip.Location = new System.Drawing.Point(0, 106);
            _InfoToolStrip.Name = "_InfoToolStrip";
            _InfoToolStrip.Size = new System.Drawing.Size(413, 28);
            _InfoToolStrip.Stretch = true;
            _InfoToolStrip.TabIndex = 4;
            // 
            // _InfoLabel
            // 
            _InfoLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _InfoLabel.Margin = new Padding(6, 1, 0, 2);
            _InfoLabel.Name = "_InfoLabel";
            _InfoLabel.Size = new System.Drawing.Size(27, 25);
            _InfoLabel.Text = "Info";
            // 
            // _AutoDelayToggleButton
            // 
            __AutoDelayToggleButton.Checked = true;
            __AutoDelayToggleButton.CheckOnClick = true;
            __AutoDelayToggleButton.CheckState = CheckState.Indeterminate;
            __AutoDelayToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AutoDelayToggleButton.Image = (System.Drawing.Image)resources.GetObject("_AutoDelayToggleButton.Image");
            __AutoDelayToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AutoDelayToggleButton.Name = "__AutoDelayToggleButton";
            __AutoDelayToggleButton.Size = new System.Drawing.Size(51, 25);
            __AutoDelayToggleButton.Text = "Delay: ?";
            // 
            // _OpenDetectorToggleButton
            // 
            __OpenDetectorToggleButton.Checked = true;
            __OpenDetectorToggleButton.CheckOnClick = true;
            __OpenDetectorToggleButton.CheckState = CheckState.Indeterminate;
            __OpenDetectorToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __OpenDetectorToggleButton.Image = (System.Drawing.Image)resources.GetObject("_OpenDetectorToggleButton.Image");
            __OpenDetectorToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenDetectorToggleButton.Name = "__OpenDetectorToggleButton";
            __OpenDetectorToggleButton.Size = new System.Drawing.Size(96, 25);
            __OpenDetectorToggleButton.Text = "Open Detector ?";
            __OpenDetectorToggleButton.ToolTipText = "Toggle enabling open detector";
            // 
            // _TerminalStateReadButton
            // 
            __TerminalStateReadButton.Checked = true;
            __TerminalStateReadButton.CheckState = CheckState.Indeterminate;
            __TerminalStateReadButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __TerminalStateReadButton.Image = (System.Drawing.Image)resources.GetObject("_TerminalStateReadButton.Image");
            __TerminalStateReadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __TerminalStateReadButton.Name = "__TerminalStateReadButton";
            __TerminalStateReadButton.Size = new System.Drawing.Size(98, 25);
            __TerminalStateReadButton.Text = "Terminals: Rear ?";
            __TerminalStateReadButton.ToolTipText = "Read and display terminal state";
            // 
            // _ResolutionDigitsNumericLabel
            // 
            _ResolutionDigitsNumericLabel.Name = "_ResolutionDigitsNumericLabel";
            _ResolutionDigitsNumericLabel.Size = new System.Drawing.Size(40, 25);
            _ResolutionDigitsNumericLabel.Text = "Digits:";
            // 
            // _ResolutionDigitsNumeric
            // 
            _ResolutionDigitsNumeric.AutoSize = false;
            _ResolutionDigitsNumeric.Name = "_ResolutionDigitsNumeric";
            _ResolutionDigitsNumeric.Size = new System.Drawing.Size(31, 25);
            _ResolutionDigitsNumeric.Text = "0";
            _ResolutionDigitsNumeric.ToolTipText = "Sets the measurement resolution";
            _ResolutionDigitsNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _ToolStripPanel
            // 
            _ToolStripPanel.BackColor = System.Drawing.Color.Transparent;
            _ToolStripPanel.Controls.Add(_SubsystemToolStrip);
            _ToolStripPanel.Controls.Add(_ReadingToolStrip);
            _ToolStripPanel.Controls.Add(_FunctionConfigurationToolStrip);
            _ToolStripPanel.Controls.Add(_FilterToolStrip);
            _ToolStripPanel.Controls.Add(_InfoToolStrip);
            _ToolStripPanel.Dock = DockStyle.Top;
            _ToolStripPanel.Location = new System.Drawing.Point(1, 1);
            _ToolStripPanel.Name = "_ToolStripPanel";
            _ToolStripPanel.Orientation = Orientation.Horizontal;
            _ToolStripPanel.RowMargin = new Padding(3, 0, 0, 0);
            _ToolStripPanel.Size = new System.Drawing.Size(413, 134);
            // 
            // MeterView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_ToolStripPanel);
            Name = "MeterView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(415, 132);
            _ReadingToolStrip.ResumeLayout(false);
            _ReadingToolStrip.PerformLayout();
            _FunctionConfigurationToolStrip.ResumeLayout(false);
            _FunctionConfigurationToolStrip.PerformLayout();
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            _FilterToolStrip.ResumeLayout(false);
            _FilterToolStrip.PerformLayout();
            _InfoToolStrip.ResumeLayout(false);
            _InfoToolStrip.PerformLayout();
            _ToolStripPanel.ResumeLayout(false);
            _ToolStripPanel.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _SubsystemToolStrip;
        private ToolStripComboBox __SenseFunctionComboBox;

        private ToolStripComboBox _SenseFunctionComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SenseFunctionComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged -= SenseFunctionComboBox_SelectedIndexChanged;
                }

                __SenseFunctionComboBox = value;
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged += SenseFunctionComboBox_SelectedIndexChanged;
                }
            }
        }

        private ToolStrip _FunctionConfigurationToolStrip;
        private ToolStripLabel _FunctionLabel;
        private ToolStripLabel _ApertureNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _ApertureNumeric;
        private ToolStripLabel _SenseRangeNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _SenseRangeNumeric;
        private ToolStripButton __AutoZeroToggleButton;

        private ToolStripButton _AutoZeroToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AutoZeroToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AutoZeroToggleButton != null)
                {
                    __AutoZeroToggleButton.CheckStateChanged -= AutoZeroToggleButton_CheckStateChanged;
                }

                __AutoZeroToggleButton = value;
                if (__AutoZeroToggleButton != null)
                {
                    __AutoZeroToggleButton.CheckStateChanged += AutoZeroToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStrip _FilterToolStrip;
        private ToolStripLabel _FilterLabel;
        private ToolStripLabel _FilterCountNumericLabel;
        private ToolStripLabel _FilterWindowNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _FilterWindowNumeric;
        private ToolStripButton __AutoRangeToggleButton;

        private ToolStripButton _AutoRangeToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AutoRangeToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AutoRangeToggleButton != null)
                {
                    __AutoRangeToggleButton.CheckStateChanged -= AutoRangeToggleButton_CheckStateChanged;
                }

                __AutoRangeToggleButton = value;
                if (__AutoRangeToggleButton != null)
                {
                    __AutoRangeToggleButton.CheckStateChanged += AutoRangeToggleButton_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _FilterCountNumeric;
        private ToolStripButton __ApplyFunctionModeButton;

        private ToolStripButton _ApplyFunctionModeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyFunctionModeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyFunctionModeButton != null)
                {
                    __ApplyFunctionModeButton.Click -= ApplyFunctionModeButton_Click;
                }

                __ApplyFunctionModeButton = value;
                if (__ApplyFunctionModeButton != null)
                {
                    __ApplyFunctionModeButton.Click += ApplyFunctionModeButton_Click;
                }
            }
        }

        private ToolStripButton __FilterEnabledToggleButton;

        private ToolStripButton _FilterEnabledToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FilterEnabledToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FilterEnabledToggleButton != null)
                {
                    __FilterEnabledToggleButton.CheckStateChanged -= FilterEnabledToggleButton_CheckStateChanged;
                }

                __FilterEnabledToggleButton = value;
                if (__FilterEnabledToggleButton != null)
                {
                    __FilterEnabledToggleButton.CheckStateChanged += FilterEnabledToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStripButton __WindowTypeToggleButton;

        private ToolStripButton _WindowTypeToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WindowTypeToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WindowTypeToggleButton != null)
                {
                    __WindowTypeToggleButton.CheckStateChanged -= WindowTypeToggleButton_CheckStateChanged;
                }

                __WindowTypeToggleButton = value;
                if (__WindowTypeToggleButton != null)
                {
                    __WindowTypeToggleButton.CheckStateChanged += WindowTypeToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStrip _InfoToolStrip;
        private ToolStripLabel _InfoLabel;
        private ToolStripButton __AutoDelayToggleButton;

        private ToolStripButton _AutoDelayToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AutoDelayToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AutoDelayToggleButton != null)
                {
                    __AutoDelayToggleButton.CheckStateChanged -= AutoDelayToggleButton_CheckStateChanged;
                }

                __AutoDelayToggleButton = value;
                if (__AutoDelayToggleButton != null)
                {
                    __AutoDelayToggleButton.CheckStateChanged += AutoDelayToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStripButton __OpenDetectorToggleButton;

        private ToolStripButton _OpenDetectorToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDetectorToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDetectorToggleButton != null)
                {
                    __OpenDetectorToggleButton.CheckStateChanged -= OpenDetectorToggleButton_CheckStateChanged;
                }

                __OpenDetectorToggleButton = value;
                if (__OpenDetectorToggleButton != null)
                {
                    __OpenDetectorToggleButton.CheckStateChanged += OpenDetectorToggleButton_CheckStateChanged;
                }
            }
        }

        private ToolStripButton __TerminalStateReadButton;

        private ToolStripButton _TerminalStateReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TerminalStateReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TerminalStateReadButton != null)
                {
                    __TerminalStateReadButton.CheckStateChanged -= TerminalStateReadButton_CheckStateChanged;
                    __TerminalStateReadButton.Click -= TerminalStateReadButton_Click;
                }

                __TerminalStateReadButton = value;
                if (__TerminalStateReadButton != null)
                {
                    __TerminalStateReadButton.CheckStateChanged += TerminalStateReadButton_CheckStateChanged;
                    __TerminalStateReadButton.Click += TerminalStateReadButton_Click;
                }
            }
        }

        private ToolStripLabel _ResolutionDigitsNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _ResolutionDigitsNumeric;
        private ToolStrip _ReadingToolStrip;
        private ToolStripComboBox __ReadingElementTypesComboBox;

        private ToolStripComboBox _ReadingElementTypesComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadingElementTypesComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadingElementTypesComboBox != null)
                {
                    __ReadingElementTypesComboBox.SelectedIndexChanged -= ReadingElementTypesComboBox_SelectedIndexChanged;
                }

                __ReadingElementTypesComboBox = value;
                if (__ReadingElementTypesComboBox != null)
                {
                    __ReadingElementTypesComboBox.SelectedIndexChanged += ReadingElementTypesComboBox_SelectedIndexChanged;
                }
            }
        }

        private ToolStripLabel _ReadingElementTypesComboBoxLabel;
        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ReadFunctionModeMenuItem;

        private ToolStripMenuItem _ReadFunctionModeMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadFunctionModeMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadFunctionModeMenuItem != null)
                {
                    __ReadFunctionModeMenuItem.Click -= ReadFunctionModeMenuItem_Click;
                }

                __ReadFunctionModeMenuItem = value;
                if (__ReadFunctionModeMenuItem != null)
                {
                    __ReadFunctionModeMenuItem.Click += ReadFunctionModeMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ApplyFunctionModeMenuItem;

        private ToolStripMenuItem _ApplyFunctionModeMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyFunctionModeMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyFunctionModeMenuItem != null)
                {
                    __ApplyFunctionModeMenuItem.Click -= ApplyFunctionModeButton_Click;
                }

                __ApplyFunctionModeMenuItem = value;
                if (__ApplyFunctionModeMenuItem != null)
                {
                    __ApplyFunctionModeMenuItem.Click += ApplyFunctionModeButton_Click;
                }
            }
        }

        private ToolStripButton __MeasureValueButton;

        private ToolStripButton _MeasureValueButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureValueButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureValueButton != null)
                {
                    __MeasureValueButton.Click -= MeasureValueButton_Click;
                }

                __MeasureValueButton = value;
                if (__MeasureValueButton != null)
                {
                    __MeasureValueButton.Click += MeasureValueButton_Click;
                }
            }
        }

        private ToolStripMenuItem _MeasureOptionsMenuItem;
        private ToolStripMenuItem __MeasureImmediateMenuItem;

        private ToolStripMenuItem _MeasureImmediateMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureImmediateMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureImmediateMenuItem != null)
                {
                    __MeasureImmediateMenuItem.Click -= MeasureImmediateMenuItem_Click;
                }

                __MeasureImmediateMenuItem = value;
                if (__MeasureImmediateMenuItem != null)
                {
                    __MeasureImmediateMenuItem.Click += MeasureImmediateMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem _FetchOnMeasurementEventMenuItem;
        private ToolStripMenuItem _AutoInitiateMenuItem;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsToolStripMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsToolStripMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsToolStripMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsToolStripMenuItem_Click;
                }
            }
        }

        private ToolStripLabel _ReadSplitButton;
        private ToolStripPanel _ToolStripPanel;
    }
}