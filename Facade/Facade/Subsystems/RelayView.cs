using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A relay view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class RelayView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public RelayView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._ChannelListBuilder = new ChannelListBuilder();
            this._MemoryLocationNumeric.NumericUpDownControl.Minimum = 1m;
            this._MemoryLocationNumeric.NumericUpDownControl.Maximum = 100m;
            this._MemoryLocationNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._MemoryLocationNumeric.NumericUpDownControl.Value = 1m;
            this._SlotNumberNumeric.NumericUpDownControl.Minimum = 1m;
            this._SlotNumberNumeric.NumericUpDownControl.Maximum = 10m;
            this._SlotNumberNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._SlotNumberNumeric.NumericUpDownControl.Value = 1m;
            this._RelayNumberNumeric.NumericUpDownControl.Minimum = 1m;
            this._RelayNumberNumeric.NumericUpDownControl.Maximum = 100m;
            this._RelayNumberNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._RelayNumberNumeric.NumericUpDownControl.Value = 1m;
            this.__AddRelayMenuItem.Name = "_AddRelayMenuItem";
            this.__AddMemoryLocationToListMenuItem.Name = "_AddMemoryLocationToListMenuItem";
            this.__ClearChannelLIstMenuItem.Name = "_ClearChannelLIstMenuItem";
        }

        /// <summary> Creates a new <see cref="RelayView"/> </summary>
        /// <returns> A <see cref="RelayView"/>. </returns>
        public static RelayView Create()
        {
            RelayView view = null;
            try
            {
                view = new RelayView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary>Gets or sets the channel list.</summary>
        private ChannelListBuilder _ChannelListBuilder;

        /// <summary> Gets a list of channels. </summary>
        /// <value> A List of channels. </value>
        public string ChannelList => this._ChannelListBuilder.ToString();

        /// <summary> Adds channel to list. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void AddChannelToList()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"adding channel [{( int ) this._SlotNumberNumeric.Value},{( int ) this._RelayNumberNumeric.Value}] to the list";
                if ( this._ChannelListBuilder is null )
                {
                    this._ChannelListBuilder = new ChannelListBuilder();
                }

                this._ChannelListBuilder.AddChannel( ( int ) this._SlotNumberNumeric.Value, ( int ) this._RelayNumberNumeric.Value );
                this.NotifyPropertyChanged( nameof( this.ChannelList ) );
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears the channel list. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ClearChannelList()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"clearing channel list";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this._ChannelListBuilder = new ChannelListBuilder();
                this.NotifyPropertyChanged( nameof( this.ChannelList ) );
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Adds memory location to list. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void AddMemoryLocationToList()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"adding memory location {( int ) this._MemoryLocationNumeric.Value}";
                if ( this._ChannelListBuilder is null )
                {
                    this._ChannelListBuilder = new ChannelListBuilder();
                }

                this._ChannelListBuilder.AddChannel( ( int ) this._MemoryLocationNumeric.Value );
                this.NotifyPropertyChanged( nameof( this.ChannelList ) );
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Adds a channel to list click to 'e'. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AddChannelToList_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.AddChannelToList();
        }

        /// <summary> Adds a memory location button click to 'e'. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AddMemoryLocationButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.AddMemoryLocationToList();
        }

        /// <summary> Clears the channel ist menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ClearChannelLIstMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ClearChannelList();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
