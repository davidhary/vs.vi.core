﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class TriggerView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(TriggerView));
            _Layout = new TableLayoutPanel();
            _InfoTextBox = new TextBox();
            _ArmLayer1View = new ArmLayerView();
            _ArmLayer2View = new ArmLayerView();
            _TriggerLayer1View = new TriggerLayerView();
            _SubsystemToolStrip = new ToolStrip();
            _SubsystemSplitButton = new ToolStripSplitButton();
            __ApplySettingsMenuItem = new ToolStripMenuItem();
            __ApplySettingsMenuItem.Click += new EventHandler(ApplySettingsToolStripMenuItem_Click);
            __ReadSettingsMenuItem = new ToolStripMenuItem();
            __ReadSettingsMenuItem.Click += new EventHandler(ReadSettingsToolStripMenuItem_Click);
            __InitiateButton = new ToolStripButton();
            __InitiateButton.Click += new EventHandler(InitiateButton_Click);
            __AbortButton = new ToolStripButton();
            __AbortButton.Click += new EventHandler(AbortButton_Click);
            __SendBusTriggerButton = new ToolStripButton();
            __SendBusTriggerButton.Click += new EventHandler(SendBusTriggerButton_Click);
            _Layout.SuspendLayout();
            _SubsystemToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 2;
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 2.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _Layout.Controls.Add(_InfoTextBox, 0, 4);
            _Layout.Controls.Add(_ArmLayer1View, 0, 1);
            _Layout.Controls.Add(_ArmLayer2View, 0, 2);
            _Layout.Controls.Add(_TriggerLayer1View, 0, 3);
            _Layout.Controls.Add(_SubsystemToolStrip, 0, 0);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 5;
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _Layout.Size = new System.Drawing.Size(415, 352);
            _Layout.TabIndex = 0;
            // 
            // _InfoTextBox
            // 
            _InfoTextBox.Dock = DockStyle.Fill;
            _InfoTextBox.Location = new System.Drawing.Point(3, 310);
            _InfoTextBox.Margin = new Padding(3, 4, 3, 4);
            _InfoTextBox.Multiline = true;
            _InfoTextBox.Name = "_InfoTextBox";
            _InfoTextBox.ReadOnly = true;
            _InfoTextBox.ScrollBars = ScrollBars.Both;
            _InfoTextBox.Size = new System.Drawing.Size(407, 38);
            _InfoTextBox.TabIndex = 14;
            _InfoTextBox.Text = "<info>";
            // 
            // _ArmLayer1View
            // 
            _ArmLayer1View.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _ArmLayer1View.Dock = DockStyle.Top;
            _ArmLayer1View.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ArmLayer1View.LayerNumber = 1;
            _ArmLayer1View.Location = new System.Drawing.Point(3, 28);
            _ArmLayer1View.Name = "_ArmLayer1View";
            _ArmLayer1View.Padding = new Padding(1);
            _ArmLayer1View.Size = new System.Drawing.Size(407, 88);
            _ArmLayer1View.TabIndex = 0;
            // 
            // _ArmLayer2View
            // 
            _ArmLayer2View.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _ArmLayer2View.Dock = DockStyle.Top;
            _ArmLayer2View.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ArmLayer2View.LayerNumber = 2;
            _ArmLayer2View.Location = new System.Drawing.Point(3, 122);
            _ArmLayer2View.Name = "_ArmLayer2View";
            _ArmLayer2View.Padding = new Padding(1);
            _ArmLayer2View.Size = new System.Drawing.Size(407, 88);
            _ArmLayer2View.TabIndex = 1;
            // 
            // _TriggerLayer1View
            // 
            _TriggerLayer1View.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _TriggerLayer1View.Dock = DockStyle.Top;
            _TriggerLayer1View.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerLayer1View.LayerNumber = 1;
            _TriggerLayer1View.Location = new System.Drawing.Point(3, 216);
            _TriggerLayer1View.Name = "_TriggerLayer1View";
            _TriggerLayer1View.Padding = new Padding(1);
            _TriggerLayer1View.Size = new System.Drawing.Size(407, 87);
            _TriggerLayer1View.TabIndex = 2;
            // 
            // _SubsystemToolStrip
            // 
            _SubsystemToolStrip.AutoSize = false;
            _SubsystemToolStrip.BackColor = System.Drawing.Color.Transparent;
            _SubsystemToolStrip.GripMargin = new Padding(0);
            _SubsystemToolStrip.Items.AddRange(new ToolStripItem[] { _SubsystemSplitButton, __InitiateButton, __AbortButton, __SendBusTriggerButton });
            _SubsystemToolStrip.Location = new System.Drawing.Point(0, 0);
            _SubsystemToolStrip.Name = "_SubsystemToolStrip";
            _SubsystemToolStrip.Size = new System.Drawing.Size(413, 25);
            _SubsystemToolStrip.Stretch = true;
            _SubsystemToolStrip.TabIndex = 9;
            _SubsystemToolStrip.Text = "Trigger";
            // 
            // _SubsystemSplitButton
            // 
            _SubsystemSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SubsystemSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __ApplySettingsMenuItem, __ReadSettingsMenuItem });
            _SubsystemSplitButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemSplitButton.Image = (System.Drawing.Image)resources.GetObject("_SubsystemSplitButton.Image");
            _SubsystemSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SubsystemSplitButton.Name = "_SubsystemSplitButton";
            _SubsystemSplitButton.Size = new System.Drawing.Size(60, 22);
            _SubsystemSplitButton.Text = "Trigger";
            _SubsystemSplitButton.ToolTipText = "Subsystem actions split button";
            // 
            // _ApplySettingsMenuItem
            // 
            __ApplySettingsMenuItem.Name = "__ApplySettingsMenuItem";
            __ApplySettingsMenuItem.Size = new System.Drawing.Size(180, 22);
            __ApplySettingsMenuItem.Text = "Apply Settings";
            // 
            // _ReadSettingsMenuItem
            // 
            __ReadSettingsMenuItem.Name = "__ReadSettingsMenuItem";
            __ReadSettingsMenuItem.Size = new System.Drawing.Size(180, 22);
            __ReadSettingsMenuItem.Text = "Read Settings";
            // 
            // _InitiateButton
            // 
            __InitiateButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __InitiateButton.Image = (System.Drawing.Image)resources.GetObject("_InitiateButton.Image");
            __InitiateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __InitiateButton.Name = "__InitiateButton";
            __InitiateButton.Size = new System.Drawing.Size(47, 22);
            __InitiateButton.Text = "Initiate";
            __InitiateButton.ToolTipText = "Starts the trigger plan";
            // 
            // _AbortButton
            // 
            __AbortButton.Alignment = ToolStripItemAlignment.Right;
            __AbortButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AbortButton.Image = (System.Drawing.Image)resources.GetObject("_AbortButton.Image");
            __AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AbortButton.Margin = new Padding(10, 1, 0, 2);
            __AbortButton.Name = "__AbortButton";
            __AbortButton.Size = new System.Drawing.Size(41, 22);
            __AbortButton.Text = "Abort";
            __AbortButton.ToolTipText = "Aborts a trigger plan";
            // 
            // _SendBusTriggerButton
            // 
            __SendBusTriggerButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __SendBusTriggerButton.Image = (System.Drawing.Image)resources.GetObject("_SendBusTriggerButton.Image");
            __SendBusTriggerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __SendBusTriggerButton.Name = "__SendBusTriggerButton";
            __SendBusTriggerButton.Size = new System.Drawing.Size(43, 22);
            __SendBusTriggerButton.Text = "Assert";
            __SendBusTriggerButton.ToolTipText = "Sends a bus trigger to the instrument";
            // 
            // TriggerView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "TriggerView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(417, 354);
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            _SubsystemToolStrip.ResumeLayout(false);
            _SubsystemToolStrip.PerformLayout();
            ResumeLayout(false);
        }

        private ArmLayerView _ArmLayer1View;
        private ArmLayerView _ArmLayer2View;
        private TriggerLayerView _TriggerLayer1View;
        private TableLayoutPanel _Layout;
        private ToolStrip _SubsystemToolStrip;
        private ToolStripButton __InitiateButton;

        private ToolStripButton _InitiateButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitiateButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitiateButton != null)
                {
                    __InitiateButton.Click -= InitiateButton_Click;
                }

                __InitiateButton = value;
                if (__InitiateButton != null)
                {
                    __InitiateButton.Click += InitiateButton_Click;
                }
            }
        }

        private ToolStripButton __AbortButton;

        private ToolStripButton _AbortButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortButton != null)
                {
                    __AbortButton.Click -= AbortButton_Click;
                }

                __AbortButton = value;
                if (__AbortButton != null)
                {
                    __AbortButton.Click += AbortButton_Click;
                }
            }
        }

        private ToolStripSplitButton _SubsystemSplitButton;
        private ToolStripMenuItem __ApplySettingsMenuItem;

        private ToolStripMenuItem _ApplySettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click -= ApplySettingsToolStripMenuItem_Click;
                }

                __ApplySettingsMenuItem = value;
                if (__ApplySettingsMenuItem != null)
                {
                    __ApplySettingsMenuItem.Click += ApplySettingsToolStripMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadSettingsMenuItem;

        private ToolStripMenuItem _ReadSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click -= ReadSettingsToolStripMenuItem_Click;
                }

                __ReadSettingsMenuItem = value;
                if (__ReadSettingsMenuItem != null)
                {
                    __ReadSettingsMenuItem.Click += ReadSettingsToolStripMenuItem_Click;
                }
            }
        }

        private ToolStripButton __SendBusTriggerButton;

        private ToolStripButton _SendBusTriggerButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SendBusTriggerButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SendBusTriggerButton != null)
                {
                    __SendBusTriggerButton.Click -= SendBusTriggerButton_Click;
                }

                __SendBusTriggerButton = value;
                if (__SendBusTriggerButton != null)
                {
                    __SendBusTriggerButton.Click += SendBusTriggerButton_Click;
                }
            }
        }

        private TextBox _InfoTextBox;
    }
}