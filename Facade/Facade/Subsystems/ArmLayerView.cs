using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.SplitExtensions;
using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{

    /// <summary> An Arm Layer view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ArmLayerView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public ArmLayerView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._CountNumeric.NumericUpDownControl.Minimum = 1m;
            this._CountNumeric.NumericUpDownControl.Maximum = 99999m;
            this._CountNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._CountNumeric.NumericUpDownControl.Value = 1m;
            this._InputLineNumeric.NumericUpDownControl.Minimum = 1m;
            this._InputLineNumeric.NumericUpDownControl.Maximum = 6m;
            this._InputLineNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._InputLineNumeric.NumericUpDownControl.Value = 2m;
            this._OutputLineNumeric.NumericUpDownControl.Minimum = 1m;
            this._OutputLineNumeric.NumericUpDownControl.Maximum = 6m;
            this._OutputLineNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._OutputLineNumeric.NumericUpDownControl.Value = 1m;
            this._TimerIntervalNumeric.NumericUpDownControl.Minimum = 0m;
            this._TimerIntervalNumeric.NumericUpDownControl.Maximum = 99999m;
            this._TimerIntervalNumeric.NumericUpDownControl.DecimalPlaces = 3;
            this._TimerIntervalNumeric.NumericUpDownControl.Value = 1m;
            this._DelayNumeric.NumericUpDownControl.Minimum = 0m;
            this._DelayNumeric.NumericUpDownControl.Maximum = 99999m;
            this._DelayNumeric.NumericUpDownControl.DecimalPlaces = 3;
            this._DelayNumeric.NumericUpDownControl.Value = 0m;
            this.__ApplyMenuItem.Name = "_ApplyMenuItem";
            this.__ReadMenuItem.Name = "_ReadMenuItem";
            this.__InfiniteCountButton.Name = "_InfiniteCountButton";
            this.__BypassToggleButton.Name = "_BypassToggleButton";
        }

        /// <summary> Creates a new ArmLayerView. </summary>
        /// <returns> An ArmLayerView. </returns>
        public static ArmLayerView Create()
        {
            ArmLayerView view = null;
            try
            {
                view = new ArmLayerView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS "

        /// <summary> The layer number. </summary>
        private int _LayerNumber;

        /// <summary> Gets or sets the layer number. </summary>
        /// <value> The layer number. </value>
        public int LayerNumber
        {
            get => this._LayerNumber;

            set {
                if ( value != this.LayerNumber )
                {
                    this._LayerNumber = value;
                    this._SubsystemSplitButton.Text = $"Arm{value}";
                }
            }
        }

        /// <summary> Gets or sets the number of triggers. </summary>
        /// <value> The number of triggers. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int Count
        {
            get => this._InfiniteCountButton.CheckState == CheckState.Checked ? int.MaxValue : ( int ) this._CountNumeric.Value;

            set {
                if ( value != this.Count )
                {
                    if ( value == int.MaxValue )
                    {
                        this._InfiniteCountButton.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        this._InfiniteCountButton.CheckState = CheckState.Unchecked;
                        this._CountNumeric.Value = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the arm source. </summary>
        /// <value> The arm source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmSources Source
        {
            get => ( ArmSources ) Conversions.ToInteger( this._SourceComboBox.ComboBox.SelectedValue );

            set {
                if ( value != this.Source )
                {
                    this._SourceComboBox.ComboBox.SelectedItem = value.ValueNamePair();
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Applies the settings onto the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ApplySettings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} applying {this._SubsystemSplitButton.Text} settings";
                _ = this.PublishInfo( $"{activity};. " );
                this.ArmLayerSubsystem.StartElapsedStopwatch();
                _ = this._InfiniteCountButton.CheckState == CheckState.Checked
                    ? this.ArmLayerSubsystem.ApplyArmCount( int.MaxValue )
                    : this.ArmLayerSubsystem.ApplyArmCount( ( int ) this._CountNumeric.Value );

                _ = this.ArmLayerSubsystem.ApplyInputLineNumber( ( int ) this._InputLineNumeric.Value );
                _ = this.ArmLayerSubsystem.ApplyOutputLineNumber( ( int ) this._OutputLineNumeric.Value );
                _ = this.ArmLayerSubsystem.ApplyArmSource( ( ArmSources ) Conversions.ToInteger( this._SourceComboBox.ComboBox.SelectedValue ) );
                _ = this.ArmLayerSubsystem.ApplyArmLayerBypassMode( this._BypassToggleButton.CheckState == CheckState.Checked ? TriggerLayerBypassModes.Source : TriggerLayerBypassModes.Acceptor );
                if ( this.LayerNumber == 2 )
                {
                    _ = this.ArmLayerSubsystem.ApplyDelay( TimeSpan.FromMilliseconds( ( double ) (1000m * this._DelayNumeric.Value) ) );
                    _ = this.ArmLayerSubsystem.ApplyTimerTimeSpan( TimeSpan.FromMilliseconds( ( double ) (1000m * this._TimerIntervalNumeric.Value) ) );
                }

                this.ArmLayerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads the settings from the instrument. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadSettings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing exception state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} reading  {this._SubsystemSplitButton.Text} settings";
                _ = this.PublishInfo( $"{activity};. " );
                ReadSettings( this.ArmLayerSubsystem );
                this.ApplyPropertyChanged( this.ArmLayerSubsystem );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( ArmLayerView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SUBSYSTEM "

        /// <summary> Gets or sets the arm layer subsystem. </summary>
        /// <value> The arm layer subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmLayerSubsystemBase ArmLayerSubsystem { get; private set; }

        /// <summary> Bind subsystem. </summary>
        /// <param name="subsystem">   The subsystem. </param>
        /// <param name="layerNumber"> The layer number. </param>
        public void BindSubsystem( ArmLayerSubsystemBase subsystem, int layerNumber )
        {
            if ( this.ArmLayerSubsystem is object )
            {
                this.BindSubsystem( false, this.ArmLayerSubsystem );
                this.ArmLayerSubsystem = null;
            }

            this.LayerNumber = layerNumber;
            this.ArmLayerSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.ArmLayerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ArmLayerSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ArmLayerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.SupportedArmSources ) );
                // must Not read setting when biding because the instrument may be locked Or in a trigger mode
                // The bound values should be sent when binding Or when applying propert change.
                // ReadSettings( subsystem );
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.ArmLayerSubsystemPropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( ArmLayerSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.ArmCount ) );
            this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.ArmSource ) );
            this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.IsArmCountInfinite ) );
            this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.IsArmLayerBypass ) );
            this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.InputLineNumber ) );
            this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.OutputLineNumber ) );
            if ( subsystem.LayerNumber == 2 )
            {
                this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.Delay ) );
                this.HandlePropertyChanged( subsystem, nameof( ArmLayerSubsystemBase.TimerInterval ) );
            }
        }

        /// <summary> Handle the Calculate subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ArmLayerSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ArmLayerSubsystemBase.ArmCount ):
                    {
                        if ( subsystem.ArmCount.HasValue )
                            this._CountNumeric.Value = subsystem.ArmCount.Value;
                        break;
                    }

                case nameof( ArmLayerSubsystemBase.ArmSource ):
                    {
                        if ( subsystem.ArmSource.HasValue && this._SourceComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._SourceComboBox.ComboBox.SelectedItem = subsystem.ArmSource.Value.ValueNamePair();
                        }

                        break;
                    }

                case nameof( ArmLayerSubsystemBase.SupportedArmSources ):
                    {
                        this.InitializingComponents = true;
                        this._SourceComboBox.ComboBox.ListSupportedArmSources( subsystem.SupportedArmSources );
                        this.InitializingComponents = false;
                        if ( subsystem.ArmSource.HasValue && this._SourceComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._SourceComboBox.ComboBox.SelectedItem = subsystem.ArmSource.Value.ValueNamePair();
                        }

                        break;
                    }

                case nameof( ArmLayerSubsystemBase.InputLineNumber ):
                    {
                        if ( subsystem.InputLineNumber.HasValue )
                            this._InputLineNumeric.Value = subsystem.InputLineNumber.Value;
                        break;
                    }

                case nameof( ArmLayerSubsystemBase.IsArmCountInfinite ):
                    {
                        this._InfiniteCountButton.CheckState = subsystem.IsArmCountInfinite.ToCheckState();
                        break;
                    }

                case nameof( ArmLayerSubsystemBase.IsArmLayerBypass ):
                    {
                        this._BypassToggleButton.CheckState = subsystem.IsArmLayerBypass.ToCheckState();
                        break;
                    }

                case nameof( ArmLayerSubsystemBase.OutputLineNumber ):
                    {
                        if ( subsystem.OutputLineNumber.HasValue )
                            this._OutputLineNumeric.Value = subsystem.OutputLineNumber.Value;
                        break;
                    }

                case nameof( ArmLayerSubsystemBase.Delay ):
                    {
                        if ( subsystem.Delay.HasValue )
                            this._DelayNumeric.Value = ( decimal ) (0.001d * subsystem.Delay.Value.TotalMilliseconds);
                        break;
                    }

                case nameof( ArmLayerSubsystemBase.TimerInterval ):
                    {
                        if ( subsystem.TimerInterval.HasValue )
                            this._TimerIntervalNumeric.Value = ( decimal ) (0.001d * subsystem.TimerInterval.Value.TotalMilliseconds);
                        break;
                    }
            }
        }

        /// <summary> Arm layer subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ArmLayerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( ArmLayerSubsystemBase )}.{e.PropertyName} change";
            IAsyncResult controlAsyncResult = null;
            IAsyncResult stripAsyncResult = null;
            try
            {
                if ( this.InvokeRequired )
                {
                    // _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ArmLayerSubsystemPropertyChanged ), new object[] { sender, e } );
                    controlAsyncResult = this.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.ArmLayerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    // _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.ArmLayerSubsystemPropertyChanged ), new object[] { sender, e } );
                    stripAsyncResult = this._SubsystemToolStrip.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.ArmLayerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ArmLayerSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                // https://stackoverflow.com/questions/22356/cleanest-way-to-invoke-cross-thread-events
                if ( controlAsyncResult is object ) _ = this.EndInvoke( controlAsyncResult );
                if ( stripAsyncResult is object ) _ = this._SubsystemToolStrip.EndInvoke( stripAsyncResult );
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Reads the settings from the instrument. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( ArmLayerSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryArmCount();
            _ = subsystem.QueryInputLineNumber();
            _ = subsystem.QueryOutputLineNumber();
            _ = subsystem.QueryArmSource();
            _ = subsystem.QueryArmLayerBypassMode();
            if ( subsystem.LayerNumber == 2 )
            {
                _ = subsystem.QueryDelay();
                _ = subsystem.QueryTimerTimeSpan();
            }

            subsystem.StopElapsedStopwatch();
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Applies the menu ite click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplyMenuIte_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ApplySettings();
        }

        /// <summary> Reads menu ite click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadMenuIte_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ReadSettings();
        }

        /// <summary> Bypass toggle button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void BypassToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            this._BypassToggleButton.Text = this._BypassToggleButton.CheckState.ToCheckStateCaption( "Bypass", "~Bypass", "Bypass?" );
        }

        /// <summary> Infinite count button check state changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void InfiniteCountButton_CheckStateChanged( object sender, EventArgs e )
        {
            this._InfiniteCountButton.Text = this._InfiniteCountButton.CheckState.ToCheckStateCaption( "Infinite", "Finite", "Infinite?" );
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
