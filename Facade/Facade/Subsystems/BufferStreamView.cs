using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.DataGridViewExtensions;

namespace isr.VI.Facade
{

    /// <summary> A Buffer Stream view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class BufferStreamView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public BufferStreamView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._BufferSizeNumeric.NumericUpDownControl.CausesValidation = true;
            this._BufferSizeNumeric.NumericUpDownControl.Minimum = 0m;
            this._BufferSizeNumeric.NumericUpDownControl.Maximum = 27500000m;
            this._StartBufferStreamMenuItem.Enabled = false;
            this._AssertBusTriggerButton.Enabled = false;
            this.__DisplayBufferMenuItem.Name = "_DisplayBufferMenuItem";
            this.__ClearBufferDisplayMenuItem.Name = "_ClearBufferDisplayMenuItem";
            this.__ExplainBufferStreamingMenuItem.Name = "_ExplainBufferStreamingMenuItem";
            this.__ConfigureStreamingMenuItem.Name = "_ConfigureStreamingMenuItem";
            this.__StartBufferStreamMenuItem.Name = "_StartBufferStreamMenuItem";
            this.__BufferSizeNumeric.Name = "_BufferSizeNumeric";
            this.__AssertBusTriggerButton.Name = "_AssertBusTriggerButton";
        }

        /// <summary> Creates a new <see cref="BufferStreamView"/> </summary>
        /// <returns> A <see cref="BufferStreamView"/>. </returns>
        public static BufferStreamView Create()
        {
            BufferStreamView view = null;
            try
            {
                view = new BufferStreamView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLIC MEMBERS: BUFFER "

        /// <summary> Applies the buffer capacity. </summary>
        /// <param name="capacity"> The capacity. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ApplyBufferCapacity( int capacity )
        {
            string activity = string.Empty;
            try
            {
                if ( this.Device.IsDeviceOpen )
                {
                    if ( this.BufferSubsystem is object )
                    {
                        activity = $"{this.Device.ResourceNameCaption} setting {typeof( TraceSubsystemBase )}.{nameof( BufferSubsystemBase.Capacity )} to {capacity}";
                        _ = this.PublishVerbose( activity );
                        ApplyBufferCapacity( this.BufferSubsystem, capacity );
                    }
                    else if ( this.TraceSubsystem is object )
                    {
                        activity = $"{this.Device.ResourceNameCaption} setting {typeof( TraceSubsystemBase )}.{nameof( TraceSubsystemBase.PointsCount )} to {capacity}";
                        _ = this.PublishVerbose( activity );
                        ApplyBufferCapacity( this.TraceSubsystem, capacity );
                    }
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
        }

        /// <summary> Reads the buffer. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadBuffer()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading buffer";
                _ = this.PublishVerbose( $"{activity};. " );
                if ( this.BufferSubsystem is object )
                {
                    this.ReadBuffer( this.BufferSubsystem );
                }
                else if ( this.TraceSubsystem is object )
                {
                    this.ReadBuffer( this.TraceSubsystem );
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " PUBLIC MEMBERS: BUFFER DISPLAY "

        private DataGridView _DataGridViewThis;

        /// <summary>   Gets or sets the reference to the data grid view. </summary>
        /// <value> The reference to the data grid view. </value>
        private DataGridView DataGridViewThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DataGridViewThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DataGridViewThis != null )
                {
                    this._DataGridViewThis.DataError -= this.DataGridView_DataError;
                }

                this._DataGridViewThis = value;
                if ( this._DataGridViewThis != null )
                {
                    this._DataGridViewThis.DataError += this.DataGridView_DataError;
                }
            }
        }

        /// <summary> Gets or sets the data grid view. </summary>
        /// <value> The data grid view. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DataGridView DataGridView
        {
            get => this.DataGridViewThis;

            set => this.DataGridViewThis = value;
        }

        /// <summary> Gets the buffer readings. </summary>
        /// <value> The buffer readings. </value>
        private BufferReadingBindingList BufferReadings { get; set; } = new BufferReadingBindingList();

        /// <summary> Displays a buffer readings. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void DisplayBufferReadings()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading buffer";
                _ = this.PublishVerbose( $"{activity};. " );
                this.BufferSubsystem.StartElapsedStopwatch();
                this.ClearBufferDisplay();
                this.BufferReadings.Add( this.BufferSubsystem.QueryBufferReadings() );
                this.BufferSubsystem.LastReading = this.BufferReadings.LastReading;
                this.BufferSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears the buffer display. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ClearBufferDisplay()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing buffer display";
                _ = this.PublishVerbose( $"{activity};. " );
                if ( this.BufferReadings is null )
                {
                    this.BufferReadings = new BufferReadingBindingList();
                    _ = (this.DataGridView?.Bind( this.BufferReadings, true ));
                }

                this.BufferReadings.Clear();
            }
            catch ( Exception ex )
            {
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( VisaSessionBase value )
        {
            if ( this.Device is object )
            {
                this.DataGridView = null;
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishVerbose( $"{value.ResourceNameCaption} assigned to {nameof( BufferStreamView ).SplitWords()}" );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( VisaSessionBase value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " BUFFER SUBSYSTEM "

        /// <summary> Gets the Buffer subsystem. </summary>
        /// <value> The Buffer subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public BufferSubsystemBase BufferSubsystem { get; private set; }

        /// <summary> Bind Buffer subsystem. </summary>
        /// <param name="subsystem">  The subsystem. </param>
        /// <param name="bufferName"> Name of the buffer. </param>
        public void BindSubsystem( BufferSubsystemBase subsystem, string bufferName )
        {
            if ( this.BufferSubsystem is object )
            {
                this.BindSubsystem( false, this.BufferSubsystem );
                this.BufferSubsystem = null;
            }

            this.BufferSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.BufferSubsystem );
                this._SubsystemSplitButton.Text = bufferName;
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, BufferSubsystemBase subsystem )
        {
            if ( add )
                _ = (this.DataGridView?.Bind( subsystem.BufferReadingsBindingList, true ));
            var binding = this.AddRemoveBinding( this._BufferSizeNumeric, add, nameof( NumericUpDown.Value ), subsystem, nameof( BufferSubsystemBase.Capacity ) );
            // has to apply the value.
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            _ = this.AddRemoveBinding( this._BufferCountLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.ActualPointCount ) );
            _ = this.AddRemoveBinding( this._FirstPointNumberLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.FirstPointNumber ) );
            _ = this.AddRemoveBinding( this._LastPointNumberLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.LastPointNumber ) );
            // must Not read setting when biding because the instrument may be locked Or in a trigger mode
            // The bound values should be sent when binding Or when applying propert change.
            // ReadSettings( subsystem );
            if ( add )
            {
                this.BufferSubsystem.PropertyChanged += this.HandleBufferSubsystemPropertyChange;
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                this.BufferSubsystem.PropertyChanged -= this.HandleBufferSubsystemPropertyChange;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( BufferSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( BufferSubsystemBase.BufferStreamingEnabled ) );
            this.HandlePropertyChanged( subsystem, nameof( BufferSubsystemBase.BufferStreamingActive ) );
            this.HandlePropertyChanged( subsystem, nameof( BufferSubsystemBase.BufferReadingsCount ) );
        }

        /// <summary> Handles the Buffer subsystem property change. </summary>
        /// <param name="subsystem">    The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( BufferSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is object && !string.IsNullOrWhiteSpace( propertyName ) )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( BufferSubsystemBase.BufferReadingsCount ):
                        {
                            if ( subsystem.BufferReadingsCount > 0 )
                            {
                                string message = this.PublishInfo( $"Streaming reading #{subsystem.BufferReadingsCount}: {subsystem.LastReading.Amount} 0x{subsystem.LastReading.StatusWord:X4}" );
                                subsystem.Session.StatusPrompt = message;
                            }

                            break;
                        }

                    case nameof( BufferSubsystemBase.BufferStreamingEnabled ):
                        {
                            this.NotifyPropertyChanged( nameof( BufferSubsystemBase.BufferStreamingEnabled ) );
                            break;
                        }

                    case nameof( BufferSubsystemBase.BufferStreamingActive ):
                        {
                            this._ConfigureStreamingMenuItem.Enabled = !subsystem.BufferStreamingActive;
                            if ( subsystem.BufferReadingsCount > 0 && !subsystem.BufferStreamingActive )
                            {
                                string message = this.PublishInfo( $"Streaming ended reading #{subsystem.BufferReadingsCount}: {subsystem.LastReading.Amount} 0x{subsystem.LastReading.StatusWord:X4}" );
                                subsystem.Session.StatusPrompt = message;
                            }

                            this.NotifyPropertyChanged( nameof( BufferSubsystemBase.BufferStreamingActive ) );
                            break;
                        }
                }
            }
        }

        /// <summary> Handles the Buffer subsystem property change. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleBufferSubsystemPropertyChange( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( BufferSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleBufferSubsystemPropertyChange ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleBufferSubsystemPropertyChange ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as BufferSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Reads the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( BufferSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryCapacity();
            _ = subsystem.QueryFirstPointNumber();
            _ = subsystem.QueryLastPointNumber();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Applies the buffer capacity. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="capacity">  The capacity. </param>
        private static void ApplyBufferCapacity( BufferSubsystemBase subsystem, int capacity )
        {
            // overrides and set to the minimum size: Me._TraceSizeNumeric.Value = Me._TraceSizeNumeric.NumericUpDownControl.Minimum
            subsystem.StartElapsedStopwatch();
            _ = subsystem.ApplyCapacity( capacity );
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Reads the buffer. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadBuffer( BufferSubsystemBase subsystem )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading buffer";
                _ = this.PublishVerbose( $"{activity};. " );
                subsystem.StartElapsedStopwatch();
                this.ClearBufferDisplay();
                this.BufferReadings.Add( subsystem.QueryBufferReadings() );
                subsystem.LastReading = this.BufferReadings.LastReading;
                subsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TRACE SUBSYSTEM "

        /// <summary> Gets the Trace subsystem. </summary>
        /// <value> The Trace subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TraceSubsystemBase TraceSubsystem { get; private set; }

        /// <summary> Bind Trace subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( TraceSubsystemBase subsystem )
        {
            if ( this.TraceSubsystem is object )
            {
                this.BindSubsystem( false, this.TraceSubsystem );
                this.TraceSubsystem = null;
            }

            this.TraceSubsystem = subsystem;
            if ( subsystem is object )
            {
                this._SubsystemSplitButton.Text = "Trace";
                this.BindSubsystem( true, this.TraceSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TraceSubsystemBase subsystem )
        {
            if ( add )
                _ = (this.DataGridView?.Bind( subsystem.BufferReadingsBindingList, true ));
            var binding = this.AddRemoveBinding( this._BufferSizeNumeric, add, nameof( NumericUpDown.Value ), subsystem, nameof( TraceSubsystemBase.PointsCount ) );
            // has to apply the value.
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            _ = this.AddRemoveBinding( this._BufferCountLabel, add, nameof( Control.Text ), subsystem, nameof( TraceSubsystemBase.ActualPointCount ) );
            this._FirstPointNumberLabel.Visible = false;
            this._LastPointNumberLabel.Visible = false;
            // must Not read setting when biding because the instrument may be locked Or in a trigger mode
            // The bound values should be sent when binding Or when applying propert change.
            // ReadSettings( subsystem );
            if ( add )
            {
                this.TraceSubsystem.PropertyChanged += this.HandleTraceSubsystemPropertyChange;
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                this.TraceSubsystem.PropertyChanged -= this.HandleTraceSubsystemPropertyChange;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( TraceSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.BufferStreamingEnabled ) );
            this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.BufferStreamingActive ) );
            this.HandlePropertyChanged( subsystem, nameof( TraceSubsystemBase.BufferReadingsCount ) );
        }

        /// <summary> Handles the trace subsystem property change. </summary>
        /// <param name="subsystem">    The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TraceSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is object && !string.IsNullOrWhiteSpace( propertyName ) )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( TraceSubsystemBase.BufferReadingsCount ):
                        {
                            if ( subsystem.BufferReadingsCount > 0 )
                            {
                                string message = this.PublishInfo( $"Streaming reading #{subsystem.BufferReadingsCount}: {subsystem.LastBufferReading.Amount} 0x{subsystem.LastBufferReading.StatusWord:X4}" );
                                subsystem.Session.StatusPrompt = message;
                            }

                            break;
                        }

                    case nameof( TraceSubsystemBase.BufferStreamingEnabled ):
                        {
                            this.NotifyPropertyChanged( nameof( TraceSubsystemBase.BufferStreamingEnabled ) );
                            break;
                        }

                    case nameof( TraceSubsystemBase.BufferStreamingActive ):
                        {
                            this._ConfigureStreamingMenuItem.Enabled = !subsystem.BufferStreamingActive;
                            if ( subsystem.BufferReadingsCount > 0 && !subsystem.BufferStreamingActive )
                            {
                                string message = this.PublishInfo( $"Streaming ended reading #{subsystem.BufferReadingsCount}: {subsystem.LastBufferReading.Amount} 0x{subsystem.LastBufferReading.StatusWord:X4}" );
                                subsystem.Session.StatusPrompt = message;
                            }

                            this.NotifyPropertyChanged( nameof( TraceSubsystemBase.BufferStreamingActive ) );
                            break;
                        }
                }
            }
        }

        /// <summary> Handles the trace subsystem property change. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleTraceSubsystemPropertyChange( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( TraceSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleTraceSubsystemPropertyChange ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TraceSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Reads the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( TraceSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryPointsCount();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Applies the buffer capacity. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="capacity">  The capacity. </param>
        private static void ApplyBufferCapacity( TraceSubsystemBase subsystem, int capacity )
        {
            // overrides and set to the minimum size: Me._TraceSizeNumeric.Value = Me._TraceSizeNumeric.NumericUpDownControl.Minimum
            subsystem.StartElapsedStopwatch();
            _ = subsystem.ApplyPointsCount( capacity );
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Reads the buffer. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ReadBuffer( TraceSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            this.ClearBufferDisplay();
            this.BufferReadings.Add( subsystem.QueryBufferReadings() );
            subsystem.LastBufferReading = this.BufferReadings.LastReading;
            subsystem.StopElapsedStopwatch();
        }


        #endregion

        #region " TRIGGER "

        /// <summary> Gets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystemBase TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( TriggerSubsystemBase subsystem )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            this.TriggerSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
                this.ApplyPropertyChanged( subsystem );
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Applies the property changed described by subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void ApplyPropertyChanged( TriggerSubsystemBase subsystem )
        {
            this.HandlePropertyChanged( subsystem, nameof( TriggerSubsystemBase.TriggerState ) );
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TriggerSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( TriggerSubsystemBase.TriggerState ):
                    {
                        // TO_DO: Bind trigger state label caption to the Subsystem reading label in the display view.
                        // TO_DO: Move functionality (e.g., handler trigger plan state change) from the user interface to the subsystem
                        this._TriggerStateLabel.Visible = subsystem.TriggerState.HasValue;
                        if ( subsystem.SupportsTriggerState && subsystem.TriggerState.HasValue )
                        {
                            this._TriggerStateLabel.Text = subsystem.TriggerState.Value.ToString();
                            if ( !subsystem.IsTriggerStateActive() && this._StartBufferStreamMenuItem.Checked )
                            {
                                this._StartBufferStreamMenuItem.Checked = false;
                            }
                        }

                        break;
                    }
                    // ?? this causes a cross thread exception. 
                    // Me._TriggerStateLabel.Invalidate()
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( TriggerSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Initiate trigger plan. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitiateTriggerPlan()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing execution state";
                this.Device.ClearExecutionState();
                activity = $"{this.Device.ResourceNameCaption} initiating trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.TriggerSubsystem.StartElapsedStopwatch();
                this.TriggerSubsystem.Initiate();
                _ = this.TriggerSubsystem.QueryTriggerState();
                this.TriggerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Aborts trigger plan. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void AbortTriggerPlan()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Aborting trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.TriggerSubsystem.Abort();
                _ = this.Device.Session.QueryOperationCompleted( TimeSpan.FromMilliseconds( 100d ) );
                _ = this.TriggerSubsystem.QueryTriggerState();
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SYSTEM SUBSYSTEM "

        /// <summary> Gets the System subsystem. </summary>
        /// <value> The System subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SystemSubsystemBase SystemSubsystem { get; private set; }

        /// <summary> Bind System subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( SystemSubsystemBase subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                this.SystemSubsystem.PropertyChanged -= this.HandleSystemSubsystemPropertyChange;
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( subsystem is object )
            {
                // must Not read setting when biding because the instrument may be locked Or in a trigger mode
                // The bound values should be sent when binding Or when applying propert change.
                // ReadSettings( subsystem );
                // Me.ApplyPropertyChanged(subsystem)
                this.SystemSubsystem.PropertyChanged += this.HandleSystemSubsystemPropertyChange;
            }
        }

        /// <summary> Reads the settings. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private static void ReadSettings( SystemSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.QueryFrontTerminalsSelected();
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SystemSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is object && !string.IsNullOrWhiteSpace( propertyName ) )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( SystemSubsystemBase.SupportsScanCardOption ):
                        {
                            this._UsingScanCardMenuItem.Enabled = subsystem.SupportsScanCardOption;
                            if ( !subsystem.SupportsScanCardOption )
                            {
                                this._UsingScanCardMenuItem.Checked = false;
                            }

                            break;
                        }
                }
            }
        }

        /// <summary> Handles the system subsystem property change. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void HandleSystemSubsystemPropertyChange( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SystemSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleSystemSubsystemPropertyChange ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleSystemSubsystemPropertyChange ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SystemSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " ARM LAYER 1 SUBSYSTEM "

        /// <summary> Gets the ArmLayer1 subsystem. </summary>
        /// <value> The ArmLayer1 subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmLayerSubsystemBase ArmLayer1Subsystem { get; private set; }

        /// <summary> Bind ArmLayer1 subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem1( ArmLayerSubsystemBase subsystem )
        {
            if ( this.ArmLayer1Subsystem is object )
            {
                this.ArmLayer1Subsystem = null;
            }

            this.ArmLayer1Subsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }

        #endregion

        #region " ARM LAYER 2 SUBSYSTEM "

        /// <summary> Gets the ArmLayer2 subsystem. </summary>
        /// <value> The ArmLayer2 subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmLayerSubsystemBase ArmLayer2Subsystem { get; private set; }

        /// <summary> Bind ArmLayer2 subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem2( ArmLayerSubsystemBase subsystem )
        {
            if ( this.ArmLayer2Subsystem is object )
            {
                this.ArmLayer2Subsystem = null;
            }

            this.ArmLayer2Subsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }

        #endregion

        #region " DIGITAL OUTPUT SUBSYSTEM "

        /// <summary> Gets the digital output subsystem. </summary>
        /// <value> The digital output subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DigitalOutputSubsystemBase DigitalOutputSubsystem { get; private set; }

        /// <summary> Bind ArmLayer2 subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( DigitalOutputSubsystemBase subsystem )
        {
            if ( this.DigitalOutputSubsystem is object )
            {
                this.DigitalOutputSubsystem = null;
            }

            this.DigitalOutputSubsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }

        #endregion

        #region " SENSE SUBSYSTEM "

        /// <summary> Gets the Sense subsystem. </summary>
        /// <value> The Sense subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SenseSubsystemBase SenseSubsystem { get; private set; }

        /// <summary> Bind Sense subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( SenseSubsystemBase subsystem )
        {
            if ( this.SenseSubsystem is object )
            {
                this.SenseSubsystem.PropertyChanged -= this.SenseSubsystemPropertyChanged;
                this.SenseSubsystem = null;
            }

            this.SenseSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.SenseSubsystem.PropertyChanged += this.SenseSubsystemPropertyChanged;
            }
        }

        /// <summary> Gets the function mode changed. </summary>
        /// <value> The function mode changed. </value>
        public bool FunctionModeChanged => !Nullable.Equals( this.SenseFunctionSubsystem?.FunctionMode, this.SenseSubsystem?.FunctionMode );

        /// <summary> Handles the function modes changed action. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected virtual void HandleFunctionModesChanged( SenseSubsystemBase subsystem )
        {
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseSubsystemBase subsystem, string propertyName )
        {
            if ( this.InitializingComponents || subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            // Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            // Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
            switch ( propertyName ?? "" )
            {
                case nameof( SenseSubsystemBase.FunctionMode ):
                    {
                        if ( this.FunctionModeChanged )
                            this.HandleFunctionModesChanged( this.SenseSubsystem );
                        break;
                    }
            }
        }

        /// <summary> Sense subsystem property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SenseSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else if ( this._SubsystemToolStrip.InvokeRequired )
                {
                    // Because ToolStripItems derive directly from Component instead of from Control, their containing ToolStrip's invoke should be used
                    _ = this._SubsystemToolStrip.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE FUNCTION SUBSYSTEM "

        /// <summary> Gets the sense function subsystem. </summary>
        /// <value> The sense function subsystem. </value>
        public SenseFunctionSubsystemBase SenseFunctionSubsystem { get; private set; }

        /// <summary> Bind Sense function subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( SenseFunctionSubsystemBase subsystem )
        {
            if ( this.SenseFunctionSubsystem is object )
            {
                this.SenseFunctionSubsystem = null;
            }

            this.SenseFunctionSubsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }


        #endregion

        #region " BINNING SUBSYSTEM "

        /// <summary> Gets the <see cref="VI.BinningSubsystemBase"/>. </summary>
        /// <value> The <see cref="VI.BinningSubsystemBase"/>. </value>
        public BinningSubsystemBase BinningSubsystem { get; private set; }

        /// <summary> Bind <see cref="VI.BinningSubsystemBase"/>. </summary>
        /// <param name="subsystem"> The <see cref="VI.BinningSubsystemBase"/>. </param>
        public void BindSubsystem( BinningSubsystemBase subsystem )
        {
            if ( this.BinningSubsystem is object )
            {
                this.BinningSubsystem = null;
            }

            this.BinningSubsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }

        #endregion

        #region " ROUTE SUBSYSTEM "

        /// <summary> Gets the Route subsystem. </summary>
        /// <value> The Route subsystem. </value>
        public RouteSubsystemBase RouteSubsystem { get; private set; }

        /// <summary> Bind Route subsystem. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public void BindSubsystem( RouteSubsystemBase subsystem )
        {
            if ( this.RouteSubsystem is object )
            {
                this.RouteSubsystem = null;
            }

            this.RouteSubsystem = subsystem;
            if ( subsystem is object )
            {
            }
        }

        /// <summary>
        /// Configures four wire resistance scan using virtual instrument library functions.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="scanList">      List of scans. </param>
        /// <param name="sampleCount">   Number of samples. </param>
        /// <param name="triggerSource"> The trigger source. </param>
        /// <param name="triggerCount">  Number of triggers. </param>
        /// <returns> A String. </returns>
        public string ConfigureFourWireResistanceScan( string scanList, int sampleCount, ArmSources triggerSource, int triggerCount )
        {
            string result = string.Empty;
            // this is required for getting the correct function mode when fetching buffers.
            _ = this.SenseSubsystem.ApplyFunctionMode( SenseFunctionModes.ResistanceFourWire );
            _ = this.TriggerSubsystem.ApplyContinuousEnabled( false );
            _ = this.RouteSubsystem.ApplySelectedScanListType( ScanListType.None );
            _ = this.RouteSubsystem.ApplyScanListFunction( "(@1:10)", SenseFunctionModes.None, this.SenseSubsystem.FunctionModeReadWrites );
            _ = this.RouteSubsystem.ApplyScanListFunction( scanList, SenseFunctionModes.ResistanceFourWire, this.SenseSubsystem.FunctionModeReadWrites );
            _ = this.TriggerSubsystem.ApplyTriggerCount( sampleCount );
            _ = this.TriggerSubsystem.ApplyTriggerSource( TriggerSources.Immediate );
            _ = this.ArmLayer2Subsystem.ApplyArmCount( triggerCount );
            _ = this.ArmLayer2Subsystem.ApplyArmSource( triggerSource );
            if ( ArmSources.Timer == triggerSource )
            {
                _ = this.ArmLayer2Subsystem.ApplyTimerTimeSpan( TimeSpan.FromMilliseconds( 600d ) );
            }

            this.TraceSubsystem.ClearBuffer();
            _ = this.TraceSubsystem.ApplyPointsCount( sampleCount );
            _ = this.TraceSubsystem.ApplyFeedSource( FeedSources.Sense );
            _ = this.TraceSubsystem.ApplyFeedControl( FeedControls.Next );
            _ = this.RouteSubsystem.ApplySelectedScanListType( ScanListType.Internal );
            return result;
        }


        #endregion

        #region " CONFIGURE BUFFER STREAM "

        /// <summary> Restore Trigger State. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RestoreState()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            string title = this.Device.OpenResourceTitle;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{title} aborting trigger plan";
                _ = this.PublishInfo( $"{activity};. " );
                this.TriggerSubsystem.Abort();
                _ = this.Device.Session.QueryOperationCompleted();
                activity = $"{title} restoring device state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ResetKnownState();
                _ = this.Device.Session.QueryOperationCompleted();
                this.Device.ClearExecutionState();
                _ = this.Device.Session.QueryOperationCompleted();
                _ = this.TriggerSubsystem.ApplyContinuousEnabled( false );
                this.Device.Session.EnableServiceRequestWaitComplete();
                this.Device.Session.StatusPrompt = "Instrument state restored";
                this._StartBufferStreamMenuItem.Enabled = false;
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Configure trigger plan. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="triggerCount">  Number of triggers. </param>
        /// <param name="sampleCount">   Number of samples. </param>
        /// <param name="triggerSource"> The trigger source. </param>
        protected virtual void ConfigureTriggerPlan( int triggerCount, int sampleCount, TriggerSources triggerSource )
        {
            _ = this.TriggerSubsystem.ApplyContinuousEnabled( false );
            this.TriggerSubsystem.Abort();
            this.TriggerSubsystem.ClearTriggerModel();
            this.Device.Session.Wait();
            _ = this.Device.Session.ReadStatusRegister();
            _ = this.TriggerSubsystem.ApplyAutoDelayEnabled( false );
            this.TriggerSubsystem.Session.EnableServiceRequestWaitComplete();
            _ = this.TriggerSubsystem.Session.ReadStatusRegister();
            _ = this.ArmLayer1Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = this.ArmLayer1Subsystem.ApplyArmCount( 1 );
            _ = this.ArmLayer1Subsystem.ApplyArmLayerBypassMode( TriggerLayerBypassModes.Acceptor );
            _ = this.ArmLayer2Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = this.ArmLayer2Subsystem.ApplyArmCount( sampleCount );
            _ = this.ArmLayer2Subsystem.ApplyDelay( TimeSpan.Zero );
            _ = this.ArmLayer2Subsystem.ApplyArmLayerBypassMode( TriggerLayerBypassModes.Acceptor );
            _ = this.TriggerSubsystem.ApplyTriggerSource( triggerSource );
            _ = this.TriggerSubsystem.ApplyTriggerCount( triggerCount );
            _ = this.TriggerSubsystem.ApplyDelay( TimeSpan.Zero );
            _ = this.TriggerSubsystem.ApplyTriggerLayerBypassMode( TriggerLayerBypassModes.Acceptor );
        }

        /// <summary> Configure measurement. </summary>
        /// <remarks> David, 2020-07-25. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        private void ConfigureMeasurement()
        {
            _ = this.SystemSubsystem.ApplyAutoZeroEnabled( true );
            _ = this.SenseSubsystem.ApplyFunctionMode( My.Settings.Default.StreamBufferSenseFunctionMode );
            _ = this.Device.Session.QueryOperationCompleted();
            // verify that function mode change occurred
            if ( this.FunctionModeChanged )
            {
                throw new Core.OperationFailedException( $"Failed settings new {nameof( SenseFunctionSubsystemBase ).SplitWords()} to {My.Settings.Default.StreamBufferSenseFunctionMode} from {this.SenseFunctionSubsystem.FunctionMode}" );
            }

            _ = this.SenseFunctionSubsystem.ApplyPowerLineCycles( 1d );
            bool autoRangeEnabled = true;
            var actuaAutoRangeEnabled = this.SenseFunctionSubsystem.ApplyAutoRangeEnabled( autoRangeEnabled );
            if ( !Nullable.Equals( actuaAutoRangeEnabled, autoRangeEnabled ) )
            {
                throw new Core.OperationFailedException( $"Failed settings {nameof( SenseFunctionSubsystemBase.AutoRangeEnabled ).SplitWords()} to {autoRangeEnabled}" );
            }

            _ = this.SenseFunctionSubsystem.ApplyResolutionDigits( 9d );
            _ = this.SenseFunctionSubsystem.ApplyOpenLeadDetectorEnabled( true );
            // Me.SenseFunctionSubsystem.ApplyPowerLineCycles(1)
            // Me.SenseFunctionSubsystem.ApplyRange(My.Settings.NominalResistance)

        }

        /// <summary> Configure digital output. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        protected virtual void ConfigureDigitalOutput( DigitalOutputSubsystemBase subsystem )
        {
            subsystem.StartElapsedStopwatch();
            _ = subsystem.ApplyDigitalActiveLevels( new int[] { 1, 2, 3, 4 }, new DigitalActiveLevels[] { DigitalActiveLevels.Low, DigitalActiveLevels.Low, DigitalActiveLevels.Low, DigitalActiveLevels.Low } );
            subsystem.StopElapsedStopwatch();
        }

        /// <summary> Configure limit binning. </summary>
        protected virtual void ConfigureLimitBinning()
        {
            double expectedResistance = My.Settings.Default.NominalResistance;
            double resistanceTolerance = My.Settings.Default.ResistanceTolerance;
            double openLimit = My.Settings.Default.OpenLimit;
            int passOutputValue = My.Settings.Default.PasstBitmask;
            int failOutputValue = My.Settings.Default.FailBitmask;
            int overflowOutputValue = My.Settings.Default.OverflowBitmask;

            // sets the expected duration of the binning probe, which is used to wait before 
            // enabling the next measurement cycle. 
            this.BinningSubsystem.BinningStrobeDuration = My.Settings.Default.BinningStrobeDuration;
            _ = this.BinningSubsystem.ApplyPassSource( passOutputValue );

            // limit 2 is set for the nominal values
            double expectedValue = expectedResistance * (1d - resistanceTolerance);
            _ = this.BinningSubsystem.ApplyLimit2LowerLevel( expectedValue );
            expectedValue = expectedResistance * (1d + resistanceTolerance);
            _ = this.BinningSubsystem.ApplyLimit2UpperLevel( expectedValue );
            _ = this.BinningSubsystem.ApplyLimit2AutoClear( true );
            _ = this.BinningSubsystem.ApplyLimit2Enabled( true );
            _ = this.BinningSubsystem.ApplyLimit2LowerSource( failOutputValue );
            _ = this.BinningSubsystem.ApplyLimit2UpperSource( failOutputValue );

            // limit 1 is set for the overflow
            expectedValue = expectedResistance * resistanceTolerance;
            _ = this.BinningSubsystem.ApplyLimit1LowerLevel( expectedValue );
            _ = this.BinningSubsystem.ApplyLimit1UpperLevel( openLimit );
            _ = this.BinningSubsystem.ApplyLimit1AutoClear( true );
            _ = this.BinningSubsystem.ApplyLimit1Enabled( true );
            _ = this.BinningSubsystem.ApplyLimit1LowerSource( overflowOutputValue );
            _ = this.BinningSubsystem.ApplyLimit1UpperSource( overflowOutputValue );
            _ = this.BinningSubsystem.ApplyBinningStrobeEnabled( true );
        }

        /// <summary> Configure trace for fetching only. </summary>
        /// <param name="binningStrokeDuration"> Duration of the binning stroke. </param>
        protected virtual void ConfigureTrace( TimeSpan binningStrokeDuration )
        {
            this.TraceSubsystem.ClearBuffer();
            this.TraceSubsystem.BinningDuration = binningStrokeDuration;
            _ = this.TraceSubsystem.ApplyFeedSource( FeedSources.Sense );
            _ = this.TraceSubsystem.ApplyFeedControl( FeedControls.Never );
        }

        /// <summary> Builds buffer streaming description. </summary>
        /// <returns> A String. </returns>
        public virtual string BuildBufferStreamingDescription()
        {
            if ( this.InitializingComponents )
                return string.Empty;
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( $"Buffer streaming plan:" );
            _ = builder.AppendLine( $"Measurement: {My.Settings.Default.StreamBufferSenseFunctionMode}; auto zero; {1} NPLC; auto range; 8.5 digits." );
            _ = builder.AppendLine( $"Limits, Ω: Pass: {My.Settings.Default.NominalResistance}±{My.Settings.Default.NominalResistance * My.Settings.Default.ResistanceTolerance}; Open: {My.Settings.Default.OpenLimit}." );
            _ = builder.AppendLine( $"Binning Bitmasks: Pass={My.Settings.Default.PasstBitmask}; Fail={My.Settings.Default.FailBitmask}; Open={My.Settings.Default.OverflowBitmask}." );
            _ = builder.AppendLine( $"Using Scan Card: {this._UsingScanCardMenuItem.Checked}" );
            if ( this._UsingScanCardMenuItem.Checked )
            {
                _ = builder.AppendLine( $"Arm layer 1: {1} count; {ArmSources.Immediate}; {0} delay." );
                _ = builder.AppendLine( $"Arm layer 2: {My.Settings.Default.StreamTriggerCount} count; {My.Settings.Default.StreamBufferArmSource}; {0} delay." );
                _ = builder.AppendLine( $"Trigger: {ArmSources.Immediate}; {My.Settings.Default.ScanCardSampleCount} count; {0} delay; {TriggerLayerBypassModes.Acceptor} bypass." );
            }
            else
            {
                _ = builder.AppendLine( $"Arm layer 1: {1} count; {ArmSources.Immediate}; {0} delay." );
                _ = builder.AppendLine( $"Arm layer 2: {1} count; {ArmSources.Immediate}; {0} delay." );
                _ = builder.AppendLine( $"Trigger: {My.Settings.Default.StreamBufferTriggerSource}; {My.Settings.Default.StreamTriggerCount} count; {0} delay; {TriggerLayerBypassModes.Acceptor} bypass." );
            }

            return builder.ToString();
        }

        /// <summary> Configure buffer stream. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConfigureBufferStream()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Configure trigger plan";
                _ = this.PublishInfo( $"{activity};. " );
                this.ConfigureTriggerPlan( My.Settings.Default.StreamTriggerCount, 1, My.Settings.Default.StreamBufferTriggerSource );
                if ( (this.SenseSubsystem is object && (!this.SenseSubsystem.FunctionMode.HasValue || ( int? ) this.SenseSubsystem.FunctionMode != ( int? ) My.Settings.Default.StreamBufferSenseFunctionMode)) == true )
                    this.ConfigureMeasurement();
                if ( this.BinningSubsystem is object && (this.BinningSubsystem.Limit1AutoClear.GetValueOrDefault( false ) == false || this.BinningSubsystem.Limit1Enabled.GetValueOrDefault( false ) == false) )
                {
                    this.ConfigureLimitBinning();
                    this.ConfigureDigitalOutput( this.DigitalOutputSubsystem );
                }

                if ( this.TraceSubsystem is object && this.TraceSubsystem.FeedSource.GetValueOrDefault( FeedSources.None ) != FeedSources.Sense )
                {
                    this.ConfigureTrace( this.BinningSubsystem is object ? this.BinningSubsystem.BinningStrobeDuration : My.Settings.Default.BinningStrobeDuration );
                }

                if ( this._UsingScanCardMenuItem.Checked )
                {
                    // the fetched buffer includes only reading values.
                    this.TraceSubsystem.OrderedReadingElementTypes = new List<ReadingElementTypes>() { ReadingElementTypes.Reading };
                    _ = this.ConfigureFourWireResistanceScan( My.Settings.Default.ScanCardScanList, My.Settings.Default.ScanCardSampleCount, My.Settings.Default.StreamBufferArmSource, My.Settings.Default.StreamTriggerCount );
                }

                _ = this.BufferSubsystem is object
                    ? (this.DataGridView?.Bind( this.BufferSubsystem.BufferReadingsBindingList, true ))
                    : (this.DataGridView?.Bind( this.TraceSubsystem.BufferReadingsBindingList, true ));

                this.Device.Session.StatusPrompt = "Ready to commence buffer stream";
                this._StartBufferStreamMenuItem.Enabled = true;
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " COMMENCE BUFFER STREAM "

        /// <summary> Gets the buffer streaming enabled. </summary>
        /// <value> The buffer streaming enabled. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool BufferStreamingEnabled => this.BufferSubsystem is object
                    ? this.BufferSubsystem.BufferStreamingEnabled
                    : this.TraceSubsystem is object && this.TraceSubsystem.BufferStreamingEnabled;

        /// <summary> Gets the buffer streaming Active. </summary>
        /// <value> The buffer streaming Active. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool BufferStreamingActive => this.BufferSubsystem is object
                    ? this.BufferSubsystem.BufferStreamingActive
                    : this.TraceSubsystem is object && this.TraceSubsystem.BufferStreamingActive;

        /// <summary> Stops buffer streaming. </summary>
        private void StopBufferStreaming()
        {
            var timeout = TraceSubsystemBase.EstimateStreamStopTimeoutInterval( this.TraceSubsystem.StreamCycleDuration, My.Settings.Default.BufferStreamPollInterval, 1.5d );
            if ( this.BufferSubsystem is object )
                this.BufferSubsystem.BufferStreamTasker.AsyncCompleted -= this.BufferStreamTasker_AsyncCompleted;
            if ( this.TraceSubsystem is object )
                this.TraceSubsystem.BufferStreamTasker.AsyncCompleted -= this.BufferStreamTasker_AsyncCompleted;
            var (success, details) = this.BufferSubsystem is object ? this.BufferSubsystem.StopBufferStream( timeout ) : this.TraceSubsystem.StopBufferStream( timeout );
            if ( success )
            {
                this.TriggerSubsystem.Abort();
                this.ReadStatusRegister();
                _ = this.TriggerSubsystem.QueryTriggerState();
                this.ReadStatusRegister();
            }
            else
            {
                _ = this.PublishWarning( $"buffer streaming failed {details}" );
            }
        }

        /// <summary>
        /// Event handler. Called by BufferStreamTasker for asynchronous completed events.
        /// </summary>
        /// <remarks> David, 2020-08-06. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Asynchronous completed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void BufferStreamTasker_AsyncCompleted( object sender, AsyncCompletedEventArgs e )
        {
            string activity = "Handling buffer stream completed event";
            if ( sender is null || e is null )
                return;
            try
            {
                if ( e.Error is object )
                {
                    _ = this.PublishException( activity, e.Error );
                    this.StopBufferStreaming();
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Starts buffer streaming. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void StartBufferStreaming( TraceSubsystemBase subsystem )
        {
            this.Device.ClearExecutionState();
            this.TriggerSubsystem.Initiate();
            Core.ApplianceBase.DoEvents();
            subsystem.BufferStreamTasker.AsyncCompleted += this.BufferStreamTasker_AsyncCompleted;
            subsystem.StartBufferStream( this.TriggerSubsystem, My.Settings.Default.BufferStreamPollInterval, this.SenseSubsystem.FunctionUnit );
        }

        /// <summary> Starts buffer streaming. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        private void StartBufferStreaming( BufferSubsystemBase subsystem )
        {
            this.Device.ClearExecutionState();
            subsystem.BufferReadingUnit = this.SenseSubsystem.FunctionUnit;
            this.TriggerSubsystem.Initiate();
            Core.ApplianceBase.DoEvents();
            subsystem.BufferStreamTasker.AsyncCompleted += this.BufferStreamTasker_AsyncCompleted;
            subsystem.StartBufferStream( this.TriggerSubsystem, My.Settings.Default.BufferStreamPollInterval );
        }

        /// <summary> Commence buffer stream. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CommenceBufferStream()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Commencing buffer stream";
                _ = this.PublishInfo( $"{activity};. " );
                if ( this.BufferSubsystem is object )
                {
                    this.StartBufferStreaming( this.BufferSubsystem );
                }
                else
                {
                    this.StartBufferStreaming( this.TraceSubsystem );
                }

                if ( this.Device.StatusSubsystemBase.ErrorAvailable )
                {
                    this.StopBufferStreaming();
                    _ = this.PublishWarning( this.Device.StatusSubsystemBase.DeviceErrorReport );
                    this.Device.Session.StatusPrompt = "Streaming aborted";
                }
                else
                {
                    this.Device.Session.StatusPrompt = this._UsingScanCardMenuItem.Checked ? $"Streaming started--awaiting {My.Settings.Default.StreamBufferArmSource} triggers" : $"Streaming started--awaiting {My.Settings.Default.StreamBufferTriggerSource} triggers";
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: STREAM BUFFER "

        /// <summary> Starts buffer stream. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void StartBufferStream()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this._StartBufferStreamMenuItem.Checked )
                {
                    this.CommenceBufferStream();
                }
                else
                {
                    activity = $"{this.Device.ResourceNameCaption} stopping buffer streaming";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.StopBufferStreaming();
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Stream buffer menu item check state changed. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void StartBufferStreamMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.StartBufferStream();
            this._AssertBusTriggerButton.Enabled = this._StartBufferStreamMenuItem.Checked && (this._UsingScanCardMenuItem.Checked ? ArmSources.Bus == My.Settings.Default.StreamBufferArmSource : TriggerSources.Bus == My.Settings.Default.StreamBufferTriggerSource);
        }

        /// <summary> Configure streaming menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ConfigureStreamingMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            if ( this._ConfigureStreamingMenuItem.Checked )
            {
                if ( this._UsingScanCardMenuItem.Checked && this.SystemSubsystem.QueryFrontTerminalsSelected().Value )
                {
                    _ = this.InfoProvider.Annunciate( this._SubsystemToolStrip, Core.Forma.InfoProviderLevel.Error, "Set Rear Terminal" );
                    return;
                }

                this.ConfigureBufferStream();
            }
            else
            {
                this.RestoreState();
            }
        }

        /// <summary> Explain buffer streaming menu item click. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExplainBufferStreamingMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} building external scan trigger plan description";
            try
            {
                Core.Controls.PopupContainer.PopupInfo( this, this.BuildBufferStreamingDescription(), this._SubsystemToolStrip.Location, this.Size );
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                this.InfoProvider.SetIconPadding( this, -15 );
                _ = this.InfoProvider.Annunciate( this._SubsystemSplitButton, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
            }
        }

        /// <summary> Assert bus trigger button click. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void AssertBusTriggerButton_Click( object sender, EventArgs e )
        {
            this.TraceSubsystem.BusTriggerRequested = true;
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Buffer size text box validating. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void BufferSizeNumeric_Validating( object sender, CancelEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ApplyBufferCapacity( ( int ) this._BufferSizeNumeric.Value );
        }

        /// <summary> Handles the DataError event of the _dataGridView control. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DataGridView_DataError( object sender, DataGridViewDataErrorEventArgs e )
        {
            try
            {
                // prevent error reporting when adding a new row or editing a cell
                if ( sender is DataGridView grid )
                {
                    if ( grid.CurrentRow is object && grid.CurrentRow.IsNewRow )
                        return;
                    if ( grid.IsCurrentCellInEditMode )
                        return;
                    if ( grid.IsCurrentRowDirty )
                        return;
                    string activity = $"{this.Device.ResourceNameCaption} exception editing row {e.RowIndex} column {e.ColumnIndex};. {e.Exception.ToFullBlownString()}";
                    _ = this.PublishVerbose( activity );
                    _ = this.InfoProvider.Annunciate( grid, Core.Forma.InfoProviderLevel.Error, activity );
                }
            }
            catch
            {
            }
        }

        /// <summary> Displays a buffer menu item click. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DisplayBufferMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.DisplayBufferReadings();
        }

        /// <summary> Clears the buffer display menu item click. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ClearBufferDisplayMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ClearBufferDisplay();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
