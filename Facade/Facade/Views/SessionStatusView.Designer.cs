﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class SessionStatusView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionStatusView));
            StatusView = new StatusView();
            SessionView = new SessionView();
            _SelectorOpener = new Core.Controls.SelectorOpener();
            SuspendLayout();
            // 
            // _StatusView
            // 
            StatusView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            StatusView.BackColor = System.Drawing.Color.Transparent;
            StatusView.Dock = DockStyle.Bottom;
            StatusView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            StatusView.Location = new System.Drawing.Point(0, 266);
            StatusView.Margin = new Padding(0);
            StatusView.Name = "_StatusView";
            StatusView.PublishBindingSuccessEnabled = false;
            StatusView.Size = new System.Drawing.Size(383, 31);
            StatusView.TabIndex = 0;
            StatusView.TraceLogEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_StatusView.TraceLogEvent");
            StatusView.TraceLogLevel = TraceEventType.Information;
            StatusView.TraceShowEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_StatusView.TraceShowEvent");
            StatusView.TraceShowLevel = TraceEventType.Information;
            // 
            // _SessionView
            // 
            SessionView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            SessionView.Dock = DockStyle.Fill;
            SessionView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            SessionView.Location = new System.Drawing.Point(0, 0);
            SessionView.Margin = new Padding(3, 4, 3, 4);
            SessionView.Name = "_SessionView";
            SessionView.PublishBindingSuccessEnabled = false;
            SessionView.Size = new System.Drawing.Size(383, 266);
            SessionView.TabIndex = 1;
            SessionView.Termination = @"\n";
            SessionView.TraceLogEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_SessionView.TraceLogEvent");
            SessionView.TraceLogLevel = TraceEventType.Information;
            SessionView.TraceShowEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_SessionView.TraceShowEvent");
            SessionView.TraceShowLevel = TraceEventType.Information;
            // 
            // _SelectorOpener
            // 
            _SelectorOpener.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _SelectorOpener.BackColor = System.Drawing.Color.Transparent;
            _SelectorOpener.Dock = DockStyle.Bottom;
            _SelectorOpener.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SelectorOpener.Location = new System.Drawing.Point(0, 297);
            _SelectorOpener.Margin = new Padding(0);
            _SelectorOpener.Name = "_SelectorOpener";
            _SelectorOpener.PublishBindingSuccessEnabled = false;
            _SelectorOpener.Size = new System.Drawing.Size(383, 29);
            _SelectorOpener.TabIndex = 2;
            _SelectorOpener.TraceLogEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_SelectorOpener.TraceLogEvent");
            _SelectorOpener.TraceLogLevel = TraceEventType.Information;
            _SelectorOpener.TraceShowEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_SelectorOpener.TraceShowEvent");
            _SelectorOpener.TraceShowLevel = TraceEventType.Information;
            // 
            // SessionStatusView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(SessionView);
            Controls.Add(StatusView);
            Controls.Add(_SelectorOpener);
            Name = "SessionStatusView";
            Size = new System.Drawing.Size(383, 326);
            ResumeLayout(false);
        }

        private Core.Controls.SelectorOpener _SelectorOpener;
    }
}