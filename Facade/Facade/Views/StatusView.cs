using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core;
using isr.Core.Forma;
using isr.Core.Models;
using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{

    /// <summary> A control for handling status display and control. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2018-12-20, 6.0.6928.x. </para>
    /// </remarks>
    [Description( "Status display control" )]
    [System.Drawing.ToolboxBitmap( typeof( StatusView ), "StatusView" )]
    [ToolboxItem( true )]
    public partial class StatusView : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public StatusView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.ToolTip.IsBalloon = true;
            this._ReadTerminalsStateMenuItem.ToolTipText = My.Resources.Resources.TerminalsHint;
            this._UsingStatusSubsystemMenuItem.ToolTipText = My.Resources.Resources.StatusSubsystemOnlyHint;
            this._TraceMenuItem.ToolTipText = My.Resources.Resources.TraceHint;
            this._TraceShowLevelComboBox.ToolTipText = My.Resources.Resources.TraceHint;
            this._TraceLogLevelComboBox.ToolTipText = My.Resources.Resources.TraceHint;
            this._ToggleVisaEventHandlerMenuItem.ToolTipText = My.Resources.Resources.TraceHint;
            this._ServiceRequestAutoReadMenuItem.ToolTipText = My.Resources.Resources.ServiceRequestAutoReadHint;
            this._PollAutoReadMenuItem.ToolTipText = My.Resources.Resources.PollAutoReadHint;
            this.__ToolStrip.Name = "_ToolStrip";
            this.__SessionSettingsMenuItem.Name = "_SessionSettingsMenuItem";
            this.__DeviceSettingsMenuItem.Name = "_DeviceSettingsMenuItem";
            this.__UserInterfaceSettingsMenuItem.Name = "_UserInterfaceSettingsMenuItem";
            this.__UIBaseSettingsMenuItem.Name = "_UIBaseSettingsMenuItem";
            this.__UsingStatusSubsystemMenuItem.Name = "_UsingStatusSubsystemMenuItem";
            this.__ClearInterfaceMenuItem.Name = "_ClearInterfaceMenuItem";
            this.__ClearDeviceMenuItem.Name = "_ClearDeviceMenuItem";
            this.__ResetKnownStateMenuItem.Name = "_ResetKnownStateMenuItem";
            this.__ClearExecutionStateMenuItem.Name = "_ClearExecutionStateMenuItem";
            this.__InitKnownStateMenuItem.Name = "_InitKnownStateMenuItem";
            this.__ClearErrorReportMenuItem.Name = "_ClearErrorReportMenuItem";
            this.__ReadDeviceErrorsMenuItem.Name = "_ReadDeviceErrorsMenuItem";
            this.__ReadTerminalsStateMenuItem.Name = "_ReadTerminalsStateMenuItem";
            this.__StoreTimeoutMenuItem.Name = "_StoreTimeoutMenuItem";
            this.__RestoreTimeoutMenuItem.Name = "_RestoreTimeoutMenuItem";
            this.__SessionReadStatusByteMenuItem.Name = "_SessionReadStatusByteMenuItem";
            this.__SessionReadStandardEventRegisterMenuItem.Name = "_SessionReadStandardEventRegisterMenuItem";
            this.__SendBusTriggerMenuItem.Name = "_SendBusTriggerMenuItem";
            this.__PollEnabledMenuItem.Name = "_PollEnabledMenuItem";
            this.__PollSendMenuItem.Name = "_PollSendMenuItem";
            this.__ProgramServiceRequestEnableBitmaskMenuItem.Name = "_ProgramServiceRequestEnableBitmaskMenuItem";
            this.__ToggleVisaEventHandlerMenuItem.Name = "_ToggleVisaEventHandlerMenuItem";
            this.__ServiceRequestHandlerAddRemoveMenuItem.Name = "_ServiceRequestHandlerAddRemoveMenuItem";
            this._EditResourceNamesMenuItem.Name = "EditResourceNamesMenuItem";
        }

        /// <summary> Creates a new <see cref="StatusView"/> </summary>
        /// <returns> A <see cref="StatusView"/>. </returns>
        public static StatusView Create()
        {
            StatusView statusView = null;
            try
            {
                statusView = new StatusView();
                return statusView;
            }
            catch
            {
                statusView?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    // TO_DO: Doe we need this? Me.BindVisaSessionBase(Nothing)
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " SESSION BASE: ASSIGNMENT "

        /// <summary> Gets the session. </summary>
        /// <value> The session. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Pith.SessionBase SessionBase { get; private set; }

        /// <summary> Gets the sentinel indication having an open session. </summary>
        /// <value> The is open. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsSessionOpen => this.SessionBase is object && this.SessionBase.IsSessionOpen;

        /// <summary> Binds the Session base to its controls. </summary>
        /// <param name="visaSessionBase"> The visa session base. </param>
        private void BindSessionBase( VisaSessionBase visaSessionBase )
        {
            if ( visaSessionBase is null )
            {
                this.BindSessionBaseThis( null );
            }
            else
            {
                this.BindSessionBaseThis( visaSessionBase.Session );
            }
        }

        /// <summary> Bind session base. </summary>
        /// <param name="sessionBase"> The session. </param>
        private void BindSessionBaseThis( Pith.SessionBase sessionBase )
        {
            if ( this.SessionBase is object )
            {
                this.BindSessionViewModel( false, this.SessionBase );
                this.SessionBase = null;
            }

            this.BindStatusSubsystemBaseThis( null );
            if ( sessionBase is object )
            {
                this.SessionBase = sessionBase;
                this.BindSessionViewModel( true, this.SessionBase );
            }
        }

        /// <summary> Binds the Session view model to its controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The status subsystem view model. </param>
        private void BindSessionViewModel( bool add, Pith.SessionBase viewModel )
        {
            var binding = this.AddRemoveBinding( this._SessionDownDownButton, add, nameof( this.Enabled ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            binding = this.AddRemoveBinding( this._ClearInterfaceMenuItem, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.SupportsClearInterface ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            binding = this.AddRemoveBinding( this._SessionTimeoutTextBox, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.TimeoutCandidate ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation;
            binding.FormatString = "hh\\:mm\\:ss"; // "s\.fff";
            binding = this.AddRemoveBinding( this._ProgramServiceRequestEnableBitmaskMenuItem, add, nameof( ToolStripMenuItem.Checked ), viewModel, nameof( Pith.SessionBase.ServiceRequestEventEnabled ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            binding = this.AddRemoveBinding( this._ServiceRequestEnableCommandTextBox, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.ServiceRequestEnableCommandFormat ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation;
            binding = this.AddRemoveBinding( this._ServiceRequestBitMaskTextBox, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation;
            // this format string is not accepted
            // binding .FormatString = "0x{0:X2}"

            // this works
            // binding .FormatString = "X"

            // this does not work
            // binding .FormatString = "X2"

            // this does not work
            // binding .FormatInfo = New Pith.RegisterValueFormatProvider

            // this works
            if ( add )
            {
                binding.Format += BindingEventHandlers.DisplayRegisterEventHandler;
            }
            else
            {
                binding.Format -= BindingEventHandlers.DisplayRegisterEventHandler;
            }

            if ( add )
            {
                binding.Parse += BindingEventHandlers.ParseStatusRegisterEventHandler;
            }
            else
            {
                binding.Parse -= BindingEventHandlers.ParseStatusRegisterEventHandler;
            }

            binding = this.AddRemoveBinding( this._ServiceRequestSplitButton, add, nameof( this.Enabled ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            binding = this.AddRemoveBinding( this._PollSplitButton, add, nameof( this.Enabled ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            // termination
            binding = this.AddRemoveBinding( this._ReadTerminationEnabledMenuItem, add, nameof( ToolStripMenuItem.Checked ), viewModel, nameof( Pith.SessionBase.ReadTerminationCharacterEnabled ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            binding = this.AddRemoveBinding( this._ReadTerminationTextBox, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.ReadTerminationCharacter ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation;
            binding = this.AddRemoveBinding( this._WriteTerminationTextBox, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.TerminationSequence ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation;

            // notification
            this._SessionNotificationLevelComboBox.Enabled = true;
            if ( add )
            {
                this._SessionNotificationLevelComboBox.ComboBox.DataSource = viewModel.NotifySyncLevels.ToList();
                this._SessionNotificationLevelComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
                this._SessionNotificationLevelComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                this._SessionNotificationLevelComboBox.ComboBox.BindingContext = this.BindingContext;
                // this is necessary because the combo box binding does not set the data source value on it item change event
                this._SessionNotificationLevelComboBox.ComboBox.SelectedValueChanged += this.HandleNotificationLevelComboBoxValueChanged;
            }
            else
            {
                this._SessionNotificationLevelComboBox.ComboBox.DataSource = null;
                this._SessionNotificationLevelComboBox.ComboBox.SelectedValueChanged -= this.HandleNotificationLevelComboBoxValueChanged;
            }

            _ = this.AddRemoveBinding( this._SessionNotificationLevelComboBox, add, nameof( ToolStripComboBox.SelectedItem ), viewModel, nameof( Pith.SessionBase.MessageNotificationEvent ) );
            if ( add )
            {
                _ = this._SessionNotificationLevelComboBox.ComboBox.SelectValue( viewModel.MessageNotificationLevel );
            }

            _ = this.AddRemoveBinding( this._ToggleVisaEventHandlerMenuItem, add, nameof( CheckBox.Checked ), viewModel, nameof( Pith.SessionBase.ServiceRequestEventEnabled ), DataSourceUpdateMode.Never );
        }

        /// <summary> Handles the notification level combo box value changed. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleNotificationLevelComboBoxValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null || this.SessionBase is null )
                return;
            // _SessionNotificationLevelComboBox
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripComboBox combo )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"Setting session notification level to {combo.Text}";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    this.SessionBase.MessageNotificationLevel = combo.SelectedEnumValue( Pith.NotifySyncLevel.None );
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SESSION BASE: STATUS AND STANDARD REGISTERS "

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadStatusRegister()
        {
            string activity = string.Empty;
            try
            {
                if ( this.IsSessionOpen )
                {
                    activity = $"reading status byte after {this.SessionBase.StatusReadDelay.TotalMilliseconds}ms delay";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    _ = this.SessionBase.DelayReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Session read status byte menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionReadStatusByteMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} reading status byte";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    _ = this.SessionBase.ReadStatusRegister();
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Session read standard event register menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionReadStandardEventRegisterMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} reading status register";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    _ = this.SessionBase.ReadStatusRegister();
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                    if ( this.SessionBase.ErrorAvailable )
                    {
                        activity = $"{this.VisaSessionBase.ResourceNameCaption} error; not reading standard event registers";
                        this.SessionBase.StatusPrompt = activity;
                        _ = this.PublishInfo( $"{activity};. " );
                    }
                    else if ( this.SessionBase.MessageAvailable )
                    {
                        activity = $"{this.VisaSessionBase.ResourceNameCaption} message to read; not reading standard event registers";
                        this.SessionBase.StatusPrompt = activity;
                        _ = this.PublishInfo( $"{activity};. " );
                    }
                    else
                    {
                        activity = $"{this.VisaSessionBase.ResourceNameCaption} reading standard event registers";
                        this.SessionBase.StatusPrompt = activity;
                        _ = this.PublishInfo( $"{activity};. " );
                        this.VisaSessionBase.StartElapsedStopwatch();
                        this.VisaSessionBase.Session.ReadStandardEventRegisters();
                        this.VisaSessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                    }
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SESSION BASE: TIMEOUT "

        private void SessionTimeoutTextBox_Validating( object sender, CancelEventArgs e )
        {
            // this.SessionBase.TimeoutCandidate = TimeSpan.FromSeconds( double.Parse( _SessionTimeoutTextBox.Text ) ) ;
        }

        private void SessionTimeoutTextBox_Validated( object sender, EventArgs e )
        {
            // this.SessionBase.TimeoutCandidate = TimeSpan.FromSeconds( double.Parse( _SessionTimeoutTextBox.Text ) );
        }

        /// <summary> Applies and stores the timeout. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StoreTimeoutMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} storing current and setting a new timeout";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    this.SessionBase.StoreCommunicationTimeout( this.SessionBase.TimeoutCandidate );
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Restores timeout menu item click. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RestoreTimeoutMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} restoring previous timeout";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    this.SessionBase.RestoreCommunicationTimeout();
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SESSION BASE ACTIONS "

        /// <summary> Sends the bus trigger click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SendBusTrigger_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} sending bus trigger";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.AssertTrigger();
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SESSION BASE: DEBUG TEST CODE "

        /// <summary> Increments the given value. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> A TraceEventType. </returns>
        private static TraceEventType Increment( TraceEventType value )
        {
            switch ( value )
            {
                case TraceEventType.Critical:
                    {
                        value = TraceEventType.Error;
                        break;
                    }

                case TraceEventType.Error:
                    {
                        value = TraceEventType.Warning;
                        break;
                    }

                case TraceEventType.Warning:
                    {
                        value = TraceEventType.Information;
                        break;
                    }

                case TraceEventType.Information:
                    {
                        value = TraceEventType.Verbose;
                        break;
                    }

                case TraceEventType.Verbose:
                    {
                        value = TraceEventType.Critical;
                        break;
                    }
            }

            Application.DoEvents();
            return value;
        }

        /// <summary>
        /// Tool strip double click for testing the trace and notification event binding.
        /// </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void ToolStrip_DoubleClick( object sender, EventArgs e )
        {
            this.VisaSessionBase.TraceShowLevel = Increment( this.VisaSessionBase.TraceShowLevel );
            this.VisaSessionBase.TraceLogLevel = Increment( this.VisaSessionBase.TraceLogLevel );
            switch ( this.SessionBase.MessageNotificationLevel )
            {
                case Pith.NotifySyncLevel.None:
                    {
                        this.SessionBase.MessageNotificationLevel = Pith.NotifySyncLevel.Sync;
                        break;
                    }

                case Pith.NotifySyncLevel.Sync:
                    {
                        this.SessionBase.MessageNotificationLevel = Pith.NotifySyncLevel.Async;
                        break;
                    }

                case Pith.NotifySyncLevel.Async:
                    {
                        this.SessionBase.MessageNotificationLevel = Pith.NotifySyncLevel.None;
                        break;
                    }
            }
        }

        #endregion

        #region " STATUS SUBSYSTEM BASE: ASSIGNMENT  "

        /// <summary> Gets the status subsystem. </summary>
        /// <value> The status subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public StatusSubsystemBase StatusSubsystemBase { get; private set; }

        /// <summary> Bind status subsystem view model. </summary>
        /// <param name="visaSessionBase"> The visa session view model. </param>
        private void BindStatusSubsystemBase( VisaSessionBase visaSessionBase )
        {
            if ( visaSessionBase is null )
            {
                this.BindStatusSubsystemBaseThis( null );
            }
            else
            {
                this.BindStatusSubsystemBaseThis( visaSessionBase.StatusSubsystemBase );
            }
        }

        /// <summary> Binds the StatusSubsystem view model to its controls. </summary>
        /// <param name="statusSubsystemBase"> The status subsystem view model. </param>
        private void BindStatusSubsystemBaseThis( StatusSubsystemBase statusSubsystemBase )
        {
            if ( this.StatusSubsystemBase is object )
            {
                this.StatusSubsystemBase = null;
            }

            this._ClearErrorReportMenuItem.Enabled = statusSubsystemBase is object;
            this._ReadDeviceErrorsMenuItem.Enabled = statusSubsystemBase is object;
            this.StatusSubsystemBase = statusSubsystemBase;
        }

        #endregion

        #region " STATUS SUBSYSTEM BASE: DEVICE ERRORS "

        /// <summary> Clears the error report menu item click. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearErrorReportMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} clearing error report";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.StatusSubsystemBase.ClearErrorReport();
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads device errors menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadDeviceErrorsMenuItem_Click( object sender, EventArgs e )
        {
            string activity = $"{this.StatusSubsystemBase.ResourceNameCaption} reading device errors";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( sender is ToolStripMenuItem menuItem )
                {
                    activity = $"{this.StatusSubsystemBase.ResourceNameCaption} reading device errors";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    if ( this.SessionBase.IsErrorBitSet() )
                    {
                        var (success, details) = this.StatusSubsystemBase.TryQueryExistingDeviceErrors();
                        if ( !success )
                            _ = this.PublishWarning( $"Failed {activity}; {details}" );
                    }

                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE (VISA SESSION BASE): ASSIGNMENT "

#pragma warning disable IDE1006 // Naming Styles
        private VisaSessionBase __VisaSessionBase;

        private VisaSessionBase _VisaSessionBase
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__VisaSessionBase;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__VisaSessionBase != null )
                {
                    this.__VisaSessionBase.PropertyChanged -= this.VisaSessionBase_PropertyChanged;
                }

                this.__VisaSessionBase = value;
                if ( this.__VisaSessionBase != null )
                {
                    this.__VisaSessionBase.PropertyChanged += this.VisaSessionBase_PropertyChanged;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets the visa session base. </summary>
        /// <value> The visa session base. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase => this._VisaSessionBase;

        /// <summary> Binds the visa session to its controls. </summary>
        /// <param name="visaSessionBase"> The visa session view model. </param>
        public void BindVisaSessionBase( VisaSessionBase visaSessionBase )
        {
            if ( this.VisaSessionBase is object )
            {
                // RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
                this.VisaSessionBase.Closed -= this.HandleDeviceClosed;
                this.BindVisaSessionViewModel( false, this.VisaSessionBase );
                this.SessionSettings = null;
                this.DeviceSettings = null;
                this.UserInterfaceSettings = null;
                this._VisaSessionBase = null;
            }

            if ( visaSessionBase is object )
            {
                this._VisaSessionBase = visaSessionBase;
                // AddHandler visaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
                visaSessionBase.Closed += this.HandleDeviceClosed;
                this.SessionSettings = Pith.My.MySettings.Default;
                this.DeviceSettings = null;
                this.UserInterfaceSettings = null;
                this.BindVisaSessionViewModel( true, this.VisaSessionBase );
            }

            this.BindSessionBase( visaSessionBase );
            this.BindStatusSubsystemBase( visaSessionBase );
        }

        /// <summary> Handles the property changed. </summary>
        /// <param name="session">      The session. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( VisaSessionBase session, string propertyName )
        {
            if ( session is null || propertyName is null )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( this.VisaSessionBase.SubsystemSupportMode ):
                    {
                        this.UsingStatusSubsystemOnly = session.SubsystemSupportMode == SubsystemSupportMode.StatusOnly;
                        break;
                    }
            }
        }

        /// <summary> Visa session base property changed. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void VisaSessionBase_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null || this.SessionBase is null )
                return;
            string activity = string.Empty;
            VisaSessionBase session = sender as VisaSessionBase;
            try
            {
                this.HandlePropertyChanged( session, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> True to using status subsystem only. </summary>
        private bool _UsingStatusSubsystemOnly;

        /// <summary> Gets or sets the using status subsystem only. </summary>
        /// <value> The using status subsystem only. </value>
        public bool UsingStatusSubsystemOnly
        {
            get => this._UsingStatusSubsystemOnly;

            set {
                if ( value != this.UsingStatusSubsystemOnly )
                {
                    this._UsingStatusSubsystemOnly = value;
                    this.NotifyPropertyChanged();
                    this._VisaSessionBase.SubsystemSupportMode = value ? SubsystemSupportMode.StatusOnly : SubsystemSupportMode.Full;
                }
            }
        }

        /// <summary> Binds the visa session view model to its controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The device view model. </param>
        private void BindVisaSessionViewModel( bool add, VisaSessionBase viewModel )
        {

            // this could be enabled when the device opens
            this.ReadTerminalsState = null;
            // menus
            this._DeviceDropDownButton.Enabled = viewModel.IsSessionOpen;
            var binding = this.AddRemoveBinding( this._DeviceDropDownButton, add, nameof( this.Enabled ), viewModel, nameof( VI.VisaSessionBase.IsSessionOpen ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            // open options
            _ = this.AddRemoveBinding( this._UsingStatusSubsystemMenuItem, add, nameof( ToolStripMenuItem.Checked ), this, nameof( this.UsingStatusSubsystemOnly ) );

            // trace show level
            binding = this.AddRemoveBinding( this._TraceShowLevelComboBox.ComboBox, add, nameof( this.Enabled ), viewModel, nameof( VI.VisaSessionBase.IsSessionOpen ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            if ( add )
            {
                this._TraceShowLevelComboBox.ComboBox.DataSource = ViewModelTalkerBase.TraceEvents.ToArray();
                this._TraceShowLevelComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
                this._TraceShowLevelComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                this._TraceShowLevelComboBox.ComboBox.BindingContext = this.BindingContext;
                // this is necessary because the combo box binding does not set the data source value on it item change event
                this._TraceShowLevelComboBox.ComboBox.SelectedValueChanged += this.HandleTraceShowLevelComboBoxValueChanged;
            }
            else
            {
                this._TraceShowLevelComboBox.ComboBox.DataSource = null;
                this._TraceShowLevelComboBox.ComboBox.SelectedValueChanged -= this.HandleTraceShowLevelComboBoxValueChanged;
            }

            _ = this.AddRemoveBinding( this._TraceShowLevelComboBox, add, nameof( ToolStripComboBox.SelectedItem ), viewModel, nameof( VI.VisaSessionBase.TraceShowEvent ) );
            SelectItem( this._TraceShowLevelComboBox, viewModel.TraceShowLevel );

            // trace log level
            binding = this.AddRemoveBinding( this._TraceLogLevelComboBox.ComboBox, add, nameof( this.Enabled ), viewModel, nameof( VI.VisaSessionBase.IsSessionOpen ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            if ( add )
            {
                this._TraceLogLevelComboBox.ComboBox.DataSource = ViewModelTalkerBase.TraceEvents.ToList();
                this._TraceLogLevelComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
                this._TraceLogLevelComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                this._TraceLogLevelComboBox.ComboBox.BindingContext = this.BindingContext;
                // this is necessary because the combo box binding does not set the data source value on it item change event
                this._TraceLogLevelComboBox.ComboBox.SelectedValueChanged += this.HandleTraceLogLevelComboBoxValueChanged;
            }
            else
            {
                this._TraceLogLevelComboBox.ComboBox.DataSource = null;
                this._TraceLogLevelComboBox.ComboBox.SelectedValueChanged -= this.HandleTraceLogLevelComboBoxValueChanged;
            }

            _ = this.AddRemoveBinding( this._TraceLogLevelComboBox, add, nameof( ToolStripComboBox.SelectedItem ), viewModel, nameof( VI.VisaSessionBase.TraceLogEvent ) );
            SelectItem( this._TraceLogLevelComboBox, viewModel.TraceLogLevel );
            binding = this.AddRemoveBinding( this._UsingStatusSubsystemMenuItem, add, nameof( ToolStripMenuItem.Enabled ), viewModel, nameof( VI.VisaSessionBase.IsSessionOpen ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            if ( add )
            {
                binding.Format += BindingEventHandlers.InvertDisplayHandler;
            }
            else
            {
                binding.Format -= BindingEventHandlers.InvertDisplayHandler;
            }

            // SRQ
            _ = this.AddRemoveBinding( this._ServiceRequestHandlerAddRemoveMenuItem, add, nameof( CheckBox.Checked ), viewModel, nameof( VI.VisaSessionBase.ServiceRequestHandlerAssigned ), DataSourceUpdateMode.Never );
            _ = this.AddRemoveBinding( this._ServiceRequestAutoReadMenuItem, add, nameof( ToolStripMenuItem.Checked ), viewModel, nameof( this.VisaSessionBase.ServiceRequestAutoRead ) );

            // POLL
            binding = this.AddRemoveBinding( this._PollMessageStatusBitTextBox, add, nameof( Control.Text ), viewModel, nameof( VI.VisaSessionBase.PollMessageAvailableBitmask ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation;
            binding = this.AddRemoveBinding( this._PollIntervalTextBox, add, nameof( Control.Text ), viewModel, nameof( VI.VisaSessionBase.PollTimespan ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnValidation;
            binding.FormatString = @"s\.fff";
            _ = this.AddRemoveBinding( this._PollEnabledMenuItem, add, nameof( ToolStripMenuItem.Checked ), viewModel, nameof( VI.VisaSessionBase.PollEnabled ), DataSourceUpdateMode.Never );
            _ = this.AddRemoveBinding( this._PollAutoReadMenuItem, add, nameof( ToolStripMenuItem.Checked ), viewModel, nameof( VI.VisaSessionBase.PollAutoRead ) );
        }

        #region " visa session base (device base) EVENT HANDLERS "

        /// <summary> Handles the device Close. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceClosed( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.VisaSessionBase.ResourceNameCaption} UI handling device closed";
            }
            // TO_DO: Doe we need this? Me.BindStatusSubsystemBase(VisaSessionBase)
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the device opened. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceOpened( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.VisaSessionBase.ResourceNameCaption} UI handling device opened";
            }
            // TO_DO: Doe we need this? Me.BindStatusSubsystemBase(VisaSessionBase)
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #endregion

        #region " VISA SESSION EVENT HANDLERS: RESET "

        /// <summary> Clears interface. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearInterfaceMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} clearing interface";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.VisaSessionBase.StartElapsedStopwatch();
                    this.VisaSessionBase.ClearInterface();
                    this.VisaSessionBase.ElapsedTime = this.VisaSessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears device (SDC). </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearDeviceMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} clearing device active state";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.VisaSessionBase.StartElapsedStopwatch();
                    this.VisaSessionBase.ClearActiveState();
                    this.VisaSessionBase.ElapsedTime = this.VisaSessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears (CLS) the execution state menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/>
        /// instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearExecutionStateMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} clearing execution state";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.VisaSessionBase.StartElapsedStopwatch();
                    this.VisaSessionBase.ClearExecutionState();
                    this.VisaSessionBase.ElapsedTime = this.VisaSessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Resets (RST) the known state menu item click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ResetKnownStateMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} resetting known state";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.VisaSessionBase.StartElapsedStopwatch();
                    this.VisaSessionBase.ResetKnownState();
                    this.VisaSessionBase.ElapsedTime = this.VisaSessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Initializes to known state menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitKnownStateMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} initializing known state";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.VisaSessionBase.StartElapsedStopwatch();
                    this.VisaSessionBase.InitKnownState();
                    this.VisaSessionBase.ElapsedTime = this.VisaSessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE/SESSION: SERVICE REQUEST "

        /// <summary> Program service request enable bitmask menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ProgramServiceRequestEnableBitmaskMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    var bitmask = this.SessionBase.ServiceRequestEnabledBitmask ?? this.SessionBase.DefaultServiceRequestEnableBitmask;
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} applying service request enable bitmask 0x{( int ) bitmask:x2}";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    this.SessionBase.ApplyServiceRequestEnableBitmask( bitmask );
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Toggle visa event handler menu item check state changed. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ToggleVisaEventHandlerMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} turning {(this.SessionBase.ServiceRequestEventEnabled ? "OFF" : "ON")}  VISA event handling";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    if ( this.SessionBase.ServiceRequestEventEnabled )
                    {
                        this.SessionBase.DisableServiceRequestEventHandler();
                    }
                    else
                    {
                        this.SessionBase.EnableServiceRequestEventHandler();
                    }

                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Toggles the session service request handler . </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ServiceRequestHandlerAddRemoveMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} {(this.VisaSessionBase.ServiceRequestHandlerAssigned ? "REMOVING" : "ADDING")} VISA SRQ event handler";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.VisaSessionBase.StartElapsedStopwatch();
                    if ( this.VisaSessionBase.ServiceRequestHandlerAssigned )
                    {
                        this.VisaSessionBase.RemoveServiceRequestEventHandler();
                    }
                    else
                    {
                        this.VisaSessionBase.AddServiceRequestEventHandler();
                    }

                    this.VisaSessionBase.ElapsedTime = this.VisaSessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " POLL "

        /// <summary> Poll enabled menu item check state changed. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void PollEnabledMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem menuItem )
            {
                menuItem.Text = menuItem.Checked ? "Press to Stop" : "Press to Start";
            }
        }

        /// <summary> Poll enabled menu item check state changed. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PollEnabledMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    if ( this.VisaSessionBase.PollEnabled && this.VisaSessionBase.ServiceRequestHandlerAssigned )
                    {
                        _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Info, "Service request handler must be removed before polling" );
                    }
                    else
                    {
                        activity = $"{this.VisaSessionBase.ResourceNameCaption} {(this.VisaSessionBase.PollEnabled ? "STOPPING" : "STARTING")} status byte polling";
                        this.SessionBase.StatusPrompt = activity;
                        _ = this.PublishInfo( $"{activity};. " );
                        this.VisaSessionBase.StartElapsedStopwatch();
                        this.VisaSessionBase.PollSynchronizingObject = this;
                        this.VisaSessionBase.PollEnabled = !this.VisaSessionBase.PollEnabled;
                        this.VisaSessionBase.ElapsedTime = this.VisaSessionBase.ReadElapsedTime( true );
                    }
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Poll send menu item click. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PollSendMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} sending {this._PollWriteCommandTextBox.Text}";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    _ = this.SessionBase.WriteEscapedLine( this._PollWriteCommandTextBox.Text );
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " DEVICE: TERMINALS "

        /// <summary> State of the read terminals. </summary>
        private Func<bool?> _ReadTerminalsState;

        /// <summary> Gets or sets the delegate for reading the terminals state. </summary>
        /// <value> The delegate for reading the terminals state. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Func<bool?> ReadTerminalsState
        {
            get => this._ReadTerminalsState;

            set {
                this._ReadTerminalsState = value;
                this._ReadTerminalsStateMenuItem.Enabled = value is object;
            }
        }

        /// <summary> Read terminals state menu item click. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadTerminalsStateMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is ToolStripMenuItem menuItem && this.ReadTerminalsState is object )
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.VisaSessionBase.ResourceNameCaption} checking terminals state";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.VisaSessionBase.StartElapsedStopwatch();
                    _ = this.ReadTerminalsState.DynamicInvoke();
                    this.VisaSessionBase.ElapsedTime = this.VisaSessionBase.ReadElapsedTime( true );
                }
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " APP MENU: SETTINGS of SESSION BASE, DEVICE, UI "

        /// <summary> Shows the settings hint. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void ShowSettingsHint()
        {
            var toolTip = new ToolTip() { IsBalloon = true, ToolTipIcon = ToolTipIcon.Info, ToolTipTitle = "Settings hint" };
            // call twice per https://stackoverflow.com/questions/8716917/how-to-show-a-net-balloon-tooltip
            toolTip.Show( string.Empty, this._ToolStrip );
            toolTip.Show( "Opens the settings dialog", this._ToolStrip );
        }

        /// <summary> The session settings. </summary>
        private System.Configuration.ApplicationSettingsBase _SessionSettings;

        /// <summary> Gets or sets the session settings. </summary>
        /// <value> The session settings. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public System.Configuration.ApplicationSettingsBase SessionSettings
        {
            get => this._SessionSettings;

            set {
                this._SessionSettings = value;
                this._SessionSettingsMenuItem.Enabled = value is object;
            }
        }

        /// <summary> Session settings menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void SessionSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.SessionSettings is null )
            {
                this.SessionSettings = Pith.My.MySettings.Default;
            }

            if ( this.SessionSettings is object )
            {
                WindowsForms.EditConfiguration( "Session Settings", this.SessionSettings );
            }
        }

        /// <summary> The device settings. </summary>
        private System.Configuration.ApplicationSettingsBase _DeviceSettings;

        /// <summary> Gets or sets the Device settings. </summary>
        /// <value> The Device settings. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public System.Configuration.ApplicationSettingsBase DeviceSettings
        {
            get => this._DeviceSettings;

            set {
                this._DeviceSettings = value;
                this._DeviceSettingsMenuItem.Enabled = value is object;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Device tool strip menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void DeviceSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.DeviceSettings is object )
            {
                WindowsForms.EditConfiguration( "Device Settings", this.DeviceSettings );
            }
        }

        /// <summary> The user interface settings. </summary>
        private System.Configuration.ApplicationSettingsBase _UserInterfaceSettings;

        /// <summary> Gets or sets the UserInterface settings. </summary>
        /// <value> The UserInterface settings. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public System.Configuration.ApplicationSettingsBase UserInterfaceSettings
        {
            get => this._UserInterfaceSettings;

            set {
                this._UserInterfaceSettings = value;
                this._UserInterfaceSettingsMenuItem.Enabled = value is object;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> UserInterface tool strip menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void UserInterfaceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.UserInterfaceSettings is object )
            {
                WindowsForms.EditConfiguration( "User Interface Settings", this.UserInterfaceSettings );
            }
        }

        /// <summary> Base settings menu item click. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void UIBaseSettingsMenuItem_Click( object sender, EventArgs e )
        {
            if ( My.Settings.Default is object )
            {
                WindowsForms.EditConfiguration( "UI Base Settings", My.Settings.Default );
            }
        }

        /// <summary> Opens status subsystem menu item check state changed. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void OpenStatusSubsystemMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem menuItem )
            {
                menuItem.Text = menuItem.Checked ? "Using status subsystem only" : "Using all subsystems";
            }
        }

        /// <summary> Edit resource names menu item click. </summary>
        /// <remarks> David, 2020-06-07. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void EditResourceNamesMenuItem_Click( object sender, EventArgs e )
        {
            using var editorForm = new ResourceNameInfoEditorForm();
            _ = editorForm.ShowDialog( this );
        }

        #endregion

        #region " APP MENU: TRACE LEVEL "

        /// <summary>
        /// Event handler. Called by _DisplayTraceLevelComboBox for selected value changed events.
        /// </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleTraceShowLevelComboBoxValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.VisaSessionBase.ResourceNameCaption} selecting trace show level";
                this.VisaSessionBase.ApplyTalkerTraceLevel( ListenerType.Display, ( TraceEventType ) Conversions.ToInteger( this._TraceShowLevelComboBox.ComboBox.SelectedValue ) );
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _LogTraceLevelComboBox for selected value changed events.
        /// </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleTraceLogLevelComboBoxValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.VisaSessionBase.ResourceNameCaption} selecting trace log level";
                _ = this.PublishInfo( $"{activity};. " );
                this.VisaSessionBase.ApplyTalkerTraceLevel( ListenerType.Logger, SelectedValue( this._TraceLogLevelComboBox, TraceEventType.Information ) );
            }
            catch ( Exception ex )
            {
                this.SessionBase.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
