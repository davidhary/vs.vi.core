using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class SplitVisaView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            StatusLabel = new ToolStripStatusLabel();
            StatusStrip = new StatusStrip();
            ProgressBar = new Core.Controls.StatusStripCustomProgressBar();
            _Layout = new TableLayoutPanel();
            TraceMessagesBox = new Core.Forma.TraceMessagesBox();
            __TreePanel = new Core.Controls.TreePanel();
            __TreePanel.AfterNodeSelected += new EventHandler<TreeViewEventArgs>(TreePanel_AfterNodeSelected);
            StatusStrip.SuspendLayout();
            _Layout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)__TreePanel).BeginInit();
            __TreePanel.SuspendLayout();
            SuspendLayout();
            // 
            // _StatusLabel
            // 
            StatusLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            StatusLabel.Name = "_StatusLabel";
            StatusLabel.Overflow = ToolStripItemOverflow.Never;
            StatusLabel.Size = new System.Drawing.Size(663, 20);
            StatusLabel.Spring = true;
            StatusLabel.Text = "Loading...";
            StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _StatusStrip
            // 
            StatusStrip.Items.AddRange(new ToolStripItem[] { StatusLabel, ProgressBar });
            StatusStrip.Location = new System.Drawing.Point(0, 615);
            StatusStrip.Name = "_StatusStrip";
            StatusStrip.Padding = new Padding(1, 0, 16, 0);
            StatusStrip.Size = new System.Drawing.Size(780, 25);
            StatusStrip.TabIndex = 7;
            StatusStrip.Text = "StatusStrip1";
            // 
            // _ProgressBar
            // 
            ProgressBar.Maximum = 100;
            ProgressBar.Name = "_ProgressBar";
            ProgressBar.Size = new System.Drawing.Size(100, 23);
            ProgressBar.Text = "0 %";
            ProgressBar.Value = 0;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.Controls.Add(__TreePanel, 1, 1);
            _Layout.Location = new System.Drawing.Point(27, 167);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            _Layout.Size = new System.Drawing.Size(341, 217);
            _Layout.TabIndex = 13;
            // 
            // _TraceMessagesBox
            // 
            TraceMessagesBox.AlertLevel = TraceEventType.Warning;
            TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info;
            TraceMessagesBox.CaptionFormat = "{0} ≡";
            TraceMessagesBox.CausesValidation = false;
            TraceMessagesBox.Font = new System.Drawing.Font("Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            TraceMessagesBox.Location = new System.Drawing.Point(113, 52);
            TraceMessagesBox.Multiline = true;
            TraceMessagesBox.Name = "_TraceMessagesBox";
            TraceMessagesBox.PresetCount = 100;
            TraceMessagesBox.ReadOnly = true;
            TraceMessagesBox.ResetCount = 200;
            TraceMessagesBox.ScrollBars = ScrollBars.Both;
            TraceMessagesBox.Size = new System.Drawing.Size(601, 471);
            TraceMessagesBox.StatusPrompt = null;
            TraceMessagesBox.TabIndex = 15;
            TraceMessagesBox.TraceLevel = TraceEventType.Verbose;
            // 
            // _TreePanel
            // 
            __TreePanel.Dock = DockStyle.Fill;
            __TreePanel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TreePanel.Location = new System.Drawing.Point(3, 6);
            __TreePanel.Name = "__TreePanel";
            __TreePanel.Size = new System.Drawing.Size(335, 205);
            __TreePanel.SplitterDistance = 111;
            __TreePanel.TabIndex = 0;
            // 
            // SplitVisaView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(TraceMessagesBox);
            Controls.Add(_Layout);
            Controls.Add(StatusStrip);
            Name = "SplitVisaView";
            Size = new System.Drawing.Size(780, 640);
            StatusStrip.ResumeLayout(false);
            StatusStrip.PerformLayout();
            _Layout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)__TreePanel).EndInit();
            __TreePanel.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        private TableLayoutPanel _Layout;
        private Core.Controls.TreePanel __TreePanel;

        private Core.Controls.TreePanel _TreePanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TreePanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TreePanel != null)
                {
                    __TreePanel.AfterNodeSelected -= TreePanel_AfterNodeSelected;
                }

                __TreePanel = value;
                if (__TreePanel != null)
                {
                    __TreePanel.AfterNodeSelected += TreePanel_AfterNodeSelected;
                }
            }
        }
    }
}
