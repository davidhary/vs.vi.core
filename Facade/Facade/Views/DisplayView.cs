using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Forma;
using isr.Core.WinForms.BindingExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{

    /// <summary> A control for handling status display and control. </summary>
    /// <remarks>
    /// David, 2018-12-20, 6.0.6928. <para>
    /// <div>Icons made by
    /// <a href="https://www.flaticon.com/authors/darius-dan" title="Darius Dan">Darius Dan</a>
    /// from <a href="https://www.flaticon.com/" title="Flat Icon">www.flaticon.com</a></div>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    [Description( "Displays instrument measurement, status and error" )]
    [System.Drawing.ToolboxBitmap( typeof( DisplayView ), "DisplayView" )]
    [ToolboxItem( true )]
    public partial class DisplayView : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public DisplayView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.BindSettings();
            this.InitializingComponents = false;
            this.__Layout.Name = "_Layout";
            this.__ErrorLabel.Name = "_ErrorLabel";
        }

        /// <summary> Creates a new <see cref="DisplayView"/> </summary>
        /// <returns> A <see cref="DisplayView"/>. </returns>
        public static DisplayView Create()
        {
            DisplayView view = null;
            try
            {
                view = new DisplayView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM METHODS "

        /// <summary> Layout resize. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Layout_Resize( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.Height = this._Layout.Height;
        }

        #endregion

        #region " VISA SESSION BASE: DEVICE "

        /// <summary> The visa session base. </summary>

        /// <summary> Gets the visa session base. </summary>
        /// <value> The visa session base. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Binds the visa session base (device base) to its controls. </summary>
        /// <param name="visaSessionBase"> The visa session base (device base) view model. </param>
        public void BindVisaSessionBase( VisaSessionBase visaSessionBase )
        {
            if ( this.VisaSessionBase is object )
            {
                this.BindViewModel( false, this.VisaSessionBase );
                this.VisaSessionBase.Opened -= this.HandleDeviceOpened;
                this.VisaSessionBase.Closed -= this.HandleDeviceClosed;
                this.VisaSessionBase = null;
            }

            if ( visaSessionBase is object )
            {
                this.VisaSessionBase = visaSessionBase;
                this.VisaSessionBase.Opened += this.HandleDeviceOpened;
                this.VisaSessionBase.Closed += this.HandleDeviceClosed;
                this.BindViewModel( true, this.VisaSessionBase );
            }

            this.BindSessionBase( visaSessionBase );
            this.BindStatusSubsystemBase( visaSessionBase );
        }

        /// <summary> Bind view model. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The status subsystem view model. </param>
        private void BindViewModel( bool add, VisaSessionBase viewModel )
        {
            _ = this.AddRemoveBinding( this._ServiceRequestHandledLabel, add, nameof( this.Visible ), viewModel, nameof( this.VisaSessionBase.ServiceRequestHandlerAssigned ), DataSourceUpdateMode.Never );
        }

        /// <summary> Handles the device Close. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceClosed( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.VisaSessionBase.ResourceNameCaption} UI handling device closed";
            }
            // TO_DO: Doe we need this? Me.BindStatusSubsystemBase(Me.VisaSessionBase)
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the device opened. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceOpened( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.VisaSessionBase.ResourceNameCaption} UI handling device opened";
            }
            // TO_DO: Doe we need this? Me.BindStatusSubsystemBase(me.VisaSessionBase)
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SESSION BASE "

        /// <summary> Gets or sets the session. </summary>
        /// <value> The session. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Pith.SessionBase SessionBase { get; private set; }

        /// <summary> Binds the Session base to its controls. </summary>
        /// <param name="visaSessionBase"> The visa session base. </param>
        private void BindSessionBase( VisaSessionBase visaSessionBase )
        {
            if ( visaSessionBase is null )
            {
                this.BindSessionBaseThis( null );
            }
            else
            {
                this.BindSessionBaseThis( visaSessionBase.Session );
            }
        }

        /// <summary> Bind session base. </summary>
        /// <param name="sessionBase"> The session. </param>
        private void BindSessionBaseThis( Pith.SessionBase sessionBase )
        {
            if ( this.SessionBase is object )
            {
                this.BindSessionViewModel( false, this.SessionBase );
                this.SessionBase = null;
            }

            this._TitleStatusStrip.Visible = sessionBase is object;
            this._SessionReadingStatusStrip.Visible = sessionBase is object;
            this._StatusStrip.Visible = sessionBase is object || this.StatusSubsystemBase is object;
            // hide the sense and source status strip if nothing was assigned
            this._MeasureStatusStrip.Visible = this.MeasureToolstripSubsystemBase is object;
            this._SubsystemStatusStrip.Visible = this.SubsystemToolStripSentinel is object;
            if ( sessionBase is object )
            {
                this.SessionBase = sessionBase;
                this.BindSessionViewModel( true, this.SessionBase );
            }
        }

        /// <summary> Bind visibility. </summary>
        /// <param name="label">     The label. </param>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The status subsystem view model. </param>
        /// <param name="bitValue">  The bit value (must include only a single bit). </param>
        private void BindVisibility( Core.Controls.ToolStripStatusLabel label, bool add, Pith.SessionBase viewModel, Pith.ServiceRequests bitValue )
        {
            var binding = this.AddRemoveBinding( label, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.ServiceRequestStatus ), DataSourceUpdateMode.Never );
            void eventHandler( object sender, ConvertEventArgs e ) { if ( e is object && ReferenceEquals( e.DesiredType, typeof( bool ) ) ) { e.Value = 0 != (bitValue & ( Pith.ServiceRequests ) Conversions.ToInteger( e.Value )); } }
            if ( add )
            {
                binding.Format += eventHandler;
            }
            else
            {
                binding.Format -= eventHandler;
            }
        }

        /// <summary> Bind visibility. </summary>
        /// <param name="label">     The label. </param>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The status subsystem view model. </param>
        /// <param name="bitValue">  The bit value (must include only a single bit). </param>
        private void BindVisibility( Core.Controls.ToolStripStatusLabel label, bool add, Pith.SessionBase viewModel, Pith.StandardEvents bitValue )
        {
            var binding = this.AddRemoveBinding( label, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.StandardEventStatus ), DataSourceUpdateMode.Never );
            void eventHandler( object sender, ConvertEventArgs e ) { if ( e is object && ReferenceEquals( e.DesiredType, typeof( bool ) ) ) { e.Value = 0 != (bitValue & ( Pith.StandardEvents ) Conversions.ToInteger( e.Value )); } }
            if ( add )
            {
                binding.Format += eventHandler;
            }
            else
            {
                binding.Format -= eventHandler;
            }
        }

        /// <summary> Binds the Session view model to its controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The status subsystem view model. </param>
        private void BindSessionViewModel( bool add, Pith.SessionBase viewModel )
        {
            _ = this.AddRemoveBinding( this._TitleLabel, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.ResourceTitleCaption ), DataSourceUpdateMode.Never );
            _ = this.AddRemoveBinding( this._StatusRegisterLabel, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.StatusRegisterCaption ), DataSourceUpdateMode.Never );
            if ( My.Settings.Default.DisplayStandardServiceRequests )
            {
                this.BindVisibility( this._MeasurementEventBitLabel, add, viewModel, Pith.ServiceRequests.MeasurementEvent );
                this.BindVisibility( this._SystemEventBitLabel, add, viewModel, Pith.ServiceRequests.SystemEvent );
                this.BindVisibility( this._ErrorAvailableBitLabel, add, viewModel, Pith.ServiceRequests.ErrorAvailable );
                this.BindVisibility( this._MessageAvailableBitLabel, add, viewModel, Pith.ServiceRequests.MessageAvailable );
                this.BindVisibility( this._QuestionableSummaryBitLabel, add, viewModel, Pith.ServiceRequests.QuestionableEvent );
                this.BindVisibility( this._EventSummaryBitLabel, add, viewModel, Pith.ServiceRequests.StandardEvent );
                this.BindVisibility( this._ServiceRequestBitLabel, add, viewModel, Pith.ServiceRequests.RequestingService );
            }
            else
            {
                _ = this.AddRemoveBinding( this._MeasurementEventBitLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.HasMeasurementEvent ), DataSourceUpdateMode.Never );
                _ = this.AddRemoveBinding( this._SystemEventBitLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.HasSystemEvent ), DataSourceUpdateMode.Never );
                _ = this.AddRemoveBinding( this._ErrorAvailableBitLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.ErrorAvailable ), DataSourceUpdateMode.Never );
                _ = this.AddRemoveBinding( this._MessageAvailableBitLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.MessageAvailable ), DataSourceUpdateMode.Never );
                _ = this.AddRemoveBinding( this._QuestionableSummaryBitLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.HasQuestionableEvent ), DataSourceUpdateMode.Never );
                _ = this.AddRemoveBinding( this._EventSummaryBitLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.HasStandardEvent ), DataSourceUpdateMode.Never );
                _ = this.AddRemoveBinding( this._ServiceRequestBitLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.RequestingService ), DataSourceUpdateMode.Never );
            }

            _ = this.AddRemoveBinding( this._ServiceRequestEnabledLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.ServiceRequestEventEnabled ), DataSourceUpdateMode.Never );
            var binding = this.AddRemoveBinding( this._ServiceRequestEnableBitmaskLabel, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ), DataSourceUpdateMode.Never );
            if ( add )
            {
                binding.Format += BindingEventHandlers.DisplayX2EventHandler;
            }
            else
            {
                binding.Format += BindingEventHandlers.DisplayX2EventHandler;
            }

            binding = this.AddRemoveBinding( this._StandardEventEnableBitmaskLabel, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.StandardEventEnableBitmask ), DataSourceUpdateMode.Never );
            if ( add )
            {
                binding.Format += BindingEventHandlers.DisplayX2EventHandler;
            }
            else
            {
                binding.Format += BindingEventHandlers.DisplayX2EventHandler;
            }

            _ = this.AddRemoveBinding( this._SessionElapsedTimeLabel, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.ElapsedTimeCaption ), DataSourceUpdateMode.Never );
            _ = this.AddRemoveBinding( this._StandardRegisterLabel, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.StandardRegisterCaption ), DataSourceUpdateMode.Never );
            _ = this.AddRemoveBinding( this._StandardRegisterLabel, add, nameof( this.Visible ), viewModel, nameof( Pith.SessionBase.StandardEventStatusHasValue ), DataSourceUpdateMode.Never );
            this.BindVisibility( this._PowerOnEventLabel, add, viewModel, Pith.StandardEvents.PowerToggled );
            binding = this.AddRemoveBinding( this._SessionOpenCloseStatusLabel, add, nameof( ToolStripLabel.Image ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ), DataSourceUpdateMode.Never );
            static void eventHandler( object sender, ConvertEventArgs e ) => e.ToggleImage( My.Resources.Resources.user_online_2, My.Resources.Resources.user_offline_2 );
            // ConvertEventHandler eventHandler = ( sender, e ) => e.ToggleImage( My.Resources.Resources.user_online_2, My.Resources.Resources.user_offline_2 );
            if ( add )
            {
                binding.Format += eventHandler;
            }
            else
            {
                binding.Format -= eventHandler;
            }

            this._SessionOpenCloseStatusLabel.Image = My.Resources.Resources.user_offline_2;
        }

        #endregion

        #region " STATUS SUBSYSTEM BASE "

        /// <summary> Gets or sets the status subsystem. </summary>
        /// <value> The status subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public StatusSubsystemBase StatusSubsystemBase { get; private set; }

        /// <summary> Bind status subsystem view model. </summary>
        /// <param name="visaSessionBase"> The visa session view model. </param>
        private void BindStatusSubsystemBase( VisaSessionBase visaSessionBase )
        {
            if ( visaSessionBase is null )
            {
                this.BindStatusSubsystemBaseThis( null );
            }
            else
            {
                this.BindStatusSubsystemBaseThis( visaSessionBase.StatusSubsystemBase );
            }
        }

        /// <summary> Bind status subsystem base. </summary>
        /// <param name="statusSubsystemBase"> The status subsystem. </param>
        private void BindStatusSubsystemBaseThis( StatusSubsystemBase statusSubsystemBase )
        {
            if ( this.StatusSubsystemBase is object )
            {
                this.BindStatusSubsystemViewModel( false, this.StatusSubsystemBase );
                this.StatusSubsystemBase = null;
            }

            this._ErrorStatusStrip.Visible = statusSubsystemBase is object;
            this._StatusStrip.Visible = statusSubsystemBase is object || this.SessionBase is object;
            // hide the sense and source status strip if nothing was assigned
            this._MeasureStatusStrip.Visible = this.MeasureToolstripSubsystemBase is object;
            this._SubsystemStatusStrip.Visible = this.SubsystemToolStripSentinel is object;
            if ( statusSubsystemBase is object )
            {
                this.StatusSubsystemBase = statusSubsystemBase;
                this.BindStatusSubsystemViewModel( true, this.StatusSubsystemBase );
            }
        }

        /// <summary> Binds the StatusSubsystem view model to its controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The status subsystem view model. </param>
        private void BindStatusSubsystemViewModel( bool add, StatusSubsystemBase viewModel )
        {
            _ = this.AddRemoveBinding( this._ErrorLabel, add, nameof( Control.Text ), viewModel, nameof( VI.StatusSubsystemBase.CompoundErrorMessage ) );
            _ = this.AddRemoveBinding( this._ErrorLabel, add, nameof( this.ForeColor ), viewModel, nameof( VI.StatusSubsystemBase.ErrorForeColor ) );
            _ = this.AddRemoveBinding( this._MeasurementEventStatusLabel, add, nameof( this.Visible ), viewModel, nameof( VI.StatusSubsystemBase.MeasurementEventStatus ), DataSourceUpdateMode.Never );
            var binding = this.AddRemoveBinding( this._MeasurementEventStatusLabel, add, nameof( Control.Text ), viewModel, nameof( VI.StatusSubsystemBase.MeasurementEventStatus ), DataSourceUpdateMode.Never );
            if ( add )
            {
                binding.Format += BindingEventHandlers.DisplayX2EventHandler;
            }
            else
            {
                binding.Format += BindingEventHandlers.DisplayX2EventHandler;
            }
        }

        /// <summary> Event handler. Called by _ErrorLabel for text changed events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ErrorLabel_TextChanges( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( this.StatusSubsystemBase is object && (this.StatusSubsystemBase.HasDeviceError || this.StatusSubsystemBase.HasErrorReport) )
                {
                    string value = string.Empty;
                    value = this.StatusSubsystemBase.HasErrorReport ? this.StatusSubsystemBase.DeviceErrorReport : this.StatusSubsystemBase.CompoundErrorMessage;
                    if ( !string.IsNullOrWhiteSpace( value ) )
                    {
                        activity = $"logging error label text: {value}";
                        _ = this.PublishWarning( $"error reported: {value}" );
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " MEASURE TOOL STRIP SUBSYSTEMS "

        /// <summary>
        /// Gets or sets the measure tool strip subsystem sentinel, which indicates if a measure
        /// subsystem was assigned.
        /// </summary>
        /// <value> The measure subsystem sentinel. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private SubsystemBase MeasureToolstripSubsystemBase { get; set; }

        /// <summary> Releases the subsystem from its measure tool strip controls. </summary>
        /// <param name="subsystem"> The System subsystem. </param>
        public void ReleaseMeasureToolStrip( SubsystemBase subsystem )
        {
            if ( subsystem is object )
            {
                if ( subsystem is BufferSubsystemBase bufferSubsystem )
                {
                    this.BindMeasureToolStrip( false, bufferSubsystem );
                }
                else
                {
                    if ( subsystem is MultimeterSubsystemBase meterSubsystem )
                    {
                        this.BindMeasureToolStrip( false, meterSubsystem );
                    }
                    else
                    {
                        if ( subsystem is MeasureSubsystemBase measureSubsystem )
                        {
                            this.BindMeasureToolStrip( false, measureSubsystem );
                        }
                        else
                        {
                            if ( subsystem is MeasureCurrentSubsystemBase measureCurrentSubsystem )
                            {
                                this.BindMeasureToolStrip( false, measureCurrentSubsystem );
                            }
                            else
                            {
                                if ( subsystem is MeasureVoltageSubsystemBase measureVoltageSubsystem )
                                {
                                    this.BindMeasureToolStrip( false, measureVoltageSubsystem );
                                }
                                else
                                {
                                    if ( subsystem is SenseFunctionSubsystemBase SenseFunctionSubsystem )
                                    {
                                        this.BindMeasureToolStrip( false, SenseFunctionSubsystem );
                                    }
                                    else
                                    {
                                        if ( subsystem is SenseSubsystemBase SenseSubsystem )
                                        {
                                            this.BindMeasureToolStrip( false, SenseSubsystem );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        #region " BUFFER SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the buffer  subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The buffer subsystem. </param>
        public void BindMeasureToolStrip( BufferSubsystemBase subsystem )
        {
            if ( this.MeasureToolstripSubsystemBase is object )
            {
                this.ReleaseMeasureToolStrip( this.MeasureToolstripSubsystemBase );
            }

            this.MeasureToolstripSubsystemBase = subsystem;
            this._MeasureStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindMeasureToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the buffer subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The buffer subsystem. </param>
        private void BindMeasureToolStrip( bool add, BufferSubsystemBase subsystem )
        {
            // Me.AddRemoveBinding(Me._MeasureMetaStatusLabel, add, NameOf(Control.ForeColor), viewModel, NameOf(isr.VI.BufferSubsystemBase.FailureColor))
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.LastReadingStatus ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( BufferSubsystemBase.LastReadingStatus ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( BufferSubsystemBase.LastReading ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.Text ), subsystem, nameof( BufferSubsystemBase.LastReadingCaption ) );
            _ = this.AddRemoveBinding( this._MeasureElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.ElapsedTimeCaption ) );
            _ = this.AddRemoveBinding( this._LastReadingLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.LastRawReading ) );
        }

        #endregion

        #region " HARMONICS MEASURE SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Harmonics Measure Subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Harmonics Measure Subsystem. </param>
        public void BindMeasureToolStrip( HarmonicsMeasureSubsystemBase subsystem )
        {
            if ( this.MeasureToolstripSubsystemBase is object )
            {
                this.ReleaseMeasureToolStrip( this.MeasureToolstripSubsystemBase );
            }

            this.MeasureToolstripSubsystemBase = subsystem;
            this._MeasureStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindMeasureToolStrip( true, subsystem );
            }

            this.BindTerminalsDisplay( subsystem );
        }

        /// <summary> Binds the Harmonics Measure Subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Harmonics Measure Subsystem. </param>
        private void BindMeasureToolStrip( bool add, HarmonicsMeasureSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( this.ForeColor ), subsystem, nameof( HarmonicsMeasureSubsystemBase.FailureColor ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( Control.Text ), subsystem, nameof( HarmonicsMeasureSubsystemBase.FailureCode ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( HarmonicsMeasureSubsystemBase.FailureLongDescription ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( HarmonicsMeasureSubsystemBase.LastReading ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.Text ), subsystem, nameof( HarmonicsMeasureSubsystemBase.ReadingCaption ) );
            _ = this.AddRemoveBinding( this._MeasureElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( HarmonicsMeasureSubsystemBase.ElapsedTimeCaption ) );
            _ = this.AddRemoveBinding( this._LastReadingLabel, add, nameof( Control.Text ), subsystem, nameof( HarmonicsMeasureSubsystemBase.LastReading ) );
        }

        /// <summary> Binds the Harmonics Measure Subsystem to the its terminals controls. </summary>
        /// <param name="subsystem"> The System subsystem. </param>
        public void BindTerminalsDisplay( HarmonicsMeasureSubsystemBase subsystem )
        {
            if ( subsystem?.SupportsFrontTerminalsSelectionQuery == true )
            {
                if ( this.TerminalsSubsystemBase is object )
                {
                    this.ReleaseTerminalsDisplay( this.TerminalsSubsystemBase );
                }

                this.TerminalsSubsystemBase = subsystem;
                if ( subsystem is object )
                {
                    this.BindTerminalsDisplay( true, subsystem );
                }
            }
        }

        /// <summary> Binds the Harmonics Measure Subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The System subsystem. </param>
        private void BindTerminalsDisplay( bool add, HarmonicsMeasureSubsystemBase subsystem )
        {
            if ( subsystem.SupportsFrontTerminalsSelectionQuery )
            {
                _ = this.AddRemoveBinding( this._TerminalsStatusLabel, add, nameof( Control.Text ), subsystem, nameof( HarmonicsMeasureSubsystemBase.TerminalsCaption ) );
                this._TerminalsStatusLabel.Visible = add;
                if ( add )
                {
                    this._TerminalsStatusLabel.Text = subsystem.TerminalsCaption;
                }
            }
        }



        #endregion

        #region " MEASURE SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Measure subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Measure subsystem. </param>
        public void BindMeasureToolStrip( MeasureSubsystemBase subsystem )
        {
            if ( this.MeasureToolstripSubsystemBase is object )
            {
                this.ReleaseMeasureToolStrip( this.MeasureToolstripSubsystemBase );
            }

            this.MeasureToolstripSubsystemBase = subsystem;
            this._MeasureStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindMeasureToolStrip( true, subsystem );
            }

            this.BindTerminalsDisplay( subsystem );
        }

        /// <summary> Binds the Measure subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Measure subsystem. </param>
        private void BindMeasureToolStrip( bool add, MeasureSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( this.ForeColor ), subsystem, nameof( MeasureSubsystemBase.FailureColor ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureSubsystemBase.FailureCode ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( MeasureSubsystemBase.FailureLongDescription ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( MeasureSubsystemBase.LastReading ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.Text ), subsystem, nameof( MeasureSubsystemBase.ReadingCaption ) );
            _ = this.AddRemoveBinding( this._MeasureElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureSubsystemBase.ElapsedTimeCaption ) );
            _ = this.AddRemoveBinding( this._LastReadingLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureSubsystemBase.LastReading ) );
        }

        #endregion

        #region " MEASURE CURRENT SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Measure subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Measure Current subsystem. </param>
        public void BindMeasureToolStrip( MeasureCurrentSubsystemBase subsystem )
        {
            if ( this.MeasureToolstripSubsystemBase is object )
            {
                this.ReleaseMeasureToolStrip( this.MeasureToolstripSubsystemBase );
            }

            this.MeasureToolstripSubsystemBase = subsystem;
            this._MeasureStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindMeasureToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the Measure Current subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Measure Current. </param>
        private void BindMeasureToolStrip( bool add, MeasureCurrentSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( MeasureCurrentSubsystemBase.LastReading ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.Text ), subsystem, nameof( MeasureCurrentSubsystemBase.ReadingCaption ) );
            _ = this.AddRemoveBinding( this._MeasureElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureCurrentSubsystemBase.ElapsedTimeCaption ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( this.ForeColor ), subsystem, nameof( MeasureCurrentSubsystemBase.FailureColor ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureCurrentSubsystemBase.FailureCode ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( MeasureCurrentSubsystemBase.FailureLongDescription ) );
            _ = this.AddRemoveBinding( this._LastReadingLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureCurrentSubsystemBase.LastReading ) );
        }

        #endregion

        #region " MEASURE VOLTAGE SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Measure Voltage subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Measure Voltage subsystem. </param>
        public void BindMeasureToolStrip( MeasureVoltageSubsystemBase subsystem )
        {
            if ( this.MeasureToolstripSubsystemBase is object )
            {
                this.ReleaseMeasureToolStrip( this.MeasureToolstripSubsystemBase );
            }

            this.MeasureToolstripSubsystemBase = subsystem;
            this._MeasureStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindMeasureToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the Measure Voltage subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Measure Voltage subsystem. </param>
        private void BindMeasureToolStrip( bool add, MeasureVoltageSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( MeasureVoltageSubsystemBase.LastReading ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.Text ), subsystem, nameof( MeasureVoltageSubsystemBase.ReadingCaption ) );
            _ = this.AddRemoveBinding( this._MeasureElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureVoltageSubsystemBase.ElapsedTimeCaption ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( this.ForeColor ), subsystem, nameof( MeasureVoltageSubsystemBase.FailureColor ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureVoltageSubsystemBase.FailureCode ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( MeasureVoltageSubsystemBase.FailureLongDescription ) );
            _ = this.AddRemoveBinding( this._LastReadingLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureVoltageSubsystemBase.LastReading ) );
        }

        #endregion

        #region " MULTIMETER SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Multimeter subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Multimeter subsystem. </param>
        public void BindMeasureToolStrip( MultimeterSubsystemBase subsystem )
        {
            if ( this.MeasureToolstripSubsystemBase is object )
            {
                this.ReleaseMeasureToolStrip( this.MeasureToolstripSubsystemBase );
            }

            this.MeasureToolstripSubsystemBase = subsystem;
            this._MeasureStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindMeasureToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the Multimeter subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Multimeter subsystem. </param>
        private void BindMeasureToolStrip( bool add, MultimeterSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( MultimeterSubsystemBase.LastReading ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.Text ), subsystem, nameof( MultimeterSubsystemBase.ReadingCaption ) );
            _ = this.AddRemoveBinding( this._MeasureElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( MultimeterSubsystemBase.ElapsedTimeCaption ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( this.ForeColor ), subsystem, nameof( MultimeterSubsystemBase.FailureColor ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( Control.Text ), subsystem, nameof( MultimeterSubsystemBase.FailureCode ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( MultimeterSubsystemBase.FailureLongDescription ) );
            _ = this.AddRemoveBinding( this._LastReadingLabel, add, nameof( Control.Text ), subsystem, nameof( MultimeterSubsystemBase.LastReading ) );
        }

        #endregion

        #region " SENSE FUNCTION SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Sense Function subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Sense Function subsystem. </param>
        public void BindMeasureToolStrip( SenseFunctionSubsystemBase subsystem )
        {
            if ( this.MeasureToolstripSubsystemBase is object )
            {
                this.ReleaseMeasureToolStrip( this.MeasureToolstripSubsystemBase );
            }

            this.MeasureToolstripSubsystemBase = subsystem;
            this._MeasureStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindMeasureToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the Sense Function subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Sense Function subsystem. </param>
        private void BindMeasureToolStrip( bool add, SenseFunctionSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( SenseFunctionSubsystemBase.LastReading ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.Text ), subsystem, nameof( SenseFunctionSubsystemBase.ReadingCaption ) );
            _ = this.AddRemoveBinding( this._MeasureElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( SenseFunctionSubsystemBase.ElapsedTimeCaption ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( this.ForeColor ), subsystem, nameof( SenseFunctionSubsystemBase.FailureColor ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( Control.Text ), subsystem, nameof( SenseFunctionSubsystemBase.FailureCode ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( SenseFunctionSubsystemBase.FailureLongDescription ) );
            _ = this.AddRemoveBinding( this._LastReadingLabel, add, nameof( Control.Text ), subsystem, nameof( SenseFunctionSubsystemBase.LastReading ) );
        }

        #endregion

        #region " SENSE SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Sense subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Sense subsystem. </param>
        public void BindMeasureToolStrip( SenseSubsystemBase subsystem )
        {
            if ( this.MeasureToolstripSubsystemBase is object )
            {
                this.ReleaseMeasureToolStrip( this.MeasureToolstripSubsystemBase );
            }

            this.MeasureToolstripSubsystemBase = subsystem;
            this._MeasureStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindMeasureToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the Sense subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Sense Function subsystem. </param>
        private void BindMeasureToolStrip( bool add, SenseSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( SenseSubsystemBase.LastReading ) );
            _ = this.AddRemoveBinding( this._ReadingAmountLabel, add, nameof( ToolStripItem.Text ), subsystem, nameof( SenseSubsystemBase.ReadingCaption ) );
            _ = this.AddRemoveBinding( this._MeasureElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( SenseSubsystemBase.ElapsedTimeCaption ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( this.ForeColor ), subsystem, nameof( SenseSubsystemBase.FailureColor ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( Control.Text ), subsystem, nameof( SenseSubsystemBase.FailureCode ) );
            _ = this.AddRemoveBinding( this._MeasureMetaStatusLabel, add, nameof( ToolStripItem.ToolTipText ), subsystem, nameof( SenseSubsystemBase.FailureLongDescription ) );
            _ = this.AddRemoveBinding( this._LastReadingLabel, add, nameof( Control.Text ), subsystem, nameof( SenseSubsystemBase.LastReading ) );
        }

        #endregion

        #endregion

        #region " TERMINALS LABEL BINDING "

        /// <summary>
        /// Gets or sets the terminals subsystem sentinel, which indicates if a terminals subsystem was
        /// assigned.
        /// </summary>
        /// <value> The measure subsystem sentinel. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private SubsystemBase TerminalsSubsystemBase { get; set; }

        /// <summary> Releases the subsystem from its terminals controls. </summary>
        /// <param name="subsystem"> The System subsystem. </param>
        public void ReleaseTerminalsDisplay( SubsystemBase subsystem )
        {
            if ( subsystem is object )
            {
                if ( subsystem is SystemSubsystemBase sysSubsystem )
                {
                    this.BindTerminalsDisplay( false, sysSubsystem );
                }
                else
                {
                    if ( subsystem is MultimeterSubsystemBase meterSubsystem )
                    {
                        this.BindTerminalsDisplay( false, meterSubsystem );
                    }
                    else
                    {
                        if ( subsystem is MeasureSubsystemBase measureSubsystem )
                        {
                            this.BindTerminalsDisplay( false, measureSubsystem );
                        }
                    }
                }
            }
        }

        #region " MEASURE SUBSYSTEM "

        /// <summary> Binds the Measure subsystem to the its terminals controls. </summary>
        /// <param name="subsystem"> The System subsystem. </param>
        public void BindTerminalsDisplay( MeasureSubsystemBase subsystem )
        {
            if ( subsystem?.SupportsFrontTerminalsSelectionQuery == true )
            {
                if ( this.TerminalsSubsystemBase is object )
                {
                    this.ReleaseTerminalsDisplay( this.TerminalsSubsystemBase );
                }

                this.TerminalsSubsystemBase = subsystem;
                if ( subsystem is object )
                {
                    this.BindTerminalsDisplay( true, subsystem );
                }
            }
        }

        /// <summary> Binds the Measure subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The System subsystem. </param>
        private void BindTerminalsDisplay( bool add, MeasureSubsystemBase subsystem )
        {
            if ( subsystem.SupportsFrontTerminalsSelectionQuery )
            {
                _ = this.AddRemoveBinding( this._TerminalsStatusLabel, add, nameof( Control.Text ), subsystem, nameof( MeasureSubsystemBase.TerminalsCaption ) );
                this._TerminalsStatusLabel.Visible = add;
                if ( add )
                {
                    this._TerminalsStatusLabel.Text = subsystem.TerminalsCaption;
                }
            }
        }

        #endregion

        #region " MULTIMETER SUBSYSTEM "

        /// <summary> Binds the multimeter subsystem to the its terminals controls. </summary>
        /// <param name="subsystem"> The System subsystem. </param>
        public void BindTerminalsDisplay( MultimeterSubsystemBase subsystem )
        {
            if ( subsystem?.SupportsFrontTerminalsSelectionQuery == true )
            {
                if ( this.TerminalsSubsystemBase is object )
                {
                    this.ReleaseTerminalsDisplay( this.TerminalsSubsystemBase );
                }

                this.TerminalsSubsystemBase = subsystem;
                if ( subsystem is object )
                {
                    this.BindTerminalsDisplay( true, subsystem );
                }
            }
        }

        /// <summary> Binds the multimeter subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The System subsystem. </param>
        private void BindTerminalsDisplay( bool add, MultimeterSubsystemBase subsystem )
        {
            if ( subsystem.SupportsFrontTerminalsSelectionQuery )
            {
                _ = this.AddRemoveBinding( this._TerminalsStatusLabel, add, nameof( Control.Text ), subsystem, nameof( MultimeterSubsystemBase.TerminalsCaption ) );
                this._TerminalsStatusLabel.Visible = add;
                if ( add )
                {
                    this._TerminalsStatusLabel.Text = subsystem.TerminalsCaption;
                }
            }
        }

        #endregion

        #region " SYSTEM SUBSYSTEM "

        /// <summary> Binds the System subsystem to the its terminals controls. </summary>
        /// <param name="subsystem"> The System subsystem. </param>
        public void BindTerminalsDisplay( SystemSubsystemBase subsystem )
        {
            if ( subsystem?.SupportsFrontTerminalsSelectionQuery == true )
            {
                if ( this.TerminalsSubsystemBase is object )
                {
                    this.ReleaseTerminalsDisplay( this.TerminalsSubsystemBase );
                }

                this.TerminalsSubsystemBase = subsystem;
                if ( subsystem is object )
                {
                    this.BindTerminalsDisplay( true, subsystem );
                }
            }
        }

        /// <summary> Binds the System subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The System subsystem. </param>
        private void BindTerminalsDisplay( bool add, SystemSubsystemBase subsystem )
        {
            if ( subsystem.SupportsFrontTerminalsSelectionQuery )
            {
                _ = this.AddRemoveBinding( this._TerminalsStatusLabel, add, nameof( Control.Text ), subsystem, nameof( SystemSubsystemBase.TerminalsCaption ) );
                this._TerminalsStatusLabel.Visible = add;
                if ( add )
                {
                    this._TerminalsStatusLabel.Text = subsystem.TerminalsCaption;
                }
            }
        }

        #endregion

        #endregion

        #region " SUBSYSTEM TOOL STRIP SENTINEL "

        /// <summary> Gets or sets sentinel for the subsystem tool strip. </summary>
        /// <value> The sentinel for the subsystem tool strip. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private SubsystemBase SubsystemToolStripSentinel { get; set; }

        /// <summary> Releases the subsystem from its SUBSYSTEM controls. </summary>
        /// <param name="subsystem"> The System subsystem. </param>
        public void ReleaseSubsystemToolStrip( SubsystemBase subsystem )
        {
            if ( subsystem is object )
            {
                if ( subsystem is SourceSubsystemBase sourceSubsystem )
                {
                    this.BindSubsystemToolStrip( false, sourceSubsystem );
                }
                else
                {
                    if ( subsystem is ChannelSubsystemBase channelSubsystem )
                    {
                        this.BindSubsystemToolStrip( false, channelSubsystem );
                    }
                    else
                    {
                        if ( subsystem is TriggerSubsystemBase triggerSubsystem )
                        {
                            this.BindSubsystemToolStrip( false, triggerSubsystem );
                        }
                    }
                }
            }
        }


        #region " SOURCE SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Source subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Source subsystem. </param>
        public void BindSubsystemToolStrip( SourceSubsystemBase subsystem )
        {
            if ( this.SubsystemToolStripSentinel is object )
            {
                this.ReleaseSubsystemToolStrip( this.SubsystemToolStripSentinel );
            }

            this.SubsystemToolStripSentinel = subsystem;
            this._SubsystemStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindSubsystemToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the Source subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Source subsystem. </param>
        private void BindSubsystemToolStrip( bool add, SourceSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._SubsystemReadingLabel, add, nameof( Control.Text ), subsystem, nameof( SourceSubsystemBase.LevelCaption ) );
            _ = this.AddRemoveBinding( this._SubsystemElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( SubsystemBase.ElapsedTimeCaption ) );
        }

        #endregion

        #region " CHANNEL SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Channel subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Channel subsystem. </param>
        public void BindSubsystemToolStrip( ChannelSubsystemBase subsystem )
        {
            if ( this.SubsystemToolStripSentinel is object )
            {
                this.ReleaseSubsystemToolStrip( this.SubsystemToolStripSentinel );
            }

            this.SubsystemToolStripSentinel = subsystem;
            this._SubsystemStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindSubsystemToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the Channel subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Channel subsystem. </param>
        private void BindSubsystemToolStrip( bool add, ChannelSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._SubsystemReadingLabel, add, nameof( Control.Text ), subsystem, nameof( ChannelSubsystemBase.ClosedChannelsCaption ) );
            _ = this.AddRemoveBinding( this._SubsystemElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( SubsystemBase.ElapsedTimeCaption ) );
        }

        #endregion

        #region " TRIGGER SUBSYSTEM MODEL VIEW "

        /// <summary> Binds the Trigger subsystem to the its tool strip controls. </summary>
        /// <param name="subsystem"> The Trigger subsystem. </param>
        public void BindSubsystemToolStrip( TriggerSubsystemBase subsystem )
        {
            if ( this.SubsystemToolStripSentinel is object )
            {
                this.ReleaseSubsystemToolStrip( this.SubsystemToolStripSentinel );
            }

            this.SubsystemToolStripSentinel = subsystem;
            this._SubsystemStatusStrip.Visible = subsystem is object;
            if ( subsystem is object )
            {
                this.BindSubsystemToolStrip( true, subsystem );
            }
        }

        /// <summary> Binds the Trigger subsystem to the its tool strip controls. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="subsystem"> The Trigger subsystem. </param>
        private void BindSubsystemToolStrip( bool add, TriggerSubsystemBase subsystem )
        {
            _ = this.AddRemoveBinding( this._SubsystemReadingLabel, add, nameof( Control.Text ), subsystem, nameof( TriggerSubsystemBase.TriggerStateCaption ) );
            _ = this.AddRemoveBinding( this._SubsystemElapsedTimeLabel, add, nameof( Control.Text ), subsystem, nameof( SubsystemBase.ElapsedTimeCaption ) );
        }

        #endregion

        #endregion

        #region " BIND SETTINGS "

        /// <summary> Bind settings. </summary>
        private void BindSettings()
        {
            this._Layout.BackColor = My.Settings.Default.CharcoalColor;
            this._Layout.DataBindings.Add( new Binding( nameof( this.BackColor ), My.Settings.Default, nameof( My.Settings.Default.CharcoalColor ), true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._SessionReadingStatusStrip.BackColor = My.Settings.Default.CharcoalColor;
            this._SessionReadingStatusStrip.DataBindings.Add( new Binding( nameof( this.BackColor ), My.Settings.Default, nameof( My.Settings.Default.CharcoalColor ), true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._SubsystemStatusStrip.BackColor = My.Settings.Default.CharcoalColor;
            this._SubsystemStatusStrip.DataBindings.Add( new Binding( nameof( this.BackColor ), My.Settings.Default, nameof( My.Settings.Default.CharcoalColor ), true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._TitleStatusStrip.BackColor = My.Settings.Default.CharcoalColor;
            this._TitleStatusStrip.DataBindings.Add( new Binding( nameof( this.BackColor ), My.Settings.Default, nameof( My.Settings.Default.CharcoalColor ), true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ErrorStatusStrip.BackColor = My.Settings.Default.CharcoalColor;
            this._ErrorStatusStrip.DataBindings.Add( new Binding( nameof( this.BackColor ), My.Settings.Default, nameof( My.Settings.Default.CharcoalColor ), true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._StatusStrip.BackColor = My.Settings.Default.CharcoalColor;
            this._StatusStrip.DataBindings.Add( new Binding( nameof( this.BackColor ), My.Settings.Default, nameof( My.Settings.Default.CharcoalColor ), true, DataSourceUpdateMode.OnPropertyChanged ) );
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
