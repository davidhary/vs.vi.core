using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.EscapeSequencesExtensions;
using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A simple read write control. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-29 </para>
    /// </remarks>
    public partial class SessionView : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        public SessionView() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.BindSettings();
            this.RefreshCommandListThis();
            this.OnConnectionChanged();
            this.InitializingComponents = false;
            this.AppendTermination = true;
            this._ReadTextBox.Text = $@"'Append Termination' is {(this.AppendTermination ? "" : "un")}checked--Termination characters are {(this.AppendTermination ? "not" : "")}required;
Write *ESE 255{My.Settings.Default.Termination} if opening a stand alone VISA Session";
            this.__QueryButton.Name = "_QueryButton";
            this.__WriteButton.Name = "_WriteButton";
            this.__ReadButton.Name = "_ReadButton";
            this.__ReadDelayNumeric.Name = "_ReadDelayNumeric";
            this.__StatusReadDelayNumeric.Name = "_StatusReadDelayNumeric";
            this.__EraseDisplayMenuItem.Name = "_EraseDisplayMenuItem";
            this.__ClearSessionMenuItem.Name = "_ClearSessionMenuItem";
            this.__ReadStatusMenuItem.Name = "_ReadStatusMenuItem";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    if ( this.components is object )
                        this.components?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " BIND SETTINGS "

        /// <summary> Bind settings. </summary>
        private void BindSettings()
        {
            this._StatusReadDelayNumeric.NumericUpDownControl.Minimum = 0m;
            this._StatusReadDelayNumeric.NumericUpDownControl.Maximum = My.Settings.Default.MaximumDelay;
            _ = this._StatusReadDelayNumeric.NumericUpDownControl.DataBindings.Add( nameof( NumericUpDown.Maximum ), My.Settings.Default, nameof( My.Settings.Default.MaximumDelay ) );
            this._StatusReadDelayNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._StatusReadDelayNumeric.NumericUpDownControl.Value = My.Settings.Default.StatusReadDelay;
            _ = this._StatusReadDelayNumeric.NumericUpDownControl.DataBindings.Add( nameof( NumericUpDown.Value ), My.Settings.Default, nameof( My.Settings.Default.StatusReadDelay ) );
            this._ReadDelayNumeric.NumericUpDownControl.Minimum = 0m;
            this._ReadDelayNumeric.NumericUpDownControl.Maximum = My.Settings.Default.MaximumDelay;
            _ = this._ReadDelayNumeric.NumericUpDownControl.DataBindings.Add( nameof( NumericUpDown.Maximum ), My.Settings.Default, nameof( My.Settings.Default.MaximumDelay ) );
            this._ReadDelayNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._ReadDelayNumeric.NumericUpDownControl.Value = My.Settings.Default.ReadDelay;
            _ = this._ReadDelayNumeric.NumericUpDownControl.DataBindings.Add( nameof( NumericUpDown.Value ), My.Settings.Default, nameof( My.Settings.Default.ReadDelay ) );

            // Me._Termination = isr.Core.Agnostic.EscapeSequencesExtensions.NewLineEscape
            this._Termination = My.Settings.Default.Termination;
        }

        #endregion

        #region " VISA SESSION BASE (DEVICE BASE) "

        /// <summary> Gets the visa session base. </summary>
        /// <value> The visa session base. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Binds the visa session to its controls. </summary>
        /// <param name="visaSessionBase"> The visa session view model. </param>
        public void BindVisaSessionBase( VisaSessionBase visaSessionBase )
        {
            if ( this.VisaSessionBase is object )
            {
                this.VisaSessionBase.PropertyChanged -= this.VisaSessionPropertyChanged;
                this.VisaSessionBase = null;
            }

            if ( visaSessionBase is object )
            {
                this.VisaSessionBase = visaSessionBase;
                this.VisaSessionBase.PropertyChanged += this.VisaSessionPropertyChanged;
            }

            this.BindSessionBase( visaSessionBase );
        }

        /// <summary> Handles the session property changed action. </summary>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( VisaSessionBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( VI.VisaSessionBase.ServiceRequestReading ):
                    {
                        if ( this._ShowServiceRequestReadingMenuItem.Checked )
                        {
                            this.UpdateReadMessage( sender.ServiceRequestReading );
                        }

                        break;
                    }

                case nameof( VI.VisaSessionBase.PollReading ):
                    {
                        if ( this._ShowPollReadingsMenuItem.Checked )
                        {
                            this.UpdateReadMessage( sender.PollReading );
                        }

                        break;
                    }
            }
        }

        /// <summary> Handles the Session property changed event. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void VisaSessionPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is null || e is null )
                return;
            string activity = $"handling {nameof( VI.VisaSessionBase )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as VisaSessionBase, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SESSION BASE "

        /// <summary> Gets the session. </summary>
        /// <value> The session. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected Pith.SessionBase SessionBase { get; private set; }

        /// <summary> Binds the Session base to its controls. </summary>
        /// <param name="visaSessionBase"> The visa session base. </param>
        private void BindSessionBase( VisaSessionBase visaSessionBase )
        {
            if ( visaSessionBase is null )
            {
                this.BindSessionBaseThis( null );
            }
            else
            {
                this.BindSessionBaseThis( visaSessionBase.Session );
            }
        }

        /// <summary> Bind session base. </summary>
        /// <param name="sessionBase"> The session. </param>
        private void BindSessionBaseThis( Pith.SessionBase sessionBase )
        {
            if ( this.SessionBase is object )
            {
                this.SessionBase.StatusPrompt = $"Releasing session {this.SessionBase.ResourceNameCaption}";
                this.BindSessionViewModel( false, this.SessionBase );
                this.SessionBase = null;
            }

            if ( sessionBase is object )
            {
                this.SessionBase = sessionBase;
                this.BindSessionViewModel( true, this.SessionBase );
            }

            this.OnConnectionChanged();
        }

        /// <summary> Gets the sentinel indication having an open session. </summary>
        /// <value> The is open. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsSessionOpen => this.SessionBase is object && this.SessionBase.IsSessionOpen;

        /// <summary> Executes the connection changed action. </summary>
        private void OnConnectionChanged()
        {
            this._ClearSessionMenuItem.Enabled = this.IsSessionOpen;
            this._QueryButton.Enabled = this.IsSessionOpen;
            this._ReadButton.Enabled = this.IsSessionOpen;
            this._WriteButton.Enabled = this.IsSessionOpen;
            this._ReadStatusMenuItem.Enabled = this.IsSessionOpen;
            this._EraseDisplayMenuItem.Enabled = true;
            if ( this.IsSessionOpen )
            {
                this.SessionBase.StatusPrompt = "Session Open";
                this.ReadStatusRegister();
            }
            else if ( this.SessionBase is object )
            {
                this.SessionBase.StatusPrompt = "Session Not open; open session";
            }
        }

        /// <summary> Bind session view model. </summary>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The session view model. </param>
        private void BindSessionViewModel( bool add, Pith.SessionBase viewModel )
        {
            _ = this.AddRemoveBinding( this._ClearSessionMenuItem, add, nameof( this.Enabled ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ) );
            _ = this.AddRemoveBinding( this._QueryButton, add, nameof( this.Enabled ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ) );
            _ = this.AddRemoveBinding( this._ReadButton, add, nameof( this.Enabled ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ) );
            _ = this.AddRemoveBinding( this._WriteButton, add, nameof( this.Enabled ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ) );
            _ = this.AddRemoveBinding( this._ReadStatusMenuItem, add, nameof( this.Enabled ), viewModel, nameof( Pith.SessionBase.IsDeviceOpen ) );
            this._EraseDisplayMenuItem.Enabled = true;
        }

        /// <summary> Reads delay numeric value changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadDelayNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.SessionBase is object )
            {
                this.SessionBase.ReadDelay = TimeSpan.FromMilliseconds( ( double ) this._ReadDelayNumeric.Value );
            }
        }

        /// <summary> Status read delay numeric value changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void StatusReadDelayNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.SessionBase is object )
            {
                this.SessionBase.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) this._StatusReadDelayNumeric.Value );
            }
        }


        #endregion

        #region " COMMANDS "

        /// <summary> The termination. </summary>
        private string _Termination;

        /// <summary> Gets or sets the termination. </summary>
        /// <value> The termination. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string Termination
        {
            get => this._Termination;

            set {
                if ( !string.Equals( this.Termination, value ) )
                {
                    this._Termination = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Dictionary of commands. </summary>
        private Dictionary<string, string> _CommandDictionary;

        /// <summary> Command dictionary. </summary>
        /// <returns> A Dictionary(Of String, String) </returns>
        private IDictionary<string, string> CommandDictionary()
        {
            if ( this._CommandDictionary is null )
            {
                this._CommandDictionary = new Dictionary<string, string>();
                this.AddCommandThis( Pith.Ieee488.Syntax.ClearExecutionStateCommand );
                this.AddCommandThis( Pith.Ieee488.Syntax.IdentityQueryCommand );
                this.AddCommandThis( Pith.Ieee488.Syntax.OperationCompleteCommand );
                this.AddCommandThis( Pith.Ieee488.Syntax.OperationCompletedQueryCommand );
                this.AddCommandThis( Pith.Ieee488.Syntax.ResetKnownStateCommand );
                this.AddCommandThis( string.Format( System.Globalization.CultureInfo.CurrentCulture, Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat, 255 ) );
                this.AddCommandThis( Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand );
                this.AddCommandThis( string.Format( System.Globalization.CultureInfo.CurrentCulture, Pith.Ieee488.Syntax.StandardEventEnableCommandFormat, 255 ) );
                this.AddCommandThis( Pith.Ieee488.Syntax.StandardEventEnableQueryCommand );
                this.AddCommandThis( Pith.Ieee488.Syntax.WaitCommand );
            }

            return this._CommandDictionary;
        }

        /// <summary> Clears the commands. </summary>
        public void ClearCommands()
        {
            this._CommandDictionary.Clear();
        }

        /// <summary> Inserts a command. </summary>
        /// <param name="value">       The value. </param>
        /// <param name="termination"> The termination. </param>
        private void InsertCommandThis( string value, string termination )
        {
            if ( !this.CommandDictionary().ContainsKey( value ) )
            {
                var dix = new Dictionary<string, string>( this.CommandDictionary() );
                this.CommandDictionary().Clear();
                this.CommandDictionary().Add( value, value + termination );
                foreach ( string key in dix.Keys )
                    this.CommandDictionary().Add( key, dix[key] );
            }
        }

        /// <summary> Inserts a command. </summary>
        /// <param name="value"> The value. </param>
        private void InsertCommandThis( string value )
        {
            this.InsertCommandThis( value, My.Settings.Default.Termination );
        }

        /// <summary> Inserts a command described by value. </summary>
        /// <param name="value"> The value. </param>
        public void InsertCommand( string value )
        {
            this.InsertCommandThis( value );
        }

        /// <summary> Adds a command. </summary>
        /// <param name="value">       The value. </param>
        /// <param name="termination"> The termination. </param>
        private void AddCommandThis( string value, string termination )
        {
            if ( !this.CommandDictionary().ContainsKey( value ) )
            {
                this.CommandDictionary().Add( value, value + termination );
            }
        }

        /// <summary> Adds a command. </summary>
        /// <param name="value"> The value. </param>
        private void AddCommandThis( string value )
        {
            this.AddCommandThis( value, My.Settings.Default.Termination );
        }

        /// <summary> Adds a command. </summary>
        /// <param name="value"> The value. </param>
        public void AddCommand( string value )
        {
            this.AddCommandThis( value );
        }

        /// <summary> Refresh command list. </summary>
        private void RefreshCommandListThis()
        {
            this._WriteComboBox.Items.Clear();
            this._WriteComboBox.Items.AddRange( this.CommandDictionary().Values.ToArray() );
            this._WriteComboBox.SelectedIndex = 1;
        }

        /// <summary> Refresh command list. </summary>
        public void RefreshCommandList()
        {
            this.RefreshCommandListThis();
        }

        #endregion

        #region " READ And WRITE "

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadStatusRegister()
        {
            string activity = string.Empty;
            try
            {
                if ( this.IsSessionOpen )
                {
                    activity = $"reading status byte after {this.SessionBase.StatusReadDelay.TotalMilliseconds}ms delay";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    _ = this.SessionBase.DelayReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                this._ReadTextBox.Text = ex.ToFullBlownString();
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Gets or sets the append termination to the entered commands. </summary>
        /// <value> The append termination sentinel. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool AppendTermination
        {
            get => this._AppendTerminationMenuItem.Checked;

            set => this._AppendTerminationMenuItem.Checked = value;
        }

        /// <summary> Queries. </summary>
        /// <param name="textToWrite"> The text to write. </param>
        public void Query( string textToWrite )
        {
            this.Write( textToWrite );
            this.Read();
        }

        /// <summary> Writes. </summary>
        /// <param name="textToWrite"> The text to write. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void Write( string textToWrite )
        {
            Cursor.Current = Cursors.WaitCursor;
            string activity = string.Empty;
            try
            {
                activity = $"writing {textToWrite}";
                this.SessionBase.StatusPrompt = activity;
                _ = this.PublishInfo( $"{activity};. " );
                this.SessionBase.StartElapsedStopwatch();
                if ( this.AppendTermination )
                {
                    textToWrite = textToWrite.RemoveCommonEscapeSequences();
                    _ = this.SessionBase.WriteLine( textToWrite );
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime();
                }
                else
                {
                    textToWrite = textToWrite.ReplaceCommonEscapeSequences();
                    _ = this.SessionBase.Write( textToWrite );
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime();
                }

                activity = $"done {activity}";
                this.SessionBase.StatusPrompt = activity;
                _ = this.PublishInfo( $"{activity};. " );
            }
            catch ( Exception ex )
            {
                this._ReadTextBox.Text = ex.ToFullBlownString();
                _ = this.PublishException( activity, ex );
                this.SessionBase.StatusPrompt = $"failed {activity}";
            }
            finally
            {
                _ = this.SessionBase.ReadStatusRegister();
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Reads a message from the session. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public async void Read()
        {
            string activity = string.Empty;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                activity = "Awaiting read delay";
                this.SessionBase.StatusPrompt = activity;
                _ = this.PublishInfo( $"{activity};. " );
                this.SessionBase.StatusPrompt = activity;
                _ = this.PublishInfo( $"{activity};. " );
                Application.DoEvents();
                this.SessionBase.StartElapsedStopwatch();
                await System.Threading.Tasks.Task.Delay( this.SessionBase.ReadDelay );
                activity = "Reading status register";
                this.SessionBase.StatusPrompt = activity;
                _ = this.PublishInfo( $"{activity};. " );
                _ = this.SessionBase.ReadStatusRegister();
                if ( this.SessionBase.MessageAvailable )
                {
                    activity = "reading";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    string responseString = this.SessionBase.ReadLine();
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                    string message = responseString.InsertCommonEscapeSequences();
                    activity = $"received: {message}";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.UpdateReadMessage( message );
                }
                else
                {
                    activity = "nothing to read";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                }
            }
            catch ( Exception ex )
            {
                this._ReadTextBox.Text = ex.ToFullBlownString();
                _ = this.PublishException( activity, ex );
                this.SessionBase.StatusPrompt = $"failed {activity}";
            }
            finally
            {
                this.ReadStatusRegister();
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Erases the text boxes. </summary>
        public void Erase()
        {
            this._ReadTextBox.Text = string.Empty;
        }

        /// <summary> Clears the session. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ClearSession()
        {
            string activity = string.Empty;
            try
            {
                if ( this.SessionBase is object )
                {
                    activity = "clearing active state";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                    this.SessionBase.StartElapsedStopwatch();
                    this.SessionBase.ClearActiveState( this.SessionBase.DeviceClearRefractoryPeriod );
                    this.SessionBase.ElapsedTime = this.SessionBase.ReadElapsedTime( true );
                    activity = $"done {activity}";
                    this.SessionBase.StatusPrompt = activity;
                    _ = this.PublishInfo( $"{activity};. " );
                }
            }
            catch ( Exception ex )
            {
                this._ReadTextBox.Text = ex.ToFullBlownString();
                _ = this.PublishException( activity, ex );
                this.SessionBase.StatusPrompt = $"failed {activity}";
            }
            finally
            {
                this.ReadStatusRegister();
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Reads status button click. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadStatusMenuItem_Click( object sender, EventArgs e )
        {
            this.ReadStatusRegister();
        }

        /// <summary> Queries (write and then reads) the instrument. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void QueryButton_Click( object sender, EventArgs e )
        {
            this.Query( this._WriteComboBox.Text );
        }

        /// <summary> Writes a message to the instrument. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void WriteButton_Click( object sender, EventArgs e )
        {
            this.Write( this._WriteComboBox.Text );
        }

        /// <summary> Updates the read message described by message. </summary>
        /// <param name="message"> The message. </param>
        private void UpdateReadMessage( string message )
        {
            var builder = new System.Text.StringBuilder();
            if ( this._ReadTextBox.Text.Length > 0 )
            {
                _ = builder.AppendLine( this._ReadTextBox.Text );
            }

            _ = builder.Append( message );
            this._ReadTextBox.Text = builder.ToString();
        }

        /// <summary> Reads a message from the instrument. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadButton_Click( object sender, EventArgs e )
        {
            this.Read();
        }

        /// <summary> Event handler. Called by _ClearSessionButton for click events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ClearSessionMenuItem_Click( object sender, EventArgs e )
        {
            this.ClearSession();
        }

        /// <summary> Event handler. Called by clear for click events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EraseDisplayMenuItem_Click( object sender, EventArgs e )
        {
            this.Erase();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
