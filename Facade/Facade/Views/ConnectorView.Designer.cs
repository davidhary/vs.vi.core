﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class ConnectorView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectorView));
            _Layout = new TableLayoutPanel();
            _ConnectGroupBox = new GroupBox();
            _ResourceSelectorConnector = new Core.Controls.SelectorOpener();
            _ResourceInfoLabel = new Label();
            _IdentityTextBox = new TextBox();
            _Layout.SuspendLayout();
            _ConnectGroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_ConnectGroupBox, 1, 1);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(548, 419);
            _Layout.TabIndex = 1;
            // 
            // _ConnectGroupBox
            // 
            _ConnectGroupBox.Controls.Add(_ResourceSelectorConnector);
            _ConnectGroupBox.Controls.Add(_ResourceInfoLabel);
            _ConnectGroupBox.Controls.Add(_IdentityTextBox);
            _ConnectGroupBox.Location = new System.Drawing.Point(42, 106);
            _ConnectGroupBox.Name = "_ConnectGroupBox";
            _ConnectGroupBox.Size = new System.Drawing.Size(464, 207);
            _ConnectGroupBox.TabIndex = 2;
            _ConnectGroupBox.TabStop = false;
            _ConnectGroupBox.Text = "CONNECT";
            // 
            // _ResourceSelectorConnector
            // 
            _ResourceSelectorConnector.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _ResourceSelectorConnector.BackColor = System.Drawing.Color.Transparent;
            _ResourceSelectorConnector.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ResourceSelectorConnector.Location = new System.Drawing.Point(24, 21);
            _ResourceSelectorConnector.Margin = new Padding(0);
            _ResourceSelectorConnector.Name = "_ResourceSelectorConnector";
            _ResourceSelectorConnector.PublishBindingSuccessEnabled = false;
            _ResourceSelectorConnector.Size = new System.Drawing.Size(419, 29);
            _ResourceSelectorConnector.TabIndex = 5;
            _ResourceSelectorConnector.TraceLogEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_ResourceSelectorConnector.TraceLogEvent");
            _ResourceSelectorConnector.TraceLogLevel = TraceEventType.Information;
            _ResourceSelectorConnector.TraceShowEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_ResourceSelectorConnector.TraceShowEvent");
            _ResourceSelectorConnector.TraceShowLevel = TraceEventType.Information;
            // 
            // _ResourceInfoLabel
            // 
            _ResourceInfoLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _ResourceInfoLabel.Location = new System.Drawing.Point(28, 89);
            _ResourceInfoLabel.Name = "_ResourceInfoLabel";
            _ResourceInfoLabel.Size = new System.Drawing.Size(408, 102);
            _ResourceInfoLabel.TabIndex = 4;
            _ResourceInfoLabel.Text = resources.GetString("_ResourceInfoLabel.Text");
            // 
            // _IdentityTextBox
            // 
            _IdentityTextBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _IdentityTextBox.Location = new System.Drawing.Point(28, 61);
            _IdentityTextBox.Name = "_IdentityTextBox";
            _IdentityTextBox.ReadOnly = true;
            _IdentityTextBox.Size = new System.Drawing.Size(408, 25);
            _IdentityTextBox.TabIndex = 3;
            // 
            // ConnectorView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_Layout);
            Name = "ConnectorView";
            Size = new System.Drawing.Size(548, 419);
            _Layout.ResumeLayout(false);
            _ConnectGroupBox.ResumeLayout(false);
            _ConnectGroupBox.PerformLayout();
            ResumeLayout(false);
        }

        private TableLayoutPanel _Layout;
        private GroupBox _ConnectGroupBox;
        private Core.Controls.SelectorOpener _ResourceSelectorConnector;
        private Label _ResourceInfoLabel;
        private TextBox _IdentityTextBox;
    }
}