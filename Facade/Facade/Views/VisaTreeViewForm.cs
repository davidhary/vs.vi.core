using System;
using System.Windows.Forms;

namespace isr.VI.Facade
{

    /// <summary> Form for viewing the visa tree view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-01-03 </para>
    /// </remarks>
    public class VisaTreeViewForm : Core.Forma.ConsoleForm
    {

        #region " CONSTRUCTION and CLEAN UP "

        /// <summary> Default constructor. </summary>
        public VisaTreeViewForm() : base()
        {
            this.Name = "Visa.View.Form";
        }

        /// <summary> Creates a new VisaTreeViewForm. </summary>
        /// <returns> A VisaTreeViewForm. </returns>
        public static VisaTreeViewForm Create()
        {
            VisaTreeView VisaTreeView = null;
            try
            {
                VisaTreeView = new VisaTreeView();
                return Create( VisaTreeView );
            }
            catch
            {
                if ( VisaTreeView is object )
                {
                    VisaTreeView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Creates a new VisaTreeViewForm. </summary>
        /// <param name="visaTreeView"> The visa view. </param>
        /// <returns> A VisaTreeViewForm. </returns>
        public static VisaTreeViewForm Create( VisaTreeView visaTreeView )
        {
            VisaTreeViewForm result = null;
            try
            {
                result = new VisaTreeViewForm() { VisaTreeView = visaTreeView };
            }
            catch
            {
                if ( result is object )
                {
                    result.Dispose();
                }

                throw;
            }

            return result;
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    if ( this.VisaTreeViewDisposeEnabled && this.VisaTreeView is object )
                    {
                        var session = this.VisaTreeView.VisaSessionBase;
                        if ( this.VisaTreeView is object )
                            this.VisaTreeView.Dispose();
                        if ( session is object )
                            session.Dispose();
                    }

                    this.VisaTreeView = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding Visa View";
                // the talker control publishes the device messages which thus get published to the form message box.
                this.AddTalkerControl( this.VisaTreeView.VisaSessionBase.CandidateResourceTitle, this.VisaTreeView, false, false );
                // any form messages will be logged.
                activity = $"{this.Name}; adding log listener";
                this.AddListener( My.MyLibrary.Logger );
                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.CandidateResourceName ) )
                {
                    activity = $"{this.Name}; starting {this.VisaTreeView.VisaSessionBase.CandidateResourceName} selection task";
                    _ = this.VisaTreeView.VisaSessionBase.AsyncValidateResourceName( this.VisaTreeView.VisaSessionBase.CandidateResourceName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary> Gets the await the resource name validation task enabled. </summary>
        /// <value> The await the resource name validation task enabled. </value>
        protected bool AwaitResourceNameValidationTaskEnabled { get; set; }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.VisaTreeView.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
                if ( this.VisaTreeView.VisaSessionBase.IsValidatingResourceName() )
                {
                    if ( this.AwaitResourceNameValidationTaskEnabled )
                    {
                        activity = $"{this.Name}; awaiting {this.VisaSessionBase.CandidateResourceName} validation";
                        this.VisaTreeView.VisaSessionBase.AwaitResourceNameValidation( My.Settings.Default.ResourceNameSelectionTimeout );
                    }
                    else
                    {
                        activity = $"{this.Name}; validating {this.VisaSessionBase.CandidateResourceName}";
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if ( this.VisaTreeView is object )
                    this.VisaTreeView.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " VISA TREE VIEW "

        /// <summary> Gets the visa view. </summary>
        /// <value> The visa view. </value>
        public VisaTreeView VisaTreeView { get; set; }

        /// <summary> Gets the visa view dispose enabled. </summary>
        /// <value> The visa view dispose enabled. </value>
        public bool VisaTreeViewDisposeEnabled { get; set; }

        /// <summary> Gets the locally assigned visa session if any. </summary>
        /// <value> The visa session. </value>
        protected VisaSessionBase VisaSessionBase => this.VisaTreeView.VisaSessionBase;

        /// <summary> Creates view. </summary>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        private void CreateViewView( string candidateResourceName, string candidateResourceTitle )
        {
            try
            {
                this.VisaTreeView = new VisaTreeView( new VisaSession() );
                this.VisaSessionBase.CandidateResourceTitle = candidateResourceTitle;
                this.VisaSessionBase.CandidateResourceName = candidateResourceName;
            }
            catch
            {
                this.VisaTreeView?.Dispose();
                throw;
            }
        }

        #endregion

        #region " SHOW DIALOG "

        /// <summary> Shows the form dialog. </summary>
        /// <param name="owner">                  The owner. </param>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public DialogResult ShowDialog( IWin32Window owner, string candidateResourceName, string candidateResourceTitle )
        {
            VisaTreeView VisaTreeView = null;
            try
            {
                this.CreateViewView( candidateResourceName, candidateResourceTitle );
                return this.ShowDialog( owner, VisaTreeView );
            }
            catch
            {
                if ( VisaTreeView is object )
                {
                    VisaTreeView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Shows the form dialog. </summary>
        /// <param name="owner">        The owner. </param>
        /// <param name="visaTreeView"> The visa view. </param>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public DialogResult ShowDialog( IWin32Window owner, VisaTreeView visaTreeView )
        {
            this.VisaTreeView = visaTreeView;
            return this.ShowDialog( owner );
        }

        /// <summary> Shows the form. </summary>
        /// <param name="owner">                  The owner. </param>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        public void Show( IWin32Window owner, string candidateResourceName, string candidateResourceTitle )
        {
            VisaTreeView VisaTreeView = null;
            try
            {
                this.CreateViewView( candidateResourceName, candidateResourceTitle );
                this.Show( owner, VisaTreeView );
            }
            catch
            {
                if ( VisaTreeView is object )
                {
                    VisaTreeView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Shows the form. </summary>
        /// <param name="owner">        The owner. </param>
        /// <param name="visaTreeView"> The visa view. </param>
        public void Show( IWin32Window owner, VisaTreeView visaTreeView )
        {
            this.VisaTreeView = visaTreeView;
            this.Show( owner );
        }

        #endregion

    }
}
