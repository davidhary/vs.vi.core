﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class DisplayView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _MeasureStatusStrip = new StatusStrip();
            _TerminalsStatusLabel = new Core.Controls.ToolStripStatusLabel();
            _MeasureMetaStatusLabel = new Core.Controls.ToolStripStatusLabel();
            _ReadingAmountLabel = new Core.Controls.ToolStripStatusLabel();
            _MeasureElapsedTimeLabel = new Core.Controls.ToolStripStatusLabel();
            _StandardRegisterLabel = new Core.Controls.ToolStripStatusLabel();
            _StatusRegisterLabel = new Core.Controls.ToolStripStatusLabel();
            __Layout = new TableLayoutPanel();
            __Layout.Resize += new EventHandler(Layout_Resize);
            _SessionReadingStatusStrip = new StatusStrip();
            _LastReadingLabel = new Core.Controls.ToolStripStatusLabel();
            _SessionElapsedTimeLabel = new Core.Controls.ToolStripStatusLabel();
            _SubsystemStatusStrip = new StatusStrip();
            _SubsystemReadingLabel = new Core.Controls.ToolStripStatusLabel();
            _SubsystemElapsedTimeLabel = new Core.Controls.ToolStripStatusLabel();
            _TitleStatusStrip = new StatusStrip();
            _TitleLabel = new Core.Controls.ToolStripStatusLabel();
            _SessionOpenCloseStatusLabel = new Core.Controls.ToolStripStatusLabel();
            _ErrorStatusStrip = new StatusStrip();
            __ErrorLabel = new Core.Controls.ToolStripStatusLabel();
            __ErrorLabel.TextChanged += new EventHandler(ErrorLabel_TextChanges);
            _StatusStrip = new StatusStrip();
            _ServiceRequestEnableBitmaskLabel = new Core.Controls.ToolStripStatusLabel();
            _StandardEventEnableBitmaskLabel = new Core.Controls.ToolStripStatusLabel();
            _ServiceRequestEnabledLabel = new Core.Controls.ToolStripStatusLabel();
            _OperationSummaryBitLabel = new Core.Controls.ToolStripStatusLabel();
            _ServiceRequestBitLabel = new Core.Controls.ToolStripStatusLabel();
            _EventSummaryBitLabel = new Core.Controls.ToolStripStatusLabel();
            _MessageAvailableBitLabel = new Core.Controls.ToolStripStatusLabel();
            _QuestionableSummaryBitLabel = new Core.Controls.ToolStripStatusLabel();
            _ErrorAvailableBitLabel = new Core.Controls.ToolStripStatusLabel();
            _SystemEventBitLabel = new Core.Controls.ToolStripStatusLabel();
            _MeasurementEventBitLabel = new Core.Controls.ToolStripStatusLabel();
            _PowerOnEventLabel = new Core.Controls.ToolStripStatusLabel();
            _UserRequestEventLabel = new Core.Controls.ToolStripStatusLabel();
            _CommandErrorEventLabel = new Core.Controls.ToolStripStatusLabel();
            _ExecutionErrorEventLabel = new Core.Controls.ToolStripStatusLabel();
            _DeviceDependentErrorEventToolLabel = new Core.Controls.ToolStripStatusLabel();
            _QueryErrorEventLabel = new Core.Controls.ToolStripStatusLabel();
            _OperationCompleteEventLabel = new Core.Controls.ToolStripStatusLabel();
            _ServiceRequestHandledLabel = new Core.Controls.ToolStripStatusLabel();
            _MeasurementEventStatusLabel = new Core.Controls.ToolStripStatusLabel();
            _MeasureStatusStrip.SuspendLayout();
            __Layout.SuspendLayout();
            _SessionReadingStatusStrip.SuspendLayout();
            _SubsystemStatusStrip.SuspendLayout();
            _TitleStatusStrip.SuspendLayout();
            _ErrorStatusStrip.SuspendLayout();
            _StatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _MeasureStatusStrip
            // 
            _MeasureStatusStrip.BackColor = System.Drawing.Color.Transparent;
            _MeasureStatusStrip.Font = new System.Drawing.Font("Segoe UI", 20.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MeasureStatusStrip.GripMargin = new Padding(0);
            _MeasureStatusStrip.Items.AddRange(new ToolStripItem[] { _TerminalsStatusLabel, _MeasureMetaStatusLabel, _ReadingAmountLabel, _MeasureElapsedTimeLabel });
            _MeasureStatusStrip.Location = new System.Drawing.Point(0, 22);
            _MeasureStatusStrip.Name = "_MeasureStatusStrip";
            _MeasureStatusStrip.ShowItemToolTips = true;
            _MeasureStatusStrip.Size = new System.Drawing.Size(441, 42);
            _MeasureStatusStrip.SizingGrip = false;
            _MeasureStatusStrip.TabIndex = 18;
            // 
            // _TerminalsStatusLabel
            // 
            _TerminalsStatusLabel.Font = new System.Drawing.Font("Segoe UI", 20.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TerminalsStatusLabel.ForeColor = System.Drawing.SystemColors.Info;
            _TerminalsStatusLabel.Name = "_TerminalsStatusLabel";
            _TerminalsStatusLabel.Size = new System.Drawing.Size(30, 37);
            _TerminalsStatusLabel.Text = "F";
            _TerminalsStatusLabel.ToolTipText = "Terminals status";
            // 
            // _MeasureMetaStatusLabel
            // 
            _MeasureMetaStatusLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _MeasureMetaStatusLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MeasureMetaStatusLabel.ForeColor = System.Drawing.Color.LimeGreen;
            _MeasureMetaStatusLabel.Margin = new Padding(0);
            _MeasureMetaStatusLabel.Name = "_MeasureMetaStatusLabel";
            _MeasureMetaStatusLabel.Overflow = ToolStripItemOverflow.Never;
            _MeasureMetaStatusLabel.Size = new System.Drawing.Size(16, 42);
            _MeasureMetaStatusLabel.Text = "p";
            _MeasureMetaStatusLabel.ToolTipText = "Measure meta status";
            // 
            // _ReadingAmountLabel
            // 
            _ReadingAmountLabel.AutoSize = false;
            _ReadingAmountLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _ReadingAmountLabel.ForeColor = System.Drawing.Color.Aquamarine;
            _ReadingAmountLabel.Margin = new Padding(0);
            _ReadingAmountLabel.Name = "_ReadingAmountLabel";
            _ReadingAmountLabel.Overflow = ToolStripItemOverflow.Never;
            _ReadingAmountLabel.Size = new System.Drawing.Size(357, 42);
            _ReadingAmountLabel.Spring = true;
            _ReadingAmountLabel.Text = "-.------- mV";
            _ReadingAmountLabel.ToolTipText = "Reading value and unit";
            // 
            // _MeasureElapsedTimeLabel
            // 
            _MeasureElapsedTimeLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _MeasureElapsedTimeLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MeasureElapsedTimeLabel.ForeColor = System.Drawing.SystemColors.Info;
            _MeasureElapsedTimeLabel.Margin = new Padding(0);
            _MeasureElapsedTimeLabel.Name = "_MeasureElapsedTimeLabel";
            _MeasureElapsedTimeLabel.Size = new System.Drawing.Size(23, 42);
            _MeasureElapsedTimeLabel.Text = "ms";
            _MeasureElapsedTimeLabel.ToolTipText = "Measure last action time in ms";
            // 
            // _StandardRegisterLabel
            // 
            _StandardRegisterLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _StandardRegisterLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _StandardRegisterLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _StandardRegisterLabel.Margin = new Padding(0);
            _StandardRegisterLabel.Name = "_StandardRegisterLabel";
            _StandardRegisterLabel.Size = new System.Drawing.Size(19, 22);
            _StandardRegisterLabel.Text = "0x..";
            _StandardRegisterLabel.ToolTipText = "Standard Register Value";
            _StandardRegisterLabel.Visible = false;
            // 
            // _StatusRegisterLabel
            // 
            _StatusRegisterLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _StatusRegisterLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _StatusRegisterLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _StatusRegisterLabel.Margin = new Padding(0);
            _StatusRegisterLabel.Name = "_StatusRegisterLabel";
            _StatusRegisterLabel.Size = new System.Drawing.Size(19, 22);
            _StatusRegisterLabel.Text = "0x..";
            _StatusRegisterLabel.ToolTipText = "Status Register Value";
            // 
            // _Layout
            // 
            __Layout.AutoSize = true;
            __Layout.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            __Layout.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(38)), Conversions.ToInteger(Conversions.ToByte(44)));
            __Layout.ColumnCount = 1;
            __Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            __Layout.Controls.Add(_SessionReadingStatusStrip, 0, 2);
            __Layout.Controls.Add(_MeasureStatusStrip, 0, 1);
            __Layout.Controls.Add(_SubsystemStatusStrip, 0, 3);
            __Layout.Controls.Add(_TitleStatusStrip, 0, 0);
            __Layout.Controls.Add(_ErrorStatusStrip, 0, 4);
            __Layout.Controls.Add(_StatusStrip, 0, 5);
            __Layout.Dock = DockStyle.Top;
            __Layout.Location = new System.Drawing.Point(1, 1);
            __Layout.Margin = new Padding(0);
            __Layout.Name = "__Layout";
            __Layout.RowCount = 6;
            __Layout.RowStyles.Add(new RowStyle());
            __Layout.RowStyles.Add(new RowStyle());
            __Layout.RowStyles.Add(new RowStyle());
            __Layout.RowStyles.Add(new RowStyle());
            __Layout.RowStyles.Add(new RowStyle());
            __Layout.RowStyles.Add(new RowStyle());
            __Layout.Size = new System.Drawing.Size(441, 152);
            __Layout.TabIndex = 23;
            // 
            // _SessionReadingStatusStrip
            // 
            _SessionReadingStatusStrip.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(38)), Conversions.ToInteger(Conversions.ToByte(44)));
            _SessionReadingStatusStrip.GripMargin = new Padding(0);
            _SessionReadingStatusStrip.Items.AddRange(new ToolStripItem[] { _MeasurementEventStatusLabel, _LastReadingLabel, _SessionElapsedTimeLabel });
            _SessionReadingStatusStrip.Location = new System.Drawing.Point(0, 64);
            _SessionReadingStatusStrip.Name = "_SessionReadingStatusStrip";
            _SessionReadingStatusStrip.ShowItemToolTips = true;
            _SessionReadingStatusStrip.Size = new System.Drawing.Size(441, 22);
            _SessionReadingStatusStrip.SizingGrip = false;
            _SessionReadingStatusStrip.TabIndex = 0;
            _SessionReadingStatusStrip.Text = "Session Status Strip";
            // 
            // _LastReadingLabel
            // 
            _LastReadingLabel.ForeColor = System.Drawing.Color.Aquamarine;
            _LastReadingLabel.Margin = new Padding(0);
            _LastReadingLabel.Name = "_LastReadingLabel";
            _LastReadingLabel.Size = new System.Drawing.Size(343, 22);
            _LastReadingLabel.Spring = true;
            _LastReadingLabel.Text = "last reading";
            _LastReadingLabel.ToolTipText = "Last reading";
            // 
            // _SessionElapsedTimeLabel
            // 
            _SessionElapsedTimeLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SessionElapsedTimeLabel.ForeColor = System.Drawing.SystemColors.Info;
            _SessionElapsedTimeLabel.Name = "_SessionElapsedTimeLabel";
            _SessionElapsedTimeLabel.Size = new System.Drawing.Size(23, 17);
            _SessionElapsedTimeLabel.Text = "ms";
            _SessionElapsedTimeLabel.ToolTipText = "Session last action time in ms";
            // 
            // _SubsystemStatusStrip
            // 
            _SubsystemStatusStrip.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(38)), Conversions.ToInteger(Conversions.ToByte(44)));
            _SubsystemStatusStrip.GripMargin = new Padding(0);
            _SubsystemStatusStrip.Items.AddRange(new ToolStripItem[] { _SubsystemReadingLabel, _SubsystemElapsedTimeLabel });
            _SubsystemStatusStrip.Location = new System.Drawing.Point(0, 86);
            _SubsystemStatusStrip.Name = "_SubsystemStatusStrip";
            _SubsystemStatusStrip.ShowItemToolTips = true;
            _SubsystemStatusStrip.Size = new System.Drawing.Size(441, 22);
            _SubsystemStatusStrip.SizingGrip = false;
            _SubsystemStatusStrip.TabIndex = 19;
            _SubsystemStatusStrip.Text = "Subsystem Status Strip";
            // 
            // _SubsystemReadingLabel
            // 
            _SubsystemReadingLabel.BackColor = System.Drawing.Color.Transparent;
            _SubsystemReadingLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SubsystemReadingLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _SubsystemReadingLabel.Name = "_SubsystemReadingLabel";
            _SubsystemReadingLabel.Size = new System.Drawing.Size(403, 17);
            _SubsystemReadingLabel.Spring = true;
            _SubsystemReadingLabel.Text = "-.-------- mA";
            _SubsystemReadingLabel.ToolTipText = "Subsystem reading";
            // 
            // _SubsystemElapsedTimeLabel
            // 
            _SubsystemElapsedTimeLabel.ForeColor = System.Drawing.SystemColors.Info;
            _SubsystemElapsedTimeLabel.Name = "_SubsystemElapsedTimeLabel";
            _SubsystemElapsedTimeLabel.Size = new System.Drawing.Size(23, 17);
            _SubsystemElapsedTimeLabel.Text = "ms";
            _SubsystemElapsedTimeLabel.ToolTipText = "Subsystem last action time in ms";
            // 
            // _TitleStatusStrip
            // 
            _TitleStatusStrip.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(38)), Conversions.ToInteger(Conversions.ToByte(44)));
            _TitleStatusStrip.GripMargin = new Padding(0);
            _TitleStatusStrip.Items.AddRange(new ToolStripItem[] { _TitleLabel, _SessionOpenCloseStatusLabel });
            _TitleStatusStrip.Location = new System.Drawing.Point(0, 0);
            _TitleStatusStrip.Name = "_TitleStatusStrip";
            _TitleStatusStrip.ShowItemToolTips = true;
            _TitleStatusStrip.Size = new System.Drawing.Size(441, 22);
            _TitleStatusStrip.SizingGrip = false;
            _TitleStatusStrip.TabIndex = 20;
            _TitleStatusStrip.Text = "Title Status Strip";
            // 
            // _TitleLabel
            // 
            _TitleLabel.BackColor = System.Drawing.Color.Transparent;
            _TitleLabel.ForeColor = System.Drawing.SystemColors.Info;
            _TitleLabel.Name = "_TitleLabel";
            _TitleLabel.Size = new System.Drawing.Size(426, 17);
            _TitleLabel.Spring = true;
            _TitleLabel.Text = "Multimeter";
            _TitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            _TitleLabel.ToolTipText = "Device title";
            // 
            // _SessionOpenCloseStatusLabel
            // 
            _SessionOpenCloseStatusLabel.Name = "_SessionOpenCloseStatusLabel";
            _SessionOpenCloseStatusLabel.Size = new System.Drawing.Size(0, 17);
            _SessionOpenCloseStatusLabel.ToolTipText = "Session open status";
            // 
            // _ErrorStatusStrip
            // 
            _ErrorStatusStrip.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(38)), Conversions.ToInteger(Conversions.ToByte(44)));
            _ErrorStatusStrip.GripMargin = new Padding(0);
            _ErrorStatusStrip.Items.AddRange(new ToolStripItem[] { __ErrorLabel });
            _ErrorStatusStrip.Location = new System.Drawing.Point(0, 108);
            _ErrorStatusStrip.Name = "_ErrorStatusStrip";
            _ErrorStatusStrip.ShowItemToolTips = true;
            _ErrorStatusStrip.Size = new System.Drawing.Size(441, 22);
            _ErrorStatusStrip.SizingGrip = false;
            _ErrorStatusStrip.TabIndex = 21;
            _ErrorStatusStrip.Text = "Error Status Strip";
            // 
            // _ErrorLabel
            // 
            __ErrorLabel.BackColor = System.Drawing.Color.Transparent;
            __ErrorLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ErrorLabel.ForeColor = System.Drawing.Color.LimeGreen;
            __ErrorLabel.Name = "__ErrorLabel";
            __ErrorLabel.Size = new System.Drawing.Size(426, 17);
            __ErrorLabel.Spring = true;
            __ErrorLabel.Text = "000, No Errors";
            __ErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _StatusStrip
            // 
            _StatusStrip.BackColor = System.Drawing.Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(30)), Conversions.ToInteger(Conversions.ToByte(38)), Conversions.ToInteger(Conversions.ToByte(44)));
            _StatusStrip.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _StatusStrip.GripMargin = new Padding(0);
            _StatusStrip.Items.AddRange(new ToolStripItem[] { _ServiceRequestEnableBitmaskLabel, _StandardEventEnableBitmaskLabel, _ServiceRequestEnabledLabel, _StatusRegisterLabel, _OperationSummaryBitLabel, _ServiceRequestBitLabel, _EventSummaryBitLabel, _MessageAvailableBitLabel, _QuestionableSummaryBitLabel, _ErrorAvailableBitLabel, _SystemEventBitLabel, _MeasurementEventBitLabel, _StandardRegisterLabel, _PowerOnEventLabel, _UserRequestEventLabel, _CommandErrorEventLabel, _ExecutionErrorEventLabel, _DeviceDependentErrorEventToolLabel, _QueryErrorEventLabel, _OperationCompleteEventLabel, _ServiceRequestHandledLabel });
            _StatusStrip.Location = new System.Drawing.Point(0, 130);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.ShowItemToolTips = true;
            _StatusStrip.Size = new System.Drawing.Size(441, 22);
            _StatusStrip.SizingGrip = false;
            _StatusStrip.TabIndex = 22;
            _StatusStrip.Text = "Status Strip";
            // 
            // _ServiceRequestEnableBitmaskLabel
            // 
            _ServiceRequestEnableBitmaskLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _ServiceRequestEnableBitmaskLabel.Name = "_ServiceRequestEnableBitmaskLabel";
            _ServiceRequestEnableBitmaskLabel.Size = new System.Drawing.Size(19, 17);
            _ServiceRequestEnableBitmaskLabel.Text = "0x..";
            _ServiceRequestEnableBitmaskLabel.ToolTipText = "Service Request Enable Bitmask";
            // 
            // _StandardEventEnableBitmaskLabel
            // 
            _StandardEventEnableBitmaskLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _StandardEventEnableBitmaskLabel.Name = "_StandardEventEnableBitmaskLabel";
            _StandardEventEnableBitmaskLabel.Size = new System.Drawing.Size(19, 17);
            _StandardEventEnableBitmaskLabel.Text = "0x..";
            _StandardEventEnableBitmaskLabel.ToolTipText = "Standard Event Enable Bitmask";
            // 
            // _ServiceRequestEnabledLabel
            // 
            _ServiceRequestEnabledLabel.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _ServiceRequestEnabledLabel.Font = new System.Drawing.Font("Segoe UI", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ServiceRequestEnabledLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _ServiceRequestEnabledLabel.Image = My.Resources.Resources.lightning_16;
            _ServiceRequestEnabledLabel.ImageScaling = ToolStripItemImageScaling.None;
            _ServiceRequestEnabledLabel.Name = "_ServiceRequestEnabledLabel";
            _ServiceRequestEnabledLabel.Size = new System.Drawing.Size(16, 17);
            _ServiceRequestEnabledLabel.Text = "SRE";
            _ServiceRequestEnabledLabel.ToolTipText = "Service request is enabled";
            _ServiceRequestEnabledLabel.Visible = false;
            // 
            // _OperationSummaryBitLabel
            // 
            _OperationSummaryBitLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _OperationSummaryBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _OperationSummaryBitLabel.Margin = new Padding(0);
            _OperationSummaryBitLabel.Name = "_OperationSummaryBitLabel";
            _OperationSummaryBitLabel.Size = new System.Drawing.Size(21, 22);
            _OperationSummaryBitLabel.Text = "osb";
            _OperationSummaryBitLabel.ToolTipText = " Operation Summary Bit (OSB) is on";
            _OperationSummaryBitLabel.Visible = false;
            // 
            // _ServiceRequestBitLabel
            // 
            _ServiceRequestBitLabel.BackColor = System.Drawing.Color.Transparent;
            _ServiceRequestBitLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ServiceRequestBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _ServiceRequestBitLabel.Margin = new Padding(0);
            _ServiceRequestBitLabel.Name = "_ServiceRequestBitLabel";
            _ServiceRequestBitLabel.Size = new System.Drawing.Size(19, 22);
            _ServiceRequestBitLabel.Text = "rqs";
            _ServiceRequestBitLabel.ToolTipText = "Request for Service (RQS) bit or the Master Summary Status Bit (MSB) bit is on";
            _ServiceRequestBitLabel.Visible = false;
            // 
            // _EventSummaryBitLabel
            // 
            _EventSummaryBitLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _EventSummaryBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _EventSummaryBitLabel.Margin = new Padding(0);
            _EventSummaryBitLabel.Name = "_EventSummaryBitLabel";
            _EventSummaryBitLabel.Size = new System.Drawing.Size(20, 22);
            _EventSummaryBitLabel.Text = "esb";
            _EventSummaryBitLabel.ToolTipText = "Event Summary Bit (ESB) is on";
            _EventSummaryBitLabel.Visible = false;
            // 
            // _MessageAvailableBitLabel
            // 
            _MessageAvailableBitLabel.BackColor = System.Drawing.Color.Transparent;
            _MessageAvailableBitLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MessageAvailableBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _MessageAvailableBitLabel.Margin = new Padding(0);
            _MessageAvailableBitLabel.Name = "_MessageAvailableBitLabel";
            _MessageAvailableBitLabel.Size = new System.Drawing.Size(23, 22);
            _MessageAvailableBitLabel.Text = "mav";
            _MessageAvailableBitLabel.ToolTipText = "Message Available Bit (MAV) is on";
            _MessageAvailableBitLabel.Visible = false;
            // 
            // _QuestionableSummaryBitLabel
            // 
            _QuestionableSummaryBitLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _QuestionableSummaryBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _QuestionableSummaryBitLabel.Margin = new Padding(0);
            _QuestionableSummaryBitLabel.Name = "_QuestionableSummaryBitLabel";
            _QuestionableSummaryBitLabel.Size = new System.Drawing.Size(21, 22);
            _QuestionableSummaryBitLabel.Text = "qsb";
            _QuestionableSummaryBitLabel.ToolTipText = "Questionable Summary Bit (QSB) is on";
            _QuestionableSummaryBitLabel.Visible = false;
            // 
            // _ErrorAvailableBitLabel
            // 
            _ErrorAvailableBitLabel.BackColor = System.Drawing.Color.Transparent;
            _ErrorAvailableBitLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ErrorAvailableBitLabel.ForeColor = System.Drawing.Color.Red;
            _ErrorAvailableBitLabel.Margin = new Padding(0);
            _ErrorAvailableBitLabel.Name = "_ErrorAvailableBitLabel";
            _ErrorAvailableBitLabel.Size = new System.Drawing.Size(20, 22);
            _ErrorAvailableBitLabel.Text = "Eav";
            _ErrorAvailableBitLabel.ToolTipText = "Error Available Bit (EAV) is on";
            _ErrorAvailableBitLabel.Visible = false;
            // 
            // _SystemEventBitLabel
            // 
            _SystemEventBitLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SystemEventBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _SystemEventBitLabel.Margin = new Padding(0);
            _SystemEventBitLabel.Name = "_SystemEventBitLabel";
            _SystemEventBitLabel.Size = new System.Drawing.Size(19, 22);
            _SystemEventBitLabel.Text = "ssb";
            _SystemEventBitLabel.ToolTipText = "System Event Bit (SSB) is on";
            _SystemEventBitLabel.Visible = false;
            // 
            // _MeasurementEventBitLabel
            // 
            _MeasurementEventBitLabel.Font = new System.Drawing.Font("Segoe UI", 6.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MeasurementEventBitLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _MeasurementEventBitLabel.Margin = new Padding(0);
            _MeasurementEventBitLabel.Name = "_MeasurementEventBitLabel";
            _MeasurementEventBitLabel.Size = new System.Drawing.Size(23, 22);
            _MeasurementEventBitLabel.Text = "msb";
            _MeasurementEventBitLabel.ToolTipText = "Measurement Event Bit (MEB) is on";
            _MeasurementEventBitLabel.Visible = false;
            // 
            // _PowerOnEventLabel
            // 
            _PowerOnEventLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _PowerOnEventLabel.Name = "_PowerOnEventLabel";
            _PowerOnEventLabel.Size = new System.Drawing.Size(22, 17);
            _PowerOnEventLabel.Text = "pon";
            _PowerOnEventLabel.ToolTipText = "Power on";
            _PowerOnEventLabel.Visible = false;
            // 
            // _UserRequestEventLabel
            // 
            _UserRequestEventLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _UserRequestEventLabel.Name = "_UserRequestEventLabel";
            _UserRequestEventLabel.Size = new System.Drawing.Size(20, 17);
            _UserRequestEventLabel.Text = "urq";
            _UserRequestEventLabel.ToolTipText = "User request ";
            _UserRequestEventLabel.Visible = false;
            // 
            // _CommandErrorEventLabel
            // 
            _CommandErrorEventLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _CommandErrorEventLabel.Name = "_CommandErrorEventLabel";
            _CommandErrorEventLabel.Size = new System.Drawing.Size(22, 17);
            _CommandErrorEventLabel.Text = "cme";
            _CommandErrorEventLabel.ToolTipText = "Command error ";
            _CommandErrorEventLabel.Visible = false;
            // 
            // _ExecutionErrorEventLabel
            // 
            _ExecutionErrorEventLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _ExecutionErrorEventLabel.Name = "_ExecutionErrorEventLabel";
            _ExecutionErrorEventLabel.Size = new System.Drawing.Size(20, 17);
            _ExecutionErrorEventLabel.Text = "exe";
            _ExecutionErrorEventLabel.ToolTipText = "Execution error";
            _ExecutionErrorEventLabel.Visible = false;
            // 
            // _DeviceDependentErrorEventToolLabel
            // 
            _DeviceDependentErrorEventToolLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _DeviceDependentErrorEventToolLabel.Name = "_DeviceDependentErrorEventToolLabel";
            _DeviceDependentErrorEventToolLabel.Size = new System.Drawing.Size(22, 17);
            _DeviceDependentErrorEventToolLabel.Text = "dde";
            _DeviceDependentErrorEventToolLabel.ToolTipText = "Device dependent error";
            _DeviceDependentErrorEventToolLabel.Visible = false;
            // 
            // _QueryErrorEventLabel
            // 
            _QueryErrorEventLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _QueryErrorEventLabel.Name = "_QueryErrorEventLabel";
            _QueryErrorEventLabel.Size = new System.Drawing.Size(21, 17);
            _QueryErrorEventLabel.Text = "qye";
            _QueryErrorEventLabel.ToolTipText = "Query error";
            _QueryErrorEventLabel.Visible = false;
            // 
            // _OperationCompleteEventLabel
            // 
            _OperationCompleteEventLabel.ForeColor = System.Drawing.Color.DarkOrange;
            _OperationCompleteEventLabel.Name = "_OperationCompleteEventLabel";
            _OperationCompleteEventLabel.Size = new System.Drawing.Size(21, 17);
            _OperationCompleteEventLabel.Text = "opc";
            _OperationCompleteEventLabel.ToolTipText = "Operation complete";
            _OperationCompleteEventLabel.Visible = false;
            // 
            // _ServiceRequestHandledLabel
            // 
            _ServiceRequestHandledLabel.Image = My.Resources.Resources.EventHandleGreen16;
            _ServiceRequestHandledLabel.Name = "_ServiceRequestHandledLabel";
            _ServiceRequestHandledLabel.Size = new System.Drawing.Size(16, 17);
            _ServiceRequestHandledLabel.ToolTipText = "Service request handler assigned";
            _ServiceRequestHandledLabel.Visible = false;
            // 
            // _MeasurementEventStatusLabel
            // 
            _MeasurementEventStatusLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            _MeasurementEventStatusLabel.Name = "_MeasurementEventStatusLabel";
            _MeasurementEventStatusLabel.Size = new System.Drawing.Size(29, 17);
            _MeasurementEventStatusLabel.Text = "mes";
            _MeasurementEventStatusLabel.ToolTipText = "Measurement Event Status";
            // 
            // DisplayView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(__Layout);
            Name = "DisplayView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(443, 148);
            _MeasureStatusStrip.ResumeLayout(false);
            _MeasureStatusStrip.PerformLayout();
            __Layout.ResumeLayout(false);
            __Layout.PerformLayout();
            _SessionReadingStatusStrip.ResumeLayout(false);
            _SessionReadingStatusStrip.PerformLayout();
            _SubsystemStatusStrip.ResumeLayout(false);
            _SubsystemStatusStrip.PerformLayout();
            _TitleStatusStrip.ResumeLayout(false);
            _TitleStatusStrip.PerformLayout();
            _ErrorStatusStrip.ResumeLayout(false);
            _ErrorStatusStrip.PerformLayout();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private StatusStrip _MeasureStatusStrip;
        private Core.Controls.ToolStripStatusLabel _MeasureMetaStatusLabel;
        private Core.Controls.ToolStripStatusLabel _ReadingAmountLabel;
        private Core.Controls.ToolStripStatusLabel _StatusRegisterLabel;
        private Core.Controls.ToolStripStatusLabel _StandardRegisterLabel;
        private Core.Controls.ToolStripStatusLabel _MeasureElapsedTimeLabel;
        private StatusStrip _SessionReadingStatusStrip;
        private Core.Controls.ToolStripStatusLabel _LastReadingLabel;
        private StatusStrip _SubsystemStatusStrip;
        private Core.Controls.ToolStripStatusLabel _SubsystemReadingLabel;
        private StatusStrip _TitleStatusStrip;
        private Core.Controls.ToolStripStatusLabel _TitleLabel;
        private StatusStrip _ErrorStatusStrip;
        private Core.Controls.ToolStripStatusLabel __ErrorLabel;

        private Core.Controls.ToolStripStatusLabel _ErrorLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ErrorLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ErrorLabel != null)
                {
                    __ErrorLabel.TextChanged -= ErrorLabel_TextChanges;
                }

                __ErrorLabel = value;
                if (__ErrorLabel != null)
                {
                    __ErrorLabel.TextChanged += ErrorLabel_TextChanges;
                }
            }
        }

        private TableLayoutPanel __Layout;

        private TableLayoutPanel _Layout
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __Layout;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__Layout != null)
                {
                    __Layout.Resize -= Layout_Resize;
                }

                __Layout = value;
                if (__Layout != null)
                {
                    __Layout.Resize += Layout_Resize;
                }
            }
        }

        private Core.Controls.ToolStripStatusLabel _SessionElapsedTimeLabel;
        private Core.Controls.ToolStripStatusLabel _ServiceRequestBitLabel;
        private Core.Controls.ToolStripStatusLabel _MessageAvailableBitLabel;
        private StatusStrip _StatusStrip;
        private Core.Controls.ToolStripStatusLabel _ServiceRequestEnabledLabel;
        private Core.Controls.ToolStripStatusLabel _ErrorAvailableBitLabel;
        private Core.Controls.ToolStripStatusLabel _SubsystemElapsedTimeLabel;
        private Core.Controls.ToolStripStatusLabel _TerminalsStatusLabel;
        private Core.Controls.ToolStripStatusLabel _SessionOpenCloseStatusLabel;
        private Core.Controls.ToolStripStatusLabel _OperationSummaryBitLabel;
        private Core.Controls.ToolStripStatusLabel _EventSummaryBitLabel;
        private Core.Controls.ToolStripStatusLabel _QuestionableSummaryBitLabel;
        private Core.Controls.ToolStripStatusLabel _SystemEventBitLabel;
        private Core.Controls.ToolStripStatusLabel _MeasurementEventBitLabel;
        private Core.Controls.ToolStripStatusLabel _PowerOnEventLabel;
        private Core.Controls.ToolStripStatusLabel _UserRequestEventLabel;
        private Core.Controls.ToolStripStatusLabel _CommandErrorEventLabel;
        private Core.Controls.ToolStripStatusLabel _ExecutionErrorEventLabel;
        private Core.Controls.ToolStripStatusLabel _DeviceDependentErrorEventToolLabel;
        private Core.Controls.ToolStripStatusLabel _QueryErrorEventLabel;
        private Core.Controls.ToolStripStatusLabel _OperationCompleteEventLabel;
        private Core.Controls.ToolStripStatusLabel _ServiceRequestEnableBitmaskLabel;
        private Core.Controls.ToolStripStatusLabel _StandardEventEnableBitmaskLabel;
        private Core.Controls.ToolStripStatusLabel _ServiceRequestHandledLabel;
        private Core.Controls.ToolStripStatusLabel _MeasurementEventStatusLabel;
    }
}