using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class ResourceNameInfoEditor
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _AddResourceButton = new Button();
            _AddResourceButton.Click += new EventHandler(AddResourceButton_Click);
            _ResourceNamesListBox = new ListBox();
            _ResourceNamesListBox.SelectedValueChanged += new EventHandler(ResourceNamesListBox_SelectedValueChanged);
            ResourceNameTextBox = new TextBox();
            ResourceNameTextBoxLabel = new Label();
            _TestResourceButton = new Button();
            _TestResourceButton.Click += new EventHandler(TestResourceButton_Click);
            MessagesTextBox = new TextBox();
            LayoutPanel = new TableLayoutPanel();
            ResourcePanel = new Panel();
            _BackupButton = new Button();
            _BackupButton.Click += new EventHandler(BackupButton_Click);
            _RestoreButton = new Button();
            _RestoreButton.Click += new EventHandler(RestoreButton_Click);
            _RemoveButton = new Button();
            _RemoveButton.Click += new EventHandler(RemoveButton_Click);
            ResourceFolderLabel = new Label();
            LayoutPanel.SuspendLayout();
            ResourcePanel.SuspendLayout();
            SuspendLayout();
            // 
            // AddResourceButton
            // 
            _AddResourceButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _AddResourceButton.Location = new System.Drawing.Point(242, 3);
            _AddResourceButton.Name = "_AddResourceButton";
            _AddResourceButton.Size = new System.Drawing.Size(64, 25);
            _AddResourceButton.TabIndex = 4;
            _AddResourceButton.Text = "&Add";
            _AddResourceButton.UseVisualStyleBackColor = true;
            // 
            // ResourceNamesListBox
            // 
            _ResourceNamesListBox.Dock = DockStyle.Fill;
            _ResourceNamesListBox.FormattingEnabled = true;
            _ResourceNamesListBox.ItemHeight = 17;
            _ResourceNamesListBox.Location = new System.Drawing.Point(6, 67);
            _ResourceNamesListBox.Name = "_ResourceNamesListBox";
            _ResourceNamesListBox.Size = new System.Drawing.Size(440, 236);
            _ResourceNamesListBox.TabIndex = 4;
            // 
            // ResourceNameTextBox
            // 
            ResourceNameTextBox.Dock = DockStyle.Bottom;
            ResourceNameTextBox.Location = new System.Drawing.Point(0, 30);
            ResourceNameTextBox.Name = "ResourceNameTextBox";
            ResourceNameTextBox.Size = new System.Drawing.Size(440, 25);
            ResourceNameTextBox.TabIndex = 1;
            // 
            // ResourceNameTextBoxLabel
            // 
            ResourceNameTextBoxLabel.AutoSize = true;
            ResourceNameTextBoxLabel.Location = new System.Drawing.Point(3, 11);
            ResourceNameTextBoxLabel.Name = "ResourceNameTextBoxLabel";
            ResourceNameTextBoxLabel.Size = new System.Drawing.Size(65, 17);
            ResourceNameTextBoxLabel.TabIndex = 0;
            ResourceNameTextBoxLabel.Text = "Resource:";
            // 
            // TestResourceButton
            // 
            _TestResourceButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _TestResourceButton.Location = new System.Drawing.Point(110, 3);
            _TestResourceButton.Name = "_TestResourceButton";
            _TestResourceButton.Size = new System.Drawing.Size(64, 25);
            _TestResourceButton.TabIndex = 2;
            _TestResourceButton.Text = "&Test";
            _TestResourceButton.UseVisualStyleBackColor = true;
            // 
            // MessagesTextBox
            // 
            MessagesTextBox.Dock = DockStyle.Top;
            MessagesTextBox.Location = new System.Drawing.Point(6, 309);
            MessagesTextBox.Multiline = true;
            MessagesTextBox.Name = "MessagesTextBox";
            MessagesTextBox.ReadOnly = true;
            MessagesTextBox.ScrollBars = ScrollBars.Both;
            MessagesTextBox.Size = new System.Drawing.Size(440, 123);
            MessagesTextBox.TabIndex = 5;
            MessagesTextBox.Text = "<messages>";
            // 
            // LayoutPanel
            // 
            LayoutPanel.ColumnCount = 3;
            LayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            LayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            LayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            LayoutPanel.Controls.Add(ResourcePanel, 1, 1);
            LayoutPanel.Controls.Add(MessagesTextBox, 1, 3);
            LayoutPanel.Controls.Add(_ResourceNamesListBox, 1, 2);
            LayoutPanel.Controls.Add(ResourceFolderLabel, 1, 4);
            LayoutPanel.Dock = DockStyle.Fill;
            LayoutPanel.Location = new System.Drawing.Point(0, 0);
            LayoutPanel.Name = "LayoutPanel";
            LayoutPanel.RowCount = 6;
            LayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            LayoutPanel.RowStyles.Add(new RowStyle());
            LayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            LayoutPanel.RowStyles.Add(new RowStyle());
            LayoutPanel.RowStyles.Add(new RowStyle());
            LayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            LayoutPanel.Size = new System.Drawing.Size(452, 455);
            LayoutPanel.TabIndex = 6;
            // 
            // ResourcePanel
            // 
            ResourcePanel.Controls.Add(_BackupButton);
            ResourcePanel.Controls.Add(_RestoreButton);
            ResourcePanel.Controls.Add(_RemoveButton);
            ResourcePanel.Controls.Add(_AddResourceButton);
            ResourcePanel.Controls.Add(_TestResourceButton);
            ResourcePanel.Controls.Add(ResourceNameTextBoxLabel);
            ResourcePanel.Controls.Add(ResourceNameTextBox);
            ResourcePanel.Dock = DockStyle.Top;
            ResourcePanel.Location = new System.Drawing.Point(6, 6);
            ResourcePanel.Name = "ResourcePanel";
            ResourcePanel.Size = new System.Drawing.Size(440, 55);
            ResourcePanel.TabIndex = 7;
            // 
            // BackupButton
            // 
            _BackupButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _BackupButton.Location = new System.Drawing.Point(308, 3);
            _BackupButton.Name = "_BackupButton";
            _BackupButton.Size = new System.Drawing.Size(64, 25);
            _BackupButton.TabIndex = 6;
            _BackupButton.Text = "Backup";
            _BackupButton.UseVisualStyleBackColor = true;
            // 
            // RestoreButton
            // 
            _RestoreButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _RestoreButton.Location = new System.Drawing.Point(374, 3);
            _RestoreButton.Name = "_RestoreButton";
            _RestoreButton.Size = new System.Drawing.Size(64, 25);
            _RestoreButton.TabIndex = 5;
            _RestoreButton.Text = "Restore";
            _RestoreButton.UseVisualStyleBackColor = true;
            // 
            // RemoveButton
            // 
            _RemoveButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _RemoveButton.Location = new System.Drawing.Point(176, 3);
            _RemoveButton.Name = "_RemoveButton";
            _RemoveButton.Size = new System.Drawing.Size(64, 25);
            _RemoveButton.TabIndex = 3;
            _RemoveButton.Text = "&Remove";
            _RemoveButton.UseVisualStyleBackColor = true;
            // 
            // ResourceFolderLabel
            // 
            ResourceFolderLabel.AutoSize = true;
            ResourceFolderLabel.Location = new System.Drawing.Point(6, 435);
            ResourceFolderLabel.Name = "ResourceFolderLabel";
            ResourceFolderLabel.Size = new System.Drawing.Size(146, 17);
            ResourceFolderLabel.TabIndex = 8;
            ResourceFolderLabel.Text = "location of resource file";
            // 
            // ResourceNameInfoEditor
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(LayoutPanel);
            Name = "ResourceNameInfoEditor";
            Size = new System.Drawing.Size(452, 455);
            LayoutPanel.ResumeLayout(false);
            LayoutPanel.PerformLayout();
            ResourcePanel.ResumeLayout(false);
            ResourcePanel.PerformLayout();
            ResumeLayout(false);
        }

        private Button _AddResourceButton;

        private Button AddResourceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _AddResourceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_AddResourceButton != null)
                {
                    _AddResourceButton.Click -= AddResourceButton_Click;
                }

                _AddResourceButton = value;
                if (_AddResourceButton != null)
                {
                    _AddResourceButton.Click += AddResourceButton_Click;
                }
            }
        }

        private TextBox ResourceNameTextBox;
        private Label ResourceNameTextBoxLabel;
        private Button _TestResourceButton;

        private Button TestResourceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _TestResourceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_TestResourceButton != null)
                {
                    _TestResourceButton.Click -= TestResourceButton_Click;
                }

                _TestResourceButton = value;
                if (_TestResourceButton != null)
                {
                    _TestResourceButton.Click += TestResourceButton_Click;
                }
            }
        }

        private ListBox _ResourceNamesListBox;

        private ListBox ResourceNamesListBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ResourceNamesListBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ResourceNamesListBox != null)
                {
                    _ResourceNamesListBox.SelectedValueChanged -= ResourceNamesListBox_SelectedValueChanged;
                }

                _ResourceNamesListBox = value;
                if (_ResourceNamesListBox != null)
                {
                    _ResourceNamesListBox.SelectedValueChanged += ResourceNamesListBox_SelectedValueChanged;
                }
            }
        }

        private TextBox MessagesTextBox;
        private Panel ResourcePanel;
        private Button _RemoveButton;

        private Button RemoveButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _RemoveButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_RemoveButton != null)
                {
                    _RemoveButton.Click -= RemoveButton_Click;
                }

                _RemoveButton = value;
                if (_RemoveButton != null)
                {
                    _RemoveButton.Click += RemoveButton_Click;
                }
            }
        }

        private TableLayoutPanel LayoutPanel;
        private Button _RestoreButton;

        private Button RestoreButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _RestoreButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_RestoreButton != null)
                {
                    _RestoreButton.Click -= RestoreButton_Click;
                }

                _RestoreButton = value;
                if (_RestoreButton != null)
                {
                    _RestoreButton.Click += RestoreButton_Click;
                }
            }
        }

        private Button _BackupButton;

        private Button BackupButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _BackupButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_BackupButton != null)
                {
                    _BackupButton.Click -= BackupButton_Click;
                }

                _BackupButton = value;
                if (_BackupButton != null)
                {
                    _BackupButton.Click += BackupButton_Click;
                }
            }
        }

        private Label ResourceFolderLabel;
    }
}
