using System;
using System.Windows.Forms;

namespace isr.VI.Facade
{

    /// <summary> Form for viewing the visa view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-01-03 </para>
    /// </remarks>
    public class VisaViewForm : Core.Forma.ConsoleForm
    {

        #region " CONSTRUCTION and CLEAN UP "

        /// <summary> Default constructor. </summary>
        public VisaViewForm() : base()
        {
            this.Name = "Visa.View.Form";
        }

        /// <summary> Creates a new VisaViewForm. </summary>
        /// <returns> A VisaViewForm. </returns>
        public static VisaViewForm Create()
        {
            VisaView visaView = null;
            try
            {
                visaView = new VisaView();
                return Create( visaView );
            }
            catch
            {
                if ( visaView is object )
                {
                    visaView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Creates a new VisaViewForm. </summary>
        /// <param name="visaView"> The visa view. </param>
        /// <returns> A VisaViewForm. </returns>
        public static VisaViewForm Create( VisaView visaView )
        {
            VisaViewForm result = null;
            try
            {
                result = new VisaViewForm() { VisaView = visaView };
            }
            catch
            {
                if ( result is object )
                {
                    result.Dispose();
                }

                throw;
            }

            return result;
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    if ( this.VisaViewDisposeEnabled && this.VisaView is object )
                        this.VisaView.Dispose();
                    if ( this.VisaSessionBase is object )
                    {
                        this.VisaSessionBase.Dispose();
                        this.VisaSessionBase = null;
                    }

                    this.VisaView = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding Visa View";
                // the talker control publishes the device messages which thus get published to the form message box.
                this.AddTalkerControl( this.VisaView.VisaSessionBase.CandidateResourceTitle, this.VisaView, false, false );
                // any form messages will be logged.
                activity = $"{this.Name}; adding log listener";
                this.AddListener( My.MyLibrary.Logger );
                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.CandidateResourceName ) )
                {
                    activity = $"{this.Name}; starting {this.VisaView.VisaSessionBase.CandidateResourceName} selection task";
                    _ = this.VisaView.VisaSessionBase.AsyncValidateResourceName( this.VisaView.VisaSessionBase.CandidateResourceName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary> Gets or sets the await the resource name validation task enabled. </summary>
        /// <value> The await the resource name validation task enabled. </value>
        protected bool AwaitResourceNameValidationTaskEnabled { get; set; }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.VisaView.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
                if ( this.VisaView.VisaSessionBase.IsValidatingResourceName() )
                {
                    if ( this.AwaitResourceNameValidationTaskEnabled )
                    {
                        activity = $"{this.Name}; awaiting {this.VisaView.VisaSessionBase.CandidateResourceName} validation";
                        this.VisaView.VisaSessionBase.AwaitResourceNameValidation( My.Settings.Default.ResourceNameSelectionTimeout );
                    }
                    else
                    {
                        activity = $"{this.Name}; validating {this.VisaView.VisaSessionBase.CandidateResourceName}";
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if ( this.VisaView is object )
                    this.VisaView.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " VISA VIEW "

        /// <summary> Gets or sets the visa view. </summary>
        /// <value> The visa view. </value>
        public VisaView VisaView { get; set; }

        /// <summary> Gets or sets the visa view dispose enabled. </summary>
        /// <value> The visa view dispose enabled. </value>
        public bool VisaViewDisposeEnabled { get; set; }

        /// <summary> Gets or sets the locally assigned visa session if any. </summary>
        /// <value> The visa session. </value>
        protected VisaSessionBase VisaSessionBase { get; set; }

        /// <summary> Creates view. </summary>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        private void CreateViewView( string candidateResourceName, string candidateResourceTitle )
        {
            this.VisaSessionBase = new VisaSession();
            this.VisaView = new VisaView( this.VisaSessionBase );
            this.VisaView.VisaSessionBase.CandidateResourceTitle = candidateResourceTitle;
            this.VisaView.VisaSessionBase.CandidateResourceName = candidateResourceName;
        }

        #endregion

        #region " SHOW DIALOG "

        /// <summary> Shows the form dialog. </summary>
        /// <param name="owner">                  The owner. </param>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public DialogResult ShowDialog( IWin32Window owner, string candidateResourceName, string candidateResourceTitle )
        {
            VisaView visaView = null;
            try
            {
                this.CreateViewView( candidateResourceName, candidateResourceTitle );
                return this.ShowDialog( owner, visaView );
            }
            catch
            {
                if ( visaView is object )
                {
                    visaView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Shows the form dialog. </summary>
        /// <param name="owner">    The owner. </param>
        /// <param name="visaView"> The visa view. </param>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public DialogResult ShowDialog( IWin32Window owner, VisaView visaView )
        {
            this.VisaView = visaView;
            return this.ShowDialog( owner );
        }

        /// <summary> Shows the form. </summary>
        /// <param name="owner">                  The owner. </param>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        public void Show( IWin32Window owner, string candidateResourceName, string candidateResourceTitle )
        {
            VisaView visaView = null;
            try
            {
                this.CreateViewView( candidateResourceName, candidateResourceTitle );
                this.Show( owner, visaView );
            }
            catch
            {
                if ( visaView is object )
                {
                    visaView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Shows the form. </summary>
        /// <param name="owner">    The owner. </param>
        /// <param name="visaView"> The visa view. </param>
        public void Show( IWin32Window owner, VisaView visaView )
        {
            this.VisaView = visaView;
            this.Show( owner );
        }

        #endregion

    }
}
