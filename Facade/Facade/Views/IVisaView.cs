using System;
using System.Windows.Forms;

namespace isr.VI.Facade
{

    /// <summary> Interface for visa view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IVisaView : IDisposable, Core.ITalker
    {

        /// <summary> Gets or sets the visa session base. </summary>
        /// <value> The visa session base. </value>
        VisaSessionBase VisaSessionBase { get; }

        /// <summary> Gets or sets the display view. </summary>
        /// <value> The display view. </value>
        DisplayView DisplayView { get; }

        /// <summary> Gets or sets the status view. </summary>
        /// <value> The status view. </value>
        StatusView StatusView { get; }

        /// <summary> Gets or sets the number of views. </summary>
        /// <value> The number of views. </value>
        int ViewsCount { get; }

        /// <summary> Adds (inserts) a View. </summary>
        /// <param name="view"> The control. </param>
        void AddView( VisaViewControl view );

        /// <summary> Adds (inserts) a View. </summary>
        /// <param name="view">        The View control. </param>
        /// <param name="viewIndex">   Zero-based index of the view. </param>
        /// <param name="viewName">    Name of the view. </param>
        /// <param name="viewCaption"> The view caption. </param>
        void AddView( Control view, int viewIndex, string viewName, string viewCaption );

        /// <summary> Adds (inserts) a View. </summary>
        /// <param name="view">        The control. </param>
        /// <param name="viewIndex">   Zero-based index of the view. </param>
        /// <param name="viewName">    The name of the view. </param>
        /// <param name="viewCaption"> The caption of the view. </param>
        void AddView( Core.Forma.ModelViewTalkerBase view, int viewIndex, string viewName, string viewCaption );

        /// <summary> Gets or sets the number of internal resource names. </summary>
        /// <value> The number of internal resource names. </value>
        int InternalResourceNamesCount { get; }

        /// <summary> Gets or sets the name of the internal selected resource. </summary>
        /// <value> The name of the internal selected resource. </value>
        string InternalSelectedResourceName { get; }
    }

    /// <summary> A visa view control. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>" )]
    public struct VisaViewControl
    {

        /// <summary> Constructor. </summary>
        /// <param name="control"> The control. </param>
        /// <param name="index">   Zero-based index of the control. </param>
        /// <param name="name">    Name of the control. </param>
        /// <param name="caption"> The control caption. </param>
        public VisaViewControl( Control control, int index, string name, string caption )
        {
            this.Control = control;
            this.Index = index;
            this.Name = name;
            this.Caption = caption;
        }

        /// <summary> Gets or sets the control. </summary>
        /// <value> The control. </value>
        public Control Control { get; private set; }

        /// <summary> Gets or sets the zero-based index of this object. </summary>
        /// <value> The index. </value>
        public int Index { get; private set; }

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; private set; }

        /// <summary> Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        public string Caption { get; private set; }
    }
}
