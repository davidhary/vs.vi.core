using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class StatusView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(StatusView));
            __ToolStrip = new ToolStrip();
            __ToolStrip.DoubleClick += new EventHandler(ToolStrip_DoubleClick);
            _ApplicationDropDownButton = new ToolStripDropDownButton();
            EditSettingsToolStripMenuItem = new ToolStripMenuItem();
            __SessionSettingsMenuItem = new ToolStripMenuItem();
            __SessionSettingsMenuItem.Click += new EventHandler(SessionSettingsMenuItem_Click);
            __DeviceSettingsMenuItem = new ToolStripMenuItem();
            __DeviceSettingsMenuItem.Click += new EventHandler(DeviceSettingsMenuItem_Click);
            __UserInterfaceSettingsMenuItem = new ToolStripMenuItem();
            __UserInterfaceSettingsMenuItem.Click += new EventHandler(UserInterfaceToolStripMenuItem_Click);
            __UIBaseSettingsMenuItem = new ToolStripMenuItem();
            __UIBaseSettingsMenuItem.Click += new EventHandler(UIBaseSettingsMenuItem_Click);
            _TraceMenuItem = new ToolStripMenuItem();
            _TraceLogLevelMenuItem = new ToolStripMenuItem();
            _TraceLogLevelComboBox = new Core.Controls.ToolStripComboBox();
            _TraceShowLevelMenuItem = new ToolStripMenuItem();
            _TraceShowLevelComboBox = new Core.Controls.ToolStripComboBox();
            _SessionNotificationLevelMenuItem = new ToolStripMenuItem();
            _SessionNotificationLevelComboBox = new Core.Controls.ToolStripComboBox();
            __UsingStatusSubsystemMenuItem = new Core.Controls.ToolStripMenuItem();
            __UsingStatusSubsystemMenuItem.CheckStateChanged += new EventHandler(OpenStatusSubsystemMenuItem_CheckStateChanged);
            _DeviceDropDownButton = new Core.Controls.ToolStripDropDownButton();
            _ResetMenuItem = new ToolStripMenuItem();
            __ClearInterfaceMenuItem = new Core.Controls.ToolStripMenuItem();
            __ClearInterfaceMenuItem.Click += new EventHandler(ClearInterfaceMenuItem_Click);
            __ClearDeviceMenuItem = new ToolStripMenuItem();
            __ClearDeviceMenuItem.Click += new EventHandler(ClearDeviceMenuItem_Click);
            __ResetKnownStateMenuItem = new ToolStripMenuItem();
            __ResetKnownStateMenuItem.Click += new EventHandler(ResetKnownStateMenuItem_Click);
            __ClearExecutionStateMenuItem = new ToolStripMenuItem();
            __ClearExecutionStateMenuItem.Click += new EventHandler(ClearExecutionStateMenuItem_Click);
            __InitKnownStateMenuItem = new ToolStripMenuItem();
            __InitKnownStateMenuItem.Click += new EventHandler(InitKnownStateMenuItem_Click);
            __ClearErrorReportMenuItem = new ToolStripMenuItem();
            __ClearErrorReportMenuItem.Click += new EventHandler(ClearErrorReportMenuItem_Click);
            __ReadDeviceErrorsMenuItem = new ToolStripMenuItem();
            __ReadDeviceErrorsMenuItem.Click += new EventHandler(ReadDeviceErrorsMenuItem_Click);
            __ReadTerminalsStateMenuItem = new ToolStripMenuItem();
            __ReadTerminalsStateMenuItem.Click += new EventHandler(ReadTerminalsStateMenuItem_Click);
            _SessionDownDownButton = new Core.Controls.ToolStripDropDownButton();
            _ReadTerminationMenuItem = new ToolStripMenuItem();
            _ReadTerminationTextBox = new Core.Controls.ToolStripTextBox();
            _ReadTerminationEnabledMenuItem = new Core.Controls.ToolStripMenuItem();
            _WriteTerminationMenuItem = new ToolStripMenuItem();
            _WriteTerminationTextBox = new Core.Controls.ToolStripTextBox();
            _SessionTimeoutMenuItem = new ToolStripMenuItem();
            _SessionTimeoutTextBox = new Core.Controls.ToolStripTextBox();
            __StoreTimeoutMenuItem = new ToolStripMenuItem();
            __StoreTimeoutMenuItem.Click += new EventHandler(StoreTimeoutMenuItem_Click);
            __RestoreTimeoutMenuItem = new ToolStripMenuItem();
            __RestoreTimeoutMenuItem.Click += new EventHandler(RestoreTimeoutMenuItem_Click);
            __SessionReadStatusByteMenuItem = new ToolStripMenuItem();
            __SessionReadStatusByteMenuItem.Click += new EventHandler(SessionReadStatusByteMenuItem_Click);
            __SessionReadStandardEventRegisterMenuItem = new ToolStripMenuItem();
            __SessionReadStandardEventRegisterMenuItem.Click += new EventHandler(SessionReadStandardEventRegisterMenuItem_Click);
            __SendBusTriggerMenuItem = new ToolStripMenuItem();
            __SendBusTriggerMenuItem.Click += new EventHandler(SendBusTrigger_Click);
            _PollSplitButton = new Core.Controls.ToolStripDropDownButton();
            _PollMessageStatusBitMenuItem = new ToolStripMenuItem();
            _PollMessageStatusBitTextBox = new Core.Controls.ToolStripTextBox();
            _PollCommandMenuItem = new ToolStripMenuItem();
            _PollWriteCommandTextBox = new Core.Controls.ToolStripTextBox();
            _PollIntervalMenuItem = new ToolStripMenuItem();
            _PollIntervalTextBox = new Core.Controls.ToolStripTextBox();
            __PollEnabledMenuItem = new Core.Controls.ToolStripMenuItem();
            __PollEnabledMenuItem.CheckStateChanged += new EventHandler(PollEnabledMenuItem_CheckStateChanged);
            __PollEnabledMenuItem.Click += new EventHandler(PollEnabledMenuItem_Click);
            _PollAutoReadMenuItem = new Core.Controls.ToolStripMenuItem();
            __PollSendMenuItem = new ToolStripMenuItem();
            __PollSendMenuItem.Click += new EventHandler(PollSendMenuItem_Click);
            _ServiceRequestSplitButton = new Core.Controls.ToolStripDropDownButton();
            _ServiceRequestBitmaskMenuItem = new Core.Controls.ToolStripMenuItem();
            _ServiceRequestBitMaskTextBox = new Core.Controls.ToolStripTextBox();
            _ServiceRequestEnableCommandMenuItem = new ToolStripMenuItem();
            _ServiceRequestEnableCommandTextBox = new Core.Controls.ToolStripTextBox();
            __ProgramServiceRequestEnableBitmaskMenuItem = new Core.Controls.ToolStripMenuItem();
            __ProgramServiceRequestEnableBitmaskMenuItem.Click += new EventHandler(ProgramServiceRequestEnableBitmaskMenuItem_Click);
            __ToggleVisaEventHandlerMenuItem = new Core.Controls.ToolStripMenuItem();
            __ToggleVisaEventHandlerMenuItem.Click += new EventHandler(ToggleVisaEventHandlerMenuItem_CheckStateChanged);
            __ServiceRequestHandlerAddRemoveMenuItem = new Core.Controls.ToolStripMenuItem();
            __ServiceRequestHandlerAddRemoveMenuItem.Click += new EventHandler(ServiceRequestHandlerAddRemoveMenuItem_CheckStateChanged);
            _ServiceRequestAutoReadMenuItem = new Core.Controls.ToolStripMenuItem();
            _EditResourceNamesMenuItem = new ToolStripMenuItem();
            _EditResourceNamesMenuItem.Click += new EventHandler(EditResourceNamesMenuItem_Click);
            __ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ToolStrip
            // 
            __ToolStrip.BackColor = Color.Transparent;
            __ToolStrip.Dock = DockStyle.Fill;
            __ToolStrip.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __ToolStrip.GripMargin = new Padding(0);
            __ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            __ToolStrip.Items.AddRange(new ToolStripItem[] { _ApplicationDropDownButton, _DeviceDropDownButton, _SessionDownDownButton, _PollSplitButton, _ServiceRequestSplitButton });
            __ToolStrip.Location = new Point(0, 0);
            __ToolStrip.Name = "__ToolStrip";
            __ToolStrip.Size = new Size(474, 31);
            __ToolStrip.TabIndex = 12;
            // 
            // _ApplicationDropDownButton
            // 
            _ApplicationDropDownButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _ApplicationDropDownButton.DropDownItems.AddRange(new ToolStripItem[] { EditSettingsToolStripMenuItem, _TraceMenuItem, _SessionNotificationLevelMenuItem, __UsingStatusSubsystemMenuItem, _EditResourceNamesMenuItem });
            _ApplicationDropDownButton.Image = (Image)resources.GetObject("_ApplicationDropDownButton.Image");
            _ApplicationDropDownButton.ImageTransparentColor = Color.Magenta;
            _ApplicationDropDownButton.Name = "_ApplicationDropDownButton";
            _ApplicationDropDownButton.Size = new Size(46, 28);
            _ApplicationDropDownButton.Text = "App";
            _ApplicationDropDownButton.ToolTipText = "Application options";
            // 
            // EditSettingsToolStripMenuItem
            // 
            EditSettingsToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __SessionSettingsMenuItem, __DeviceSettingsMenuItem, __UserInterfaceSettingsMenuItem, __UIBaseSettingsMenuItem });
            EditSettingsToolStripMenuItem.Name = "EditSettingsToolStripMenuItem";
            EditSettingsToolStripMenuItem.Size = new Size(256, 22);
            EditSettingsToolStripMenuItem.Text = "Edit Settings";
            // 
            // _SessionSettingsMenuItem
            // 
            __SessionSettingsMenuItem.Name = "__SessionSettingsMenuItem";
            __SessionSettingsMenuItem.Size = new Size(134, 22);
            __SessionSettingsMenuItem.Text = "Session...";
            __SessionSettingsMenuItem.ToolTipText = "Opens session settings dialog";
            // 
            // _DeviceSettingsMenuItem
            // 
            __DeviceSettingsMenuItem.Name = "__DeviceSettingsMenuItem";
            __DeviceSettingsMenuItem.Size = new Size(134, 22);
            __DeviceSettingsMenuItem.Text = "Device...";
            __DeviceSettingsMenuItem.ToolTipText = "Opens device settings editor dialog";
            // 
            // _UserInterfaceSettingsMenuItem
            // 
            __UserInterfaceSettingsMenuItem.Name = "__UserInterfaceSettingsMenuItem";
            __UserInterfaceSettingsMenuItem.Size = new Size(134, 22);
            __UserInterfaceSettingsMenuItem.Text = "UI...";
            __UserInterfaceSettingsMenuItem.ToolTipText = "Opens User Interface settings editor dialog";
            // 
            // _UIBaseSettingsMenuItem
            // 
            __UIBaseSettingsMenuItem.Name = "__UIBaseSettingsMenuItem";
            __UIBaseSettingsMenuItem.Size = new Size(134, 22);
            __UIBaseSettingsMenuItem.Text = "UI Base...";
            __UIBaseSettingsMenuItem.ToolTipText = "Edits the user interface base settings";
            // 
            // _TraceMenuItem
            // 
            _TraceMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _TraceLogLevelMenuItem, _TraceShowLevelMenuItem });
            _TraceMenuItem.Name = "_TraceMenuItem";
            _TraceMenuItem.Size = new Size(256, 22);
            _TraceMenuItem.Text = "Select Trace Levels";
            _TraceMenuItem.ToolTipText = "Opens the trace menus";
            // 
            // _TraceLogLevelMenuItem
            // 
            _TraceLogLevelMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _TraceLogLevelComboBox });
            _TraceLogLevelMenuItem.Name = "_TraceLogLevelMenuItem";
            _TraceLogLevelMenuItem.Size = new Size(194, 22);
            _TraceLogLevelMenuItem.Text = "Trace Log Level";
            _TraceLogLevelMenuItem.ToolTipText = "Selects trace level for logging";
            // 
            // _TraceLogLevelComboBox
            // 
            _TraceLogLevelComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _TraceLogLevelComboBox.Name = "_TraceLogLevelComboBox";
            _TraceLogLevelComboBox.Size = new Size(100, 22);
            _TraceLogLevelComboBox.ToolTipText = "Selects trace level for logging";
            // 
            // _TraceShowLevelMenuItem
            // 
            _TraceShowLevelMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _TraceShowLevelComboBox });
            _TraceShowLevelMenuItem.Name = "_TraceShowLevelMenuItem";
            _TraceShowLevelMenuItem.Size = new Size(194, 22);
            _TraceShowLevelMenuItem.Text = "Display Trace Level";
            _TraceShowLevelMenuItem.ToolTipText = "Selects trace level for display";
            // 
            // _TraceShowLevelComboBox
            // 
            _TraceShowLevelComboBox.Name = "_TraceShowLevelComboBox";
            _TraceShowLevelComboBox.Size = new Size(100, 22);
            _TraceShowLevelComboBox.ToolTipText = "Selects trace level for display";
            // 
            // _SessionNotificationLevelMenuItem
            // 
            _SessionNotificationLevelMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _SessionNotificationLevelComboBox });
            _SessionNotificationLevelMenuItem.Name = "_SessionNotificationLevelMenuItem";
            _SessionNotificationLevelMenuItem.Size = new Size(256, 22);
            _SessionNotificationLevelMenuItem.Text = "Session Notification Level";
            _SessionNotificationLevelMenuItem.ToolTipText = "Shows the session notification level selector";
            // 
            // _SessionNotificationLevelComboBox
            // 
            _SessionNotificationLevelComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _SessionNotificationLevelComboBox.Name = "_SessionNotificationLevelComboBox";
            _SessionNotificationLevelComboBox.Size = new Size(100, 22);
            _SessionNotificationLevelComboBox.ToolTipText = "Select the session notification level";
            // 
            // _UsingStatusSubsystemMenuItem
            // 
            __UsingStatusSubsystemMenuItem.CheckOnClick = true;
            __UsingStatusSubsystemMenuItem.Name = "__UsingStatusSubsystemMenuItem";
            __UsingStatusSubsystemMenuItem.Size = new Size(256, 22);
            __UsingStatusSubsystemMenuItem.Text = "Using Status Subsystem Only";
            __UsingStatusSubsystemMenuItem.ToolTipText = "Toggle to use status or all subsystems";
            // 
            // _DeviceDropDownButton
            // 
            _DeviceDropDownButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _DeviceDropDownButton.DropDownItems.AddRange(new ToolStripItem[] { _ResetMenuItem, __ClearErrorReportMenuItem, __ReadDeviceErrorsMenuItem, __ReadTerminalsStateMenuItem });
            _DeviceDropDownButton.ImageTransparentColor = Color.Magenta;
            _DeviceDropDownButton.Name = "_DeviceDropDownButton";
            _DeviceDropDownButton.Size = new Size(62, 28);
            _DeviceDropDownButton.Text = "Device";
            _DeviceDropDownButton.ToolTipText = "Device resets, Service Requests and trace management";
            // 
            // _ResetMenuItem
            // 
            _ResetMenuItem.DropDownItems.AddRange(new ToolStripItem[] { __ClearInterfaceMenuItem, __ClearDeviceMenuItem, __ResetKnownStateMenuItem, __ClearExecutionStateMenuItem, __InitKnownStateMenuItem });
            _ResetMenuItem.Name = "_ResetMenuItem";
            _ResetMenuItem.Size = new Size(205, 22);
            _ResetMenuItem.Text = "Reset...";
            _ResetMenuItem.ToolTipText = "Opens the reset menus";
            // 
            // _ClearInterfaceMenuItem
            // 
            __ClearInterfaceMenuItem.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __ClearInterfaceMenuItem.Name = "__ClearInterfaceMenuItem";
            __ClearInterfaceMenuItem.Size = new Size(242, 22);
            __ClearInterfaceMenuItem.Text = "Clear Interface";
            __ClearInterfaceMenuItem.ToolTipText = "Issues an interface clear command";
            // 
            // _ClearDeviceMenuItem
            // 
            __ClearDeviceMenuItem.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __ClearDeviceMenuItem.Name = "__ClearDeviceMenuItem";
            __ClearDeviceMenuItem.Size = new Size(242, 22);
            __ClearDeviceMenuItem.Text = "Clear Device (SDC)";
            __ClearDeviceMenuItem.ToolTipText = "Issues Selective Device Clear";
            // 
            // _ResetKnownStateMenuItem
            // 
            __ResetKnownStateMenuItem.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __ResetKnownStateMenuItem.Name = "__ResetKnownStateMenuItem";
            __ResetKnownStateMenuItem.Size = new Size(242, 22);
            __ResetKnownStateMenuItem.Text = "Reset Known State (RST)";
            __ResetKnownStateMenuItem.ToolTipText = "Issues *RST";
            // 
            // _ClearExecutionStateMenuItem
            // 
            __ClearExecutionStateMenuItem.Name = "__ClearExecutionStateMenuItem";
            __ClearExecutionStateMenuItem.Size = new Size(242, 22);
            __ClearExecutionStateMenuItem.Text = "Clear Execution State (CLS)";
            __ClearExecutionStateMenuItem.ToolTipText = "Clears execution state (CLS)";
            // 
            // _InitKnownStateMenuItem
            // 
            __InitKnownStateMenuItem.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            __InitKnownStateMenuItem.Name = "__InitKnownStateMenuItem";
            __InitKnownStateMenuItem.Size = new Size(242, 22);
            __InitKnownStateMenuItem.Text = "Initialize Known State";
            __InitKnownStateMenuItem.ToolTipText = "Issues *RST, CLear and initialize to custom known state";
            // 
            // _ClearErrorReportMenuItem
            // 
            __ClearErrorReportMenuItem.Name = "__ClearErrorReportMenuItem";
            __ClearErrorReportMenuItem.Size = new Size(205, 22);
            __ClearErrorReportMenuItem.Text = "Clear Error Report";
            __ClearErrorReportMenuItem.ToolTipText = "Clears the error report";
            // 
            // _ReadDeviceErrorsMenuItem
            // 
            __ReadDeviceErrorsMenuItem.Name = "__ReadDeviceErrorsMenuItem";
            __ReadDeviceErrorsMenuItem.Size = new Size(205, 22);
            __ReadDeviceErrorsMenuItem.Text = "Read Device Errors";
            __ReadDeviceErrorsMenuItem.ToolTipText = "Reads device errors";
            // 
            // _ReadTerminalsStateMenuItem
            // 
            __ReadTerminalsStateMenuItem.Name = "__ReadTerminalsStateMenuItem";
            __ReadTerminalsStateMenuItem.Size = new Size(205, 22);
            __ReadTerminalsStateMenuItem.Text = "Read Terminals State";
            // 
            // _SessionDownDownButton
            // 
            _SessionDownDownButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _SessionDownDownButton.DropDownItems.AddRange(new ToolStripItem[] { _ReadTerminationMenuItem, _WriteTerminationMenuItem, _SessionTimeoutMenuItem, __SessionReadStatusByteMenuItem, __SessionReadStandardEventRegisterMenuItem, __SendBusTriggerMenuItem });
            _SessionDownDownButton.Image = (Image)resources.GetObject("_SessionDownDownButton.Image");
            _SessionDownDownButton.ImageTransparentColor = Color.Magenta;
            _SessionDownDownButton.Name = "_SessionDownDownButton";
            _SessionDownDownButton.Size = new Size(67, 28);
            _SessionDownDownButton.Text = "Session";
            _SessionDownDownButton.ToolTipText = "Select Session Options";
            // 
            // _ReadTerminationMenuItem
            // 
            _ReadTerminationMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _ReadTerminationTextBox, _ReadTerminationEnabledMenuItem });
            _ReadTerminationMenuItem.Name = "_ReadTerminationMenuItem";
            _ReadTerminationMenuItem.Size = new Size(257, 22);
            _ReadTerminationMenuItem.Text = "Read Termination";
            _ReadTerminationMenuItem.ToolTipText = "Read termination. Typically Line Feed (10)";
            // 
            // _ReadTerminationTextBox
            // 
            _ReadTerminationTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _ReadTerminationTextBox.Name = "_ReadTerminationTextBox";
            _ReadTerminationTextBox.Size = new Size(100, 22);
            _ReadTerminationTextBox.ToolTipText = "Read termination in Hex";
            // 
            // _ReadTerminationEnabledMenuItem
            // 
            _ReadTerminationEnabledMenuItem.CheckOnClick = true;
            _ReadTerminationEnabledMenuItem.Name = "_ReadTerminationEnabledMenuItem";
            _ReadTerminationEnabledMenuItem.Size = new Size(160, 22);
            _ReadTerminationEnabledMenuItem.Text = "Enabled";
            _ReadTerminationEnabledMenuItem.ToolTipText = "Checked to enable read termination";
            // 
            // _WriteTerminationMenuItem
            // 
            _WriteTerminationMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _WriteTerminationTextBox });
            _WriteTerminationMenuItem.Name = "_WriteTerminationMenuItem";
            _WriteTerminationMenuItem.Size = new Size(257, 22);
            _WriteTerminationMenuItem.Text = "Write Termination";
            _WriteTerminationMenuItem.ToolTipText = @"Write termination; typically \n (line feed)";
            // 
            // _WriteTerminationTextBox
            // 
            _WriteTerminationTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _WriteTerminationTextBox.Name = "_WriteTerminationTextBox";
            _WriteTerminationTextBox.Size = new Size(100, 22);
            _WriteTerminationTextBox.ToolTipText = @"Write termination escape sequence, e.g., \n\r";
            // 
            // _SessionTimeoutMenuItem
            // 
            _SessionTimeoutMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _SessionTimeoutTextBox, __StoreTimeoutMenuItem, __RestoreTimeoutMenuItem });
            _SessionTimeoutMenuItem.Name = "_SessionTimeoutMenuItem";
            _SessionTimeoutMenuItem.Size = new Size(257, 22);
            _SessionTimeoutMenuItem.Text = "Timeout";
            // 
            // _SessionTimeoutTextBox
            // 
            _SessionTimeoutTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _SessionTimeoutTextBox.Name = "_SessionTimeoutTextBox";
            _SessionTimeoutTextBox.Size = new Size(100, 22);
            _SessionTimeoutTextBox.ToolTipText = "Timeout in seconds";
            // 
            // _StoreTimeoutMenuItem
            // 
            __StoreTimeoutMenuItem.Name = "__StoreTimeoutMenuItem";
            __StoreTimeoutMenuItem.Size = new Size(172, 22);
            __StoreTimeoutMenuItem.Text = "Store and Set";
            __StoreTimeoutMenuItem.ToolTipText = "Pushes the previous timeout onto the stack and set a new timeout value";
            // 
            // _RestoreTimeoutMenuItem
            // 
            __RestoreTimeoutMenuItem.Name = "__RestoreTimeoutMenuItem";
            __RestoreTimeoutMenuItem.Size = new Size(172, 22);
            __RestoreTimeoutMenuItem.Text = "Restore and Set";
            __RestoreTimeoutMenuItem.ToolTipText = "Pulls the last timeout from the stack and sets it";
            // 
            // _SessionReadStatusByteMenuItem
            // 
            __SessionReadStatusByteMenuItem.Name = "__SessionReadStatusByteMenuItem";
            __SessionReadStatusByteMenuItem.Size = new Size(257, 22);
            __SessionReadStatusByteMenuItem.Text = "Read Status Register";
            __SessionReadStatusByteMenuItem.ToolTipText = "Reads the session status byte";
            // 
            // _SessionReadStandardEventRegisterMenuItem
            // 
            __SessionReadStandardEventRegisterMenuItem.Name = "__SessionReadStandardEventRegisterMenuItem";
            __SessionReadStandardEventRegisterMenuItem.Size = new Size(257, 22);
            __SessionReadStandardEventRegisterMenuItem.Text = "Read Standard Event Register";
            __SessionReadStandardEventRegisterMenuItem.ToolTipText = "Reads the standard event register";
            // 
            // _SendBusTriggerMenuItem
            // 
            __SendBusTriggerMenuItem.Name = "__SendBusTriggerMenuItem";
            __SendBusTriggerMenuItem.Size = new Size(257, 22);
            __SendBusTriggerMenuItem.Text = "Send Bus Trigger";
            __SendBusTriggerMenuItem.ToolTipText = "Sneds a bus trigger to the instrument";
            // 
            // _PollSplitButton
            // 
            _PollSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _PollSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _PollMessageStatusBitMenuItem, _PollCommandMenuItem, _PollIntervalMenuItem, __PollEnabledMenuItem, _PollAutoReadMenuItem, __PollSendMenuItem });
            _PollSplitButton.Image = (Image)resources.GetObject("_PollSplitButton.Image");
            _PollSplitButton.ImageTransparentColor = Color.Magenta;
            _PollSplitButton.Name = "_PollSplitButton";
            _PollSplitButton.Size = new Size(45, 28);
            _PollSplitButton.Text = "Poll";
            // 
            // _PollMessageStatusBitMenuItem
            // 
            _PollMessageStatusBitMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _PollMessageStatusBitTextBox });
            _PollMessageStatusBitMenuItem.Name = "_PollMessageStatusBitMenuItem";
            _PollMessageStatusBitMenuItem.Size = new Size(193, 22);
            _PollMessageStatusBitMenuItem.Text = "Bitmask:";
            _PollMessageStatusBitMenuItem.ToolTipText = "Message is flagged as received when status byte message bit  is on";
            // 
            // _PollMessageStatusBitTextBox
            // 
            _PollMessageStatusBitTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _PollMessageStatusBitTextBox.Name = "_PollMessageStatusBitTextBox";
            _PollMessageStatusBitTextBox.Size = new Size(100, 22);
            _PollMessageStatusBitTextBox.ToolTipText = "Message status bit used to detect if a message is available for reading";
            // 
            // _PollCommandMenuItem
            // 
            _PollCommandMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _PollWriteCommandTextBox });
            _PollCommandMenuItem.Name = "_PollCommandMenuItem";
            _PollCommandMenuItem.Size = new Size(193, 22);
            _PollCommandMenuItem.Text = "Command:";
            _PollCommandMenuItem.ToolTipText = "Click Send Command to send this command to the instrument";
            // 
            // _PollWriteCommandTextBox
            // 
            _PollWriteCommandTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _PollWriteCommandTextBox.Name = "_PollWriteCommandTextBox";
            _PollWriteCommandTextBox.Size = new Size(100, 22);
            _PollWriteCommandTextBox.Text = @"*IDN?\n";
            _PollWriteCommandTextBox.ToolTipText = "Command to write";
            // 
            // _PollIntervalMenuItem
            // 
            _PollIntervalMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _PollIntervalTextBox });
            _PollIntervalMenuItem.Name = "_PollIntervalMenuItem";
            _PollIntervalMenuItem.Size = new Size(193, 22);
            _PollIntervalMenuItem.Text = "Interval:";
            // 
            // _PollIntervalTextBox
            // 
            _PollIntervalTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _PollIntervalTextBox.Name = "_PollIntervalTextBox";
            _PollIntervalTextBox.Size = new Size(100, 22);
            _PollIntervalTextBox.ToolTipText = "Sets the polling interval in seconds";
            // 
            // _PollEnabledMenuItem
            // 
            __PollEnabledMenuItem.Name = "__PollEnabledMenuItem";
            __PollEnabledMenuItem.Size = new Size(193, 22);
            __PollEnabledMenuItem.Text = "Press to Start";
            __PollEnabledMenuItem.ToolTipText = "Check to enable polling the instrument service request on a timer";
            // 
            // _PollAutoReadMenuItem
            // 
            _PollAutoReadMenuItem.CheckOnClick = true;
            _PollAutoReadMenuItem.Name = "_PollAutoReadMenuItem";
            _PollAutoReadMenuItem.Size = new Size(193, 22);
            _PollAutoReadMenuItem.Text = "Auto Read Enabled";
            _PollAutoReadMenuItem.ToolTipText = "Reads upon detecting the message available bit";
            // 
            // _PollSendMenuItem
            // 
            __PollSendMenuItem.Name = "__PollSendMenuItem";
            __PollSendMenuItem.Size = new Size(193, 22);
            __PollSendMenuItem.Text = "Send Command";
            __PollSendMenuItem.ToolTipText = "Click to send the poll command.  A command can also be sent from the Session pane" + "l.";
            // 
            // _ServiceRequestSplitButton
            // 
            _ServiceRequestSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _ServiceRequestSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _ServiceRequestBitmaskMenuItem, _ServiceRequestEnableCommandMenuItem, __ProgramServiceRequestEnableBitmaskMenuItem, __ToggleVisaEventHandlerMenuItem, __ServiceRequestHandlerAddRemoveMenuItem, _ServiceRequestAutoReadMenuItem });
            _ServiceRequestSplitButton.Image = (Image)resources.GetObject("_ServiceRequestSplitButton.Image");
            _ServiceRequestSplitButton.ImageTransparentColor = Color.Magenta;
            _ServiceRequestSplitButton.Name = "_ServiceRequestSplitButton";
            _ServiceRequestSplitButton.Size = new Size(46, 28);
            _ServiceRequestSplitButton.Text = "SRQ";
            // 
            // _ServiceRequestBitmaskMenuItem
            // 
            _ServiceRequestBitmaskMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _ServiceRequestBitMaskTextBox });
            _ServiceRequestBitmaskMenuItem.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _ServiceRequestBitmaskMenuItem.Name = "_ServiceRequestBitmaskMenuItem";
            _ServiceRequestBitmaskMenuItem.Size = new Size(232, 22);
            _ServiceRequestBitmaskMenuItem.Text = "Events Enable Bitmask:";
            _ServiceRequestBitmaskMenuItem.ToolTipText = "Specifies which instrument events invoke service request";
            // 
            // _ServiceRequestBitMaskTextBox
            // 
            _ServiceRequestBitMaskTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _ServiceRequestBitMaskTextBox.Name = "_ServiceRequestBitMaskTextBox";
            _ServiceRequestBitMaskTextBox.Size = new Size(100, 22);
            _ServiceRequestBitMaskTextBox.ToolTipText = "Service request Bit Mask in Hex";
            // 
            // _ServiceRequestEnableCommandMenuItem
            // 
            _ServiceRequestEnableCommandMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _ServiceRequestEnableCommandTextBox });
            _ServiceRequestEnableCommandMenuItem.Name = "_ServiceRequestEnableCommandMenuItem";
            _ServiceRequestEnableCommandMenuItem.Size = new Size(232, 22);
            _ServiceRequestEnableCommandMenuItem.Text = "Events Enable Command:";
            // 
            // _ServiceRequestEnableCommandTextBox
            // 
            _ServiceRequestEnableCommandTextBox.Font = new Font("Segoe UI", 9.0f);
            _ServiceRequestEnableCommandTextBox.Name = "_ServiceRequestEnableCommandTextBox";
            _ServiceRequestEnableCommandTextBox.Size = new Size(100, 22);
            _ServiceRequestEnableCommandTextBox.ToolTipText = "Enter the service request enable command, e.g., *SRE {0:D}\" ";
            // 
            // _ProgramServiceRequestEnableBitmaskMenuItem
            // 
            __ProgramServiceRequestEnableBitmaskMenuItem.Name = "__ProgramServiceRequestEnableBitmaskMenuItem";
            __ProgramServiceRequestEnableBitmaskMenuItem.Size = new Size(232, 22);
            __ProgramServiceRequestEnableBitmaskMenuItem.Text = "Apply SRQ Bitmask";
            __ProgramServiceRequestEnableBitmaskMenuItem.ToolTipText = "Sets the service request enable bitmask";
            // 
            // _ToggleVisaEventHandlerMenuItem
            // 
            __ToggleVisaEventHandlerMenuItem.Name = "__ToggleVisaEventHandlerMenuItem";
            __ToggleVisaEventHandlerMenuItem.Size = new Size(232, 22);
            __ToggleVisaEventHandlerMenuItem.Text = "Toggle Visa Events";
            // 
            // _ServiceRequestHandlerAddRemoveMenuItem
            // 
            __ServiceRequestHandlerAddRemoveMenuItem.Name = "__ServiceRequestHandlerAddRemoveMenuItem";
            __ServiceRequestHandlerAddRemoveMenuItem.Size = new Size(232, 22);
            __ServiceRequestHandlerAddRemoveMenuItem.Text = "Toggle SRQ Handling";
            __ServiceRequestHandlerAddRemoveMenuItem.ToolTipText = "Check to handle device service request";
            // 
            // _ServiceRequestAutoReadMenuItem
            // 
            _ServiceRequestAutoReadMenuItem.CheckOnClick = true;
            _ServiceRequestAutoReadMenuItem.Name = "_ServiceRequestAutoReadMenuItem";
            _ServiceRequestAutoReadMenuItem.Size = new Size(232, 22);
            _ServiceRequestAutoReadMenuItem.Text = "Auto Read Enabled";
            _ServiceRequestAutoReadMenuItem.ToolTipText = "Enables reading messages when service request is enabled";
            // 
            // EditResourceNamesMenuItem
            // 
            _EditResourceNamesMenuItem.Name = "_EditResourceNamesMenuItem";
            _EditResourceNamesMenuItem.Size = new Size(256, 22);
            _EditResourceNamesMenuItem.Text = "Edit Resource Names";
            // 
            // StatusView
            // 
            BackColor = Color.Transparent;
            Controls.Add(__ToolStrip);
            Name = "StatusView";
            Size = new Size(474, 31);
            __ToolStrip.ResumeLayout(false);
            __ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip __ToolStrip;

        private ToolStrip _ToolStrip
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToolStrip;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToolStrip != null)
                {
                    __ToolStrip.DoubleClick -= ToolStrip_DoubleClick;
                }

                __ToolStrip = value;
                if (__ToolStrip != null)
                {
                    __ToolStrip.DoubleClick += ToolStrip_DoubleClick;
                }
            }
        }

        private Core.Controls.ToolStripDropDownButton _DeviceDropDownButton;
        private ToolStripMenuItem _ResetMenuItem;
        private Core.Controls.ToolStripMenuItem __ClearInterfaceMenuItem;

        private Core.Controls.ToolStripMenuItem _ClearInterfaceMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearInterfaceMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearInterfaceMenuItem != null)
                {
                    __ClearInterfaceMenuItem.Click -= ClearInterfaceMenuItem_Click;
                }

                __ClearInterfaceMenuItem = value;
                if (__ClearInterfaceMenuItem != null)
                {
                    __ClearInterfaceMenuItem.Click += ClearInterfaceMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ClearDeviceMenuItem;

        private ToolStripMenuItem _ClearDeviceMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearDeviceMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearDeviceMenuItem != null)
                {
                    __ClearDeviceMenuItem.Click -= ClearDeviceMenuItem_Click;
                }

                __ClearDeviceMenuItem = value;
                if (__ClearDeviceMenuItem != null)
                {
                    __ClearDeviceMenuItem.Click += ClearDeviceMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ResetKnownStateMenuItem;

        private ToolStripMenuItem _ResetKnownStateMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ResetKnownStateMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ResetKnownStateMenuItem != null)
                {
                    __ResetKnownStateMenuItem.Click -= ResetKnownStateMenuItem_Click;
                }

                __ResetKnownStateMenuItem = value;
                if (__ResetKnownStateMenuItem != null)
                {
                    __ResetKnownStateMenuItem.Click += ResetKnownStateMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __InitKnownStateMenuItem;

        private ToolStripMenuItem _InitKnownStateMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitKnownStateMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitKnownStateMenuItem != null)
                {
                    __InitKnownStateMenuItem.Click -= InitKnownStateMenuItem_Click;
                }

                __InitKnownStateMenuItem = value;
                if (__InitKnownStateMenuItem != null)
                {
                    __InitKnownStateMenuItem.Click += InitKnownStateMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ClearExecutionStateMenuItem;

        private ToolStripMenuItem _ClearExecutionStateMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearExecutionStateMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearExecutionStateMenuItem != null)
                {
                    __ClearExecutionStateMenuItem.Click -= ClearExecutionStateMenuItem_Click;
                }

                __ClearExecutionStateMenuItem = value;
                if (__ClearExecutionStateMenuItem != null)
                {
                    __ClearExecutionStateMenuItem.Click += ClearExecutionStateMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripDropDownButton _SessionDownDownButton;
        private Core.Controls.ToolStripMenuItem _ReadTerminationEnabledMenuItem;
        private ToolStripMenuItem _WriteTerminationMenuItem;
        private Core.Controls.ToolStripTextBox _WriteTerminationTextBox;
        private ToolStripMenuItem _ReadTerminationMenuItem;
        private Core.Controls.ToolStripTextBox _ReadTerminationTextBox;
        private ToolStripMenuItem _SessionTimeoutMenuItem;

        private Core.Controls.ToolStripTextBox __SessionTimeoutTextBox;

        private Core.Controls.ToolStripTextBox _SessionTimeoutTextBox
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get {
                return __SessionTimeoutTextBox;
            }

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( __SessionTimeoutTextBox != null )
                {
                    __SessionTimeoutTextBox.Validating -= SessionTimeoutTextBox_Validating;
                }

                __SessionTimeoutTextBox = value;
                if ( __SessionTimeoutTextBox != null )
                {
                    __SessionTimeoutTextBox.Validating += SessionTimeoutTextBox_Validating;
                    // __SessionTimeoutTextBox.LostFocus += SessionTimeoutTextBox_Validated;
                }
            }
        }


        private ToolStripMenuItem __StoreTimeoutMenuItem;

        private ToolStripMenuItem _StoreTimeoutMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StoreTimeoutMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StoreTimeoutMenuItem != null)
                {
                    __StoreTimeoutMenuItem.Click -= StoreTimeoutMenuItem_Click;
                }

                __StoreTimeoutMenuItem = value;
                if (__StoreTimeoutMenuItem != null)
                {
                    __StoreTimeoutMenuItem.Click += StoreTimeoutMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripDropDownButton _PollSplitButton;
        private ToolStripMenuItem _PollCommandMenuItem;
        private Core.Controls.ToolStripTextBox _PollWriteCommandTextBox;
        private ToolStripMenuItem _PollIntervalMenuItem;
        private Core.Controls.ToolStripTextBox _PollIntervalTextBox;
        private ToolStripMenuItem _PollMessageStatusBitMenuItem;
        private Core.Controls.ToolStripTextBox _PollMessageStatusBitTextBox;
        private ToolStripMenuItem __SessionReadStatusByteMenuItem;

        private ToolStripMenuItem _SessionReadStatusByteMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SessionReadStatusByteMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SessionReadStatusByteMenuItem != null)
                {
                    __SessionReadStatusByteMenuItem.Click -= SessionReadStatusByteMenuItem_Click;
                }

                __SessionReadStatusByteMenuItem = value;
                if (__SessionReadStatusByteMenuItem != null)
                {
                    __SessionReadStatusByteMenuItem.Click += SessionReadStatusByteMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __RestoreTimeoutMenuItem;

        private ToolStripMenuItem _RestoreTimeoutMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RestoreTimeoutMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RestoreTimeoutMenuItem != null)
                {
                    __RestoreTimeoutMenuItem.Click -= RestoreTimeoutMenuItem_Click;
                }

                __RestoreTimeoutMenuItem = value;
                if (__RestoreTimeoutMenuItem != null)
                {
                    __RestoreTimeoutMenuItem.Click += RestoreTimeoutMenuItem_Click;
                }
            }
        }

        private ToolStripDropDownButton _ApplicationDropDownButton;
        private ToolStripMenuItem _TraceMenuItem;
        private ToolStripMenuItem _TraceLogLevelMenuItem;
        private Core.Controls.ToolStripComboBox _TraceLogLevelComboBox;
        private ToolStripMenuItem _TraceShowLevelMenuItem;
        private Core.Controls.ToolStripComboBox _TraceShowLevelComboBox;
        private ToolStripMenuItem _SessionNotificationLevelMenuItem;
        private Core.Controls.ToolStripComboBox _SessionNotificationLevelComboBox;
        private Core.Controls.ToolStripMenuItem __UsingStatusSubsystemMenuItem;

        private Core.Controls.ToolStripMenuItem _UsingStatusSubsystemMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __UsingStatusSubsystemMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__UsingStatusSubsystemMenuItem != null)
                {
                    __UsingStatusSubsystemMenuItem.CheckStateChanged -= OpenStatusSubsystemMenuItem_CheckStateChanged;
                }

                __UsingStatusSubsystemMenuItem = value;
                if (__UsingStatusSubsystemMenuItem != null)
                {
                    __UsingStatusSubsystemMenuItem.CheckStateChanged += OpenStatusSubsystemMenuItem_CheckStateChanged;
                }
            }
        }

        private ToolStripMenuItem __ClearErrorReportMenuItem;

        private ToolStripMenuItem _ClearErrorReportMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearErrorReportMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearErrorReportMenuItem != null)
                {
                    __ClearErrorReportMenuItem.Click -= ClearErrorReportMenuItem_Click;
                }

                __ClearErrorReportMenuItem = value;
                if (__ClearErrorReportMenuItem != null)
                {
                    __ClearErrorReportMenuItem.Click += ClearErrorReportMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __ReadDeviceErrorsMenuItem;

        private ToolStripMenuItem _ReadDeviceErrorsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadDeviceErrorsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadDeviceErrorsMenuItem != null)
                {
                    __ReadDeviceErrorsMenuItem.Click -= ReadDeviceErrorsMenuItem_Click;
                }

                __ReadDeviceErrorsMenuItem = value;
                if (__ReadDeviceErrorsMenuItem != null)
                {
                    __ReadDeviceErrorsMenuItem.Click += ReadDeviceErrorsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __SessionReadStandardEventRegisterMenuItem;

        private ToolStripMenuItem _SessionReadStandardEventRegisterMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SessionReadStandardEventRegisterMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SessionReadStandardEventRegisterMenuItem != null)
                {
                    __SessionReadStandardEventRegisterMenuItem.Click -= SessionReadStandardEventRegisterMenuItem_Click;
                }

                __SessionReadStandardEventRegisterMenuItem = value;
                if (__SessionReadStandardEventRegisterMenuItem != null)
                {
                    __SessionReadStandardEventRegisterMenuItem.Click += SessionReadStandardEventRegisterMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __PollSendMenuItem;

        private ToolStripMenuItem _PollSendMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PollSendMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PollSendMenuItem != null)
                {
                    __PollSendMenuItem.Click -= PollSendMenuItem_Click;
                }

                __PollSendMenuItem = value;
                if (__PollSendMenuItem != null)
                {
                    __PollSendMenuItem.Click += PollSendMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripMenuItem __PollEnabledMenuItem;

        private Core.Controls.ToolStripMenuItem _PollEnabledMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PollEnabledMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PollEnabledMenuItem != null)
                {
                    __PollEnabledMenuItem.CheckStateChanged -= PollEnabledMenuItem_CheckStateChanged;
                    __PollEnabledMenuItem.Click -= PollEnabledMenuItem_Click;
                }

                __PollEnabledMenuItem = value;
                if (__PollEnabledMenuItem != null)
                {
                    __PollEnabledMenuItem.CheckStateChanged += PollEnabledMenuItem_CheckStateChanged;
                    __PollEnabledMenuItem.Click += PollEnabledMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripMenuItem _PollAutoReadMenuItem;
        private Core.Controls.ToolStripDropDownButton _ServiceRequestSplitButton;
        private Core.Controls.ToolStripMenuItem _ServiceRequestBitmaskMenuItem;
        private Core.Controls.ToolStripTextBox _ServiceRequestBitMaskTextBox;
        private ToolStripMenuItem _ServiceRequestEnableCommandMenuItem;
        private Core.Controls.ToolStripTextBox _ServiceRequestEnableCommandTextBox;
        private Core.Controls.ToolStripMenuItem __ProgramServiceRequestEnableBitmaskMenuItem;

        private Core.Controls.ToolStripMenuItem _ProgramServiceRequestEnableBitmaskMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ProgramServiceRequestEnableBitmaskMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ProgramServiceRequestEnableBitmaskMenuItem != null)
                {
                    __ProgramServiceRequestEnableBitmaskMenuItem.Click -= ProgramServiceRequestEnableBitmaskMenuItem_Click;
                }

                __ProgramServiceRequestEnableBitmaskMenuItem = value;
                if (__ProgramServiceRequestEnableBitmaskMenuItem != null)
                {
                    __ProgramServiceRequestEnableBitmaskMenuItem.Click += ProgramServiceRequestEnableBitmaskMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripMenuItem __ServiceRequestHandlerAddRemoveMenuItem;

        private Core.Controls.ToolStripMenuItem _ServiceRequestHandlerAddRemoveMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ServiceRequestHandlerAddRemoveMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ServiceRequestHandlerAddRemoveMenuItem != null)
                {
                    __ServiceRequestHandlerAddRemoveMenuItem.Click -= ServiceRequestHandlerAddRemoveMenuItem_CheckStateChanged;
                }

                __ServiceRequestHandlerAddRemoveMenuItem = value;
                if (__ServiceRequestHandlerAddRemoveMenuItem != null)
                {
                    __ServiceRequestHandlerAddRemoveMenuItem.Click += ServiceRequestHandlerAddRemoveMenuItem_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripMenuItem _ServiceRequestAutoReadMenuItem;
        private ToolStripMenuItem __ReadTerminalsStateMenuItem;

        private ToolStripMenuItem _ReadTerminalsStateMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadTerminalsStateMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadTerminalsStateMenuItem != null)
                {
                    __ReadTerminalsStateMenuItem.Click -= ReadTerminalsStateMenuItem_Click;
                }

                __ReadTerminalsStateMenuItem = value;
                if (__ReadTerminalsStateMenuItem != null)
                {
                    __ReadTerminalsStateMenuItem.Click += ReadTerminalsStateMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripMenuItem __ToggleVisaEventHandlerMenuItem;

        private Core.Controls.ToolStripMenuItem _ToggleVisaEventHandlerMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ToggleVisaEventHandlerMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ToggleVisaEventHandlerMenuItem != null)
                {
                    __ToggleVisaEventHandlerMenuItem.Click -= ToggleVisaEventHandlerMenuItem_CheckStateChanged;
                }

                __ToggleVisaEventHandlerMenuItem = value;
                if (__ToggleVisaEventHandlerMenuItem != null)
                {
                    __ToggleVisaEventHandlerMenuItem.Click += ToggleVisaEventHandlerMenuItem_CheckStateChanged;
                }
            }
        }

        private ToolStripMenuItem __SendBusTriggerMenuItem;

        private ToolStripMenuItem _SendBusTriggerMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SendBusTriggerMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SendBusTriggerMenuItem != null)
                {
                    __SendBusTriggerMenuItem.Click -= SendBusTrigger_Click;
                }

                __SendBusTriggerMenuItem = value;
                if (__SendBusTriggerMenuItem != null)
                {
                    __SendBusTriggerMenuItem.Click += SendBusTrigger_Click;
                }
            }
        }

        private ToolStripMenuItem EditSettingsToolStripMenuItem;
        private ToolStripMenuItem __SessionSettingsMenuItem;

        private ToolStripMenuItem _SessionSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SessionSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SessionSettingsMenuItem != null)
                {
                    __SessionSettingsMenuItem.Click -= SessionSettingsMenuItem_Click;
                }

                __SessionSettingsMenuItem = value;
                if (__SessionSettingsMenuItem != null)
                {
                    __SessionSettingsMenuItem.Click += SessionSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __DeviceSettingsMenuItem;

        private ToolStripMenuItem _DeviceSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DeviceSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DeviceSettingsMenuItem != null)
                {
                    __DeviceSettingsMenuItem.Click -= DeviceSettingsMenuItem_Click;
                }

                __DeviceSettingsMenuItem = value;
                if (__DeviceSettingsMenuItem != null)
                {
                    __DeviceSettingsMenuItem.Click += DeviceSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __UserInterfaceSettingsMenuItem;

        private ToolStripMenuItem _UserInterfaceSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __UserInterfaceSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__UserInterfaceSettingsMenuItem != null)
                {
                    __UserInterfaceSettingsMenuItem.Click -= UserInterfaceToolStripMenuItem_Click;
                }

                __UserInterfaceSettingsMenuItem = value;
                if (__UserInterfaceSettingsMenuItem != null)
                {
                    __UserInterfaceSettingsMenuItem.Click += UserInterfaceToolStripMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __UIBaseSettingsMenuItem;

        private ToolStripMenuItem _UIBaseSettingsMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __UIBaseSettingsMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__UIBaseSettingsMenuItem != null)
                {
                    __UIBaseSettingsMenuItem.Click -= UIBaseSettingsMenuItem_Click;
                }

                __UIBaseSettingsMenuItem = value;
                if (__UIBaseSettingsMenuItem != null)
                {
                    __UIBaseSettingsMenuItem.Click += UIBaseSettingsMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem _EditResourceNamesMenuItem;

        private ToolStripMenuItem EditResourceNamesMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _EditResourceNamesMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_EditResourceNamesMenuItem != null)
                {
                    _EditResourceNamesMenuItem.Click -= EditResourceNamesMenuItem_Click;
                }

                _EditResourceNamesMenuItem = value;
                if (_EditResourceNamesMenuItem != null)
                {
                    _EditResourceNamesMenuItem.Click += EditResourceNamesMenuItem_Click;
                }
            }
        }
    }
}
