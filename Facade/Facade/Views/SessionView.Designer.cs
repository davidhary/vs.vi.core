using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class SessionView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionView));
            _WriteComboBox = new ComboBox();
            _ReadTextBox = new TextBox();
            __QueryButton = new Core.Controls.ToolStripButton();
            __QueryButton.Click += new EventHandler(QueryButton_Click);
            __WriteButton = new Core.Controls.ToolStripButton();
            __WriteButton.Click += new EventHandler(WriteButton_Click);
            __ReadButton = new Core.Controls.ToolStripButton();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            __ReadDelayNumeric = new Core.Controls.ToolStripNumericUpDown();
            __ReadDelayNumeric.ValueChanged += new EventHandler<EventArgs>(ReadDelayNumeric_ValueChanged);
            __StatusReadDelayNumeric = new Core.Controls.ToolStripNumericUpDown();
            __StatusReadDelayNumeric.ValueChanged += new EventHandler<EventArgs>(StatusReadDelayNumeric_ValueChanged);
            _ToolStrip = new ToolStrip();
            _MoreOptionsSplitButton = new ToolStripSplitButton();
            __EraseDisplayMenuItem = new Core.Controls.ToolStripMenuItem();
            __EraseDisplayMenuItem.Click += new EventHandler(EraseDisplayMenuItem_Click);
            __ClearSessionMenuItem = new Core.Controls.ToolStripMenuItem();
            __ClearSessionMenuItem.Click += new EventHandler(ClearSessionMenuItem_Click);
            __ReadStatusMenuItem = new Core.Controls.ToolStripMenuItem();
            __ReadStatusMenuItem.Click += new EventHandler(ReadStatusMenuItem_Click);
            _ShowPollReadingsMenuItem = new Core.Controls.ToolStripMenuItem();
            _ShowServiceRequestReadingMenuItem = new ToolStripMenuItem();
            _AppendTerminationMenuItem = new ToolStripMenuItem();
            _ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _WriteComboBox
            // 
            _WriteComboBox.Dock = DockStyle.Top;
            _WriteComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _WriteComboBox.FormattingEnabled = true;
            _WriteComboBox.Location = new System.Drawing.Point(1, 1);
            _WriteComboBox.Margin = new Padding(3, 4, 3, 4);
            _WriteComboBox.Name = "_WriteComboBox";
            _WriteComboBox.Size = new System.Drawing.Size(388, 25);
            _WriteComboBox.TabIndex = 53;
            // 
            // _ReadTextBox
            // 
            _ReadTextBox.Dock = DockStyle.Fill;
            _ReadTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ReadTextBox.Location = new System.Drawing.Point(1, 54);
            _ReadTextBox.Margin = new Padding(3, 5, 3, 5);
            _ReadTextBox.Multiline = true;
            _ReadTextBox.Name = "_ReadTextBox";
            _ReadTextBox.ReadOnly = true;
            _ReadTextBox.ScrollBars = ScrollBars.Vertical;
            _ReadTextBox.Size = new System.Drawing.Size(388, 273);
            _ReadTextBox.TabIndex = 59;
            _ReadTextBox.TabStop = false;
            // 
            // _QueryButton
            // 
            __QueryButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __QueryButton.Image = (System.Drawing.Image)resources.GetObject("_QueryButton.Image");
            __QueryButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __QueryButton.Margin = new Padding(0, 1, 1, 2);
            __QueryButton.Name = "__QueryButton";
            __QueryButton.Size = new System.Drawing.Size(47, 25);
            __QueryButton.Text = "&Query";
            __QueryButton.ToolTipText = "Write (send) the 'Message to Send' to the instrument, read the reply and display " + "in the 'Received data' text box";
            // 
            // _WriteButton
            // 
            __WriteButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __WriteButton.Image = (System.Drawing.Image)resources.GetObject("_WriteButton.Image");
            __WriteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __WriteButton.Margin = new Padding(0, 1, 2, 2);
            __WriteButton.Name = "__WriteButton";
            __WriteButton.Size = new System.Drawing.Size(43, 25);
            __WriteButton.Text = "&Write";
            __WriteButton.ToolTipText = "Write (send) the 'Message to Send' to the instrument";
            // 
            // _ReadButton
            // 
            __ReadButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __ReadButton.Image = (System.Drawing.Image)resources.GetObject("_ReadButton.Image");
            __ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadButton.Margin = new Padding(0, 1, 2, 2);
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(42, 25);
            __ReadButton.Text = "&Read";
            __ReadButton.ToolTipText = "Read a message from the instrument and display in the 'Received data' text box";
            // 
            // _ReadDelayNumeric
            // 
            __ReadDelayNumeric.Name = "__ReadDelayNumeric";
            __ReadDelayNumeric.Size = new System.Drawing.Size(45, 25);
            __ReadDelayNumeric.Text = "10";
            __ReadDelayNumeric.ToolTipText = "Delay before reading in milliseconds";
            __ReadDelayNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            // 
            // _StatusReadDelayNumeric
            // 
            __StatusReadDelayNumeric.Name = "__StatusReadDelayNumeric";
            __StatusReadDelayNumeric.Size = new System.Drawing.Size(45, 25);
            __StatusReadDelayNumeric.Text = "10";
            __StatusReadDelayNumeric.ToolTipText = "Delay time before reading status in milliseconds";
            __StatusReadDelayNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            // 
            // _ToolStrip
            // 
            _ToolStrip.BackColor = System.Drawing.Color.Transparent;
            _ToolStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ToolStrip.GripMargin = new Padding(0);
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { __QueryButton, __WriteButton, __ReadButton, __ReadDelayNumeric, __StatusReadDelayNumeric, _MoreOptionsSplitButton });
            _ToolStrip.Location = new System.Drawing.Point(1, 26);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Size = new System.Drawing.Size(388, 28);
            _ToolStrip.TabIndex = 65;
            _ToolStrip.Text = "ToolStrip1";
            // 
            // _MoreOptionsSplitButton
            // 
            _MoreOptionsSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _MoreOptionsSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __EraseDisplayMenuItem, __ClearSessionMenuItem, __ReadStatusMenuItem, _ShowPollReadingsMenuItem, _ShowServiceRequestReadingMenuItem, _AppendTerminationMenuItem });
            _MoreOptionsSplitButton.Image = (System.Drawing.Image)resources.GetObject("_MoreOptionsSplitButton.Image");
            _MoreOptionsSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _MoreOptionsSplitButton.Name = "_MoreOptionsSplitButton";
            _MoreOptionsSplitButton.Size = new System.Drawing.Size(79, 25);
            _MoreOptionsSplitButton.Text = "&Options...";
            _MoreOptionsSplitButton.ToolTipText = "Select from additional options";
            // 
            // _EraseDisplayMenuItem
            // 
            __EraseDisplayMenuItem.Name = "__EraseDisplayMenuItem";
            __EraseDisplayMenuItem.Size = new System.Drawing.Size(194, 22);
            __EraseDisplayMenuItem.Text = "&Erase Display";
            __EraseDisplayMenuItem.ToolTipText = "Clears the display";
            // 
            // _ClearSessionMenuItem
            // 
            __ClearSessionMenuItem.Name = "__ClearSessionMenuItem";
            __ClearSessionMenuItem.Size = new System.Drawing.Size(194, 22);
            __ClearSessionMenuItem.Text = "&Clear Session";
            __ClearSessionMenuItem.ToolTipText = "Clears the session (*CLS)";
            // 
            // _ReadStatusMenuItem
            // 
            __ReadStatusMenuItem.Name = "__ReadStatusMenuItem";
            __ReadStatusMenuItem.Size = new System.Drawing.Size(194, 22);
            __ReadStatusMenuItem.Text = "&Read Status Byte";
            __ReadStatusMenuItem.ToolTipText = "Reads the status byte";
            // 
            // _ShowPollReadingsMenuItem
            // 
            _ShowPollReadingsMenuItem.CheckOnClick = true;
            _ShowPollReadingsMenuItem.Name = "_ShowPollReadingsMenuItem";
            _ShowPollReadingsMenuItem.Size = new System.Drawing.Size(194, 22);
            _ShowPollReadingsMenuItem.Text = "Show &Poll Readings";
            _ShowPollReadingsMenuItem.ToolTipText = "Displays polled reading when session polling is enabled";
            // 
            // _ShowServiceRequestReadingMenuItem
            // 
            _ShowServiceRequestReadingMenuItem.CheckOnClick = true;
            _ShowServiceRequestReadingMenuItem.Name = "_ShowServiceRequestReadingMenuItem";
            _ShowServiceRequestReadingMenuItem.Size = new System.Drawing.Size(194, 22);
            _ShowServiceRequestReadingMenuItem.Text = "Show SR&Q Reading";
            _ShowServiceRequestReadingMenuItem.ToolTipText = "Shows service request reading when service request auto read is enabled";
            // 
            // _AppendTerminationMenuItem
            // 
            _AppendTerminationMenuItem.CheckOnClick = true;
            _AppendTerminationMenuItem.Name = "_AppendTerminationMenuItem";
            _AppendTerminationMenuItem.Size = new System.Drawing.Size(194, 22);
            _AppendTerminationMenuItem.Text = "&Append Termination";
            _AppendTerminationMenuItem.ToolTipText = "Appends termination to instrument commands thus not having the add the terminatio" + @"n character (\n)";
            // 
            // SessionView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_ReadTextBox);
            Controls.Add(_ToolStrip);
            Controls.Add(_WriteComboBox);
            Name = "SessionView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(390, 328);
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ComboBox _WriteComboBox;
        private TextBox _ReadTextBox;
        private Core.Controls.ToolStripButton __QueryButton;

        private Core.Controls.ToolStripButton _QueryButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __QueryButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__QueryButton != null)
                {
                    __QueryButton.Click -= QueryButton_Click;
                }

                __QueryButton = value;
                if (__QueryButton != null)
                {
                    __QueryButton.Click += QueryButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripButton __WriteButton;

        private Core.Controls.ToolStripButton _WriteButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WriteButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WriteButton != null)
                {
                    __WriteButton.Click -= WriteButton_Click;
                }

                __WriteButton = value;
                if (__WriteButton != null)
                {
                    __WriteButton.Click += WriteButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripButton __ReadButton;

        private Core.Controls.ToolStripButton _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown __ReadDelayNumeric;

        private Core.Controls.ToolStripNumericUpDown _ReadDelayNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadDelayNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadDelayNumeric != null)
                {
                    __ReadDelayNumeric.ValueChanged -= ReadDelayNumeric_ValueChanged;
                }

                __ReadDelayNumeric = value;
                if (__ReadDelayNumeric != null)
                {
                    __ReadDelayNumeric.ValueChanged += ReadDelayNumeric_ValueChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown __StatusReadDelayNumeric;

        private Core.Controls.ToolStripNumericUpDown _StatusReadDelayNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StatusReadDelayNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StatusReadDelayNumeric != null)
                {
                    __StatusReadDelayNumeric.ValueChanged -= StatusReadDelayNumeric_ValueChanged;
                }

                __StatusReadDelayNumeric = value;
                if (__StatusReadDelayNumeric != null)
                {
                    __StatusReadDelayNumeric.ValueChanged += StatusReadDelayNumeric_ValueChanged;
                }
            }
        }

        private ToolStrip _ToolStrip;
        private ToolStripSplitButton _MoreOptionsSplitButton;
        private Core.Controls.ToolStripMenuItem __EraseDisplayMenuItem;

        private Core.Controls.ToolStripMenuItem _EraseDisplayMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EraseDisplayMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EraseDisplayMenuItem != null)
                {
                    __EraseDisplayMenuItem.Click -= EraseDisplayMenuItem_Click;
                }

                __EraseDisplayMenuItem = value;
                if (__EraseDisplayMenuItem != null)
                {
                    __EraseDisplayMenuItem.Click += EraseDisplayMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripMenuItem __ClearSessionMenuItem;

        private Core.Controls.ToolStripMenuItem _ClearSessionMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearSessionMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearSessionMenuItem != null)
                {
                    __ClearSessionMenuItem.Click -= ClearSessionMenuItem_Click;
                }

                __ClearSessionMenuItem = value;
                if (__ClearSessionMenuItem != null)
                {
                    __ClearSessionMenuItem.Click += ClearSessionMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripMenuItem __ReadStatusMenuItem;

        private Core.Controls.ToolStripMenuItem _ReadStatusMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadStatusMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadStatusMenuItem != null)
                {
                    __ReadStatusMenuItem.Click -= ReadStatusMenuItem_Click;
                }

                __ReadStatusMenuItem = value;
                if (__ReadStatusMenuItem != null)
                {
                    __ReadStatusMenuItem.Click += ReadStatusMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripMenuItem _ShowPollReadingsMenuItem;
        private ToolStripMenuItem _ShowServiceRequestReadingMenuItem;
        private ToolStripMenuItem _AppendTerminationMenuItem;
    }
}
