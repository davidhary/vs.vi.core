using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core;
using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A split view for VISA Sessions </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para></remarks>
    [DisplayName( "Split Visa View" )]
    [Description( "Split View for VISA Sessions" )]
    [System.Drawing.ToolboxBitmap( typeof( SplitVisaView ) )]
    public partial class SplitVisaView : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public SplitVisaView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this._Layout.Dock = DockStyle.Fill;
            this._TreePanel.Dock = DockStyle.Fill;
            this._TreePanel.SplitterDistance = 120;
            this._TreePanel.ClearNodes();
            _ = this._TreePanel.AddNode( _MessagesNodeName, _MessagesNodeName, this.TraceMessagesBox );

            // note that the caption is not set if this is run inside the On Load function.
            // set defaults for the messages box.
            this.TraceMessagesBox.ResetCount = 500;
            this.TraceMessagesBox.PresetCount = 250;
            this.TraceMessagesBox.ContainerTreeNode = this._TreePanel.GetNode( _MessagesNodeName );
            this.TraceMessagesBox.TabCaption = _MessagesNodeName;
            this.TraceMessagesBox.AlertsToggleTreeNode = this._TreePanel.GetNode( _MessagesNodeName );
            // Me._TraceMessagesBox.ContainerPanel = Me._TreePanel.Panel2
            // Me._TraceMessagesBox.AlertsToggleControl = Me._TreePanel.Panel2
            this.TraceMessagesBox.CommenceUpdates();
            this._TreePanel.Enabled = true;
            this.InitializingComponents = false;
            this.__TreePanel.Name = "_TreePanel";
        }

        /// <summary> Constructor. </summary>
        /// <param name="visaSessionBase"> The visa session. </param>
        public SplitVisaView( VisaSessionBase visaSessionBase ) : this()
        {
            // assigns the visa session.
            this.BindVisaSessionBaseThis( visaSessionBase );
        }

        /// <summary> Creates a new <see cref="SplitVisaView"/> </summary>
        /// <returns> A <see cref="SplitVisaView"/>. </returns>
        public static SplitVisaView Create()
        {
            SplitVisaView view = null;
            try
            {
                view = new SplitVisaView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    this.TraceMessagesBox.SuspendUpdatesReleaseIndicators();
                    // make sure the session is unbound in case the form is closed without closing the session.
                    this.BindVisaSessionBase( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EVENT HANDLERS:  FORM "

        /// <summary>
        /// Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
        /// </summary>
        /// <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
        /// data. </param>
        protected override void OnFormClosing( CancelEventArgs e )
        {
            base.OnFormClosing( e );
            try
            {
                if ( e is object && !e.Cancel )
                {
                    this.RemovePrivateListener( this.TraceMessagesBox );
                }
            }
            finally
            {
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                activity = $"Loading the driver console form";
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );
                if ( !this.DesignMode )
                {
                    // add listeners before the first talker publish command
                    this.AddPrivateListeners();
                }

                _ = this.PublishVerbose( $"{activity};. " );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
                if ( MyDialogResult.Abort == WindowsForms.ShowDialogAbortIgnore( ex ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnLoad( e );
                this.Cursor = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
                ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " VISA SESSION BASE (DEVICE BASE) "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Bind visa session base. </summary>
        /// <param name="visaSessionBase"> True to show or False to hide the control. </param>
        private void BindVisaSessionBaseThis( VisaSessionBase visaSessionBase )
        {
            if ( this.VisaSessionBase is object )
            {
                this.VisaSessionBase.Opening -= this.DeviceOpening;
                this.VisaSessionBase.Opened -= this.DeviceOpened;
                this.VisaSessionBase.Closing -= this.DeviceClosing;
                this.VisaSessionBase.Closed -= this.DeviceClosed;
                this.VisaSessionBase.Initialized -= this.DeviceInitialized;
                this.VisaSessionBase.Initializing -= this.DeviceInitializing;
                this.VisaSessionBase.SessionFactory.PropertyChanged -= this.SessionFactoryPropertyChanged;
                this.AssignTalker( null );
                this.VisaSessionBase = null;
            }

            this.VisaSessionBase = visaSessionBase;
            if ( visaSessionBase is object )
            {
                this.AssignTalker( this.VisaSessionBase.Talker );
                this.VisaSessionBase.Opening += this.DeviceOpening;
                this.VisaSessionBase.Opened += this.DeviceOpened;
                this.VisaSessionBase.Closing += this.DeviceClosing;
                this.VisaSessionBase.Closed += this.DeviceClosed;
                this.VisaSessionBase.Initialized += this.DeviceInitialized;
                this.VisaSessionBase.Initializing += this.DeviceInitializing;
                this.VisaSessionBase.SessionFactory.PropertyChanged += this.SessionFactoryPropertyChanged;
                // Me.VisaSessionBase.SessionFactory.CandidateResourceName = isr.VI.Ttm.My.Settings.ResourceName
                if ( this.VisaSessionBase.IsSessionOpen )
                {
                    this.DeviceOpened( this.VisaSessionBase, EventArgs.Empty );
                }
                else
                {
                    this.DeviceClosed( this.VisaSessionBase, EventArgs.Empty );
                }

                this.HandlePropertyChanged( this.VisaSessionBase.SessionFactory, nameof( this.VisaSessionBase.SessionFactory.IsOpen ) );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <param name="visaSessionBase"> True to show or False to hide the control. </param>
        public virtual void BindVisaSessionBase( VisaSessionBase visaSessionBase )
        {
            this.BindVisaSessionBaseThis( visaSessionBase );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.VisaSessionBase.ResourceNameCaption} reading service request";
            try
            {
                _ = this.VisaSessionBase.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #region " SESSION FACTORY "

        /// <summary> Name of the resource. </summary>
        private string _ResourceName;

        /// <summary> Gets or sets the name of the resource. </summary>
        /// <value> The name of the resource. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string ResourceName
        {
            get => this._ResourceName;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourceName ) )
                {
                    this._ResourceName = value;
                    this.NotifyPropertyChanged();
                }

                if ( this.VisaSessionBase is object )
                {
                    this.VisaSessionBase.CandidateResourceName = value;
                }
            }
        }

        /// <summary> A filter specifying the resource. </summary>
        private string _ResourceFilter;

        /// <summary> Gets or sets the Search Pattern of the resource. </summary>
        /// <value> The Search Pattern of the resource. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string ResourceFilter
        {
            get => this._ResourceFilter;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourceFilter ) )
                {
                    this._ResourceFilter = value;
                    this.NotifyPropertyChanged();
                }

                if ( this.VisaSessionBase is object )
                {
                    this.VisaSessionBase.ResourcesFilter = value;
                }
            }
        }

        /// <summary> Executes the session factory property changed action. </summary>
        /// <param name="sender">       Specifies the object where the call originated. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SessionFactory sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SessionFactory.ValidatedResourceName ):
                    {
                        _ = sender.IsOpen
                            ? this.PublishInfo( $"Resource connected;. {sender.ValidatedResourceName}" )
                            : this.PublishInfo( $"Resource locate;. {sender.ValidatedResourceName}" );

                        this.ResourceName = sender.ValidatedResourceName;
                        break;
                    }

                case nameof( SessionFactory.CandidateResourceName ):
                    {
                        if ( !sender.IsOpen )
                        {
                            _ = this.PublishInfo( $"Candidate resource;. {sender.ValidatedResourceName}" );
                        }

                        this.ResourceName = sender.CandidateResourceName;
                        break;
                    }

                case nameof( SessionFactory.ResourcesFilter ):
                    {
                        break;
                    }
            }
        }

        /// <summary> Session factory property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionFactoryPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling session factory {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SessionFactoryPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SessionFactory, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE EVENTS "

        /// <summary>
        /// Event handler. Called upon device opening so as to instantiated all subsystems.
        /// </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected virtual void DeviceOpening( object sender, CancelEventArgs e )
        {
        }

        /// <summary>
        /// Event handler. Called after the device opened and all subsystems were defined.
        /// </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceOpened( object sender, EventArgs e )
        {
            var outcome = TraceEventType.Information;
            if ( this.VisaSessionBase.Session.Enabled & !this.VisaSessionBase.Session.IsSessionOpen )
                outcome = TraceEventType.Warning;
            _ = this.Publish( outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ", this.VisaSessionBase.ResourceTitleCaption, this.VisaSessionBase.Session.Enabled.GetHashCode(), this.VisaSessionBase.Session.IsDeviceOpen.GetHashCode(), this.VisaSessionBase.Session.IsSessionOpen.GetHashCode() );
            string activity = string.Empty;
            try
            {
                activity = $"Opened {this.ResourceName}";
                _ = this.PublishVerbose( $"{activity};. " );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " OPENING / OPEN "

        /// <summary>
        /// Attempts to open a session to the device using the specified resource name.
        /// </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The title. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryOpenDeviceSession( string resourceName, string resourceTitle )
        {
            string activity = $"opening {resourceName} VISA session";
            try
            {
                var (success, details) = this.VisaSessionBase.TryOpenSession( resourceName, resourceTitle );
                return success ? (success, details) : (success, $"failed {activity};. {details}");
            }
            catch ( Exception ex )
            {
                return (false, $"Exception {activity};. {ex.ToFullBlownString()}");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region " INITALIZING / INITIALIZED  "

        /// <summary> Device initializing. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        protected virtual void DeviceInitializing( object sender, CancelEventArgs e )
        {
        }

        /// <summary> Device initialized. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected virtual void DeviceInitialized( object sender, EventArgs e )
        {
        }

        #endregion

        #region " CLOSING / CLOSED "

        /// <summary> Event handler. Called when device is closing. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceClosing( object sender, CancelEventArgs e )
        {
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                    activity = $"Disconnecting from {this.ResourceName}";
                    _ = this.PublishInfo( $"{activity};. " );
                    if ( this.VisaSessionBase.Session is object )
                        this.VisaSessionBase.Session.DisableServiceRequestEventHandler();
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
        }

        /// <summary> Event handler. Called when device is closed. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceClosed( object sender, EventArgs e )
        {
            string activity = string.Empty;
            if ( this.VisaSessionBase is object )
            {
                try
                {
                    if ( this.VisaSessionBase.Session.IsSessionOpen )
                    {
                        activity = $"{this.VisaSessionBase.Session.ResourceNameCaption} closed but session is still open";
                        _ = this.PublishWarning( $"{activity};. " );
                    }
                    else if ( this.VisaSessionBase.Session.IsDeviceOpen )
                    {
                        activity = $"{this.VisaSessionBase.Session.ResourceNameCaption} closed but emulated session is still open";
                        _ = this.PublishWarning( $"{activity};. " );
                    }
                    else
                    {
                        activity = $"Disconnected from {this.ResourceName}";
                        _ = this.PublishVerbose( $"{activity};. " );
                    }
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
            else
            {
                activity = $"Already disconnected; device disposed";
                _ = this.PublishInfo( $"{activity};. " );
            }
        }

        #endregion

        #endregion

        #region " LAYOUT "

        /// <summary> Refresh layout. </summary>
        private void RefreshLayout()
        {
            this._Layout.RowStyles.Clear();
            _ = this._Layout.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
            _ = this._Layout.RowStyles.Add( new RowStyle( SizeType.Percent, 100f ) );
            _ = this._Layout.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
            if ( this.HeaderControl is object )
            {
                this._Layout.SetRow( this.HeaderControl, 0 );
            }

            if ( this._TreePanel is object )
                this._Layout.SetRow( this._TreePanel, 1 );
            if ( this.FooterControl is object )
                this._Layout.SetRow( this.FooterControl, 0 );
            this._Layout.Refresh();
        }

        /// <summary> Gets the header control. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The header control. </value>
        private Control HeaderControl { get; set; }

        /// <summary> Adds a header. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="headerControl"> The control. </param>
        public void AddHeader( Control headerControl )
        {
            if ( headerControl is null )
                throw new ArgumentNullException( nameof( headerControl ) );
            this.HeaderControl = headerControl;
            this._Layout.Controls.Add( headerControl, 1, 0 );
            headerControl.Dock = DockStyle.Top;
            this.RefreshLayout();
        }

        /// <summary> Gets the footer control. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The footer control. </value>
        private Control FooterControl { get; set; }

        /// <summary> Adds a footer. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="footerControl"> The footer control. </param>
        public void AddFooter( Control footerControl )
        {
            if ( footerControl is null )
                throw new ArgumentNullException( nameof( footerControl ) );
            this.FooterControl = footerControl;
            this._Layout.Controls.Add( footerControl, 1, 2 );
            footerControl.Dock = DockStyle.Bottom;
            this.RefreshLayout();
        }

        /// <summary> Name of the messages node. </summary>
        private const string _MessagesNodeName = "Messages";

        /// <summary> Selects the navigator tree view node. </summary>
        /// <param name="nodeName"> The node. </param>
        public void SelectNavigatorTreeViewNode( string nodeName )
        {
            this._TreePanel.SelectNavigatorTreeViewNode( nodeName );
        }

        /// <summary> Adds a node. </summary>
        /// <remarks> David, 2020-09-03. </remarks>
        /// <param name="nodeName">    The node. </param>
        /// <param name="nodeCaption"> The node caption. </param>
        /// <param name="nodeControl"> The node control. </param>
        /// <returns> A TreeNode. </returns>
        public TreeNode AddNode( string nodeName, string nodeCaption, Control nodeControl )
        {
            return this._TreePanel.AddNode( nodeName, nodeCaption, nodeControl );
        }

        /// <summary> Inserts a node. </summary>
        /// <remarks> David, 2020-09-03. </remarks>
        /// <param name="index">       Zero-based index of the. </param>
        /// <param name="nodeName">    The node. </param>
        /// <param name="nodeCaption"> The node caption. </param>
        /// <param name="nodeControl"> The node control. </param>
        /// <returns> A TreeNode. </returns>
        public TreeNode InsertNode( int index, string nodeName, string nodeCaption, Control nodeControl )
        {
            return this._TreePanel.InsertNode( index, nodeName, nodeCaption, nodeControl );
        }

        /// <summary> Removes the node described by nodeName. </summary>
        /// <remarks> David, 2020-09-03. </remarks>
        /// <param name="nodeName"> The node. </param>
        public void RemoveNode( string nodeName )
        {
            this._TreePanel.RemoveNode( nodeName );
        }

        /// <summary> After node selected. </summary>
        /// <remarks> David, 2020-09-03. </remarks>
        /// <param name="e"> Tree view event information. </param>
        protected virtual void AfterNodeSelected( TreeViewEventArgs e )
        {
        }

        /// <summary> Tree panel after node selected. </summary>
        /// <remarks> David, 2020-09-03. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Tree view event information. </param>
        private void TreePanel_AfterNodeSelected( object sender, TreeViewEventArgs e )
        {
            this.AfterNodeSelected( e );
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Adds the listeners such as the current trace messages box. </summary>
        protected void AddPrivateListeners()
        {
            this.AddPrivateListener( this.TraceMessagesBox );
        }

        /// <summary> Assign talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( ITraceMessageTalker talker )
        {
            base.AssignTalker( talker );
            this.AddListener( this.TraceMessagesBox );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        /// <summary> Executes the trace messages box property changed action. </summary>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TraceMessagesBox sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            if ( string.Equals( propertyName, nameof( Core.Forma.TraceMessagesBox.StatusPrompt ) ) )
            {
                this.StatusLabel.Text = Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact( sender.StatusPrompt, this.StatusLabel );
                this.StatusLabel.ToolTipText = sender.StatusPrompt;
            }
        }

        /// <summary> Trace messages box property changed. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TraceMessagesBox_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceName} handling trace message box {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TraceMessagesBox_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TraceMessagesBox, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " PROTECTED CONTROLS "

        /// <summary> Gets the trace messages box. </summary>
        /// <value> The trace messages box. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected TraceMessagesBox TraceMessagesBox { get; private set; }

        /// <summary> Gets the status strip. </summary>
        /// <value> The status strip. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected StatusStrip StatusStrip { get; private set; }

        /// <summary> Gets the status label. </summary>
        /// <value> The status label. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected ToolStripStatusLabel StatusLabel { get; private set; }

        /// <summary> Gets the progress bar. </summary>
        /// <value> The progress bar. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected Core.Controls.StatusStripCustomProgressBar ProgressBar { get; private set; }

        /// <summary> Gets or sets the progress percent. </summary>
        /// <value> The progress percent. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int ProgressPercent
        {
            get => this.ProgressBar.Value;

            set {
                if ( value != this.ProgressPercent )
                {
                    this.ProgressBar.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
