using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> ASession Status view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SessionStatusView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public SessionStatusView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary> Creates a new <see cref="SessionStatusView"/> </summary>
        /// <returns> A <see cref="SessionStatusView"/>. </returns>
        public static SessionStatusView Create()
        {
            SessionStatusView view = null;
            try
            {
                view = new SessionStatusView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    // TO_DO: Doe we need this? Me.BindVisaSessionBase(Nothing)
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " visa session base (device base) "

        /// <summary> Gets the visa session base. </summary>
        /// <value> The visa session base. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Binds the visa session to its controls. </summary>
        /// <param name="visaSessionBase"> The visa session view model. </param>
        public void BindVisaSessionBase( VisaSessionBase visaSessionBase )
        {
            if ( this.VisaSessionBase is object )
            {
                this._SelectorOpener.AssignSelectorViewModel( null );
                this._SelectorOpener.AssignOpenerViewModel( null );
                this.VisaSessionBase = null;
            }

            if ( visaSessionBase is object )
            {
                this.VisaSessionBase = visaSessionBase;
                this._SelectorOpener.AssignSelectorViewModel( visaSessionBase.SessionFactory );
                this._SelectorOpener.AssignOpenerViewModel( visaSessionBase );
            }

            this.StatusView.BindVisaSessionBase( visaSessionBase );
            this.SessionView.BindVisaSessionBase( visaSessionBase );
        }

        #endregion

        #region " EXPOSED VIEWS "

        /// <summary> Gets the status view. </summary>
        /// <value> The status view. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public StatusView StatusView { get; private set; }

        /// <summary> Gets the session view. </summary>
        /// <value> The session view. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SessionView SessionView { get; private set; }


        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
