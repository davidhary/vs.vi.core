using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A visa session view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-04-24 </para>
    /// </remarks>
    public class SplitVisaSessionView : SplitVisaView
    {

        #region " CONSTRUCTION AND CLEANUP "

        /// <summary> Default constructor. </summary>
        public SplitVisaSessionView() : base()
        {
            this.AssignTopHeader( new DisplayView() );
            this.AssignVisaSessionStatusView( new SessionStatusView() );
        }

        /// <summary> Creates a new <see cref="SplitVisaSessionView"/> </summary>
        /// <returns> A <see cref="SplitVisaSessionView"/>. </returns>
        public static new SplitVisaSessionView Create()
        {
            SplitVisaSessionView view = null;
            try
            {
                view = new SplitVisaSessionView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        #endregion

        #region " VISA SESSION BASE (DEVICE BASE) "

        /// <summary> Binds the visa session base (device base) to its controls. </summary>
        /// <param name="visaSessionBase"> The visa session base (device base) view model. </param>
        public override void BindVisaSessionBase( VisaSessionBase visaSessionBase )
        {
            base.BindVisaSessionBase( visaSessionBase );
            this.TopHeader.BindVisaSessionBase( visaSessionBase );
            this.SessionStatusView.BindVisaSessionBase( visaSessionBase );
            this.SessionStatusView.StatusView.SessionSettings = Pith.My.MySettings.Default;
            this.SessionStatusView.StatusView.DeviceSettings = null;
            this.SessionStatusView.StatusView.UserInterfaceSettings = My.Settings.Default;
        }

        #endregion

        #region " DEVICE EVENTS "

        /// <summary>
        /// Event handler. Called upon device opening so as to instantiated all subsystems.
        /// </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected override void DeviceOpening( object sender, CancelEventArgs e )
        {
            base.DeviceOpening( sender, e );
            _ = this.PublishVerbose( $"Opening access to {this.ResourceName};. " );
        }

        /// <summary>
        /// Event handler. Called after the device opened and all subsystems were defined.
        /// </summary>
        /// <param name="sender"> <see cref="T:System.Object" /> instance of this
        /// <see cref="T:System.Windows.Forms.Control" /> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void DeviceOpened( object sender, EventArgs e )
        {
            base.DeviceOpened( sender, e );
            string activity = string.Empty;
            try
            {
                activity = "displaying top header resource name";
                _ = this.PublishVerbose( $"{activity};. " );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Device initializing. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Cancel event information. </param>
        protected override void DeviceInitializing( object sender, CancelEventArgs e )
        {
            base.DeviceInitializing( sender, e );
        }

        /// <summary> Device initialized. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected override void DeviceInitialized( object sender, EventArgs e )
        {
            base.DeviceInitialized( sender, e );
        }

        /// <summary> Event handler. Called when device is closing. </summary>
        /// <param name="sender"> <see cref="T:System.Object" /> instance of this
        /// <see cref="T:System.Windows.Forms.Control" />
        /// </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void DeviceClosing( object sender, CancelEventArgs e )
        {
            base.DeviceClosing( sender, e );
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
        }

        /// <summary> Event handler. Called when device is closed. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void DeviceClosed( object sender, EventArgs e )
        {
            base.DeviceClosed( sender, e );
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
        }

        #endregion

        #region " TOP HEADER "

        /// <summary> Gets or sets the top header. </summary>
        /// <value> The top header. </value>
        private DisplayView TopHeader { get; set; }

        /// <summary> Assign top header. </summary>
        /// <param name="topHeader"> The top header. </param>
        private void AssignTopHeader( DisplayView topHeader )
        {
            this.TopHeader = topHeader;
            this.AddHeader( topHeader );
        }

        #endregion

        #region " VISA SESSION STATUS CONNECTOR VIEW "

        /// <summary> Gets or sets the session status view. </summary>
        /// <value> The session status view. </value>
        private SessionStatusView SessionStatusView { get; set; }

        /// <summary> Gets or sets the name of the visa session status node. </summary>
        /// <value> The name of the visa session status node. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string VisaSessionStatusNodeName { get; private set; } = "Session";

        /// <summary> Assign visa session status view. </summary>
        /// <param name="sessionStatusView"> The session status view. </param>
        private void AssignVisaSessionStatusView( SessionStatusView sessionStatusView )
        {
            this.SessionStatusView = sessionStatusView;
            _ = this.AddNode( this.VisaSessionStatusNodeName, this.VisaSessionStatusNodeName, this.TopHeader );
        }

        #endregion

        #region " SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "Device Facade Settings Editor", My.Settings.Default );
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
