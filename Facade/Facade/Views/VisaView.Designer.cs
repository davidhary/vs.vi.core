﻿using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class VisaView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __Tabs = new TabControl();
            __Tabs.DrawItem += new DrawItemEventHandler(Tabs_DrawItem);
            _SessionTabPage = new TabPage();
            _SessionView = new SessionView();
            StatusView = new StatusView();
            _SelectorOpener = new Core.Controls.SelectorOpener();
            _MessagesTabPage = new TabPage();
            _TraceMessagesBox = new Core.Forma.TraceMessagesBox();
            _StatusStrip = new StatusStrip();
            _StatusPromptLabel = new Core.Controls.ToolStripStatusLabel();
            _Panel = new Panel();
            DisplayView = new DisplayView();
            _Layout = new TableLayoutPanel();
            __Tabs.SuspendLayout();
            _SessionTabPage.SuspendLayout();
            _MessagesTabPage.SuspendLayout();
            _StatusStrip.SuspendLayout();
            _Panel.SuspendLayout();
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _Tabs
            // 
            __Tabs.Controls.Add(_SessionTabPage);
            __Tabs.Controls.Add(_MessagesTabPage);
            __Tabs.Dock = DockStyle.Fill;
            __Tabs.ItemSize = new System.Drawing.Size(52, 22);
            __Tabs.Location = new System.Drawing.Point(0, 143);
            __Tabs.Name = "__Tabs";
            __Tabs.SelectedIndex = 0;
            __Tabs.Size = new System.Drawing.Size(364, 285);
            __Tabs.TabIndex = 5;
            // 
            // _SessionTabPage
            // 
            _SessionTabPage.Controls.Add(_SessionView);
            _SessionTabPage.Controls.Add(StatusView);
            _SessionTabPage.Controls.Add(_SelectorOpener);
            _SessionTabPage.Location = new System.Drawing.Point(4, 26);
            _SessionTabPage.Name = "_SessionTabPage";
            _SessionTabPage.Size = new System.Drawing.Size(356, 255);
            _SessionTabPage.TabIndex = 0;
            _SessionTabPage.Text = "Session";
            _SessionTabPage.UseVisualStyleBackColor = true;
            // 
            // _SessionView
            // 
            _SessionView.Dock = DockStyle.Fill;
            _SessionView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SessionView.Location = new System.Drawing.Point(0, 0);
            _SessionView.Margin = new Padding(3, 4, 3, 4);
            _SessionView.Name = "_SessionView";
            _SessionView.Size = new System.Drawing.Size(356, 195);
            _SessionView.TabIndex = 25;
            // 
            // _StatusView
            // 
            StatusView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            StatusView.BackColor = System.Drawing.Color.Transparent;
            StatusView.Dock = DockStyle.Bottom;
            StatusView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            StatusView.Location = new System.Drawing.Point(0, 195);
            StatusView.Margin = new Padding(0);
            StatusView.Name = "_StatusView";
            StatusView.Size = new System.Drawing.Size(356, 31);
            StatusView.TabIndex = 27;
            // 
            // _SelectorOpener
            // 
            _SelectorOpener.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _SelectorOpener.BackColor = System.Drawing.Color.Transparent;
            _SelectorOpener.Dock = DockStyle.Bottom;
            _SelectorOpener.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SelectorOpener.Location = new System.Drawing.Point(0, 226);
            _SelectorOpener.Margin = new Padding(0);
            _SelectorOpener.Name = "_SelectorOpener";
            _SelectorOpener.SelectedValueChangeCount = 0;
            _SelectorOpener.Size = new System.Drawing.Size(356, 29);
            _SelectorOpener.TabIndex = 26;
            // 
            // _MessagesTabPage
            // 
            _MessagesTabPage.Controls.Add(_TraceMessagesBox);
            _MessagesTabPage.Location = new System.Drawing.Point(4, 26);
            _MessagesTabPage.Name = "_MessagesTabPage";
            _MessagesTabPage.Size = new System.Drawing.Size(356, 273);
            _MessagesTabPage.TabIndex = 3;
            _MessagesTabPage.Text = "Log";
            _MessagesTabPage.UseVisualStyleBackColor = true;
            // 
            // _TraceMessagesBox
            // 
            _TraceMessagesBox.AlertLevel = TraceEventType.Warning;
            _TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info;
            _TraceMessagesBox.CaptionFormat = "{0} ≡";
            _TraceMessagesBox.CausesValidation = false;
            _TraceMessagesBox.Dock = DockStyle.Fill;
            _TraceMessagesBox.Font = new System.Drawing.Font("Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TraceMessagesBox.Location = new System.Drawing.Point(0, 0);
            _TraceMessagesBox.Multiline = true;
            _TraceMessagesBox.Name = "_TraceMessagesBox";
            _TraceMessagesBox.PresetCount = 500;
            _TraceMessagesBox.ReadOnly = true;
            _TraceMessagesBox.ResetCount = 1000;
            _TraceMessagesBox.ScrollBars = ScrollBars.Both;
            _TraceMessagesBox.Size = new System.Drawing.Size(356, 273);
            _TraceMessagesBox.TabIndex = 1;
            _TraceMessagesBox.TraceLevel = TraceEventType.Verbose;
            // 
            // _StatusStrip
            // 
            _StatusStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _StatusStrip.GripMargin = new Padding(0);
            _StatusStrip.Items.AddRange(new ToolStripItem[] { _StatusPromptLabel });
            _StatusStrip.Location = new System.Drawing.Point(0, 428);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Padding = new Padding(1, 0, 16, 0);
            _StatusStrip.ShowItemToolTips = true;
            _StatusStrip.Size = new System.Drawing.Size(364, 22);
            _StatusStrip.SizingGrip = false;
            _StatusStrip.TabIndex = 15;
            _StatusStrip.Text = "StatusStrip1";
            // 
            // _StatusPromptLabel
            // 
            _StatusPromptLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _StatusPromptLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _StatusPromptLabel.Name = "_StatusPromptLabel";
            _StatusPromptLabel.Overflow = ToolStripItemOverflow.Never;
            _StatusPromptLabel.Size = new System.Drawing.Size(347, 17);
            _StatusPromptLabel.Spring = true;
            _StatusPromptLabel.Text = "<Status>";
            _StatusPromptLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            _StatusPromptLabel.ToolTipText = "Status prompt";
            // 
            // _Panel
            // 
            _Panel.Controls.Add(__Tabs);
            _Panel.Controls.Add(DisplayView);
            _Panel.Controls.Add(_StatusStrip);
            _Panel.Dock = DockStyle.Fill;
            _Panel.Location = new System.Drawing.Point(0, 0);
            _Panel.Margin = new Padding(0);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(364, 450);
            _Panel.TabIndex = 16;
            // 
            // _DisplayView
            // 
            DisplayView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            DisplayView.Dock = DockStyle.Top;
            DisplayView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            DisplayView.Location = new System.Drawing.Point(0, 0);
            DisplayView.Margin = new Padding(3, 4, 3, 4);
            DisplayView.Name = "_DisplayView";
            DisplayView.Size = new System.Drawing.Size(364, 143);
            DisplayView.TabIndex = 16;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 1;
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.Controls.Add(_Panel, 0, 1);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Margin = new Padding(0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 2;
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.Size = new System.Drawing.Size(364, 450);
            _Layout.TabIndex = 17;
            // 
            // VisaView
            // 
            Controls.Add(_Layout);
            Name = "VisaView";
            Size = new System.Drawing.Size(364, 450);
            __Tabs.ResumeLayout(false);
            _SessionTabPage.ResumeLayout(false);
            _MessagesTabPage.ResumeLayout(false);
            _MessagesTabPage.PerformLayout();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            _Layout.ResumeLayout(false);
            ResumeLayout(false);
        }

        private TabPage _SessionTabPage;
        private TabPage _MessagesTabPage;
        private TabControl __Tabs;

        private TabControl _Tabs
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __Tabs;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__Tabs != null)
                {
                    __Tabs.DrawItem -= Tabs_DrawItem;
                }

                __Tabs = value;
                if (__Tabs != null)
                {
                    __Tabs.DrawItem += Tabs_DrawItem;
                }
            }
        }

        private Panel _Panel;
        private TableLayoutPanel _Layout;
        private Core.Forma.TraceMessagesBox _TraceMessagesBox;
        private StatusStrip _StatusStrip;
        private Core.Controls.ToolStripStatusLabel _StatusPromptLabel;
        private SessionView _SessionView;
        private Core.Controls.SelectorOpener _SelectorOpener;
    }
}