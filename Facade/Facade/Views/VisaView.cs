using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using isr.Core;
using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> Provides a user interface for a <see cref="VisaSession"/> </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2018-12-24, 6.0.6932.x"> Create based on the read and write control panel </para></remarks>
    [DisplayName( "VISA View" )]
    [Description( "User interface for VISA Sessions" )]
    [ToolboxBitmap( typeof( VisaView ) )]
    public partial class VisaView : ModelViewTalkerBase, IVisaView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public VisaView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();

            // note that the caption is not set if this is run inside the On Load function.
            // set defaults for the messages box.
            this._TraceMessagesBox.ResetCount = 500;
            this._TraceMessagesBox.PresetCount = 250;
            this._TraceMessagesBox.ContainerPanel = this._MessagesTabPage;
            this._TraceMessagesBox.AlertsToggleControl = this._MessagesTabPage;
            this._TraceMessagesBox.CommenceUpdates();
            this._Tabs.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.InitializingComponents = false;
            this.__Tabs.Name = "_Tabs";
        }

        /// <summary> Constructor. </summary>
        /// <param name="visaSessionBase"> The visa session base (device base) (device). </param>
        public VisaView( VisaSessionBase visaSessionBase ) : this()
        {
            // assigns the visa session.
            this.BindVisaSessionBaseThis( visaSessionBase );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.VI.Facade.VisaView and optionally releases
        /// the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    this._TraceMessagesBox.SuspendUpdatesReleaseIndicators();
                    // make sure the session is unbound in case the form is closed without closing the session.
                    this.BindVisaSessionBaseThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Tabs draw item. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Draw item event information. </param>
        private void Tabs_DrawItem( object sender, DrawItemEventArgs e )
        {
            var page = this._Tabs.TabPages[e.Index];
            var paddedBounds = e.Bounds;
            var backClr = e.State == DrawItemState.Selected ? SystemColors.ControlDark : page.BackColor;
            using ( Brush brush = new SolidBrush( backClr ) )
            {
                e.Graphics.FillRectangle( brush, paddedBounds );
            }

            int yOffset = e.State == DrawItemState.Selected ? -2 : 1;
            paddedBounds = e.Bounds;
            paddedBounds.Offset( 1, yOffset );
            TextRenderer.DrawText( e.Graphics, page.Text, page.Font, paddedBounds, page.ForeColor );
        }


        #endregion

        #region " FORM EVENTS "

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                this.AddPrivateListener( this._TraceMessagesBox );
                this._TraceMessagesBox.ContainerPanel = this._MessagesTabPage;
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        #endregion

        #region " VISA SESSION BASE (DEVICE BASE) "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
        /// <param name="visaSessionBase"> The assigned visa session base (device base) or nothing to
        /// release the session. </param>
        public virtual void BindVisaSessionBase( VisaSessionBase visaSessionBase )
        {
            this.BindVisaSessionBaseThis( visaSessionBase );
        }

        /// <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="visaSessionBase"> The assigned visa session or nothing to release the session. </param>
        private void BindVisaSessionBaseThis( VisaSessionBase visaSessionBase )
        {
            if ( this.VisaSessionBase is object )
            {
                if ( this.VisaSessionBase.Talker is object )
                    this.VisaSessionBase.Talker.RemoveListeners();
                this.VisaSessionBase.Opening -= this.HandleDeviceOpening;
                this.VisaSessionBase.Opened -= this.HandleDeviceOpened;
                this.VisaSessionBase.Closing -= this.HandleDeviceClosing;
                this.VisaSessionBase.Closed -= this.HandleDeviceClosed;
                this._SelectorOpener.AssignSelectorViewModel( null );
                this._SelectorOpener.AssignOpenerViewModel( null );
                this.BindVisaSession( false, this.VisaSessionBase );
                this.BindSession( false, this.VisaSessionBase.Session );
                this.VisaSessionBase = null;
            }

            this.VisaSessionBase = visaSessionBase;
            if ( visaSessionBase is object )
            {
                visaSessionBase.AddPrivateListener( this._TraceMessagesBox );
                this.VisaSessionBase.Opening += this.HandleDeviceOpening;
                this.VisaSessionBase.Opened += this.HandleDeviceOpened;
                this.VisaSessionBase.Closing += this.HandleDeviceClosing;
                this.VisaSessionBase.Closed += this.HandleDeviceClosed;
                this._SelectorOpener.AssignSelectorViewModel( visaSessionBase.SessionFactory );
                this._SelectorOpener.AssignOpenerViewModel( visaSessionBase );
                this.BindVisaSession( true, this.VisaSessionBase );
                this.BindSession( true, this.VisaSessionBase.Session );
                this.VisaSessionBase.Session.ApplySettings();
                if ( !string.IsNullOrWhiteSpace( this.VisaSessionBase.CandidateResourceName ) )
                    this.VisaSessionBase.Session.CandidateResourceName = this.VisaSessionBase.CandidateResourceName;
                if ( !string.IsNullOrWhiteSpace( this.VisaSessionBase.CandidateResourceTitle ) )
                    this.VisaSessionBase.Session.CandidateResourceTitle = this.VisaSessionBase.CandidateResourceTitle;
                if ( this.VisaSessionBase.IsDeviceOpen )
                {
                    this.VisaSessionBase.Session.OpenResourceName = this.VisaSessionBase.OpenResourceName;
                    this.VisaSessionBase.Session.OpenResourceTitle = this.VisaSessionBase.OpenResourceTitle;
                }

                this.AssignTalker( visaSessionBase.Talker );
                this.ApplyListenerTraceLevel( ListenerType.Display, visaSessionBase.Talker.TraceShowLevel );
            }

            this._SessionView.BindVisaSessionBase( visaSessionBase );
            this.DisplayView.BindVisaSessionBase( visaSessionBase );
            this.StatusView.BindVisaSessionBase( visaSessionBase );
        }

        /// <summary> Bind visa session. </summary>
        /// <param name="add">             True to add. </param>
        /// <param name="visaSessionBase"> The assigned visa session or nothing to release the session. </param>
        private void BindVisaSession( bool add, VisaSessionBase visaSessionBase )
        {
            foreach ( TabPage t in this._Tabs.TabPages )
            {
                if ( !(ReferenceEquals( t, this._MessagesTabPage ) || ReferenceEquals( t, this._SessionTabPage )) )
                {
                    _ = this.AddRemoveBinding( t, add, nameof( this.Enabled ), visaSessionBase, nameof( visaSessionBase.IsSessionOpen ) );
                }
            }
        }

        /// <summary> Bind session. </summary>
        /// <param name="add">       True to add. </param>
        /// <param name="viewModel"> The view model. </param>
        private void BindSession( bool add, Pith.SessionBase viewModel )
        {
            _ = this.AddRemoveBinding( this._StatusPromptLabel, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.StatusPrompt ) );
        }

        #endregion

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Gets the resource name caption. </summary>
        /// <value> The resource name caption. </value>
        private string ResourceNameCaption { get; set; } = string.Empty;

        /// <summary> Executes the device Closing action. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnDeviceClosing( CancelEventArgs e )
        {
        }

        /// <summary> Handles the device Closing. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceClosing( object sender, CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceNameCaption} UI handling device closing";
                this.OnDeviceClosing( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the device Closed action. </summary>
        protected virtual void OnDeviceClosed()
        {
        }

        /// <summary> Handles the device Close. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceClosed( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceNameCaption} UI handling device closed";
                this.OnDeviceClosed();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the device Opening action. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnDeviceOpening( CancelEventArgs e )
        {
            this.ResourceNameCaption = this.VisaSessionBase.ResourceNameCaption;
        }

        /// <summary> Handles the device Opening. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceOpening( object sender, CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.VisaSessionBase.ResourceNameCaption} UI handling device Opening";
                this.OnDeviceOpening( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the device opened action. </summary>
        protected virtual void OnDeviceOpened()
        {
            this.ResourceNameCaption = this.VisaSessionBase.ResourceNameCaption;
        }

        /// <summary> Handles the device opened. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceOpened( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.VisaSessionBase.ResourceNameCaption} UI handling device opened";
                this.OnDeviceOpened();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CUSTOMIZATION "

        /// <summary> Gets the Display view. </summary>
        /// <value> The Display view. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DisplayView DisplayView { get; private set; }

        /// <summary> Gets the status view. </summary>
        /// <value> The status view. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public StatusView StatusView { get; private set; }

        /// <summary> Gets the number of views. </summary>
        /// <value> The number of tabs. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int ViewsCount => this._Tabs.TabCount;

        /// <summary> Adds (inserts) a View. </summary>
        /// <param name="view"> The view control. </param>
        public void AddView( VisaViewControl view )
        {
            this.AddView( view.Control, view.Index, view.Name, view.Caption );
        }

        /// <summary> Adds (inserts) a View. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="view">        The view control. </param>
        /// <param name="viewIndex">   Zero-based index of the view. </param>
        /// <param name="viewName">    The name of the view. </param>
        /// <param name="viewCaption"> The caption of the view. </param>
        public void AddView( Control view, int viewIndex, string viewName, string viewCaption )
        {
            if ( view is null )
                throw new ArgumentNullException( nameof( view ) );
            var tabPage = new TabPage( viewName );
            // https://social.msdn.microsoft.com/Forums/windows/en-US/5d10fd0c-1aa6-4092-922e-1fd7af979663/tabpagesinsert-bug?forum=winforms
            // https://stackoverflow.com/questions/1532301/visual-studio-tabcontrol-tabpages-insert-not-working
            // The tab control's handler must be created for the insert method to work
            _ = this._Tabs.Handle;
            this._Tabs.TabPages.Insert( viewIndex, tabPage );
            tabPage.SuspendLayout();
            tabPage.Controls.Add( view );
            tabPage.Location = new Point( 4, 26 );
            tabPage.Name = $"{viewName?.Replace( " ", string.Empty )}TabPage";
            tabPage.Size = new Size( 356, 255 );
            tabPage.Text = viewCaption;
            tabPage.UseVisualStyleBackColor = true;
            tabPage.ResumeLayout();
            view.Dock = DockStyle.Fill;
        }

        /// <summary> Adds (inserts) a View. </summary>
        /// <param name="view">        The view control. </param>
        /// <param name="viewIndex">   Zero-based index of the view. </param>
        /// <param name="viewName">    The name of the view. </param>
        /// <param name="viewCaption"> The caption of the view. </param>
        public void AddView( ModelViewTalkerBase view, int viewIndex, string viewName, string viewCaption )
        {
            this.AddView( view as Control, viewIndex, viewName, viewCaption );
            view.AddPrivateListener( this._TraceMessagesBox );
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Adds a private listener. </summary>
        /// <param name="listener"> The listener. </param>
        private void AddPrivateListenerThis( IMessageListener listener )
        {
            // this causes a binding cross thread exception when binding to the selector opener. Weird. 
            this._SelectorOpener.AddPrivateListener( listener );
            this.StatusView.AddPrivateListener( listener );
            this.DisplayView.AddPrivateListener( listener );
            this._SessionView.AddPrivateListener( listener );
        }

        /// <summary> Assigns talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( ITraceMessageTalker talker )
        {
            this._SelectorOpener.AssignTalker( talker );
            this.StatusView.AssignTalker( talker );
            this.DisplayView.AssignTalker( talker );
            this._SessionView.AssignTalker( talker );
            this.AddPrivateListenerThis( this._TraceMessagesBox );
            base.AssignTalker( talker );
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            this._SelectorOpener.ApplyListenerTraceLevel( listenerType, value );
            this._SessionView?.ApplyListenerTraceLevel( listenerType, value );
            // this should apply only to the listeners associated with this form
            // MyBase.ApplyListenerTraceLevel(listenerType, value)
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

        #region " UNIT TESTS INTERNALS "

        /// <summary> Gets the number of internal resource names. </summary>
        /// <value> The number of internal resource names. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int InternalResourceNamesCount => this._SelectorOpener.InternalResourceNamesCount;

        /// <summary> Gets the name of the internal selected resource. </summary>
        /// <value> The name of the internal selected resource. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string InternalSelectedResourceName => this._SelectorOpener.InternalSelectedResourceName;

        #endregion

    }
}
