using System;
using System.Windows.Forms;

namespace isr.VI.Facade
{

    /// <summary> Form for viewing the split visa view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-01-03 </para>
    /// </remarks>
    public class SplitVisaViewForm : Core.Forma.ConsoleForm
    {

        #region " CONSTRUCTION and CLEAN UP "

        /// <summary> Default constructor. </summary>
        public SplitVisaViewForm() : base()
        {
            this.Name = "Visa.View.Form";
        }

        /// <summary> Creates a new SplitVisaViewForm. </summary>
        /// <returns> A SplitVisaViewForm. </returns>
        public static SplitVisaViewForm Create()
        {
            SplitVisaView SplitVisaView = null;
            try
            {
                SplitVisaView = new SplitVisaView();
                return Create( SplitVisaView );
            }
            catch
            {
                if ( SplitVisaView is object )
                {
                    SplitVisaView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Creates a new SplitVisaViewForm. </summary>
        /// <param name="splitVisaView"> The visa view. </param>
        /// <returns> A SplitVisaViewForm. </returns>
        public static SplitVisaViewForm Create( SplitVisaView splitVisaView )
        {
            SplitVisaViewForm result = null;
            try
            {
                result = new SplitVisaViewForm() { SplitVisaView = splitVisaView };
            }
            catch
            {
                if ( result is object )
                {
                    result.Dispose();
                }

                throw;
            }

            return result;
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.VisaViewDisposeEnabled && this.SplitVisaView is object )
                    {
                        this.SplitVisaView.Dispose();
                        if ( this.DisplayView is object )
                            this.DisplayView.Dispose();
                        if ( this.SessionStatusView is object )
                            this.SessionStatusView.Dispose();
                    }

                    this.SplitVisaView = null;
                    this.DisplayView = null;
                    this.SessionStatusView = null;
                    if ( this.VisaSessionBase is object )
                    {
                        this.VisaSessionBase.Dispose();
                        this.VisaSessionBase = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding Visa View";
                // the talker control publishes the device messages which thus get published to the form message box.
                this.AddTalkerControl( this.SplitVisaView.VisaSessionBase.CandidateResourceTitle, this.SplitVisaView, false, false );
                // any form messages will be logged.
                activity = $"{this.Name}; adding log listener";
                this.AddListener( My.MyLibrary.Logger );
                if ( !string.IsNullOrWhiteSpace( this.SplitVisaView.VisaSessionBase.CandidateResourceName ) )
                {
                    activity = $"{this.Name}; starting {this.SplitVisaView.VisaSessionBase.CandidateResourceName} selection task";
                    _ = this.SplitVisaView.VisaSessionBase.AsyncValidateResourceName( this.SplitVisaView.VisaSessionBase.CandidateResourceName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary> Gets or sets the await the resource name validation task enabled. </summary>
        /// <value> The await the resource name validation task enabled. </value>
        protected bool AwaitResourceNameValidationTaskEnabled { get; set; }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.SplitVisaView.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
                if ( this.SplitVisaView.VisaSessionBase.IsValidatingResourceName() )
                {
                    if ( this.AwaitResourceNameValidationTaskEnabled )
                    {
                        activity = $"{this.Name}; awaiting {this.SplitVisaView.VisaSessionBase.CandidateResourceName} validation";
                        this.SplitVisaView.VisaSessionBase.AwaitResourceNameValidation( My.Settings.Default.ResourceNameSelectionTimeout );
                    }
                    else
                    {
                        activity = $"{this.Name}; validating {this.SplitVisaView.VisaSessionBase.CandidateResourceName}";
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if ( this.SplitVisaView is object )
                    this.SplitVisaView.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " VISA "

        /// <summary> Gets or sets the locally assigned visa session if any. </summary>
        /// <value> The visa session. </value>
        protected VisaSessionBase VisaSessionBase { get; set; }

        /// <summary> Gets or sets the display view. </summary>
        /// <value> The display view. </value>
        private DisplayView DisplayView { get; set; }

        /// <summary> Gets or sets the session status view. </summary>
        /// <value> The session status view. </value>
        private SessionStatusView SessionStatusView { get; set; }

        /// <summary> Gets or sets the visa view. </summary>
        /// <value> The visa view. </value>
        public SplitVisaView SplitVisaView { get; set; }

        /// <summary> Gets or sets the visa view dispose enabled. </summary>
        /// <value> The visa view dispose enabled. </value>
        public bool VisaViewDisposeEnabled { get; set; }

        /// <summary> Creates split view. </summary>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        private void CreateSplitView( string candidateResourceName, string candidateResourceTitle )
        {
            this.VisaViewDisposeEnabled = true;
            this.VisaSessionBase = new VisaSession();
            this.SplitVisaView = new SplitVisaView();
            this.DisplayView = new DisplayView();
            this.DisplayView.BindVisaSessionBase( this.VisaSessionBase );
            this.SessionStatusView = new SessionStatusView();
            this.SessionStatusView.BindVisaSessionBase( this.VisaSessionBase );
            this.SplitVisaView.BindVisaSessionBase( this.VisaSessionBase );
            this.SplitVisaView.AddHeader( this.DisplayView );
            _ = this.SplitVisaView.AddNode( "Session", "Session", this.SessionStatusView );
            this.SplitVisaView.VisaSessionBase.CandidateResourceTitle = candidateResourceTitle;
            this.SplitVisaView.VisaSessionBase.CandidateResourceName = candidateResourceName;
        }

        #endregion

        #region " SHOW DIALOG "

        /// <summary> Shows the form dialog. </summary>
        /// <param name="owner">                  The owner. </param>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public DialogResult ShowDialog( IWin32Window owner, string candidateResourceName, string candidateResourceTitle )
        {
            SplitVisaView SplitVisaView = null;
            try
            {
                this.CreateSplitView( candidateResourceName, candidateResourceTitle );
                return this.ShowDialog( owner, this.SplitVisaView );
            }
            catch
            {
                if ( SplitVisaView is object )
                {
                    SplitVisaView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Shows the form dialog. </summary>
        /// <param name="owner">         The owner. </param>
        /// <param name="splitVisaView"> The visa view. </param>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public DialogResult ShowDialog( IWin32Window owner, SplitVisaView splitVisaView )
        {
            this.SplitVisaView = splitVisaView;
            return this.ShowDialog( owner );
        }

        /// <summary> Shows the form. </summary>
        /// <param name="owner">                  The owner. </param>
        /// <param name="candidateResourceName">  Name of the candidate resource. </param>
        /// <param name="candidateResourceTitle"> The candidate resource title. </param>
        public void Show( IWin32Window owner, string candidateResourceName, string candidateResourceTitle )
        {
            SplitVisaView SplitVisaView = null;
            try
            {
                this.CreateSplitView( candidateResourceName, candidateResourceTitle );
                this.Show( owner, this.SplitVisaView );
            }
            catch
            {
                if ( SplitVisaView is object )
                {
                    SplitVisaView.Dispose();
                }

                throw;
            }
        }

        /// <summary> Shows the form. </summary>
        /// <param name="owner">         The owner. </param>
        /// <param name="splitVisaView"> The visa view. </param>
        public void Show( IWin32Window owner, SplitVisaView splitVisaView )
        {
            this.SplitVisaView = splitVisaView;
            this.Show( owner );
        }

        #endregion

    }
}
