using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Facade
{

    /// <summary> A connector view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ConnectorView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public ConnectorView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary> Creates a new <see cref="ConnectorView"/> </summary>
        /// <returns> A <see cref="ConnectorView"/>. </returns>
        public static ConnectorView Create()
        {
            ConnectorView view = null;
            try
            {
                view = new ConnectorView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the session is unbound in case the form is closed without closing the device.
                    this.BindVisaSessionBaseThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " VISA SESSION BASE (DEVICE BASE) "

        /// <summary> Gets or sets the visa session base. </summary>
        /// <value> The visa session base. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Bind visa session base. </summary>
        /// <param name="value"> The value. </param>
        public void BindVisaSessionBase( VisaSessionBase value )
        {
            this.BindVisaSessionBaseThis( value );
        }

        /// <summary> Bind visa session base. </summary>
        /// <param name="value"> The value. </param>
        private void BindVisaSessionBaseThis( VisaSessionBase value )
        {
            if ( this.VisaSessionBase is object )
            {
                this.AssignTalker( null );
                this.VisaSessionBase.Opening -= this.DeviceOpening;
                this.VisaSessionBase.Opened -= this.DeviceOpened;
                this.VisaSessionBase.Closing -= this.DeviceClosing;
                this.VisaSessionBase.Closed -= this.DeviceClosed;
                this.VisaSessionBase.Initialized -= this.DeviceInitialized;
                this.VisaSessionBase.Initializing -= this.DeviceInitializing;
                this._ResourceSelectorConnector.Enabled = false;
                this._ResourceSelectorConnector.AssignSelectorViewModel( null );
                this._ResourceSelectorConnector.AssignOpenerViewModel( null );
                this._ResourceSelectorConnector.AssignTalker( null );
            }

            this.VisaSessionBase = value;
            if ( value is object )
            {
                this.AssignTalker( this.VisaSessionBase.Talker );
                this.VisaSessionBase.Opening += this.DeviceOpening;
                this.VisaSessionBase.Opened += this.DeviceOpened;
                this.VisaSessionBase.Closing += this.DeviceClosing;
                this.VisaSessionBase.Closed += this.DeviceClosed;
                this.VisaSessionBase.Initialized += this.DeviceInitialized;
                this.VisaSessionBase.Initializing += this.DeviceInitializing;
                this._ResourceSelectorConnector.Enabled = true;
                this._ResourceSelectorConnector.Openable = true;
                this._ResourceSelectorConnector.Clearable = true;
                this._ResourceSelectorConnector.Searchable = true;
                this._ResourceSelectorConnector.AssignSelectorViewModel( value.SessionFactory );
                this._ResourceSelectorConnector.AssignOpenerViewModel( value );
                this._ResourceSelectorConnector.AssignTalker( value.Talker );
            }

            this.BindSessionFactory( value );
        }

        /// <summary> Reads the status register. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.VisaSessionBase.ResourceNameCaption} reading service request";
            try
            {
                _ = this.VisaSessionBase.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE EVENTS "

        /// <summary>
        /// Event handler. Called upon device opening so as to instantiated all subsystems.
        /// </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected void DeviceOpening( object sender, CancelEventArgs e )
        {
            _ = this.PublishVerbose( $"Opening access to {this.ResourceName};. " );
            this._IdentityTextBox.Text = "connecting...";
        }

        /// <summary>
        /// Event handler. Called after the device opened and all subsystems were defined.
        /// </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void DeviceOpened( object sender, EventArgs e )
        {
            var outcome = TraceEventType.Information;
            if ( this.VisaSessionBase.Session.Enabled & !this.VisaSessionBase.Session.IsSessionOpen )
                outcome = TraceEventType.Warning;
            _ = this.Publish( outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ", this.VisaSessionBase.ResourceTitleCaption, this.VisaSessionBase.Session.Enabled.GetHashCode(), this.VisaSessionBase.Session.IsDeviceOpen.GetHashCode(), this.VisaSessionBase.Session.IsSessionOpen.GetHashCode() );
            string activity = string.Empty;
            try
            {
                this._IdentityTextBox.Text = "identifying...";
                Application.DoEvents();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Attempts to open a session to the device using the specified resource name.
        /// </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The title. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryOpenDeviceSession( string resourceName, string resourceTitle )
        {
            string activity = $"opening {resourceName} VISA session";
            try
            {
                var (success, details) = this.VisaSessionBase.TryOpenSession( resourceName, resourceTitle );
                return success ? (success, details) : (success, $"failed {activity};. {details}");
            }
            catch ( Exception ex )
            {
                return (false, $"Exception {activity};. {ex.ToFullBlownString()}");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Device initializing. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Cancel event information. </param>
        protected virtual void DeviceInitializing( object sender, CancelEventArgs e )
        {
        }

        /// <summary> Device initialized. </summary>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected virtual void DeviceInitialized( object sender, EventArgs e )
        {
            this._IdentityTextBox.Text = this.VisaSessionBase.StatusSubsystemBase.Identity;
        }

        /// <summary> Event handler. Called when device is closing. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceClosing( object sender, CancelEventArgs e )
        {
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                    activity = $"Disconnecting from {this.ResourceName}";
                    _ = this.PublishInfo( $"{activity};. " );
                    if ( this.VisaSessionBase.Session is object )
                        this.VisaSessionBase.Session.DisableServiceRequestEventHandler();
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
        }

        /// <summary> Event handler. Called when device is closed. </summary>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceClosed( object sender, EventArgs e )
        {
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                    activity = $"Disconnected from {this.ResourceName}";
                    _ = this.PublishVerbose( $"{activity};. " );
                    if ( this.VisaSessionBase.Session.IsSessionOpen )
                    {
                        _ = this.PublishWarning( $"{this.VisaSessionBase.Session.ResourceNameCaption} closed but session still open;. " );
                        this._IdentityTextBox.Text = "<failed disconnecting>";
                    }
                    else if ( this.VisaSessionBase.Session.IsDeviceOpen )
                    {
                        _ = this.PublishWarning( $"{this.VisaSessionBase.Session.ResourceNameCaption} closed but emulated session still open;. " );
                        this._IdentityTextBox.Text = "<failed disconnecting>";
                    }
                    else
                    {
                        _ = this.PublishVerbose( "Disconnected; Device access closed." );
                        this._IdentityTextBox.Text = "<disconnected>";
                    }
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
            else
            {
                _ = this.PublishInfo( "Disconnected; Device disposed." );
            }
        }

        #endregion

        #region " SESSION FACTORY "

        /// <summary> Gets or sets the session factory. </summary>
        /// <value> The session factory. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SessionFactory SessionFactory { get; private set; }

        /// <summary> Bind session factory. </summary>
        /// <param name="value"> The value. </param>
        public void BindSessionFactory( VisaSessionBase value )
        {
            if ( value is null )
            {
                this.BindSessionFactoryThis( null );
            }
            else
            {
                this.BindSessionFactoryThis( value.SessionFactory );
            }
        }

        /// <summary> Bind session factory. </summary>
        /// <param name="value"> The value. </param>
        private void BindSessionFactoryThis( SessionFactory value )
        {
            if ( this.SessionFactory is object )
            {
                this.AssignTalker( null );
                this.SessionFactory.PropertyChanged -= this.SessionFactoryPropertyChanged;
            }

            this.SessionFactory = value;
            if ( value is object )
            {
                this.AssignTalker( this.SessionFactory.Talker );
                this.SessionFactory.PropertyChanged += this.SessionFactoryPropertyChanged;
                this.HandlePropertyChanged( value, nameof( VI.SessionFactory.IsOpen ) );
            }
        }

        /// <summary> Name of the resource. </summary>
        private string _ResourceName;

        /// <summary> Gets or sets the name of the resource. </summary>
        /// <value> The name of the resource. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string ResourceName
        {
            get => this._ResourceName;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourceName ) )
                {
                    this._ResourceName = value;
                    this.VisaSessionBase.SessionFactory.CandidateResourceName = value;
                }
            }
        }

        /// <summary> Gets or sets the Search Pattern of the resource. </summary>
        /// <value> The Search Pattern of the resource. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string ResourceFilter
        {
            get => this.VisaSessionBase.SessionFactory.ResourcesFilter;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourceFilter ) )
                {
                    this.VisaSessionBase.SessionFactory.ResourcesFilter = value;
                }
            }
        }

        /// <summary> Executes the session factory property changed action. </summary>
        /// <param name="sender">       Specifies the object where the call originated. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SessionFactory sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( this.SessionFactory.ValidatedResourceName ):
                    {
                        this.ResourceName = sender.ValidatedResourceName;
                        break;
                    }

                case nameof( this.SessionFactory.CandidateResourceName ):
                    {
                        this.ResourceName = sender.CandidateResourceName;
                        break;
                    }

                case nameof( this.SessionFactory.IsOpen ):
                    {
                        if ( sender.IsOpen )
                        {
                            this.ResourceName = sender.ValidatedResourceName;
                            this._IdentityTextBox.Text = $"Resource {this.ResourceName} connected";
                            _ = this.PublishInfo( $"Resource connected;. {this.ResourceName}" );
                        }
                        else
                        {
                            this.ResourceName = sender.CandidateResourceName;
                            this._IdentityTextBox.Text = $"Resource {this.ResourceName} not open";
                            _ = this.PublishInfo( $"Resource not open;. {this.ResourceName}" );
                        }

                        break;
                    }
            }
        }

        /// <summary> Session factory property changed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionFactoryPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling session factory {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SessionFactoryPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SessionFactory, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
