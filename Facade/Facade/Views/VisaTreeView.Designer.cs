﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Facade
{
    [DesignerGenerated()]
    public partial class VisaTreeView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _SessionView = new SessionView();
            StatusView = new StatusView();
            _SelectorOpener = new Core.Controls.SelectorOpener();
            _TraceMessagesBox = new Core.Forma.TraceMessagesBox();
            _StatusStrip = new StatusStrip();
            _StatusPromptLabel = new Core.Controls.ToolStripStatusLabel();
            _Panel = new Panel();
            __TreePanel = new Core.Controls.TreePanel();
            __TreePanel.AfterNodeSelected += new EventHandler<TreeViewEventArgs>(TreePanel_AfterNodeSelected);
            _SessionPanel = new Panel();
            DisplayView = new DisplayView();
            _Layout = new TableLayoutPanel();
            _StatusStrip.SuspendLayout();
            _Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)__TreePanel).BeginInit();
            __TreePanel.SuspendLayout();
            _SessionPanel.SuspendLayout();
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _SessionView
            // 
            _SessionView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _SessionView.Dock = DockStyle.Fill;
            _SessionView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SessionView.Location = new System.Drawing.Point(0, 3);
            _SessionView.Margin = new Padding(3, 4, 3, 4);
            _SessionView.Name = "_SessionView";
            _SessionView.Padding = new Padding(1);
            _SessionView.Size = new System.Drawing.Size(217, 117);
            _SessionView.TabIndex = 25;
            // 
            // _StatusView
            // 
            StatusView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            StatusView.BackColor = System.Drawing.Color.Transparent;
            StatusView.Dock = DockStyle.Bottom;
            StatusView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            StatusView.Location = new System.Drawing.Point(0, 149);
            StatusView.Margin = new Padding(0);
            StatusView.Name = "_StatusView";
            StatusView.Size = new System.Drawing.Size(217, 31);
            StatusView.TabIndex = 27;
            StatusView.UsingStatusSubsystemOnly = false;
            // 
            // _SelectorOpener
            // 
            _SelectorOpener.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _SelectorOpener.BackColor = System.Drawing.Color.Transparent;
            _SelectorOpener.Dock = DockStyle.Bottom;
            _SelectorOpener.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SelectorOpener.Location = new System.Drawing.Point(0, 120);
            _SelectorOpener.Margin = new Padding(0);
            _SelectorOpener.Name = "_SelectorOpener";
            _SelectorOpener.Size = new System.Drawing.Size(217, 29);
            _SelectorOpener.TabIndex = 26;
            // 
            // _TraceMessagesBox
            // 
            _TraceMessagesBox.AlertLevel = TraceEventType.Warning;
            _TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info;
            _TraceMessagesBox.CaptionFormat = "{0} ≡";
            _TraceMessagesBox.CausesValidation = false;
            _TraceMessagesBox.Font = new System.Drawing.Font("Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TraceMessagesBox.Location = new System.Drawing.Point(315, 365);
            _TraceMessagesBox.Multiline = true;
            _TraceMessagesBox.Name = "_TraceMessagesBox";
            _TraceMessagesBox.PresetCount = 500;
            _TraceMessagesBox.ReadOnly = true;
            _TraceMessagesBox.ResetCount = 1000;
            _TraceMessagesBox.ScrollBars = ScrollBars.Both;
            _TraceMessagesBox.Size = new System.Drawing.Size(134, 49);
            _TraceMessagesBox.StatusPrompt = null;
            _TraceMessagesBox.TabIndex = 1;
            _TraceMessagesBox.TraceLevel = TraceEventType.Verbose;
            // 
            // _StatusStrip
            // 
            _StatusStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _StatusStrip.GripMargin = new Padding(0);
            _StatusStrip.Items.AddRange(new ToolStripItem[] { _StatusPromptLabel });
            _StatusStrip.Location = new System.Drawing.Point(0, 426);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Padding = new Padding(1, 0, 16, 0);
            _StatusStrip.ShowItemToolTips = true;
            _StatusStrip.Size = new System.Drawing.Size(495, 22);
            _StatusStrip.SizingGrip = false;
            _StatusStrip.TabIndex = 15;
            _StatusStrip.Text = "StatusStrip1";
            // 
            // _StatusPromptLabel
            // 
            _StatusPromptLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _StatusPromptLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _StatusPromptLabel.Name = "_StatusPromptLabel";
            _StatusPromptLabel.Overflow = ToolStripItemOverflow.Never;
            _StatusPromptLabel.Size = new System.Drawing.Size(478, 17);
            _StatusPromptLabel.Spring = true;
            _StatusPromptLabel.Text = "<Status>";
            _StatusPromptLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            _StatusPromptLabel.ToolTipText = "Status prompt";
            // 
            // _Panel
            // 
            _Panel.Controls.Add(__TreePanel);
            _Panel.Controls.Add(_TraceMessagesBox);
            _Panel.Controls.Add(_SessionPanel);
            _Panel.Controls.Add(DisplayView);
            _Panel.Controls.Add(_StatusStrip);
            _Panel.Dock = DockStyle.Fill;
            _Panel.Location = new System.Drawing.Point(0, 0);
            _Panel.Margin = new Padding(0);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(495, 448);
            _Panel.TabIndex = 16;
            // 
            // _TreePanel
            // 
            __TreePanel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TreePanel.Location = new System.Drawing.Point(27, 174);
            __TreePanel.Name = "__TreePanel";
            __TreePanel.Size = new System.Drawing.Size(78, 84);
            __TreePanel.SplitterDistance = 25;
            __TreePanel.TabIndex = 18;
            // 
            // _SessionPanel
            // 
            _SessionPanel.Controls.Add(_SessionView);
            _SessionPanel.Controls.Add(_SelectorOpener);
            _SessionPanel.Controls.Add(StatusView);
            _SessionPanel.Location = new System.Drawing.Point(123, 168);
            _SessionPanel.Name = "_SessionPanel";
            _SessionPanel.Padding = new Padding(0, 3, 0, 0);
            _SessionPanel.Size = new System.Drawing.Size(217, 180);
            _SessionPanel.TabIndex = 17;
            // 
            // _DisplayView
            // 
            DisplayView.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            DisplayView.Dock = DockStyle.Top;
            DisplayView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            DisplayView.Location = new System.Drawing.Point(0, 0);
            DisplayView.Margin = new Padding(3, 4, 3, 4);
            DisplayView.Name = "_DisplayView";
            DisplayView.Padding = new Padding(1);
            DisplayView.Size = new System.Drawing.Size(495, 152);
            DisplayView.TabIndex = 16;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 1;
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.Controls.Add(_Panel, 0, 1);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Margin = new Padding(0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 2;
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.Size = new System.Drawing.Size(495, 448);
            _Layout.TabIndex = 17;
            // 
            // VisaTreeView
            // 
            Controls.Add(_Layout);
            Name = "VisaTreeView";
            Size = new System.Drawing.Size(495, 448);
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)__TreePanel).EndInit();
            __TreePanel.ResumeLayout(false);
            _SessionPanel.ResumeLayout(false);
            _Layout.ResumeLayout(false);
            ResumeLayout(false);
        }

        private Panel _Panel;
        private TableLayoutPanel _Layout;
        private Core.Forma.TraceMessagesBox _TraceMessagesBox;
        private StatusStrip _StatusStrip;
        private Core.Controls.ToolStripStatusLabel _StatusPromptLabel;
        private SessionView _SessionView;
        private Core.Controls.SelectorOpener _SelectorOpener;
        private Panel _SessionPanel;
        private Core.Controls.TreePanel __TreePanel;

        private Core.Controls.TreePanel _TreePanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TreePanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TreePanel != null)
                {
                    __TreePanel.AfterNodeSelected -= TreePanel_AfterNodeSelected;
                }

                __TreePanel = value;
                if (__TreePanel != null)
                {
                    __TreePanel.AfterNodeSelected += TreePanel_AfterNodeSelected;
                }
            }
        }
    }
}