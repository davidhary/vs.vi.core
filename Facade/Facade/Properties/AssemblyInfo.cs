﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.VI.Facade.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.VI.Facade.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.VI.Facade.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
