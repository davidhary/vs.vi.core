using System;

namespace isr.VI.Facade
{
    /// <summary>   A settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Core.Json.SettingsSection( nameof( Settings ) )]
    public class Settings : isr.Core.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public Settings() : base( isr.Core.Json.JsonSettingsBase.BuildCallingAssemblySettingsFullFileName() )
        {
        }

        /// <summary>   (Immutable) The lazy instance. </summary>
        private static readonly Lazy<Settings> LazyInstance = new( () => new Settings() );

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static Settings Instance
        {
            get {
                if ( !LazyInstance.IsValueCreated )
                {
                    LazyInstance.Value.ReadSettings();
                }
                return LazyInstance.Value;
            }
        }

        private System.TimeSpan _StatusReadDelay = TimeSpan.FromMilliseconds( 10 );

        /// <summary>   Gets or sets the status read delay. </summary>
        /// <value> The status read delay. </value>
        public System.TimeSpan StatusReadDelay
        {
            get => this._StatusReadDelay;
            set {
                if ( !System.TimeSpan.Equals( value, this.StatusReadDelay ) )
                {
                    this._StatusReadDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.TimeSpan _ReadDelay = TimeSpan.FromMilliseconds( 10 );

        /// <summary>   Gets or sets the read delay. </summary>
        /// <value> The read delay. </value>
        public System.TimeSpan ReadDelay
        {
            get => this._ReadDelay;
            set {
                if ( !System.TimeSpan.Equals( value, this.ReadDelay ) )
                {
                    this._ReadDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.TimeSpan _MaximumDelay = TimeSpan.FromMilliseconds( 1000 );

        /// <summary>   Gets or sets the read delay. </summary>
        /// <value> The read delay. </value>
        public System.TimeSpan MaximumDelay
        {
            get => this._MaximumDelay;
            set {
                if ( !System.TimeSpan.Equals( value, this.MaximumDelay ) )
                {
                    this._MaximumDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _Termination = "\\n";

        public string Termination
        {
            get => this._Termination;
            set {
                if ( !string.Equals( value, this.Termination ) )
                {
                    this._Termination = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

		System.Drawing.Color _CharcoalColor = System.Drawing.Color.FromRGB(30, 38, 44);
        public System.Drawing.Color CharcoalColor 
		{
            get => this._CharcoalColor;
            set {
                if ( !Systemm.Drawing.Color.Equals( value, this.CharcoalColor ) )
                {
                    this._CharcoalColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _ResourceTitle = "Facade";

        public string ResourceTitle
        {
            get => this._ResourceTitle;
            set {
                if ( !string.Equals( value, this.ResourceTitle ) )
                {
                    this._ResourceTitle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.TimeSpan _ResourceNameSelectionTimeout = TimeSpan.FromMilliseconds( 10000 );

        public System.TimeSpan ResourceNameSelectionTimeout
        {
            get => this._ResourceNameSelectionTimeout;
            set {
                if ( !System.TimeSpan.Equals( value, this.ResourceNameSelectionTimeout ) )
                {
                    this._ResourceNameSelectionTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private double _NominalResistance = 100;

        public double NominalResistance
        {
            get => this._NominalResistance;
            set {
                if ( !double.Equals( value, this.NominalResistance ) )
                {
                    this._NominalResistance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private double _ResistanceTolerance = 0.01;

        public double ResistanceTolerance
        {
            get => this._ResistanceTolerance;
            set {
                if ( !double.Equals( value, this.ResistanceTolerance ) )
                {
                    this._ResistanceTolerance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private double _OpenLimit = 1000;

        public double OpenLimit
        {
            get => this._OpenLimit;
            set {
                if ( !double.Equals( value, this.OpenLimit ) )
                {
                    this._OpenLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _PassBitmask = 1;

        public int PassBitmask
        {
            get => this._PassBitmask;
            set {
                if ( !int.Equals( value, this.PassBitmask ) )
                {
                    this._PassBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        private int _FailBitmask = 2;

        public int FailBitmask
        {
            get => this._FailBitmask;
            set {
                if ( !int.Equals( value, this.FailBitmask ) )
                {
                    this._FailBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _OverflowBitmask = 4;

        public int OverflowBitmask
        {
            get => this._OverflowBitmask;
            set {
                if ( !int.Equals( value, this.OverflowBitmask ) )
                {
                    this._OverflowBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.TimeSpan _BinningStrobeDuration = TimeSpan.FromMilliseconds( 10 );

        public System.TimeSpan BinningStrobeDuration
        {
            get => this._BinningStrobeDuration;
            set {
                if ( !System.TimeSpan.Equals( value, this.BinningStrobeDuration ) )
                {
                    this._BinningStrobeDuration = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.TimeSpan _BufferStreamPollInterval = TimeSpan.FromMilliseconds( 50 );

        public System.TimeSpan BufferStreamPollInterval
        {
            get => this._BufferStreamPollInterval;
            set {
                if ( !System.TimeSpan.Equals( value, this.BufferStreamPollInterval ) )
                {
                    this._BufferStreamPollInterval = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

		private bool _DisplayStandardServiceRequests = true;
        public bool DisplayStandardServiceRequests
		{
            get => this._DisplayStandardServiceRequests;
            set {
                if ( !bool.Equals( value, this.DisplayStandardServiceRequests ) )
                {
                    this._DisplayStandardServiceRequests = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

		isr.VI.SenseFunctionModes _StreamBufferSenseFunctionMode = isr.VI.SenseFunctionModes.ResistanceFourWire ;
        public isr.VI.SenseFunctionModes StreamBufferSenseFunctionMode 
		{
            get => this._DisplayStandardServiceRequests;
            set {
                if ( !isr.VI.SenseFunctionModes.Equals( value, this.DisplayStandardServiceRequests ) )
                {
                    this._DisplayStandardServiceRequests = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

		private string _ScanCardScanList = "(@1:3)";
        public string ScanCardScanList
		{
            get => this._ScanCardScanList;
            set {
                if ( !string.Equals( value, this.ScanCardScanList ) )
                {
                    this._ScanCardScanList = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

		private int _ScanCardSampleCount  = 3;
        public int ScanCardSampleCount 
		{
            get => this._ScanCardSampleCount;
            set {
                if ( !int.Equals( value, this.ScanCardSampleCount ) )
                {
                    this._ScanCardSampleCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private  int _StreamTriggerCount = 999;

        public int StreamTriggerCount {
            get => this._StreamTriggerCount;
            set {
                if ( !int.Equals( value, this.StreamTriggerCount ) )
                {
                    this._StreamTriggerCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private isr.VI.TriggerSources _StreamBufferTriggerSource  = isr.VI.TriggerSources.Bus ;

        public isr.VI.TriggerSources StreamBufferTriggerSource 
		{
            get => this._StreamBufferTriggerSource ;
            set {
                if ( !isr.VI.TriggerSources.Equals( value, this.StreamBufferTriggerSource  ) )
                {
                    this.StreamBufferTriggerSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        private isr.VI.ArmSources _StreamBufferArmSource  = isr.VI.ArmSources.Bus ;

        public isr.VI.ArmSources StreamBufferArmSource 
		{
            get => this._StreamBufferArmSource ;
            set {
                if ( !isr.VI.ArmSources.Equals( value, this.StreamBufferArmSource  ) )
                {
                    this.StreamBufferArmSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private  int _StobeLineNumber = 4;

        public int StrobeLineNumber
		{
            get => this._StrobeLineNumber;
            set {
                if ( !int.Equals( value, this.StrobeLineNumber ) )
                {
                    this._StrobeLineNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.TimeSpan _StrobeDuration = TimeSpan.FromMilliseconds( 10 );

        public System.TimeSpan StrobeDuration
        {
            get => this._StrobeDuration;
            set {
                if ( !System.TimeSpan.Equals( value, this.StrobeDuration ) )
                {
                    this._StrobeDuration = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private  int _BinLineNumber = 1;

        public int BinLineNumber
		{
            get => this._BinLineNumber;
            set {
                if ( !int.Equals( value, this.BinLineNumber ) )
                {
                    this._BinLineNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.TimeSpan _BinDuration = TimeSpan.FromMilliseconds( 20 );

        public System.TimeSpan BinDuration
        {
            get => this._BinDuration;
            set {
                if ( !System.TimeSpan.Equals( value, this.BinDuration ) )
                {
                    this._BinDuration = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

    }
}
