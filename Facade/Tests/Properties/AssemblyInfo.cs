﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "Facade VI Tests" )]
[assembly: AssemblyDescription( "Virtual Instrument Facade Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.Facade.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
