using System;
using System.Diagnostics;

using isr.Core.SplitExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.FacadeTests
{
    public sealed partial class DeviceManager
    {

        /// <summary>   Assert visa view trace message should emit. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <param name="view">             The view. </param>
        /// <param name="queueListener">    The queue listener. </param>
        public static void AssertVisaViewTraceMessageShouldEmit( Facade.IVisaView view, Core.TraceMessagesQueueListener queueListener )
        {
            string payload = $"{nameof( Facade.IVisaView ).SplitWords()} trace message";
            int traceEventId = 1;
            _ = view.Talker.Publish( TraceEventType.Warning, traceEventId, payload );
            int fetchNumber = 0;
            Core.TraceMessage traceMessage = null;
            _ = Core.ApplianceBase.DoEventsWaitUntil( TimeSpan.FromMilliseconds( 200d ), () => {
                // with the new talker, the device identifies the following libraries: 
                // 0x0100 core agnostic; 0x01006 vi device and 0x01026 Keithley Meter
                // so these test looks for the first warning
                fetchNumber = 0;
                traceMessage = null;
                do
                {
                    while ( !queueListener.Any )
                        Core.ApplianceBase.DoEvents();
                    while ( queueListener.Any )
                    {
                        Core.ApplianceBase.DoEvents();
                        traceMessage = queueListener.TryDequeue();
                        fetchNumber += 1;
                        if ( traceMessage.EventType <= TraceEventType.Warning )
                        {
                            // we expect a single such message
                            break;
                        }
                    }
                }
                while ( traceMessage.EventType > TraceEventType.Warning );
                return traceMessage.EventType <= TraceEventType.Warning;
            } );
            if ( traceMessage is null )
                Assert.Fail( $"{payload} failed to trace fetch number {fetchNumber}" );
            Assert.AreEqual( traceEventId, traceMessage.Id, $"{payload} trace event id mismatch fetch #{fetchNumber} message {traceMessage.Details}" );
            // the queue may already have some messages
            // Assert.AreEqual(0, queueListener.Count, $"{payload} expected no more messages after fetch #{fetchNumber} message {traceMessage.Details}")
        }

        /// <summary>   Assert model should match. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="subsystem">        The subsystem. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertModelShouldMatch( StatusSubsystemBase subsystem, DeviceTests.ResourceSettingsBase resourceSettings )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            Assert.AreEqual( resourceSettings.ResourceModel, subsystem.VersionInfoBase.Model, $"Version Info Model {subsystem.ResourceNameCaption} Identity: '{subsystem.VersionInfoBase.Identity}'", System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>   Assert resource name should be selected. </summary>
        /// <remarks>
        /// Visa Tree View Timing:<para>
        /// Ping elapsed 0.119   </para><para>
        /// Create device 0.125   </para><para>
        /// Session Factory enumerating resource names 0.388   </para><para>
        /// Connector listing resource names 0.002   </para><para>
        /// Session factory listing resource names 0.000   </para><para>
        /// Session factory selecting TCPIP0:192.168.0.254:gpib0,22:INSTR selected  0.337   </para><para>
        /// Factory selected resource 0.000   </para><para>
        /// Check selected resource name 2.293   </para><para>
        /// </para>
        /// Visa View Timing:<para>
        /// Ping elapsed 0.123   </para><para>
        /// Create device 0.097   </para><para>
        /// Session Factory enumerating resource names 0.369   </para><para>
        /// Connector listing resource names 0.005   </para><para>
        /// Session factory listing resource names 0.000   </para><para>
        /// Session factory selecting TCPIP0:192.168.0.254:gpib0,22:INSTR selected  0.354   </para><para>
        /// Factory selected resource 0.000   </para><para>
        /// Check selected resource name 2.298   </para><para>
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="visaView">         The visa view control. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertResourceNameShouldBeSelected( DeviceTests.TestSite testInfo, Facade.IVisaView visaView, DeviceTests.ResourceSettingsBase resourceSettings )
        {
            if ( visaView is null )
                throw new ArgumentNullException( nameof( visaView ) );
            Assert.IsTrue( visaView.Talker is object, $"Talker is nothing #1" );
            var sw = Stopwatch.StartNew();
            var sessionBase = visaView.VisaSessionBase;
            Assert.IsFalse( string.IsNullOrEmpty( sessionBase.Session.ResourcesFilter ), "Resources filter should be defined" );
            string activity = "Session Factory enumerating resource names";
            var factory = sessionBase.SessionFactory;
            // enumerate resources without filtering
            _ = factory.EnumerateResources( false );
            testInfo.TraceMessage( $"{activity} {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            bool actualBoolean = factory.HasResources;
            bool expectedBoolean = true;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{activity} should succeed" );

            activity = "Connector listing resource names";
            actualBoolean = visaView.InternalResourceNamesCount > 0;
            expectedBoolean = true;
            testInfo.TraceMessage( $"{activity} {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{activity} should succeed" );

            activity = "Session factory listing resource names";
            actualBoolean = sessionBase.SessionFactory.ResourceNames.Count > 0;
            expectedBoolean = true;
            testInfo.TraceMessage( $"{activity} {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{activity} should succeed" );

            activity = $"Session factory selecting {resourceSettings.ResourceName} selected {factory.ValidatedResourceName}";
            var (success, Details) = factory.TryValidateResource( resourceSettings.ResourceName );
            testInfo.TraceMessage( $"{activity} {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            Assert.IsTrue( success, $"{activity} should succeed: {Details}" );
            Assert.IsTrue( visaView.Talker is object, $"Talker should not be nothing #2" );

            actualBoolean = !string.IsNullOrWhiteSpace( factory.ValidatedResourceName );
            expectedBoolean = true;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"Resource {resourceSettings.ResourceName} not found" );
            Assert.IsTrue( visaView.Talker is object, $"Talker should not be nothing #3" );

            activity = "Factory selected resource";
            string actualResource = factory.ValidatedResourceName;
            string expectedResource = resourceSettings.ResourceName;
            testInfo.TraceMessage( $"{activity} {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            Assert.AreEqual( expectedResource, actualResource, $"{activity} should match" );
        }

        /// <summary>   Assert session resource names should match. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="session">          The session. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertSessionResourceNamesShouldMatch( Pith.SessionBase session, DeviceTests.ResourceSettingsBase resourceSettings )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            int expectedErrorAvailableBits = ( int ) Pith.ServiceRequests.ErrorAvailable;
            int actualErrorAvailableBits = ( int ) session.ErrorAvailableBit;
            Assert.AreEqual( expectedErrorAvailableBits, actualErrorAvailableBits, $"Error available bits on creating device." );

            string actualResource = session.ValidatedResourceName;
            string expectedResource = resourceSettings.ResourceName;
            Assert.AreEqual( expectedResource, actualResource, $"Visa Session validated resource mismatch" );

            actualResource = session.CandidateResourceName;
            expectedResource = resourceSettings.ResourceName;
            Assert.AreEqual( expectedResource, actualResource, $"Visa Session candidate resource mismatch" );

            actualResource = session.OpenResourceName;
            expectedResource = resourceSettings.ResourceName;
            Assert.AreEqual( expectedResource, actualResource, $"Visa Session open resource mismatch" );

            string actualTitle = session.OpenResourceTitle;
            string expectedTitle = resourceSettings.ResourceTitle;
            Assert.AreEqual( expectedTitle, actualTitle, $"Visa Session open title mismatch" );

            actualTitle = session.CandidateResourceTitle;
            expectedTitle = resourceSettings.ResourceTitle;
            Assert.AreEqual( expectedTitle, actualTitle, $"Visa Session candidate resource title mismatch" );
        }

        /// <summary>   Asserts Visa Session Base Should Open. </summary>
        /// <remarks>
        /// The session open overhead is due to the device initialization. Timing: <para>
        /// Ping elapsed 0.194     </para><para>
        /// Ping elapsed 0.000     </para><para>
        /// Test session opened 0.627     </para><para>
        /// Test session Closed 0.013     </para><para>
        /// Session opened 3.803     </para><para>
        /// Session open 6.021     </para><para>
        /// Session checked 0.001     </para><para>
        /// Session closed 0.019     </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="trialNumber">      The trial number. </param>
        /// <param name="visaSessionBase">  The session base. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertVisaSessionBaseShouldOpen( DeviceTests.TestSite testInfo, int trialNumber, VisaSessionBase visaSessionBase, DeviceTests.ResourceSettingsBase resourceSettings )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( visaSessionBase is null )
                throw new ArgumentNullException( nameof( visaSessionBase ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            var sw = Stopwatch.StartNew();
            if ( !resourceSettings.ResourcePinged )
                Assert.Inconclusive( $"{resourceSettings.ResourceTitle} not found" );
            testInfo.TraceMessage( $"Ping elapsed {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            if ( visaSessionBase is null )
                throw new ArgumentNullException( nameof( visaSessionBase ) );
            bool actualBoolean;
            sw.Restart();
            bool expectedBoolean;
            if ( !visaSessionBase.IsDeviceOpen )
            {
                var (Success, Details) = visaSessionBase.TryOpenSession( resourceSettings.ResourceName, resourceSettings.ResourceTitle );
                actualBoolean = Success;
                expectedBoolean = true;
                Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} opening session {resourceSettings.ResourceName} canceled; {Details}" );
            }

            actualBoolean = visaSessionBase.IsSessionOpen;
            expectedBoolean = true;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} opening session {resourceSettings.ResourceName} session not open" );
            testInfo.TraceMessage( $"Session opened {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            actualBoolean = visaSessionBase.IsDeviceOpen;
            expectedBoolean = true;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} opening session {resourceSettings.ResourceName} device not open" );
            AssertModelShouldMatch( visaSessionBase.StatusSubsystemBase, resourceSettings );
            testInfo.AssertMessageQueue();
            if ( visaSessionBase.IsSessionOpen )
                _ = visaSessionBase.StatusSubsystemBase.TryQueryExistingDeviceErrors();
            testInfo.AssertMessageQueue();
        }

        /// <summary>   Assert Visa View session should open. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="trialNumber">      The trial number. </param>
        /// <param name="visaView">         The View control. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertVisaViewSessionShouldOpen( DeviceTests.TestSite testInfo, int trialNumber, Facade.IVisaView visaView, DeviceTests.ResourceSettingsBase resourceSettings )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( visaView is null )
                throw new ArgumentNullException( nameof( visaView ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            if ( !resourceSettings.ResourcePinged )
                Assert.Inconclusive( $"{resourceSettings.ResourceTitle} not found" );
            if ( visaView is null )
                throw new ArgumentNullException( nameof( visaView ) );
            AssertVisaSessionBaseShouldOpen( testInfo, trialNumber, visaView.VisaSessionBase, resourceSettings );
        }

        /// <summary>   Assert session should close. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="trialNumber">      The trial number. </param>
        /// <param name="visaSessionBase">  The session base. </param>
        public static void AssertVisaSessionBaseShouldClose( DeviceTests.TestSite testInfo, int trialNumber, VisaSessionBase visaSessionBase )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( visaSessionBase is null )
                throw new ArgumentNullException( nameof( visaSessionBase ) );
            bool actualBoolean;
            try
            {
                testInfo.AssertMessageQueue();
                if ( visaSessionBase.IsSessionOpen )
                    _ = visaSessionBase.StatusSubsystemBase.TryQueryExistingDeviceErrors();
                testInfo.AssertMessageQueue();
            }
            catch
            {
                throw;
            }
            finally
            {
                _ = visaSessionBase.TryCloseSession();
            }

            Assert.IsFalse( visaSessionBase.IsDeviceOpen, $"{visaSessionBase.ResourceNameCaption} failed closing session" );
            testInfo.AssertMessageQueue();
            actualBoolean = visaSessionBase.IsDeviceOpen;
            bool expectedBoolean = false;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} closing session {visaSessionBase.OpenResourceName} control still connected" );
            actualBoolean = visaSessionBase.IsDeviceOpen;
            expectedBoolean = false;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} closing session {visaSessionBase.OpenResourceName} device still open" );
        }

        /// <summary>   Assert visa view session should close. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">     Information describing the test. </param>
        /// <param name="trialNumber">  The trial number. </param>
        /// <param name="visaView">     The control. </param>
        public static void AssertVisaViewSessionShouldClose( DeviceTests.TestSite testInfo, int trialNumber, Facade.IVisaView visaView )
        {
            if ( visaView is null )
                throw new ArgumentNullException( nameof( visaView ) );
            AssertVisaSessionBaseShouldClose( testInfo, trialNumber, visaView.VisaSessionBase );
        }

        /// <summary>   Assert visa view session should open and close. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="trialNumber">      The trial number. </param>
        /// <param name="visaView">         The control. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertVisaViewSessionShouldOpenAndClose( DeviceTests.TestSite testInfo, int trialNumber, Facade.IVisaView visaView, DeviceTests.ResourceSettingsBase resourceSettings )
        {
            try
            {
                AssertVisaViewSessionShouldOpen( testInfo, trialNumber, visaView, resourceSettings );
            }
            catch
            {
                throw;
            }
            finally
            {
                AssertVisaViewSessionShouldClose( testInfo, trialNumber, visaView );
            }
        }

        /// <summary>   Assert visa view should adds trace messages. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="device">   The device. </param>
        public static void AssertVisaViewShouldAddsTraceMessages( DeviceTests.TestSite testInfo, VisaSessionBase device )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            device.AddListener( testInfo.TraceMessagesQueueListener );
            int messageCount = testInfo.TraceMessagesQueueListener.Count;
            using Facade.IVisaView view = new Facade.VisaTreeView( device );
            // when assigning the device to the view, the listener is assigned.
            _ = Core.ApplianceBase.DoEventsWaitUntil( TimeSpan.FromMilliseconds( 400d ), () => messageCount != testInfo.TraceMessagesQueueListener.Count );
            // this often failed on the first round.
            // Assert.AreNotEqual(messageCount, testInfo.TraceMessagesQueueListener.Count, $"Constructing a device should add trace messages")

            // view.AddListener(TestInfo.TraceMessagesQueueListener)
            AssertVisaViewTraceMessageShouldEmit( view, testInfo.TraceMessagesQueueListener );
        }
    }
}
