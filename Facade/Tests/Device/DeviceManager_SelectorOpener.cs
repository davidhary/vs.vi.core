using System;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.FacadeTests
{
    public sealed partial class DeviceManager
    {

        /// <summary> An opener. </summary>
        private class Opener : Core.Models.OpenerViewModel
        {

            /// <summary> True if is open, false if not. </summary>
            private bool _IsOpen;

            /// <summary> Gets the Open status. </summary>
            /// <value> The Open status. </value>
            public override bool IsOpen => this._IsOpen;

            /// <summary> Opens a resource. </summary>
            /// <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
            /// occurs. </exception>
            /// <param name="resourceName">  The name of the resource. </param>
            /// <param name="resourceTitle"> The resource title. </param>
            public override void OpenResource( string resourceName, string resourceTitle )
            {
                this._IsOpen = true;
                base.OpenResource( resourceName, resourceTitle );
                var e = new System.ComponentModel.CancelEventArgs();
                this.OnOpening( e );
                if ( e.Cancel )
                {
                    throw new OperationCanceledException( $"Opening {resourceTitle}:{resourceName} status canceled;. " );
                }

                this.OnOpened( EventArgs.Empty );
            }

            /// <summary> Closes the resource. </summary>
            public override void CloseResource()
            {
                base.CloseResource();
                this._IsOpen = false;
                this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                this.OnClosed( EventArgs.Empty );
            }

            /// <summary> Gets or sets the active state cleared. </summary>
            /// <value> The active state cleared. </value>
            public bool ActiveStateCleared { get; set; }

            /// <summary> Applies default settings and clears the resource active state. </summary>
            public override void ClearActiveState()
            {
                this.ActiveStateCleared = true;
            }

            /// <summary> Identifies talkers. </summary>
            public override void IdentifyTalkers()
            {
                base.IdentifyTalkers();
                VI.My.MyLibrary.Appliance.Identify( this.Talker );
            }

            /// <summary>
            /// Uses the <see cref="Core.ITalker"/> to publish the message or log if
            /// <see cref="Core.ITalker"/> is nothing.
            /// </summary>
            /// <param name="eventType"> Type of the event. </param>
            /// <param name="activity">  The activity. </param>
            /// <returns> A String. </returns>
            protected override string Publish( TraceEventType eventType, string activity )
            {
                return this.Publish( new Core.TraceMessage( eventType, VI.My.MyLibrary.TraceEventId, activity ) );
            }

            /// <summary> Publish exception. </summary>
            /// <param name="activity"> The activity. </param>
            /// <param name="ex">       The ex. </param>
            /// <returns> A String. </returns>
            protected override string PublishException( string activity, Exception ex )
            {
                return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
            }
        }

        /// <summary>   Assert selector should enumerate resources. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="selectorOpener">   The selector opener. </param>
        /// <param name="selector">         The selector. </param>
        public static void AssertSelectorShouldEnumerateResources( Core.Controls.SelectorOpener selectorOpener, Core.Models.SelectorViewModel selector )
        {
            if ( selectorOpener is null )
                throw new ArgumentNullException( nameof( selectorOpener ) );
            if ( selector is null )
                throw new ArgumentNullException( nameof( selector ) );
            selectorOpener.AssignSelectorViewModel( selector );
            _ = selector.EnumerateResources( false );
            Assert.AreEqual( selector.ResourceNames.Count, selectorOpener.InternalResourceNamesCount, $"Resource names count should match internal resource names count" );
        }

        /// <summary>   Assert resource should open and close. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="selectorOpener">   The control. </param>
        /// <param name="selector">         The selector. </param>
        /// <param name="opener">           The opener. </param>
        public static void AssertResourceShouldOpenAndClose( Core.Controls.SelectorOpener selectorOpener, Core.Models.SelectorViewModel selector, Core.Models.OpenerViewModel opener )
        {
            if ( selectorOpener is null )
                throw new ArgumentNullException( nameof( selectorOpener ) );
            if ( selector is null )
                throw new ArgumentNullException( nameof( selector ) );
            if ( opener is null )
                throw new ArgumentNullException( nameof( opener ) );
            using var form = new System.Windows.Forms.Form();
            form.Controls.Add( selectorOpener );
            form.Show();
            selectorOpener.Visible = true;
            selectorOpener.AssignSelectorViewModel( selector );
            selectorOpener.AssignOpenerViewModel( opener );
            _ = selector.EnumerateResources( true );
            Assert.AreEqual( selector.ResourceNames.Count, selectorOpener.InternalResourceNamesCount, $"Resource names count should match internal resource names count" );
            Assert.IsTrue( selectorOpener.SelectedValueChangeCount > 0, $"Selected value count {selectorOpener.SelectedValueChangeCount} should exceed zero" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( selector.ValidatedResourceName ), "Validated resource name is not empty" );
            Assert.IsTrue( opener.OpenEnabled, $"Opener open enabled after validating the resource" );
            var e = new Core.ActionEventArgs();
            _ = opener.TryOpen( e );
            Assert.IsFalse( e.Failed, $"open failed {e.Details}" );
            Assert.AreEqual( selector.ValidatedResourceName, opener.OpenResourceName, "validated resource name assigned to resource name" );
            Assert.AreEqual( opener.CandidateResourceTitle, opener.OpenResourceTitle, "title assigned to resource title" );
            _ = opener.TryClose( e );
            Assert.IsFalse( e.Failed, $"close failed {e.Details}" );
        }

        /// <summary>   Assert resource should open and close. </summary>
        /// <remarks>   David, 2021-07-06. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertResourceShouldOpenAndClose( DeviceTests.TestSite testInfo, DeviceTests.ResourceSettingsBase resourceSettings )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            using var control = new Core.Controls.SelectorOpener();
            control.AddListener( testInfo.TraceMessagesQueueListener );
            Core.Models.SelectorViewModel selector = new SessionFactory() {
                Searchable = true,
                ResourcesFilter = SessionFactory.Get.Factory.ResourcesProvider().ResourceFinder.BuildMinimalResourcesFilter()
            };
            var opener = new Opener() {
                CandidateResourceTitle = resourceSettings.ResourceTitle,
                CandidateResourceName = resourceSettings.ResourceName
            };
            AssertResourceShouldOpenAndClose( control, selector, opener );
        }
    }
}
