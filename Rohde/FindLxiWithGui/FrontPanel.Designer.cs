﻿namespace RsVISA.NET_FindLxi_WithGUI
{
    partial class Form_FrontPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBox_mDNS = new System.Windows.Forms.CheckBox();
            this.checkBox_VXI11 = new System.Windows.Forms.CheckBox();
            this.FoundResources = new System.Windows.Forms.ListBox();
            this.buttonFind = new System.Windows.Forms.Button();
            this.selector_RsVisa = new System.Windows.Forms.RadioButton();
            this.selector_NIVisa = new System.Windows.Forms.RadioButton();
            this.Group_VISAselector = new System.Windows.Forms.GroupBox();
            this.Group_VISAselector.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox_mDNS
            // 
            this.checkBox_mDNS.AutoSize = true;
            this.checkBox_mDNS.Location = new System.Drawing.Point(354, 89);
            this.checkBox_mDNS.Name = "checkBox_mDNS";
            this.checkBox_mDNS.Size = new System.Drawing.Size(57, 17);
            this.checkBox_mDNS.TabIndex = 2;
            this.checkBox_mDNS.Text = "mDNS";
            this.checkBox_mDNS.UseVisualStyleBackColor = true;
            this.checkBox_mDNS.CheckedChanged += new System.EventHandler(this.checkBox_mDNS_CheckedChanged);
            // 
            // checkBox_VXI11
            // 
            this.checkBox_VXI11.AutoSize = true;
            this.checkBox_VXI11.Location = new System.Drawing.Point(354, 112);
            this.checkBox_VXI11.Name = "checkBox_VXI11";
            this.checkBox_VXI11.Size = new System.Drawing.Size(109, 17);
            this.checkBox_VXI11.TabIndex = 3;
            this.checkBox_VXI11.Text = "VXI-11 Broadcast";
            this.checkBox_VXI11.UseVisualStyleBackColor = true;
            // 
            // FoundResources
            // 
            this.FoundResources.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.FoundResources.FormattingEnabled = true;
            this.FoundResources.Location = new System.Drawing.Point(12, 12);
            this.FoundResources.Name = "FoundResources";
            this.FoundResources.Size = new System.Drawing.Size(325, 342);
            this.FoundResources.TabIndex = 4;
            this.FoundResources.TabStop = false;
            this.FoundResources.SelectedIndexChanged += new System.EventHandler(this.FoundResources_SelectedIndexChanged);
            // 
            // buttonFind
            // 
            this.buttonFind.Location = new System.Drawing.Point(354, 152);
            this.buttonFind.Name = "buttonFind";
            this.buttonFind.Size = new System.Drawing.Size(120, 23);
            this.buttonFind.TabIndex = 5;
            this.buttonFind.Text = "Find";
            this.buttonFind.UseVisualStyleBackColor = true;
            this.buttonFind.Click += new System.EventHandler(this.buttonFind_Click);
            // 
            // selector_RsVisa
            // 
            this.selector_RsVisa.AutoSize = true;
            this.selector_RsVisa.Checked = true;
            this.selector_RsVisa.Location = new System.Drawing.Point(6, 25);
            this.selector_RsVisa.Name = "selector_RsVisa";
            this.selector_RsVisa.Size = new System.Drawing.Size(63, 17);
            this.selector_RsVisa.TabIndex = 6;
            this.selector_RsVisa.TabStop = true;
            this.selector_RsVisa.Text = "RS Visa";
            this.selector_RsVisa.UseVisualStyleBackColor = true;
            this.selector_RsVisa.CheckedChanged += new System.EventHandler(this.VISAselectorChanged);
            // 
            // selector_NIVisa
            // 
            this.selector_NIVisa.AutoSize = true;
            this.selector_NIVisa.Location = new System.Drawing.Point(6, 41);
            this.selector_NIVisa.Name = "selector_NIVisa";
            this.selector_NIVisa.Size = new System.Drawing.Size(59, 17);
            this.selector_NIVisa.TabIndex = 7;
            this.selector_NIVisa.TabStop = true;
            this.selector_NIVisa.Text = "NI Visa";
            this.selector_NIVisa.UseVisualStyleBackColor = true;
            this.selector_NIVisa.CheckedChanged += new System.EventHandler(this.VISAselectorChanged);
            // 
            // Group_VISAselector
            // 
            this.Group_VISAselector.Controls.Add(this.selector_NIVisa);
            this.Group_VISAselector.Controls.Add(this.selector_RsVisa);
            this.Group_VISAselector.Location = new System.Drawing.Point(354, 12);
            this.Group_VISAselector.Name = "Group_VISAselector";
            this.Group_VISAselector.Size = new System.Drawing.Size(120, 64);
            this.Group_VISAselector.TabIndex = 8;
            this.Group_VISAselector.TabStop = false;
            this.Group_VISAselector.Text = "Select VISA";
            // 
            // Form_FrontPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 369);
            this.Controls.Add(this.buttonFind);
            this.Controls.Add(this.FoundResources);
            this.Controls.Add(this.checkBox_VXI11);
            this.Controls.Add(this.checkBox_mDNS);
            this.Controls.Add(this.Group_VISAselector);
            this.MinimumSize = new System.Drawing.Size(502, 300);
            this.Name = "Form_FrontPanel";
            this.Text = "Find Resources Window";
            this.Group_VISAselector.ResumeLayout(false);
            this.Group_VISAselector.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox checkBox_mDNS;
        private System.Windows.Forms.CheckBox checkBox_VXI11;
        private System.Windows.Forms.ListBox FoundResources;
        private System.Windows.Forms.Button buttonFind;
        private System.Windows.Forms.RadioButton selector_RsVisa;
        private System.Windows.Forms.RadioButton selector_NIVisa;
        private System.Windows.Forms.GroupBox Group_VISAselector;
    }
}

