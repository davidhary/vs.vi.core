﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using RohdeSchwarz.Visa;
using Ivi.Visa;

namespace RsVISA.NET_FindLxi_WithGUI
{
    public partial class Form_FrontPanel : Form
    {
        private Assembly _niAssembly;
        public Form_FrontPanel()
        {
            InitializeComponent();

            #pragma warning disable 0618
            _niAssembly = Assembly.LoadWithPartialName("NationalInstruments.Visa");
            #pragma warning restore 0618

            selector_RsVisa.Checked = true;
            selector_NIVisa.Enabled = (_niAssembly != null);
            VISAselectorChanged(selector_RsVisa, null);
        }

        private void FoundResources_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox_mDNS_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void buttonFind_Click(object sender, EventArgs e)
        {
          buttonFind.Enabled = false;
          FoundResources.Items.Clear();
          FoundResources.Items.Add("Searching...");
          IResourceManager rm;
            IEnumerable<string> instruments;
            if (selector_RsVisa.Checked)
            {
              var rsRm = new RohdeSchwarz.Visa.ResourceManager();
              rsRm.SetFindResourceMode(checkBox_VXI11.Checked, checkBox_mDNS.Checked);
              rm = rsRm as IResourceManager;
              instruments = rm.Find("?*");
            }
            else
            {
                // Because the NI VISA might or might not be present, we need to invoke it dynamically with reflections.
                var niRMclass = _niAssembly.GetType("NationalInstruments.Visa.ResourceManager");  
                rm = niRMclass.GetConstructor(Type.EmptyTypes).Invoke(Type.EmptyTypes) as IResourceManager;
                instruments = rm.GetType().GetMethod("Find", new Type[] { typeof(string) }).Invoke(rm, new object[] { "?*" }) as IEnumerable<string>;
            }

            buttonFind.Enabled = true;  
            FoundResources.Items.Clear();
            foreach (var instrument in instruments)
            {
                FoundResources.Items.Add(instrument);
            }
        }

        private void VISAselectorChanged(object sender, EventArgs e)
        {
            if (sender == selector_RsVisa)
            {
                selector_NIVisa.Checked = !selector_RsVisa.Checked;
            }
            else
            {
                selector_RsVisa.Checked = !selector_NIVisa.Checked;
            }

            checkBox_mDNS.Visible = selector_RsVisa.Checked;
            checkBox_VXI11.Visible = selector_RsVisa.Checked;
        }
    }
}
