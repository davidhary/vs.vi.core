// This example uses Manufacturer-independent interface 
// You need to adjust the Resource string to fit your instrument before running the example.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using System.Text;

using Ivi.Visa; //This .NET assembly is installed with IVI.NET Shared Components

namespace Rohde.IdnQuery
{
    /// <summary>
    /// Example of manufacturer-independent interface Ivi.Visa.NET
    /// Before running the example, adjust the ResourceName to fit your instrument
    /// </summary>
    class Program
    {
        private static readonly string ResourceName = Rohde.IdnQuery.Properties.Settings.Default.ResourceName;

        static void Main()
        {

            var framework = Assembly.GetEntryAssembly()?.GetCustomAttribute<TargetFrameworkAttribute>();
            Console.WriteLine( $"Running under {framework.FrameworkName}." );
            Console.WriteLine();

            // Get VISA.NET Shared Components version.
            Version visaNetSharedComponentsVersion = typeof( GlobalResourceManager ).Assembly.GetName().Version;
            Console.WriteLine( $"VISA.NET Shared Components version {visaNetSharedComponentsVersion}." );

            // Check whether VISA Shared Components is installed before using VISA.NET.
            // If access VISA.NET without the visaConfMgr.dll library, an unhandled exception will
            // be thrown during termination process due to a bug in the implementation of the
            // VISA.NET Shared Components, and the application will crash.
            try
            {
                // Get an available version of the VISA Shared Components.
                FileVersionInfo visaSharedComponentsInfo = FileVersionInfo.GetVersionInfo( Path.Combine( Environment.SystemDirectory, "visaConfMgr.dll" ) );
                Console.WriteLine( $"VISA Shared Components version {visaSharedComponentsInfo.ProductVersion} detected." );
            }
            catch ( FileNotFoundException )
            {
                Console.WriteLine( $"VISA implementation compatible with VISA.NET Shared Components {visaNetSharedComponentsVersion} not found. Please install corresponding vendor-specific VISA implementation first." );
                return;
            }

            try
            {

                string filter = "TCPIP?*INSTR";
                Console.WriteLine();
                Console.WriteLine( "Executing GlobalResourceManager.Find( {0} )", "\"" + filter + "\"   " );
                Console.WriteLine();
                System.Collections.Generic.IEnumerable<string> resources = GlobalResourceManager.Find( filter );

            }
            catch ( Exception e )
            {
                Console.WriteLine( "Error finding resources:\n{0}", e.Message );
                Console.WriteLine( "Press any key to finish." );
                _ = Console.ReadKey();
                return;
            }

            IMessageBasedSession io;
            // Separate try-catch for the instrument initialization prevents accessing uninitialized object
            try
            {
                IEnumerable<string> resources = GlobalResourceManager.Find( "TCPIP?*INSTR" );

                // Initialization:
                io = GlobalResourceManager.Open( ResourceName ) as IMessageBasedSession;
                io.Clear();
                // Timeout for VISA Read Operations
                io.TimeoutMilliseconds = 3000;
            }
            catch ( NativeVisaException e )
            {
                Console.WriteLine( "Error initializing the io session:\n{0}", e.Message );
                Console.WriteLine( "Press any key to finish." );
                _ = Console.ReadKey();
                return;
            }

            // try block to catch any InstrumentErrorException()
            try
            {
                Console.WriteLine( "Resource: {0}", ResourceName );
                Console.WriteLine( "IVI GlobalResourceManager selected the following VISA.NET:" );
                Console.WriteLine( "Manufacturer: {0}", io.ResourceManufacturerName );
                Console.WriteLine( "Implementation Version: {0}\n", io.ResourceImplementationVersion );

                io.RawIO.Write( "*RST;*CLS" ); // Reset the instrument, clear the Error queue
                io.RawIO.Write( "*IDN?" );
                var idnResponse = io.RawIO.ReadString();

                Console.WriteLine( "Instrument Identification string:\n{0}", idnResponse );
            }
            catch ( VisaException e )
            {
                Console.WriteLine( "Instrument reports error(s):\n{0}", e.Message );
            }
            finally
            {
                Console.WriteLine( "Press any key to finish." );
                _ = Console.ReadKey();
            }
        }
    }
}
