# VI Core libraries

Virtual Instruments (VI) libraries using the [IVI Foundation] VISA.

* [Runtime Pre-Requisites](#Runtime-Pre-Requisites)
* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Runtime-Pre-Requisites"></a>
## Runtime Pre-Requisites

### .Net Framework 4.7.2
[Microsoft /.NET Framework] must installed before proceeding with this installation.

### IVI Visa
[IVI VISA] 5.11 and above is required for accessing devices.
The IVI VISA implementation can be obtained from either one of the following vendors: 
* Compiled using VISA Shared Components version: 5.12.0

#### Keysight I/O Suite
The [Keysight I/O Suite] version 18.1 and above is recommended.
* Compiled using I/O Library Suite revision: 18.1.26209.5 released 2020-10-15.

#### NI VISA 
[NI VISA] revision 19.0 and above can be used.

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [Core Libraries] - Core Libraries
* [Typed Units Libraries] - Typed Units Project
* [VI Libraries] - VI Core Libraries.

```
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.vi.core.git
git clone git@bitbucket.org:davidhary/Arebis.UnitsAmounts.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\Core\Core
%vslib%\Core\TypedUnits
%vslib%\IO\VI
```
where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions


#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Search and Replace]
* [IVI VISA] - IVI Foundation VISA
* [Keysight] - I/O Libraris
* [Test Script Builder] - Test Script Builder

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix] -- we are but a spec on the shoulders of giants  
* [John Simmons] - outlaw programmer  
* [Stack overflow] - Joel Spolsky  

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries]  
[Core Libraries]  
[Lua Global Support Libraries]

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries]
[IVI VISA]  
[Test Script Builder]  
[Keysight]  



[Core Libraries]: https://bitbucket.org/davidhary/vs.core
[Typed Units Libraries]: https://bitbucket.org/davidhary/Arebis.UnitsAmounts
[VI Libraries]: https://www.bitbucket.org/davidhary/vs.vi.core
[Lua Global Support Libraries]: https://bitbucket.org/davidhary/tsp.core

[IVI Foundation]: https://www.ivifoundation.org
[IVI VISA]: http://www.ivifoundation.org
[Keysight I/O Suite]: https://www.keysight.com/en/pd-1985909/io-libraries-suite
[NI VISA]: https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html#346210
[Test Script Builder]: https://www.tek.com/keithley-test-script-builder
[Microsoft /.NET Framework]: https://dotnet.microsoft.com/download

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm




