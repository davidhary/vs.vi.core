# Register Based Operations

## Category: VISA

## Description:
This example demonstrates how to use In/MoveIn on a PXI session.

## Language: C#  

## Required Software: IVI-VISA  

## Required Hardware: Any message-based device
