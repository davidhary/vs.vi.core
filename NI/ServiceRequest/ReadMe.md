# Service Request

## Category: VISA

## Description:
This example demonstrates how to use the service request event and
the service request status byte to determine when generated data is ready
and how to read it.

## Language: C#  

## Required Software: IVI-VISA  

## Required Hardware: Any message-based device
