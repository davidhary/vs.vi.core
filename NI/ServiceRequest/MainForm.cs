//==================================================================================================
//
// Title      : MainForm.cs
// Purpose    : This application illustrates how to use the service request event and
//              the service request status byte to determine when generated data is ready
//              and how to read it.
//
//==================================================================================================

using System;
using System.Windows.Forms;

using Ivi.Visa;

namespace NationalInstruments.Examples.ServiceRequest
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class MainForm : System.Windows.Forms.Form
    {
        private IMessageBasedSession _Session;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            this.InitializeComponent();
            this.InitializeUI();
            this._ToolTip.SetToolTip( this._EnableSRQButton, "Enable the instrument's SRQ event on MAV by sending the following command (varies by instrument):" );
            this._ToolTip.SetToolTip( this._WriteButton, "Send string to device" );
            this._ToolTip.SetToolTip( this._CloseButton, "Causes the control to release its handle to the device" );
            this._ToolTip.SetToolTip( this._OpenButton, "The resource name of the device is set and the control attempts to connect to the device" );

            try
            {
                // This example uses an instance of the NationalInstruments.Visa.ResourceManager class to find resources on the system.
                // Alternatively, static methods provided by the Ivi.Visa.ResourceManager class may be used when an application
                // requires additional VISA .NET implementations.
                var validResources = Ivi.Visa.GlobalResourceManager.Find( "(GPIB|TCPIP|USB)?*INSTR" );
                foreach ( var resource in validResources )
                {
                    _ = this._ResourceNameComboBox.Items.Add( resource );
                }
            }
            catch ( Exception )
            {
                _ = this._ResourceNameComboBox.Items.Add( "No 488.2 INSTR resource found on the system" );
                this.UpdateResourceNameControls( true );
                this._CloseButton.Enabled = false;
            }
            this._ResourceNameComboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( this._Components != null )
                {
                    this._Components.Dispose();
                }
                if ( this._Session != null )
                {
                    this._Session.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._Components = new System.ComponentModel.Container();
            this._ResourceNameLabel = new System.Windows.Forms.Label();
            this._OpenButton = new System.Windows.Forms.Button();
            this._CloseButton = new System.Windows.Forms.Button();
            this._CommandLabel = new System.Windows.Forms.Label();
            this._CommandTextBox = new System.Windows.Forms.TextBox();
            this._EnableSRQButton = new System.Windows.Forms.Button();
            this._ConfiguringGroupBox = new System.Windows.Forms.GroupBox();
            this._ReadTerminationCharacterNumeric = new System.Windows.Forms.NumericUpDown();
            this._ReadTerminationEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this._ReadTerminationLabel = new System.Windows.Forms.Label();
            this._ResourceNameComboBox = new System.Windows.Forms.ComboBox();
            this._SelectResourceLabel = new System.Windows.Forms.Label();
            this._WritingGroupBox = new System.Windows.Forms.GroupBox();
            this._WriteTextBox = new System.Windows.Forms.TextBox();
            this._WriteButton = new System.Windows.Forms.Button();
            this._ReadingGroupBox = new System.Windows.Forms.GroupBox();
            this._ClearButton = new System.Windows.Forms.Button();
            this._ReadTextBox = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this._Components);
            this._ConfiguringGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ReadTerminationCharacterNumeric)).BeginInit();
            this._WritingGroupBox.SuspendLayout();
            this._ReadingGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _ResourceNameLabel
            // 
            this._ResourceNameLabel.AutoSize = true;
            this._ResourceNameLabel.BackColor = System.Drawing.Color.Transparent;
            this._ResourceNameLabel.Location = new System.Drawing.Point(16, 85);
            this._ResourceNameLabel.Name = "_ResourceNameLabel";
            this._ResourceNameLabel.Size = new System.Drawing.Size(87, 13);
            this._ResourceNameLabel.TabIndex = 1;
            this._ResourceNameLabel.Text = "Resource Name:";
            this._ResourceNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _OpenButton
            // 
            this._OpenButton.Location = new System.Drawing.Point(256, 49);
            this._OpenButton.Name = "_OpenButton";
            this._OpenButton.Size = new System.Drawing.Size(104, 23);
            this._OpenButton.TabIndex = 2;
            this._OpenButton.Text = "Open Session";
            this._OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // _CloseButton
            // 
            this._CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CloseButton.Location = new System.Drawing.Point(255, 76);
            this._CloseButton.Name = "_CloseButton";
            this._CloseButton.Size = new System.Drawing.Size(104, 23);
            this._CloseButton.TabIndex = 3;
            this._CloseButton.Text = "Close Session";
            this._CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // _CommandLabel
            // 
            this._CommandLabel.BackColor = System.Drawing.Color.Transparent;
            this._CommandLabel.Location = new System.Drawing.Point(16, 176);
            this._CommandLabel.Name = "_CommandLabel";
            this._CommandLabel.Size = new System.Drawing.Size(352, 18);
            this._CommandLabel.TabIndex = 4;
            this._CommandLabel.Text = "Type the command to enable the instrument\'s SRQ event on MAV:";
            this._CommandLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _CommandTextBox
            // 
            this._CommandTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._CommandTextBox.Location = new System.Drawing.Point(16, 197);
            this._CommandTextBox.Name = "_CommandTextBox";
            this._CommandTextBox.Size = new System.Drawing.Size(248, 20);
            this._CommandTextBox.TabIndex = 5;
            this._CommandTextBox.Text = "*SRE 16";
            // 
            // _EnableSRQButton
            // 
            this._EnableSRQButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._EnableSRQButton.Location = new System.Drawing.Point(264, 195);
            this._EnableSRQButton.Name = "_EnableSRQButton";
            this._EnableSRQButton.Size = new System.Drawing.Size(104, 24);
            this._EnableSRQButton.TabIndex = 6;
            this._EnableSRQButton.Text = "Enable SRQ";
            this._EnableSRQButton.Click += new System.EventHandler(this.EnableSRQButton_Click);
            // 
            // _ConfiguringGroupBox
            // 
            this._ConfiguringGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ConfiguringGroupBox.Controls.Add(this._ReadTerminationCharacterNumeric);
            this._ConfiguringGroupBox.Controls.Add(this._ReadTerminationEnabledCheckBox);
            this._ConfiguringGroupBox.Controls.Add(this._ReadTerminationLabel);
            this._ConfiguringGroupBox.Controls.Add(this._ResourceNameComboBox);
            this._ConfiguringGroupBox.Controls.Add(this._CloseButton);
            this._ConfiguringGroupBox.Controls.Add(this._OpenButton);
            this._ConfiguringGroupBox.Location = new System.Drawing.Point(8, 61);
            this._ConfiguringGroupBox.Name = "_ConfiguringGroupBox";
            this._ConfiguringGroupBox.Size = new System.Drawing.Size(368, 168);
            this._ConfiguringGroupBox.TabIndex = 7;
            this._ConfiguringGroupBox.TabStop = false;
            this._ConfiguringGroupBox.Text = "Configuring";
            // 
            // _ReadTerminationCharacterNumeric
            // 
            this._ReadTerminationCharacterNumeric.Location = new System.Drawing.Point(137, 49);
            this._ReadTerminationCharacterNumeric.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this._ReadTerminationCharacterNumeric.Name = "_ReadTerminationCharacterNumeric";
            this._ReadTerminationCharacterNumeric.Size = new System.Drawing.Size(40, 20);
            this._ReadTerminationCharacterNumeric.TabIndex = 19;
            this._ReadTerminationCharacterNumeric.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // _ReadTerminationEnabledCheckBox
            // 
            this._ReadTerminationEnabledCheckBox.AutoSize = true;
            this._ReadTerminationEnabledCheckBox.BackColor = System.Drawing.Color.Transparent;
            this._ReadTerminationEnabledCheckBox.Checked = true;
            this._ReadTerminationEnabledCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ReadTerminationEnabledCheckBox.Location = new System.Drawing.Point(138, 76);
            this._ReadTerminationEnabledCheckBox.Name = "_ReadTerminationEnabledCheckBox";
            this._ReadTerminationEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this._ReadTerminationEnabledCheckBox.TabIndex = 18;
            this._ReadTerminationEnabledCheckBox.Text = "&Enabled";
            this._ReadTerminationEnabledCheckBox.UseVisualStyleBackColor = false;
            // 
            // _ReadTerminationLabel
            // 
            this._ReadTerminationLabel.AutoSize = true;
            this._ReadTerminationLabel.BackColor = System.Drawing.Color.Transparent;
            this._ReadTerminationLabel.Location = new System.Drawing.Point(16, 52);
            this._ReadTerminationLabel.Name = "_ReadTerminationLabel";
            this._ReadTerminationLabel.Size = new System.Drawing.Size(118, 13);
            this._ReadTerminationLabel.TabIndex = 17;
            this._ReadTerminationLabel.Text = "Read Termination Byte:";
            // 
            // _ResourceNameComboBox
            // 
            this._ResourceNameComboBox.FormattingEnabled = true;
            this._ResourceNameComboBox.Location = new System.Drawing.Point(98, 17);
            this._ResourceNameComboBox.Name = "_ResourceNameComboBox";
            this._ResourceNameComboBox.Size = new System.Drawing.Size(261, 21);
            this._ResourceNameComboBox.TabIndex = 4;
            // 
            // _SelectResourceLabel
            // 
            this._SelectResourceLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._SelectResourceLabel.Location = new System.Drawing.Point(8, 8);
            this._SelectResourceLabel.Name = "_SelectResourceLabel";
            this._SelectResourceLabel.Size = new System.Drawing.Size(368, 56);
            this._SelectResourceLabel.TabIndex = 8;
            this._SelectResourceLabel.Text = "Select the Resource Name associated with your device and press the Configure Devi" +
    "ce button. Then enter the command string that enables SRQ and click the Enable S" +
    "RQ button.";
            // 
            // _WritingGroupBox
            // 
            this._WritingGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._WritingGroupBox.Controls.Add(this._WriteTextBox);
            this._WritingGroupBox.Controls.Add(this._WriteButton);
            this._WritingGroupBox.Location = new System.Drawing.Point(8, 242);
            this._WritingGroupBox.Name = "_WritingGroupBox";
            this._WritingGroupBox.Size = new System.Drawing.Size(368, 56);
            this._WritingGroupBox.TabIndex = 9;
            this._WritingGroupBox.TabStop = false;
            this._WritingGroupBox.Text = "Writing";
            // 
            // _WriteTextBox
            // 
            this._WriteTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._WriteTextBox.Location = new System.Drawing.Point(8, 24);
            this._WriteTextBox.Name = "_WriteTextBox";
            this._WriteTextBox.Size = new System.Drawing.Size(248, 20);
            this._WriteTextBox.TabIndex = 2;
            this._WriteTextBox.Text = "*IDN?\\n";
            // 
            // _WriteButton
            // 
            this._WriteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._WriteButton.Location = new System.Drawing.Point(256, 23);
            this._WriteButton.Name = "_WriteButton";
            this._WriteButton.Size = new System.Drawing.Size(104, 23);
            this._WriteButton.TabIndex = 1;
            this._WriteButton.Text = "Write";
            this._WriteButton.Click += new System.EventHandler(this.WriteButton_Click);
            // 
            // _ReadingGroupBox
            // 
            this._ReadingGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ReadingGroupBox.Controls.Add(this._ClearButton);
            this._ReadingGroupBox.Controls.Add(this._ReadTextBox);
            this._ReadingGroupBox.Location = new System.Drawing.Point(8, 311);
            this._ReadingGroupBox.Name = "_ReadingGroupBox";
            this._ReadingGroupBox.Size = new System.Drawing.Size(368, 120);
            this._ReadingGroupBox.TabIndex = 10;
            this._ReadingGroupBox.TabStop = false;
            this._ReadingGroupBox.Text = "Reading";
            // 
            // _ClearButton
            // 
            this._ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._ClearButton.Location = new System.Drawing.Point(255, 86);
            this._ClearButton.Name = "_ClearButton";
            this._ClearButton.Size = new System.Drawing.Size(104, 23);
            this._ClearButton.TabIndex = 1;
            this._ClearButton.Text = "Clear";
            this._ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // _ReadTextBox
            // 
            this._ReadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ReadTextBox.Location = new System.Drawing.Point(8, 24);
            this._ReadTextBox.Multiline = true;
            this._ReadTextBox.Name = "_ReadTextBox";
            this._ReadTextBox.ReadOnly = true;
            this._ReadTextBox.Size = new System.Drawing.Size(352, 56);
            this._ReadTextBox.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(384, 448);
            this.Controls.Add(this._ReadingGroupBox);
            this.Controls.Add(this._WritingGroupBox);
            this.Controls.Add(this._SelectResourceLabel);
            this.Controls.Add(this._EnableSRQButton);
            this.Controls.Add(this._CommandTextBox);
            this.Controls.Add(this._CommandLabel);
            this.Controls.Add(this._ResourceNameLabel);
            this.Controls.Add(this._ConfiguringGroupBox);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(296, 482);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Service Request";
            this._ConfiguringGroupBox.ResumeLayout(false);
            this._ConfiguringGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ReadTerminationCharacterNumeric)).EndInit();
            this._WritingGroupBox.ResumeLayout(false);
            this._WritingGroupBox.PerformLayout();
            this._ReadingGroupBox.ResumeLayout(false);
            this._ReadingGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Label _SelectResourceLabel;
        private System.Windows.Forms.GroupBox _ConfiguringGroupBox;
        private System.Windows.Forms.GroupBox _WritingGroupBox;
        private System.Windows.Forms.GroupBox _ReadingGroupBox;
        private System.Windows.Forms.Button _ClearButton;
        private System.Windows.Forms.Label _ResourceNameLabel;
        private System.Windows.Forms.Button _OpenButton;
        private System.Windows.Forms.Button _CloseButton;
        private System.Windows.Forms.TextBox _CommandTextBox;
        private System.Windows.Forms.Label _CommandLabel;
        private System.Windows.Forms.Button _EnableSRQButton;
        private System.Windows.Forms.TextBox _WriteTextBox;
        private System.Windows.Forms.Button _WriteButton;
        private System.Windows.Forms.TextBox _ReadTextBox;
        private System.Windows.Forms.ComboBox _ResourceNameComboBox;
        private System.ComponentModel.IContainer _Components;
        private System.Windows.Forms.NumericUpDown _ReadTerminationCharacterNumeric;
        private System.Windows.Forms.CheckBox _ReadTerminationEnabledCheckBox;
        private System.Windows.Forms.Label _ReadTerminationLabel;

        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.Run( new MainForm() );
        }

        private void UpdateResourceNameControls( bool enable )
        {
            this._ResourceNameComboBox.Enabled = enable;
            this._OpenButton.Enabled = enable;
            this._CloseButton.Enabled = !enable;
            if ( enable )
            {
                _ = this._OpenButton.Focus();
            }
        }

        private void UpdateSRQControls( bool enable )
        {
            this._CommandTextBox.Enabled = enable;
            this._EnableSRQButton.Enabled = enable;
            if ( enable )
            {
                _ = this._EnableSRQButton.Focus();
            }
        }

        private void UpdateWriteControls( bool enable )
        {
            this._WriteTextBox.Enabled = enable;
            this._WriteButton.Enabled = enable;
            if ( enable )
            {
                _ = this._WriteButton.Focus();
            }
        }

        private void InitializeUI()
        {
            this.UpdateResourceNameControls( true );
            this.UpdateSRQControls( false );
            this.UpdateWriteControls( false );
        }


        // When the Open Session button is pressed, the resource name of the
        // device is set and the control attempts to connect to the device
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void OpenButton_Click( object sender, System.EventArgs e )
        {
            try
            {
                this._Session = ( Ivi.Visa.IMessageBasedSession ) Ivi.Visa.GlobalResourceManager.Open( this._ResourceNameComboBox.Text );
                // Use SynchronizeCallbacks to specify that the object marshals callbacks across threads appropriately.
                this._Session.SynchronizeCallbacks = true;
                this._Session.TerminationCharacter = ( byte ) (this._ReadTerminationCharacterNumeric.Value);
                this._Session.TerminationCharacterEnabled = this._ReadTerminationEnabledCheckBox.Checked;

                this.UpdateResourceNameControls( false );
                this.UpdateSRQControls( true );
            }
            catch ( Exception exp )
            {
                _ = MessageBox.Show( exp.Message );
            }
        }


        // The Enable SRQ button writes the string that tells the instrument to
        // enable the SRQ bit
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void EnableSRQButton_Click( object sender, System.EventArgs e )
        {
            string activity = "adding SRQ handler";
            try
            {   // Registering a handler for an event automatically enables that event.

                this._Session.ServiceRequest += this.OnServiceRequest;
                activity = $"writing {this._CommandTextBox.Text}";
                this.WriteToSession( this._CommandTextBox.Text );
                activity = "updating SRQ Controls";
                this.UpdateSRQControls( false );
                activity = "updating Write Controls";
                this.UpdateWriteControls( true );
            }
            catch ( Exception exp )
            {
                _ = MessageBox.Show( $"Exception {activity}: {exp.Message}" );
            }
        }

        // Pressing Close Session causes the control to release its handle to the device
        private void CloseButton_Click( object sender, System.EventArgs e )
        {
            this._Session.ServiceRequest -= this.OnServiceRequest;
            this._Session.Dispose();
            this.InitializeUI();
        }

        // Clicking the Write Button causes the Send String to be written to the device
        private void WriteButton_Click( object sender, System.EventArgs e )
        {
            this.WriteToSession( this._WriteTextBox.Text );
        }

        // Pressing the Clear button clears the read text box
        private void ClearButton_Click( object sender, System.EventArgs e )
        {
            this._ReadTextBox.Clear();
        }

        private string ReplaceCommonEscapeSequences( string s )
        {
            return s.Replace( "\\n", "\n" ).Replace( "\\r", "\r" );
        }

        private string InsertCommonEscapeSequences( string s )
        {
            return s.Replace( "\n", "\\n" ).Replace( "\r", "\\r" );
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void WriteToSession( string txtWrite )
        {
            try
            {
                string textToWrite = this.ReplaceCommonEscapeSequences( txtWrite );
                this._Session.RawIO.Write( textToWrite );
            }
            catch ( Exception exp )
            {
                _ = MessageBox.Show( exp.Message );
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void OnServiceRequest( object sender, VisaEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, VisaEventArgs>( this.OnServiceRequest ), new object[] { sender, e } );
                }
                else
                {
                    var mbs = ( IMessageBasedSession ) sender;
                    StatusByteFlags sb = mbs.ReadStatusByte();

                    if ( (sb & StatusByteFlags.MessageAvailable) != 0 )
                    {
                        string textRead = mbs.RawIO.ReadString();
                        this._ReadTextBox.Text = this.InsertCommonEscapeSequences( textRead );
                    }
                    else
                    {
                        _ = MessageBox.Show( "MAV in status register is not set, which means that message is not available. Make sure the command to enable SRQ is correct, and the instrument is 488.2 compatible." );
                    }
                }
            }
            catch ( Exception exp )
            {
                _ = MessageBox.Show( exp.Message );
            }
        }
    }
}
