using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

using Ivi.Visa;

using Microsoft.VisualBasic;

namespace NI.SimpleReadWrite
{

    /// <summary> Summary description for Form1. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public partial class MainForm : Form
    {

        /// <summary> The last resource string. </summary>
        private string _LastResourceString = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public MainForm()
        {
            // 
            // Required for Windows Form Designer support
            // 
            this.InitializeComponent();
            this.SetupControlState( false );
        }

        /// <summary> Clean up any resources being used. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        ///                          resources; <see langword="false" /> to release only unmanaged
        ///                          resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( this._Session is object )
                {
                    this._Session.Dispose();
                }

                if ( this._Components is object )
                {
                    this._Components.Dispose();
                }
            }

            base.Dispose( disposing );
        }

        #region " OPEN / CLOSE SESSIONS "

        /// <summary> Opens the session. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenSession()
        {
            using var sr = new SelectResource();
            if ( this._LastResourceString is object )
            {
                sr.ResourceName = this._LastResourceString;
            }

            var result = sr.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this._LastResourceString = sr.ResourceName;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this.ResourcesComboBox.DataSource = null;
                    this.ResourcesComboBox.Items.Clear();
                    this.ResourcesComboBox.Text = sr.ResourceName;
                    this._Session = ( IMessageBasedSession ) GlobalResourceManager.Open( sr.ResourceName );
                    // setting to 1000 -- 1000; 1001 -- 1, 4000 -- 10000'
                    this._Session.TerminationCharacter = ( byte ) Math.Round( this.ReadTerminationCharacterNumeric.Value );
                    this._Session.TerminationCharacterEnabled = this.ReadTerminationEnabledCheckBox.Checked;
                    this._Session.TimeoutMilliseconds = ( int ) Math.Round( this.TimeoutNumerc.Value );
                    this.SetupControlState( true );
                }
                catch ( InvalidCastException e1 )
                {
                    _ = MessageBox.Show( $"Resource selected must be a message-based session: {e1}" );
                }
                catch ( Exception exp )
                {
                    _ = MessageBox.Show( exp.Message );
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        /// <summary> Opens the sessions. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenSessions()
        {
            using var sr = new SelectResources();
            var result = sr.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    this._Sessions = new List<IMessageBasedSession>();
                    var names = sr.ResourceNames;
                    this.ResourcesComboBox.DataSource = null;
                    this.ResourcesComboBox.Items.Clear();
                    this.ResourcesComboBox.DataSource = names;
                    var builder = new System.Text.StringBuilder();
                    foreach ( string rs in names )
                    {
                        this._LastResourceString = rs;
                        IMessageBasedSession visaSession = ( IMessageBasedSession ) GlobalResourceManager.Open( rs );
                        // setting to 1000 -- 1000; 1001 -- 1, 4000 -- 10000'
                        visaSession.TimeoutMilliseconds = 1001;
                        this._Sessions.Add( visaSession );
                        int solution = 3;
                        if ( this.UsingTspCheckBox.Checked )
                        {
                            // TSP INITIALIZATION CODE:
                            visaSession.RawIO.Write( "_G.status.request_enable=0\n" );
                            if ( solution == 0 )
                            {
                            }
                            // Error
                            // getting error -286 TSP Runtime error 
                            // instrument reports User Abort Error
                            // Visa reports USRE ABORT
                            else if ( solution == 1 )
                            {
                            }
                            // still getting error -286 TSP Runtime error
                            // but instrument connects and trace shows no error.
                            else if ( solution == 2 )
                            {
                                // no longer getting error -286 TSP Runtime error
                                Thread.Sleep( 11 );
                            }
                            else if ( solution == 3 )
                            {
                                // no longer getting error -286 TSP Runtime error
                                Thread.Sleep( 1 );
                            }

                        }
                        // this is the culprit: 
                        visaSession.Clear();
                        if ( solution == 0 )
                        {
                        }
                        // error 
                        else if ( solution == 1 )
                        {
                            Thread.Sleep( 11 );
                        }
                        else if ( solution == 2 )
                        {
                        }
                        else if ( solution == 3 )
                        {
                        }

                        if ( this.UsingTspCheckBox.Checked )
                            visaSession.RawIO.Write( "_G.waitcomplete() _G.print('1')\n" );
                        _ = builder.AppendLine( InsertCommonEscapeSequences( visaSession.RawIO.ReadString() ) );
                    }

                    this._ReadTextBox.Text = builder.ToString();
                    if ( this._Sessions.Count > 0 )
                    {
                        this._Session = this._Sessions[0];
                        this.ResourcesComboBox.Text = this._Session.ResourceName;
                        this.SetupControlState( this._Session is object );
                    }
                }
                catch ( InvalidCastException e1 )
                {
                    _ = MessageBox.Show( $"Resource selected must be a message-based session: {e1}" );
                }
                catch ( Exception exp )
                {
                    _ = MessageBox.Show( exp.Message );
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        /// <summary> Opens session click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenSession_Click( object sender, EventArgs e )
        {
            if ( this.MultipleResourcesCheckBox.Checked )
            {
                this.OpenSessions();
            }
            else
            {
                this.OpenSession();
            }
        }

        /// <summary> Closes session click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CloseSession_Click( object sender, EventArgs e )
        {
            this.SetupControlState( false );
            if ( this._Sessions is null )
            {
                if ( this._Session is object )
                {
                    this._Session.Dispose();
                }
            }
            else
            {
                var sq = new Queue<IMessageBasedSession>( this._Sessions );
                this._Sessions.Clear();
                while ( sq.Count > 0 )
                {
                    var s = sq.Dequeue();
                    s.Dispose();
                }
            }

            this._Session = null;
        }

        #endregion

        #region " MISC "

        /// <summary> Sets up the control state. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="isSessionOpen"> True if session is open, false if not. </param>
        private void SetupControlState( bool isSessionOpen )
        {
            this.OpenSessionButton.Enabled = !isSessionOpen;
            this.CloseSessionButton.Enabled = isSessionOpen;
            this.QueryButton.Enabled = isSessionOpen;
            this.WriteButton.Enabled = isSessionOpen;
            this.ReadButton.Enabled = isSessionOpen;
            this._WriteTextBox.Enabled = isSessionOpen;
            this.ClearButton.Enabled = isSessionOpen;
            if ( isSessionOpen )
            {
                // _ReadTextBox.Text = String.Empty
                _ = this._WriteTextBox.Focus();
            }
        }

        /// <summary> Replace common escape sequences. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="s"> The string. </param>
        /// <returns> A String. </returns>
        private static string ReplaceCommonEscapeSequences( string s )
        {
            return s.Replace( @"\n", "\n" ).Replace( @"\r", "\r" );
        }

        /// <summary> Inserts a common escape sequences described by s. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="s"> The string. </param>
        /// <returns> A String. </returns>
        private static string InsertCommonEscapeSequences( string s )
        {
            return s.Replace( "\n", @"\n" ).Replace( "\r", @"\r" );
        }

        /// <summary> Multiple resources check box checked changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MultipleResourcesCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.MultipleResourcesCheckBox.Checked )
            {
                this.OpenSessionButton.Text = "Open Sessions";
                this.CloseSessionButton.Text = "Close Sessions";
            }
            else
            {
                this.OpenSessionButton.Text = "Open Session";
                this.CloseSessionButton.Text = "Close Session";
            }
        }

        #endregion

        #region " SESSIONS "

        /// <summary> The session. </summary>
        private IMessageBasedSession _Session;

        /// <summary> The sessions. </summary>
        private List<IMessageBasedSession> _Sessions;

        /// <summary> Resources combo box selected index changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ResourcesComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._Sessions is object && this.ResourcesComboBox.SelectedIndex >= 0 && this._Sessions.Count > this.ResourcesComboBox.SelectedIndex )
            {
                this._Session = this._Sessions[this.ResourcesComboBox.SelectedIndex];
            }
        }

        #endregion

        #region " READ / WRITE "

        /// <summary> Writes. </summary>
        /// <remarks> David, 2021-03-30. </remarks>
        /// <param name="value"> The value to write. </param>
        private void Write( string value )
        {
            this._ReadTextBox.AppendText( "Write: " );
            this._Session.RawIO.Write( ReplaceCommonEscapeSequences( value ) );
            this._ReadTextBox.AppendText( $"{value}{Environment.NewLine}" );
            this._ReadTextBox.SelectionStart = this._ReadTextBox.Text.Length;
        }

        /// <summary> Reads this object. </summary>
        /// <remarks> David, 2021-03-30. </remarks>
        private void Read()
        {
            this._ReadTextBox.AppendText( "Read: " );
            this._ReadTextBox.AppendText( $"{InsertCommonEscapeSequences( this._Session.RawIO.ReadString() )}{Environment.NewLine}" );
            this._ReadTextBox.SelectionStart = this._ReadTextBox.Text.Length;
        }

        /// <summary> Queries a click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Query_Click( object sender, EventArgs e )
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.Write( this._WriteTextBox.Text );
                this.Read();
            }
            catch ( Exception exp )
            {
                this._ReadTextBox.AppendText( $"Error: {exp.Message}{Environment.NewLine}" );
                this._ReadTextBox.SelectionStart = this._ReadTextBox.Text.Length;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Writes a click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Write_Click( object sender, EventArgs e )
        {
            try
            {
                this.Write( this._WriteTextBox.Text );
            }
            catch ( Exception exp )
            {
                this._ReadTextBox.AppendText( $"Error: {exp.Message}{Environment.NewLine}" );
                this._ReadTextBox.SelectionStart = this._ReadTextBox.Text.Length;
                // System.Windows.Forms.MessageBox.Show(exp.ToString)
            }
        }

        /// <summary> Reads a click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Read_Click( object sender, EventArgs e )
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.Read();
            }
            catch ( Exception exp )
            {
                this._ReadTextBox.AppendText( $"Error: {exp.Message}{Environment.NewLine}" );
                this._ReadTextBox.SelectionStart = this._ReadTextBox.Text.Length;
            }
            // System.Windows.Forms.MessageBox.Show(exp.ToString)
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Reads status byte click. </summary>
        /// <remarks> David, 2021-03-30. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ReadStatusByte_Click( object sender, EventArgs e )
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this._ReadTextBox.AppendText( "Status: " );
                this._ReadTextBox.AppendText( $"{( int ) this._Session.ReadStatusByte():X2}{Environment.NewLine}" );
                this._ReadTextBox.SelectionStart = this._ReadTextBox.Text.Length;
            }
            catch ( Exception exp )
            {
                this._ReadTextBox.AppendText( $"Error: {exp.Message}{Environment.NewLine}" );
                this._ReadTextBox.SelectionStart = this._ReadTextBox.Text.Length;
            }
            // System.Windows.Forms.MessageBox.Show(exp.ToString)
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Clears the click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Clear_Click( object sender, EventArgs e )
        {
            this._ReadTextBox.Text = string.Empty;
        }

        private void TimeoutNumerc_ValueChanged( object sender, EventArgs e )
        {
            if ( this._Session is object )
            {
                this._Session.TimeoutMilliseconds = ( int ) Math.Round( this.TimeoutNumerc.Value );
            }
        }

        private void ReadTerminationCharacter_ValueChanged( object sender, EventArgs e )
        {
            if ( this._Session is object )
            {
                this._Session.TerminationCharacter = ( byte ) Math.Round( this.ReadTerminationCharacterNumeric.Value );
                this._Session.TerminationCharacterEnabled = this.ReadTerminationEnabledCheckBox.Checked;
            }
        }

        #endregion


    }
}
