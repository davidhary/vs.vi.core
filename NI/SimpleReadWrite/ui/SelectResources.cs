using System;
using System.Collections.Generic;

using Ivi.Visa;

namespace NI.SimpleReadWrite
{

    /// <summary> A select resources. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public partial class SelectResources
    {
        public SelectResources()
        {
            this.InitializeComponent();
            this._AvailableResourcesListBox.Name = "_AvailableResourcesListBox";
            this._ResourceStringsListBox.Name = "_ResourceStringsListBox";
            this._AddButton.Name = "_AddButton";
            this._RemoveButton.Name = "_RemoveButton";
        }

        /// <summary> Event handler. Called by MyBase for load events. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        private void OnLoad( object sender, EventArgs e )
        {
            // This example uses an instance of the NationalInstruments.Visa.ResourceManager class to find resources on the system.
            // Alternatively, static methods provided by the Ivi.Visa.ResourceManager class may be used when an application
            // requires additional VISA .NET implementations.
            var resources = GlobalResourceManager.Find( "(ASRL|GPIB|TCPIP|USB)?*" );
            foreach ( string s in resources )
                _ = this.AvailableResourcesListBox.Items.Add( s );
        }

        /// <summary> Adds a button click to 'e'. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AddButton_Click( object sender, EventArgs e )
        {
            string selectedString = this.AvailableResourcesListBox.SelectedItem.ToString();
            _ = this.ResourceStringsListBox.Items.Add( selectedString );
        }

        /// <summary> Removes the button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RemoveButton_Click( object sender, EventArgs e )
        {
            this.ResourceStringsListBox.Items.Remove( this.ResourceStringsListBox.SelectedItem );
        }

        /// <summary> Gets a list of names of the resources. </summary>
        /// <value> A list of names of the resources. </value>
        public IEnumerable<string> ResourceNames
        {
            get {
                var l = new List<string>();
                foreach ( object item in this.ResourceStringsListBox.Items )
                    l.Add( item.ToString() );
                return l.ToArray();
            }
        }
    }
}
