using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace NI.SimpleReadWrite
{

    public partial class SelectResources : Form
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _ResourceStringsListBoxLabel = new Label();
            _AvailableResourcesListBoxLabel = new Label();
            CloseButton = new Button();
            OkButton = new Button();
            _AvailableResourcesListBox = new ListBox();
            _AvailableResourcesListBox.DoubleClick += new EventHandler(AddButton_Click);
            _ResourceStringsListBox = new ListBox();
            _ResourceStringsListBox.DoubleClick += new EventHandler(RemoveButton_Click);
            _AddButton = new Button();
            _AddButton.Click += new EventHandler(AddButton_Click);
            _RemoveButton = new Button();
            _RemoveButton.Click += new EventHandler(RemoveButton_Click);
            SuspendLayout();
            // 
            // _ResourceStringsListBoxLabel
            // 
            _ResourceStringsListBoxLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _ResourceStringsListBoxLabel.AutoSize = true;
            _ResourceStringsListBoxLabel.Location = new Point(1, 163);
            _ResourceStringsListBoxLabel.Name = "_ResourceStringsListBoxLabel";
            _ResourceStringsListBoxLabel.Size = new Size(91, 13);
            _ResourceStringsListBoxLabel.TabIndex = 12;
            _ResourceStringsListBoxLabel.Text = "Resource Strings:";
            // 
            // _AvailableResourcesListBoxLabel
            // 
            _AvailableResourcesListBoxLabel.AutoSize = true;
            _AvailableResourcesListBoxLabel.Location = new Point(1, 4);
            _AvailableResourcesListBoxLabel.Name = "_AvailableResourcesListBoxLabel";
            _AvailableResourcesListBoxLabel.Size = new Size(107, 13);
            _AvailableResourcesListBoxLabel.TabIndex = 11;
            _AvailableResourcesListBoxLabel.Text = "Available Resources:";
            // 
            // closeButton
            // 
            CloseButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            CloseButton.DialogResult = DialogResult.Cancel;
            CloseButton.Location = new Point(213, 267);
            CloseButton.Name = "closeButton";
            CloseButton.Size = new Size(66, 25);
            CloseButton.TabIndex = 9;
            CloseButton.Text = "Cancel";
            // 
            // okButton
            // 
            OkButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            OkButton.DialogResult = DialogResult.OK;
            OkButton.Location = new Point(143, 267);
            OkButton.Name = "okButton";
            OkButton.Size = new Size(66, 25);
            OkButton.TabIndex = 8;
            OkButton.Text = "OK";
            // 
            // _AvailableResourcesListBox
            // 
            _AvailableResourcesListBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            _AvailableResourcesListBox.Location = new Point(4, 17);
            _AvailableResourcesListBox.Name = "__AvailableResourcesListBox";
            _AvailableResourcesListBox.Size = new Size(278, 134);
            _AvailableResourcesListBox.TabIndex = 7;
            // 
            // _ResourceStringsListBox
            // 
            _ResourceStringsListBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            _ResourceStringsListBox.FormattingEnabled = true;
            _ResourceStringsListBox.Location = new Point(4, 179);
            _ResourceStringsListBox.Name = "__ResourceStringsListBox";
            _ResourceStringsListBox.Size = new Size(278, 82);
            _ResourceStringsListBox.TabIndex = 13;
            // 
            // _AddButton
            // 
            _AddButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _AddButton.Location = new Point(4, 267);
            _AddButton.Name = "__AddButton";
            _AddButton.Size = new Size(47, 25);
            _AddButton.TabIndex = 14;
            _AddButton.Text = "Add";
            _AddButton.UseVisualStyleBackColor = true;
            // 
            // _RemoveButton
            // 
            _RemoveButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _RemoveButton.Location = new Point(55, 267);
            _RemoveButton.Name = "__RemoveButton";
            _RemoveButton.Size = new Size(61, 25);
            _RemoveButton.TabIndex = 15;
            _RemoveButton.Text = "Remove";
            _RemoveButton.UseVisualStyleBackColor = true;
            // 
            // SelectResources
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(284, 297);
            Controls.Add(_RemoveButton);
            Controls.Add(_AddButton);
            Controls.Add(_ResourceStringsListBox);
            Controls.Add(_ResourceStringsListBoxLabel);
            Controls.Add(_AvailableResourcesListBoxLabel);
            Controls.Add(CloseButton);
            Controls.Add(OkButton);
            Controls.Add(_AvailableResourcesListBox);
            Name = "SelectResources";
            Text = "Select Resources";
            Load += new EventHandler(OnLoad);
            ResumeLayout(false);
            PerformLayout();
        }

        private Label _ResourceStringsListBoxLabel;
        private Label _AvailableResourcesListBoxLabel;
        private Button CloseButton;
        private Button OkButton;
        private ListBox _AvailableResourcesListBox;

        private ListBox AvailableResourcesListBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _AvailableResourcesListBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_AvailableResourcesListBox != null)
                {
                    _AvailableResourcesListBox.DoubleClick -= AddButton_Click;
                }

                _AvailableResourcesListBox = value;
                if (_AvailableResourcesListBox != null)
                {
                    _AvailableResourcesListBox.DoubleClick += AddButton_Click;
                }
            }
        }

        private ListBox _ResourceStringsListBox;

        private ListBox ResourceStringsListBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ResourceStringsListBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ResourceStringsListBox != null)
                {
                    _ResourceStringsListBox.DoubleClick -= RemoveButton_Click;
                }

                _ResourceStringsListBox = value;
                if (_ResourceStringsListBox != null)
                {
                    _ResourceStringsListBox.DoubleClick += RemoveButton_Click;
                }
            }
        }

        private Button _AddButton;

        private Button AddButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _AddButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_AddButton != null)
                {
                    _AddButton.Click -= AddButton_Click;
                }

                _AddButton = value;
                if (_AddButton != null)
                {
                    _AddButton.Click += AddButton_Click;
                }
            }
        }

        private Button _RemoveButton;

        private Button RemoveButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _RemoveButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_RemoveButton != null)
                {
                    _RemoveButton.Click -= RemoveButton_Click;
                }

                _RemoveButton = value;
                if (_RemoveButton != null)
                {
                    _RemoveButton.Click += RemoveButton_Click;
                }
            }
        }
    }
}
