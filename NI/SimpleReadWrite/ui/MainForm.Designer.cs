using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace NI.SimpleReadWrite
{
    public partial class MainForm
    {
        private TextBox _WriteTextBox;
        private TextBox _ReadTextBox;
        private Button _QueryButton;

        private Button QueryButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _QueryButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_QueryButton != null)
                {
                    _QueryButton.Click -= Query_Click;
                }

                _QueryButton = value;
                if (_QueryButton != null)
                {
                    _QueryButton.Click += Query_Click;
                }
            }
        }

        private Button _WriteButton;

        private Button WriteButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _WriteButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_WriteButton != null)
                {
                    _WriteButton.Click -= Write_Click;
                }

                _WriteButton = value;
                if (_WriteButton != null)
                {
                    _WriteButton.Click += Write_Click;
                }
            }
        }

        private Button _ReadButton;

        private Button ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ReadButton != null)
                {
                    _ReadButton.Click -= Read_Click;
                }

                _ReadButton = value;
                if (_ReadButton != null)
                {
                    _ReadButton.Click += Read_Click;
                }
            }
        }

        private Button _OpenSessionButton;

        private Button OpenSessionButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _OpenSessionButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_OpenSessionButton != null)
                {
                    _OpenSessionButton.Click -= OpenSession_Click;
                }

                _OpenSessionButton = value;
                if (_OpenSessionButton != null)
                {
                    _OpenSessionButton.Click += OpenSession_Click;
                }
            }
        }

        private Button _ClearButton;

        private Button ClearButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ClearButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ClearButton != null)
                {
                    _ClearButton.Click -= Clear_Click;
                }

                _ClearButton = value;
                if (_ClearButton != null)
                {
                    _ClearButton.Click += Clear_Click;
                }
            }
        }

        private Button _CloseSessionButton;

        private Button CloseSessionButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _CloseSessionButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_CloseSessionButton != null)
                {
                    _CloseSessionButton.Click -= CloseSession_Click;
                }

                _CloseSessionButton = value;
                if (_CloseSessionButton != null)
                {
                    _CloseSessionButton.Click += CloseSession_Click;
                }
            }
        }

        private Label _WraiteTextBoxLabel;
        private Label _ReadTextBoxLabel;
        private ComboBox _ResourcesComboBox;

        private ComboBox ResourcesComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ResourcesComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ResourcesComboBox != null)
                {
                    _ResourcesComboBox.SelectedIndexChanged -= ResourcesComboBox_SelectedIndexChanged;
                }

                _ResourcesComboBox = value;
                if (_ResourcesComboBox != null)
                {
                    _ResourcesComboBox.SelectedIndexChanged += ResourcesComboBox_SelectedIndexChanged;
                }
            }
        }

        private Label ResourcesComboBoxLabel;
        private CheckBox _MultipleResourcesCheckBox;

        private CheckBox MultipleResourcesCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _MultipleResourcesCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_MultipleResourcesCheckBox != null)
                {
                    _MultipleResourcesCheckBox.CheckedChanged -= MultipleResourcesCheckBox_CheckedChanged;
                }

                _MultipleResourcesCheckBox = value;
                if (_MultipleResourcesCheckBox != null)
                {
                    _MultipleResourcesCheckBox.CheckedChanged += MultipleResourcesCheckBox_CheckedChanged;
                }
            }
        }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container _Components = null;

        #region "Windows Form Designer generated code"

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this._QueryButton = new System.Windows.Forms.Button();
            this._WriteButton = new System.Windows.Forms.Button();
            this._ReadButton = new System.Windows.Forms.Button();
            this._OpenSessionButton = new System.Windows.Forms.Button();
            this._WriteTextBox = new System.Windows.Forms.TextBox();
            this._ReadTextBox = new System.Windows.Forms.TextBox();
            this._ClearButton = new System.Windows.Forms.Button();
            this._CloseSessionButton = new System.Windows.Forms.Button();
            this._WraiteTextBoxLabel = new System.Windows.Forms.Label();
            this._ReadTextBoxLabel = new System.Windows.Forms.Label();
            this._ResourcesComboBox = new System.Windows.Forms.ComboBox();
            this.ResourcesComboBoxLabel = new System.Windows.Forms.Label();
            this._MultipleResourcesCheckBox = new System.Windows.Forms.CheckBox();
            this._ReadStatusByte = new System.Windows.Forms.Button();
            this.ReadTerminationLabel = new System.Windows.Forms.Label();
            this._ReadTerminationEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this._ReadTerminationCharacterNumeric = new System.Windows.Forms.NumericUpDown();
            this._TimeoutNumerc = new System.Windows.Forms.NumericUpDown();
            this.TimeoutNumericLabel = new System.Windows.Forms.Label();
            this.UsingTspCheckBox = new System.Windows.Forms.CheckBox();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._ReadTerminationCharacterNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._TimeoutNumerc)).BeginInit();
            this.SuspendLayout();
            // 
            // _QueryButton
            // 
            this._QueryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._QueryButton.Location = new System.Drawing.Point(219, 147);
            this._QueryButton.Name = "_QueryButton";
            this._QueryButton.Size = new System.Drawing.Size(48, 23);
            this._QueryButton.TabIndex = 3;
            this._QueryButton.Text = "&Query";
            this._QueryButton.Click += new System.EventHandler(this.Query_Click);
            // 
            // _WriteButton
            // 
            this._WriteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._WriteButton.Location = new System.Drawing.Point(274, 147);
            this._WriteButton.Name = "_WriteButton";
            this._WriteButton.Size = new System.Drawing.Size(48, 23);
            this._WriteButton.TabIndex = 4;
            this._WriteButton.Text = "&Write";
            this._WriteButton.Click += new System.EventHandler(this.Write_Click);
            // 
            // _ReadButton
            // 
            this._ReadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._ReadButton.Location = new System.Drawing.Point(327, 147);
            this._ReadButton.Name = "_ReadButton";
            this._ReadButton.Size = new System.Drawing.Size(48, 23);
            this._ReadButton.TabIndex = 5;
            this._ReadButton.Text = "&Read";
            this._ReadButton.Click += new System.EventHandler(this.Read_Click);
            // 
            // _OpenSessionButton
            // 
            this._OpenSessionButton.Location = new System.Drawing.Point(5, 5);
            this._OpenSessionButton.Name = "_OpenSessionButton";
            this._OpenSessionButton.Size = new System.Drawing.Size(92, 22);
            this._OpenSessionButton.TabIndex = 0;
            this._OpenSessionButton.Text = "Open Session";
            this._OpenSessionButton.Click += new System.EventHandler(this.OpenSession_Click);
            // 
            // _WriteTextBox
            // 
            this._WriteTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._WriteTextBox.Location = new System.Drawing.Point(5, 118);
            this._WriteTextBox.Name = "_WriteTextBox";
            this._WriteTextBox.Size = new System.Drawing.Size(368, 20);
            this._WriteTextBox.TabIndex = 2;
            this._WriteTextBox.Text = "*IDN?\\n";
            // 
            // _ReadTextBox
            // 
            this._ReadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ReadTextBox.Location = new System.Drawing.Point(5, 178);
            this._ReadTextBox.Multiline = true;
            this._ReadTextBox.Name = "_ReadTextBox";
            this._ReadTextBox.ReadOnly = true;
            this._ReadTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._ReadTextBox.Size = new System.Drawing.Size(368, 191);
            this._ReadTextBox.TabIndex = 6;
            this._ReadTextBox.TabStop = false;
            // 
            // _ClearButton
            // 
            this._ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ClearButton.Location = new System.Drawing.Point(8, 375);
            this._ClearButton.Name = "_ClearButton";
            this._ClearButton.Size = new System.Drawing.Size(367, 24);
            this._ClearButton.TabIndex = 7;
            this._ClearButton.Text = "Clear";
            this._ClearButton.Click += new System.EventHandler(this.Clear_Click);
            // 
            // _CloseSessionButton
            // 
            this._CloseSessionButton.Location = new System.Drawing.Point(97, 5);
            this._CloseSessionButton.Name = "_CloseSessionButton";
            this._CloseSessionButton.Size = new System.Drawing.Size(92, 22);
            this._CloseSessionButton.TabIndex = 1;
            this._CloseSessionButton.Text = "Close Session";
            this._CloseSessionButton.Click += new System.EventHandler(this.CloseSession_Click);
            // 
            // _WraiteTextBoxLabel
            // 
            this._WraiteTextBoxLabel.AutoSize = true;
            this._WraiteTextBoxLabel.Location = new System.Drawing.Point(5, 102);
            this._WraiteTextBoxLabel.Name = "_WraiteTextBoxLabel";
            this._WraiteTextBoxLabel.Size = new System.Drawing.Size(77, 13);
            this._WraiteTextBoxLabel.TabIndex = 8;
            this._WraiteTextBoxLabel.Text = "String to Write:";
            // 
            // _ReadTextBoxLabel
            // 
            this._ReadTextBoxLabel.AutoSize = true;
            this._ReadTextBoxLabel.Location = new System.Drawing.Point(5, 158);
            this._ReadTextBoxLabel.Name = "_ReadTextBoxLabel";
            this._ReadTextBoxLabel.Size = new System.Drawing.Size(56, 13);
            this._ReadTextBoxLabel.TabIndex = 9;
            this._ReadTextBoxLabel.Text = "Received:";
            // 
            // _ResourcesComboBox
            // 
            this._ResourcesComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ResourcesComboBox.FormattingEnabled = true;
            this._ResourcesComboBox.Location = new System.Drawing.Point(5, 73);
            this._ResourcesComboBox.Name = "_ResourcesComboBox";
            this._ResourcesComboBox.Size = new System.Drawing.Size(367, 21);
            this._ResourcesComboBox.TabIndex = 10;
            this._ResourcesComboBox.SelectedIndexChanged += new System.EventHandler(this.ResourcesComboBox_SelectedIndexChanged);
            // 
            // ResourcesComboBoxLabel
            // 
            this.ResourcesComboBoxLabel.AutoSize = true;
            this.ResourcesComboBoxLabel.Location = new System.Drawing.Point(5, 57);
            this.ResourcesComboBoxLabel.Name = "ResourcesComboBoxLabel";
            this.ResourcesComboBoxLabel.Size = new System.Drawing.Size(92, 13);
            this.ResourcesComboBoxLabel.TabIndex = 11;
            this.ResourcesComboBoxLabel.Text = "Resource Names:";
            // 
            // _MultipleResourcesCheckBox
            // 
            this._MultipleResourcesCheckBox.AutoSize = true;
            this._MultipleResourcesCheckBox.Location = new System.Drawing.Point(195, 8);
            this._MultipleResourcesCheckBox.Name = "_MultipleResourcesCheckBox";
            this._MultipleResourcesCheckBox.Size = new System.Drawing.Size(62, 17);
            this._MultipleResourcesCheckBox.TabIndex = 12;
            this._MultipleResourcesCheckBox.Text = "Multiple";
            this.ToolTip.SetToolTip(this._MultipleResourcesCheckBox, "Check if opening multiple resources");
            this._MultipleResourcesCheckBox.UseVisualStyleBackColor = true;
            this._MultipleResourcesCheckBox.CheckedChanged += new System.EventHandler(this.MultipleResourcesCheckBox_CheckedChanged);
            // 
            // _ReadStatusByte
            // 
            this._ReadStatusByte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._ReadStatusByte.Location = new System.Drawing.Point(166, 147);
            this._ReadStatusByte.Name = "_ReadStatusByte";
            this._ReadStatusByte.Size = new System.Drawing.Size(48, 23);
            this._ReadStatusByte.TabIndex = 13;
            this._ReadStatusByte.Text = "&STB";
            this.ToolTip.SetToolTip(this._ReadStatusByte, "Read the status byte");
            this._ReadStatusByte.UseVisualStyleBackColor = true;
            this._ReadStatusByte.Click += new System.EventHandler(this.ReadStatusByte_Click);
            // 
            // ReadTerminationLabel
            // 
            this.ReadTerminationLabel.AutoSize = true;
            this.ReadTerminationLabel.Location = new System.Drawing.Point(5, 37);
            this.ReadTerminationLabel.Name = "ReadTerminationLabel";
            this.ReadTerminationLabel.Size = new System.Drawing.Size(118, 13);
            this.ReadTerminationLabel.TabIndex = 14;
            this.ReadTerminationLabel.Text = "Read Termination Byte:";
            // 
            // _ReadTerminationEnabledCheckBox
            // 
            this._ReadTerminationEnabledCheckBox.AutoSize = true;
            this._ReadTerminationEnabledCheckBox.Checked = true;
            this._ReadTerminationEnabledCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ReadTerminationEnabledCheckBox.Location = new System.Drawing.Point(169, 35);
            this._ReadTerminationEnabledCheckBox.Name = "_ReadTerminationEnabledCheckBox";
            this._ReadTerminationEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this._ReadTerminationEnabledCheckBox.TabIndex = 15;
            this._ReadTerminationEnabledCheckBox.Text = "&Enabled";
            this._ReadTerminationEnabledCheckBox.UseVisualStyleBackColor = true;
            this._ReadTerminationEnabledCheckBox.CheckedChanged += new System.EventHandler(this.ReadTerminationCharacter_ValueChanged);
            // 
            // _ReadTerminationCharacterNumeric
            // 
            this._ReadTerminationCharacterNumeric.Location = new System.Drawing.Point(126, 33);
            this._ReadTerminationCharacterNumeric.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this._ReadTerminationCharacterNumeric.Name = "_ReadTerminationCharacterNumeric";
            this._ReadTerminationCharacterNumeric.Size = new System.Drawing.Size(40, 20);
            this._ReadTerminationCharacterNumeric.TabIndex = 16;
            this._ReadTerminationCharacterNumeric.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this._ReadTerminationCharacterNumeric.ValueChanged += new System.EventHandler(this.ReadTerminationCharacter_ValueChanged);
            // 
            // _TimeoutNumerc
            // 
            this._TimeoutNumerc.Location = new System.Drawing.Point(286, 33);
            this._TimeoutNumerc.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this._TimeoutNumerc.Name = "_TimeoutNumerc";
            this._TimeoutNumerc.Size = new System.Drawing.Size(51, 20);
            this._TimeoutNumerc.TabIndex = 17;
            this._TimeoutNumerc.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this._TimeoutNumerc.ValueChanged += new System.EventHandler(this.TimeoutNumerc_ValueChanged);
            // 
            // TimeoutNumericLabel
            // 
            this.TimeoutNumericLabel.AutoSize = true;
            this.TimeoutNumericLabel.Location = new System.Drawing.Point(238, 37);
            this.TimeoutNumericLabel.Name = "TimeoutNumericLabel";
            this.TimeoutNumericLabel.Size = new System.Drawing.Size(46, 13);
            this.TimeoutNumericLabel.TabIndex = 18;
            this.TimeoutNumericLabel.Text = "T/O ms:";
            // 
            // UsingTspCheckBox
            // 
            this.UsingTspCheckBox.AutoSize = true;
            this.UsingTspCheckBox.Location = new System.Drawing.Point(288, 8);
            this.UsingTspCheckBox.Name = "UsingTspCheckBox";
            this.UsingTspCheckBox.Size = new System.Drawing.Size(47, 17);
            this.UsingTspCheckBox.TabIndex = 19;
            this.UsingTspCheckBox.Text = "TSP";
            this.ToolTip.SetToolTip(this.UsingTspCheckBox, "Check if using a TSP instrument");
            this.UsingTspCheckBox.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(378, 405);
            this.Controls.Add(this.UsingTspCheckBox);
            this.Controls.Add(this.TimeoutNumericLabel);
            this.Controls.Add(this._TimeoutNumerc);
            this.Controls.Add(this._ReadTerminationCharacterNumeric);
            this.Controls.Add(this._ReadTerminationEnabledCheckBox);
            this.Controls.Add(this.ReadTerminationLabel);
            this.Controls.Add(this._ReadStatusByte);
            this.Controls.Add(this._MultipleResourcesCheckBox);
            this.Controls.Add(this.ResourcesComboBoxLabel);
            this.Controls.Add(this._ResourcesComboBox);
            this.Controls.Add(this._ReadTextBoxLabel);
            this.Controls.Add(this._WraiteTextBoxLabel);
            this.Controls.Add(this._CloseSessionButton);
            this.Controls.Add(this._ClearButton);
            this.Controls.Add(this._ReadTextBox);
            this.Controls.Add(this._WriteTextBox);
            this.Controls.Add(this._OpenSessionButton);
            this.Controls.Add(this._ReadButton);
            this.Controls.Add(this._WriteButton);
            this.Controls.Add(this._QueryButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(295, 316);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simple Read/Write";
            ((System.ComponentModel.ISupportInitialize)(this._ReadTerminationCharacterNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._TimeoutNumerc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private Button _ReadStatusByte;

        internal Button ReadStatusByte
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ReadStatusByte;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ReadStatusByte != null)
                {
                    _ReadStatusByte.Click -= ReadStatusByte_Click;
                }

                _ReadStatusByte = value;
                if (_ReadStatusByte != null)
                {
                    _ReadStatusByte.Click += ReadStatusByte_Click;
                }
            }
        }

        private Label ReadTerminationLabel;
        private CheckBox _ReadTerminationEnabledCheckBox;

        private CheckBox ReadTerminationEnabledCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ReadTerminationEnabledCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ReadTerminationEnabledCheckBox != null)
                {
                    _ReadTerminationEnabledCheckBox.CheckedChanged -= ReadTerminationCharacter_ValueChanged;
                }

                _ReadTerminationEnabledCheckBox = value;
                if (_ReadTerminationEnabledCheckBox != null)
                {
                    _ReadTerminationEnabledCheckBox.CheckedChanged += ReadTerminationCharacter_ValueChanged;
                }
            }
        }

        private NumericUpDown _ReadTerminationCharacterNumeric;

        private NumericUpDown ReadTerminationCharacterNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _ReadTerminationCharacterNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_ReadTerminationCharacterNumeric != null)
                {
                    _ReadTerminationCharacterNumeric.ValueChanged -= ReadTerminationCharacter_ValueChanged;
                }

                _ReadTerminationCharacterNumeric = value;
                if (_ReadTerminationCharacterNumeric != null)
                {
                    _ReadTerminationCharacterNumeric.ValueChanged += ReadTerminationCharacter_ValueChanged;
                }
            }
        }

        private NumericUpDown _TimeoutNumerc;

        private NumericUpDown TimeoutNumerc
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _TimeoutNumerc;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_TimeoutNumerc != null)
                {
                    _TimeoutNumerc.ValueChanged -= TimeoutNumerc_ValueChanged;
                }

                _TimeoutNumerc = value;
                if (_TimeoutNumerc != null)
                {
                    _TimeoutNumerc.ValueChanged += TimeoutNumerc_ValueChanged;
                }
            }
        }

        private Label TimeoutNumericLabel;
        #endregion

        private ToolTip ToolTip;
        private System.ComponentModel.IContainer components;
        private CheckBox UsingTspCheckBox;
    }
}
