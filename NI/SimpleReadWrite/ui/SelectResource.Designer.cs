using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace NI.SimpleReadWrite
{
    public partial class SelectResource
    {
        private ListBox _AvailableResourcesListBox;

        private ListBox AvailableResourcesListBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _AvailableResourcesListBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_AvailableResourcesListBox != null)
                {
                    _AvailableResourcesListBox.DoubleClick -= AvailableResourcesListBox_DoubleClick;
                    _AvailableResourcesListBox.SelectedIndexChanged -= AvailableResourcesListBox_SelectedIndexChanged;
                }

                _AvailableResourcesListBox = value;
                if (_AvailableResourcesListBox != null)
                {
                    _AvailableResourcesListBox.DoubleClick += AvailableResourcesListBox_DoubleClick;
                    _AvailableResourcesListBox.SelectedIndexChanged += AvailableResourcesListBox_SelectedIndexChanged;
                }
            }
        }

        private Button okButton;
        private Button closeButton;
        private TextBox visaResourceNameTextBox;
        private Label AvailableResourcesLabel;
        private Label ResourceStringLabel;

        #region "Windows Form Designer generated code"

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.Resources.ResourceManager(typeof(SelectResource));
            _AvailableResourcesListBox = new ListBox();
            _AvailableResourcesListBox.DoubleClick += new EventHandler(AvailableResourcesListBox_DoubleClick);
            _AvailableResourcesListBox.SelectedIndexChanged += new EventHandler(AvailableResourcesListBox_SelectedIndexChanged);
            okButton = new Button();
            closeButton = new Button();
            visaResourceNameTextBox = new TextBox();
            AvailableResourcesLabel = new Label();
            ResourceStringLabel = new Label();
            SuspendLayout();
            // 
            // availableResourcesListBox
            // 
            _AvailableResourcesListBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _AvailableResourcesListBox.Location = new Point(5, 18);
            _AvailableResourcesListBox.Name = "_availableResourcesListBox";
            _AvailableResourcesListBox.Size = new Size(282, 108);
            _AvailableResourcesListBox.TabIndex = 0;
            // 
            // okButton
            // 
            okButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            okButton.DialogResult = DialogResult.OK;
            okButton.Location = new Point(5, 187);
            okButton.Name = "okButton";
            okButton.Size = new Size(77, 25);
            okButton.TabIndex = 2;
            okButton.Text = "OK";
            // 
            // closeButton
            // 
            closeButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            closeButton.DialogResult = DialogResult.Cancel;
            closeButton.Location = new Point(82, 187);
            closeButton.Name = "closeButton";
            closeButton.Size = new Size(77, 25);
            closeButton.TabIndex = 3;
            closeButton.Text = "Cancel";
            // 
            // visaResourceNameTextBox
            // 
            visaResourceNameTextBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            visaResourceNameTextBox.Location = new Point(5, 157);
            visaResourceNameTextBox.Name = "visaResourceNameTextBox";
            visaResourceNameTextBox.Size = new Size(282, 20);
            visaResourceNameTextBox.TabIndex = 4;
            visaResourceNameTextBox.Text = "GPIB0::2::INSTR";
            // 
            // AvailableResourcesLabel
            // 
            AvailableResourcesLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            AvailableResourcesLabel.Location = new Point(5, 5);
            AvailableResourcesLabel.Name = "AvailableResourcesLabel";
            AvailableResourcesLabel.Size = new Size(279, 12);
            AvailableResourcesLabel.TabIndex = 5;
            AvailableResourcesLabel.Text = "Available Resources:";
            // 
            // ResourceStringLabel
            // 
            ResourceStringLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            ResourceStringLabel.Location = new Point(5, 141);
            ResourceStringLabel.Name = "ResourceStringLabel";
            ResourceStringLabel.Size = new Size(279, 13);
            ResourceStringLabel.TabIndex = 6;
            ResourceStringLabel.Text = "Resource String:";
            // 
            // SelectResource
            // 
            AcceptButton = okButton;
            AutoScaleBaseSize = new Size(5, 13);
            CancelButton = closeButton;
            ClientSize = new Size(292, 220);
            Controls.Add(ResourceStringLabel);
            Controls.Add(AvailableResourcesLabel);
            Controls.Add(visaResourceNameTextBox);
            Controls.Add(closeButton);
            Controls.Add(okButton);
            Controls.Add(_AvailableResourcesListBox);
            Icon = (Icon)resources.GetObject("$this.Icon");
            KeyPreview = true;
            MaximizeBox = false;
            MinimumSize = new Size(177, 247);
            Name = "SelectResource";
            StartPosition = FormStartPosition.CenterParent;
            Text = "Select Resource";
            // Me.Load += New System.EventHandler(Me.OnLoad)
            ResumeLayout(false);
        }
        #endregion


    }
}
