# Find Resources

## Category: VISA

## Description:
This example demonstrates how to use the IVI-VISA .NET
API to find all of the available resources on the system.
In the example, you select between several filters to narrow the list.

## Language: C#  

## Required Software: IVI-VISA  

## Required Hardware: Any message-based device
